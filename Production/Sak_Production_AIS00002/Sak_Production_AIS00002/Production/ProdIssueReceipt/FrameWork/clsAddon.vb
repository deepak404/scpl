Imports System.IO
Imports System.Reflection

Public Class clsAddOn
    Public WithEvents objApplication As SAPbouiCOM.Application
    Public oForm As SAPbouiCOM.Form
    Public oMatrix As SAPbouiCOM.Matrix
    Public objCompany As SAPbobsCOM.Company
    Dim oProgBarx As SAPbouiCOM.ProgressBar
    Public objGenFunc As Ananthi.SBOLib.GeneralFunctions
    Public objUIXml As Ananthi.SBOLib.UIXML
    Public ZB_row As Integer = 0
    'Public object_name As Classfilename
    Public otblcrt As New tablecreafn
    Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
    Dim oUserTablesMD As SAPbobsCOM.UserTablesMD
    Dim ret As Long
    Dim str As String
    Dim objForm As SAPbouiCOM.Form
    Dim MenuCount As Integer = 0

    'Public objcommon As clsCommon
    'Public objtestapplication As clstestapplication
    Public objMachineGroup As ClsMachineGroupMaster
    Public objToolsGroup As clsToolsGroupMastervb
    Public objLabourGroup As ClsLabourGroupMaster
    Public objLabourMaster As clsLabourMaster
    'Public objMachineMaster As clsMachineMaster

    Public objProductionReceipt As clsProductionReceipt
    Public objRMEntry As clsRMEntry
    Public objOpration As ClsOperationMstr
    Public objQltyParam As ClsQualityParam
    Public objWorkOrder As ClsWorkOrder
    'Public objProDcution As ClsProductiontransfer
    Public objRoot As ClsRootMaster
    Public objPartsDetails As clsPartsDetails
    Public objGeneralSetting As clsGeneralSetting
    Public objBuyProduct As ClsBuyProductDetails
    Public objAPInvoice As ClsAPInvoice
    'Public ObjTl As ClsTool
    'Public objMachineMaster1 As MachineMaster
    Dim Rootcard = New String(,) {{"C", "Consumables"}, {"R", "Raw Materials"}, {"T", "Tools"}, {"S", "Scrab"}, {"M", "Machinery"}, {"L", "Labour"}}

    Private Function CheckLicense() As Boolean
        Try
            Dim onewfom As SAPbouiCOM.Form
            Dim HardWareKey As String
            objAddOn.objApplication.Menus.Item("257").Activate()
            onewfom = objAddOn.objApplication.Forms.ActiveForm
            HardWareKey = onewfom.Items.Item("79").Specific.string
            onewfom.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

            For I As Integer = 0 To HWKey.Length - 1
                If HWKey(I) = HardWareKey Then
                    Return True
                End If
            Next
            MessageBox.Show("You are not Authorized Run the Addon")
            Return False

        Catch ex As Exception
            objApplication.SetStatusBarMessage("Check License Function Failue - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Function

    Public Sub Intialize()
        Dim objSBOConnector As New Ananthi.SBOLib.SBOConnector
        objApplication = objSBOConnector.GetApplication(System.Environment.GetCommandLineArgs.GetValue(1))
        objCompany = objSBOConnector.GetCompany(objApplication)
        If objAddOn.objCompany.InTransaction = False Then objAddOn.objCompany.StartTransaction()
        If objAddOn.objCompany.InTransaction Then
            objAddOn.objApplication.StatusBar.SetText("Validate:" & objAddOn.objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

        End If

        'If CheckLicense() = True Then
        Try
            'RMBIN()
            'InsBIn()

            createObjects()
            createTables()
            createUDOs()
            loadMenu()
            BatchSerial()
        Catch ex As Exception
            ' objAddOn.objApplication.MessageBox(ex.ToString)
            ' End
        End Try

        objApplication.SetStatusBarMessage("Addon connected  successfully!", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        'Else
        'End
        'End If

    End Sub

    Private Sub createUDOs()
         
        Dim ct1(1), ct2(8), Ct3(1), Ct4(1), ct5(1), ct6(1), ct7(2), ct8(2) As String

        
        ct2(0) = "AS_IGN1"
        ct2(1) = "AS_IGN2"
        ct2(2) = "AS_IGN3"
        ct2(3) = "AS_IGN4"
        ct2(4) = "AS_IGN5"
        ct2(5) = "AS_IGN6"
        ct2(6) = "AS_IGN7"
        ct2(7) = "AS_IGN8"

        createUDO("AS_OIGN", "AS_OIGN", "Production Receipt", ct2, SAPbobsCOM.BoUDOObjType.boud_Document, False, True)

      

        If Not otblcrt.UDOExists("AS_OPRN") Then
            Dim findAliasNDescription = New String(,) {{"Code", "Code"}, {"Name", "Name"}, {"U_Cycle", "U_Cycle"}}
            otblcrt.RegisterUDO("AS_OPRN", "Operation Master", SAPbobsCOM.BoUDOObjType.boud_MasterData, findAliasNDescription, "AS_OPRN", "AS_PRN1", "AS_PRN2", "AS_PRN3", "AS_PRN4")
            findAliasNDescription = Nothing
        End If
        If Not otblcrt.UDOExists("AS_ROOT") Then
            Dim findAliasNDescription = New String(,) {{"DocNum", "DocNum"}, {"U_RuleC", "U_RuleC"}, {"U_RuleN", "U_RuleN"}}
            'otblcrt.RegisterUDO("AS_ROOT", "Operation Master", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AS_ROOT", "AS_ROOT1")
            otblcrt.RegisterUDO("AS_ROOT", "Root Card Master", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AS_ROOT", "AS_ROOT1")
            findAliasNDescription = Nothing
        End If
        If Not otblcrt.UDOExists("AS_OWORD") Then
            Dim findAliasNDescription = New String(,) {{"DocNum", "DocNum"}, {"DocEntry", "DocEntry"}, {"U_RuleC", "U_RuleC"}, {"U_RuleN", "U_RuleN"}}
            otblcrt.RegisterUDO("AS_OWORD", "Work Order", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AS_OWORD", "AS_WORD1", "AS_WORD2", "AS_WORD3", "AS_WORD4")
            findAliasNDescription = Nothing
        End If
        If Not otblcrt.UDOExists("AIS_OMCD") Then
            Dim findAliasNDescription = New String(,) {{"DocNum", "DocNum"}}
            otblcrt.RegisterUDO("AIS_OMCD", "Machine Details", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OMCD", "AIS_MCD1")
            findAliasNDescription = Nothing
        End If
        


    End Sub

    Private Sub createObjects()
        'Library Object Initilisation
        oBatch = New ClsBatch
        objGenFunc = New Ananthi.SBOLib.GeneralFunctions(objCompany)
        objUIXml = New Ananthi.SBOLib.UIXML(objApplication)
        'Business Object Initialisation
        objLabourMaster = New clsLabourMaster
        objLabourGroup = New ClsLabourGroupMaster
        'objMachineMaster = New clsMachineMaster
        objProductionReceipt = New clsProductionReceipt
        objRMEntry = New clsRMEntry
        objOpration = New ClsOperationMstr
        objQltyParam = New ClsQualityParam
        objWorkOrder = New ClsWorkOrder
        'objProDcution = New ClsProductiontransfer
        objRoot = New ClsRootMaster
        objPartsDetails = New clsPartsDetails
        'ObjTl = New ClsTool
        objGeneralSetting = New clsGeneralSetting
        objMachineGroup = New ClsMachineGroupMaster
        objToolsGroup = New clsToolsGroupMastervb
        objBuyProduct = New ClsBuyProductDetails
        ' objMachineMaster1 = New MachineMaster
        objAPInvoice = New ClsAPInvoice

    End Sub

    Private Sub objApplication_ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) Handles objApplication.ItemEvent
        Try
            Select Case pVal.FormTypeEx

             
                    'Case "MNU_OLBR"
                    '    objLabourMaster.ItemEvent(FormUID, pVal, BubbleEvent)

                    'Case "RMEN"
                    '    objRMEntry.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "OPRN"
                    objOpration.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "AIS_PARAM"
                    '    objQltyParam.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "AS_OIGN"
                    objProductionReceipt.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "AS_OWORD"
                    objWorkOrder.ItemEvent(FormUID, pVal, BubbleEvent)

                Case "AIS_OMCD"
                    objMachine.ItemEvent(FormUID, pVal, BubbleEvent)



                    'Case "PORDER"
                    '    objProDcution.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "ROOT"
                    objRoot.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "PARTS"
                    '    objPartsDetails.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "WIPGT"
                    '    objGeneralSetting.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "OMGR"
                    '    objMachineGroup.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "OTGR"
                    '    objToolsGroup.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "OLGM"
                    '    objLabourGroup.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "AS_BDET"
                    '    objBuyProduct.ItemEvent(FormUID, pVal, BubbleEvent)
                Case batserFormId
                    obatserno.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case GenInsBINFormId
                    '    objProductionReceipt.ItemEvent_RM(FormUID, pVal, BubbleEvent)
                    'Case GenRMBINFormId
                    '    objProductionReceipt.ItemEvent_RMBIN(FormUID, pVal, BubbleEvent)
                Case ItemOperationFormID
                    objItemOperation.ItemEvent(FormUID, pVal, BubbleEvent)

                Case MachineFormID
                    objMachine.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "0"
                    Try
                        Dim oMessageForm As SAPbouiCOM.Form
                        oMessageForm = objApplication.Forms.Item(pVal.FormUID)
                        Dim item As SAPbouiCOM.Item
                        For Each item In oMessageForm.Items
                            If item.UniqueID = "1" Then
                                Dim oStatic As SAPbouiCOM.StaticText
                                oStatic = oMessageForm.Items.Item("7").Specific
                                If (oStatic.Caption.Contains("Unsaved data will be lost. Do you want to continue without saving?")) And sPressed = False Then
                                    sPressed = True
                                    oMessageForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    Exit For
                                End If
                            End If
                        Next
                    Catch ex As Exception
                    End Try



                    'Case APInvoiceFormID
                    '    objAPInvoice.ItemEvent(FormUID, pVal, BubbleEvent)
            End Select
            Select Case FormUID


                Case PlanningDetailsFormID
                    objProductionReceipt.ItemEvent_SubGrid(PlanningDetailsFormID, pVal, BubbleEvent)
                Case MachineUniqueId
                    '    oMachineMaster.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case PartsUniqueId
                    '    oPartsMaster.ItemEvent(FormUID, pVal, BubbleEvent)


            End Select
        Catch ex As Exception
            objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub

    Private Sub objApplication_FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean) Handles objApplication.FormDataEvent
        Try
            'If BusinessObjectInfo.FormTypeEx = "149" And (BusinessObjectInfo.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD) And BusinessObjectInfo.BeforeAction = False And BusinessObjectInfo.ActionSuccess = True Then
            'ElseIf BusinessObjectInfo.FormTypeEx = "143" And (BusinessObjectInfo.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD) And BusinessObjectInfo.BeforeAction = False And BusinessObjectInfo.ActionSuccess = True Then
            'End If
            ''If BusinessObjectInfo.FormTypeEx = "MNU_OIGN" And BusinessObjectInfo.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD Then
            ''    objProductionReceipt.FormDataEvent(BusinessObjectInfo.FormUID, BusinessObjectInfo, BubbleEvent)
            ''    'objProductionReceipt.FormDataEvent(BusinessObjectInfo, BubbleEvent)
            ''End If



            Select Case BusinessObjectInfo.FormTypeEx
                'Case MachineMasterFormID
                '    oMachineMaster.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case ItemOperationFormID
                    objItemOperation.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    'Case PartsMasterFormID
                    '    oPartsMaster.FormDataEvent(BusinessObjectInfo, BubbleEvent)

                    'Case MachineFormID
                    '    objMachine.FormDataEvent(BusinessObjectInfo, BubbleEvent)


                Case batserFormId
                    obatserno.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case "AS_OWORD"
                    objWorkOrder.FormDataEvent(BusinessObjectInfo.FormUID, BusinessObjectInfo, BubbleEvent)
                    'Case "141"
                    '    objAPInvoice.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    'Case "150"
                    '    oItm.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    'Case "WIPGT"
                    '    objGeneralSetting.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    'Case "OMGR"
                    '    objMachineGroup.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    'Case "OTGR"
                    '    objToolsGroup.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    'Case "OLGM"
                    '    objLabourGroup.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case "OPRN"
                    objOpration.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    'Case "AIS_PARAM"
                    '    objQltyParam.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    'Case "MNU_OLBR"
                    '    objLabourMaster.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case "AS_ROOT"
                    objRoot.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    'Case "MNU_OIGN"
                    '    objProductionReceipt.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    '    Case "AS_OIGN"
                    '        objProductionReceipt.FormDataEvent(BusinessObjectInfo, BubbleEvent)


            End Select

        Catch ex As Exception
            '  MsgBox(ex.ToString)
            objAddOn.objApplication.StatusBar.SetText(ex.ToString, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try



    End Sub

    Public Shared Function ErrorHandler(ByVal p_ex As Exception, ByVal objApplication As SAPbouiCOM.Application)
        Dim sMsg As String = Nothing
        If p_ex.Message = "Form - already exists [66000-11]" Then
            Return True
            Exit Function  'ignore error
        End If
        Return False
    End Function

    Private Sub objApplication_MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) Handles objApplication.MenuEvent
        If pVal.BeforeAction Then
            Select Case objAddOn.objApplication.Forms.ActiveForm.TypeEx
                'Case UOMTypeEx
                '    oUOM.MenuEvent(pVal, BubbleEvent)
            End Select
            Select Case objAddOn.objApplication.Forms.ActiveForm.UniqueID
                'Case MachineUniqueId
                '    oMachineMaster.MenuEvent(pVal, BubbleEvent)
                'Case PartsUniqueId
                '    oPartsMaster.MenuEvent(pVal, BubbleEvent)
                'Case GenInsBINFormId
                '    objProductionReceipt.BIN_MenuEvent(pVal, BubbleEvent)
                'Case ItemOperationFormID
                '    objItemOperation.MenuEvent(pVal, BubbleEvent)
            End Select
        Else
            Try
                Select Case pVal.MenuUID


                    'Case "MNU_OLBR"
                    '    objLabourMaster.LoadScreen()
                    'Case "MNU_OIGE"
                    '    'objProductionIssue.LoadScreen()
                    Case "AS_OIGN"
                        objProductionReceipt.LoadXML()
                    Case "MNU_OIOD"
                        objItemOperation.LoadForm()
                    Case "MNU_OPRN"
                        objOpration.LoadScreen()
                    Case "AS_OWORD"
                        objWorkOrder.LoadScreen()
                        'Case "MNU_PORDER"
                        '    objProDcution.LoadScreen()
                    Case "MNU_ROOT"
                        objRoot.LoadScreen()

                    Case "AIS_OMCD"
                        objMachine.LoadScreen()
                        '    'Case "MNU_OTOOL"
                        '    '    ObjTl.LoadForm()
                        '    'ObjTl.LoadScreen()
                        'Case "MNU_WIPGT"
                        '    objGeneralSetting.LoadForm()
                        'Case "MNU_OMGR"
                        '    objMachineGroup.LoadForm()
                        'Case "MNU_OTGR"
                        '    objToolsGroup.LoadForm()
                        'Case "MNU_OLGM"
                        '    objLabourGroup.LoadScreen()
                        'Case "AIS_Batch"
                        '    oBatch.LoadScreen()
                        'Case MachineMasterFormID
                        '    oMachineMaster.LoadForm()
                        'Case PartsMasterFormID
                        '    oPartsMaster.LoadForm()
                        'Case ItemMaseterMenuId
                        '    oItm.LoadForm()
                        'Case BatchFormID
                        '    oBatch.LoadScreen()


                        'Select Case objAddOn.objApplication.Forms.ActiveForm.UniqueID
                        '    Case MachineUniqueId
                        '        oMachineMaster.MenuEvent(pVal, BubbleEvent)
                        '    Case PartsUniqueId
                        '        oPartsMaster.MenuEvent(pVal, BubbleEvent)
                        'End Select
                End Select


                If pVal.MenuUID = "1283" Or pVal.MenuUID = "1282" Or pVal.MenuUID = "1289" Or pVal.MenuUID = "1290" Or pVal.MenuUID = "1292" Or pVal.MenuUID = "1293" Or pVal.MenuUID = "1291" Or pVal.MenuUID = "1288" Or pVal.MenuUID = "1281" Or pVal.MenuUID = "Delete" Or pVal.MenuUID = "Add" Then

                    Select Case objAddOn.objApplication.Forms.ActiveForm.UniqueID
                        Case "AS_OWORD"
                            objWorkOrder.Menu_event(pVal, BubbleEvent)
                        

                    End Select
                    Select Case objAddOn.objApplication.Forms.ActiveForm.TypeEx
                        'Case "OPRN"
                        '    objOpration.Menu_event(pVal, BubbleEvent)
                        'Case "AIS_PARAM"
                        '    objQltyParam.Menu_event(pVal, BubbleEvent)
                        'Case "MNU_OIGE"
                        '    ' objProductionIssue.NonEditableMode()
                        Case "AS_OWORD"
                            objWorkOrder.Menu_event(pVal, BubbleEvent)
                        Case "AIS_OMCD"
                            objMachine.Menu_event(pVal, BubbleEvent)

                      
                            '    'Case "PORDER"
                            '    '    objProDcution.Menu_event(pVal, BubbleEvent)
                            'Case "ROOT"
                            '    objRoot.Menu_event(pVal, BubbleEvent)
                            'Case "PARTS"
                            '    objPartsDetails.Menu_event(pVal, BubbleEvent)
                            'Case "OTGR"
                            '    objToolsGroup.MenuEvent(pVal, BubbleEvent)
                            'Case "OLGM"
                            '    objLabourGroup.MenuEvent(pVal, BubbleEvent)
                            'Case "OMGR"
                            '    objMachineGroup.MenuEvent(pVal, BubbleEvent)


                    End Select
                End If




            Catch ex As Exception
                objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            End Try
        End If

    End Sub

    Private Sub loadMenu()


        MenuCount = objApplication.Menus.Item("43520").SubMenus.Count

        CreateMenu("", MenuCount + 1, "Production", SAPbouiCOM.BoMenuType.mt_POPUP, "MNU_PDN", objApplication.Menus.Item("43520"))

        CreateMenu("", 8, "Operation Master", SAPbouiCOM.BoMenuType.mt_STRING, "MNU_OPRN", objApplication.Menus.Item("MNU_PDN"))
        CreateMenu("", 9, "Route Card Master", SAPbouiCOM.BoMenuType.mt_STRING, "MNU_ROOT", objApplication.Menus.Item("MNU_PDN"))
        CreateMenu("", 10, "Work Order", SAPbouiCOM.BoMenuType.mt_STRING, "AS_OWORD", objApplication.Menus.Item("MNU_PDN"))
        CreateMenu("", 11, "Item operation", SAPbouiCOM.BoMenuType.mt_STRING, "MNU_OIOD", objApplication.Menus.Item("MNU_PDN"))
        CreateMenu("", 12, "Production Receipt", SAPbouiCOM.BoMenuType.mt_STRING, "AS_OIGN", objApplication.Menus.Item("MNU_PDN"))
        CreateMenu("", 12, "Machine Details", SAPbouiCOM.BoMenuType.mt_STRING, "AIS_OMCD", objApplication.Menus.Item("MNU_PDN"))

    End Sub
    ' For Menu Creation
    Private Function CreateMenu(ByVal ImagePath As String, ByVal Position As Int32, ByVal DisplayName As String, ByVal MenuType As SAPbouiCOM.BoMenuType, ByVal UniqueID As String, ByVal ParentMenu As SAPbouiCOM.MenuItem) As SAPbouiCOM.MenuItem
        Try
            Dim oMenuPackage As SAPbouiCOM.MenuCreationParams
            oMenuPackage = objApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
            oMenuPackage.Image = ImagePath
            oMenuPackage.Position = Position
            oMenuPackage.Type = MenuType
            oMenuPackage.UniqueID = UniqueID
            oMenuPackage.String = DisplayName
            ParentMenu.SubMenus.AddEx(oMenuPackage)
        Catch ex As Exception
            objApplication.StatusBar.SetText("Menu Already Exists", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_None)
        End Try
        Return ParentMenu.SubMenus.Item(UniqueID)
    End Function

    Private Sub createTables()

        Dim objUDFEngine As New Ananthi.SBOLib.UDFEngine(objCompany)
        objAddOn.objApplication.SetStatusBarMessage("Creating Tables Please Wait...", SAPbouiCOM.BoMessageTime.bmt_Long, False)

        objUDFEngine.CreateTable("AIS_WrkTyp", "Work Order Type", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        objUDFEngine.AddAlphaField("@AIS_WrkTyp", "WhsCode", "Wharehouse Code", 50)
        objUDFEngine.AddAlphaField("@AIS_WrkTyp", "WhsName", "Wharehouse Name", 100)
        objUDFEngine.AddAlphaField("@AIS_WrkTyp", "WeightLoad", "WeightLoad", 1) 'Y,N
        objUDFEngine.AddAlphaField("@AIS_WrkTyp", "InspReq", "InspReq", 1) 'Y,N


        objUDFEngine.AddAlphaField("OIGN", "BaseLine", "BaseLine", 11)
        objUDFEngine.AddAlphaField("OIGN", "WBaseNum", "Work Order Base Num", 11)


        objUDFEngine.CreateTable("AIS_OMCD", "Machine Details", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddAlphaField("@AIS_OMCD", "MCCode", "Machine Code", 100)
        objUDFEngine.AddAlphaField("@AIS_OMCD", "Series", "Series", 30)
        objUDFEngine.AddDateField("@AIS_OMCD", "DocDate", "Document Date", SAPbobsCOM.BoFldSubTypes.st_None)

        objUDFEngine.CreateTable("AIS_MCD1", "Parameter Detail", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AIS_MCD1", "Parameter", "Parameter", 30)

         
   

        'Opertion Master 
        objUDFEngine.CreateTable("AS_OPRN", "Operation Master", SAPbobsCOM.BoUTBTableType.bott_MasterData)

        objUDFEngine.AddFloatField("@AS_OPRN", "Cycle", "Standard Cycle table", SAPbobsCOM.BoFldSubTypes.st_Rate)
        objUDFEngine.addField("@AS_OPRN", "Process", "Process Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "P,R,PA", "Process,Issue/Receipt,Packing", "R")
        objUDFEngine.AddAlphaField("@AS_OPRN", "Type", "Type", 20)
        objUDFEngine.AddFloatField("@AS_OPRN", "ASpindle", "Available Spindle", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_OPRN", "Production", "Production", 20)
        objUDFEngine.addField("@AS_OPRN", "FOperation", "FOperation", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddDateField("@AS_OPRN", "Reset", "Last Reset Date", SAPbobsCOM.BoFldSubTypes.st_None)

        objUDFEngine.CreateTable("AS_PRN1", "Operation Labour Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AS_PRN1", "LabourCode", "Labour Code", 30)
        objUDFEngine.AddAlphaField("@AS_PRN1", "LabourName", "Labour Name", 100)



        objUDFEngine.CreateTable("AS_PRN2", "Operation Machine Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AS_PRN2", "mcode", "Machine Code", 30)
        objUDFEngine.AddAlphaField("@AS_PRN2", "mname", "Machine Name", 100)


        objUDFEngine.CreateTable("AS_PRN3", "Operation IN Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AS_PRN3", "ItmCde", "Item Code", 30)
        objUDFEngine.AddAlphaField("@AS_PRN3", "Type", "Type", 20)
        objUDFEngine.AddAlphaField("@AS_PRN3", "Whs", "Warehouse", 30)
        objUDFEngine.AddAlphaField("@AS_PRN3", "WName", "Warehouse Name", 100)
        objUDFEngine.AddAlphaField("@AS_PRN3", "Desc", "Desc Name", 100)
        objUDFEngine.AddAlphaField("@AS_PRN3", "UOM", "UOM", 100)
        objUDFEngine.AddFloatField("@AS_PRN3", "RecipeQty", "Recipe Qty", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_PRN3", "Qty", "Qty", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_PRN3", "Remarks", "Remarks", 200)


        objUDFEngine.CreateTable("AS_PRN4", "Operation OUT Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)

        objUDFEngine.AddFloatField("@AS_PRN4", "BQty", "Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_PRN4", "Whs", "Warehouse", 30)
        objUDFEngine.AddAlphaField("@AS_PRN4", "WName", "Warehouse Name", 30)
        objUDFEngine.AddAlphaField("@AS_PRN4", "Desc", "Desc Name", 100)
        objUDFEngine.AddAlphaField("@AS_PRN4", " FgCode", " FgCode", 100)
        objUDFEngine.addField("@AS_PRN4", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 10, SAPbobsCOM.BoFldSubTypes.st_None, "M,B", "Main,ByProduct", "B")

        objUDFEngine.CreateTable("AS_OWORD", "Work Order Header", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddAlphaField("@AS_OWORD", "Type", "Type", 50)
        objUDFEngine.AddAlphaField("@AS_OWORD", "RuleC", "Rule Code", 50)
        objUDFEngine.AddAlphaField("@AS_OWORD", "RuleN", "Rule Name", 100)
        objUDFEngine.AddAlphaField("@AS_OWORD", "FGCode", "FgCode", 50)
        objUDFEngine.AddAlphaField("@AS_OWORD", "FgName", "FgName", 100)
        objUDFEngine.AddFloatField("@AS_OWORD", "NetWeight", "NetWeight", SAPbobsCOM.BoFldSubTypes.st_Quantity)

        objUDFEngine.AddFloatField("@AS_OWORD", "ProducedQty", "ProducedQty", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_OWORD", "SalesOrderQty", "TotalQty", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.addField("@AS_OWORD", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "O,C", "Open,Close", "O")
        objUDFEngine.AddDateField("@AS_OWORD", "DocDate", "DocDate", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_OWORD", "StartDate", "StartDate", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_OWORD", "EndDate", "EndDate", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddAlphaField("@AS_OWORD", "BaseNum", "BaseNum", 11)
        objUDFEngine.AddAlphaField("@AS_OWORD", "BaseLine", "BaseLine", 11)
        objUDFEngine.AddAlphaField("@AS_OWORD", "BaseEntry", "BaseEntry", 11)
        objUDFEngine.AddAlphaField("@AS_OWORD", "BaseObject", "BaseObject", 30)
        objUDFEngine.AddAlphaField("@AS_OWORD", "Series", "Series", 30)
        objUDFEngine.AddAlphaField("@AS_OWORD", "MachineCode", "Machine Code", 50)
        objUDFEngine.AddAlphaField("@AS_OWORD", "MachineName", "Machine Name", 100)
        objUDFEngine.AddFloatField("@AS_OWORD", "RequiredHrs", "RequiredHrs", SAPbobsCOM.BoFldSubTypes.st_Quantity)


        objUDFEngine.CreateTable("AS_WORD1", "Work Order Operation", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AS_WORD1", "OpCode", "OpCode", 50)
        objUDFEngine.AddAlphaField("@AS_WORD1", "OpName", "OpName", 100)
        objUDFEngine.AddFloatField("@AS_WORD1", "PCyc", "Process Cycle Per Unit", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_WORD1", "Labour", "Labour", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_WORD1", "Process", "Process Type", 50)
        objUDFEngine.AddAlphaField("@AS_WORD1", "Unit", "No of Unit", 50)
        objUDFEngine.addField("@AS_WORD1", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "O,C", "Open,Close", "O")
        'objUDFEngine.addField("@AS_WORD1", "QStatus", "Quality Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "A,R", "Accepted,Rejected", "A")
        objUDFEngine.AddAlphaField("@AS_WORD1", "QParam", "Quality Parameter", 10)
        objUDFEngine.AddFloatField("@AS_WORD1", "PCycle", "Process Cycle", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_WORD1", "ActTime", "Actual Time", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddNumericField("@AS_WORD1", "PDEntry", "Process DocEntry", 8)
        objUDFEngine.AddAlphaField("@AS_WORD1", "RejRes", "Rejected Reason", 250)

        objUDFEngine.AddAlphaField("@AS_WORD1", "Issue", "Issue For Production", 50)
        objUDFEngine.AddAlphaField("@AS_WORD1", "Recpt", "Production Receipt", 50)
        objUDFEngine.AddAlphaField("@AS_WORD1", "IBaseEntry", "Issue For Production", 50)
        objUDFEngine.AddAlphaField("@AS_WORD1", "RBaseEntry", "Goods Receipt", 50)


        objUDFEngine.CreateTable("AS_WORD2", "Work Order Sales Ordre Dt.", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AS_WORD2", "BaseNum", "BaseNum", 11)
        objUDFEngine.AddAlphaField("@AS_WORD2", "BaseEntry", "BaseEntry", 11)
        objUDFEngine.AddAlphaField("@AS_WORD2", "CardCode", "CardCode", 50)
        objUDFEngine.AddAlphaField("@AS_WORD2", "CardName", "CardName", 100)
        objUDFEngine.AddFloatField("@AS_WORD2", "TotalQty", "TotalQty", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_WORD2", "CompletedQTy", "CompletedQTy", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_WORD2", "PendingQty", "PendingQty", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_WORD2", "Qty", "Qty", SAPbobsCOM.BoFldSubTypes.st_Quantity)

         
        objUDFEngine.CreateTable("AS_WORD3", "Work Order Doffno Det.", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AS_WORD3", "DoffNo", "DoffNo", 100)
        objUDFEngine.AddAlphaField("@AS_WORD3", "MCCode", "MCCode", 50)
        objUDFEngine.AddAlphaField("@AS_WORD3", "MCName", "MCName", 100)
        objUDFEngine.AddAlphaField("@AS_WORD3", "Reason", "Reason", 100)
        objUDFEngine.AddAlphaField("@AS_WORD3", "QBaseNum", "QBaseNum", 11)
        objUDFEngine.AddAlphaField("@AS_WORD3", "QBaseEntry", "QBaseEntry", 11)

        objUDFEngine.addField("@AS_WORD3", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "A,R,W", "Accepted,Rejected,Waiting", "W")
        objUDFEngine.addField("@AS_WORD3", "LineStatus", "Line Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "O,C", "Open,Close", "O")


        objUDFEngine.CreateTable("AS_WORD4", "Work Order Receipt Nos.", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AS_WORD4", "BaseNum", "BaseNum", 11)
        objUDFEngine.AddAlphaField("@AS_WORD4", "BaseEntry", "BaseEntry", 11)
        objUDFEngine.AddAlphaField("@AS_WORD4", "Series", "Series", 100)
        objUDFEngine.AddAlphaField("@AS_WORD4", "LineID", "LineID", 100)
        objUDFEngine.AddAlphaField("@AS_WORD4", "GIBaseNum", "GIBaseNum", 11)
        objUDFEngine.AddAlphaField("@AS_WORD4", "GIBaseEntry", "GIBaseEntry", 11)
        objUDFEngine.AddAlphaField("@AS_WORD4", "GRBaseNum", "GRBaseNum", 11)
        objUDFEngine.AddAlphaField("@AS_WORD4", "GRBaseEntry", "GRBaseEntry", 11)


     


        'objUDFEngine.CreateTable("AIS_OPART", "Part Details", SAPbobsCOM.BoUTBTableType.bott_Document)
        'objUDFEngine.AddAlphaField("@AIS_OPART", "mcode", "Machine Code", 50)
        'objUDFEngine.AddAlphaField("@AIS_OPART", "mname", "Machine Name", 100)

        'objUDFEngine.CreateTable("AIS_PART1", "Part Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        'objUDFEngine.AddAlphaField("@AIS_PART1", "pcode", "Part Code", 20)
        'objUDFEngine.AddAlphaField("@AIS_PART1", "pname", "Part Name", 150)
        'objUDFEngine.AddAlphaField("@AIS_PART1", "specifn", "Specification", 250)
        'objUDFEngine.AddFloatField("@AIS_PART1", "qty", "Qty", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        'objUDFEngine.AddFloatField("@AIS_PART1", "life", "Frequency(Days)", SAPbobsCOM.BoFldSubTypes.st_Rate)
        'objUDFEngine.AddDateField("@AIS_PART1", "lmd", "Last Maintenance Date", SAPbobsCOM.BoFldSubTypes.st_None)
        'objUDFEngine.AddNumericField("@AIS_PART1", "nod", "Period", 10)
        'objUDFEngine.AddDateField("@AIS_PART1", "nmd", "Next Maintenance Date", SAPbobsCOM.BoFldSubTypes.st_None)

        'objUDFEngine.CreateTable("AS_TANK", "Tank Information", SAPbobsCOM.BoUTBTableType.bott_NoObject)

        '    'RootCard
        objUDFEngine.CreateTable("AS_ROOT", "Root Order", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddAlphaField("@AS_ROOT", "RuleC", "Rule Code", 50)
        objUDFEngine.AddAlphaField("@AS_ROOT", "RuleN", "Rule Name", 100)
        objUDFEngine.AddAlphaField("@AS_ROOT", "FGCode", "FgCode", 50)
        objUDFEngine.AddAlphaField("@AS_ROOT", "FgName", "FgName", 100)
        objUDFEngine.AddDateField("@AS_ROOT", "DDate", "DocDate", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddNumericField("@AS_ROOT", "number", "Running Number", 10)
        'otblcrt.CreateUserFields("@AIS_WIPU", "DDate", "Docdate", SAPbobsCOM.BoFieldTypes.db_Date)
        objUDFEngine.CreateTable("AS_ROOT1", "Root Order Lines", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AS_ROOT1", "OpCode", "OpCode", 50)
        objUDFEngine.AddAlphaField("@AS_ROOT1", "OpName", "OpName", 100)
        objUDFEngine.AddFloatField("@AS_ROOT1", "PCyc", "Process Cycle", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_ROOT1", "Process", "Process Type", 50)
        ' objUDFEngine.addField("@AS_ROOT1", "Process", "Process Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "P,R", "Production,Receipt", "P")
        'ProDuction Transfer

        '    objUDFEngine.CreateTable("AS_OPORD", "Production Order", SAPbobsCOM.BoUTBTableType.bott_Document)
        '    objUDFEngine.AddAlphaField("@AS_OPORD", "WONo", "Work Order No", 50)
        '    'objUDFEngine.AddAlphaField("@AS_OPORD", "WoDocentry", "Work Order No", 50)
        '    objUDFEngine.AddAlphaField("@AS_OPORD", "WONa", "Work Order Name", 50)
        '    objUDFEngine.AddDateField("@AS_OPORD", "DDate", "DocDate", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddDateField("@AS_OPORD", "SDate", "Start Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddDateField("@AS_OPORD", "EDate", "End Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddDateField("@AS_OPORD", "STime", "Start Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        '    objUDFEngine.AddDateField("@AS_OPORD", "ETime", "End Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        '    objUDFEngine.AddAlphaField("@AS_OPORD", "THour", "Total Hour", 50)
        '    objUDFEngine.AddFloatField("@AS_OPORD", "PCyc", "Process Cycle", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        '    objUDFEngine.AddNumericField("@AS_OPORD", "WODocE", "Work Order DocEntry", 10)
        '    objUDFEngine.AddAlphaField("@AS_OPORD", "RuleC", "Rule Code", 50)
        '    objUDFEngine.AddAlphaField("@AS_OPORD", "RuleN", "Rule Name", 100)
        '    objUDFEngine.AddAlphaField("@AS_OPORD", "FGCode", "FgCode", 50)
        '    objUDFEngine.AddAlphaField("@AS_OPORD", "FgName", "FgName", 100)
        '    objUDFEngine.AddFloatField("@AS_OPORD", "TotCost", "Total Cost", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        '    objUDFEngine.addField("@AS_OPORD", "FrmTank", "From Tank", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        '    objUDFEngine.addField("@AS_OPORD", "ToTank", "To Tank", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")

        '    objUDFEngine.CreateTable("AS_PORD1", "Production Order Lines", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        '    objUDFEngine.AddAlphaField("@AS_PORD1", "LCode", "Labour Code", 50)
        '    objUDFEngine.AddAlphaField("@AS_PORD1", "LName", "Labour Name", 100)
        '    objUDFEngine.AddDateField("@AS_PORD1", "SDate", "Start Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddDateField("@AS_PORD1", "EDate", "End Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddDateField("@AS_PORD1", "STime", "Start Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        '    objUDFEngine.AddDateField("@AS_PORD1", "ETime", "End Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        '    objUDFEngine.AddAlphaField("@AS_PORD1", "THour", "Total Hour", 50)
        '    objUDFEngine.AddFloatField("@AS_PORD1", "PerHr", "Per Hour Cost", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_PORD1", "LCost", "Labour Total Cost", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.CreateTable("AS_PORD2", "Production Order Lines1", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        '    objUDFEngine.AddDateField("@AS_PORD2", "TTime", "Total Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        '    objUDFEngine.AddAlphaField("@AS_PORD2", "Reason", "Reason", 100)

        '    objUDFEngine.addField("@AS_OPORD", "Process", "Process Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        '    'Master Table for Labour Group Master:
        '    objUDFEngine.CreateTable("AS_OLGM", "Labour Group Master", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        '    'objUDFEngine.AddAlphaField("@AS_OLGM", "lgcode", "Labour Group code", 50)
        '    'objUDFEngine.AddAlphaField("@AS_OLGM", "lgname", "Labour Group Name", 120)
        '    objUDFEngine.AddAlphaField("@AS_OLGM", "Code", "Labour Group Code", 120)
        '    objUDFEngine.AddAlphaField("@AS_OLGM", "Name", "Labour Group Name", 120)
        '    'objUDFEngine.addField("@AS_OLGM", "lgloc", "Labour Group Location", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        '    'objUDFEngine.addField("@AS_OLGM", "lgact", "Labour Group Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")
        '    objUDFEngine.AddAlphaField("@AS_OLGM", "remarks", "Labour Group Remarks", 200)

        '    'Master Table for Machine Group Master:
        '    'objUDFEngine.CreateTable("AS_OMGM", "Machine Group Master", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        '    'objUDFEngine.AddAlphaField("@AS_OMGM", "mgcode", "Labour Group code", 50)
        '    'objUDFEngine.AddAlphaField("@AS_OMGM", "mgname", "Labour Group Name", 120)
        '    'objUDFEngine.addField("@AS_OMGM", "mgact", "Labour Group Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")
        '    'objUDFEngine.AddAlphaField("@AS_OMGM", "remarks", "Labour Group Remarks", 200)

        '    'Master Table for Machine:
        '    objUDFEngine.CreateTable("AS_OMCN", "Machine Master", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        '    objUDFEngine.AddAlphaField("@AS_OMCN", "mname", "Machine Name", 100)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "mcost", "Machine Cost", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "cons", "Consumablest", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "heat", "Heating", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "sundry", "SundryShopSply", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "dgenwhs", "Dpt Gen Whse", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "otrexp", "Other Expense", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "depr", "Depreciation", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "insr", "Insurence", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "rent", "Rent", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "pcph", "Pwr Cons per Hr", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "ikwv", "IKW Value", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "mhour", "No. of Hr per month", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "pcphr", "Per Hr Value Pwr Cons", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "pcpm", "Machine Cost", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddAlphaField("@AS_OMCN", "Specifn", "Specification", 100)
        '    objUDFEngine.AddAlphaField("@AS_OMCN", "Unit", "Unit", 100)
        '    objUDFEngine.AddAlphaField("@AS_OMCN", "Loc", "Location", 100)
        '    objUDFEngine.AddAlphaField("@AS_OMCN", "Sec", "Section", 100)
        '    objUDFEngine.AddDateField("@AS_OMCN", "LMD", "Last Maintenance Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddNumericField("@AS_OMCN", "MP", "Maintenance Period", 50)
        '    objUDFEngine.AddDateField("@AS_OMCN", "NMD", "Next Maintenance Date", SAPbobsCOM.BoFldSubTypes.st_None)

        '    objUDFEngine.AddDateField("@AS_OMCN", "DATE", "Maintenance Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddFloatField("@AS_OMCN", "USAGE", "USAGE", SAPbobsCOM.BoFldSubTypes.st_Rate)

        '    objUDFEngine.addField("@AS_OMCN", "NonProd", "Non Production", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")

        '    'OITM

        '    'Master Table for Machine:
        '    'OITMEXTRAFIELDSS FOR MACHINE:
        '    objUDFEngine.CreateTable("AS_OMCN", "Machine Master", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        '    objUDFEngine.AddAlphaField("OITM", "mname", "Machine Name", 100)
        '    objUDFEngine.AddFloatField("OITM", "mcost", "Machine Cost", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "cons", "Consumablest", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "heat", "Heating", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "sundry", "SundryShopSply", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "dgenwhs", "Dpt Gen Whse", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "otrexp", "Other Expense", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "depr", "Depreciation", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "insr", "Insurence", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "rent", "Rent", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "pcph", "Pwr Cons per Hr", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "ikwv", "IKW Value", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "mhour", "No. of Hr per month", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "pcphr", "Per Hr Value Pwr Cons", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("OITM", "pcpm", "Machine Cost", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddAlphaField("OITM", "Specifn", "Specification", 100)
        '    objUDFEngine.AddAlphaField("OITM", "Unit", "Unit", 100)
        '    objUDFEngine.AddAlphaField("OITM", "Loc", "Location", 100)
        '    objUDFEngine.AddAlphaField("OITM", "Sec", "Section", 100)
        '    objUDFEngine.AddDateField("OITM", "LMD", "Last Maintenance Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddNumericField("OITM", "MP", "Maintenance Period", 50)
        '    objUDFEngine.AddDateField("OITM", "NMD", "Next Maintenance Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddDateField("OITM", "FCDate", "FC Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddDateField("OITM", "InsDate", "Insurance Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddDateField("OITM", "TPTDate", "Transport Permitt Date", SAPbobsCOM.BoFldSubTypes.st_None)

        '    objUDFEngine.AddDateField("OITM", "DATE", "Maintenance Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddFloatField("OITM", "USAGE", "USAGE", SAPbobsCOM.BoFldSubTypes.st_Rate)

        '    objUDFEngine.addField("OITM", "NonProd", "Non Production", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        '    objUDFEngine.addField("OITM", "Consum", "Consumable", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "Petrol,Diesel,Others", "Petrol,Diesel,Others", "Petrol")
        '    objUDFEngine.AddAlphaField("OITM", "Vehicle", "Is Vehicle ", 3, "Y,N", "Yes,No", "N")






        '    'Master for  Tools :
        '    objUDFEngine.CreateTable("AS_OTOOL", "TOOL Master", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        '    objUDFEngine.AddAlphaField("@AS_OTOOL", "tname", "Tools Name", 100)
        '    objUDFEngine.AddFloatField("@AS_OTOOL", "USAGE", "USAGE", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddDateField("@AS_OTOOL", "LMD", "Last Maintenance Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddNumericField("@AS_OTOOL", "MP", "Maintenance Period", 50)
        '    'Master Table for Labour:
        '    objUDFEngine.CreateTable("AS_OLBR", "Labour Master", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        '    objUDFEngine.AddAlphaField("@AS_OLBR", "lname", "Labour Name", 100)
        '    objUDFEngine.AddFloatField("@AS_OLBR", "lcost", "Labour Cost", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.addField("@AS_OLBR", "lgcode", "Labour Group Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        '    objUDFEngine.addField("@AS_OLBR", "lloc", "Labour Location", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        '    objUDFEngine.addField("@AS_OLBR", "lsec", "Labour Section", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        '    objUDFEngine.addField("@AS_OLBR", "lunit", "Labour Unit", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")

        '    objUDFEngine.AddFloatField("@AS_OLBR", "lsal", "Labour Salary", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OLBR", "ldays", "Labour Days", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddFloatField("@AS_OLBR", "lhrs", "Labour Hours", SAPbobsCOM.BoFldSubTypes.st_Rate)
        '    objUDFEngine.AddAlphaField("@AS_OLBR", "remarks", "Labour Remarks", 200)

        'Document Header Table for Production Issue
        'objUDFEngine.CreateTable("AS_OIGE", "Production Issue", SAPbobsCOM.BoUTBTableType.bott_Document)
        'objUDFEngine.AddAlphaField("@AS_OIGE", "icode", "Item Code", 20)
        'objUDFEngine.AddAlphaField("@AS_OIGE", "iname", "Item Description", 150)
        'objUDFEngine.AddNumericField("@AS_OIGE", "number", "Document Number", 10)
        'objUDFEngine.AddNumericField("@AS_OIGE", "idocnum", "GoodsIssue DocNum", 10)
        'objUDFEngine.AddAlphaField("@AS_OIGE", "whscode", "Warehouse Code", 20)
        'objUDFEngine.AddDateField("@AS_OIGE", "DocDate", "Document Date", SAPbobsCOM.BoFldSubTypes.st_None)
        'objUDFEngine.AddFloatField("@AS_OIGE", "totqty", "Total Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        'objUDFEngine.AddAlphaField("@AS_OIGE", "remarks", "Remarks", 200)
        'objUDFEngine.addField("@AS_OIGE", "dstatus", "Document Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "O,C", "Open,Closed", "O")

        'objUDFEngine.AddAlphaField("@AS_OIGE", "SONum", "Sales Order No", 50)
        'objUDFEngine.AddAlphaField("@AS_OIGE", "CustName", "Customer Name", 50)
        'objUDFEngine.AddAlphaField("@AS_OIGE", "Colour", "Colour", 40)
        'objUDFEngine.AddAlphaField("@AS_OIGE", "WONum", "Work Order No", 50)


        ''Document Line Table for Production Issue:
        'objUDFEngine.CreateTable("AS_IGE1", "Production Issue Line", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        'objUDFEngine.AddAlphaField("@AS_IGE1", "itemcode", "Item Code", 30)
        'objUDFEngine.AddAlphaField("@AS_IGE1", "itemname", "Item Descritption", 100)
        'objUDFEngine.AddAlphaField("@AS_IGE1", "invuom", "Inventory UOM", 50)
        'objUDFEngine.AddFloatField("@AS_IGE1", "qty", "Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        'objUDFEngine.AddFloatField("@AS_IGE1", "instock", "InStock Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        'objUDFEngine.AddAlphaField("@AS_IGE1", "whscode", "WareHouse Code", 30)
        'objUDFEngine.AddAlphaField("@AS_IGE1", "remarks", "Remarks", 100)




        '    'Document Header Table for Production Receipt:
        objUDFEngine.CreateTable("AS_OIGN", "Production Receipt", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddFloatField("@AS_OIGN", "NetWeight", "NetWeight", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddNumericField("@AS_OIGN", "idocnum", "Issue DocNum", 10)
        objUDFEngine.AddAlphaField("@AS_OIGN", "isdocnum", "Issue DocNum", 30)
        objUDFEngine.AddAlphaField("@AS_OIGN", "itemcode", "Item Code", 30)
        objUDFEngine.AddAlphaField("@AS_OIGN", "itemname", "Item Descritption", 100)
        objUDFEngine.AddAlphaField("@AS_OIGN", "pouchcode", "Item Code", 30)
        objUDFEngine.AddAlphaField("@AS_OIGN", "pouchname", "Item Descritption", 100)
        objUDFEngine.AddFloatField("@AS_OIGN", "qty", "Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_OIGN", "whscode", "WareHouse Code", 30)
        objUDFEngine.AddDateField("@AS_OIGN", "DocDate", "Document Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddFloatField("@AS_OIGN", "cost", "Cost", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        'objUDFEngine.AddFloatField("@AS_OIGN", "elpsqty", "Elapsed Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_OIGN", "elpsqty", "Elapsed Quantity", 50)
        objUDFEngine.AddFloatField("@AS_OIGN", "mcost", "Total MachineCost", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AS_OIGN", "lcost", "Total LabourCost", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddNumericField("@AS_OIGN", "rdocnum", "GoodsReceipt DocNum", 10)
        objUDFEngine.AddNumericField("@AS_OIGN", "BaseLine", "BaseLine", 10)

        objUDFEngine.AddAlphaField("@AS_OIGN", "remarks", "Remarks", 200)
        objUDFEngine.AddAlphaField("@AS_OIGN", "fgbatch", "FG Batch", 32)
        objUDFEngine.AddAlphaField("@AS_OIGN", "obbatch", "OB Batch", 32)

        objUDFEngine.AddAlphaField("@AS_OIGN", "BatchNo", "BatchNo", 100)
        objUDFEngine.AddAlphaField("@AS_OIGN", "StartNo", "StartNo", 100)


        objUDFEngine.AddFloatField("@AS_OIGN", "NoofDoff", "NoofDoff", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_OIGN", "DoffQty", "NoofDoff Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)

        objUDFEngine.AddFloatField("@AS_OIGN", "totqty", "Tot Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_OIGN", "PCyc", "Process Cycle", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_OIGN", "WONo", "Work Order No", 50)
        objUDFEngine.AddAlphaField("@AS_OIGN", "Lot", "Lot Number", 50)
        objUDFEngine.addField("@AS_OIGN", "Procs", "Receipt Details", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        'objUDFEngine.AddAlphaField("@AS_OPORD", "WoDocentry", "Work Order No", 50)
        'objUDFEngine.AddAlphaField("@AS_OIGN", "WONa", "Work Order Name", 50)
        objUDFEngine.AddAlphaField("@AS_OIGN", "RuleC", "Rule Code", 50)
        objUDFEngine.AddAlphaField("@AS_OIGN", "RuleN", "Rule Name", 100)
        objUDFEngine.addField("@AS_OIGN", "Process", "Process Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.AddAlphaField("@AS_OIGN", "WODocE", "Work Order No", 50)
        objUDFEngine.AddDateField("@AS_OIGN", "SDate", "Start Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_OIGN", "EDate", "End Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddFloatField("@AS_OIGN", "STime", "Start Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddFloatField("@AS_OIGN", "ETime", "End Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddFloatField("@AS_OIGN", "TotHrs", "Total Hours", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.addField("@AS_OIGN", "FrmTank", "From Tank", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.addField("@AS_OIGN", "ToTank", "To Tank", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.AddFloatField("@AS_OIGN", "BiTot", "BiPdt TotQty", SAPbobsCOM.BoFldSubTypes.st_Quantity)

        objUDFEngine.AddAlphaField("@AS_OIGN", "SONum", "Sales Order No", 50)
        objUDFEngine.AddAlphaField("@AS_OIGN", "CustName", "Customer Name", 50)
        objUDFEngine.addField("@AS_OIGN", "Colour", "Colour", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "W,B", "White,Black", "W")
        objUDFEngine.AddAlphaField("@AS_OIGN", "GRBaseNum", "GRBaseNum", 50)
        objUDFEngine.AddAlphaField("@AS_OIGN", "GRBaseEntry", "GRBaseEntry", 50)
        objUDFEngine.AddAlphaField("@AS_OIGN", "GIBaseNum", "GIBaseNum", 50)
        objUDFEngine.AddAlphaField("@AS_OIGN", "GIBaseEntry", "GIBaseEntry", 50)

        'Document Line Table for Production Receipt:

        objUDFEngine.CreateTable("AS_IGN1", "Raw Materials", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AS_IGN1", "mchncode", "Machine Code", 30)
        objUDFEngine.AddAlphaField("@AS_IGN1", "itemcode", "Item Code", 30)
        objUDFEngine.AddAlphaField("@AS_IGN1", "itemname", "Item Descritption", 100)
        objUDFEngine.AddAlphaField("@AS_IGN1", "invuom", "Inventory UOM", 50)
        objUDFEngine.AddFloatField("@AS_IGN1", "qty", "Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_IGN1", "TotQty", "Total Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_IGN1", "BalQty", "Balance Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_IGN1", "instock", "InStock Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_IGN1", "whscode", "WareHouse Code", 30)
        objUDFEngine.AddAlphaField("@AS_IGN1", "WhsName", "WareHouse Name", 30)
        objUDFEngine.AddDateField("@AS_IGN1", "fdate", "From Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN1", "tdate", "To Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN1", "ftime", "From Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddDateField("@AS_IGN1", "ttime", "To Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddAlphaField("@AS_IGN1", "remarks", "Remarks", 100)
        objUDFEngine.AddAlphaField("@AS_IGN1", "batch", "Batch Number", 100)
        objUDFEngine.AddAlphaField("@AS_IGN1", "NtReq", "Issue Not Required", 5)



        objUDFEngine.AddFloatField("@AS_OIGN", "ActualQty", "ActualQty", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_IGN1", "QCDocNum", "QCDocNum", 11)
        objUDFEngine.AddAlphaField("@AS_IGN1", "QCDocEntry", "QCDocEntry", 11)


        objUDFEngine.AddFloatField("@AS_IGN1", "NofSqft", "No.of Sqrft", SAPbobsCOM.BoFldSubTypes.st_Quantity)

        objUDFEngine.AddFloatField("@AS_IGN1", "Moistur", "Moisture Level", SAPbobsCOM.BoFldSubTypes.st_Percentage)

        objUDFEngine.AddAlphaField("@AS_IGN1", "Bin", "Bin Number", 10)
        'Document Line Table for Machine
        objUDFEngine.CreateTable("AS_IGN2", "Machine", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AS_IGN2", "mcode", "Item Code", 30)
        objUDFEngine.AddAlphaField("@AS_IGN2", "mname", "Item Descritption", 100)
        objUDFEngine.AddFloatField("@AS_IGN2", "bundle", "Bundle Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_IGN2", "pouch", "Pouch Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_IGN2", "ob", "OB Pouch Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_IGN2", "bunbach", "Item Code", 32)
        objUDFEngine.AddAlphaField("@AS_IGN2", "poubatch", "Item Code", 32)
        objUDFEngine.AddAlphaField("@AS_IGN2", "obbatch", "Item Code", 32)
        objUDFEngine.AddDateField("@AS_IGN2", "fdate", "From Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN2", "tdate", "To Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN2", "ftime", "From Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddDateField("@AS_IGN2", "ttime", "To Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddFloatField("@AS_IGN2", "tottime", "tottime", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_IGN2", "totcost", "Total Cost", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddAlphaField("@AS_IGN2", "Shift", "Shift", 30)
        'BY Bavani
        objUDFEngine.AddDateField("@AS_IGN2", "Afdate", "From Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN2", "Atdate", "To Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN2", "Aftime", "From Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddDateField("@AS_IGN2", "Attime", "To Time", SAPbobsCOM.BoFldSubTypes.st_Time)


        'Document Line Table for Labour
        objUDFEngine.CreateTable("AS_IGN3", "Labour", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AS_IGN3", "mchncode", "Machine Code", 30)
        objUDFEngine.AddAlphaField("@AS_IGN3", "lcode", "Labour Code", 30)
        objUDFEngine.AddAlphaField("@AS_IGN3", "labcode", "Labour Code", 30)
        objUDFEngine.AddAlphaField("@AS_IGN3", "lname", "Labour Descritption", 100)
        objUDFEngine.AddDateField("@AS_IGN3", "fdate", "From Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN3", "tdate", "To Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN3", "ftime", "From Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddDateField("@AS_IGN3", "ttime", "To Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddFloatField("@AS_IGN3", "tottime", "tottime", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_IGN3", "totcost", "Total Cost", SAPbobsCOM.BoFldSubTypes.st_Sum)


        'Document Line Table for Idle
        objUDFEngine.CreateTable("AS_IGN4", "Idle", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AS_IGN4", "mchncode", "Machine Code", 30)
        objUDFEngine.AddFloatField("@AS_IGN4", "idltime", "Idle Time", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddDateField("@AS_IGN4", "fdate", "From Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN4", "tdate", "To Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN4", "ftime", "From Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddDateField("@AS_IGN4", "ttime", "To Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddAlphaField("@AS_IGN4", "reason", "Idle Reasons", 200)
        objUDFEngine.AddAlphaField("@AS_IGN4", "BaseNum", "BaseNum", 11)
        objUDFEngine.AddAlphaField("@AS_IGN4", "BaseEntry", "BaseEntry", 11)

        objUDFEngine.AddAlphaField("@AS_IGN4", "Shift", "Shift", 30)
        objUDFEngine.AddDateField("@AS_IGN4", "FromDate", "From Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN4", "ToDate", "To Date", SAPbobsCOM.BoFldSubTypes.st_None)


        objUDFEngine.addField("@AS_IGN4", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "N,M,B", "None,Machine Setting,Break Down", "N")


        'Fg
        objUDFEngine.CreateTable("AS_IGN5", "By Products", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AS_IGN5", "itemcode", "Item Code", 30)
        objUDFEngine.AddAlphaField("@AS_IGN5", "itemname", "Item Descritption", 100)
        objUDFEngine.AddAlphaField("@AS_IGN5", "pouchcode", "Item Code", 30)
        objUDFEngine.AddAlphaField("@AS_IGN5", "pouchname", "Item Descritption", 100)
        objUDFEngine.AddFloatField("@AS_IGN5", "qty", "Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_IGN5", "batch", "Batch Number", 100)
        objUDFEngine.AddAlphaField("@AS_IGN5", "Bin", "Bin Number", 100)
        objUDFEngine.AddFloatField("@AS_IGN5", "input", "Prod Input", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_IGN5", "Perc", "% of Lot From TotBags", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_IGN5", "GrpLot", "Group Lot", 100)
        objUDFEngine.AddAlphaField("@AS_IGN5", "whscode", "WareHouse Code", 30)
        objUDFEngine.AddAlphaField("@AS_IGN5", "WhsName", "WareHouse Name", 30)
        objUDFEngine.AddAlphaField("@AS_IGN5", "BoxNum", "BoxNum", 100)
        objUDFEngine.AddFloatField("@AS_IGN5", "ActualQty", "ActualQty", SAPbobsCOM.BoFldSubTypes.st_Quantity)


        objUDFEngine.AddDateField("@AS_IGN5", "fdate", "From Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN5", "tdate", "To Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AS_IGN5", "ftime", "From Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddDateField("@AS_IGN5", "ttime", "To Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        'objUDFEngine.AddAlphaField("@AS_IGN5", "Wast", "Is Waste", 30)
        objUDFEngine.addField("@AS_IGN5", "Wast", "Is Waste", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.addField("@AS_IGN5", "Split", "Splitted", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddAlphaField("@AS_IGN5", "RefNo", "Reference No", 30)

        objUDFEngine.addField("@AS_IGN5", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 10, SAPbobsCOM.BoFldSubTypes.st_None, "M,B", "Main,ByProduct", "B")

        objUDFEngine.AddFloatField("@AS_IGN5", "NofSqft", "No.of Sqrft", SAPbobsCOM.BoFldSubTypes.st_Quantity)

        objUDFEngine.AddFloatField("@AS_IGN5", "WillPerc", "Willing OP Percentage", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AS_IGN5", "WillVal", "WillVal", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AS_IGN5", "ExctOP", "Exact OP", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_IGN5", "OPDiff", "OP Diff", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_IGN5", "ExtOPVal", "Exact OP Value", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AS_IGN5", "Moistur", "Moisture Level", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AS_IGN5", "WillQty", "Willing OP Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)

        objUDFEngine.AddAlphaField("@AS_IGN5", "invuom", "Inventory UOM", 50)


        objUDFEngine.CreateTable("AS_IGN6", "Parameter Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AS_IGN6", "PName", "PName", 30)
        objUDFEngine.AddAlphaField("@AS_IGN6", "Result", "Result", 100)
        objUDFEngine.addField("@AS_IGN6", "Approval", "Approval Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 40, SAPbobsCOM.BoFldSubTypes.st_None, "A,R,W,D", "Accepted,Rejected,Waiting,Deviation", "W")


        objUDFEngine.CreateTable("AS_IGN7", "Spindle Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddFloatField("@AS_IGN7", "NoOfSpindle", "NoOfSpindle", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AS_IGN7", "NoOfSpindle2", "NoOfSpindle2", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddAlphaField("@AS_IGN7", "No", "No", 50)
        objUDFEngine.AddFloatField("@AS_IGN7", "AvaSpindle", "Available Spindle", SAPbobsCOM.BoFldSubTypes.st_Quantity)


        objUDFEngine.CreateTable("AS_IGN8", "Spindle Box Num.", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AS_IGN8", "UniqueID", "UniqueID", 11)
        objUDFEngine.AddAlphaField("@AS_IGN8", "DoffNo", "DoffNo", 100)
        objUDFEngine.AddAlphaField("@AS_IGN8", "Spindle", "Spindle No", 100)

        InsBIn()
        RMBIN()
        ItemOperation()
        ''Buy Product Header
        '    objUDFEngine.CreateTable("AS_BDET", "BuyProdcut Header", SAPbobsCOM.BoUTBTableType.bott_Document)
        '    objUDFEngine.AddAlphaField("@AS_BDET", "WONo", "Work Order No", 50)
        '    objUDFEngine.AddAlphaField("@AS_BDET", "RuleC", "Rule Code", 50)
        '    objUDFEngine.AddAlphaField("@AS_BDET", "itm", "Item Code", 30)
        '    objUDFEngine.AddAlphaField("@AS_BDET", "itmnam", "Item Descritption", 100)
        '    objUDFEngine.AddAlphaField("@AS_BDET", "Whs", "WhsCode", 30)
        '    objUDFEngine.AddFloatField("@AS_BDET", "totqty", "Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)

        ''Buy Product line
        '    objUDFEngine.CreateTable("AS_DET1", "BuyProduct line", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        '    objUDFEngine.AddAlphaField("@AS_DET1", "itemcode", "Item Code", 30)
        '    objUDFEngine.AddAlphaField("@AS_DET1", "itemname", "Item Descritption", 100)
        '    objUDFEngine.AddFloatField("@AS_DET1", "qty", "Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        '    objUDFEngine.AddAlphaField("@AS_DET1", "lot", "Lot Number", 100)
        '    objUDFEngine.AddFloatField("@AS_DET1", "input", "Prod Input", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        '    objUDFEngine.AddFloatField("@AS_DET1", "Perc", "% of Lot From TotBags", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        '    objUDFEngine.AddFloatField("@AS_DET1", "output", "Prod Output", SAPbobsCOM.BoFldSubTypes.st_Quantity)



        '' RM details entry packing
        '    objUDFEngine.CreateTable("AS_RMEN", "RMEntry", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        '    objUDFEngine.AddAlphaField("@AS_RMEN", "mchncode", "Machine Code", 30)
        '    objUDFEngine.AddAlphaField("@AS_RMEN", "dyncode", "Dynamic Code", 30)
        '    objUDFEngine.AddAlphaField("@AS_RMEN", "whscode", "Whse Code", 30)
        '    objUDFEngine.AddAlphaField("@AS_RMEN", "itemcode", "Item Code", 30)
        '    objUDFEngine.AddAlphaField("@AS_RMEN", "itemname", "Item Name", 100)
        '    objUDFEngine.AddAlphaField("@AS_RMEN", "remarks", "Remarks", 254)
        '    objUDFEngine.AddAlphaField("@AS_RMEN", "batch", "Batch", 100)
        '    objUDFEngine.AddAlphaField("@AS_RMEN", "mchncode", "Machine Code", 30)
        '    objUDFEngine.AddFloatField("@AS_RMEN", "qty", "Quantity", SAPbobsCOM.BoFldSubTypes.st_Quantity)

        '' Labour details entry packing
        '    objUDFEngine.CreateTable("AS_PLBR", "Packing Labour", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        '    objUDFEngine.AddAlphaField("@AS_PLBR", "lbrcode", "Machine Code", 30)
        '    objUDFEngine.AddAlphaField("@AS_PLBR", "lbrname", "Labour Name", 100)
        '    objUDFEngine.AddAlphaField("@AS_PLBR", "mchncode", "Machine Code", 30)
        '    objUDFEngine.AddAlphaField("@AS_PLBR", "dyncode", "Dynamic Code", 30)
        '    objUDFEngine.AddFloatField("@AS_PLBR", "nooflbr", "No.Of Labour", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        '    objUDFEngine.AddDateField("@AS_PLBR", "fdate", "From Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddDateField("@AS_PLBR", "tdate", "To Date", SAPbobsCOM.BoFldSubTypes.st_None)
        '    objUDFEngine.AddDateField("@AS_PLBR", "fromtime", "From Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        '    objUDFEngine.AddDateField("@AS_PLBR", "totime", "To Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        '    objUDFEngine.AddDateField("@AS_PLBR", "tothrs", "Total Hours", SAPbobsCOM.BoFldSubTypes.st_Time)
        '    objUDFEngine.AddFloatField("@AS_PLBR", "totcost", "Total Cost", SAPbobsCOM.BoFldSubTypes.st_Sum)

        '' Idle time - packing
        '    objUDFEngine.CreateTable("AS_IDLE", "Idle", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        '    objUDFEngine.AddAlphaField("@AS_IDLE", "mchncode", "Machine Code", 30)
        '    objUDFEngine.AddAlphaField("@AS_IDLE", "dyncode", "Dynamic Code", 30)
        '    objUDFEngine.AddDateField("@AS_IDLE", "idltime", "Idle Time", SAPbobsCOM.BoFldSubTypes.st_Time)
        '    objUDFEngine.AddAlphaField("@AS_IDLE", "reason", "Idle Reasons", 200)

        '    objUDFEngine.AddAlphaField("OITM", "packing", "Is Packing Material", 2, "Y,N", "Yes,No", "N")
        '    objUDFEngine.AddAlphaField("OITM", "Waste", "Is Waste Product", 3, "Y,N", "Yes,No", "N")
        '    otblcrt.CreateUserFieldsComboBox("OITB", "ItmGrp", "Item Group", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", Rootcard, "T")


        '    objUDFEngine.AddFloatField("@AIS_PART1", "liklo", "Last Replace Kilometer", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        '    objUDFEngine.AddAlphaField("@TI_MAINTENANCE", "PartNme", "Part Name", 30)
        '    objUDFEngine.AddAlphaField("@TI_ACTIVITYHDR", "PartNme", "Part Name", 30)
        '    objUDFEngine.CreateTable("AS_TANK", "Tank Information", SAPbobsCOM.BoUTBTableType.bott_NoObject)


        '    machinemaster()

    End Sub


    Sub ItemOperation()
        Try
            Me.ItemOperationHeader()
            Me.ItemOperationDetail()

            If Not objAddOn.UDOExists("IOD") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "DocNum"}}
                objAddOn.RegisterUDO("IOD", "Item Operation Details", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OIOD", "AIS_IOD1")
                findAliasNDescription = Nothing
            End If
        Catch ex As Exception
            'oGFun.ExceptionHandler(ex)
        End Try
    End Sub
    Public Sub ItemOperationHeader()
        Try
            Dim objUDFEngine As New Ananthi.SBOLib.UDFEngine(objCompany)
            ' Create table
            objUDFEngine.CreateTable("AIS_OIOD", "Item Operation Header", SAPbobsCOM.BoUTBTableType.bott_Document)
            'Create field
            otblcrt.CreateUserFields("@AIS_OIOD", "DocDate", "Doc Date", SAPbobsCOM.BoFieldTypes.db_Date)
            otblcrt.CreateUserFields("@AIS_OIOD", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_OIOD", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

        Catch ex As Exception
            objApplication.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub

    Public Sub ItemOperationDetail()
        Try
            Dim objUDFEngine As New Ananthi.SBOLib.UDFEngine(objCompany)
            ' Create table
            objUDFEngine.CreateTable("AIS_IOD1", "Item Operation Detail", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            ' Create Field
            otblcrt.CreateUserFields("@AIS_IOD1", "OperCode", "Operation Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_IOD1", "OperName", "Operation Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            otblcrt.CreateUserFields("@AIS_IOD1", "MCCode", "MC Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_IOD1", "MCName", "MC Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            objUDFEngine.AddFloatField("@AIS_IOD1", "PCyc", "Process Cycle", SAPbobsCOM.BoFldSubTypes.st_Quantity)
            objUDFEngine.AddAlphaField("@AIS_IOD1", "Process", "Process Type", 50)

        Catch ex As Exception
            objApplication.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub


    Sub InsBIn()
        'Bin Allocation
        Dim objUDFEngine As New Ananthi.SBOLib.UDFEngine(objCompany)
        objUDFEngine.CreateTable("AIS_QC_Bin", "Gen. Ins Bin", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        otblcrt.CreateUserFields("@AIS_QC_Bin", "Parent1", "Parent1", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("@AIS_QC_Bin", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("@AIS_QC_Bin", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("@AIS_QC_Bin", "WhsCode", "WhsCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("@AIS_QC_Bin", "WhsNam", "WhsNam", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

        otblcrt.CreateUserFields("@AIS_QC_Bin", "TotQty", "OLength", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
        otblcrt.CreateUserFields("@AIS_QC_Bin", "Qty", "Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
        otblcrt.CreateUserFields("@AIS_QC_Bin", "Bin", "Bin", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("@AIS_QC_Bin", "BName", "BName", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

        otblcrt.CreateUserFields("@AIS_QC_Bin", "BatchNo", "BatchNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

    End Sub
    Sub RMBIN()
        'Bin Allocation Raw Material
        Dim objUDFEngine As New Ananthi.SBOLib.UDFEngine(objCompany)
        objUDFEngine.CreateTable("AIS_QC_Bin1", "Gen. Ins Bin", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        otblcrt.CreateUserFields("@AIS_QC_Bin1", "Parent1", "Parent1", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("@AIS_QC_Bin1", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("@AIS_QC_Bin1", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("@AIS_QC_Bin1", "WhsCode", "WhsCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("@AIS_QC_Bin1", "WhsNam", "WhsNam", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

        otblcrt.CreateUserFields("@AIS_QC_Bin1", "TotQty", "OLength", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
        otblcrt.CreateUserFields("@AIS_QC_Bin1", "Qty", "Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
        otblcrt.CreateUserFields("@AIS_QC_Bin1", "Bin", "Bin", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("@AIS_QC_Bin1", "BName", "BName", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("@AIS_QC_Bin1", "BQty", "BQty", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("@AIS_QC_Bin1", "BatchNo", "BatchNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

    End Sub
    Sub machinemaster()
        ''  for Machine Master
        otblcrt.CreateUserFields("OITM", "MGRCode", "Machine Group Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
        otblcrt.CreateUserFields("OITM", "WhsCode", "Warehouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
        otblcrt.CreateUserFields("OITM", "WhsName", "Warehouse Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("OITM", "Location", "Location", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("OITM", "PerHrRt", "Per Hour Rate", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Rate)

        otblcrt.CreateUserFields("OITM", "Sec", "Section", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
        otblcrt.CreateUserFields("OITM", "Dept", "Department Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
        otblcrt.CreateUserFields("OITM", "Model", "Model No.", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
        otblcrt.CreateUserFields("OITM", "IDate", "Installed Date", SAPbobsCOM.BoFieldTypes.db_Date)

        otblcrt.CreateUserFields("OITM", "AvaiHr", "Available Hours", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
        otblcrt.CreateUserFields("OITM", "SPhone", "Service Phone", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
        otblcrt.CreateUserFields("OITM", "SPriod", "Service Priod", SAPbobsCOM.BoFieldTypes.db_Numeric)
        otblcrt.CreateUserFields("OITM", "LSDate", "Last Service Date", SAPbobsCOM.BoFieldTypes.db_Date)
        otblcrt.CreateUserFields("OITM", "NSDate", "Next Service Date", SAPbobsCOM.BoFieldTypes.db_Date)
        otblcrt.CreateUserFields("OITM", "CardCode", "Vendor Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
        otblcrt.CreateUserFields("OITM", "CardName", "Vendor Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        otblcrt.CreateUserFields("OITM", "Amount", "Amount", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "DsnCap", "Design Capacity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
        otblcrt.CreateUserFields("OITM", "McnCap", "Machine Capacity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
        otblcrt.CreateUserFields("OITM", "Effcy", "Efficiency", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
        'otblcrt.CreateUserFields("OITM", "AvaiHrs""Available Hrs", SAPbouiCOM.BoFieldsType.ft_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)

        'otblcrt.CreateUserFieldsComboBox("OITM", "ToolType", "Tools Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", ToolsType, "P")
        otblcrt.CreateUserFields("OITM", "LifeTime", "Life Time", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)

        'Machine Cost Fields
        otblcrt.CreateUserFields("OITM", "Consumab", "Consumables", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "Heating", "Heating", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "Sundry", "Sundry Shop supply", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "Depart", "Departmental Warehouse", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "Expense", "Other Expense", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "Depreci", "Depreciation", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "Insuranc", "Insurance", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "Rent", "Rent", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "MPHRate", "Machine Per Hour Rate", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "PowerPHr", "Power Consumption per hr", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "Value", "Value for 1KW in Rs", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "PHVPower", "PH.value of power consumption", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "PowerPMn", "Power Consumption per Month", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "NHPMonth", "No of Hours per month", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
        otblcrt.CreateUserFields("OITM", "year", "year", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)

    End Sub



    Private Sub objApplication_RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean) Handles objApplication.RightClickEvent
        'If eventInfo.BeforeAction Then
        '    RemoveRightMenu("AS_OWORD")
        'End If

        Select Case objApplication.Forms.ActiveForm.TypeEx
            'Case UOMTypeEx
            '    oUOM.RightClickEvent(eventInfo, BubbleEvent)



        End Select
        'Select Case objApplication.Forms.ActiveForm.TypeEx
        '    'Case "AS_OWORD"
        '    '    objWorkOrder.RightClickEvent(eventInfo, BubbleEvent)
        '    'Case "PARTS"
        '    '    objPartsDetails.RightClickEvent(eventInfo, BubbleEvent)
        'End Select
        'If eventInfo.FormUID = "RightClk" Then
        '    If (eventInfo.BeforeAction = True) Then
        '        Dim oMenuItem As SAPbouiCOM.MenuItem
        '        Dim oMenus As SAPbouiCOM.Menus

        '        Try
        '            Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
        '            oCreationPackage = objApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)

        '            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
        '            oCreationPackage.UniqueID = "OnlyOnRC"
        '            oCreationPackage.String = "Only On Right Click"
        '            oCreationPackage.Enabled = True

        '            oMenuItem = objApplication.Menus.Item("1280") 'Data'
        '            oMenus = oMenuItem.SubMenus
        '            oMenus.AddEx(oCreationPackage)
        '        Catch ex As Exception
        '            MessageBox.Show(ex.Message)
        '        End Try
        '    Else
        '        Dim oMenuItem As SAPbouiCOM.MenuItem
        '        Dim oMenus As SAPbouiCOM.Menus

        '        Try
        '            objApplication.Menus.RemoveEx("OnlyOnRC")
        '        Catch ex As Exception
        '            MessageBox.Show(ex.Message)
        '        End Try
        '    End If
        'End If

    End Sub
    Sub BatchSerial()
        Try
            Me.BatchSerialHeader()
            Me.BatchSerialDetails()

            If Not objAddOn.UDOExists("BSN") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "DocNum"}}
                objAddOn.RegisterUDO("BSN", "Bat/Ser No", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_RVL_OBSN", "AIS_RVL_BSN1")
                findAliasNDescription = Nothing
            End If
        Catch ex As Exception
            'oGFun.ExceptionHandler(ex)
        End Try
    End Sub
    Public Sub BatchSerialHeader()
        Try
            Dim objUDFEngine As New Ananthi.SBOLib.UDFEngine(objCompany)
            ' Create table
            objUDFEngine.CreateTable("AIS_RVL_OBSN", "Bat/Ser Hed", SAPbobsCOM.BoUTBTableType.bott_Document)
            otblcrt.CreateUserFields("@AIS_RVL_OBSN", "DocDate", "Doc Date", SAPbobsCOM.BoFieldTypes.db_Date)
            otblcrt.CreateUserFields("@AIS_RVL_OBSN", "Remark", "Remark", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
            otblcrt.CreateUserFields("@AIS_RVL_OBSN", "prefix", "prefix", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_RVL_OBSN", "from", "from", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_RVL_OBSN", "to", "to", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_RVL_OBSN", "sufix", "sufix", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_RVL_OBSN", "sufix", "sufix", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_RVL_OBSN", "SubGrnNo", "SubGrnNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_RVL_OBSN", "totqty", "totqty", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_RVL_OBSN", "lineid", "lineid", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)


        Catch ex As Exception
            objApplication.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub

    Public Sub BatchSerialDetails()
        Try
            Dim objUDFEngine As New Ananthi.SBOLib.UDFEngine(objCompany)
            ' Create table
            objUDFEngine.CreateTable("AIS_RVL_BSN1", "Bat/Ser Det", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            ' Create Field
            otblcrt.CreateUserFields("@AIS_RVL_BSN1", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_RVL_BSN1", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
            otblcrt.CreateUserFields("@AIS_RVL_BSN1", "TotQty", "Total qty", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_RVL_BSN1", "Qty", "qty", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_RVL_BSN1", "BatSerNo", "BatSerNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

            otblcrt.CreateUserFields("@AIS_RVL_BSN1", "Warehse", "Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_RVL_BSN1", "WhsNam", "Warehouse Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            otblcrt.CreateUserFields("@AIS_RVL_BSN1", "TotlQty", "Total Qty", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        Catch ex As Exception
            objApplication.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub

    Private Sub createUDO1(ByVal tblname As String, ByVal udocode As String, ByVal udoname As String, ByVal type As SAPbobsCOM.BoUDOObjType, Optional ByVal DfltForm As Boolean = False, Optional ByVal FindForm As Boolean = False)
        'objAddOn.objApplication.SetStatusBarMessage("UDO Created Please Wait..", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
        Dim creationPackage As SAPbouiCOM.FormCreationParams
        Dim objform As SAPbouiCOM.Form
        'Dim i As Integer
        Dim c_Yes As SAPbobsCOM.BoYesNoEnum = SAPbobsCOM.BoYesNoEnum.tYES
        Dim lRetCode As Long
        oUserObjectMD = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
        If Not oUserObjectMD.GetByKey(udocode) Then
            oUserObjectMD.Code = udocode
            oUserObjectMD.Name = udoname
            oUserObjectMD.ObjectType = type
            oUserObjectMD.TableName = tblname
            oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
            If DfltForm = True Then
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
                Select Case udoname

                    Case "UDO Name"
                        oUserObjectMD.FormColumns.FormColumnAlias = "Code"
                        oUserObjectMD.FormColumns.FormColumnDescription = "Code"
                        oUserObjectMD.FormColumns.Add()
                        oUserObjectMD.FormColumns.FormColumnAlias = "Name"
                        oUserObjectMD.FormColumns.FormColumnDescription = "Name"
                        oUserObjectMD.FormColumns.Add()
                        oUserObjectMD.FormColumns.FormColumnAlias = "U_Rate"
                        oUserObjectMD.FormColumns.FormColumnDescription = "RatePerDay"
                        oUserObjectMD.FormColumns.Add()

                End Select

            End If
            If FindForm = True Then
                If type = SAPbobsCOM.BoUDOObjType.boud_MasterData Then
                    oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES

                    Select Case udoname

                        Case "UDO Name"
                            oUserObjectMD.FindColumns.ColumnAlias = "Code"
                            oUserObjectMD.FindColumns.ColumnDescription = "Code"
                            oUserObjectMD.FindColumns.Add()
                            oUserObjectMD.FindColumns.ColumnAlias = "Name"
                            oUserObjectMD.FindColumns.ColumnDescription = "Name"
                            oUserObjectMD.FindColumns.Add()
                    End Select

                ElseIf type = SAPbobsCOM.BoUDOObjType.boud_Document Then
                    oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                    Select Case udoname
                    End Select
                End If
            End If
            'If childTable.Length > 0 Then
            '    For i = 0 To childTable.Length - 2
            '        If Trim(childTable(i)) <> "" Then
            '            oUserObjectMD.ChildTables.TableName = childTable(i)
            '            oUserObjectMD.ChildTables.Add()
            '        End If
            '    Next
            'End If
            lRetCode = oUserObjectMD.Add()
            If lRetCode <> 0 Then

                'MsgBox("error" + CStr(lRetCode))
                'MsgBox(objAddOn.objCompany.GetLastErrorDescription)
            Else

            End If
            If DfltForm = True Then
                creationPackage = objAddOn.objApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)
                ' Need to set the parameter with the object unique ID
                creationPackage.ObjectType = "1"
                creationPackage.UniqueID = udoname
                creationPackage.FormType = udoname
                creationPackage.BorderStyle = SAPbouiCOM.BoFormTypes.ft_Fixed
                objform = objAddOn.objApplication.Forms.AddEx(creationPackage)
            End If
        End If
        'objAddOn.objApplication.SetStatusBarMessage("UDO Created Successfully..", SAPbouiCOM.BoMessageTime.bmt_Short, False)
    End Sub

    Private Sub createUDO(ByVal tblname As String, ByVal udocode As String, ByVal udoname As String, ByVal childTable() As String, ByVal type As SAPbobsCOM.BoUDOObjType, Optional ByVal DfltForm As Boolean = False, Optional ByVal FindForm As Boolean = False)
        Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
        Dim creationPackage As SAPbouiCOM.FormCreationParams
        Dim objform As SAPbouiCOM.Form
        Dim i As Integer
        'Dim c_Yes As SAPbobsCOM.BoYesNoEnum = SAPbobsCOM.BoYesNoEnum.tYES
        Dim lRetCode As Long
        oUserObjectMD = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
        If Not oUserObjectMD.GetByKey(udoname) Then
            oUserObjectMD.Code = udocode
            oUserObjectMD.Name = udoname
            oUserObjectMD.ObjectType = type
            oUserObjectMD.TableName = tblname
            oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
            If DfltForm = True Then
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES

                oUserObjectMD.FormColumns.FormColumnAlias = "Code"
                oUserObjectMD.FormColumns.FormColumnDescription = "Code"
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "Name"
                oUserObjectMD.FormColumns.FormColumnDescription = "Name"
                oUserObjectMD.FormColumns.Add()
            End If
            If FindForm = True Then
                If type = SAPbobsCOM.BoUDOObjType.boud_MasterData Then
                    oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                    Select Case udoname

                        Case "UDO Name"
                            oUserObjectMD.FindColumns.ColumnAlias = "U_Code"
                            oUserObjectMD.FindColumns.ColumnDescription = "Code"
                            oUserObjectMD.FindColumns.Add()
                            oUserObjectMD.FindColumns.ColumnAlias = "U_Name"
                            oUserObjectMD.FindColumns.ColumnDescription = "Name"
                            oUserObjectMD.FindColumns.Add()

                    End Select
                Else
                    oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                    Select Case udoname

                        Case "UDO Name"
                            oUserObjectMD.FindColumns.ColumnAlias = "DocNum"
                            oUserObjectMD.FindColumns.ColumnDescription = "DocNum"
                            oUserObjectMD.FindColumns.Add()
                            oUserObjectMD.FindColumns.ColumnAlias = "U_workno"
                            oUserObjectMD.FindColumns.ColumnDescription = "Printing work order no"
                            oUserObjectMD.FindColumns.Add()
                            oUserObjectMD.FindColumns.ColumnAlias = "U_custcode"
                            oUserObjectMD.FindColumns.ColumnDescription = "Customer Code"
                            oUserObjectMD.FindColumns.Add()
                            oUserObjectMD.FindColumns.ColumnAlias = "U_custname"
                            oUserObjectMD.FindColumns.ColumnDescription = "Customer Name"
                            oUserObjectMD.FindColumns.Add()
                            oUserObjectMD.FindColumns.ColumnAlias = "U_date"
                            oUserObjectMD.FindColumns.ColumnDescription = "Date"
                            oUserObjectMD.FindColumns.Add()


                    End Select
                End If
            End If
            If childTable.Length > 0 Then
                For i = 0 To childTable.Length - 2
                    If Trim(childTable(i)) <> "" Then
                        oUserObjectMD.ChildTables.TableName = childTable(i)
                        oUserObjectMD.ChildTables.Add()
                    End If
                Next
            End If
            lRetCode = oUserObjectMD.Add()
            If lRetCode <> 0 Then
                'MsgBox("error" + CStr(lRetCode))
                'MsgBox(objAddOn.objCompany.GetLastErrorDescription)
            Else

            End If
            If DfltForm = True Then
                creationPackage = objAddOn.objApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)
                ' Need to set the parameter with the object unique ID
                creationPackage.ObjectType = "1"
                creationPackage.UniqueID = udoname
                creationPackage.FormType = udoname
                creationPackage.BorderStyle = SAPbouiCOM.BoFormTypes.ft_Fixed
                objform = objAddOn.objApplication.Forms.AddEx(creationPackage)
            End If
        End If

    End Sub

    'Private Sub objApplication_AppEvent(ByVal EventType As SAPbouiCOM.BoAppEventTypes) Handles objApplication.AppEvent
    '    If EventType = SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged Or EventType = SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition Or EventType = SAPbouiCOM.BoAppEventTypes.aet_ShutDown Then
    '        Try
    '            ' objUIXml.LoadMenuXML("RemoveMenu.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded)
    '            If objCompany.Connected Then objCompany.Disconnect()
    '            objCompany = Nothing
    '            objApplication = Nothing
    '            System.Runtime.InteropServices.Marshal.ReleaseComObject(objCompany)
    '            System.Runtime.InteropServices.Marshal.ReleaseComObject(objApplication)
    '            GC.Collect()
    '        Catch ex As Exception
    '        End Try
    '        End
    '    End If
    'End Sub

    Private Sub applyFilter()
        Dim oFilters As SAPbouiCOM.EventFilters
        Dim oFilter As SAPbouiCOM.EventFilter
        oFilters = New SAPbouiCOM.EventFilters
        'Item Master Data 
        oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)

        oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_MENU_CLICK)




        oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)

        oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK)


    End Sub




#Region "Common Fuctions"

    Function isDuplicate(ByVal oEditText As SAPbouiCOM.EditText, ByVal strTableName As String, ByVal strFildName As String, ByVal strMessage As String) As Boolean
        Try
            Dim rsetPayMethod As SAPbobsCOM.Recordset = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim blReturnVal As Boolean = False
            Dim strQuery As String
            '  If oEditText.Value.Equals("") Then

            ' ObjApplication.StatusBar.SetText(strMessage & " : Should Not Be left Empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ' Return False
            '  End If

            strQuery = "SELECT * FROM " & strTableName & " WHERE UPPER(" & strFildName & ")=UPPER('" & oEditText.Value & "')"
            rsetPayMethod.DoQuery(strQuery)

            If rsetPayMethod.RecordCount > 0 Then
                oEditText.Active = True
                objApplication.StatusBar.SetText(strMessage & " [ " & oEditText.Value & " ] : Already Exist in Table...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                Return False
            End If
            Return True

        Catch ex As Exception
            objApplication.StatusBar.SetText(" isDuplicate Function Failed : ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End Try
    End Function
    Sub Sub_SetNewLine(ByVal oMatrix As SAPbouiCOM.Matrix, ByVal oDBDSDetail As SAPbouiCOM.DBDataSource, ByVal Parent1 As Integer, Optional ByVal Parent2 As Integer = 0, Optional ByVal RowID As Integer = 1, Optional ByVal ColumnUID As String = "")
        Try
            If ColumnUID.Equals("") = False Then
                If oMatrix.VisualRowCount > 0 Then
                    If oMatrix.Columns.Item(ColumnUID).Cells.Item(RowID).Specific.Value.Equals("") = False And RowID = oMatrix.VisualRowCount Then
                        oMatrix.FlushToDataSource()
                        oMatrix.AddRow()
                        oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                        oDBDSDetail.Offset = oMatrix.VisualRowCount - 1
                        oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, oMatrix.VisualRowCount)
                        oDBDSDetail.SetValue("U_Parent1", oDBDSDetail.Offset, Parent1)
                        If Parent2 <> 0 Then oDBDSDetail.SetValue("U_Parent2", oDBDSDetail.Offset, Parent2)
                        oMatrix.SetLineData(oMatrix.VisualRowCount)
                        oMatrix.FlushToDataSource()
                    End If
                Else
                    oMatrix.FlushToDataSource()
                    oMatrix.AddRow()
                    oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                    oDBDSDetail.Offset = oMatrix.VisualRowCount - 1
                    oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, oMatrix.VisualRowCount)
                    oDBDSDetail.SetValue("U_Parent1", oDBDSDetail.Offset, Parent1)
                    If Parent2 <> 0 Then oDBDSDetail.SetValue("U_Parent2", oDBDSDetail.Offset, Parent2)
                    oMatrix.SetLineData(oMatrix.VisualRowCount)
                    oMatrix.FlushToDataSource()
                End If

            Else
                oMatrix.FlushToDataSource()
                oMatrix.AddRow()
                oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                oDBDSDetail.Offset = oMatrix.VisualRowCount - 1
                oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, oMatrix.VisualRowCount)
                oDBDSDetail.SetValue("U_Parent1", oDBDSDetail.Offset, Parent1)
                If Parent2 <> 0 Then oDBDSDetail.SetValue("U_Parent2", oDBDSDetail.Offset, Parent2)
                oMatrix.SetLineData(oMatrix.VisualRowCount)
                oMatrix.FlushToDataSource()
            End If
            For i As Integer = 0 To oMatrix.VisualRowCount - 1
                oDBDSDetail.SetValue("LineId", i, i + 1)
            Next
            oMatrix.LoadFromDataSource()
        Catch ex As Exception
            ' ExceptionHandler(ex)
        Finally
        End Try
    End Sub
    Function getSingleValue(ByVal strSQL As String) As String
        Try
            Dim rset As SAPbobsCOM.Recordset = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim strReturnVal As String = ""
            rset.DoQuery(strSQL)
            Return IIf(rset.RecordCount > 0, rset.Fields.Item(0).Value.ToString(), "")
        Catch ex As Exception
            objApplication.StatusBar.SetText(" Get Single Value Function Failed :  " & ex.Message + strSQL, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return ""
        End Try
    End Function

    Function LoadComboBoxSeries(ByVal oComboBox As SAPbouiCOM.ComboBox, ByVal UDOID As String) As Boolean
        Try
            oComboBox.ValidValues.LoadSeries(UDOID, SAPbouiCOM.BoSeriesMode.sf_Add)
            oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)
        Catch ex As Exception
            objApplication.StatusBar.SetText("LoadComboBoxSeries Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        Finally
        End Try

    End Function

    Sub setRightMenu(ByVal strMenuUID As String, ByVal strMenuName As String)
        Try
            'Dim MenuItem As SAPbouiCOM.MenuItem = objAddOn.objApplication.Menus.Item("1280") 'Data'
            Dim MenuItem As SAPbouiCOM.MenuItem = objAddOn.objApplication.Menus.Item("1280") 'Data'
            Dim Menu As SAPbouiCOM.Menus = MenuItem.SubMenus
            Dim MenuParam As SAPbouiCOM.MenuCreationParams = objAddOn.objApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)

            MenuParam.Type = SAPbouiCOM.BoMenuType.mt_STRING
            MenuParam.UniqueID = strMenuUID
            MenuParam.String = strMenuName
            MenuParam.Enabled = True
            If MenuItem.SubMenus.Exists(strMenuUID) = False Then Menu.AddEx(MenuParam)

        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("SubMenuAddEx Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub




    Sub SetComboBoxValueRefresh(ByVal oComboBox As SAPbouiCOM.ComboBox, ByVal strQry As String)
        Try
            Dim rsetValidValue As SAPbobsCOM.Recordset = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim intCount As Integer = oComboBox.ValidValues.Count
            ' Remove the Combo Box Value Based On Count ...
            If intCount > 0 Then
                While intCount > 0
                    oComboBox.ValidValues.Remove(intCount - 1, SAPbouiCOM.BoSearchKey.psk_Index)
                    intCount = intCount - 1
                End While
            End If

            rsetValidValue.DoQuery(strQry)
            rsetValidValue.MoveFirst()
            For j As Integer = 0 To rsetValidValue.RecordCount - 1
                oComboBox.ValidValues.Add(rsetValidValue.Fields.Item(0).Value, rsetValidValue.Fields.Item(1).Value)
                rsetValidValue.MoveNext()
            Next

        Catch ex As Exception
            Msg("SetComboBoxValueRefresh Method Faild : " & ex.Message)
        Finally
        End Try
    End Sub

    Function setComboBoxValue(ByVal oComboBox As SAPbouiCOM.ComboBox, ByVal strQry As String) As Boolean
        Try
            If oComboBox.ValidValues.Count = 0 Then
                Dim rsetValidValue As SAPbobsCOM.Recordset = DoQuery(strQry)
                rsetValidValue.MoveFirst()
                For j As Integer = 0 To rsetValidValue.RecordCount - 1
                    oComboBox.ValidValues.Add(rsetValidValue.Fields.Item(0).Value, rsetValidValue.Fields.Item(1).Value)
                    rsetValidValue.MoveNext()
                Next
            End If
            ' If oComboBox.ValidValues.Count > 0 Then oComboBox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)
        Catch ex As Exception
            objApplication.StatusBar.SetText("setComboBoxValue Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return True
        Finally
        End Try

    End Function

    Sub setDocNum(ByVal ofrm As SAPbouiCOM.Form)
        Try
            Dim strSerialCode As String = ofrm.Items.Item("c_Series").Specific.Selected.Value
            Dim strDocNum As Long = ofrm.BusinessObject.GetNextSerialNumber(strSerialCode, ofrm.UDFFormUID)
            ofrm.DataSources.DBDataSources.Item(0).SetValue("DocNum", 0, strDocNum)
        Catch ex As Exception
            objApplication.StatusBar.SetText("Set DocNum Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function LoadDocumentDate(ByVal oEditText As SAPbouiCOM.EditText) As Boolean
        Try
            oEditText.Active = True
            oEditText.String = "A"
        Catch ex As Exception
            objApplication.StatusBar.SetText("LoadDocumentDate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        Finally
        End Try

    End Function

    'Function setLocationCombo(ByVal oComboBox As SAPbouiCOM.ComboBox) As Boolean
    '    Try
    '        setComboBoxValue(oComboBox, "Select Code, Location from OLCT Where Code in ( Select U_LocCode from [@AIS_WIPGT1] where isnull(U_Active,'N') ='Y' )")

    '    Catch ex As Exception
    '        objApplication.StatusBar.SetText("setLocationCombo Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
    '        Return True
    '    Finally
    '    End Try

    'End Function

    Function DoQuery(ByVal strSql As String) As SAPbobsCOM.Recordset
        Try
            Dim rsetCode As SAPbobsCOM.Recordset = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            rsetCode.DoQuery(strSql)
            Return rsetCode
        Catch ex As Exception
            objApplication.StatusBar.SetText("Execute Query Function Failed:" & ex.Message + strSql, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return Nothing
        Finally
        End Try
    End Function

    Sub SetNewLine(ByVal oMatrix As SAPbouiCOM.Matrix, ByVal oDBDSDetail As SAPbouiCOM.DBDataSource, Optional ByVal RowID As Integer = 1, Optional ByVal ColumnUID As String = "")
        Try
            If ColumnUID.Equals("") = False Then
                If oMatrix.VisualRowCount > 0 Then
                    If oMatrix.Columns.Item(ColumnUID).Cells.Item(RowID).Specific.Value.Equals("") = False And RowID = oMatrix.VisualRowCount Then
                        oMatrix.FlushToDataSource()
                        oMatrix.AddRow()
                        oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                        oDBDSDetail.Offset = oMatrix.VisualRowCount - 1
                        oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, oMatrix.VisualRowCount)

                        oMatrix.SetLineData(oMatrix.VisualRowCount)
                        oMatrix.FlushToDataSource()
                    End If
                Else
                    oMatrix.FlushToDataSource()
                    oMatrix.AddRow()
                    oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                    oDBDSDetail.Offset = oMatrix.VisualRowCount - 1
                    oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, oMatrix.VisualRowCount)

                    oMatrix.SetLineData(oMatrix.VisualRowCount)
                    oMatrix.FlushToDataSource()
                End If

            Else
                oMatrix.FlushToDataSource()
                oMatrix.AddRow()
                oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                oDBDSDetail.Offset = oMatrix.VisualRowCount - 1
                oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, oMatrix.VisualRowCount)

                oMatrix.SetLineData(oMatrix.VisualRowCount)
                oMatrix.FlushToDataSource()
            End If
        Catch ex As Exception
            objApplication.StatusBar.SetText("SetNewLine Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub DeleteRow(ByVal oMatrix As SAPbouiCOM.Matrix, ByVal oDBDSDetail As SAPbouiCOM.DBDataSource)
        Try
            oMatrix.FlushToDataSource()

            For i As Integer = 1 To oMatrix.VisualRowCount
                oMatrix.GetLineData(i)
                oDBDSDetail.Offset = i - 1
                oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, i)
                oMatrix.SetLineData(i)
                oMatrix.FlushToDataSource()
            Next
            'oDBDSDetail.RemoveRecord(oDBDSDetail.Size - 1)
            'oMatrix.LoadFromDataSource()

        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("DeleteRow  Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub Msg(ByVal strMsg As String, Optional ByVal msgTime As String = "S", Optional ByVal errType As String = "W")
        Dim time As SAPbouiCOM.BoMessageTime
        Dim msgType As SAPbouiCOM.BoStatusBarMessageType
        Select Case errType.ToUpper()
            Case "E"
                msgType = SAPbouiCOM.BoStatusBarMessageType.smt_Error
            Case "W"
                msgType = SAPbouiCOM.BoStatusBarMessageType.smt_Warning
            Case "N"
                msgType = SAPbouiCOM.BoStatusBarMessageType.smt_None
            Case "S"
                msgType = SAPbouiCOM.BoStatusBarMessageType.smt_Success
            Case Else
                msgType = SAPbouiCOM.BoStatusBarMessageType.smt_Warning
        End Select
        Select Case msgTime.ToUpper()
            Case "M"
                time = SAPbouiCOM.BoMessageTime.bmt_Medium
            Case "S"
                time = SAPbouiCOM.BoMessageTime.bmt_Short
            Case "L"
                time = SAPbouiCOM.BoMessageTime.bmt_Long
            Case Else
                time = SAPbouiCOM.BoMessageTime.bmt_Medium
        End Select
        objAddOn.objApplication.StatusBar.SetText(strMsg, time, msgType)
    End Sub

    Sub AddXML(ByVal pathstr As String)
        Try
            Dim xmldoc As New Xml.XmlDocument

            'Dim stackTrace As New Diagnostics.StackFrame(0)
            'Dim ss = stackTrace.GetMethod.Name

            Dim asm As Assembly = Assembly.GetExecutingAssembly()
            Dim location As String = asm.FullName
            Dim appName As String = System.IO.Path.GetDirectoryName(location)
            Dim stream As System.IO.Stream

            Try
                stream = System.Reflection.Assembly.GetCallingAssembly().GetManifestResourceStream(System.Reflection.Assembly.GetCallingAssembly.GetName().Name + "." + pathstr)
                Dim tempstreamreader As New System.IO.StreamReader(stream, True)
            Catch ex As Exception
                stream = System.Reflection.Assembly.GetEntryAssembly().GetManifestResourceStream(System.Reflection.Assembly.GetEntryAssembly.GetName().Name + "." + pathstr)
            End Try

            Dim streamreader As New System.IO.StreamReader(stream, True)
            xmldoc.LoadXml(streamreader.ReadToEnd())
            streamreader.Close()
            objApplication.LoadBatchActions(xmldoc.InnerXml)
        Catch ex As Exception
            objApplication.StatusBar.SetText("AddXML Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub DeleteEmptyRowInFormDataEvent(ByVal oMatrix As SAPbouiCOM.Matrix, ByVal ColumnUID As String, ByVal oDBDSDetail As SAPbouiCOM.DBDataSource)
        Try
            If oMatrix.VisualRowCount > 0 Then
                If oMatrix.Columns.Item(ColumnUID).Cells.Item(oMatrix.VisualRowCount).Specific.Value.Equals("") Then
                    oMatrix.DeleteRow(oMatrix.VisualRowCount)
                    oDBDSDetail.RemoveRecord(oDBDSDetail.Size - 1)
                    oMatrix.FlushToDataSource()
                End If
            End If
        Catch ex As Exception
            objApplication.StatusBar.SetText("Delete Empty RowIn Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Public Function UDOExists(ByVal code As String) As Boolean
        GC.Collect()
        Dim v_UDOMD As SAPbobsCOM.UserObjectsMD
        Dim v_ReturnCode As Boolean
        v_UDOMD = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
        v_ReturnCode = v_UDOMD.GetByKey(code)
        System.Runtime.InteropServices.Marshal.ReleaseComObject(v_UDOMD)
        v_UDOMD = Nothing
        Return v_ReturnCode
    End Function

    Function GetCodeGeneration(ByVal TableName As String) As Integer
        Try
            Dim rsetCode As SAPbobsCOM.Recordset = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim strCode As String = "Select ISNULL(Max(ISNULL(DocEntry,0)),0) + 1 Code From " & Trim(TableName) & ""
            rsetCode.DoQuery(strCode)
            Return CInt(rsetCode.Fields.Item("Code").Value)
        Catch ex As Exception
            objApplication.StatusBar.SetText("GetCodeGeneration Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return True
        Finally
        End Try
    End Function

    Sub RemoveRightMenu(ByVal strMenuID As String)
        Try
            If objApplication.Menus.Item("1280").SubMenus.Exists(strMenuID) Then objApplication.Menus.Item("1280").SubMenus.RemoveEx(strMenuID)

        Catch ex As Exception
            objApplication.StatusBar.SetText("SubMenusRemoveEx Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub ChooseFromListFilteration(ByVal oForm As SAPbouiCOM.Form, ByVal strCFL_ID As String, ByVal strCFL_Alies As String, ByVal strQuery As String)
        Try
            Dim oCFL As SAPbouiCOM.ChooseFromList = oForm.ChooseFromLists.Item(strCFL_ID)
            Dim oConds As SAPbouiCOM.Conditions
            Dim oCond As SAPbouiCOM.Condition
            Dim oEmptyConds As New SAPbouiCOM.Conditions
            Dim rsetCFL As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oCFL.SetConditions(oEmptyConds)
            oConds = oCFL.GetConditions()

            rsetCFL.DoQuery(strQuery)
            rsetCFL.MoveFirst()
            For i As Integer = 1 To rsetCFL.RecordCount
                If i = (rsetCFL.RecordCount) Then
                    oCond = oConds.Add()
                    oCond.Alias = strCFL_Alies
                    oCond.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                    oCond.CondVal = Trim(rsetCFL.Fields.Item(0).Value)
                Else
                    oCond = oConds.Add()
                    oCond.Alias = strCFL_Alies
                    oCond.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                    oCond.CondVal = Trim(rsetCFL.Fields.Item(0).Value)
                    oCond.Relationship = SAPbouiCOM.BoConditionRelationship.cr_OR
                End If
                rsetCFL.MoveNext()
            Next
            If rsetCFL.RecordCount = 0 Then
                oCond = oConds.Add()
                oCond.Alias = strCFL_Alies
                oCond.Relationship = SAPbouiCOM.BoConditionRelationship.cr_NONE
                oCond.CondVal = "-1"
            End If
            oCFL.SetConditions(oConds)
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Choose FromList Filter Global Fun. Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function RegisterUDO(ByVal UDOCode As String, ByVal UDOName As String, ByVal UDOType As SAPbobsCOM.BoUDOObjType, ByVal FindField As String(,), ByVal UDOHTableName As String, Optional ByVal UDODTableName As String = "", Optional ByVal ChildTable As String = "", Optional ByVal ChildTable1 As String = "", _
    Optional ByVal ChildTable2 As String = "", Optional ByVal ChildTable3 As String = "", Optional ByVal ChildTable4 As String = "", Optional ByVal ChildTable5 As String = "", _
    Optional ByVal ChildTable6 As String = "", Optional ByVal ChildTable7 As String = "", Optional ByVal ChildTable8 As String = "", Optional ByVal ChildTable9 As String = "", _
     Optional ByVal ChildTable10 As String = "", Optional ByVal ChildTable11 As String = "", Optional ByVal LogOption As SAPbobsCOM.BoYesNoEnum = SAPbobsCOM.BoYesNoEnum.tNO) As Boolean
        Dim ActionSuccess As Boolean = False
        Try
            RegisterUDO = False
            Dim v_udoMD As SAPbobsCOM.UserObjectsMD
            v_udoMD = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
            v_udoMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.CanLog = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.Code = UDOCode
            v_udoMD.Name = UDOName
            v_udoMD.TableName = UDOHTableName

            If UDODTableName <> "" Then
                v_udoMD.ChildTables.TableName = UDODTableName
                v_udoMD.ChildTables.Add()
            End If

            If ChildTable <> "" Then
                v_udoMD.ChildTables.TableName = ChildTable
                v_udoMD.ChildTables.Add()
            End If
            If ChildTable1 <> "" Then
                v_udoMD.ChildTables.TableName = ChildTable1
                v_udoMD.ChildTables.Add()
            End If
            If ChildTable2 <> "" Then
                v_udoMD.ChildTables.TableName = ChildTable2
                v_udoMD.ChildTables.Add()
            End If
            If ChildTable3 <> "" Then
                v_udoMD.ChildTables.TableName = ChildTable3
                v_udoMD.ChildTables.Add()
            End If
            If ChildTable4 <> "" Then
                v_udoMD.ChildTables.TableName = ChildTable4
                v_udoMD.ChildTables.Add()
            End If
            If ChildTable5 <> "" Then
                v_udoMD.ChildTables.TableName = ChildTable5
                v_udoMD.ChildTables.Add()
            End If
            If ChildTable6 <> "" Then
                v_udoMD.ChildTables.TableName = ChildTable6
                v_udoMD.ChildTables.Add()
            End If
            If ChildTable7 <> "" Then
                v_udoMD.ChildTables.TableName = ChildTable7
                v_udoMD.ChildTables.Add()
            End If
            If ChildTable8 <> "" Then
                v_udoMD.ChildTables.TableName = ChildTable8
                v_udoMD.ChildTables.Add()
            End If
            If ChildTable9 <> "" Then
                v_udoMD.ChildTables.TableName = ChildTable9
                v_udoMD.ChildTables.Add()
            End If
            If ChildTable10 <> "" Then
                v_udoMD.ChildTables.TableName = ChildTable10
                v_udoMD.ChildTables.Add()
            End If
            If ChildTable11 <> "" Then
                v_udoMD.ChildTables.TableName = ChildTable11
                v_udoMD.ChildTables.Add()
            End If

            If LogOption = SAPbobsCOM.BoYesNoEnum.tYES Then
                v_udoMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES
                v_udoMD.LogTableName = "A" & UDOHTableName
            End If
            v_udoMD.ObjectType = UDOType
            For i As Int16 = 0 To FindField.GetLength(0) - 1
                If i > 0 Then v_udoMD.FindColumns.Add()
                v_udoMD.FindColumns.ColumnAlias = FindField(i, 0)
                v_udoMD.FindColumns.ColumnDescription = FindField(i, 1)
            Next

            If v_udoMD.Add() = 0 Then
                RegisterUDO = True
                If objCompany.InTransaction Then objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                objApplication.StatusBar.SetText("Successfully Registered UDO >" & UDOCode & ">" & UDOName & " >" & objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
            Else
                objApplication.StatusBar.SetText("Failed to Register UDO >" & UDOCode & ">" & UDOName & " >" & objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                MessageBox.Show(objCompany.GetLastErrorDescription)
                RegisterUDO = False
            End If
            System.Runtime.InteropServices.Marshal.ReleaseComObject(v_udoMD)
            v_udoMD = Nothing
            GC.Collect()
            If ActionSuccess = False And objCompany.InTransaction Then objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
        Catch ex As Exception
            If objCompany.InTransaction Then objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
        End Try
    End Function
#End Region

#Region "Function "
    Sub setEdittextCFL(ByVal oForm As SAPbouiCOM.Form, ByVal UId As String, ByVal strCFL_ID As String, ByVal strCFL_Obj As String, ByVal strCFL_Alies As String)
        Try

            Dim oCFL As SAPbouiCOM.ChooseFromListCreationParams
            oCFL = objApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)
            oCFL.UniqueID = strCFL_ID
            oCFL.ObjectType = strCFL_Obj
            oForm.ChooseFromLists.Add(oCFL)

            Dim txt As SAPbouiCOM.EditText = oForm.Items.Item(UId).Specific
            txt.ChooseFromListUID = strCFL_ID
            txt.ChooseFromListAlias = strCFL_Alies

        Catch ex As Exception
            objApplication.StatusBar.SetText("Set EditText CFL Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    ' Convert Hours
    Function ConvertHrs(ByVal Value As String, ByVal TimeMesurement As String) As Double
        Try

            Value = IIf(Value.ToString.Trim = "", 0, Value)
            Dim ValueTimeHr As Double = 0
            Select Case TimeMesurement
                Case "S"
                    ValueTimeHr = Value / 3600
                Case "M"
                    ValueTimeHr = Value / 60
                Case "H"
                    ValueTimeHr = Value
                Case "D"
                    ValueTimeHr = Value * 24
            End Select

            Return ValueTimeHr

        Catch ex As Exception
            objApplication.StatusBar.SetText("Convert Hours Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Function

    Function FindMinCode(ByVal TableName As String, ByVal FieldName As String, ByVal KeyField As String, ByVal KeyValue As String) As String
        Try
            Dim strsql = " select " & FieldName & " from " & _
                        " ( select ROW_NUMBER() over(Order by " & FieldName & ") ROWNUMBER , " & FieldName & " from " & TableName & " where isnull(convert(varchar," & KeyField & "),'N') ='" & KeyValue & "' " & _
                        " group by " & FieldName & ",convert(varchar," & KeyField & ") ) AA  where ROWNUMBER = (Select Min(ROWNUMBER) from ( select ROW_NUMBER() " & _
                        " over(Order by " & FieldName & ") ROWNUMBER , " & FieldName & " from " & TableName & " where isnull(convert(varchar," & KeyField & "),'N') ='" & KeyValue & "' " & _
                        " group by " & FieldName & ",convert(varchar," & KeyField & ") ) TMP1) "
            Dim returnValue = getSingleValue(strsql)
            Return returnValue
        Catch ex As Exception
            objApplication.StatusBar.SetText("Find Min Code Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Function

    Function FindMaxCode(ByVal TableName As String, ByVal FieldName As String, ByVal KeyField As String, ByVal KeyValue As String) As String
        Try
            Dim strsql = " select " & FieldName & " from " & _
                        " ( select ROW_NUMBER() over(Order by " & FieldName & ") ROWNUMBER , " & FieldName & " from " & TableName & " where isnull(convert(varchar," & KeyField & "),'N') ='" & KeyValue & "' " & _
                        " group by " & FieldName & ",convert(varchar," & KeyField & ") ) AA  where ROWNUMBER = (Select Max(ROWNUMBER) from ( select ROW_NUMBER() " & _
                        " over(Order by " & FieldName & ") ROWNUMBER , " & FieldName & " from " & TableName & " where isnull(convert(varchar," & KeyField & "),'N') ='" & KeyValue & "' " & _
                        " group by " & FieldName & ",convert(varchar," & KeyField & ") ) TMP1) "
            Dim returnValue = getSingleValue(strsql)
            Return returnValue
        Catch ex As Exception
            objApplication.StatusBar.SetText("Find Next Code Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Function

    Function FindNextCode(ByVal TableName As String, ByVal FieldName As String, ByVal KeyField As String, ByVal KeyValue As String, ByVal CurrentValue As String) As String
        Try
            If CurrentValue.Trim = "" Then
                Return FindMinCode(TableName, FieldName, KeyField, KeyValue)
            Else

                'Find the Current Row
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                Dim strsql = " select convert(int,ROWNUMBER) from ( select ROW_NUMBER() over(Order by " & _
                   FieldName & ") ROWNUMBER , " & FieldName & " from " & TableName & " where isnull(convert(varchar," & KeyField & "),'N') ='" & KeyValue & "' " & _
                   " group by " & FieldName & ",convert(varchar," & KeyField & ") ) AA  where " & FieldName & " = '" & CurrentValue & "' "

                Dim CurrentRow = getSingleValue(strsql)
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                'Max Row Number
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                strsql = " select Max(convert(int,ROWNUMBER)) from ( select ROW_NUMBER() over(Order by " & _
                   FieldName & ") ROWNUMBER , " & FieldName & " from " & TableName & " where isnull(convert(varchar," & KeyField & "),'N') ='" & KeyValue & "' " & _
                   " group by " & FieldName & ",convert(varchar," & KeyField & ") ) AA  "

                Dim MaxRow = getSingleValue(strsql)
                MaxRow = IIf(MaxRow.Trim = "", 0, MaxRow)
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                Dim NextRow As Integer
                CurrentRow = IIf(CurrentRow.Trim = "", 0, CurrentRow)
                If CInt(CurrentRow) = 0 Or CInt(CurrentRow) = CInt(MaxRow) Then
                    Return ""
                Else
                    NextRow = CInt(CurrentRow) + 1
                End If

                strsql = " select " & FieldName & " from ( select ROW_NUMBER() over(Order by " & _
                   FieldName & ") ROWNUMBER , " & FieldName & " from " & TableName & " where isnull(convert(varchar," & KeyField & "),'N') ='" & KeyValue & "' " & _
                   " group by " & FieldName & ",convert(varchar," & KeyField & ") ) AA  where  ROWNUMBER = '" & NextRow & "' "

                Dim returnValue As String = getSingleValue(strsql)
                Return returnValue

            End If
        Catch ex As Exception
            objApplication.StatusBar.SetText("Find Max Code Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Function

    Function FindPreviousCode(ByVal TableName As String, ByVal FieldName As String, ByVal KeyField As String, ByVal KeyValue As String, ByVal CurrentValue As String) As String
        Try
            If CurrentValue.Trim = "" Then
                Return FindMaxCode(TableName, FieldName, KeyField, KeyValue)
            Else

                'Find the Current Row
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                Dim strsql = "select convert(int, ROWNUMBER) from ( select ROW_NUMBER() over(Order by " & _
                   FieldName & ") ROWNUMBER , " & FieldName & " from " & TableName & " where isnull(convert(varchar," & KeyField & "),'N') ='" & KeyValue & "' " & _
                   " group by " & FieldName & ",convert(varchar," & KeyField & ") ) AA  where " & FieldName & " = '" & CurrentValue & "' "

                Dim CurrentRow = getSingleValue(strsql)
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                'Min Row Number
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                strsql = " select Min(convert(int,ROWNUMBER)) from ( select ROW_NUMBER() over(Order by " & _
                   FieldName & ") ROWNUMBER , " & FieldName & " from " & TableName & " where isnull(convert(varchar," & KeyField & "),'N') ='" & KeyValue & "' " & _
                   " group by " & FieldName & ",convert(varchar," & KeyField & ") ) AA  "

                Dim MinRow = getSingleValue(strsql)
                MinRow = IIf(MinRow.Trim = "", 0, MinRow)
                '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                Dim NextRow As Integer
                CurrentRow = IIf(CurrentRow.Trim = "", 0, CurrentRow)
                If CInt(CurrentRow) = 0 Or CInt(CurrentRow) = CInt(MinRow) Then
                    Return ""
                Else
                    NextRow = CInt(CurrentRow) - 1
                End If

                strsql = " select " & FieldName & " from ( select ROW_NUMBER() over(Order by " & _
                   FieldName & ") ROWNUMBER , " & FieldName & " from " & TableName & " where isnull(convert(varchar," & KeyField & "),'N') ='" & KeyValue & "' " & _
                   " group by " & FieldName & ",convert(varchar," & KeyField & ") ) AA  where  ROWNUMBER = '" & NextRow & "' "

                Dim returnValue As String = getSingleValue(strsql)
                Return returnValue

            End If
        Catch ex As Exception
            objApplication.StatusBar.SetText("Find Pervious Code Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Function


#End Region

#Region "Date format"
    Public Function DateConvertion(ByVal date1 As String)
        Dim date2 As String = ""
        Try
            date2 = date1.Substring(0, 4) & "-" & date1.Substring(4, 2) & "-" & date1.Substring(6, 2)
        Catch ex As Exception
            Return date2
        End Try
        Return date2
    End Function
#End Region

    Private Sub objApplication_UDOEvent(ByRef udoEventArgs As SAPbouiCOM.UDOEvent, ByRef BubbleEvent As Boolean) Handles objApplication.UDOEvent

    End Sub

    Private Sub oApplication_AppEvent(ByVal EventType As SAPbouiCOM.BoAppEventTypes) Handles objApplication.AppEvent

        Try

            If EventType = SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged Or EventType = SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged Or EventType = SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition Or EventType = SAPbouiCOM.BoAppEventTypes.aet_ShutDown Then

                System.Windows.Forms.Application.Exit()

            End If

        Catch ex As Exception

            objApplication.SetStatusBarMessage("In Menu : oApplication_AppEvent : " & ex.Message)

        End Try

    End Sub
End Class


