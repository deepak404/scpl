﻿Public Class MachineDetail
    Dim oDBDSHeader, oDBDSLine, ODBDSLine2, ODBDSource3, ODBDSource4 As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim FrmMachineDetails As SAPbouiCOM.Form
    Dim oDataTable As SAPbouiCOM.DataTable
    Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
    Dim DocEntry, Str As String
    Dim Ors As SAPbobsCOM.Recordset
    Dim current As Integer
    Dim oCombo, oCombo1 As SAPbouiCOM.ComboBox
    Sub LoadScreen()
        Try
            FrmMachineDetails = objAddOn.objUIXml.LoadScreenXML("MachineDetail.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, "AIS_OMCD")
            oDBDSHeader = FrmMachineDetails.DataSources.DBDataSources.Item("@AIS_OMCD")
            oDBDSLine = FrmMachineDetails.DataSources.DBDataSources.Item("@AIS_MCD1")
            oMatrix = FrmMachineDetails.Items.Item("Matrix").Specific
            InitForm()

            DefineModeForFields()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("LoadScreen " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Sub DefineModeForFields()
        Try


            FrmMachineDetails.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)


        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Public Sub InitForm()
        Try
            objAddOn.LoadComboBoxSeries(FrmMachineDetails.Items.Item("c_series").Specific, "AIS_OMCD")
            objAddOn.LoadDocumentDate(FrmMachineDetails.Items.Item("t_DocDate").Specific)
            oMatrix.AddRow()
            oMatrix.ClearRowData(oMatrix.VisualRowCount)
            oMatrix.Columns.Item("#").Cells.Item(oMatrix.VisualRowCount).Specific.value() = oMatrix.VisualRowCount

        Catch ex As Exception

        Finally
        End Try
    End Sub
    Sub Add_Mode()
        Try
 

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Form Add_Mode " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
 
    Sub LoadCombo()




    End Sub
    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType

                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If FrmMachineDetails.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = FrmMachineDetails.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Value.ToString.Trim
                                    Dim strDocNum As Long = FrmMachineDetails.BusinessObject.GetNextSerialNumber(strSerialCode, "AIS_OMCD")
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                        End Select
                    Catch ex As Exception


                    End Try


                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    If pVal.ItemUID = "Matrix" And pVal.ColUID = "Parameter" Then
                        oMatrix = FrmMachineDetails.Items.Item("Matrix").Specific
                        If oMatrix.Columns.Item("Parameter").Cells.Item(oMatrix.VisualRowCount).Specific.value <> "" Then
                            oMatrix.AddRow()
                            oMatrix.Columns.Item("#").Cells.Item(oMatrix.VisualRowCount).Specific.string = oMatrix.VisualRowCount
                            oMatrix.ClearRowData(oMatrix.VisualRowCount)
                        End If
                    End If

                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

                        If pVal.BeforeAction = False And Not oDataTable Is Nothing Then

                            Select Case oCFLE.ChooseFromListUID
                                Case "MAC_CFL"
                                    If pVal.BeforeAction = False Then
                                        oDBDSHeader.SetValue("U_MCCode", 0, Trim(oDataTable.GetValue("ResCode", 0)))
                                    End If
                            End Select
                        End If
                    Catch ex As Exception

                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "Matrix"
                                Select Case pVal.ColUID
                                    Case "MC"
                                        If pVal.BeforeAction = False Then
                                            objAddOn.ChooseFromListFilteration(FrmMachineDetails, "CFL_RES", "ResCode", "Select  ResCode  From ORSC Where ResType ='M'")
                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.BeforeAction And (FrmMachineDetails.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or _
                                                          FrmMachineDetails.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then

                                    If ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Return
                                    Else


                                    End If

                                End If

                        End Select
                    Catch ex As Exception

                        If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        If pVal.ItemUID = "1" Then
                            If pVal.BeforeAction = False And pVal.ActionSuccess Then
                                Me.InitForm()


                            End If
                        End If





                    Catch ex As Exception

                    End Try




            End Select
        Catch ex As Exception

        End Try

    End Sub

    




    Public Function ValidateAll() As Boolean
        Try
            Dim sumofqty As Double = 0



            Return True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText(("Validate Function Failed:" & ex.Message), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End Try
    End Function


    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        If BusinessObjectInfo.BeforeAction = False Then
                        End If
                    Catch ex As Exception
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    If Me.ValidateAll() = False Then
                        BubbleEvent = False
                        System.Media.SystemSounds.Asterisk.Play()
                        Return
                    Else

                    End If

            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub Menu_event(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    InitForm()

                Case "1281"

            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText(("Menu Event Failed:" & ex.Message), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
 



    End Sub
End Class
