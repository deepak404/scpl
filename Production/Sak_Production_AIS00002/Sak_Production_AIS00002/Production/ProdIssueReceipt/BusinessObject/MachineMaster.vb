﻿Public Class MachineMaster
    Public frmMachineMaster As SAPbouiCOM.Form
    Dim oDBDSHeader As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Public MachineMasterFormId As String = "0"
    Dim Machine As String
    Public oItem, oNewItem As SAPbouiCOM.Item
    Public partdet As New clsPartsDetails
    Sub LoadForm()
        Try
            objaddon.objapplication.Menus.Item("3073").Activate()
            frmMachineMaster = objaddon.objapplication.Forms.ActiveForm
            MachineUniqueId = frmMachineMaster.UniqueID

            'Assign Data Source
            oDBDSHeader = frmMachineMaster.DataSources.DBDataSources.Item(0)
            frmMachineMaster.Items.Item("LinkFG").Visible = False
            Me.InitForm()
            Me.DefineModesForFields()
        Catch ex As Exception
            '  objaddon.Msg("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmMachineMaster.Freeze(True)

            Dim oItem As SAPbouiCOM.Item
            Dim oLabel As SAPbouiCOM.StaticText



            Dim oEditText As SAPbouiCOM.EditText
            Dim oComboBox As SAPbouiCOM.ComboBox
            Dim oLinkButton As SAPbouiCOM.LinkedButton
            'Dim oButton As SAPbouiCOM.Button
            'Dim oCheckBox As SAPbouiCOM.CheckBox

            frmMachineMaster.Title = "Machine Master"

            frmMachineMaster.Items.Item("6").Specific.Caption = "M/C Code"
            frmMachineMaster.Items.Item("45").Specific.Caption = "Specification"
            frmMachineMaster.Items.Item("40").Specific.Caption = "Machine Group"
            frmMachineMaster.Items.Item("214").FromPane = -1
            frmMachineMaster.Items.Item("215").FromPane = -1



            'Item Orgin
            frmMachineMaster.Items.Item("14").Specific.Checked = False
            frmMachineMaster.Items.Item("13").Specific.Checked = False
            frmMachineMaster.Items.Item("12").Specific.Checked = True
            frmMachineMaster.Items.Item("42").Specific.Checked = True
            'frmMachineMaster.Items.Item("14").FromPane = -1
            'frmMachineMaster.Items.Item("13").FromPane = -1
            'frmMachineMaster.Items.Item("12").FromPane = -1
            frmMachineMaster.Items.Item("42").FromPane = -1

            frmMachineMaster.Items.Item("2009").FromPane = -1
            frmMachineMaster.Items.Item("2013").FromPane = -1
            frmMachineMaster.Items.Item("140002024").FromPane = -1
            frmMachineMaster.Items.Item("140002027").FromPane = -1
            frmMachineMaster.Items.Item("140002025").FromPane = -1
            frmMachineMaster.Items.Item("140002028").FromPane = -1
            frmMachineMaster.Items.Item("140002026").FromPane = -1
            frmMachineMaster.Items.Item("140002029").FromPane = -1
            frmMachineMaster.Items.Item("140002031").FromPane = -1
            frmMachineMaster.Items.Item("140002032").FromPane = -1

            frmMachineMaster.Items.Item("122").FromPane = -1
            ' Price List
            frmMachineMaster.Items.Item("24").FromPane = -1
            frmMachineMaster.Items.Item("25").FromPane = -1
            frmMachineMaster.Items.Item("52").FromPane = -1
            frmMachineMaster.Items.Item("34").FromPane = -1

            'Tab
            frmMachineMaster.Items.Item("3").FromPane = -1
            frmMachineMaster.Items.Item("4").FromPane = -1
            ' frmMachineMaster.Items.Item("26").FromPane = -1
            frmMachineMaster.Items.Item("27").FromPane = -1

            'Production Data
            frmMachineMaster.Items.Item("92").FromPane = -1
            frmMachineMaster.Items.Item("172").FromPane = -1
            frmMachineMaster.Items.Item("174").FromPane = -1
            frmMachineMaster.Items.Item("249").FromPane = -1

            frmMachineMaster.Items.Item("77").FromPane = -1
            frmMachineMaster.Items.Item("160").FromPane = -1

            'Manufacturer          
            frmMachineMaster.Items.Item("185").FromPane = -1
            frmMachineMaster.Items.Item("186").FromPane = -1
            frmMachineMaster.Items.Item("32").FromPane = -1
            frmMachineMaster.Items.Item("35").FromPane = -1
            frmMachineMaster.Items.Item("183").FromPane = -1
            frmMachineMaster.Items.Item("161").FromPane = -1
            frmMachineMaster.Items.Item("162").FromPane = -1

            'Item Category
            frmMachineMaster.Items.Item("2000").FromPane = -1
            frmMachineMaster.Items.Item("2001").FromPane = -1
            frmMachineMaster.Items.Item("2002").FromPane = -1
            frmMachineMaster.Items.Item("140002023").FromPane = -1

            'Item Group
            frmMachineMaster.Items.Item("40").Top = frmMachineMaster.Items.Item("45").Top + 15
            frmMachineMaster.Items.Item("39").Top = frmMachineMaster.Items.Item("45").Top + 15
            ' frmMachineMaster.Items.Item("39").Enabled = False
            'Set Machine Group
            frmMachineMaster.Items.Item("39").Enabled = True
            oComboBox = frmMachineMaster.Items.Item("39").Specific
            Try
                ' Machine = objAddOn.getSingleValue("Select U_mgName from [@AS_OMGM] where U_LocCode =(Select top 1 b.U_Location  from OUSR a join OUDG b on a.DfltsGroup = b.Code where a.USER_CODE ='" & objAddOn.objCompany.UserName & "')")
                Machine = objAddOn.getSingleValue("Select U_MchGrp from [@AS_WIPGT]")
                'Machine = "Machinery"
                oComboBox.Select(Machine, SAPbouiCOM.BoSearchKey.psk_ByValue)
                frmMachineMaster.Items.Item("39").Enabled = False
            Catch ex As Exception
                'objAddOn.objApplication.SetStatusBarMessage("Please Add Machinery in ItemGroup", SAPbouiCOM.BoMessageTime.bmt_Medium, True)
            End Try

            frmMachineMaster.ActiveItem = "5"
            frmMachineMaster.Items.Item("39").Enabled = True

            'Item Sub Group
            'Check Wheather exist or not
            Try
                oItem = frmMachineMaster.Items.Add("c_MGRCode", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
            Catch ex As Exception
                Return
            End Try
            oItem.Left = frmMachineMaster.Items.Item("39").Left
            oItem.Width = frmMachineMaster.Items.Item("39").Width
            oItem.Height = frmMachineMaster.Items.Item("39").Height
            oItem.Top = frmMachineMaster.Items.Item("39").Top + 15
            oItem.DisplayDesc = 1
            oComboBox = oItem.Specific
            oComboBox.DataBind.SetBound(True, "OITM", "U_MGRCode")
            objAddOn.setComboBoxValue(oComboBox, "Select Code,Name from [@AS_OMGR]")

            oItem = frmMachineMaster.Items.Add("l_MGRCode", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("40").Left
            oItem.Top = frmMachineMaster.Items.Item("40").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("40").Height
            oItem.Width = frmMachineMaster.Items.Item("40").Width
            oItem.LinkTo = "c_MGRCode"
            oItem.DisplayDesc = 1
            oLabel = oItem.Specific
            oLabel.Caption = "Machine Sub Group"

            'Work Center Code
            oItem = frmMachineMaster.Items.Add("t_WhsCode", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("c_MGRCode").Left
            oItem.Width = frmMachineMaster.Items.Item("c_MGRCode").Width - 20
            oItem.Height = frmMachineMaster.Items.Item("c_MGRCode").Height
            oItem.Top = frmMachineMaster.Items.Item("c_MGRCode").Top + 100
            oItem.FromPane = 6
            oItem.ToPane = 6
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_WhsCode")
            objaddon.setEdittextCFL(frmMachineMaster, "t_WhsCode", "cfl_WHS", "64", "WhsCode")
            'objaddon.ChooseFromListFilteration(frmMachineMaster, "cfl_WHS", "U_WRKC", "Select 'Y'")

            oItem = frmMachineMaster.Items.Add("l_WhsCode", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_MGRCode").Left
            oItem.Top = frmMachineMaster.Items.Item("t_WhsCode").Top
            oItem.Height = frmMachineMaster.Items.Item("l_MGRCode").Height
            oItem.Width = frmMachineMaster.Items.Item("l_MGRCode").Width - 17
            oItem.LinkTo = "t_WhsCode"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Work Center Code"

            'Work center Link
            oItem = frmMachineMaster.Items.Add("lk_WhsCode", SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)
            oItem.Left = frmMachineMaster.Items.Item("t_WhsCode").Left - 20
            oItem.Top = frmMachineMaster.Items.Item("t_WhsCode").Top
            oItem.LinkTo = "t_WhsCode"
            oItem.Visible = False
            oLinkButton = oItem.Specific
            oLinkButton.LinkedObject = "-1"
            oLinkButton.LinkedObjectType = "-1"

            'Work Center Name
            oItem = frmMachineMaster.Items.Add("t_WhsName", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_WhsCode").Left
            oItem.Width = frmMachineMaster.Items.Item("t_WhsCode").Width
            oItem.Height = frmMachineMaster.Items.Item("t_WhsCode").Height
            oItem.Top = frmMachineMaster.Items.Item("t_WhsCode").Top + 15
            oItem.FromPane = 6
            oItem.ToPane = 6
            oItem.Enabled = False
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_WhsName")

            oItem = frmMachineMaster.Items.Add("l_WhsName", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_WhsCode").Left
            oItem.Top = frmMachineMaster.Items.Item("t_WhsName").Top
            oItem.Height = frmMachineMaster.Items.Item("l_WhsCode").Height
            oItem.Width = frmMachineMaster.Items.Item("l_WhsCode").Width
            oItem.LinkTo = "t_WhsName"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Work Center Name"

            'Location
            oItem = frmMachineMaster.Items.Add("c_Location", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
            oItem.Left = frmMachineMaster.Items.Item("t_WhsName").Left
            oItem.Width = frmMachineMaster.Items.Item("t_WhsName").Width
            oItem.Height = frmMachineMaster.Items.Item("t_WhsName").Height
            oItem.Top = frmMachineMaster.Items.Item("t_WhsName").Top + 15
            oItem.Enabled = False
            oItem.FromPane = 6
            oItem.ToPane = 6
            oItem.DisplayDesc = 1
            oComboBox = oItem.Specific
            oComboBox.DataBind.SetBound(True, "OITM", "U_Location")
            objaddon.setComboBoxValue(oComboBox, "Select Code, Location from OLCT ")

            oItem = frmMachineMaster.Items.Add("l_Location", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_WhsName").Left
            oItem.Top = frmMachineMaster.Items.Item("c_Location").Top
            oItem.Height = frmMachineMaster.Items.Item("l_WhsName").Height
            oItem.Width = frmMachineMaster.Items.Item("l_WhsName").Width
            oItem.LinkTo = "c_Location"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Location*"

            'Work center Link
            oItem = frmMachineMaster.Items.Add("lk_Locatio", SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)
            oItem.Left = frmMachineMaster.Items.Item("c_Location").Left - 20
            oItem.Top = frmMachineMaster.Items.Item("c_Location").Top
            oItem.LinkTo = "c_Location"
            oItem.Visible = True
            oLinkButton = oItem.Specific
            oLinkButton.LinkedObject = "144"
            oLinkButton.LinkedObjectType = "144"

            ''Per Hour Rate
            'oItem = frmMachineMaster.Items.Add("t_PerHrRt", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            'oItem.Left = frmMachineMaster.Items.Item("c_Location").Left
            'oItem.Width = frmMachineMaster.Items.Item("c_Location").Width
            'oItem.Height = frmMachineMaster.Items.Item("c_Location").Height
            'oItem.Top = frmMachineMaster.Items.Item("c_Location").Top + 15
            'oItem.FromPane = 6
            'oItem.ToPane = 6
            'oEditText = oItem.Specific
            'oEditText.DataBind.SetBound(True, "OITM", "U_PerHrRt")

            'oItem = frmMachineMaster.Items.Add("l_PerHrRt", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            'oItem.Left = frmMachineMaster.Items.Item("l_Location").Left
            'oItem.Top = frmMachineMaster.Items.Item("t_PerHrRt").Top
            'oItem.Height = frmMachineMaster.Items.Item("l_Location").Height
            'oItem.Width = frmMachineMaster.Items.Item("l_Location").Width
            'oItem.LinkTo = "t_PerHrRt"
            'oItem.FromPane = 6
            'oItem.ToPane = 6
            'oLabel = oItem.Specific
            'oLabel.Caption = "Per Hour Rate"


            'Design Capacity
            oItem = frmMachineMaster.Items.Add("t_DsnCap", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("c_Location").Left
            oItem.Width = frmMachineMaster.Items.Item("c_Location").Width
            oItem.Height = frmMachineMaster.Items.Item("c_Location").Height
            oItem.Top = frmMachineMaster.Items.Item("c_Location").Top + 15
            oItem.FromPane = 6
            oItem.ToPane = 6
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_DsnCap")

            oItem = frmMachineMaster.Items.Add("l_DsnCap", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_Location").Left
            oItem.Top = frmMachineMaster.Items.Item("l_Location").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_Location").Height
            oItem.Width = frmMachineMaster.Items.Item("l_Location").Width
            oItem.LinkTo = "t_DsnCap"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Design Capacity"

            'Machine Capacity
            oItem = frmMachineMaster.Items.Add("t_McnCap", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("c_Location").Left
            oItem.Width = frmMachineMaster.Items.Item("c_Location").Width
            oItem.Height = frmMachineMaster.Items.Item("c_Location").Height
            oItem.Top = frmMachineMaster.Items.Item("c_Location").Top + 30
            oItem.FromPane = 6
            oItem.ToPane = 6
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_McnCap")

            oItem = frmMachineMaster.Items.Add("l_McnCap", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_Location").Left
            oItem.Top = frmMachineMaster.Items.Item("l_Location").Top + 30
            oItem.Height = frmMachineMaster.Items.Item("l_Location").Height
            oItem.Width = frmMachineMaster.Items.Item("l_Location").Width
            oItem.LinkTo = "t_McnCap"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Machine Capacity"
            'oItem = frmMachineMaster.Items.Item("1470002294").Specific

            '
            frmMachineMaster.Items.Item("1470002294").Left = frmMachineMaster.Items.Item("l_McnCap").Left
            frmMachineMaster.Items.Item("1470002294").Top = frmMachineMaster.Items.Item("l_McnCap").Top + 30
            frmMachineMaster.Items.Item("15").Left = frmMachineMaster.Items.Item("l_McnCap").Left
            frmMachineMaster.Items.Item("15").Top = frmMachineMaster.Items.Item("1470002294").Top + 20

            frmMachineMaster.Items.Item("125").Left = frmMachineMaster.Items.Item("15").Left
            frmMachineMaster.Items.Item("125").Top = frmMachineMaster.Items.Item("15").Top + 20
            frmMachineMaster.Items.Item("136").Left = frmMachineMaster.Items.Item("125").Left
            frmMachineMaster.Items.Item("136").Top = frmMachineMaster.Items.Item("125").Top + 20

            frmMachineMaster.Items.Item("50").Left = frmMachineMaster.Items.Item("136").Left + frmMachineMaster.Items.Item("136").Width
            frmMachineMaster.Items.Item("50").Top = frmMachineMaster.Items.Item("136").Top
            frmMachineMaster.Items.Item("159").Top = frmMachineMaster.Items.Item("136").Top
            frmMachineMaster.Items.Item("159").Left = frmMachineMaster.Items.Item("50").Left + 20




            'Manufacture
            frmMachineMaster.Items.Item("114").Left = frmMachineMaster.Items.Item("c_MGRCode").Left + 300
            frmMachineMaster.Items.Item("114").Top = frmMachineMaster.Items.Item("c_MGRCode").Top + 100

            frmMachineMaster.Items.Item("115").Left = frmMachineMaster.Items.Item("l_MGRCode").Left + 300
            frmMachineMaster.Items.Item("115").Top = frmMachineMaster.Items.Item("114").Top

            'Department
            oItem = frmMachineMaster.Items.Add("c_Dept", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
            oItem.Left = frmMachineMaster.Items.Item("114").Left
            oItem.Width = frmMachineMaster.Items.Item("114").Width
            oItem.Height = frmMachineMaster.Items.Item("114").Height
            oItem.Top = frmMachineMaster.Items.Item("114").Top + 15
            oItem.DisplayDesc = 1
            oItem.FromPane = 6
            oItem.ToPane = 6
            oComboBox = oItem.Specific
            oComboBox.DataBind.SetBound(True, "OITM", "U_Dept")
            objaddon.setComboBoxValue(oComboBox, "select Code,Name from oudp")

            oItem = frmMachineMaster.Items.Add("l_Dept", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("115").Left
            oItem.Top = frmMachineMaster.Items.Item("c_Dept").Top
            oItem.Height = frmMachineMaster.Items.Item("115").Height
            oItem.Width = frmMachineMaster.Items.Item("115").Width
            oItem.LinkTo = "c_Dept"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Department"

            'Model No
            oItem = frmMachineMaster.Items.Add("t_Model", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("c_Dept").Left
            oItem.Width = frmMachineMaster.Items.Item("c_Dept").Width
            oItem.Height = frmMachineMaster.Items.Item("c_Dept").Height
            oItem.Top = frmMachineMaster.Items.Item("c_Dept").Top + 15
            oItem.DisplayDesc = 1
            oItem.FromPane = 6
            oItem.ToPane = 6
            oEditText = oItem.Specific
            'oEditText.DataBind.SetBound(True, "OITM", "U_Model")

            oItem = frmMachineMaster.Items.Add("l_Model", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_Dept").Left
            oItem.Top = frmMachineMaster.Items.Item("t_Model").Top
            oItem.Height = frmMachineMaster.Items.Item("l_Dept").Height
            oItem.Width = frmMachineMaster.Items.Item("l_Dept").Width
            oItem.LinkTo = "t_Model"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Model No."

            'Instaled Date
            oItem = frmMachineMaster.Items.Add("t_IDate", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_Model").Left
            oItem.Width = frmMachineMaster.Items.Item("t_Model").Width
            oItem.Height = frmMachineMaster.Items.Item("t_Model").Height
            oItem.Top = frmMachineMaster.Items.Item("t_Model").Top + 15
            oItem.DisplayDesc = 1
            oItem.FromPane = 6
            oItem.ToPane = 6
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_IDate")

            oItem = frmMachineMaster.Items.Add("l_IDate", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_Model").Left
            oItem.Top = frmMachineMaster.Items.Item("t_IDate").Top
            oItem.Height = frmMachineMaster.Items.Item("l_Model").Height
            oItem.Width = frmMachineMaster.Items.Item("l_Model").Width
            oItem.LinkTo = "t_IDate"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Installed Date"

            'Available Hours
            oItem = frmMachineMaster.Items.Add("t_AvaiHr", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_IDate").Left
            oItem.Width = frmMachineMaster.Items.Item("t_IDate").Width
            oItem.Height = frmMachineMaster.Items.Item("t_Model").Height
            oItem.Top = frmMachineMaster.Items.Item("t_IDate").Top + 15
            oItem.DisplayDesc = 1
            oItem.FromPane = 6
            oItem.ToPane = 6
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_AvaiHr")

            oItem = frmMachineMaster.Items.Add("l_AvaiHr", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_IDate").Left
            oItem.Top = frmMachineMaster.Items.Item("t_AvaiHr").Top
            oItem.Height = frmMachineMaster.Items.Item("l_IDate").Height
            oItem.Width = frmMachineMaster.Items.Item("l_IDate").Width
            oItem.LinkTo = "t_AvaiHr"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Available Hours"

            'Service Phone
            oItem = frmMachineMaster.Items.Add("t_SPhone", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_AvaiHr").Left
            oItem.Width = frmMachineMaster.Items.Item("t_AvaiHr").Width
            oItem.Height = frmMachineMaster.Items.Item("t_AvaiHr").Height
            oItem.Top = frmMachineMaster.Items.Item("t_AvaiHr").Top + 15
            oItem.DisplayDesc = 1
            oItem.FromPane = 6
            oItem.ToPane = 6
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_SPhone")

            oItem = frmMachineMaster.Items.Add("l_SPhone", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_AvaiHr").Left
            oItem.Top = frmMachineMaster.Items.Item("t_SPhone").Top
            oItem.Height = frmMachineMaster.Items.Item("l_AvaiHr").Height
            oItem.Width = frmMachineMaster.Items.Item("l_AvaiHr").Width
            oItem.LinkTo = "t_SPhone"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Service Phone"

            'Service Priod
            oItem = frmMachineMaster.Items.Add("t_SPriod", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_SPhone").Left
            oItem.Width = frmMachineMaster.Items.Item("t_SPhone").Width
            oItem.Height = frmMachineMaster.Items.Item("t_SPhone").Height
            oItem.Top = frmMachineMaster.Items.Item("t_SPhone").Top + 15
            oItem.DisplayDesc = 1
            oItem.FromPane = 6
            oItem.ToPane = 6
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_SPriod")

            oItem = frmMachineMaster.Items.Add("l_SPriod", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_SPhone").Left
            oItem.Top = frmMachineMaster.Items.Item("t_SPriod").Top
            oItem.Height = frmMachineMaster.Items.Item("l_SPhone").Height
            oItem.Width = frmMachineMaster.Items.Item("l_SPhone").Width
            oItem.LinkTo = "t_SPriod"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Service Priod"

            'Last Service Date
            oItem = frmMachineMaster.Items.Add("t_LSDate", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_SPriod").Left
            oItem.Width = frmMachineMaster.Items.Item("t_SPriod").Width
            oItem.Height = frmMachineMaster.Items.Item("t_SPriod").Height
            oItem.Top = frmMachineMaster.Items.Item("t_SPriod").Top + 15
            oItem.DisplayDesc = 1
            oItem.FromPane = 6
            oItem.ToPane = 6
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_LSDate")

            oItem = frmMachineMaster.Items.Add("l_LSDate", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_SPriod").Left
            oItem.Top = frmMachineMaster.Items.Item("t_LSDate").Top
            oItem.Height = frmMachineMaster.Items.Item("l_SPriod").Height
            oItem.Width = frmMachineMaster.Items.Item("l_SPriod").Width
            oItem.LinkTo = "t_LSDate"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Last Service Date"

            'Next Service Date
            oItem = frmMachineMaster.Items.Add("t_NSDate", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_LSDate").Left
            oItem.Width = frmMachineMaster.Items.Item("t_LSDate").Width
            oItem.Height = frmMachineMaster.Items.Item("t_LSDate").Height
            oItem.Top = frmMachineMaster.Items.Item("t_LSDate").Top + 15
            oItem.DisplayDesc = 1
            oItem.FromPane = 6
            oItem.ToPane = 6
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_NSDate")

            oItem = frmMachineMaster.Items.Add("l_NSDate", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_LSDate").Left
            oItem.Top = frmMachineMaster.Items.Item("t_NSDate").Top
            oItem.Height = frmMachineMaster.Items.Item("l_LSDate").Height
            oItem.Width = frmMachineMaster.Items.Item("l_LSDate").Width
            oItem.LinkTo = "t_NSDate"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Next Service Date"

            'Vendor Code
            oItem = frmMachineMaster.Items.Add("t_CardCode", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_NSDate").Left
            oItem.Width = frmMachineMaster.Items.Item("t_NSDate").Width
            oItem.Height = frmMachineMaster.Items.Item("t_NSDate").Height
            oItem.Top = frmMachineMaster.Items.Item("t_NSDate").Top + 15
            oItem.DisplayDesc = 1
            oItem.FromPane = 6
            oItem.ToPane = 6
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_CardCode")
            objaddon.setEdittextCFL(frmMachineMaster, "t_CardCode", "cfl_VM", "2", "CardCode")
            objaddon.ChooseFromListFilteration(frmMachineMaster, "cfl_VM", "CardType", "Select 'S'")

            oItem = frmMachineMaster.Items.Add("l_CardCode", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_NSDate").Left
            oItem.Top = frmMachineMaster.Items.Item("t_CardCode").Top
            oItem.Height = frmMachineMaster.Items.Item("l_NSDate").Height
            oItem.Width = frmMachineMaster.Items.Item("l_NSDate").Width
            oItem.LinkTo = "t_CardCode"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Vendor Code"

            'Vendor Link
            oItem = frmMachineMaster.Items.Add("lk_CardCod", SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)
            oItem.Left = frmMachineMaster.Items.Item("t_CardCode").Left - 17
            oItem.Top = frmMachineMaster.Items.Item("t_CardCode").Top
            oItem.LinkTo = "t_CardCode"
            oItem.Visible = True
            oLinkButton = oItem.Specific
            oLinkButton.LinkedObject = "2"
            oLinkButton.LinkedObjectType = "2"

            'Vendor Name
            oItem = frmMachineMaster.Items.Add("t_CardName", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_CardCode").Left
            oItem.Width = frmMachineMaster.Items.Item("t_CardCode").Width
            oItem.Height = frmMachineMaster.Items.Item("t_CardCode").Height
            oItem.Top = frmMachineMaster.Items.Item("t_CardCode").Top + 15
            oItem.DisplayDesc = 1
            oItem.FromPane = 6
            oItem.ToPane = 6
            oItem.Enabled = False
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_CardName")

            oItem = frmMachineMaster.Items.Add("l_CardName", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_CardCode").Left
            oItem.Top = frmMachineMaster.Items.Item("t_CardName").Top
            oItem.Height = frmMachineMaster.Items.Item("l_CardCode").Height
            oItem.Width = frmMachineMaster.Items.Item("l_CardCode").Width
            oItem.LinkTo = "t_CardName"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Vendor Name"

            'Amount
            oItem = frmMachineMaster.Items.Add("t_Amount", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_CardName").Left
            oItem.Width = frmMachineMaster.Items.Item("t_CardName").Width
            oItem.Height = frmMachineMaster.Items.Item("t_CardName").Height
            oItem.Top = frmMachineMaster.Items.Item("t_CardName").Top + 15
            oItem.DisplayDesc = 1
            oItem.FromPane = 6
            oItem.ToPane = 6
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_Amount")

            oItem = frmMachineMaster.Items.Add("l_Amount", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_CardName").Left
            oItem.Top = frmMachineMaster.Items.Item("t_Amount").Top
            oItem.Height = frmMachineMaster.Items.Item("l_CardName").Height
            oItem.Width = frmMachineMaster.Items.Item("l_CardName").Width
            oItem.LinkTo = "t_Amount"
            oItem.FromPane = 6
            oItem.ToPane = 6
            oLabel = oItem.Specific
            oLabel.Caption = "Amount"
            '---------------------------------------------Floders---------------------------------------------------------------------------------------

            'Folder for Machine Cost
            Dim ofolder As SAPbouiCOM.Folder

            oItem = frmMachineMaster.Items.Add("f_Machine", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItem.Left = frmMachineMaster.Items.Item("163").Left + 200
            oItem.Width = frmMachineMaster.Items.Item("163").Width
            oItem.FromPane = 0
            oItem.ToPane = 0
            ofolder = oItem.Specific
            ofolder.Pane = 10
            ofolder.Caption = "Machine Cost"
            ofolder.GroupWith(frmMachineMaster.Items.Item("163").UniqueID)

            'Consumables
            oItem = frmMachineMaster.Items.Add("t_Consumab", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_WhsCode").Left
            oItem.Width = frmMachineMaster.Items.Item("t_WhsCode").Width
            oItem.Height = frmMachineMaster.Items.Item("t_WhsCode").Height
            oItem.Top = frmMachineMaster.Items.Item("1470002140").Top + 30
            oItem.DisplayDesc = 1
            oItem.FromPane = 10
            oItem.ToPane = 10

            'oItem.Enabled = True
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_Consumab")

            oItem = frmMachineMaster.Items.Add("l_Consumab", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_WhsCode").Left
            oItem.Top = frmMachineMaster.Items.Item("1470002140").Top + 30
            oItem.Height = frmMachineMaster.Items.Item("l_WhsCode").Height
            oItem.Width = frmMachineMaster.Items.Item("l_WhsCode").Width
            oItem.LinkTo = "t_Consumab"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Consumables"

            'Heating
            oItem = frmMachineMaster.Items.Add("t_Heating", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_WhsCode").Left
            oItem.Width = frmMachineMaster.Items.Item("t_WhsCode").Width
            oItem.Height = frmMachineMaster.Items.Item("t_WhsCode").Height
            oItem.Top = frmMachineMaster.Items.Item("t_Consumab").Top + 15
            ' oItem.DisplayDesc = 2
            oItem.FromPane = 10
            oItem.ToPane = 10
            'oItem.Enabled = True
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_Heating")

            oItem = frmMachineMaster.Items.Add("l_Heating", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_WhsCode").Left
            oItem.Top = frmMachineMaster.Items.Item("t_Consumab").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_WhsCode").Height
            oItem.Width = frmMachineMaster.Items.Item("l_WhsCode").Width
            oItem.LinkTo = "t_Heating"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Heating"

            'Sundry Shop supply
            oItem = frmMachineMaster.Items.Add("t_Sundry", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_WhsCode").Left
            oItem.Width = frmMachineMaster.Items.Item("t_WhsCode").Width
            oItem.Height = frmMachineMaster.Items.Item("t_WhsCode").Height
            oItem.Top = frmMachineMaster.Items.Item("l_Heating").Top + 15
            oItem.DisplayDesc = 3
            oItem.FromPane = 10
            oItem.ToPane = 10
            'oItem.Enabled = True
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_Sundry")

            oItem = frmMachineMaster.Items.Add("l_Sundry", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_WhsCode").Left
            oItem.Top = frmMachineMaster.Items.Item("l_Heating").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_WhsCode").Height
            oItem.Width = frmMachineMaster.Items.Item("l_WhsCode").Width + 20
            oItem.LinkTo = "t_Sundry"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Sundry Shop supply"

            'Departmental & General Warehouse
            oItem = frmMachineMaster.Items.Add("t_Depart", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_WhsCode").Left
            oItem.Width = frmMachineMaster.Items.Item("t_WhsCode").Width
            oItem.Height = frmMachineMaster.Items.Item("t_WhsCode").Height
            oItem.Top = frmMachineMaster.Items.Item("l_Sundry").Top + 15
            oItem.DisplayDesc = 4
            oItem.FromPane = 10
            oItem.ToPane = 10
            'oItem.Enabled = True
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_Depart")

            oItem = frmMachineMaster.Items.Add("l_Depart", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_WhsCode").Left
            oItem.Top = frmMachineMaster.Items.Item("l_Sundry").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_WhsCode").Height
            oItem.Width = frmMachineMaster.Items.Item("l_WhsCode").Width
            oItem.LinkTo = "t_Depart"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Dpt. General Warehouse"

            'Other Expense
            oItem = frmMachineMaster.Items.Add("t_Expense", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_WhsCode").Left
            oItem.Width = frmMachineMaster.Items.Item("t_WhsCode").Width
            oItem.Height = frmMachineMaster.Items.Item("t_WhsCode").Height
            oItem.Top = frmMachineMaster.Items.Item("l_Depart").Top + 15
            oItem.DisplayDesc = 5
            oItem.FromPane = 10
            oItem.ToPane = 10
            'oItem.Enabled = True
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_Expense")

            oItem = frmMachineMaster.Items.Add("l_Expense", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_WhsCode").Left
            oItem.Top = frmMachineMaster.Items.Item("l_Depart").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_WhsCode").Height
            oItem.Width = frmMachineMaster.Items.Item("l_WhsCode").Width
            oItem.LinkTo = "t_Expense"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Other Expense"

            'Depreciation
            oItem = frmMachineMaster.Items.Add("t_Depreci", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_WhsCode").Left
            oItem.Width = frmMachineMaster.Items.Item("t_WhsCode").Width
            oItem.Height = frmMachineMaster.Items.Item("t_WhsCode").Height
            oItem.Top = frmMachineMaster.Items.Item("l_Expense").Top + 15
            oItem.DisplayDesc = 6
            oItem.FromPane = 10
            oItem.ToPane = 10
            'oItem.Enabled = True
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_Depreci")

            oItem = frmMachineMaster.Items.Add("l_Depreci", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_WhsCode").Left
            oItem.Top = frmMachineMaster.Items.Item("l_Expense").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_WhsCode").Height
            oItem.Width = frmMachineMaster.Items.Item("l_WhsCode").Width
            oItem.LinkTo = "t_Depreci"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Depreciation"

            'Insurance
            oItem = frmMachineMaster.Items.Add("t_Insuranc", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_WhsCode").Left
            oItem.Width = frmMachineMaster.Items.Item("t_WhsCode").Width
            oItem.Height = frmMachineMaster.Items.Item("t_WhsCode").Height
            oItem.Top = frmMachineMaster.Items.Item("l_Depreci").Top + 15
            oItem.DisplayDesc = 7
            oItem.FromPane = 10
            oItem.ToPane = 10
            'oItem.Enabled = True
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_Insuranc")

            oItem = frmMachineMaster.Items.Add("l_Insuranc", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_WhsCode").Left
            oItem.Top = frmMachineMaster.Items.Item("l_Depreci").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_WhsCode").Height
            oItem.Width = frmMachineMaster.Items.Item("l_WhsCode").Width
            oItem.LinkTo = "t_Insuranc"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Insurance"

            ' Rent
            oItem = frmMachineMaster.Items.Add("t_Rent", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_WhsCode").Left
            oItem.Width = frmMachineMaster.Items.Item("t_WhsCode").Width
            oItem.Height = frmMachineMaster.Items.Item("t_WhsCode").Height
            oItem.Top = frmMachineMaster.Items.Item("l_Insuranc").Top + 15
            oItem.DisplayDesc = 8
            oItem.FromPane = 10
            oItem.ToPane = 10
            'oItem.Enabled = True
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_Rent")

            oItem = frmMachineMaster.Items.Add("l_Rent", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_WhsCode").Left
            oItem.Top = frmMachineMaster.Items.Item("l_Insuranc").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_WhsCode").Height
            oItem.Width = frmMachineMaster.Items.Item("l_WhsCode").Width
            oItem.LinkTo = "t_Rent"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Rent"

            'Machine Per Hour Rate
            oItem = frmMachineMaster.Items.Add("t_MPHRate", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("t_WhsCode").Left
            oItem.Width = frmMachineMaster.Items.Item("t_WhsCode").Width
            oItem.Height = frmMachineMaster.Items.Item("t_WhsCode").Height
            oItem.Top = frmMachineMaster.Items.Item("l_Rent").Top + 15
            oItem.DisplayDesc = 9
            oItem.FromPane = 10
            oItem.ToPane = 10
            oItem.Enabled = False
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_MPHRate")

            oItem = frmMachineMaster.Items.Add("l_MPHRate", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_WhsCode").Left
            oItem.Top = frmMachineMaster.Items.Item("l_Rent").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_WhsCode").Height
            oItem.Width = frmMachineMaster.Items.Item("l_WhsCode").Width
            oItem.LinkTo = "t_MPHRate"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Machine Per Hour Rate"


            frmMachineMaster.Items.Item("1470002165").Left = frmMachineMaster.Items.Item("l_Dept").Left
            frmMachineMaster.Items.Item("1470002165").Width = frmMachineMaster.Items.Item("l_Dept").Width
            frmMachineMaster.Items.Item("1470002165").Top = frmMachineMaster.Items.Item("l_Consumab").Top
            frmMachineMaster.Items.Item("1470002166").Left = frmMachineMaster.Items.Item("c_Dept").Left
            frmMachineMaster.Items.Item("1470002166").Top = frmMachineMaster.Items.Item("l_Consumab").Top



            frmMachineMaster.Items.Item("1470002167").Left = frmMachineMaster.Items.Item("1470002165").Left
            frmMachineMaster.Items.Item("1470002167").Top = frmMachineMaster.Items.Item("1470002165").Top + 15
            frmMachineMaster.Items.Item("1470002167").Width = frmMachineMaster.Items.Item("1470002165").Width
            frmMachineMaster.Items.Item("1470002192").Left = frmMachineMaster.Items.Item("1470002166").Left
            frmMachineMaster.Items.Item("1470002192").Top = frmMachineMaster.Items.Item("1470002166").Top + 15

            frmMachineMaster.Items.Item("1470002142").Top = frmMachineMaster.Items.Item("1470002167").Top + 15
            frmMachineMaster.Items.Item("1470002152").Top = frmMachineMaster.Items.Item("1470002167").Top + 15

            'Power Consumption per hr
            oItem = frmMachineMaster.Items.Add("t_PowerPHr", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("c_Dept").Left
            oItem.Width = frmMachineMaster.Items.Item("c_Dept").Width
            oItem.Height = frmMachineMaster.Items.Item("c_Dept").Height
            oItem.Top = frmMachineMaster.Items.Item("1470002167").Top + 20
            oItem.DisplayDesc = 10
            oItem.FromPane = 10
            oItem.ToPane = 10
            'oItem.Enabled = True
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_PowerPHr")

            oItem = frmMachineMaster.Items.Add("l_PowerPHr", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_Dept").Left
            oItem.Top = frmMachineMaster.Items.Item("1470002167").Top + 20
            oItem.Height = frmMachineMaster.Items.Item("l_Dept").Height
            oItem.Width = frmMachineMaster.Items.Item("l_Consumab").Width
            oItem.LinkTo = "t_PowerPHr"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Power Consumption per hr"
            'Value for 1KW in Rs
            oItem = frmMachineMaster.Items.Add("t_Value", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("c_Dept").Left
            oItem.Width = frmMachineMaster.Items.Item("c_Dept").Width
            oItem.Height = frmMachineMaster.Items.Item("c_Dept").Height
            oItem.Top = frmMachineMaster.Items.Item("l_PowerPHr").Top + 15
            oItem.DisplayDesc = 10
            oItem.FromPane = 10
            oItem.ToPane = 10
            'oItem.Enabled = True
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_Value")

            oItem = frmMachineMaster.Items.Add("l_Value", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_Dept").Left
            oItem.Top = frmMachineMaster.Items.Item("l_PowerPHr").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_Dept").Height
            oItem.Width = frmMachineMaster.Items.Item("l_Dept").Width
            oItem.LinkTo = "t_Value"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Value for 1KW in Rs"

            'No of Hours per month
            oItem = frmMachineMaster.Items.Add("t_NHPMonth", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("c_Dept").Left
            oItem.Width = frmMachineMaster.Items.Item("c_Dept").Width
            oItem.Height = frmMachineMaster.Items.Item("c_Dept").Height
            oItem.Top = frmMachineMaster.Items.Item("l_Value").Top + 15
            oItem.DisplayDesc = 10
            oItem.FromPane = 10
            oItem.ToPane = 10
            ' oItem.Enabled = False
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_NHPMonth")

            oItem = frmMachineMaster.Items.Add("l_NHPMonth", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_Dept").Left
            oItem.Top = frmMachineMaster.Items.Item("l_Value").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_Dept").Height
            oItem.Width = frmMachineMaster.Items.Item("l_Dept").Width + 20
            oItem.LinkTo = "t_NHPMonth"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "No.of Hrs per month"

            ' Per Hour value of power consumption

            oItem = frmMachineMaster.Items.Add("t_PHVPower", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("c_Dept").Left
            oItem.Width = frmMachineMaster.Items.Item("c_Dept").Width
            oItem.Height = frmMachineMaster.Items.Item("c_Dept").Height
            oItem.Top = frmMachineMaster.Items.Item("l_NHPMonth").Top + 15
            oItem.DisplayDesc = 10
            oItem.FromPane = 10
            oItem.ToPane = 10
            oItem.Enabled = False
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_PHVPower")

            oItem = frmMachineMaster.Items.Add("l_PHVPower", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_Dept").Left
            oItem.Top = frmMachineMaster.Items.Item("l_NHPMonth").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_Dept").Height
            oItem.Width = frmMachineMaster.Items.Item("l_Dept").Width + 30
            oItem.LinkTo = "t_PHVPower"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Per Hour value of pwr.cons."
            ' Power Consumption per Month
            oItem = frmMachineMaster.Items.Add("t_PowerPMn", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = frmMachineMaster.Items.Item("c_Dept").Left
            oItem.Width = frmMachineMaster.Items.Item("c_Dept").Width
            oItem.Height = frmMachineMaster.Items.Item("c_Dept").Height
            oItem.Top = frmMachineMaster.Items.Item("l_PHVPower").Top + 15
            oItem.DisplayDesc = 10
            oItem.FromPane = 10
            oItem.ToPane = 10
            oItem.Enabled = False
            oEditText = oItem.Specific
            oEditText.DataBind.SetBound(True, "OITM", "U_PowerPMn")

            oItem = frmMachineMaster.Items.Add("l_PowerPMn", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmMachineMaster.Items.Item("l_Dept").Left
            oItem.Top = frmMachineMaster.Items.Item("l_PHVPower").Top + 15
            oItem.Height = frmMachineMaster.Items.Item("l_Dept").Height
            oItem.Width = frmMachineMaster.Items.Item("l_Dept").Width + 20
            oItem.LinkTo = "t_PowerPMn"
            oItem.FromPane = 10
            oItem.ToPane = 10
            oLabel = oItem.Specific
            oLabel.Caption = "Power Cons. per Month"



            frmMachineMaster.Items.Item("1470002215").Top = frmMachineMaster.Items.Item("l_MPHRate").Top + 20

            frmMachineMaster.Items.Item("1470002214").Top = frmMachineMaster.Items.Item("1470002192").Top + 20

            partsbutton()
        Catch ex As Exception
            ' objaddon.objapplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmMachineMaster.Freeze(False)

        End Try
    End Sub
    Sub partsButton()
        Try
            'frmItem = objAddOn.objApplication.Forms.GetForm("150", -1)
            Dim oButton As SAPbouiCOM.Button
            frmMachineMaster.Freeze(True)
            oItem = frmMachineMaster.Items.Item("5")

            oNewItem = frmMachineMaster.Items.Add("PARTS", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oNewItem.Left = oItem.Left + oItem.Width + 4
            oNewItem.Width = 50
            oNewItem.Height = oItem.Height
            oNewItem.Top = oItem.Top

            oButton = oNewItem.Specific
            oButton.Caption = "PARTS"


        Catch ex As Exception
            'objAddOn.objApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmMachineMaster.Freeze(False)
        End Try
    End Sub

    Sub DefineModesForFields()
        Try
            frmMachineMaster.Items.Item("t_WhsName").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmMachineMaster.Items.Item("t_WhsName").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmMachineMaster.Items.Item("t_WhsName").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 3, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            frmMachineMaster.Items.Item("c_Location").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmMachineMaster.Items.Item("c_Location").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmMachineMaster.Items.Item("c_Location").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 3, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            frmMachineMaster.Items.Item("t_CardName").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmMachineMaster.Items.Item("t_CardName").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmMachineMaster.Items.Item("t_CardName").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 3, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            frmMachineMaster.Items.Item("t_MPHRate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmMachineMaster.Items.Item("t_PHVPower").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmMachineMaster.Items.Item("t_PowerPMn").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            ' frmMachineMaster.Items.Item("t_NHPMonth").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        Catch ex As Exception
            objaddon.objapplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function ValidateAll() As Boolean
        Try
            ValidateAll = True
        Catch ex As Exception
            objaddon.objapplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean, Optional ByVal form As String = "")
        Try
            'If frmMachineMaster Is Nothing Then
            '    frmMachineMaster = objAddOn.objApplication.Forms.Item(FormUID)
            '    frmMachineMaster.Title = "Machine Master"
            'End If
            Select Case pVal.EventType
                

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                'Add,Update Event
                                If pVal.BeforeAction = True And (frmMachineMaster.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmMachineMaster.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                                    If Me.ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Exit Sub
                                    Else
                                        Dim chk As SAPbouiCOM.CheckBox
                                        Dim com As SAPbouiCOM.ComboBox
                                        If frmMachineMaster.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                            If frmMachineMaster.Items.Item("248").Visible = True Then
                                                frmMachineMaster.Items.Item("248").Specific.Select("S", SAPbouiCOM.BoSearchKey.psk_ByValue)
                                            End If

                                            chk = frmMachineMaster.Items.Item("14").Specific
                                            chk.Checked = False
                                        End If
                                        com = frmMachineMaster.Items.Item("39").Specific
                                        If com.Selected.Value <> Machine Then
                                            frmMachineMaster.Items.Item("39").Enabled = True
                                            frmMachineMaster.Items.Item("39").Specific.Select(Machine, SAPbouiCOM.BoSearchKey.psk_ByValue)
                                            frmMachineMaster.ActiveItem = "5"
                                            frmMachineMaster.Items.Item("39").Enabled = False
                                        End If

                                    End If
                                End If
                                If pVal.BeforeAction = True And frmMachineMaster.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                    frmMachineMaster.Items.Item("39").Enabled = True
                                    frmMachineMaster.Items.Item("39").Specific.Select(Machine, SAPbouiCOM.BoSearchKey.psk_ByValue)
                                    frmMachineMaster.ActiveItem = "5"
                                    frmMachineMaster.Items.Item("39").Enabled = False
                                End If
                                ' frmMachineMaster.Items.Item("1").Click()
                            Case "PARTS"
                                If pVal.BeforeAction = False Then
                                    'If pVal.ItemUID = "Parts" Then
                                    Dim mcode As String = frmMachineMaster.Items.Item("5").Specific.value
                                    Dim mname As String = frmMachineMaster.Items.Item("7").Specific.value

                                    If mcode = "" Or mname = "" Then
                                        objAddOn.objApplication.StatusBar.SetText("Please Enter machine code and Machine Name", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)

                                    Else

                                        partdet.LoadScreen(mcode, mname)
                                    End If

                                    'frmItem.Items.Item("").Specific.value()
                                    'objAddOn.objPartsDetails.LoadScreen(itemcode, itemname)
                                    'End If
                                End If
                        End Select
                        
                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        BubbleEvent = False
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED

                    Select Case pVal.ItemUID
                        Case "f_Machine"
                            If pVal.BeforeAction = False Then _
                                frmMachineMaster.PaneLevel = 10
                        Case "lk_WhsCode"
                            Try
                                'Link button on Machine master
                                If pVal.BeforeAction = True Then
                                    objAddOn.objApplication.Menus.Item("WRK").Activate()
                                Else
                                    Dim frm As SAPbouiCOM.Form
                                    frm = objAddOn.objApplication.Forms.ActiveForm
                                    frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                    frm.Items.Item("5").Specific.Value = oDBDSHeader.GetValue("U_WhsCode", 0)
                                    frm.Items.Item("1").Click()
                                End If
                            Catch ex As Exception
                                objAddOn.objApplication.StatusBar.SetText("Item Pressed event Failed:" & ex.Message)
                            End Try
                        Case "1"
                            If pVal.ActionSuccess = True Then
                                Try
                                    Me.InitForm()
                                Catch ex As Exception
                                    objAddOn.objApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message)
                                End Try
                            End If
                    End Select
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try
                        If pVal.BeforeAction = False Then
                            Select Case pVal.ItemUID
                                Case "t_Rent", "t_Consumab", "t_Heating", "t_Sundry", "t_Depart", "t_Expense", "t_Depreci", "t_Insuranc"
                                    MachinePerHourRateCalc()
                                Case "t_PowerPHr", "t_Value"
                                    PowerConsumptionperMonthCalc()
                                Case "t_NHPMonth"
                                    MachinePerHourRateCalc()
                                    PowerConsumptionperMonthCalc()
                            End Select
                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("LOST FOCUS Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        If pVal.BeforeAction = False Then
                            Dim oDataTable As SAPbouiCOM.DataTable
                            Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                            oDataTable = oCFLE.SelectedObjects
                            Select Case oCFLE.ChooseFromListUID
                                Case "cfl_WHS"
                                    frmMachineMaster.Items.Item("t_WhsName").Specific.Value = oDataTable.GetValue("WhsName", 0)
                                    frmMachineMaster.Items.Item("t_WhsCode").Specific.Value = oDataTable.GetValue("WhsCode", 0)

                            End Select
                        End If
                    Catch ex As Exception

                    End Try
            End Select
            Try
                frmMachineMaster.Title = "Machine Master"
            Catch ex As Exception

            End Try
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Event :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try


    End Sub

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1281"
                    frmMachineMaster.Items.Item("39").Enabled = True
                    frmMachineMaster.Items.Item("39").Specific.Select(Machine, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                    frmMachineMaster.ActiveItem = "5"
                    frmMachineMaster.Items.Item("39").Enabled = False
                Case "1282"
                    Me.InitForm()
                Case "1293"
                    objAddOn.DeleteRow(oMatrix, oDBDSDetail)
                   
            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
                    Dim rsetUpadteIndent As SAPbobsCOM.Recordset = objaddon.objcompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Try
                        If BusinessObjectInfo.BeforeAction Then
                            If Me.ValidateAll() = False Then
                                System.Media.SystemSounds.Asterisk.Play()
                                BubbleEvent = False
                                Exit Sub
                            Else
                            End If
                        End If
                        If BusinessObjectInfo.ActionSuccess = True Then
                        End If
                    Catch ex As Exception
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If BusinessObjectInfo.ActionSuccess = True Then
                        frmMachineMaster.Title = "Machine Master"
                        frmMachineMaster.Items.Item("39").Enabled = True



                    End If
            End Select
        Catch ex As Exception
            objaddon.objapplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub MachinePerHourRateCalc()
        Try
            Dim MPHRate As Double = 0.0
            Dim Consumable = frmMachineMaster.Items.Item("t_Consumab").Specific.value
            Dim Heating = frmMachineMaster.Items.Item("t_Heating").Specific.value
            Dim Sundry = frmMachineMaster.Items.Item("t_Sundry").Specific.value
            Dim Departmental = frmMachineMaster.Items.Item("t_Depart").Specific.value
            Dim Expenses = frmMachineMaster.Items.Item("t_Expense").Specific.value
            Dim Depeciation = frmMachineMaster.Items.Item("t_Depreci").Specific.value
            Dim Insurance = frmMachineMaster.Items.Item("t_Insuranc").Specific.value
            Dim Rent = frmMachineMaster.Items.Item("t_Rent").Specific.value
            Dim NHPMonth = frmMachineMaster.Items.Item("t_NHPMonth").Specific.value
            Dim PowerPMn = frmMachineMaster.Items.Item("t_PowerPMn").Specific.value
            MPHRate = (CDbl(Consumable) + CDbl(Heating) + CDbl(Sundry) + CDbl(Departmental) + CDbl(Expenses) + CDbl(Depeciation) + CDbl(Insurance) + CDbl(Rent) + CDbl(PowerPMn)) / CDbl(NHPMonth)
            frmMachineMaster.Items.Item("t_MPHRate").Specific.value = MPHRate
            '  frmMachineMaster.Items.Item("t_PerHrRt").Specific.value = MPHRate

        Catch ex As Exception
            objaddon.objapplication.StatusBar.SetText("MachinePerHourRateCalc Method Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub
    'Power Consumption per Month
    Sub PowerConsumptionperMonthCalc()
        Try

            Dim PHVPower As Double
            Dim PowerCPM As Double

            Dim PowerPrHr = frmMachineMaster.Items.Item("t_PowerPHr").Specific.value
            Dim Value = frmMachineMaster.Items.Item("t_Value").Specific.value
            Dim NHPMonth = frmMachineMaster.Items.Item("t_NHPMonth").Specific.value
            PHVPower = (CDbl(Value) * CDbl(PowerPrHr))
            PowerCPM = PHVPower * NHPMonth
            frmMachineMaster.Items.Item("t_PHVPower").Specific.value = PHVPower
            frmMachineMaster.Items.Item("t_PowerPMn").Specific.value = PowerCPM

        Catch ex As Exception
            objaddon.objapplication.StatusBar.SetText("PowerConsumptionperMonthCalc Method Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub
End Class
