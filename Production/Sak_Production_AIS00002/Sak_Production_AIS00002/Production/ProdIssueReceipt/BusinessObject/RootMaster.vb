﻿Public Class ClsRootMaster
    Dim oDBDSHeader, oDBDSLine As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim oForm As SAPbouiCOM.Form
    Dim oDataTable As SAPbouiCOM.DataTable
    Dim oCFLE As SAPbouiCOM.ChooseFromListEvent

    Sub LoadScreen()
        Try
            oForm = objAddOn.objUIXml.LoadScreenXML("RootMaster.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, "ROOT")
            oDBDSHeader = oForm.DataSources.DBDataSources.Item(0)
            oDBDSLine = oForm.DataSources.DBDataSources.Item(1)
            oMatrix = oForm.Items.Item("Matrix").Specific
            oForm.EnableMenu("1292", True)
            oForm.EnableMenu("1293", True)
            oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            Add_Mode()
          
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Form Load " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
    Sub Add_Mode()
        oMatrix.AddRow()
        oMatrix.ClearRowData(oMatrix.VisualRowCount)
        oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value() = oMatrix.VisualRowCount

        oForm.Items.Item("14").Specific.value = oForm.BusinessObject.GetNextSerialNumber("-1", "AS_ROOT")
        'oForm.Items.Item("Code").Enabled = True
    End Sub
    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            If pVal.ItemUID = "1" Then
                                Dim Itrate As Integer
                                oForm = objAddOn.objApplication.Forms.ActiveForm
                                oMatrix = oForm.Items.Item("Matrix").Specific

                                For Itrate = 1 To oMatrix.VisualRowCount
                                    If oMatrix.Columns.Item("OpCode").Cells.Item(Itrate).Specific.string = "" Then
                                        oMatrix.DeleteRow(Itrate)
                                    End If
                                Next


                                If Validation() = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                Else
                                    objAddOn.DeleteEmptyRowInFormDataEvent(oMatrix, "OpCode", oDBDSLine)
                                End If
                            End If
                            End If

                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Click Event " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Select oCFLE.ChooseFromListUID
                            Case "Cfl_fg"
                                objAddOn.ChooseFromListFilteration(oForm, "Cfl_fg", "ItemCode", "Select ItemCode from OITM where ItemCode in (Select Code From oitt)")
                        End Select
                    Catch ex As Exception

                    End Try
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        oForm.Items.Item("14").Enabled = False
                    End If
                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        Add_Mode()
                    End If
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try
                       

                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Lost Focus " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Select Case oCFLE.ChooseFromListUID
                            Case "Cfl_fg"
                                'oMatrix.FlushToDataSource()
                                oDBDSHeader.SetValue("U_FGCode", 0, oDataTable.GetValue("ItemCode", 0))
                                oDBDSHeader.SetValue("U_FGName", 0, oDataTable.GetValue("ItemName", 0))
                                'oMatrix.LoadFromDataSourceEx()
                            Case "Cfl_Oprn"
                                oMatrix.FlushToDataSource()
                                oDBDSLine.SetValue("U_OpCode", pVal.Row - 1, oDataTable.GetValue("Code", 0))
                                oDBDSLine.SetValue("U_OpName", pVal.Row - 1, oDataTable.GetValue("Name", 0))
                                oDBDSLine.SetValue("U_PCyc", pVal.Row - 1, oDataTable.GetValue("U_Cycle", 0))
                                oDBDSLine.SetValue("U_Process", pVal.Row - 1, oDataTable.GetValue("U_Process", 0))
                                oMatrix.LoadFromDataSourceEx()
                                If oMatrix.Columns.Item("OpCode").Cells.Item(pVal.Row).Specific.String <> "" And pVal.Row = oMatrix.VisualRowCount Then
                                    objAddOn.SetNewLine(oMatrix, oDBDSLine)
                                End If
                                If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                    oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                End If
                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Cfl " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try

            End Select

        End If

    End Sub
    Public Sub FormDataEvent(ByVal BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Or oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        oForm.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                    End If
            End Select
        Catch ex As Exception

        End Try
    End Sub

    Function Validation()
        Try
            If oForm.Items.Item("RuleC").Specific.String = "" Then
                objAddOn.objApplication.SetStatusBarMessage("RULE Code Should Not Be Left", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                Return False
            End If
            If oForm.Items.Item("RuleN").Specific.String = "" Then
                objAddOn.objApplication.SetStatusBarMessage("RULE Name Should Not Be Left", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                Return False
            End If
            
            If oMatrix.VisualRowCount > 0 Then
                If oMatrix.Columns.Item("OpCode").Cells.Item(1).Specific.String = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Select Operation Code Atleast One", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    Return False
                End If

            End If
        Catch ex As Exception
            Return False
        End Try
      
        Return True
    End Function

   

    Sub Menu_event(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Select Case pVal.MenuUID

            Case "1292"
                oForm = objAddOn.objApplication.Forms.ActiveForm
                If oForm.TypeEx = "ROOT" Then
                    Try
                        Dim oEdit, otext As SAPbouiCOM.EditText
                        oForm = objAddOn.objApplication.Forms.ActiveForm
                        oMatrix = oForm.Items.Item("Matrix").Specific
                        If oMatrix.VisualRowCount = 0 Then
                            oMatrix.AddRow(1, 1)
                            oMatrix = oForm.Items.Item("Matrix").Specific
                            oEdit = oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific
                            oEdit.Value = 1

                        Else
                            'oMatrix = oForm.Items.Item("Matrix").Specific
                            'otext = oMatrix.Columns.Item("OpCode").Cells.Item(oMatrix.VisualRowCount).Specific
                            'If otext.Value <> "" Then
                            '    oMatrix.AddRow(1, -1)
                            '    oEdit = oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific
                            '    oEdit.Value = oMatrix.VisualRowCount
                            oMatrix = oForm.Items.Item("Matrix").Specific
                            otext = oMatrix.Columns.Item("OpCode").Cells.Item(oMatrix.VisualRowCount).Specific
                            If otext.Value <> "" Then
                                oForm.DataSources.DBDataSources.Item("@AS_ROOT1").Clear()
                                oMatrix = oForm.Items.Item("Matrix").Specific
                                Dim iTast As Integer

                                For iTast = 1 To oMatrix.VisualRowCount
                                    If oMatrix.IsRowSelected(iTast) = True Then
                                        Exit For
                                    End If
                                Next

                                oMatrix.AddRow(1, iTast)
                                'oMatrix.AddRow()

                                oMatrix = oForm.Items.Item("Matrix").Specific
                                oEdit = oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific
                                For iTast = 1 To oMatrix.VisualRowCount
                                    oEdit = oMatrix.Columns.Item("V_-1").Cells.Item(iTast).Specific
                                    oEdit.Value = iTast
                                Next
                            Else
                                Exit Sub
                            End If
                        End If

                    Catch ex As Exception
                        'SBO_Application.MessageBox(ex.Message)
                    End Try

                End If

        End Select

        If pVal.MenuUID = "1282" Then
            Add_Mode()
        ElseIf pVal.MenuUID = "1281" Then
            oForm.Items.Item("14").Enabled = True
        Else
            oForm.Items.Item("14").Enabled = False
            'oForm.Items.Item("Code").Enabled = False
        End If

    End Sub
End Class
