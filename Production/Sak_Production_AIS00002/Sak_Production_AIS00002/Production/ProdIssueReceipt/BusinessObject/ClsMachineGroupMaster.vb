﻿Public Class ClsMachineGroupMaster

    Public frmMachineGroupMaster As SAPbouiCOM.Form
    'Dim UDOID As String = "MGR"
    Public Const FormType = "OMGR"
    Sub Menu_event(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        If pVal.MenuUID = "1282" Or pVal.MenuUID = "1281" Then
            InitForm()
        Else
            frmMachineGroupMaster.Items.Item("Code").Enabled = False
        End If
    End Sub

    Sub LoadForm()
        Try

            frmMachineGroupMaster = objAddOn.objUIXml.LoadScreenXML("MachineGroupMaster.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            'Assign Data Source
            'Me.DefineModesForFields()
            frmMachineGroupMaster.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            'frmMachineGroupMaster.DataBrowser.BrowseBy = "Code"
            Me.InitForm()
        Catch ex As Exception
            '   objAddOn.Msg("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub

    Sub InitForm()
        frmMachineGroupMaster.Items.Item("Code").Enabled = True
        'Try
        '    frmMachineGroupMaster.Freeze(True)
        'Catch ex As Exception
        '    objAddOn.objApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        'Finally
        '    frmMachineGroupMaster.Freeze(False)

        'End Try
    End Sub

    'Sub DefineModesForFields()
    '    Try
    '        frmMachineGroupMaster.Items.Item("t_Code").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
    '    Catch ex As Exception
    '        objAddOn.objApplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
    '    Finally
    '    End Try
    'End Sub

    Function ValidateAll() As Boolean
        Try
            If Not objAddOn.isDuplicate(frmMachineGroupMaster.Items.Item("Code").Specific, "[@AS_OMGR]", "Code", "Machine Group Code Should not be duplicate") And frmMachineGroupMaster.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                Return False
            End If

            If frmMachineGroupMaster.Items.Item("Code").Specific.value.ToString.Trim.Equals("") = True Then
                objAddOn.Msg(" Machine Group Code Should Not Be Left Empty")
                frmMachineGroupMaster.ActiveItem = "Code"
                Return False
            End If
            If frmMachineGroupMaster.Items.Item("Name").Specific.value.ToString.Trim.Equals("") = True Then
                objAddOn.Msg(" Machine Group Name Should Not Be Left Empty")
                frmMachineGroupMaster.ActiveItem = "Name"
                Return False
            End If
            ValidateAll = True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function
    Public Sub FormDataEvent(ByVal BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If frmMachineGroupMaster.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Or frmMachineGroupMaster.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        frmMachineGroupMaster.Items.Item("Code").Enabled = False
                    End If
            End Select
        Catch ex As Exception

        End Try
    End Sub
    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)

        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                'Add,Update Event
                                If pVal.BeforeAction = True And (frmMachineGroupMaster.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmMachineGroupMaster.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                                    If Me.ValidateAll() = False Then
                                        BubbleEvent = False
                                        Exit Sub
                                    End If
                                End If
                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

                    End Try

            End Select
        Else
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And frmMachineGroupMaster.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        frmMachineGroupMaster.Items.Item("Code").Enabled = False
                    End If
            End Select
        End If

    End Sub
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        If pVal.MenuUID = "1282" Or pVal.MenuUID = "1281" Then
            InitForm()
        Else
            frmMachineGroupMaster.Items.Item("Code").Enabled = False
        End If
    End Sub

    'Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
    '    Try
    '        Select Case BusinessObjectInfo.EventType

    '            Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
    '                Dim rsetUpadteIndent As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
    '                Try
    '                    If BusinessObjectInfo.BeforeAction Then
    '                        If Me.ValidateAll() = False Then
    '                            System.Media.SystemSounds.Asterisk.Play()
    '                            BubbleEvent = False
    '                            Exit Sub
    '                            'Else
    '                            'If frmMachineGroupMaster.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
    '                            '    oDBDSHeader.SetValue("Code", 0, objAddOn.GetCodeGeneration("[@AIS_OMGR]"))
    '                            'End If
    '                        End If
    '                    End If
    '                    If BusinessObjectInfo.ActionSuccess = True Then
    '                    End If
    '                Catch ex As Exception
    '                    BubbleEvent = False
    '                Finally
    '                End Try
    '            Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
    '                If BusinessObjectInfo.ActionSuccess = True Then
    '                End If
    '        End Select
    '    Catch ex As Exception
    '        objAddOn.objApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
    '    Finally
    '    End Try
    'End Sub



End Class
