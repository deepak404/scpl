﻿Public Class ClsWorkOrder
    Dim oDBDSHeader, oDBDSLine, ODBDSLine2, ODBDSource3, ODBDSource4 As SAPbouiCOM.DBDataSource
    Dim oMatrix, oMatrix1, oMatrix2, oMatrix4 As SAPbouiCOM.Matrix
    Dim frmWorkOrder As SAPbouiCOM.Form
    Dim oDataTable As SAPbouiCOM.DataTable
    Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
    Dim DocEntry, Str As String
    Dim Ors As SAPbobsCOM.Recordset
    Dim current As Integer
    Dim oCombo, oCombo1 As SAPbouiCOM.ComboBox
    Dim cellCtl As SAPbouiCOM.CommonSetting

    Sub LoadScreen()
        Try
            frmWorkOrder = objAddOn.objUIXml.LoadScreenXML("WorkOrder.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, "AS_OWORD")
            oDBDSHeader = frmWorkOrder.DataSources.DBDataSources.Item("@AS_OWORD")
            oDBDSLine = frmWorkOrder.DataSources.DBDataSources.Item("@AS_WORD1")
            'ODBDSLine1 = oWorkOrder.DataSources.DBDataSources.Item(2)
            ODBDSLine2 = frmWorkOrder.DataSources.DBDataSources.Item("@AS_WORD2")
            ODBDSource3 = frmWorkOrder.DataSources.DBDataSources.Item("@AS_WORD3")
            ODBDSource4 = frmWorkOrder.DataSources.DBDataSources.Item("@AS_WORD4")
            oCombo1 = frmWorkOrder.Items.Item("26").Specific
            objAddOn.objGenFunc.setComboBoxValue(oCombo1, "select * from [@AIS_WrkTyp]")


            oMatrix = frmWorkOrder.Items.Item("Matrix").Specific
            oMatrix1 = frmWorkOrder.Items.Item("Matrix1").Specific
            oMatrix2 = frmWorkOrder.Items.Item("Matrix2").Specific
            oMatrix4 = frmWorkOrder.Items.Item("Matrix4").Specific


            frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            frmWorkOrder.Items.Item("16").Specific.string = CDate(Today.Date).ToString("ddMMyyyy")
            Add_Mode()
            'AddAndDeleteRow()
            LoadCombo()
            DefineModeForFields()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("LoadScreen " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Sub DefineModeForFields()
        Try

            frmWorkOrder.Items.Item("26").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmWorkOrder.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmWorkOrder.Items.Item("RuleC").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmWorkOrder.Items.Item("Fgc").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmWorkOrder.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmWorkOrder.Items.Item("t_MCode").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmWorkOrder.Items.Item("t_MCode").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)


            frmWorkOrder.Items.Item("t_SDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmWorkOrder.Items.Item("t_SDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            frmWorkOrder.Items.Item("t_EndDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmWorkOrder.Items.Item("t_EndDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmWorkOrder.Items.Item("21").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmWorkOrder.Items.Item("21").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)


        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub Refresh(ByVal Docnum As Integer)
        Try
            If frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                sPressed = False
                frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                frmWorkOrder.Items.Item("t_DocNum").Specific.Value = Docnum
                frmWorkOrder.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            End If

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Refresh Failed" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Finally
        End Try

    End Sub

    Sub Add_Mode()
        Try

            If frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                Dim strDocNum As String = frmWorkOrder.BusinessObject.GetNextSerialNumber("-1", "AS_OWORD")
                oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                frmWorkOrder.Items.Item("t_DocNum").Enabled = False
                frmWorkOrder.Items.Item("16").Specific.string = CDate(Today.Date).ToString("ddMMyyyy")
                oMatrix.AddRow()
                oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
                oMatrix1.AddRow()
                oMatrix1.Columns.Item("V_-1").Cells.Item(oMatrix1.VisualRowCount).Specific.value = oMatrix1.VisualRowCount
                oMatrix2.AddRow()
                oMatrix2.Columns.Item("V_-1").Cells.Item(oMatrix2.VisualRowCount).Specific.value = oMatrix2.VisualRowCount

                oCombo1 = frmWorkOrder.Items.Item("26").Specific
                objAddOn.objGenFunc.setComboBoxValue(oCombo1, "select * from [@AIS_WrkTyp]")
            End If


        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Form Add_Mode " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
    Private Sub AddAndDeleteRow()
        frmWorkOrder = objAddOn.objApplication.Forms.ActiveForm

    End Sub
    Sub LoadCombo()
        'oCombo = oForm.Items.Item("1000001").Specific

        frmWorkOrder.Items.Item("1000002").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

    End Sub
    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If frmWorkOrder Is Nothing Then
                frmWorkOrder = objAddOn.objApplication.Forms.Item(FormUID)
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Form Load s " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

        If pVal.BeforeAction = True Then
            Dim lnou, totLnou, nou, pusetime, usedtime, baltime, liftime As Decimal
            Dim opcod As String
            Dim i As Integer
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        If pVal.ItemUID = "1" Then
                            If frmWorkOrder.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                Dim Itrate As Integer
                                frmWorkOrder = objAddOn.objApplication.Forms.ActiveForm
                                oMatrix = frmWorkOrder.Items.Item("Matrix").Specific
                                For Itrate = 1 To oMatrix.VisualRowCount
                                    If oMatrix.Columns.Item("OpCode").Cells.Item(Itrate).Specific.string = "" Then
                                        oMatrix.DeleteRow(Itrate)
                                    End If
                                Next
                                If Validation() = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                Else
                                    If frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                                        If oMatrix1.VisualRowCount > 0 Then
                                            totLnou = 0
                                            For i = 1 To oMatrix1.VisualRowCount
                                                lnou = oMatrix1.Columns.Item("NoU").Cells.Item(i).Specific.value
                                                totLnou = totLnou + lnou
                                            Next
                                            frmWorkOrder.Items.Item("21").Specific.value = totLnou
                                        End If
                                        nou = frmWorkOrder.Items.Item("21").Specific.value
                                        If oMatrix.VisualRowCount > 0 Then

                                        End If
                                    End If
                                End If
                            End If
                            If pVal.ItemUID = "Matrix" And pVal.ColUID = "V_-1" Then
                                current = pVal.Row
                            End If
                        End If

                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Click Event " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try

            End Select
        Else
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED

                    If pVal.ItemUID = "1000002" Then
                        frmWorkOrder.PaneLevel = 1
                        'oForm.Settings.EnableRowFormat = "Matrix"

                    ElseIf pVal.ItemUID = "28" Then
                        frmWorkOrder.PaneLevel = 2
                        'oForm.Settings.EnableRowFormat = "Matrix1"
                    ElseIf pVal.ItemUID = "29" Then
                        frmWorkOrder.PaneLevel = 3
                        '  oForm.Settings.EnableRowFormat = "Matrix2"
                    ElseIf pVal.ItemUID = "42" Then
                        frmWorkOrder.PaneLevel = 4
                        '  oForm.Settings.EnableRowFormat = "Matrix2"
                    End If

                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        Add_Mode()
                    End If
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try


                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Lost Focus " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED

                    If pVal.ItemUID = "Matrix4" And pVal.ColUID = "BN" Then

                        objAddOn.objApplication.Menus.Item("AS_OIGN").Activate()
                        Dim oFrm As SAPbouiCOM.Form = objAddOn.objApplication.Forms.ActiveForm
                        oFrm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                        oFrm.Items.Item("DocEntry").Specific.Value = oMatrix4.Columns.Item("BN").Cells.Item(pVal.Row).Specific.string
                        oFrm.Items.Item("1").Click()
                    End If
                    


                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    Try
                        objAddOn.objApplication.StatusBar.SetText("Please Update Form Loading!!!", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

                        If pVal.ItemUID = "Matrix" And pVal.ColUID = "OpName" And frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            objAddOn.objApplication.StatusBar.SetText("Please Update Form Loading. Row ID" + pVal.Row.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            If pVal.Row = 1 Then



                                objAddOn.objApplication.StatusBar.SetText("Please Update Form Loading. Row ID2" + pVal.Row.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                Dim Type, WorkORderDocNum, WorkORderDocEntry, Status, NoUnit, OpCode As String
                                Dim LineID As Integer
                                Dim sStandardCycleTime As String = String.Empty
                                Dim Cost As Double = 0
                                OpCode = oMatrix.Columns.Item("OpCode").Cells.Item(pVal.Row).Specific.string
                                Type = oMatrix.Columns.Item("Process").Cells.Item(pVal.Row).Specific.string
                                Status = oMatrix.Columns.Item("Status").Cells.Item(pVal.Row).Specific.string
                                sStandardCycleTime = oMatrix.Columns.Item("Cycle").Cells.Item(pVal.Row).Specific.string().Trim

                                WorkORderDocNum = frmWorkOrder.Items.Item("t_DocNum").Specific.string
                                WorkORderDocEntry = frmWorkOrder.Items.Item("DocEntry").Specific.string
                                LineID = oMatrix.Columns.Item("V_-1").Cells.Item(pVal.Row).Specific.string
                                NoUnit = frmWorkOrder.Items.Item("21").Specific.string


                                Dim sOperationType As SAPbouiCOM.ComboBox = frmWorkOrder.Items.Item("26").Specific

                                Dim oRS As SAPbobsCOM.Recordset
                                oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                Dim squery As String = "EXEC DBO.[@AIS_Production_ProductionReceipt_GetFGItemDetails] '" & OpCode & "','" & sOperationType.Selected.Value.ToString() & "','" + pVal.Row.ToString() + "','" + WorkORderDocEntry + "'"
                                oRS.DoQuery(squery)
                                oRS.MoveFirst()
                                Dim IntValidation As Integer = 0
                                For IntValidation = 1 To oRS.RecordCount

                                    If oRS.Fields.Item("U_InsReq").Value = "Y" Then

                                        If oMatrix2.VisualRowCount = 0 Then

                                            objAddOn.objApplication.StatusBar.SetText("Already Issue AND Receipt is Created at Line No." + pVal.Row.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                            Return
                                        End If

                                        If oMatrix2.VisualRowCount >= 1 Then

                                            If oMatrix2.Columns.Item("DoffNo").Cells.Item(1).Specific.string = String.Empty Then
                                                objAddOn.objApplication.StatusBar.SetText("Doff Details Should not be Empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                                Return
                                            End If

                                        End If
                                    End If

                                Next


                                If Status = "C" Then
                                    objAddOn.objApplication.StatusBar.SetText("Already Issue AND Receipt is Created at Line No." + pVal.Row.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                    Return
                                End If
                                For I As Integer = LineID - 1 To 1 Step -1
                                    If oMatrix.Columns.Item("Process").Cells.Item(I).Specific.string <> "P" Then
                                        Exit For
                                    Else
                                        Cost += oMatrix.Columns.Item("V_2").Cells.Item(I).Specific.value
                                    End If
                                Next

                                Dim sFGCode As String = frmWorkOrder.Items.Item("Fgc").Specific.value
                                Dim sFGName As String = frmWorkOrder.Items.Item("Fgn").Specific.value
                                Dim sRuleC As String = frmWorkOrder.Items.Item("RuleC").Specific.value
                                Dim sSalesOrderNo As String = String.Empty
                                Dim sSalesOrderEntry As String = String.Empty
                                Dim sCustomerCode As String = String.Empty
                                If oMatrix1.VisualRowCount > 0 Then
                                    sSalesOrderNo = oMatrix1.Columns.Item("BaseNum").Cells.Item(1).Specific.string
                                    sSalesOrderEntry = oMatrix1.Columns.Item("BaseEntry").Cells.Item(1).Specific.string
                                    sCustomerCode = oMatrix1.Columns.Item("CardCode").Cells.Item(1).Specific.string
                                Else
                                    sSalesOrderNo = String.Empty
                                    sSalesOrderEntry = String.Empty
                                    sCustomerCode = String.Empty

                                End If

                                If Type = "PA" Then
                                    objAddOn.objProductionReceipt.LoadScreen(2, LineID, WorkORderDocNum, Status, Cost, NoUnit, OpCode, sFGCode, sFGName, sRuleC, sOperationType.Selected.Value.ToString(), sStandardCycleTime, sSalesOrderNo, sSalesOrderEntry, sCustomerCode, WorkORderDocEntry)
                                ElseIf Type = "R" Then
                                    objAddOn.objProductionReceipt.LoadScreen(1, LineID, WorkORderDocNum, Status, Cost, NoUnit, OpCode, sFGCode, sFGName, sRuleC, sOperationType.Selected.Value.ToString(), sStandardCycleTime, sSalesOrderNo, sSalesOrderEntry, sCustomerCode, WorkORderDocEntry)
                                End If
                                objAddOn.objApplication.StatusBar.SetText("Form Loaded", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            Else
                                objAddOn.objApplication.StatusBar.SetText("Please Update Form Loading. Row ID3" + pVal.Row.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                Dim Type, WorkORderDocNum, WorkORderDocEntry, Status, NoUnit, LotNo, OpCode As String
                                Dim LineID As Integer
                                Dim Cost As Double = 0
                                OpCode = oMatrix.Columns.Item("OpCode").Cells.Item(pVal.Row).Specific.string
                                Type = oMatrix.Columns.Item("Process").Cells.Item(pVal.Row).Specific.string
                                Status = oMatrix.Columns.Item("Status").Cells.Item(pVal.Row).Specific.string
                                If Status = "C" Then
                                    objAddOn.objApplication.StatusBar.SetText("Already Issue AND Receipt is Created at Line No." + pVal.Row.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                    Return
                                End If

                                WorkORderDocNum = frmWorkOrder.Items.Item("t_DocNum").Specific.string
                                WorkORderDocEntry = frmWorkOrder.Items.Item("DocEntry").Specific.string
                                LineID = oMatrix.Columns.Item("V_-1").Cells.Item(pVal.Row).Specific.string
                                NoUnit = frmWorkOrder.Items.Item("21").Specific.string

                                For I As Integer = LineID - 1 To 1 Step -1
                                    If oMatrix.Columns.Item("Process").Cells.Item(I).Specific.string <> "P" Then
                                        Exit For
                                    Else
                                        Cost += oMatrix.Columns.Item("V_2").Cells.Item(I).Specific.value
                                    End If
                                Next
                                Dim sFGCode As String = frmWorkOrder.Items.Item("Fgc").Specific.value
                                Dim sFGName As String = frmWorkOrder.Items.Item("Fgn").Specific.value
                                Dim sRuleC As String = frmWorkOrder.Items.Item("RuleC").Specific.value
                                Dim sOperationType As SAPbouiCOM.ComboBox = frmWorkOrder.Items.Item("26").Specific

                                Dim sStandardCycleTime As String = oMatrix.Columns.Item("Cycle").Cells.Item(pVal.Row).Specific.string

                                oMatrix1 = frmWorkOrder.Items.Item("Matrix1").Specific
                                Dim sSalesOrderNo As String = String.Empty
                                Dim sSalesOrderEntry As String = String.Empty
                                Dim sCustomerCode As String = String.Empty
                                If oMatrix1.VisualRowCount > 0 Then
                                    sSalesOrderNo = oMatrix1.Columns.Item("BaseNum").Cells.Item(1).Specific.string
                                    sSalesOrderEntry = oMatrix1.Columns.Item("BaseEntry").Cells.Item(1).Specific.string
                                    sCustomerCode = oMatrix1.Columns.Item("CardCode").Cells.Item(1).Specific.string
                                Else
                                    sSalesOrderNo = String.Empty
                                    sSalesOrderEntry = String.Empty
                                    sCustomerCode = String.Empty

                                End If

                                If Type = "PA" Then
                                    objAddOn.objProductionReceipt.LoadScreen(2, LineID, WorkORderDocNum, Status, Cost, NoUnit, OpCode, sFGCode, sFGName, sRuleC, sOperationType.Selected.Value.ToString(), sStandardCycleTime, sSalesOrderNo, sSalesOrderEntry, sCustomerCode, WorkORderDocEntry)
                                ElseIf Type = "R" Then
                                    objAddOn.objProductionReceipt.LoadScreen(1, LineID, WorkORderDocNum, Status, Cost, NoUnit, OpCode, sFGCode, sFGName, sRuleC, sOperationType.Selected.Value.ToString(), sStandardCycleTime, sSalesOrderNo, sSalesOrderEntry, sCustomerCode, WorkORderDocEntry)
                                End If
                                objAddOn.objApplication.StatusBar.SetText("Form Loaded", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                ' End If
                            End If

                        End If
                        If pVal.ItemUID = "Matrix" And pVal.ColUID = "V_5" And frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            objAddOn.objApplication.StatusBar.SetText("Please Update Form Loading. Row ID5" + pVal.Row.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

                            Dim opcod, opnam, wono As String
                            wono = frmWorkOrder.Items.Item("t_DocNum").Specific.string
                            opcod = oMatrix.Columns.Item("OpCode").Cells.Item(pVal.Row).Specific.string
                            opnam = oMatrix.Columns.Item("OpName").Cells.Item(pVal.Row).Specific.string
                            'objAddOn.objQltyParam.LoadScreen(opcod, opnam, wono)
                            If objAddOn.objQltyParam.LoadScreen(opcod, opnam, wono) = True Then
                                oMatrix.Columns.Item("V_5").Cells.Item(pVal.Row).Specific.value = "C"
                            End If
                            frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                        End If

                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("Double Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Select Case oCFLE.ChooseFromListUID
                            Case "CFL_RES"

                                If pVal.Row - 1 > 0 Then
                                    Dim c_Status As SAPbouiCOM.ComboBox = oMatrix2.Columns.Item("Status").Cells.Item(pVal.Row - 1).Specific
                                    If c_Status.Selected Is Nothing Then
                                        objAddOn.objApplication.StatusBar.SetText("Previous Quality Status Is Pending", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                        Return
                                    End If
                                End If




                              



                                oMatrix2.FlushToDataSource()
                                ODBDSource3.SetValue("U_MCCode", pVal.Row - 1, Trim(oDataTable.GetValue("ResCode", 0)))
                                ODBDSource3.SetValue("U_MCName", pVal.Row - 1, Trim(oDataTable.GetValue("ResName", 0)))
                                oMatrix2.LoadFromDataSource()
                                objAddOn.SetNewLine(oMatrix2, ODBDSource3)
                                If frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                    frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                End If

                            Case "CFL_MCode"
                                oDBDSHeader.SetValue("U_MachineCode", 0, oDataTable.GetValue("ResCode", 0))
                                oDBDSHeader.SetValue("U_MachineName", 0, oDataTable.GetValue("ResName", 0))
                                Dim sQuery As String = String.Empty

                                'Dim ItemCode As String = String.Empty
                                'sQuery = " Select ItemCode  from OITM Where ItemName in (Select isnull(U_GN ,'')    from OITM Where ItemCode in(   '" & Trim(oDBDSHeader.GetValue("U_FGCode", 0)) & "') )"
                                'Dim rsetEmpDets_MachingCapacity As SAPbobsCOM.Recordset = objAddOn.DoQuery(sQuery)
                                'If rsetEmpDets_MachingCapacity.RecordCount > 0 Then
                                '    ItemCode = Trim(rsetEmpDets_MachingCapacity.Fields.Item("ItemCode").Value)
                                'End If
                                ' as per Mr.Rajadhurai Instruction , Rengan Removed

                                '''' Remvoed by renan
                                'sQuery = "EXEC DBO.[@AIS_PPC_GetMachineCycleTime] '" + Trim(oDBDSHeader.GetValue("U_FGCode", 0)) + "','" + Trim(oDataTable.GetValue("ResCode", 0)) + "','" + Trim(oDBDSHeader.GetValue("U_ProducedQty", 0)) + "'"
                                'Dim rsetEmpDets As SAPbobsCOM.Recordset = objAddOn.DoQuery(sQuery)
                                'If rsetEmpDets.RecordCount > 0 Then
                                '    Dim sQ As String = Trim(rsetEmpDets.Fields.Item("ReqHrs").Value)
                                '    oDBDSHeader.SetValue("U_RequiredHrs", 0, sQ)
                                'Else
                                '    oDBDSHeader.SetValue("U_RequiredHrs", 0, "0")
                                'End If


                                Dim sOperationCode As String = String.Empty

                                Dim U_FGCode As String = Trim(oDBDSHeader.GetValue("U_FGCode", 0))

                                Str = "Select U_OperCode,U_OperName,U_MCCode,U_MCName, T2.U_Process ,T2.U_Cycle    from [@AIS_OIOD]  T0 Inner join  [@AIS_IOD1]   T1 On T0.DocEntry =T1.DocEntry  Inner Join [@AS_OPRN] T2 On T2.Code =T1.U_OperCode   Where Isnull(U_OperCode,'')!=''  AND   T1.U_MCCode ='" + oDataTable.GetValue("ResCode", 0) + "' AND  T0.U_ItemCode ='" + U_FGCode + "'"
                                Ors = objAddOn.DoQuery(Str)

                                Dim i, k As Integer

                                If oMatrix.VisualRowCount > 0 Then
                                    For i = 1 To oMatrix.VisualRowCount
                                        oMatrix.DeleteRow(oMatrix.VisualRowCount)
                                        oDBDSLine.Clear()
                                    Next
                                End If
                                Dim dPlannedqty As Double = 0
                                If frmWorkOrder.Items.Item("21").Specific.value <> "" Then
                                    dPlannedqty = Convert.ToDouble(frmWorkOrder.Items.Item("21").Specific.value)
                                End If

                                Ors = objAddOn.DoQuery(Str)
                                If Ors.RecordCount > 0 Then
                                    For k = 0 To Ors.RecordCount - 1
                                        objAddOn.SetNewLine(oMatrix, oDBDSLine)
                                        oMatrix.FlushToDataSource()

                                        If k = 0 Then
                                            sOperationCode = Ors.Fields.Item("U_OperCode").Value
                                        End If
                                        oDBDSLine.SetValue("U_OpCode", k, Ors.Fields.Item("U_OperCode").Value)
                                        oDBDSLine.SetValue("U_OpName", k, Ors.Fields.Item("U_OperName").Value)
                                        oDBDSLine.SetValue("U_PCyc", k, Ors.Fields.Item("U_Cycle").Value)
                                        Dim LineQty As Double = 0
                                        LineQty = dPlannedqty * Convert.ToDouble(Ors.Fields.Item("U_Cycle").Value)
                                        oDBDSLine.SetValue("U_PCycle", k, LineQty.ToString())
                                        oDBDSLine.SetValue("U_Process", k, Ors.Fields.Item("U_Process").Value)


                                        oMatrix.LoadFromDataSourceEx()
                                        Ors.MoveNext()
                                    Next
                                End If

                                objAddOn.SetNewLine(oMatrix, oDBDSLine)






                                sQuery = "EXEC DBO.[@AIS_PPC_GetMachineCycleTime] '" + sOperationCode + "','" + Trim(oDBDSHeader.GetValue("U_ProducedQty", 0)) + "'"
                                Dim rsetEmpDets As SAPbobsCOM.Recordset = objAddOn.DoQuery(sQuery)
                                If rsetEmpDets.RecordCount > 0 Then
                                    Dim sQ As String = Trim(rsetEmpDets.Fields.Item("ReqHrs").Value)
                                    oDBDSHeader.SetValue("U_RequiredHrs", 0, sQ)
                                Else
                                    oDBDSHeader.SetValue("U_RequiredHrs", 0, "0")
                                End If



                            Case "Cfl_fg"
                                oDBDSHeader.SetValue("U_FGCode", 0, oDataTable.GetValue("ItemCode", 0))
                                oDBDSHeader.SetValue("U_FgName", 0, oDataTable.GetValue("ItemName", 0))
                                Dim RouteCode As String = Trim(oDBDSHeader.GetValue("U_RuleC", 0))






                            Case "CFL_SO"
                                oMatrix1.FlushToDataSource()
                                ODBDSLine2.SetValue("U_BaseNum", pVal.Row - 1, Trim(oDataTable.GetValue("DocNum", 0)))
                                ODBDSLine2.SetValue("U_BaseEntry", pVal.Row - 1, Trim(oDataTable.GetValue("DocEntry", 0)))
                                ODBDSLine2.SetValue("U_CardCode", pVal.Row - 1, Trim(oDataTable.GetValue("CardCode", 0)))
                                ODBDSLine2.SetValue("U_CardName", pVal.Row - 1, Trim(oDataTable.GetValue("CardName", 0)))



                                Dim Ors_SalesOrder As SAPbobsCOM.Recordset
                                Dim StrQuery As String = "Select Quantity,T0.ItemCode ,ItemName   From RDR1 T0 Inner Join OITM T1 on T0.ItemCode =T1.ItemCode  Where T0.DocEntry  =  '" & Trim(oDataTable.GetValue("DocEntry", 0)) & "'"
                                Ors_SalesOrder = objAddOn.DoQuery(StrQuery)
                                oDBDSHeader.SetValue("U_FGCode", 0, Ors_SalesOrder.Fields.Item("ItemCode").Value)
                                oDBDSHeader.SetValue("U_FgName", 0, Ors_SalesOrder.Fields.Item("ItemName").Value)
                                oDBDSHeader.SetValue("U_SalesOrderQty", 0, Ors_SalesOrder.Fields.Item("Quantity").Value)

                                ODBDSLine2.SetValue("U_TotalQty", pVal.Row - 1, Ors_SalesOrder.Fields.Item("Quantity").Value)
                                ODBDSLine2.SetValue("U_PendingQty", pVal.Row - 1, Ors_SalesOrder.Fields.Item("Quantity").Value)


                                oMatrix1.LoadFromDataSource()
                                oMatrix1.Columns.Item("BaseNum").Cells.Item(pVal.Row).Click()
                                objAddOn.SetNewLine(oMatrix1, ODBDSLine2)



                            Case "CFL_sal"
                                oDBDSHeader.SetValue("U_SONum", 0, oDataTable.GetValue("DocNum", 0))
                                oDBDSHeader.SetValue("U_CustName", 0, oDataTable.GetValue("CardName", 0))
                            Case "Cfl_Oprn"
                                oMatrix.FlushToDataSource()
                                oDBDSLine.SetValue("U_OpCode", pVal.Row - 1, oDataTable.GetValue("Code", 0))
                                oDBDSLine.SetValue("U_OpName", pVal.Row - 1, oDataTable.GetValue("Name", 0))
                                oDBDSLine.SetValue("U_PCyc", pVal.Row - 1, oDataTable.GetValue("U_Cycle", 0))
                                'asdasd

                                Dim dPlannedqty As Double = 0
                                If frmWorkOrder.Items.Item("21").Specific.value <> "" Then
                                    dPlannedqty = Convert.ToDouble(frmWorkOrder.Items.Item("21").Specific.value)
                                End If
                                Dim LineQty As Double = 0
                                LineQty = dPlannedqty * Convert.ToDouble(oDataTable.GetValue("U_Cycle", 0))
                                oDBDSLine.SetValue("U_PCycle", pVal.Row - 1, LineQty.ToString())

                                oDBDSLine.SetValue("U_Process", pVal.Row - 1, oDataTable.GetValue("U_Process", 0))
                                oDBDSLine.SetValue("U_Status", pVal.Row - 1, "O")

                                oMatrix.LoadFromDataSource()
                                objAddOn.SetNewLine(oMatrix, oDBDSLine)

                            Case "Cfl_Rule"
                                DocEntry = oDataTable.GetValue("DocEntry", 0)
                                Str = "Select U_RuleC ,U_RuleN ,U_FGCode ,U_FgName   from [@AS_ROOT] where DocEntry = '" & DocEntry & "'"
                                Ors = objAddOn.DoQuery(Str)
                                oDBDSHeader.SetValue("U_RuleC", 0, Ors.Fields.Item("U_RuleC").Value)
                                oDBDSHeader.SetValue("U_RuleN", 0, Ors.Fields.Item("U_RuleN").Value)
                                oDBDSHeader.SetValue("U_FGCode", 0, Ors.Fields.Item("U_FGCode").Value)
                                oDBDSHeader.SetValue("U_FgName", 0, Ors.Fields.Item("U_FgName").Value)
                                oMatrix.LoadFromDataSourceEx()
                                matrix_load()
                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Cfl " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try



            End Select

        End If

    End Sub

#Region "Function For Checking the Quality Wheather Accepted or Not"
    Public Function Quality_Status_Checking(ByVal FormUID As String, ByVal LineID As Integer)
        Try
            For K As Integer = LineID - 1 To 1 Step -1
                Dim ocm As SAPbouiCOM.ComboBox
                ocm = oMatrix.Columns.Item("V_3").Cells.Item(K).Specific
                If ocm.Selected.Value <> "A" Then
                    Return False
                End If
                '  MsgBox(oMatrix.Columns.Item("V_3").Cells.Item(K).Specific.string)
            Next
        Catch ex As Exception
            ' objAddOn.objApplication.StatusBar.SetText("Quality Status Checking Function Failure -" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)

        End Try
        Return True
    End Function
#End Region

    Public Sub matrix_load()
        Dim i, k As Integer
        If oMatrix.VisualRowCount > 0 Then
            For i = 1 To oMatrix.VisualRowCount
                oMatrix.DeleteRow(oMatrix.VisualRowCount)
                oDBDSLine.Clear()
            Next
        End If
        Dim dPlannedqty As Double = 0
        If frmWorkOrder.Items.Item("21").Specific.value <> "" Then
            dPlannedqty = Convert.ToDouble(frmWorkOrder.Items.Item("21").Specific.value)

        End If
        Str = "Select U_OpCode ,U_OpName ,U_Cycle ,T0.U_Process  from [@AS_ROOT1] T0 Inner Join [@AS_OPRN] T1 On T0.U_OpCode =T1.Code AND T0.U_Process =T1.U_Process   where T0.DocEntry =  '" & DocEntry & "'"
        Ors = objAddOn.DoQuery(Str)
        If Ors.RecordCount > 0 Then
            For k = 0 To Ors.RecordCount - 1
                objAddOn.SetNewLine(oMatrix, oDBDSLine)
                oMatrix.FlushToDataSource()
                oDBDSLine.SetValue("U_OpCode", k, Ors.Fields.Item("U_OpCode").Value)
                oDBDSLine.SetValue("U_OpName", k, Ors.Fields.Item("U_OpName").Value)
                oDBDSLine.SetValue("U_PCyc", k, Ors.Fields.Item("U_Cycle").Value)
                Dim LineQty As Double = 0

                LineQty = dPlannedqty * Convert.ToDouble(Ors.Fields.Item("U_Cycle").Value)

                oDBDSLine.SetValue("U_PCycle", k, LineQty.ToString())

                oDBDSLine.SetValue("U_Process", k, Ors.Fields.Item("U_Process").Value)
                oMatrix.LoadFromDataSourceEx()
                Ors.MoveNext()
            Next
        End If

    End Sub

    Function Validation()

        'If oForm.Items.Item("Name").Specific.String = "" Then
        '    objAddOn.objApplication.SetStatusBarMessage("Please Select Name", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        '    Return False
        'End If
        If frmWorkOrder.Items.Item("16").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select Date", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return False
        End If
        If oMatrix.VisualRowCount > 0 Then
            Dim i As Integer
            For i = 1 To oMatrix.VisualRowCount - 1
                If oMatrix.Columns.Item("OpCode").Cells.Item(i).Specific.String = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Opcode Should Not be Empty " & i & "", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    Return False
                End If
            Next
        End If
        Return True
    End Function



    'Sub RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
    '    Try
    '        Select Case eventInfo.ItemUID
    '            Case "Matrix"
    '                'oMatrix = oForm.Items.Item("3").Specific
    '                If eventInfo.Row <> oMatrix.VisualRowCount And _
    '                    eventInfo.BeforeAction Then
    '                    'oMatrix.GetCellSpecific("U_SeqNo", eventInfo.Row).Value.ToString.Trim <> "" And _
    '                    'frmUOM.Items.Item("6").Specific.Value.ToString.Trim = "P" Then
    '                    'objAddOn.setRightMenu("Add", "Add")
    '                    objAddOn.setRightMenu("Add", "Add")
    '                    objAddOn.setRightMenu("Delete", "Delete")
    '                    objAddOn.setRightMenu("1292", "Add Row")
    '                    objAddOn.setRightMenu("1293", "Delete Row")
    '                    'objAddOn.setRightMenu(BOMRawMaterialFormId, "Raw Material")
    '                    'objAddOn.setRightMenu(BOMMachineryFormId, "Machinery")
    '                    'objAddOn.setRightMenu(BOMToolsFormId, "Tools")
    '                    'objAddOn.setRightMenu(BOMScrapFormId, "Scrap")
    '                    'objAddOn.setRightMenu(BOMConsumablesFormId, "Consumables")
    '                    'objAddOn.setRightMenu(BOMLaborFormId, "Labor")
    '                    'CurrentRow = oMatrix.GetCellSpecific("U_SeqNo", eventInfo.Row).Value

    '                End If

    '        End Select
    '    Catch ex As Exception
    '        objAddOn.objApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
    '    End Try
    'End Sub
    Public Sub FormDataEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)

        Try
            frmWorkOrder = objAddOn.objApplication.Forms.Item(FormUID)

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("ImportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
        Select Case pVal.EventType
            Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                '
                Dim Lcom As SAPbouiCOM.ComboBox
                Try
                    Lcom = frmWorkOrder.Items.Item("c_Status").Specific
                    If Lcom.Selected.Value = "C" Then
                        frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                    Else

                        If frmWorkOrder.Items.Item("Canceled").Specific.value = "Y" Then
                            frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                        Else
                            frmWorkOrder.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                            oMatrix2 = frmWorkOrder.Items.Item("Matrix2").Specific
                            oMatrix2.AddRow()
                            oMatrix2.Columns.Item("V_-1").Cells.Item(oMatrix2.VisualRowCount).Specific.value = oMatrix2.VisualRowCount
                        End If


                    End If


                    Dim oMatrix_Disablew As SAPbouiCOM.Matrix
                    Dim oRowCtrl As SAPbouiCOM.CommonSetting
                    oMatrix_Disablew = frmWorkOrder.Items.Item("Matrix2").Specific
                    oRowCtrl = oMatrix_Disablew.CommonSetting()

                    Dim sRejected As Boolean = False
                    Dim i As Integer = 0
                    For i = 1 To oMatrix_Disablew.VisualRowCount
                        Dim c_Status As SAPbouiCOM.ComboBox = oMatrix_Disablew.Columns.Item("Status").Cells.Item(i).Specific
                        If Not c_Status.Selected Is Nothing Then
                            If c_Status.Selected.Value = "A" Then
                                oRowCtrl.SetRowEditable(i, False)
                            End If
                            If c_Status.Selected.Value = "R" Then
                                oRowCtrl.SetRowEditable(i, False)
                                sRejected = True
                            End If
                            If c_Status.Selected.Value = "W" Then
                                oRowCtrl.SetRowEditable(i, True)
                                oMatrix_Disablew.Columns.Item("MCName").Editable = False
                                oMatrix_Disablew.Columns.Item("Reason").Editable = False
                                oMatrix_Disablew.Columns.Item("QBNum").Editable = False
                                oMatrix_Disablew.Columns.Item("QBEntry").Editable = False
                                oMatrix_Disablew.Columns.Item("Status").Editable = False
                                oMatrix_Disablew.Columns.Item("LStatus").Editable = False
                            End If

                            If sRejected = True Then
                                oRowCtrl.SetRowEditable(i, False)
                            End If
                        End If
                    Next

                Catch ex As Exception
                    objAddOn.objApplication.SetStatusBarMessage("et_FORM_DATA_LOAD  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)

                End Try

        End Select
    End Sub
    Sub Menu_event(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Select Case pVal.MenuUID
            Case "1283"
                'Try
                '    Dim ItemCode As String = String.Empty
                '    oWorkOrder = objAddOn.objApplication.Forms.ActiveForm
                '    Dim sQuery = " [@AIS_WorkOrder_CancelAllDocument]  '" & oWorkOrder.Items.Item("DocEntry").Specific.value.ToString() & "'"
                '    objAddOn.DoQuery(sQuery)
                '    objAddOn.objApplication.StatusBar.SetText("Remove All Base Documents Deleted:", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                '    objAddOn.objApplication.StatusBar.SetText("Remove All Base Document:" & sQuery, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                'Catch ex As Exception
                '    objAddOn.objApplication.StatusBar.SetText("Remove All Base Document:" & ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

                'End Try


            Case "1292"
                frmWorkOrder = objAddOn.objApplication.Forms.ActiveForm
                If frmWorkOrder.TypeEx = "WORDER" Then
                    Try
                        Dim oEdit, otext As SAPbouiCOM.EditText
                        frmWorkOrder = objAddOn.objApplication.Forms.ActiveForm
                        oMatrix = frmWorkOrder.Items.Item("Matrix").Specific
                        frmWorkOrder.DataSources.DBDataSources.Item("@AS_WORD1").Clear()
                        If oMatrix.VisualRowCount = 0 Then
                            oMatrix.AddRow(1, pVal.row)
                            oMatrix = frmWorkOrder.Items.Item("Matrix").Specific
                            oEdit = oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific
                            oEdit.Value = oMatrix.VisualRowCount


                        Else
                            oMatrix = frmWorkOrder.Items.Item("Matrix").Specific
                            otext = oMatrix.Columns.Item("OpCode").Cells.Item(oMatrix.VisualRowCount).Specific
                            If otext.Value <> "" Then
                                oMatrix = frmWorkOrder.Items.Item("Matrix").Specific
                                Dim iTast As Integer

                                For iTast = 1 To oMatrix.VisualRowCount
                                    If oMatrix.IsRowSelected(iTast) = True Then
                                        Exit For
                                    End If
                                Next

                                oMatrix.AddRow(1, iTast)
                                oMatrix = frmWorkOrder.Items.Item("Matrix").Specific
                                oEdit = oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific
                                For iTast = 1 To oMatrix.VisualRowCount
                                    oEdit = oMatrix.Columns.Item("V_-1").Cells.Item(iTast).Specific
                                    oEdit.Value = iTast
                                Next
                                'oEdit.Value = oMatrix.VisualRowCount

                            Else
                                Exit Sub
                            End If
                        End If

                        oMatrix1 = frmWorkOrder.Items.Item("Lot").Specific
                        'oWorkOrder.DataSources.DBDataSources.Item("@AS_WORD2").Clear()
                        'If oMatrix1.VisualRowCount = 0 Then
                        '    oMatrix1.AddRow(1, pVal.row)
                        '    oMatrix1 = oWorkOrder.Items.Item("Lot").Specific
                        '    oEdit = oMatrix1.Columns.Item("V_-1").Cells.Item(oMatrix1.VisualRowCount).Specific
                        '    oEdit.Value = oMatrix1.VisualRowCount


                        'Else
                        '    oMatrix1 = oWorkOrder.Items.Item("Lot").Specific
                        '    otext = oMatrix1.Columns.Item("LotNo").Cells.Item(oMatrix1.VisualRowCount).Specific
                        '    If otext.Value <> "" Then
                        '        oMatrix1 = oWorkOrder.Items.Item("Lot").Specific
                        '        Dim iTast As Integer

                        '        For iTast = 1 To oMatrix1.VisualRowCount
                        '            If oMatrix1.IsRowSelected(iTast) = True Then
                        '                Exit For
                        '            End If
                        '        Next

                        '        oMatrix1.AddRow(1, iTast)
                        '        oMatrix1 = oWorkOrder.Items.Item("Lot").Specific
                        '        oEdit = oMatrix1.Columns.Item("V_-1").Cells.Item(oMatrix1.VisualRowCount).Specific
                        '        For iTast = 1 To oMatrix1.VisualRowCount
                        '            oEdit = oMatrix1.Columns.Item("V_-1").Cells.Item(iTast).Specific
                        '            oEdit.Value = iTast
                        '        Next
                        '        'oEdit.Value = oMatrix.VisualRowCount

                        '    Else
                        '        Exit Sub
                        '    End If
                        'End If

                    Catch ex As Exception
                        'SBO_Application.MessageBox(ex.Message)
                    End Try

                End If
            Case "1293"
                oMatrix = frmWorkOrder.Items.Item("Matrix").Specific
                oMatrix1 = frmWorkOrder.Items.Item("Lot").Specific
                objAddOn.DeleteRow(oMatrix, oDBDSLine)
                '    ' Dim otext As SAPbouiCOM.EditText
                '    oForm = objAddOn.objApplication.Forms.ActiveForm
                '    If oForm.UniqueID = "PARTS" Then
                '        Try
                '            oForm = objAddOn.objApplication.Forms.Item("PARTS")
                '            oMatrix1 = oForm.Items.Item("Matrix").Specific
                '            'oMatrixL = frmwioupdate.Items.Item("16").Specific
                '            oMatrix1.DeleteRow(oMatrix1.VisualRowCount)

                '        Catch ex As Exception

                '        End Try
                '    End If

        End Select

        If pVal.MenuUID = "1282" Then
            Add_Mode()
        ElseIf pVal.MenuUID = "1281" Then
            frmWorkOrder.Items.Item("t_DocNum").Enabled = True
        Else
            frmWorkOrder.Items.Item("t_DocNum").Enabled = False
            'oForm.Items.Item("Code").Enabled = False
        End If

        'If current <> "0" And pVal.BeforeAction = False Then
        '    Dim Lcom As SAPbouiCOM.ComboBox
        '    Try
        '        Lcom = oMatrix.Columns.Item("Status").Cells.Item(current).Specific
        '    Catch ex As Exception

        '    End Try

        '    If pVal.MenuUID = "Delete" And oMatrix.Columns.Item("Status").Cells.Item(current).Specific.string = "O" Then
        '        Dim i As Integer
        '        For i = current To oMatrix.VisualRowCount - 1
        '            oMatrix.FlushToDataSource()
        '            oDBDSLine.SetValue("U_OpCode", i - 1, oDBDSLine.GetValue("U_OpCode", i))
        '            oDBDSLine.SetValue("U_OpName", i - 1, oDBDSLine.GetValue("U_OpName", i))
        '            oDBDSLine.SetValue("U_PCyc", i - 1, oDBDSLine.GetValue("U_PCyc", i))
        '            oDBDSLine.SetValue("U_Labour", i - 1, oDBDSLine.GetValue("U_Labour", i))
        '            oDBDSLine.SetValue("LineId", i - 1, i)
        '            oMatrix.LoadFromDataSourceEx()
        '        Next
        '        oMatrix.LoadFromDataSource()
        '        oDBDSLine.SetValue("U_OpCode", oMatrix.VisualRowCount - 1, "")
        '        oMatrix.LoadFromDataSourceEx()
        '        objAddOn.DeleteEmptyRowInFormDataEvent(oMatrix, "OpCode", oDBDSLine)
        '        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
        '        'objAddOn.DeleteEmptyRowInFormDataEvent(oMatrix, "OpCode", oDBDSLine)
        '    End If
        '    If pVal.MenuUID = "1293" And oMatrix.Columns.Item("Status").Cells.Item(current).Specific.string = "O" Then
        '        Dim i As Integer
        '        For i = current To oMatrix.VisualRowCount - 1
        '            oMatrix.FlushToDataSource()
        '            oDBDSLine.SetValue("U_OpCode", i - 1, oDBDSLine.GetValue("U_OpCode", i))
        '            oDBDSLine.SetValue("U_OpName", i - 1, oDBDSLine.GetValue("U_OpName", i))
        '            oDBDSLine.SetValue("U_PCyc", i - 1, oDBDSLine.GetValue("U_PCyc", i))
        '            oDBDSLine.SetValue("U_Labour", i - 1, oDBDSLine.GetValue("U_Labour", i))
        '            oDBDSLine.SetValue("LineId", i - 1, i)
        '            oMatrix.LoadFromDataSourceEx()
        '        Next
        '        oMatrix.LoadFromDataSource()
        '        oDBDSLine.SetValue("U_OpCode", oMatrix.VisualRowCount - 1, "")
        '        oMatrix.LoadFromDataSourceEx()
        '        objAddOn.DeleteEmptyRowInFormDataEvent(oMatrix, "OpCode", oDBDSLine)
        '        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
        '        'objAddOn.DeleteEmptyRowInFormDataEvent(oMatrix, "OpCode", oDBDSLine)
        '    End If
        '    If pVal.MenuUID = "Add" And (oMatrix.Columns.Item("Status").Cells.Item(current).Specific.string = "O") Then
        '        objAddOn.SetNewLine(oMatrix, oDBDSLine, current + 1)
        '        Dim i As Integer
        '        For i = oMatrix.VisualRowCount To current + 1 Step -1

        '            If current + 1 = i Then
        '                oMatrix.FlushToDataSource()
        '                oDBDSLine.SetValue("U_OpCode", i - 1, "")
        '                oDBDSLine.SetValue("U_OpName", i - 1, "")
        '                oDBDSLine.SetValue("U_PCyc", i - 1, 0)
        '                oDBDSLine.SetValue("U_Labour", i - 1, 0)
        '                oDBDSLine.SetValue("LineId", i - 1, i)
        '                oMatrix.LoadFromDataSourceEx()
        '            ElseIf current <> i Then
        '                oMatrix.FlushToDataSource()
        '                oDBDSLine.SetValue("U_OpCode", i - 1, oDBDSLine.GetValue("U_OpCode", i - 2))
        '                oDBDSLine.SetValue("U_OpName", i - 1, oDBDSLine.GetValue("U_OpName", i - 2))
        '                oDBDSLine.SetValue("U_PCyc", i - 1, oDBDSLine.GetValue("U_PCyc", i - 2))
        '                oDBDSLine.SetValue("U_Labour", i - 1, oDBDSLine.GetValue("U_Labour", i - 2))
        '                oDBDSLine.SetValue("LineId", i - 1, i)
        '                oMatrix.LoadFromDataSourceEx()
        '            End If

        '        Next
        '        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
        '        'objAddOn.DeleteEmptyRowInFormDataEvent(oMatrix, "OpCode", oDBDSLine)
        '    End If
        '    'If pVal.MenuUID = "1292" And (oMatrix.Columns.Item("Status").Cells.Item(current).Specific.string = "O") Then
        '    If pVal.MenuUID = "1292" Then
        '        'objAddOn.SetNewLine(oMatrix, oDBDSLine, current + 1)
        '        'Dim i As Integer
        '        'For i = oMatrix.VisualRowCount To current + 1 Step -1

        '        '    If current + 1 = i Then
        '        '        oMatrix.FlushToDataSource()
        '        '        oDBDSLine.SetValue("U_OpCode", i - 1, "")
        '        '        oDBDSLine.SetValue("U_OpName", i - 1, "")
        '        '        oDBDSLine.SetValue("U_PCyc", i - 1, 0)
        '        '        oDBDSLine.SetValue("U_Labour", i - 1, 0)
        '        '        oDBDSLine.SetValue("LineId", i - 1, i)
        '        '        oMatrix.LoadFromDataSourceEx()
        '        '    ElseIf current <> i Then
        '        '        oMatrix.FlushToDataSource()
        '        '        oDBDSLine.SetValue("U_OpCode", i - 1, oDBDSLine.GetValue("U_OpCode", i - 2))
        '        '        oDBDSLine.SetValue("U_OpName", i - 1, oDBDSLine.GetValue("U_OpName", i - 2))
        '        '        oDBDSLine.SetValue("U_PCyc", i - 1, oDBDSLine.GetValue("U_PCyc", i - 2))
        '        '        oDBDSLine.SetValue("U_Labour", i - 1, oDBDSLine.GetValue("U_Labour", i - 2))
        '        '        oDBDSLine.SetValue("LineId", i - 1, i)
        '        '        oMatrix.LoadFromDataSourceEx()
        '        '    End If

        '        'Next
        '        oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
        '        'objAddOn.DeleteEmptyRowInFormDataEvent(oMatrix, "OpCode", oDBDSLine)
        '    End If
        'End If

    End Sub
End Class
