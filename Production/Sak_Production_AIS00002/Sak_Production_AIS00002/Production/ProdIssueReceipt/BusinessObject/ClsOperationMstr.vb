﻿Public Class ClsOperationMstr
    Dim oDBDSHeader, ODBDSLine_Machine, ODBDSLine_RM, ODBDSLine_FG, ODBDSLine_Labour As SAPbouiCOM.DBDataSource
    Dim oMatrixMach, oMatrixRM, oMatrixFG, omatLabour As SAPbouiCOM.Matrix
    Dim oForm, oQualityForm As SAPbouiCOM.Form
    Dim oDataTable As SAPbouiCOM.DataTable
    Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
    Dim Ors, oRecSet, oRecSet1 As SAPbobsCOM.Recordset
    Dim ItemId
    Dim CurrentRow
    Dim CurrLineID As Integer
    Dim WoDocEntry As String
    Sub LoadScreen()
        Try
            oForm = objAddOn.objUIXml.LoadScreenXML("Operation.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, "OPRN")
            oDBDSHeader = oForm.DataSources.DBDataSources.Item(0)

            ODBDSLine_Machine = oForm.DataSources.DBDataSources.Item("@AS_PRN2")
            ODBDSLine_Labour = oForm.DataSources.DBDataSources.Item("@AS_PRN1")
            ODBDSLine_RM = oForm.DataSources.DBDataSources.Item("@AS_PRN3")
            ODBDSLine_FG = oForm.DataSources.DBDataSources.Item("@AS_PRN4")

            oMatrixRM = oForm.Items.Item("oRM").Specific
            oMatrixFG = oForm.Items.Item("oFG").Specific
            oMatrixMach = oForm.Items.Item("Mach").Specific
            omatLabour = oForm.Items.Item("Labour").Specific

            oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE

            oMatrixRM.AddRow()
            oMatrixRM.ClearRowData(oMatrixRM.VisualRowCount)
            oMatrixRM.Columns.Item("V_-1").Cells.Item(oMatrixRM.VisualRowCount).Specific.value() = oMatrixRM.VisualRowCount

            oMatrixFG.AddRow()
            oMatrixFG.ClearRowData(oMatrixFG.VisualRowCount)
            oMatrixFG.Columns.Item("V_-1").Cells.Item(oMatrixFG.VisualRowCount).Specific.value() = oMatrixFG.VisualRowCount

            oMatrixMach.AddRow()
            oMatrixMach.ClearRowData(oMatrixMach.VisualRowCount)
            oMatrixMach.Columns.Item("V_-1").Cells.Item(oMatrixMach.VisualRowCount).Specific.value() = oMatrixMach.VisualRowCount

            '

            omatLabour.AddRow()
            omatLabour.ClearRowData(omatLabour.VisualRowCount)
            omatLabour.Columns.Item("V_-1").Cells.Item(oMatrixRM.VisualRowCount).Specific.value() = omatLabour.VisualRowCount
            oForm.Items.Item("RM").Click(SAPbouiCOM.BoCellClickType.ct_Regular)


            Dim oco As SAPbouiCOM.ComboBox
            oco = oMatrixRM.Columns.Item("UOM").Cells.Item(1).Specific
            Dim rset As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            rset.DoQuery("Select distinct isnull(InvntryUom,'') InvntryUom from OITM  Where isnull(InvntryUom,'') !=''")
            If rset.RecordCount > 0 Then
                Dim i As Integer
                For i = 1 To rset.RecordCount
                    oco.ValidValues.Add(rset.Fields.Item("InvntryUom").Value, rset.Fields.Item("InvntryUom").Value)
                    rset.MoveNext()
                Next
            End If



        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Form Load " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try


    End Sub
    Public Function LoadQualityParam(ByVal opcod As String)
        Try
            'Dim str As String

            'oQualityForm = objAddOn.objUIXml.LoadScreenXML("Operation.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, "OPRN")
            'oDBDSHeader = oQualityForm.DataSources.DBDataSources.Item(0)
            'oDBDSLine = oQualityForm.DataSources.DBDataSources.Item(1)
            'oMatrix = oQualityForm.Items.Item("20").Specific
            'oQualityForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
            'Ors = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            'str = "SELECT [DocEntry] 'docentry' FROM [dbo].[@AS_OPRN]   where[Code] = '" & opcod & "'"
            'Ors = objAddOn.DoQuery(str)
            'If Ors.RecordCount > 0 Then
            '    oQualityForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            '    oQualityForm.Items.Item("Code").Enabled = True
            '    oQualityForm.Items.Item("Code").Specific.value = opcod
            '    oQualityForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            '    oQualityForm.Items.Item("Code").Enabled = False
            '    oQualityForm.Items.Item("Name").Enabled = False
            'End If

            'oQualityForm.Items.Item("7").Visible = False
            'oQualityForm.Items.Item("9").Visible = False
            'oQualityForm.Items.Item("10").Visible = False
            'oQualityForm.Items.Item("12").Visible = False
            'oQualityForm.Items.Item("13").Visible = False
            'oQualityForm.Items.Item("14").Visible = False
            'oQualityForm.Items.Item("17").Visible = False
            'oQualityForm.Items.Item("18").Visible = False
            'oQualityForm.Items.Item("Cycle").Visible = False
            'oQualityForm.Items.Item("Life").Visible = False
            'oQualityForm.Items.Item("Used").Visible = False
            'oQualityForm.Items.Item("BalTime").Visible = False
            'oMatrix.Columns.Item("Param").Editable = False
            'oMatrix.Columns.Item("Min").Editable = False
            'oMatrix.Columns.Item("Max").Editable = False
            'oMatrix.Columns.Item("Lot").Editable = True
            'oMatrix.Columns.Item("Actual").Editable = True
        Catch ex As Exception

        End Try

        Return True
    End Function
    Function quality()
        'Dim i As Integer
        'If oMatrix.VisualRowCount > 0 Then
        '    For i = 1 To oMatrix.RowCount - 1
        '        If oMatrix.Columns.Item("Actual").Cells.Item(oMatrix.VisualRowCount).Specific.value = 0 Then
        '            MessageBox.Show("Update Actual Value")
        '        End If
        '    Next
        'End If
        Return True
    End Function



#Region "Delete an Empty Row"
    Public Sub DeleteEmptyRow(ByVal FormUID As String)
        Try
            'oForm = objAddOn.objApplication.Forms.Item(FormUID)
            'oForm.Items.Item("mcode").Specific.value = ""
            'oForm.Items.Item("mname").Specific.value = ""
            'oMatrix = oForm.Items.Item("20").Specific
            'If oMatrix.Columns.Item("Param").Cells.Item(oMatrix.VisualRowCount).Specific.value = "" Then
            '    oMatrix.DeleteRow(oMatrix.VisualRowCount)
            'End If
            'oMatrixMach = oForm.Items.Item("Mach").Specific
            'If oMatrixMach.Columns.Item("mcode").Cells.Item(oMatrixMach.VisualRowCount).Specific.value = "" Then
            '    oMatrixMach.DeleteRow(oMatrixMach.VisualRowCount)
            'End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Delete an Empty Row Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub
#End Region

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then

            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Select Case oCFLE.ChooseFromListUID
                            Case "CFL_itm"
                                objAddOn.ChooseFromListFilteration(oForm, "CFL_itm", "ItemCode", "select ItemCode from oitm where ItmsGrpCod In (select ItmsGrpCod from oitb where U_Type In ('SG','R'))")

                            Case "CFL_FGItem"
                                objAddOn.ChooseFromListFilteration(oForm, "CFL_FGItem", "ItemCode", "select ItemCode from oitm where ItmsGrpCod In (select ItmsGrpCod from oitb where U_Type In ('SG','F') )")

                            Case "CFL_Machine"
                                objAddOn.ChooseFromListFilteration(oForm, "CFL_Machine", "ResCode", "select ResCode from ORSC where ResType='M'")

                            Case "CFL_Labour"
                                objAddOn.ChooseFromListFilteration(oForm, "CFL_Labour", "ResCode", "select ResCode from ORSC where ResType='L'")
                        End Select
                    Catch ex As Exception

                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                            If pVal.ItemUID = "1" Then

                                If Validation() = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                End If

                            End If
                        End If

                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Click Event " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try
            End Select
        Else
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        oForm.Items.Item("Code").Enabled = False
                    End If
                    If pVal.ItemUID = "RM" Then
                        oForm.PaneLevel = 1
                        oForm.Settings.EnableRowFormat = "25"
                        Dim fSettings As SAPbouiCOM.FormSettings
                        fSettings = oForm.Settings
                        fSettings.MatrixUID = "oRM"

                    ElseIf pVal.ItemUID = "FG" Then
                        oForm.PaneLevel = 2
                        Dim fSettings As SAPbouiCOM.FormSettings
                        fSettings = oForm.Settings
                        fSettings.MatrixUID = "oFG"



                    ElseIf pVal.ItemUID = "Machine" Then
                        oForm.PaneLevel = 3
                        Dim fSettings As SAPbouiCOM.FormSettings
                        fSettings = oForm.Settings
                        fSettings.MatrixUID = "Mach"


                    ElseIf pVal.ItemUID = "1000003" Then
                        oForm.PaneLevel = 4
                        Dim fSettings As SAPbouiCOM.FormSettings
                        fSettings = oForm.Settings
                        fSettings.MatrixUID = "Labour"


                    End If



                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Dim oDataTable As SAPbouiCOM.DataTable
                    Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                    oDataTable = oCFLE.SelectedObjects
                    Select Case oCFLE.ChooseFromListUID


                        Case "CFL_FGItem"
                            oMatrixFG.FlushToDataSource()
                            ODBDSLine_FG.SetValue("U_FgCode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                            ODBDSLine_FG.SetValue("U_Desc", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                            oMatrixFG.LoadFromDataSource()
                            oMatrixFG.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                            objAddOn.SetNewLine(oMatrixFG, ODBDSLine_FG)
                        Case "CFL_itm"

                            oMatrixRM.FlushToDataSource()
                            ODBDSLine_RM.SetValue("U_ItmCde", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                            ODBDSLine_RM.SetValue("U_Desc", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                            oMatrixRM.LoadFromDataSource()
                            oMatrixRM.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                            objAddOn.SetNewLine(oMatrixRM, ODBDSLine_RM)
                        Case "Cfl_RMWhs"
                            oMatrixRM.FlushToDataSource()
                            ODBDSLine_RM.SetValue("U_Whs", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                            ODBDSLine_RM.SetValue("U_WName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                            oMatrixRM.LoadFromDataSource()
                            oMatrixRM.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                        Case "Cfl_FGWhs"
                            oMatrixFG.FlushToDataSource()
                            ODBDSLine_FG.SetValue("U_Whs", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                            ODBDSLine_FG.SetValue("U_WName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                            oMatrixFG.LoadFromDataSource()
                            oMatrixFG.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()



                        Case "CFL_Machine"

                            oMatrixMach.FlushToDataSource()
                            ODBDSLine_Machine.SetValue("U_mcode", pVal.Row - 1, Trim(oDataTable.GetValue("ResCode", 0)))
                            ODBDSLine_Machine.SetValue("U_mname", pVal.Row - 1, Trim(oDataTable.GetValue("ResName", 0)))
                            oMatrixMach.LoadFromDataSource()
                            oMatrixMach.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                            objAddOn.SetNewLine(oMatrixMach, ODBDSLine_Machine)
                        Case "CFL_Labour"
                            omatLabour.FlushToDataSource()
                            ODBDSLine_Labour.SetValue("U_LabourCode", pVal.Row - 1, Trim(oDataTable.GetValue("ResCode", 0)))
                            ODBDSLine_Labour.SetValue("U_LabourName", pVal.Row - 1, Trim(oDataTable.GetValue("ResName", 0)))
                            omatLabour.LoadFromDataSource()
                            omatLabour.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                            objAddOn.SetNewLine(omatLabour, ODBDSLine_Labour)


                    End Select




            End Select

        End If

    End Sub
    Public Sub FormDataEvent(ByVal BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD

                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Or oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then

                        oMatrixRM = oForm.Items.Item("oRM").Specific
                        oMatrixFG = oForm.Items.Item("oFG").Specific
                        oMatrixMach = oForm.Items.Item("Mach").Specific
                        omatLabour = oForm.Items.Item("Labour").Specific
                        If oMatrixRM.VisualRowCount = 0 Then
                            oMatrixRM.AddRow()
                            oMatrixRM.ClearRowData(oMatrixRM.VisualRowCount)
                            oMatrixRM.Columns.Item("V_-1").Cells.Item(oMatrixRM.VisualRowCount).Specific.value() = oMatrixRM.VisualRowCount
                        End If
                        If oMatrixFG.VisualRowCount = 0 Then

                            oMatrixFG.AddRow()
                            oMatrixFG.ClearRowData(oMatrixFG.VisualRowCount)
                            oMatrixFG.Columns.Item("V_-1").Cells.Item(oMatrixFG.VisualRowCount).Specific.value() = oMatrixFG.VisualRowCount



                        End If
                        If oMatrixMach.VisualRowCount = 0 Then

                            oMatrixMach.AddRow()
                            oMatrixMach.ClearRowData(oMatrixMach.VisualRowCount)
                            oMatrixMach.Columns.Item("V_-1").Cells.Item(oMatrixMach.VisualRowCount).Specific.value() = oMatrixMach.VisualRowCount

                        End If


                    End If
            End Select
        Catch ex As Exception

        End Try
    End Sub

#Region "Choose From List"
    Public Sub ChooseItem(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        Dim objdt As SAPbouiCOM.DataTable
        objcfl = pval
        objdt = objcfl.SelectedObjects
        If objdt Is Nothing Then
        Else
            Select Case pval.ItemUID

                Case "Mach"
                    oMatrixMach = oForm.Items.Item("Mach").Specific

                    Try
                        Select Case pval.ColUID
                            Case "mcode"
                                oMatrixMach.Columns.Item("mname").Cells.Item(pval.Row).Specific.value = objdt.GetValue("ItemName", 0)
                                oMatrixMach.Columns.Item("mcode").Cells.Item(pval.Row).Specific.value = objdt.GetValue("ItemCode", 0)


                        End Select

                    Catch ex As Exception

                    End Try
                    If pval.ColUID = "mcode" Then
                        If oMatrixMach.Columns.Item("mcode").Cells.Item(oMatrixMach.VisualRowCount).Specific.String <> "" Then
                            oMatrixMach.AddRow()
                            oMatrixMach.Columns.Item("V_-1").Cells.Item(oMatrixMach.VisualRowCount).Specific.value = oMatrixMach.VisualRowCount
                            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                            End If
                        End If
                    End If



                Case "26"
                    oMatrixFG = oForm.Items.Item("26").Specific
                    Try
                        Select Case pval.ColUID
                            Case "Item"
                                oMatrixFG.Columns.Item("Item").Cells.Item(pval.Row).Specific.value = objdt.GetValue("ItemCode", 0)
                        End Select
                    Catch ex As Exception

                    End Try



            End Select

        End If
    End Sub
#End Region

    Function Validation()
        If oForm.Items.Item("Code").Specific.String = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select Code", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return False
        End If
        If oForm.Items.Item("Name").Specific.String = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select Name", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return False
        End If
        If oForm.Items.Item("Cycle").Specific.Value = "0" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select Cycle", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return False
        End If
        'If oMatrix.VisualRowCount > 0 Then
        '    If oMatrix.Columns.Item("ItemCode").Cells.Item(1).Specific.String = "" Then
        '        objAddOn.objApplication.SetStatusBarMessage("Select ItemCode Atleast One", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        '        Return False
        '    End If

        'End If
        Return True
    End Function

    Sub InitForm()
        oMatrixRM.AddRow()
        oMatrixRM.ClearRowData(oMatrixRM.VisualRowCount)
        oMatrixRM.Columns.Item("V_-1").Cells.Item(oMatrixRM.VisualRowCount).Specific.value() = oMatrixRM.VisualRowCount

        oMatrixFG.AddRow()
        oMatrixFG.ClearRowData(oMatrixFG.VisualRowCount)
        oMatrixFG.Columns.Item("V_-1").Cells.Item(oMatrixFG.VisualRowCount).Specific.value() = oMatrixFG.VisualRowCount

        oMatrixMach.AddRow()
        oMatrixMach.ClearRowData(oMatrixMach.VisualRowCount)
        oMatrixMach.Columns.Item("V_-1").Cells.Item(oMatrixMach.VisualRowCount).Specific.value() = oMatrixMach.VisualRowCount
    End Sub

    Sub Menu_event(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        If pVal.MenuUID = "1282" Then
            InitForm()

        End If



        Select Case pVal.MenuUID
            Case "1292"


                oForm = objAddOn.objApplication.Forms.ActiveForm
                If oForm.TypeEx = "OPRN" Then
                    Try
                        oMatrixMach = oForm.Items.Item("20").Specific

                        oForm.DataSources.DBDataSources.Item("@AS_PRN1").Clear()
                        If oMatrixMach.VisualRowCount = 0 Then
                            oForm.DataSources.DBDataSources.Item("@AS_PRN1").Clear()
                            oMatrixMach.AddRow(1, -1)
                            oMatrixMach.Columns.Item("V_-1").Cells.Item(oMatrixMach.VisualRowCount).Specific.value = oMatrixMach.VisualRowCount
                        ElseIf oMatrixMach.Columns.Item("Param").Cells.Item(oMatrixMach.VisualRowCount).Specific.String <> "" Then
                            oForm.DataSources.DBDataSources.Item("@AS_PRN1").Clear()
                            oMatrixMach.AddRow(1, -1)
                            oMatrixMach.Columns.Item("V_-1").Cells.Item(oMatrixMach.VisualRowCount).Specific.value = oMatrixMach.VisualRowCount
                        End If
                    Catch ex As Exception

                    End Try

                    Try

                        oForm = objAddOn.objApplication.Forms.ActiveForm

                        oMatrixMach = oForm.Items.Item("Mach").Specific
                        'If oMatrixMach.VisualRowCount = 0 Then
                        '    oForm.DataSources.DBDataSources.Item("@AS_PRN2").Clear()
                        '    oMatrixMach.AddRow(1, -1)
                        '    oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
                        'ElseIf oMatrixMach.Columns.Item("mcode").Cells.Item(oMatrixMach.VisualRowCount).Specific.String <> "" Then
                        '    oForm.DataSources.DBDataSources.Item("@AS_PRN2").Clear()
                        '    oMatrixMach.AddRow(1, -1)
                        '    oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
                        'End If


                    Catch ex As Exception

                    End Try

                End If


        End Select
    End Sub

End Class
