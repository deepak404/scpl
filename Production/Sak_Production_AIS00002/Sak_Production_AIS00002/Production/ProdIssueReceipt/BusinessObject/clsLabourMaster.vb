Public Class clsLabourMaster
    Dim oForm As SAPbouiCOM.Form
    Dim oCombo, oUnitCombo, oSecCombo, oLocCombo As SAPbouiCOM.ComboBox
    Public Const FormType = "MNU_OLBR"
    Public Sub LoadScreen()
        oForm = objAddOn.objUIXml.LoadScreenXML("LabourMaster.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        oForm.DataBrowser.BrowseBy = "Code"
        loadcombo()
    End Sub

    Sub loadcombo()
        oCombo = oForm.Items.Item("10").Specific
        objAddOn.objGenFunc.setComboBoxValue(oCombo, "select Code,Name FROM [@AS_OLGM] ")
        oLocCombo = oForm.Items.Item("12").Specific
        objAddOn.objGenFunc.setComboBoxValue(oLocCombo, "select Code,Name FROM [@AIS_LOCATION] ")
        oSecCombo = oForm.Items.Item("26").Specific
        objAddOn.objGenFunc.setComboBoxValue(oSecCombo, "select Code,Name FROM [@AIS_SECTION] ")
        oUnitCombo = oForm.Items.Item("24").Specific
        objAddOn.objGenFunc.setComboBoxValue(oUnitCombo, "select Code,Name FROM [@AIS_UNIT] ")
    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByVal pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Try
                    Select Case pVal.EventType
                        Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                            If pVal.ItemUID = "1" Then
                                CostCalculation()
                            End If
                    End Select
                Catch ex As Exception
                End Try
            ElseIf pVal.BeforeAction = False Then
                Try
                    Select Case pVal.EventType
                        Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                            If pVal.ItemUID = "14" Or pVal.ItemUID = "16" Or pVal.ItemUID = "18" Then
                                CostCalculation()
                            End If
                    End Select
                Catch ex As Exception
                End Try
            End If

        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Item Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
    Sub Menu_event(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        If pVal.MenuUID = "1281" Or pVal.MenuUID = "1282" Then
            oForm.Items.Item("Code").Enabled = True
        Else
            oForm.Items.Item("Code").Enabled = False
        End If
    End Sub

    Public Sub FormDataEvent(ByVal BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        'Try
        '    Select Case BusinessObjectInfo.EventType
        '        Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
        '            If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Or oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
        '                oForm.Items.Item("Code").Enabled = False
        '            Else
        '                oForm.Items.Item("Code").Enabled = True
        '            End If
        '    End Select
        'Catch ex As Exception

        'End Try
    End Sub
    Public Sub CostCalculation()
        Try
            Dim Salary, DaysPerMonth, HoursPerDay As Double
            Salary = 0
            DaysPerMonth = 0
            HoursPerDay = 0

            Try
                Salary = oForm.Items.Item("14").Specific.value
            Catch ex As Exception
            End Try
            Try
                DaysPerMonth = oForm.Items.Item("16").Specific.value
            Catch ex As Exception
            End Try
            Try
                HoursPerDay = oForm.Items.Item("18").Specific.value
            Catch ex As Exception
            End Try
            Try
                oForm.Items.Item("8").Specific.value = (Salary / DaysPerMonth) / HoursPerDay
            Catch ex As Exception
                oForm.Items.Item("8").Specific.value = 0
            End Try

        Catch ex As Exception

        End Try
    End Sub
End Class
