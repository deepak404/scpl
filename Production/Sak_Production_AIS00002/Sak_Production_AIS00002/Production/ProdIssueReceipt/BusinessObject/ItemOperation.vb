﻿
Imports System.Diagnostics
Imports System.Reflection



Public Class ItemOperation


    Dim frmItemOperation As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail As SAPbouiCOM.DBDataSource

    Dim oMatrix As SAPbouiCOM.Matrix
    Dim oCmb As SAPbouiCOM.ComboBox
    Dim str_Active As String = ""
    Public LoadValue As String = ""





    Public Sub LoadForm()
        Try


            objAddOn.objUIXml.LoadScreenXML("ItemOperation.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, "IOD")

            frmItemOperation = objAddOn.objApplication.Forms.ActiveForm
            oDBDSHeader = frmItemOperation.DataSources.DBDataSources.Item(0)
            oDBDSDetail = frmItemOperation.DataSources.DBDataSources.Item(1)

            frmItemOperation.Freeze(True)
            oMatrix = frmItemOperation.Items.Item("Matrix").Specific
            Me.DefineModesForFields()
            frmItemOperation.EnableMenu("1293", True)
            frmItemOperation.EnableMenu("1282", True)
            InitForm()
            frmItemOperation.DataBrowser.BrowseBy = "t_num"

            frmItemOperation.Items.Item("l_Dat").Visible = True
            frmItemOperation.Items.Item("t_Date").Visible = True
            frmItemOperation.Items.Item("l_in").Visible = True
            frmItemOperation.Items.Item("t_INa").Visible = True
            frmItemOperation.Items.Item("l_IC").Visible = True
            frmItemOperation.Items.Item("t_IC").Visible = True

            oMatrix.Columns.Item("OC").Editable = True
            oMatrix.Columns.Item("MC").Editable = True




        Catch ex As Exception

        Finally
            If Not frmItemOperation Is Nothing Then _
            frmItemOperation.Freeze(False)
        End Try
    End Sub



    Public Sub InitForm()
        Try

            'Series
            objAddOn.LoadComboBoxSeries(frmItemOperation.Items.Item("c_series").Specific, "IOD") ' Load the Combo Box Series
            'Date
            objAddOn.LoadDocumentDate(frmItemOperation.Items.Item("t_DocDate").Specific) ' Load Document Date 
            oMatrix.AddRow()
            oMatrix.ClearRowData(oMatrix.VisualRowCount)
            oMatrix.Columns.Item("#").Cells.Item(oMatrix.VisualRowCount).Specific.value() = oMatrix.VisualRowCount




        Catch ex As Exception

        Finally
        End Try
    End Sub
    Public Sub DefineModesForFields()
        Try
            frmItemOperation.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmItemOperation.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText(("Define Modes For Fields Method Failed:" & ex.Message), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub



    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType

                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmItemOperation.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmItemOperation.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Value.ToString.Trim
                                    Dim strDocNum As Long = frmItemOperation.BusinessObject.GetNextSerialNumber(strSerialCode, "IOD")
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                        End Select
                    Catch ex As Exception


                    End Try

 
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    If pVal.ItemUID = "Matrix" And pVal.ColUID = "OC" Then
                        oMatrix = frmItemOperation.Items.Item("Matrix").Specific
                        If oMatrix.Columns.Item("OC").Cells.Item(oMatrix.VisualRowCount).Specific.value <> "" Then
                            oMatrix.AddRow()
                            oMatrix.Columns.Item("#").Cells.Item(oMatrix.VisualRowCount).Specific.string = oMatrix.VisualRowCount
                            oMatrix.ClearRowData(oMatrix.VisualRowCount)
                        End If
                    End If




                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try


                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

                        If pVal.BeforeAction = False And Not oDataTable Is Nothing Then
                            '


                            Select Case oCFLE.ChooseFromListUID

                                Case "OPN_CFL"
                                    If pVal.BeforeAction = False Then
                                        oMatrix.FlushToDataSource()
                                        oDBDSDetail.SetValue("U_OperCode", pVal.Row - 1, Trim(oDataTable.GetValue("Code", 0)))
                                        oDBDSDetail.SetValue("U_OperName", pVal.Row - 1, Trim(oDataTable.GetValue("Name", 0)))
                                        oDBDSDetail.SetValue("U_PCyc", pVal.Row - 1, oDataTable.GetValue("U_Cycle", 0))
                                        oDBDSDetail.SetValue("U_Process", pVal.Row - 1, oDataTable.GetValue("U_Process", 0))
                                        oMatrix.LoadFromDataSource()
                                        oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        objAddOn.SetNewLine(oMatrix, oDBDSDetail)
                                    End If
                                Case "CFL_RES"
                                    oMatrix.FlushToDataSource()
                                    Dim sResCode As String = Trim(oDataTable.GetValue("ResCode", 0))
                                    oDBDSDetail.SetValue("U_MCCode", pVal.Row - 1, sResCode)
                                    oDBDSDetail.SetValue("U_MCName", pVal.Row - 1, Trim(oDataTable.GetValue("ResName", 0)))
                                    oMatrix.LoadFromDataSource()
                                    oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()


                                Case "ITM_CFL"
                                    If pVal.BeforeAction = False Then
                                        oDBDSHeader.SetValue("U_ItemCode", 0, Trim(oDataTable.GetValue("ItemCode", 0)))
                                        oDBDSHeader.SetValue("U_ItemName", 0, Trim(oDataTable.GetValue("ItemName", 0)))

                                    End If
                                 

                                  



                            End Select


                        End If



                    Catch ex As Exception

                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "Matrix"
                                Select Case pVal.ColUID
                                    Case "MC"
                                        If pVal.BeforeAction = False Then
                                            objAddOn.ChooseFromListFilteration(frmItemOperation, "CFL_RES", "ResCode", "Select  ResCode  From ORSC Where ResType ='M'")
                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.BeforeAction And (frmItemOperation.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or _
                                                          frmItemOperation.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then

                                    If ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Return
                                    Else


                                    End If

                                End If

                        End Select
                    Catch ex As Exception

                        If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        If pVal.ItemUID = "1" Then
                            If pVal.BeforeAction = False And pVal.ActionSuccess Then
                                Me.InitForm()
                               

                            End If
                        End If





                    Catch ex As Exception

                    End Try



 
            End Select
        Catch ex As Exception

        End Try

    End Sub



    Public Function ValidateAll() As Boolean
        Try
            Dim sumofqty As Double = 0



            Return True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText(("Validate Function Failed:" & ex.Message), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End Try
    End Function
    Public Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    InitForm()

                Case "1281"

            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText(("Menu Event Failed:" & ex.Message), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        If BusinessObjectInfo.BeforeAction = False Then
                        End If
                    Catch ex As Exception

                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE


                    If Me.ValidateAll() = False Then
                        BubbleEvent = False
                        System.Media.SystemSounds.Asterisk.Play()
                        Return
                    Else




                    End If

            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub



End Class


