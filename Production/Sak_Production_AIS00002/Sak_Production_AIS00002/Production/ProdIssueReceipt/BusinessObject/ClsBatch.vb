﻿Public Class ClsBatch
    Public frmBatch As SAPbouiCOM.Form
    Dim oDBDSHeader As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Sub LoadScreen()
        Try
            frmBatch = objAddOn.objUIXml.LoadScreenXML("BatchNo.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, "AIS_Batch")
            oDBDSHeader = frmBatch.DataSources.DBDataSources.Item(0)
            oDBDSDetail = frmBatch.DataSources.DBDataSources.Item(1)
            oMatrix = frmBatch.Items.Item("3").Specific
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Form Load " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
End Class
