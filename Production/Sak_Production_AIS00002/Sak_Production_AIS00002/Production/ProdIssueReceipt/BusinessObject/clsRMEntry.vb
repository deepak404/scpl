﻿Public Class clsRMEntry
    Dim oForm As SAPbouiCOM.Form
    Public Const FormType = "RMEN"
    Dim oMatrix, oMatrix1, oMatrix2 As SAPbouiCOM.Matrix
    Dim oRS As SAPbobsCOM.Recordset
    Public dyncode As String
    Public Sub LoadScreen(ByVal mchn As String, ByVal dynamicCode As String, ByVal prntItem As String, ByVal whs As String)
        Try
            Dim i As Integer
            oForm = objAddOn.objUIXml.LoadScreenXML("RMEntry.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            'dyncode = dynamicCode
            oForm.Items.Item("4").Specific.String = mchn
            oForm.Items.Item("6").Specific.String = dynamicCode
            oMatrix = oForm.Items.Item("3").Specific
            oMatrix1 = oForm.Items.Item("11").Specific
            oMatrix2 = oForm.Items.Item("12").Specific
            oForm.Items.Item("7").Click()
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            'oRS.DoQuery("SELECT T0.[Code],isnull(T1.U_whscode ,'" & whs & "') Whs,isnull(T1.U_qty,0) U_qty, isnull(T1.U_qty,0) U_qty " & _
            '            " ,isnull(T1.U_remarks ,'') U_remarks FROM ITT1 T0 left outer join [@AS_RMEN] T1 on T0.Code=T1.U_itemcode and " & _
            '            " T1.U_dyncode='" & dynamicCode & "' and T1.U_mchncode='" & mchn & "' WHERE T0.[Father] ='" & prntItem & "'")

            oRS.DoQuery("SELECT T0.[Code], T0.Warehouse [Whs],isnull(T1.U_qty,0) U_qty, isnull(T1.U_qty,0) U_qty " & _
                    " ,isnull(T1.U_remarks ,'') U_remarks FROM ITT1 T0 left outer join [@AS_RMEN] T1 on T0.Code=T1.U_itemcode and " & _
                    " T1.U_dyncode='" & dynamicCode & "' and T1.U_mchncode='" & mchn & "' WHERE T0.[Father] ='" & prntItem & "'")
            oRS.MoveFirst()
            For i = 1 To oRS.RecordCount
                oMatrix.AddRow()
                oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.value = oRS.Fields.Item("Code").Value
                oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value = oRS.Fields.Item("whs").Value
                oMatrix.Columns.Item("V_3").Cells.Item(i).Specific.value = oRS.Fields.Item("U_qty").Value
                oMatrix.Columns.Item("V_2").Cells.Item(i).Specific.value = oRS.Fields.Item("U_remarks").Value
                oRS.MoveNext()
            Next

            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("SELECT T0.[U_lbrcode], T0.[U_lbrname], T0.[U_mchncode], T0.[U_dyncode], T0.[U_nooflbr], convert(nvarchar(12), " & _
                        " T0.[U_fdate],103) U_fdate, convert(nvarchar(12),T0.[U_tdate],103) U_tdate, T0.[U_fromtime], T0.[U_totime], T0.[U_tothrs], " & _
                        " T0.[U_totcost] FROM [dbo].[@AS_PLBR]  T0 WHERE T0.[U_dyncode] ='" & dynamicCode & "' " &
                        " and  T0.[U_mchncode] ='" & mchn & "'")
            If oRS.RecordCount > 0 Then
                oRS.MoveFirst()
                For i = 1 To oRS.RecordCount
                    oMatrix1.AddRow()
                    oMatrix1.Columns.Item("V_2").Cells.Item(i).Specific.value = oRS.Fields.Item("U_lbrcode").Value
                    oMatrix1.Columns.Item("V_1").Cells.Item(i).Specific.value = oRS.Fields.Item("U_lbrname").Value
                    oMatrix1.Columns.Item("V_4").Cells.Item(i).Specific.value = oRS.Fields.Item("U_nooflbr").Value
                    oMatrix1.Columns.Item("V_8").Cells.Item(i).Specific.string = oRS.Fields.Item("U_fdate").Value
                    oMatrix1.Columns.Item("V_3").Cells.Item(i).Specific.value = oRS.Fields.Item("U_tothrs").Value
                    oMatrix1.Columns.Item("V_0").Cells.Item(i).Specific.value = oRS.Fields.Item("U_totcost").Value
                    oMatrix1.Columns.Item("V_7").Cells.Item(i).Specific.string = oRS.Fields.Item("U_tdate").Value
                    oMatrix1.Columns.Item("V_6").Cells.Item(i).Specific.value = oRS.Fields.Item("U_fromtime").Value
                    oMatrix1.Columns.Item("V_5").Cells.Item(i).Specific.value = oRS.Fields.Item("U_totime").Value

                    oRS.MoveNext()
                Next
            Else
                oMatrix1.AddRow()
            End If

            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("SELECT T0.[U_idltime], T0.[U_reason] FROM [dbo].[@AS_IDLE]  T0 WHERE T0.[U_mchncode] ='" & mchn & "' and  T0.[U_dyncode] ='" & dynamicCode & "'")
            If oRS.RecordCount > 0 Then
                oRS.MoveFirst()
                For i = 1 To oRS.RecordCount
                    oMatrix2.AddRow()
                    oMatrix2.Columns.Item("V_0").Cells.Item(i).Specific.value = oRS.Fields.Item("U_idltime").Value
                    oMatrix2.Columns.Item("V_1").Cells.Item(i).Specific.value = oRS.Fields.Item("U_reason").Value
                    oRS.MoveNext()
                Next
            Else
                oMatrix2.AddRow()
            End If

        Catch ex As Exception

        End Try

    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        'formUID = "RMEN"
        If pVal.BeforeAction = True Then
            Try
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        oForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "7" Then
                            oForm.PaneLevel = 1
                        ElseIf pVal.ItemUID = "8" Then
                            oForm.PaneLevel = 2
                        ElseIf pVal.ItemUID = "9" Then
                            oForm.PaneLevel = 3
                        End If
                End Select
            Catch ex As Exception
            End Try
        Else
            Try
                oForm = objAddOn.objApplication.Forms.Item(FormUID)
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.ItemUID = "3" Or pVal.ItemUID = "11" Then
                            ChooseItem(FormUID, pVal)
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "1" And oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                            If RawMeterialEntry() Then
                                If LabourEntry() Then
                                    IdleTimeEntry()
                                End If
                            End If
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        If pVal.ItemUID = "12" And pVal.ColUID = "V_0" Then
                            oMatrix = oForm.Items.Item("12").Specific
                            If pVal.Row = oMatrix.VisualRowCount And oMatrix.Columns.Item("V_0").Cells.Item(pVal.Row).Specific.value <> "" Then
                                oMatrix.AddRow()
                            End If
                        End If
                        If pVal.ItemUID = "11" And (pVal.ColUID = "V_2" Or pVal.ColUID = "V_3" Or pVal.ColUID = "V_4") Then
                            Try
                                oMatrix = oForm.Items.Item("11").Specific
                                Dim TotTime, lbrCode As String
                                Dim noOfLbr, lbrsCost As Double
                                TotTime = oMatrix.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.string
                                lbrCode = oMatrix.Columns.Item("V_2").Cells.Item(pVal.Row).Specific.string
                                noOfLbr = oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value
                                If TotTime <> "" And lbrCode <> "" And noOfLbr <> 0 Then
                                    lbrsCost = CalculateLabourCost(FormUID, TotTime, lbrCode, noOfLbr)
                                    oMatrix.Columns.Item("V_0").Cells.Item(pVal.Row).Specific.value = lbrsCost
                                End If
                            Catch ex As Exception

                            End Try

                        End If

                    Case SAPbouiCOM.BoEventTypes.et_FORM_RESIZE
                        oForm.Items.Item("10").Left = oForm.Items.Item("3").Left
                        oForm.Items.Item("10").Height = oForm.Items.Item("3").Height + 15
                        oForm.Items.Item("10").Width = oForm.Items.Item("3").Width
                        oForm.Items.Item("10").Top = oForm.Items.Item("3").Top - 2
                End Select
            Catch ex As Exception

            End Try
        End If
    End Sub
    Private Function RawMeterialEntry() As Boolean
        Try
            Dim sqlStr As String = ""
            Dim i As Integer
            Dim itemcode, itemname, whs, qty, remarks, machin, Batch As String
            machin = oForm.Items.Item("4").Specific.value
            dyncode = oForm.Items.Item("6").Specific.value
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oMatrix = oForm.Items.Item("3").Specific
            sqlStr = "delete from [@AS_RMEN] where U_dyncode='" & dyncode & "' and U_mchncode='" & machin & "'"
            For i = 1 To oMatrix.VisualRowCount
                itemcode = oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.value
                If itemcode <> "" Then
                    whs = oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value
                    itemname = oMatrix.Columns.Item("V_5").Cells.Item(i).Specific.value
                    remarks = oMatrix.Columns.Item("V_2").Cells.Item(i).Specific.value
                    'machin = oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.value
                    qty = oMatrix.Columns.Item("V_3").Cells.Item(i).Specific.value
                    Batch = oMatrix.Columns.Item("V_0").Cells.Item(i).Specific.value
                    sqlStr = sqlStr & " insert into [@AS_RMEN] (code,name,U_itemcode,U_itemname,U_whscode,U_dyncode,U_mchncode,U_qty,U_remarks,U_batch)" &
                        " values ('" & dyncode & "_" & machin & "_" & i.ToString & "','" & dyncode & "_" & machin & "_" & i.ToString & "', '" & itemcode & "','" & itemname & "'," &
                        "' " & whs & "','" & dyncode & "','" & machin & "'," & qty & ",'" & remarks & "','" & Batch & "')"
                End If
            Next
            oRS.DoQuery(sqlStr)
            Return True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("RM Entry Failed: " & ex.Message & ", Please give input properly...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End Try
    End Function

    Private Function LabourEntry() As Boolean
        Try
            Dim sqlStr As String = ""
            Dim i As Integer
            Dim lbrcode, lbrname, machin, fdate, tdate, ftime, ttime, tothrs As String
            Dim nooflbr, totcost As Double
            machin = oForm.Items.Item("4").Specific.value
            dyncode = oForm.Items.Item("6").Specific.value
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oMatrix = oForm.Items.Item("11").Specific
            sqlStr = "delete from [@AS_PLBR] where U_dyncode='" & dyncode & "' and U_mchncode='" & machin & "'"
            For i = 1 To oMatrix.VisualRowCount
                lbrcode = oMatrix.Columns.Item("V_2").Cells.Item(i).Specific.value
                If lbrcode <> "" Then
                    nooflbr = oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value
                    lbrname = oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.value
                    fdate = oMatrix.Columns.Item("V_8").Cells.Item(i).Specific.value
                    tdate = oMatrix.Columns.Item("V_7").Cells.Item(i).Specific.value
                    ftime = oMatrix.Columns.Item("V_6").Cells.Item(i).Specific.value
                    ttime = oMatrix.Columns.Item("V_5").Cells.Item(i).Specific.value
                    totcost = oMatrix.Columns.Item("V_0").Cells.Item(i).Specific.value
                    tothrs = oMatrix.Columns.Item("V_3").Cells.Item(i).Specific.value

                    sqlStr = sqlStr & " insert into [@AS_PLBR] (code,name,U_lbrcode,U_lbrname,U_nooflbr,U_dyncode,U_mchncode,U_tothrs,U_totcost," &
                        "U_fdate,U_tdate,U_fromtime,U_totime) values ('" & dyncode & "_" & machin & "_" & i.ToString & "','" & dyncode & "_" & machin & "_" & i.ToString & "', " &
                        "'" & lbrcode & "','" & lbrname & "'," &
                        "' " & nooflbr & "','" & dyncode & "','" & machin & "'," & tothrs & ",'" & totcost & "','" & fdate & "','" & tdate & "'" &
                        ",'" & ftime & "','" & ttime & "')"
                End If
            Next
            oRS.DoQuery(sqlStr)
            Return True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Labour Entry Failed: " & ex.Message & ", Please give input properly...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End Try
    End Function
    Private Function IdleTimeEntry() As Boolean
        Try
            Dim sqlStr As String = ""
            Dim i As Integer
            Dim reason, machin, idletime As String

            machin = oForm.Items.Item("4").Specific.value
            dyncode = oForm.Items.Item("6").Specific.value
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oMatrix = oForm.Items.Item("12").Specific
            sqlStr = "delete from [@AS_IDLE] where U_dyncode='" & dyncode & "' and U_mchncode='" & machin & "'"
            For i = 1 To oMatrix.VisualRowCount
                idletime = oMatrix.Columns.Item("V_0").Cells.Item(i).Specific.value
                If idletime <> "" Then
                    reason = oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.value

                    sqlStr = sqlStr & " insert into [@AS_IDLE] (code,name,U_dyncode,U_mchncode,U_idltime,U_reason) values ('" &
                        " " & dyncode & "_" & machin & "_" & i.ToString & "','" & dyncode & "_" & machin & "_" & i.ToString & "', " &
                        "'" & dyncode & "','" & machin & "'," & idletime & ",'" & reason & "')"
                End If
            Next
            oRS.DoQuery(sqlStr)
            Return True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Idle Time Entry Failed: " & ex.Message & ", Please give input properly...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End Try
    End Function

    Public Sub ChooseItem(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        Dim objdt As SAPbouiCOM.DataTable
        'Dim oEdit As SAPbouiCOM.EditText
        objcfl = pval
        'Dim itemcode As String
        oForm = objAddOn.objApplication.Forms.GetForm("RMEN", -1)
        objdt = objcfl.SelectedObjects
        If objdt Is Nothing Then
        Else
            Select Case pval.ItemUID
                Case "3"
                    oMatrix = oForm.Items.Item("3").Specific
                    Try
                        Select Case pval.ColUID
                            Case "V_1"
                                Try

                                    oMatrix.Columns.Item("V_1").Cells.Item(pval.Row).Specific.value = objdt.GetValue("ItemCode", 0)
                                Catch ex As Exception
                                End Try
                                Try
                                    oMatrix.Columns.Item("V_5").Cells.Item(pval.Row).Specific.value = objdt.GetValue("ItemName", 0)
                                Catch ex As Exception

                                End Try
                                If pval.Row = oMatrix.VisualRowCount Then
                                    oMatrix.AddRow()
                                End If
                            Case "V_4"
                                oMatrix.Columns.Item("V_4").Cells.Item(pval.Row).Specific.value = objdt.GetValue("WhsCode", 0)
                        End Select
                    Catch ex As Exception
                    End Try

                Case "11"
                    oMatrix = oForm.Items.Item("11").Specific
                    Try
                        Select Case pval.ColUID
                            Case "V_2"
                                Try

                                    oMatrix.Columns.Item("V_2").Cells.Item(pval.Row).Specific.value = objdt.GetValue("Code", 0)
                                Catch ex As Exception
                                End Try
                                Try
                                    oMatrix.Columns.Item("V_1").Cells.Item(pval.Row).Specific.value = objdt.GetValue("U_lname", 0)
                                Catch ex As Exception

                                End Try
                                If pval.Row = oMatrix.VisualRowCount Then
                                    oMatrix.AddRow()
                                End If
                        End Select
                    Catch ex As Exception
                    End Try
            End Select

        End If
    End Sub

    Public Function CalculateLabourCost(ByVal FormUID As String, ByVal TotTime As String, ByVal MCode As String, ByVal nooflbr As Double) As Double
        Dim TotLCost As Double = 0
        Try

            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select U_lcost ,round((U_lcost/60) *datediff(mi,0,cast('" & Left(TotTime, 2) & ":" & Right(TotTime, 2) & "' as datetime)),2) [LCost] from [@AS_OLBR] where Code ='" & MCode & "'")
            'If TotTime.Length = 3 Then
            '    TotTime = "0" + TotTime
            'End If
            TotLCost = oRS.Fields.Item("LCost").Value
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Labour Cost Calculation Function Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return 0
        End Try
        Return TotLCost * nooflbr
    End Function

End Class
