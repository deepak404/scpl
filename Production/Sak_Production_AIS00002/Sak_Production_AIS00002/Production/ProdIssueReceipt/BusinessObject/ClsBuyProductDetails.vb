﻿Public Class ClsBuyProductDetails
    Dim oDBDSHeader, oDBDSLine As SAPbouiCOM.DBDataSource
    Dim oMatrix, oIssueBiMatrix As SAPbouiCOM.Matrix
    Dim oBuyProductForm, oIssueForm As SAPbouiCOM.Form
    Public Const FormType = "AS_BDET"
    Dim oDataTable As SAPbouiCOM.DataTable
    Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
    Dim Ors, Ors1 As SAPbobsCOM.Recordset

    Dim WoDocEntry As String
    Dim rs As Integer
    Public Sub LoadScreen(ByVal WoNo As String, ByVal rootcod As String)
        
        oBuyProductForm = objAddOn.objUIXml.LoadScreenXML("Buy Product Details.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        oDBDSHeader = oBuyProductForm.DataSources.DBDataSources.Item(0)
        oDBDSLine = oBuyProductForm.DataSources.DBDataSources.Item(1)
        oBuyProductForm.Items.Item("3").Specific.value = oBuyProductForm.BusinessObject.GetNextSerialNumber("-1", "AS_BDET")
        oBuyProductForm.Items.Item("5").Specific.value = oBuyProductForm.BusinessObject.GetNextSerialNumber("-1", "AS_BDET")
        oBuyProductForm.Items.Item("7").Specific.string = WoNo
        oBuyProductForm.Items.Item("9").Specific.string = rootcod
        oBuyProductForm.DataBrowser.BrowseBy = "3"
        WoDocEntry = objAddOn.getSingleValue("select DocEntry from [@AS_OWORD] where DocNum = '" & WoNo & "'")
    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType


                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED

                    If pVal.ItemUID = "btnCnfrm" Then
                        Try
                            Dim i, j As Integer
                            Dim cod, nam, lot As String
                            Dim qty, input, perc, output As Double

                            Dim whscod = oBuyProductForm.Items.Item("19").Specific.string
                            oMatrix = oBuyProductForm.Items.Item("10").Specific
                            oIssueForm = objAddOn.objApplication.Forms.GetForm("MNU_OIGN", -1)
                            oIssueBiMatrix = oIssueForm.Items.Item("52").Specific
                            Dim issuline As Integer = oIssueBiMatrix.VisualRowCount
                            Dim GrpLot As String = objAddOn.getSingleValue("select U_Lot from [@AS_OWORD] where DocEntry = '" & WoDocEntry & "' ")
                            j = issuline

                            For i = 1 To oMatrix.VisualRowCount

                                cod = oMatrix.Columns.Item("V_0").Cells.Item(i).Specific.String
                                nam = oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.String
                                lot = oMatrix.Columns.Item("V_2").Cells.Item(i).Specific.String
                                qty = oMatrix.Columns.Item("V_3").Cells.Item(i).Specific.value
                                input = oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value
                                perc = oMatrix.Columns.Item("V_5").Cells.Item(i).Specific.value
                                output = oMatrix.Columns.Item("V_6").Cells.Item(i).Specific.value

                                '-------------V_0(ItemCode),V_1(Description),V_2(Bi Pdt Lot),V_3(BiPdt Qty),V_4(Lot NoU from WorkOrder Lot Details(Input)),V_5(% of Lot from Tot Bag),V_6(Pdt Output)--------

                                'oIssueBiMatrix.Columns.Item("V_-1").Cells.Item(j).Specific.value = i
                                oIssueBiMatrix.Columns.Item("V_4").Cells.Item(j).Specific.String = cod
                                oIssueBiMatrix.Columns.Item("V_3").Cells.Item(j).Specific.String = nam
                                oIssueBiMatrix.Columns.Item("V_0").Cells.Item(j).Specific.String = whscod
                                oIssueBiMatrix.Columns.Item("V_1").Cells.Item(j).Specific.String = lot
                                oIssueBiMatrix.Columns.Item("V_2").Cells.Item(j).Specific.value = output
                                oIssueBiMatrix.Columns.Item("V_6").Cells.Item(j).Specific.value = input
                                oIssueBiMatrix.Columns.Item("V_7").Cells.Item(j).Specific.value = perc
                                oIssueBiMatrix.Columns.Item("V_8").Cells.Item(j).Specific.value = GrpLot
                                j += 1
                            Next
                            oBuyProductForm.Items.Item("2").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

                        Catch ex As Exception

                        End Try
                    End If

            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    If pVal.ItemUID = "13" Or pVal.ItemUID = "19" Then
                        ChooseItem(FormUID, pVal)
                    End If
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS

                    If pVal.ItemUID = "17" Then

                        oMatrix = oBuyProductForm.Items.Item("10").Specific
                        Dim itmcode As String = oBuyProductForm.Items.Item("13").Specific.string
                        Dim itmnam As String = oBuyProductForm.Items.Item("15").Specific.string
                        Dim totqty As Double = oBuyProductForm.Items.Item("17").Specific.value
                        Ors = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

                        Ors.DoQuery("select U_LotNo,U_NoU  from [@AS_WORD2] where DocEntry = '" & WoDocEntry & "' and ISNULL (U_LotNo,0) <> 0")
                        Ors.MoveFirst()
                        For rs = 1 To Ors.RecordCount
                            oMatrix = oBuyProductForm.Items.Item("10").Specific
                            oMatrix.AddRow()

                            '-------------V_0(ItemCode),V_1(Description),V_2(Bi Pdt Lot),V_3(BiPdt Qty),V_4(Lot NoU from WorkOrder Lot Details(Input)),V_5(% of Lot from Tot Bag),V_6(Pdt Output)--------
                            oMatrix.Columns.Item("V_-1").Cells.Item(rs).Specific.value = rs
                            oMatrix.Columns.Item("V_0").Cells.Item(rs).Specific.String = itmcode
                            oMatrix.Columns.Item("V_1").Cells.Item(rs).Specific.String = itmnam
                            oMatrix.Columns.Item("V_3").Cells.Item(rs).Specific.value = totqty
                            oMatrix.Columns.Item("V_2").Cells.Item(rs).Specific.String = Ors.Fields.Item("U_LotNo").Value

                            Dim WoTotQty = objAddOn.getSingleValue("select U_TotQty from [@AS_OWORD] where DocEntry = '" & WoDocEntry & "' ")
                            Dim LotNou = objAddOn.getSingleValue("select U_NoU  from [@AS_WORD2] where DocEntry = '" & WoDocEntry & "' and U_LotNo = '" & Ors.Fields.Item("U_LotNo").Value & "' ")
                            Dim perc As Double = (LotNou * 100) / WoTotQty
                            oMatrix.Columns.Item("V_4").Cells.Item(rs).Specific.value = LotNou
                            oMatrix.Columns.Item("V_5").Cells.Item(rs).Specific.value = perc
                            oMatrix.Columns.Item("V_6").Cells.Item(rs).Specific.value = (perc * totqty) / 100
                            Ors.MoveNext()
                        Next

                    End If



            End Select
        End If
    End Sub
#Region "Choose From List"
    Public Sub ChooseItem(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        Dim objdt As SAPbouiCOM.DataTable
        objcfl = pval
        objdt = objcfl.SelectedObjects
        If objdt Is Nothing Then
        Else
            Select Case pval.ItemUID
              
                Case "13"
                    Try
                        oDBDSHeader.SetValue("U_itm", 0, objdt.GetValue("ItemCode", 0))
                        oDBDSHeader.SetValue("U_itmnam", 0, objdt.GetValue("ItemName", 0))

                    Catch ex As Exception

                    End Try
                Case "19"
                    Try
                        oDBDSHeader.SetValue("U_Whs", 0, objdt.GetValue("WhsCode", 0))

                    Catch ex As Exception

                    End Try
            End Select

        End If
    End Sub
#End Region


End Class
