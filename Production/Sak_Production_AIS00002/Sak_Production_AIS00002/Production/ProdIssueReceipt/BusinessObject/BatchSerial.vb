﻿
Imports System.Diagnostics
Imports System.Reflection



Public Class BatchSerial

    Dim frmserbat As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetails As SAPbouiCOM.DBDataSource

    Dim oMatrix1 As SAPbouiCOM.Matrix
    Dim oCmb As SAPbouiCOM.ComboBox
    Dim str_Active As String = ""
    Public LoadValue As String = ""
    '~~~~~~~~~~~~~~Log_Inf~~~~~~~~~~~~~~~~~~
    Dim str_log_Error As Boolean
    Dim str_log_Info As Boolean
    Dim str_log_Severe As Boolean
    Dim int_Run_No As Integer
    Dim str_Session As Integer = 0
    Dim edit_Track_Msg As String
    Dim str_Log_Info_In_Tran As String = ""
    Dim GRNLineID As String = ""
    Dim manageddoc As String = ""
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




    Public Sub LoadForm(ByVal lineid As String, ByVal itemcode As String, ByVal totqty As String, ByVal docno As String, ByVal subconno As String, ByVal frmmode As String, ByVal WhsCde As String, ByVal WhsNam As String)
        Try


            objAddOn.objUIXml.LoadScreenXML("Batch.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, "BSN")
            'frmserbat = objAddOn.objApplication.Forms.Item("BSN")
            frmserbat = objAddOn.objApplication.Forms.ActiveForm
            oDBDSHeader = frmserbat.DataSources.DBDataSources.Item(0)
            oDBDSDetails = frmserbat.DataSources.DBDataSources.Item(1)
            GRNLineID = lineid
            frmserbat.Freeze(True)
            oMatrix1 = frmserbat.Items.Item("Matrix1").Specific
            Me.DefineModesForFields()
            frmserbat.EnableMenu("1293", True)
            InitForm()
            frmserbat.DataBrowser.BrowseBy = "t_DocNum"


            frmserbat.Items.Item("t_SubGRNNo").Specific.value = subconno
            frmserbat.Items.Item("t_lineid").Specific.value = lineid
            frmserbat.Items.Item("t_totqty").Specific.value = totqty
         
            frmserbat.Items.Item("l_prefix").Visible = False
            frmserbat.Items.Item("t_prefix").Visible = False
            frmserbat.Items.Item("l_to").Visible = False
            frmserbat.Items.Item("t_to").Visible = False
            frmserbat.Items.Item("l_suffix").Visible = False
            frmserbat.Items.Item("t_suffix").Visible = False
            frmserbat.Items.Item("l_from").Visible = False
            frmserbat.Items.Item("t_from").Visible = False
            frmserbat.Items.Item("b_load").Visible = False
            oMatrix1.Columns.Item("Qty").Editable = True
            oMatrix1.Columns.Item("batchser").Editable = True



            frmserbat.Items.Item("t_from").Specific.value = 1
            frmserbat.Items.Item("t_to").Specific.value = frmserbat.Items.Item("t_totqty").Specific.value.ToString.Trim





            If (docno.ToString.Trim = "Link") Then
                objAddOn.SetNewLine(oMatrix1, oDBDSDetails)
                oMatrix1.Columns.Item("ItemCode").Cells.Item(1).Specific.value = itemcode
                oMatrix1.Columns.Item("ItemName").Cells.Item(1).Specific.value = objAddOn.getSingleValue("select ItemName from OITM where Itemcode='" & itemcode & "'")
                oMatrix1.Columns.Item("Whse").Cells.Item(1).Specific.value = WhsCde
                oMatrix1.Columns.Item("WhsNam").Cells.Item(1).Specific.value = WhsNam
                oMatrix1.Columns.Item("LineId").Cells.Item(1).Specific.value = 1
            Else
                frmserbat.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                frmserbat.Items.Item("t_DocNum").Specific.value = docno.ToString.Trim
                frmserbat.Items.Item("1").Click()

            End If

            If (frmmode.ToString.Trim = "4") Then
                frmserbat.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
            End If


        Catch ex As Exception
            LogInfo("Form Load Method Failed:" & ex.Message, "E")
        Finally
            If Not frmserbat Is Nothing Then _
            frmserbat.Freeze(False)
        End Try
    End Sub



    Public Sub InitForm()
        Try

            'Series
            objAddOn.LoadComboBoxSeries(frmserbat.Items.Item("c_series").Specific, "BSN") ' Load the Combo Box Series
            'Date
            objAddOn.LoadDocumentDate(frmserbat.Items.Item("DocDate").Specific) ' Load Document Date 


            frmserbat.Items.Item("t_DocNum").Enabled = False



        Catch ex As Exception
            LogInfo("InitForm :" + ex.Message, "E")
        Finally
        End Try
    End Sub
    Public Sub DefineModesForFields()
        Try
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText(("Define Modes For Fields Method Failed:" & ex.Message), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub



    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType

                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmserbat.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmserbat.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Value.ToString.Trim
                                    Dim strDocNum As Long = frmserbat.BusinessObject.GetNextSerialNumber(strSerialCode, "BSN")
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                        End Select
                    Catch ex As Exception
                        LogInfo("ItemEvent-Activate :" + ex.Message, "E")

                    End Try


                Case (SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED)
                    Try
                        Select Case pVal.ColUID


                        End Select
                    Catch ex As Exception
                        LogInfo("ItemEvent-Activate :" + ex.Message, "E")

                    End Try






                Case SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE
                    Try
                    Catch ex As Exception
                        LogInfo("ItemEvent-Activate :" + ex.Message, "E")
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        If pVal.BeforeAction Then
                            Select Case oCFLE.ChooseFromListUID

                                Case "21"

                            End Select
                        End If

                        If Not oDataTable Is Nothing And pVal.BeforeAction = False Then
                            Select Case oCFLE.ChooseFromListUID
                                Case "11"

                            End Select
                        End If



                        If Not oDataTable Is Nothing And pVal.BeforeAction = False And pVal.ActionSuccess Then
                            Select Case oCFLE.ChooseFromListUID
                                Case "11"
                            End Select
                        End If

                    Catch ex As Exception
                        LogInfo("Choose From List Event Failed:" & ex.Message, "E")
                    Finally
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.BeforeAction And (frmserbat.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or _
                                                          frmserbat.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then

                                    If ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Return
                                    Else
                                        'If Not objAddOn.objCompany.InTransaction Then objAddOn.objCompany.StartTransaction()
                                        ''>>~~Start Log                                  
                                        'LogInfo("********* TRANS BEGIN **************")
                                        ''>>~~End Log     
                                        'If frmserbat.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                        '    objAddOn.DeleteEmptyRowInFormDataEvent(oMatrix1, "Qty", oDBDSDetails)
                                        'End If
                                    End If

                                End If

                        End Select
                    Catch ex As Exception
                        LogInfo("Click Event Failed:" & ex.Message, "E")
                        'If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        If pVal.ItemUID = "1" Then
                            If pVal.BeforeAction = False And pVal.ActionSuccess Then
                                '  Me.InitForm()
                                If (frmserbat.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmserbat.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                                    frmserbat.Items.Item("2").Click()
                                End If


                            End If
                        End If

                        If pVal.ItemUID = "b_load" Then
                            If (pVal.BeforeAction = False And manageddoc = "Serial") Then
                                If (frmserbat.Items.Item("t_prefix").Specific.value = "") Then
                                    LogInfo("Enter the prefix...", "E")
                                    BubbleEvent = False
                                    Exit Sub

                                End If

                                If (frmserbat.Items.Item("t_suffix").Specific.value = "") Then
                                    LogInfo("Enter the Suffix...", "E")
                                    BubbleEvent = False
                                    Exit Sub
                                End If


                                For lcount As Integer = 0 To frmserbat.Items.Item("t_totqty").Specific.value.ToString.Trim - 1


                                    If (oMatrix1.VisualRowCount <= frmserbat.Items.Item("t_totqty").Specific.value.ToString.Trim) Then
                                        oMatrix1.AddRow()
                                    End If
                                    If (oMatrix1.Columns.Item("ItemCode").Cells.Item(lcount + 1).Specific.value = "") Then
                                        oMatrix1.Columns.Item("ItemCode").Cells.Item(lcount + 1).Specific.value = oMatrix1.Columns.Item("ItemCode").Cells.Item(1).Specific.value
                                        oMatrix1.Columns.Item("ItemName").Cells.Item(lcount + 1).Specific.value = oMatrix1.Columns.Item("ItemName").Cells.Item(1).Specific.value
                                    End If

                                    oMatrix1.Columns.Item("Qty").Cells.Item(lcount + 1).Specific.value = 1
                                    oMatrix1.Columns.Item("LineId").Cells.Item(lcount + 1).Specific.value = lcount + 1
                                    oMatrix1.Columns.Item("batchser").Cells.Item(lcount + 1).Specific.value = frmserbat.Items.Item("t_prefix").Specific.value.ToString.Trim + CStr(lcount + 1) + frmserbat.Items.Item("t_to").Specific.value.ToString.Trim + frmserbat.Items.Item("t_suffix").Specific.value.ToString.Trim + frmserbat.Items.Item("t_DocNum").Specific.value.ToString.Trim
                                    '  oMatrix1.Columns.Item("").Cells.Item(lcount +1).Specific.value=oMatrix1.Columns.Item("").Cells.Item("").Specific.value=


                                Next

                            End If
                        End If



                    Catch ex As Exception
                        LogInfo("Item Press Event Faild:" & ex.Message, "E")
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    Try
                        If pVal.BeforeAction = True And Not str_Active.Contains("/\" & objAddOn.objApplication.Forms.ActiveForm.UniqueID & "/\") Then
                        End If
                    Catch ex As Exception
                        LogInfo("ItemEvent-Activate :" + ex.Message, "E")
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try
                        If (pVal.BeforeAction = False) Then
                            Select Case pVal.ColUID
                                Case "Qty"
                                    If oMatrix1.VisualRowCount = pVal.Row And oMatrix1.VisualRowCount > 0 And oMatrix1.Columns.Item("Qty").Cells.Item(pVal.Row).Specific.value.ToString.Trim <> "" Then
                                        objAddOn.SetNewLine(oMatrix1, oDBDSDetails, pVal.Row, "ItemCode")
                                        oMatrix1.Columns.Item("ItemCode").Cells.Item(pVal.Row + 1).Specific.value = oMatrix1.Columns.Item("ItemCode").Cells.Item(1).Specific.value
                                        oMatrix1.Columns.Item("ItemName").Cells.Item(pVal.Row + 1).Specific.value = oMatrix1.Columns.Item("ItemName").Cells.Item(1).Specific.value
                                        oMatrix1.Columns.Item("Whse").Cells.Item(pVal.Row + 1).Specific.value = oMatrix1.Columns.Item("Whse").Cells.Item(1).Specific.value
                                        oMatrix1.Columns.Item("WhsNam").Cells.Item(pVal.Row + 1).Specific.value = oMatrix1.Columns.Item("WhsNam").Cells.Item(1).Specific.value

                                        oMatrix1.Columns.Item("LineId").Cells.Item(pVal.Row + 1).Specific.value = pVal.Row + 1

                                        '   oMatrix1.Columns.Item("totqty").Cells.Item(pVal.Row + 1).Specific.value = oMatrix1.Columns.Item("totqty").Cells.Item(1).Specific.value

                                    End If

                            End Select
                        End If

                    Catch ex As Exception
                        LogInfo("ItemEvent-Activate :" + ex.Message, "E")

                    End Try

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        If pVal.BeforeAction = False Then
                        End If
                    Catch ex As Exception
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        If pVal.BeforeAction = False Then
                            Select Case pVal.ColUID
                                Case "AsInBrid"
                                    'MsgBox(oMatrix1.Columns.Item("NetWeight").Cells.Item(pVal.Row).Specific.value - oMatrix1.Columns.Item("AsInBrid").Cells.Item(pVal.Row).Specific.value)
                                    'oMatrix1.Columns.Item("Differ").Cells.Item(pVal.Row).Specific.value = 100
                                    ' (oMatrix1.Columns.Item("NetWeight").Cells.Item(pVal.Row).Specific.value) - (oMatrix1.Columns.Item("AsInBrid").Cells.Item(pVal.Row).Specific.value)
                            End Select

                        End If
                    Catch ex As Exception

                    End Try

                Case SAPbouiCOM.BoEventTypes.et_FORM_RESIZE
                    Try
                        If pVal.BeforeAction = False And pVal.ActionSuccess Then

                            If Not frmserbat Is Nothing Then

                                frmserbat.Freeze(True)
                                ' Resize_Form()
                                frmserbat.Freeze(False)

                            End If
                        End If

                    Catch ex As Exception
                        'LogInfo("ItemEvent -Item Resize:" + ex.Message, "E")
                    End Try
            End Select
        Catch ex As Exception
            LogInfo("ItemEvent:" + ex.Message, "E")
        End Try

    End Sub



    Public Function ValidateAll() As Boolean
        Try
            Dim sumofqty As Double = 0


            Dim count = oMatrix1.VisualRowCount
            For i As Integer = count To 1 Step -1
                If oMatrix1.Columns.Item("batchser").Cells.Item(i).Specific.value = "" Then
                    oMatrix1.DeleteRow(i)
                End If
            Next

 

            Return True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText(("Validate Function Failed:" & ex.Message), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End Try
    End Function
    Public Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    ' InitForm()
                    Exit Select
                Case "1281"
                    Exit Select
            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText(("Menu Event Failed:" & ex.Message), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        If BusinessObjectInfo.BeforeAction = False Then
                        End If
                    Catch ex As Exception
                        LogInfo("Data Load Event Faild : " & ex.Message, "E")
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    If BusinessObjectInfo.BeforeAction Then
                        If Me.ValidateAll() = False Then
                            BubbleEvent = False
                            System.Media.SystemSounds.Asterisk.Play()
                            Return
                        Else
                        End If
                    Else
                        If (BusinessObjectInfo.ActionSuccess) Then



                            Dim oRS_RouteDetails As SAPbobsCOM.Recordset
                            oRS_RouteDetails = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            Dim sQuery As String = "select Max(DocNum)DocNum  from [@AIS_RVL_OBSN] Where UserSign ='" + objAddOn.objCompany.UserSignature.ToString() + "'"
                            oRS_RouteDetails.DoQuery(sQuery)
                            oRS_RouteDetails.MoveFirst()
                            objAddOn.objProductionReceipt.omatrixRM.Columns.Item("V_8").Cells.Item(CInt(GRNLineID)).Specific.value = oRS_RouteDetails.Fields.Item("DocNum").Value
                        End If
                    End If
            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

 


    Function LogInfo(ByVal str_Msg As String, Optional ByVal str_Type As String = "I")
        Try
            'If str_Type <> "I" Then
            '    objAddOn.Msg(str_Msg)
            'End If
            'If str_log_Info Then
            '    Dim deepestMethod As MethodBase = New StackTrace(1).GetFrame(0).GetMethod()
            '    Dim strClassName As String = New StackTrace(1).GetFrame(0).GetMethod().ReflectedType.Name
            '    Select Case str_Type
            '        Case "I", "E"
            '            If str_Session = 0 Then
            '                str_Session = CInt(objAddOn.getSingleValue(" Select ISNULL(Max(ISNULL(convert(int,U_Session),0)),0) + 1 Code From [@SAHAJ_OLOG] "))
            '            End If
            '            Dim str_sql As String = _
            '                                    "INSERT INTO [@SAHAJ_OLOG]([Code],[DocEntry],[U_UserId],[U_DateTime], " & _
            '                                    " [U_ProjName], [U_ClsName], [U_ProcName], [U_ErrMsg], [U_ErrType],[U_Session]) " & _
            '                                    " VALUES((Select ISNULL(Max(ISNULL(DocEntry,0)),0) + 1 Code From [@SAHAJ_OLOG]), " & _
            '                                    " (Select ISNULL(Max(ISNULL(DocEntry,0)),0) + 1 Code From [@SAHAJ_OLOG]),'" & _
            '                                    objAddOn.objCompany.UserSignature & "',GETDATE (),'" & _
            '                                    strClassName & "','" & deepestMethod.Name & "','" & _
            '                                    str_Msg.Replace("'", "''") & "','" & str_Type & "','" & str_Session & "');"
            '            If objAddOn.objCompany.InTransaction Then
            '                str_Log_Info_In_Tran = str_Log_Info_In_Tran + str_sql
            '            ElseIf str_Log_Info_In_Tran.Trim <> "" Then
            '                Dim tmp_sql As String = str_Log_Info_In_Tran
            '                str_Log_Info_In_Tran = ""
            '                objAddOn.DoQuery(tmp_sql)
            '            End If
            '            objAddOn.DoQuery(str_sql)
            '    End Select
            'End If

        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Function








End Class

