﻿Public Class clsGeneralSetting

    Public frmGeneralSetting As SAPbouiCOM.Form
    Dim oDBDSHeader As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim Ors As SAPbobsCOM.Recordset
    Dim oMatrix As SAPbouiCOM.Matrix
    'Dim UDOID As String = "WIPGT"
    Public Const FormType = "WIPGT"


    Sub LoadForm()
        Try
            frmGeneralSetting = objAddOn.objUIXml.LoadScreenXML("GeneralSetting.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE

            Dim str As String = "select Top 1 (U_mchgrp) ,code from   [@AS_WIPGT]"
            Ors = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Ors.DoQuery(str)
            If Ors.RecordCount > 0 Then
                Dim STR1 As String = Ors.Fields.Item("Code").Value


                frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                'frmGeneralSetting.DataBrowser.BrowseBy = "9"
                frmGeneralSetting.Items.Item("9").Visible = True
                frmGeneralSetting.Items.Item("9").Specific.string = STR1
                frmGeneralSetting.Items.Item("1").Click()
                frmGeneralSetting.Items.Item("9").Visible = False
                frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE


            End If
        Catch ex As Exception
            objAddOn.Msg("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub


   

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        'Assign Selected Rows
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        'Filter before open the CFL
                        'If pVal.BeforeAction = True Then
                        '    Select Case oCFLE.ChooseFromListUID
                        '        Case "Cfl_mch"
                        '            objAddOn.ChooseFromListFilteration(frmGeneralSetting, "Cfl_mch", "Code", "select Code from [@AS_OMGR]")
                        '        Case "cfl_Tol"
                        '            objAddOn.ChooseFromListFilteration(frmGeneralSetting, "cfl_Tol", "Code", "select Code from [@AS_OTGR]")
                        '    End Select

                        'End If
                        If pVal.BeforeAction = False And Not oDataTable Is Nothing Then
                            'Assign Value to selected control
                            Dim mchGrpCod, ToolGrpCod, Whscd As String
                          
                            Select Case oCFLE.ChooseFromListUID
                                Case "Cfl_mch"

                                    mchGrpCod = oDataTable.GetValue("ItmsGrpCod", 0)
                                    frmGeneralSetting.Items.Item("Machine").Specific.string = mchGrpCod.ToString
                                    frmGeneralSetting.Items.Item("11").Specific.string = oDataTable.GetValue("ItmsGrpNam", 0)
                                Case "cfl_Tol"
                                    ToolGrpCod = oDataTable.GetValue("ItmsGrpCod", 0)
                                    frmGeneralSetting.Items.Item("Tools").Specific.string = ToolGrpCod.ToString
                                    frmGeneralSetting.Items.Item("13").Specific.string = oDataTable.GetValue("ItmsGrpNam", 0)
                                Case "whs"
                                    Try
                                        Whscd = oDataTable.GetValue("WhsCode", 0)
                                        Dim oed As SAPbouiCOM.EditText

                                        oed = frmGeneralSetting.Items.Item("Whs").Specific
                                        oed.String = Whscd.ToString
                                        oed = frmGeneralSetting.Items.Item("15").Specific
                                        oed.String = oDataTable.GetValue("WhsName", 0)
                                    Catch ex As Exception

                                    End Try
                            End Select                          '  

                        End If

                    Catch ex As Exception
                        ' objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try


               

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                frmGeneralSetting.Items.Item("9").Specific.value = 1
                                'Add,Update Event
                                If pVal.BeforeAction = True And (frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                                    If Me.ValidateAll() = False Then
                                        'System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Exit Sub
                                    End If
                                End If
                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        BubbleEvent = False
                    Finally
                    End Try

                    'changed by sankaralakshmi

              
            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                
                
            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE

                    Dim rsetUpadteIndent As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Try
                        If BusinessObjectInfo.BeforeAction Then
                            If Me.ValidateAll() = False Then
                                System.Media.SystemSounds.Asterisk.Play()
                                BubbleEvent = False
                                Exit Sub
                                'Else
                                '    'Delete Last Empty Row
                                '    objAddOn.DeleteEmptyRowInFormDataEvent(oMatrix, "AccWhCode", oDBDSDetail)
                                '    If frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                '        oDBDSHeader.SetValue("Code", 0, objAddOn.GetCodeGeneration("[@AIS_OWIPGT]"))
                                '    End If
                            End If
                        End If
                        If BusinessObjectInfo.ActionSuccess = True Then
                            'frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                            'objaddon.ObjApplication.Menus.Item("1282").Activate()

                            'Dim code = ObjAddon.getSingleValue("Select top 1 Code from [@AIS_OWIPGT] ")
                            'code = IIf(code.Trim = "", 0, code)

                            'oMatrix.Clear()
                            '    oDBDSDetail.Clear()
                            '    ObjAddon.SetNewLine(oMatrix, oDBDSDetail)
                            '    ObjAddon.setComboBoxValue(oMatrix.GetCellSpecific("LocCode", 1), "Select Code, Location from OLCT ")

                            '    frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                            '    frmGeneralSetting.Items.Item("t_Code").Specific.Value = code
                            '    frmGeneralSetting.Items.Item("1").Click()

                        End If
                    Catch ex As Exception
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If BusinessObjectInfo.ActionSuccess = True Then
                        objAddOn.SetNewLine(oMatrix, oDBDSDetail)
                    End If
            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function ValidateAll() As Boolean
        Try



            If frmGeneralSetting.Items.Item("Machine").Specific.value.ToString.Trim.Equals("") = True Then
                objAddOn.Msg("Machine Group  Should Not Be Left Empty")
                frmGeneralSetting.ActiveItem = "Machine"
                Return False
            End If
            If frmGeneralSetting.Items.Item("Tools").Specific.value.ToString.Trim.Equals("") = True Then
                objAddOn.Msg(" Parts Group Should Not Be Left Empty")
                frmGeneralSetting.ActiveItem = "Tools"
                Return False
            End If
            ValidateAll = True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

End Class
