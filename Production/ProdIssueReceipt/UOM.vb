﻿'Public Class UOM


'    Public frmUOM, frmBRM, frmBMC, frmBTL, frmBSC, frmBCO, frmBLR As SAPbouiCOM.Form
'    Dim oDBDSHeader, oDBDSDetail, oDBDSDetail_BRM, oDBDSDetail_BMC, oDBDSDetail_BTL, oDBDSDetail_BSC, oDBDSDetail_BCO, oDBDSDetail_BLR, oDBDSDetail_SBLR As SAPbouiCOM.DBDataSource
'    Dim oMatrix, oMatrix_BRM, oMatrix_BMC, oMatrix_BTL, oMatrix_BSC, oMatrix_BCO, oMatrix_BLR, oMatrix_SBLR As SAPbouiCOM.Matrix
'    Dim DataTable_BRM, DataTable_BMC, DataTable_BTL, DataTable_BSC, DataTable_BCO, DataTable_BLR, DataTable_SBLR As New DataTable
'    Dim ParentCode As String = ""
'    Dim CCode As String
'    Dim CurrentRow As Integer = 0
'    Dim PLineId_BRM, PLineId_BMC, PLineId_BTL, PLineId_BSC, PLineId_BCO, PLineId_BLR As Integer
'    Dim oDataTable As SAPbouiCOM.DataTable
'    Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
'    Dim CurrentMaterailRow, CurrentMaterial As String

'    Sub LoadForm()
'        Try
'            frmUOM = objAddOn.objApplication.Forms.ActiveForm
'            UOMTypeEx = frmUOM.UniqueID
'            'Assign Data Source
'            oDBDSHeader = frmUOM.DataSources.DBDataSources.Item(0)
'            oDBDSDetail = frmUOM.DataSources.DBDataSources.Item(1)
'            oMatrix = frmUOM.Items.Item("3").Specific

'            Me.InitForm()
'            Me.DefineModesForFields()
'        Catch ex As Exception
'            objAddOn.Msg("Load Form Method Failed:" & ex.Message)
'        End Try
'    End Sub

'    Sub InitForm()
'        Try
'            frmUOM.Freeze(True)
'            frmUOM.Title = "Root Card Master"


'            'BOM Raw Material
'            DataTable_BRM.Columns.Clear()
'            DataTable_BRM.Columns.Add("ParentCode")
'            DataTable_BRM.Columns.Add("CCode")
'            DataTable_BRM.Columns.Add("PLineId")
'            DataTable_BRM.Columns.Add("ItemCode")
'            DataTable_BRM.Columns.Add("ItemName")
'            DataTable_BRM.Columns.Add("UOM")
'            DataTable_BRM.Columns.Add("BQty")
'            DataTable_BRM.Columns.Add("UCost")
'            DataTable_BRM.Columns.Add("STotal")
'            DataTable_BRM.Columns.Add("ParentMac")
'            DataTable_BRM.Columns.Add("WhsCode")

'            'BOM Machinery
'            DataTable_BMC.Columns.Clear()
'            DataTable_BMC.Columns.Add("ParentCode")
'            DataTable_BMC.Columns.Add("CCode")
'            DataTable_BMC.Columns.Add("PLineId")
'            DataTable_BMC.Columns.Add("ItemCode")
'            DataTable_BMC.Columns.Add("OpCode")
'            DataTable_BMC.Columns.Add("Oprn")
'            DataTable_BMC.Columns.Add("ItemName")
'            DataTable_BMC.Columns.Add("SetTime")
'            DataTable_BMC.Columns.Add("SetupMs")
'            DataTable_BMC.Columns.Add("MProsTime")
'            DataTable_BMC.Columns.Add("MProceMs")
'            DataTable_BMC.Columns.Add("TotTime")
'            DataTable_BMC.Columns.Add("PerHrRt")
'            DataTable_BMC.Columns.Add("STotal")

'            'BOM Tools
'            DataTable_BTL.Columns.Clear()
'            DataTable_BTL.Columns.Add("ParentCode")
'            DataTable_BTL.Columns.Add("CCode")
'            DataTable_BTL.Columns.Add("PLineId")
'            DataTable_BTL.Columns.Add("ItemCode")
'            DataTable_BTL.Columns.Add("ItemName")
'            DataTable_BTL.Columns.Add("ToolType")
'            DataTable_BTL.Columns.Add("Rate")
'            DataTable_BTL.Columns.Add("LifeTime")
'            DataTable_BTL.Columns.Add("Qty")
'            DataTable_BTL.Columns.Add("UCost")
'            DataTable_BTL.Columns.Add("STotal")

'            'BOM Scrap
'            DataTable_BSC.Columns.Clear()
'            DataTable_BSC.Columns.Add("ParentCode")
'            DataTable_BSC.Columns.Add("CCode")
'            DataTable_BSC.Columns.Add("PLineId")
'            DataTable_BSC.Columns.Add("ItemCode")
'            DataTable_BSC.Columns.Add("ItemName")
'            DataTable_BSC.Columns.Add("UOM")
'            DataTable_BSC.Columns.Add("BQty")
'            DataTable_BSC.Columns.Add("UCost")
'            DataTable_BSC.Columns.Add("STotal")

'            'BOM Consumables
'            DataTable_BCO.Columns.Clear()
'            DataTable_BCO.Columns.Add("ParentCode")
'            DataTable_BCO.Columns.Add("CtCode")
'            DataTable_BCO.Columns.Add("PLineId")
'            DataTable_BCO.Columns.Add("ItemCode")
'            DataTable_BCO.Columns.Add("ItemName")
'            DataTable_BCO.Columns.Add("UOM")
'            DataTable_BCO.Columns.Add("BQty")
'            DataTable_BCO.Columns.Add("UCost")
'            DataTable_BCO.Columns.Add("STotal")

'            'BOM Labor
'            DataTable_BLR.Columns.Clear()
'            DataTable_BLR.Columns.Add("ParentCode")
'            DataTable_BLR.Columns.Add("CCode")
'            DataTable_BLR.Columns.Add("PLineId")
'            DataTable_BLR.Columns.Add("LbrGrpCode")
'            DataTable_BLR.Columns.Add("LbrGrpName")
'            DataTable_BLR.Columns.Add("LProsTime")
'            DataTable_BLR.Columns.Add("LProceMs")
'            DataTable_BLR.Columns.Add("PerHrRt")
'            DataTable_BLR.Columns.Add("STotal")

'            'Sub BOM Labor
'            DataTable_SBLR.Columns.Clear()
'            DataTable_SBLR.Columns.Add("ParentCode")
'            DataTable_SBLR.Columns.Add("CCode")
'            DataTable_SBLR.Columns.Add("PLineId")
'            DataTable_SBLR.Columns.Add("LbrGrpCode")
'            DataTable_SBLR.Columns.Add("LbrGrpName")
'            DataTable_SBLR.Columns.Add("LProsTime")
'            DataTable_SBLR.Columns.Add("LProceMs")
'            DataTable_SBLR.Columns.Add("PerHrRt")
'            DataTable_SBLR.Columns.Add("STotal")
'            DataTable_SBLR.Columns.Add("ParentMac")
'            ' oMatrix.Columns.Item("U_SeqNo").Editable = False

'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        Finally
'            frmUOM.Freeze(False)
'        End Try
'    End Sub

'    Sub DefineModesForFields()
'        Try
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        Finally
'        End Try
'    End Sub

'    Function ValidateAll() As Boolean
'        Try
'            ValidateAll = True
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'            ValidateAll = False
'        Finally
'        End Try

'    End Function

'    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
'        Try
'            If frmUOM Is Nothing Then
'                frmUOM = objAddOn.objApplication.Forms.Item(FormUID)
'            End If
'            Select Case pVal.EventType
'                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
'                    If pVal.BeforeAction = False Then
'                        If pVal.ItemUID = "3" And pVal.ColUID = "U_Masters" Then
'                            ParentCode = frmUOM.Items.Item("4").Specific.Value
'                            PLineId_BMC = pVal.Row
'                            PLineId_BLR = pVal.Row
'                            PLineId_BTL = pVal.Row
'                            CCode = oMatrix.Columns.Item("1").Cells.Item(pVal.Row).Specific.string
'                            Dim Po As String = "1"
'                            'If Po = objAddOn.getSingleValue("Select 1 from OITT where Code = '" & CCode & "' and U_POorNot='Y'") Then
'                            LoadForm_Master()
'                            'End If
'                        End If
'                    End If

'                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
'                    If pVal.ItemUID = "3" And pVal.ColUID = "U_Master" Then
'                        If pVal.BeforeAction = True Then
'                            Try
'                                oMatrix = frmUOM.Items.Item("3").Specific
'                                oMatrix.Columns.Item("U_Master").Editable = True
'                                If oMatrix.RowCount > 0 Then
'                                    For i = 1 To oMatrix.VisualRowCount
'                                        oMatrix.Columns.Item("U_Master").Cells.Item(pVal.Row).Specific.String = "Link"
'                                    Next
'                                End If
'                            Catch ex As Exception

'                            End Try

'                        End If

'                    End If


'                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
'                    Try
'                        'Assign Selected Rows
'                        Dim oDataTable As SAPbouiCOM.DataTable
'                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
'                        oDataTable = oCFLE.SelectedObjects
'                        Dim rset As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
'                        'Filter before open the CFL
'                        If pVal.BeforeAction Then
'                            Select Case oCFLE.ChooseFromListUID
'                                Case "1"
'                                    'objAddOn.ChooseFromListFilteration(frmUOM, "1", "ItemCode", "Select Itemcode from OITM --where  ItmsGrpCod = '" & Operations & "'")
'                            End Select
'                        End If
'                    Catch ex As Exception
'                        objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                    Finally
'                    End Try

'                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
'                    Try
'                        Select Case pVal.ColUID
'                            Case "U_SeqNo"
'                                If pVal.BeforeAction Then
'                                    Dim strSeqList = ""
'                                    For i As Integer = 1 To oMatrix.VisualRowCount - 1

'                                        Dim strSeq = oMatrix.Columns.Item("U_SeqNo").Cells.Item(i).Specific.value.ToString.Trim

'                                        If strSeq.Trim = "" Then Continue For

'                                        If strSeqList.Contains("/\" + strSeq + "/\") = True Then
'                                            objAddOn.Msg("Line No." & i & " Sequance No. Should Not Be Duplicate")
'                                            BubbleEvent = False
'                                            Return
'                                        End If
'                                        strSeqList = strSeqList + "/\" + strSeq + "/\"
'                                    Next
'                                End If
'                        End Select
'                    Catch ex As Exception
'                        objAddOn.objApplication.StatusBar.SetText("Validate Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                    End Try

'                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
'                    Try
'                        If pVal.BeforeAction = False Then
'                            Select Case pVal.ColUID
'                                Case "1"
'                                    If oMatrix.Columns.Item("1").Cells.Item(pVal.Row).Specific.string <> "" Then
'                                        oMatrix.Columns.Item("U_SeqNo").Cells.Item(pVal.Row).Specific.value = pVal.Row
'                                        oMatrix.Columns.Item("U_Masters").Editable = True
'                                        oMatrix.Columns.Item("U_Masters").Cells.Item(pVal.Row).Specific.String = "Link"
'                                        oMatrix.Columns.Item("48").Cells.Item(pVal.Row).Specific.String = ""
'                                        oMatrix.Columns.Item("U_Masters").Editable = False
'                                    End If
'                                Case "3"
'                                    OperationCal(pVal.Row)
'                            End Select
'                        End If
'                        If pVal.BeforeAction = False And (pVal.ColUID = "U_OthrCost" Or pVal.ColUID = "U_InsCost") Then
'                            OperationCal(pVal.Row)
'                        End If

'                    Catch ex As Exception
'                        objAddOn.objApplication.StatusBar.SetText("Validate Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                    End Try
'                Case SAPbouiCOM.BoEventTypes.et_CLICK
'                    Try
'                        Select Case pVal.ItemUID
'                            Case "1"
'                                'Add,Update Event
'                                If pVal.BeforeAction = True And (frmUOM.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmUOM.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
'                                    If Me.ValidateAll() = False Then
'                                        System.Media.SystemSounds.Asterisk.Play()
'                                        BubbleEvent = False
'                                        Exit Sub
'                                    End If
'                                End If
'                        End Select
'                    Catch ex As Exception
'                        objAddOn.objApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                        objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
'                        BubbleEvent = False
'                    Finally
'                    End Try
'            End Select
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("Item Event Methord Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        Finally
'        End Try
'    End Sub

'    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
'        Try
'            If UOMTypeEx <> "" Then
'                Select Case pVal.MenuUID
'                    Case "1282"
'                        Me.InitForm()
'                    Case "1293"
'                        objAddOn.DeleteRow(oMatrix, oDBDSDetail)
'                        'Case BOMRawMaterialFormId
'                        '    PLineId_BRM = CurrentRow
'                        '    LoadForm_BRM()
'                        'Case BOMMachineryFormId
'                        '    PLineId_BMC = CurrentRow
'                        '    LoadForm_BMC()
'                        'Case BOMToolsFormId
'                        '    ParentCode = frmUOM.Items.Item("4").Specific.Value
'                        '    PLineId_BTL = CurrentRow
'                        '    LoadForm_BTL()
'                        'Case BOMScrapFormId
'                        '    ParentCode = frmUOM.Items.Item("4").Specific.Value
'                        '    PLineId_BSC = CurrentRow
'                        '    LoadForm_BSC()
'                        'Case BOMConsumablesFormId
'                        '    ParentCode = frmUOM.Items.Item("4").Specific.Value
'                        '    PLineId_BCO = CurrentRow
'                        '    LoadForm_BCO()
'                        'Case BOMLaborFormId
'                        'ParentCode = frmUOM.Items.Item("4").Specific.Value
'                        'PLineId_BMC = CurrentRow
'                        'PLineId_BLR = CurrentRow
'                        'PLineId_BTL = CurrentRow
'                        'LoadForm_Master()

'                End Select
'                ParentCode = frmUOM.Items.Item("4").Specific.Value
'            End If


'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        Finally
'        End Try
'    End Sub

'    Sub RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
'        Try
'            Select Case eventInfo.ItemUID
'                Case "3"
'                    oMatrix = frmUOM.Items.Item("3").Specific
'                    If eventInfo.Row <> oMatrix.VisualRowCount And _
'                        eventInfo.BeforeAction And _
'                        oMatrix.GetCellSpecific("U_SeqNo", eventInfo.Row).Value.ToString.Trim <> "" And _
'                        frmUOM.Items.Item("6").Specific.Value.ToString.Trim = "P" Then

'                        'objAddOn.setRightMenu(BOMRawMaterialFormId, "Raw Material")
'                        'objAddOn.setRightMenu(BOMMachineryFormId, "Machinery")
'                        'objAddOn.setRightMenu(BOMToolsFormId, "Tools")
'                        'objAddOn.setRightMenu(BOMScrapFormId, "Scrap")
'                        'objAddOn.setRightMenu(BOMConsumablesFormId, "Consumables")
'                        'objAddOn.setRightMenu(BOMLaborFormId, "Labor")
'                        'CurrentRow = oMatrix.GetCellSpecific("U_SeqNo", eventInfo.Row).Value

'                    End If
'            End Select
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        End Try
'    End Sub

'#Region "--- Sub BOM Raw Material/Sub Labour ----"

'    Sub LoadForm_BRM()
'        Try
'            frmBRM = objAddOn.objUIXml.LoadScreenXML(BOMRawMaterialXML, Ananth.SBOLib.UIXML.enuResourceType.Embeded, BOMRawMaterialFormId)
'            'Assign Data Source
'            oDBDSDetail_BRM = frmBRM.DataSources.DBDataSources.Item(0)
'            oDBDSDetail_SBLR = frmBRM.DataSources.DBDataSources.Item(1)
'            'Assign Matrix
'            oMatrix_BRM = frmBRM.Items.Item("RMatrix").Specific
'            oMatrix_SBLR = frmBRM.Items.Item("LMatrix").Specific
'            Me.InitForm_BRM()
'            frmBRM.Items.Item("4").Click()
'        Catch ex As Exception
'            objAddOn.Msg("Load (BRM) Form Method Failed:" & ex.Message)
'        End Try
'    End Sub

'    Sub InitForm_BRM()
'        Try
'            frmBRM.Freeze(True)

'            objAddOn.SetNewLine(oMatrix_BRM, oDBDSDetail_BRM)

'            oMatrix_BRM.Columns.Item("STotal").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
'            For i As Integer = 0 To DataTable_BRM.Rows.Count - 1
'                If DataTable_BRM.Rows.Item(i)("ParentMac").ToString.Trim = CurrentMaterial And DataTable_BRM.Rows.Item(i)("CCode").ToString.Trim = CCode Then
'                    If DataTable_BRM.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BRM Then
'                        objAddOn.SetNewLine(oMatrix_BRM, oDBDSDetail_BRM)
'                        Dim row As Integer = oMatrix_BRM.VisualRowCount - 1
'                        ' Matrix Filling
'                        oMatrix_BRM.GetCellSpecific("ParentCode", row).Value = DataTable_BRM.Rows.Item(i)("ParentCode").ToString
'                        oMatrix_BRM.GetCellSpecific("PLineId", row).Value = DataTable_BRM.Rows.Item(i)("PLineId").ToString
'                        oMatrix_BRM.GetCellSpecific("ItemCode", row).Value = DataTable_BRM.Rows.Item(i)("ItemCode").ToString
'                        oMatrix_BRM.GetCellSpecific("ItemName", row).Value = DataTable_BRM.Rows.Item(i)("ItemName").ToString
'                        oMatrix_BRM.GetCellSpecific("UOM", row).Value = DataTable_BRM.Rows.Item(i)("UOM").ToString
'                        oMatrix_BRM.GetCellSpecific("BQty", row).Value = DataTable_BRM.Rows.Item(i)("BQty").ToString
'                        oMatrix_BRM.GetCellSpecific("UCost", row).Value = DataTable_BRM.Rows.Item(i)("UCost").ToString
'                        oMatrix_BRM.GetCellSpecific("STotal", row).Value = DataTable_BRM.Rows.Item(i)("STotal").ToString
'                        oMatrix_BRM.GetCellSpecific("WhsCode", row).Value = DataTable_BRM.Rows.Item(i)("WhsCode").ToString
'                    End If
'                End If

'            Next
'            objAddOn.SetNewLine(oMatrix_SBLR, oDBDSDetail_SBLR)

'            oMatrix_SBLR.Columns.Item("STotal").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
'            For i As Integer = 0 To DataTable_SBLR.Rows.Count - 1
'                If DataTable_SBLR.Rows.Item(i)("ParentMac").ToString.Trim = CurrentMaterial And DataTable_SBLR.Rows.Item(i)("CCode").ToString.Trim = CCode Then
'                    If DataTable_SBLR.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BRM Then
'                        objAddOn.SetNewLine(oMatrix_SBLR, oDBDSDetail_SBLR)
'                        Dim row As Integer = oMatrix_SBLR.VisualRowCount - 1
'                        ' Matrix Filling
'                        oMatrix_SBLR.GetCellSpecific("ParentCode", row).Value = DataTable_SBLR.Rows.Item(i)("ParentCode").ToString
'                        oMatrix_SBLR.GetCellSpecific("PLineId", row).Value = DataTable_SBLR.Rows.Item(i)("PLineId").ToString
'                        oMatrix_SBLR.GetCellSpecific("LbrGrpCode", row).Value = DataTable_SBLR.Rows.Item(i)("LbrGrpCode").ToString
'                        oMatrix_SBLR.GetCellSpecific("LbrGrpName", row).Value = DataTable_SBLR.Rows.Item(i)("LbrGrpName").ToString
'                        oMatrix_SBLR.GetCellSpecific("LProsTime", row).Value = DataTable_SBLR.Rows.Item(i)("LProsTime").ToString
'                        oMatrix_SBLR.GetCellSpecific("LProceMs", row).Select(DataTable_SBLR.Rows.Item(i)("LProceMs").ToString)
'                        oMatrix_SBLR.GetCellSpecific("PerHrRt", row).Value = DataTable_SBLR.Rows.Item(i)("PerHrRt").ToString
'                        oMatrix_SBLR.GetCellSpecific("STotal", row).Value = DataTable_SBLR.Rows.Item(i)("STotal").ToString
'                    End If
'                End If



'            Next

'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("InitForm (BRM) Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        Finally
'            frmBRM.Freeze(False)
'        End Try
'    End Sub

'    Function SubBom_validation() As Boolean
'        If oMatrix_SBLR.RowCount > 0 Then
'            For i = 1 To oMatrix_SBLR.RowCount
'                If oMatrix_SBLR.Columns.Item("LbrGrpCode").Cells.Item(i).Specific.String <> "" Then
'                    For j = 1 To oMatrix_SBLR.RowCount
'                        If i <> j Then
'                            If oMatrix_SBLR.Columns.Item("LbrGrpCode").Cells.Item(j).Specific.String = oMatrix_SBLR.Columns.Item("LbrGrpCode").Cells.Item(i).Specific.String Then
'                                frmBRM.Items.Item("5").Click()
'                                objAddOn.objApplication.SetStatusBarMessage("  This " & oMatrix_SBLR.Columns.Item("LbrGrpCode").Cells.Item(i).Specific.String & " Enter More Than Once ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
'                                Return True
'                            End If
'                        End If

'                    Next
'                    If oMatrix_SBLR.Columns.Item("LProsTime").Cells.Item(i).Specific.value = 0 Then
'                        frmBRM.Items.Item("5").Click()
'                        objAddOn.objApplication.SetStatusBarMessage("Please Enter Process Time For This " & oMatrix_SBLR.Columns.Item("LbrGrpCode").Cells.Item(i).Specific.String & " Item", SAPbouiCOM.BoMessageTime.bmt_Short, True)
'                        Return True
'                    End If
'                End If
'            Next
'        End If
'        If oMatrix_BRM.RowCount > 0 Then
'            For i = 1 To oMatrix_BRM.RowCount
'                If oMatrix_BRM.Columns.Item("ItemCode").Cells.Item(i).Specific.String <> "" Then
'                    For j = 1 To oMatrix_BRM.RowCount
'                        If i <> j Then
'                            If oMatrix_BRM.Columns.Item("ItemCode").Cells.Item(j).Specific.String = oMatrix_BRM.Columns.Item("ItemCode").Cells.Item(i).Specific.String Then
'                                frmBRM.Items.Item("4").Click()
'                                objAddOn.objApplication.SetStatusBarMessage("  This " & oMatrix_BRM.Columns.Item("ItemCode").Cells.Item(i).Specific.String & " Enter More Than Once ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
'                                Return True
'                            End If
'                        End If

'                    Next
'                    'If oMatrix_BRM.Columns.Item("Qty").Cells.Item(i).Specific.value = 0 Then
'                    '    frmBRM.Items.Item("4").Click()
'                    '    objAddOn.objApplication.SetStatusBarMessage("Please Enter The Quantity For " & oMatrix_BRM.Columns.Item("ItemCode").Cells.Item(i).Specific.String, SAPbouiCOM.BoMessageTime.bmt_Short, True)
'                    '    Return True
'                    'End If
'                End If
'            Next

'        End If
'        Return False

'    End Function

'    'Sub Load_RAWDataTable()
'    '    '------Raw Material-----------------------------------------------------------------------------------------------

'    '    Dim rs As SAPbobsCOM.Recordset
'    '    rs = objAddOn.DoQuery(" Select U_ParentCode ParentCode,U_PLineId PLineId, U_ItemCode ItemCode," & _
'    '                       " U_ItemName ItemName, U_UOM UOM, U_BQty BQty, U_UCost UCost, U_STotal STotal from [@AIS_OBRM] " & _
'    '                       " where  U_ParentCode ='" & ParentCode & "' and U_ParentMac ='" & CurrentMaterial & "'")
'    '    DataTable_BRM.Clear()
'    '    For i As Integer = 1 To rs.RecordCount

'    '        ' Data Table Filling
'    '        DataTable_BRM.Rows.Add()

'    '        DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("ParentCode") = rs.Fields.Item("ParentCode").Value.ToString
'    '        DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("PLineId") = rs.Fields.Item("PLineId").Value.ToString
'    '        DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("ItemCode") = rs.Fields.Item("ItemCode").Value.ToString
'    '        DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("ItemName") = rs.Fields.Item("ItemName").Value.ToString
'    '        DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("UOM") = rs.Fields.Item("UOM").Value.ToString
'    '        DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("BQty") = rs.Fields.Item("BQty").Value.ToString
'    '        DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("UCost") = rs.Fields.Item("UCost").Value.ToString
'    '        DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("STotal") = rs.Fields.Item("STotal").Value.ToString

'    '        rs.MoveNext()
'    '    Next

'    '    '--------Labor Details-------------------------------------------------------------------------------------------------

'    '    rs = objAddOn.DoQuery(" Select U_ParentCode ParentCode,U_PLineId PLineId, U_LbrGrpCode LbrGrpCode," & _
'    '                       " U_LbrGrpName LbrGrpName, U_ProsTime ProsTime, " & _
'    '                       " U_ProceMs ProceMs, U_PerHrRt PerHrRt, U_STotal STotal from [@AIS_SBLR] " & _
'    '                       "where  U_ParentCode ='" & ParentCode & "' and U_ParentMac ='" & CurrentMaterial & "'")

'    '    DataTable_SBLR.Clear()
'    '    For i As Integer = 1 To rs.RecordCount

'    '        ' Data Table Filling
'    '        DataTable_SBLR.Rows.Add()

'    '        DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("ParentCode") = rs.Fields.Item("ParentCode").Value.ToString
'    '        DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("PLineId") = rs.Fields.Item("PLineId").Value.ToString
'    '        DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("LbrGrpCode") = rs.Fields.Item("LbrGrpCode").Value.ToString
'    '        DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("LbrGrpName") = rs.Fields.Item("LbrGrpName").Value.ToString
'    '        DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("LProsTime") = rs.Fields.Item("ProsTime").Value.ToString
'    '        DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("LProceMs") = rs.Fields.Item("ProceMs").Value.ToString
'    '        DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("PerHrRt") = rs.Fields.Item("PerHrRt").Value.ToString
'    '        DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("STotal") = rs.Fields.Item("STotal").Value.ToString

'    '        rs.MoveNext()
'    '    Next


'    'End Sub

'    Sub ItemEvent_BRM(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
'        Try
'            If pVal.BeforeAction = True Then
'                Select Case pVal.EventType
'                    Case SAPbouiCOM.BoEventTypes.et_CLICK
'                        Try
'                            Select Case pVal.ItemUID
'                                Case "1"
'                                    '----------- Row Validation-----------------------------------------------------------------------------------------------
'                                    For i As Integer = 1 To oMatrix_BRM.VisualRowCount
'                                        If oMatrix_BRM.Columns.Item("ItemCode").Cells.Item(i).Specific.String <> "" Then
'                                            Dim BQty = oMatrix_BRM.GetCellSpecific("BQty", i).Value
'                                            Dim UCost = oMatrix_BRM.GetCellSpecific("UCost", i).Value
'                                            BQty = IIf(BQty.ToString.Trim = "", 0, BQty)
'                                            UCost = IIf(UCost.ToString.Trim = "", 0, UCost)

'                                            If CDbl(BQty) = 0 Then
'                                                objAddOn.Msg("Line No : " & i.ToString & " Please Enter Base Qty. ")
'                                                BubbleEvent = False
'                                                Return
'                                            End If

'                                            If CDbl(UCost) = 0 Then
'                                                objAddOn.Msg("Line No : " & i.ToString & " Please Enter Unit Cost ")
'                                                BubbleEvent = False
'                                                Return
'                                            End If
'                                        End If

'                                    Next
'                                    If SubBom_validation() = True Then
'                                        BubbleEvent = False
'                                        Return
'                                    End If

'                                    '-----------------------------------------------------------------------------------------------------------------------------
'                            End Select
'                        Catch ex As Exception
'                            objAddOn.objApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                        End Try
'                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
'                        Try

'                            Select Case pVal.ChooseFromListUID
'                                Case "cfl_LITEM"
'                                    objAddOn.ChooseFromListFilteration(frmBRM, "cfl_LITEM", "U_Code", "Select U_Code from [@AIS_OLBR] where U_Active = 'Y'")
'                                    'objAddOn.ChooseFromListFilteration(frmBRM, pVal.ChooseFromListUID, "ItemCode", "Select  ItemCode from oitm where ItemCode in (Select U_Code from [@AIS_OLGR])")
'                                Case "cfl_Whs"
'                                    objAddOn.ChooseFromListFilteration(frmBRM, pVal.ChooseFromListUID, "WhsCode", "Select WhsCode from oitw where ItemCode = '" & oMatrix_BRM.Columns.Item("ItemCode").Cells.Item(pVal.Row).Specific.string & "' ")
'                                Case "cfl_RITEM"
'                                    objAddOn.ChooseFromListFilteration(frmBRM, pVal.ChooseFromListUID, "ItemCode", "Select Itemcode from OITM where  ItmsGrpCod in (Select ItmsGrpCod  from oitb where U_ItmGrp = 'R')")
'                            End Select
'                        Catch ex As Exception
'                            objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                        Finally
'                        End Try

'                End Select
'            Else
'                Select Case pVal.EventType
'                    Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
'                        Try
'                            Select Case pVal.ColUID
'                                Case "LProceMs"
'                                    SUBLBRCal(pVal.Row)
'                            End Select
'                        Catch ex As Exception
'                            objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                        End Try
'                    Case SAPbouiCOM.BoEventTypes.et_CLICK
'                        If pVal.ItemUID = "4" Then
'                            frmBRM.PaneLevel = "1"
'                        End If
'                        If pVal.ItemUID = "5" Then
'                            frmBRM.PaneLevel = "2"
'                        End If
'                        If pVal.ItemUID = "1" Then
'                            Try

'                                'clear Data Table
'                                oMatrix_BRM.DeleteRow(oMatrix_BRM.VisualRowCount)
'10:
'                                For i As Integer = 0 To DataTable_BRM.Rows.Count - 1
'                                    If DataTable_BRM.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BRM And DataTable_BRM.Rows.Item(i)("ParentMac").ToString.Trim = CurrentMaterial Then
'                                        DataTable_BRM.Rows.RemoveAt(i)
'                                        GoTo 10
'                                    End If
'                                Next

'                                'Add Data Table
'                                Dim n As Integer = 0
'                                Dim RMCostSum As Double = 0
'                                Dim RowCount As Integer = DataTable_BRM.Rows.Count
'                                For i As Integer = RowCount To RowCount + oMatrix_BRM.VisualRowCount - 1

'                                    DataTable_BRM.Rows.Add()
'                                    n = n + 1

'                                    DataTable_BRM.Rows.Item(i)("ParentCode") = ParentCode
'                                    DataTable_BRM.Rows.Item(i)("CCode") = CCode
'                                    DataTable_BRM.Rows.Item(i)("PLineId") = PLineId_BRM
'                                    DataTable_BRM.Rows.Item(i)("ParentMac") = CurrentMaterial
'                                    DataTable_BRM.Rows.Item(i)("WhsCode") = oMatrix_BRM.GetCellSpecific("WhsCode", n).Value
'                                    DataTable_BRM.Rows.Item(i)("ItemCode") = oMatrix_BRM.GetCellSpecific("ItemCode", n).Value
'                                    DataTable_BRM.Rows.Item(i)("ItemName") = oMatrix_BRM.GetCellSpecific("ItemName", n).Value
'                                    DataTable_BRM.Rows.Item(i)("UOM") = oMatrix_BRM.GetCellSpecific("UOM", n).Value
'                                    DataTable_BRM.Rows.Item(i)("BQty") = oMatrix_BRM.GetCellSpecific("BQty", n).Value
'                                    DataTable_BRM.Rows.Item(i)("UCost") = oMatrix_BRM.GetCellSpecific("UCost", n).Value
'                                    DataTable_BRM.Rows.Item(i)("STotal") = oMatrix_BRM.GetCellSpecific("STotal", n).Value

'                                    Dim STotal = oMatrix_BRM.GetCellSpecific("STotal", n).Value
'                                    STotal = IIf(STotal.ToString.Trim = "", 0, STotal)
'                                    RMCostSum = RMCostSum + CDbl(STotal)

'                                Next
'                                'oMatrix.Columns.Item("U_RMCost").Cells.Item(PLineId_BRM).Specific.Value = RMCostSum
'                                'OperationCal(PLineId_BRM)


'                                'clear Data Table
'                                oMatrix_SBLR.DeleteRow(oMatrix_SBLR.VisualRowCount)
'c:
'                                For i As Integer = 0 To DataTable_SBLR.Rows.Count - 1
'                                    If DataTable_SBLR.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BRM And DataTable_SBLR.Rows.Item(i)("ParentMac").ToString.Trim = CurrentMaterial Then
'                                        DataTable_SBLR.Rows.RemoveAt(i)
'                                        GoTo c
'                                    End If
'                                Next

'                                'Add Data Table
'                                n = 0
'                                Dim LRCostSum As Double = 0
'                                RowCount = DataTable_SBLR.Rows.Count
'                                For i As Integer = RowCount To RowCount + oMatrix_SBLR.VisualRowCount - 1

'                                    DataTable_SBLR.Rows.Add()
'                                    n = n + 1

'                                    DataTable_SBLR.Rows.Item(i)("ParentCode") = ParentCode
'                                    DataTable_SBLR.Rows.Item(i)("CCode") = CCode
'                                    DataTable_SBLR.Rows.Item(i)("PLineId") = PLineId_BRM
'                                    DataTable_SBLR.Rows.Item(i)("ParentMac") = CurrentMaterial
'                                    DataTable_SBLR.Rows.Item(i)("LbrGrpCode") = oMatrix_SBLR.GetCellSpecific("LbrGrpCode", n).Value
'                                    DataTable_SBLR.Rows.Item(i)("LbrGrpName") = oMatrix_SBLR.GetCellSpecific("LbrGrpName", n).Value
'                                    DataTable_SBLR.Rows.Item(i)("LProsTime") = oMatrix_SBLR.GetCellSpecific("LProsTime", n).Value
'                                    DataTable_SBLR.Rows.Item(i)("LProceMs") = oMatrix_SBLR.GetCellSpecific("LProceMs", n).Value
'                                    DataTable_SBLR.Rows.Item(i)("PerHrRt") = oMatrix_SBLR.GetCellSpecific("PerHrRt", n).Value
'                                    DataTable_SBLR.Rows.Item(i)("STotal") = oMatrix_SBLR.GetCellSpecific("STotal", n).Value

'                                    Dim STotal = oMatrix_SBLR.GetCellSpecific("STotal", n).Value
'                                    STotal = IIf(STotal.ToString.Trim = "", 0, STotal)
'                                    LRCostSum = LRCostSum + CDbl(STotal)

'                                Next
'                                oMatrix.Columns.Item("U_LaborCost").Cells.Item(PLineId_BLR).Specific.Value = LRCostSum
'                                'oMatrix.Columns.Item("U_LaborCost").Cells.Item(PLineId_BLR).Specific.Value = LRCostSum
'                                'OperationCal(PLineId_BLR)


'                                If frmUOM.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmUOM.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
'                                frmBRM.Close()
'                            Catch ex As Exception
'                                objAddOn.objApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                            End Try
'                        End If


'                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
'                        Try
'                            Select Case pVal.ChooseFromListUID
'                                Case "cfl_Whs"
'                                    oMatrix_BRM.FlushToDataSource()
'                                    oDataTable = pVal.SelectedObjects
'                                    Dim n As Integer = pVal.Row - 1
'                                    'For Multi Row Select
'                                    For i As Integer = 0 To oDataTable.Rows.Count - 1
'                                        oDBDSDetail_BRM.SetValue("U_WhsCode", n, oDataTable.GetValue("WhsCode", i))
'                                    Next
'                                    oMatrix_BRM.LoadFromDataSourceEx()
'                                    'Assign Custemer
'                                Case "cfl_RITEM"
'                                    oMatrix_BRM.FlushToDataSource()
'                                    oDataTable = pVal.SelectedObjects
'                                    Dim n As Integer = pVal.Row - 1
'                                    'For Multi Row Select
'                                    For i As Integer = 0 To oDataTable.Rows.Count - 1
'                                        oDBDSDetail_BRM.SetValue("U_ItemCode", n, oDataTable.GetValue("ItemCode", i))
'                                        oDBDSDetail_BRM.SetValue("U_ItemName", n, oDataTable.GetValue("ItemName", i))
'                                        oDBDSDetail_BRM.SetValue("U_UOM", n, oDataTable.GetValue("InvntryUom", i))
'                                        n = n + 1
'                                        If oDBDSDetail_BRM.Size <= n Then _
'                                            oDBDSDetail_BRM.InsertRecord(n)
'                                        oDBDSDetail_BRM.Offset = n
'                                        oDBDSDetail_BRM.SetValue("LineId", n, n + 1)
'                                    Next
'                                    oMatrix_BRM.LoadFromDataSourceEx()
'                                    'Assign Custemer
'                                Case "cfl_LITEM"
'                                    oMatrix_SBLR.FlushToDataSource()
'                                    oDataTable = pVal.SelectedObjects
'                                    Dim n As Integer = pVal.Row - 1
'                                    Dim i As Integer = 0
'                                    'For Multi Row Select
'                                    oDBDSDetail_SBLR.SetValue("U_LbrGrpCo", n, oDataTable.GetValue("U_Code", i))
'                                    oDBDSDetail_SBLR.SetValue("U_LbrGrpNa", n, oDataTable.GetValue("Name", i))

'                                    Dim LaborGroup = oDataTable.GetValue("U_Code", i)

'                                    Dim PerHourRate = objAddOn.getSingleValue("Select U_PerHrRt from [@AIS_OLBR] where U_Code = '" & LaborGroup & "' ")
'                                    PerHourRate = IIf(PerHourRate.Trim = "", 0, PerHourRate)
'                                    oDBDSDetail_SBLR.SetValue("U_PerHrRt", n, PerHourRate)

'                                    n = n + 1
'                                    If oDBDSDetail_SBLR.Size <= n Then _
'                                        oDBDSDetail_SBLR.InsertRecord(n)
'                                    oDBDSDetail_SBLR.Offset = n
'                                    oDBDSDetail_SBLR.SetValue("LineId", n, n + 1)
'                                    oMatrix_SBLR.LoadFromDataSourceEx()
'                            End Select
'                        Catch ex As Exception
'                            objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                        Finally
'                        End Try
'                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
'                        Try
'                            Select Case pVal.ColUID
'                                Case "LbrGrpCode"
'                                    If oMatrix_SBLR.Columns.Item("LbrGrpCode").Cells.Item(oMatrix_SBLR.VisualRowCount).Specific.String <> "" Then
'                                        objAddOn.SetNewLine(oMatrix_SBLR, oDBDSDetail_SBLR)
'                                    End If

'                                Case "ItemCode"
'                                    If oMatrix_BRM.Columns.Item("ItemCode").Cells.Item(oMatrix_BRM.VisualRowCount).Specific.String <> "" Then
'                                        objAddOn.SetNewLine(oMatrix_BRM, oDBDSDetail_BRM)
'                                    End If
'                                Case "LProsTime"
'                                    SUBLBRCal(pVal.Row)
'                                Case "BQty", "UCost"
'                                    Dim BQty = oMatrix_BRM.GetCellSpecific("BQty", pVal.Row).Value
'                                    Dim UCost = oMatrix_BRM.GetCellSpecific("UCost", pVal.Row).Value
'                                    BQty = IIf(BQty.ToString.Trim = "", 0, BQty)
'                                    UCost = IIf(UCost.ToString.Trim = "", 0, UCost)
'                                    oMatrix_BRM.GetCellSpecific("STotal", pVal.Row).Value = CDbl(BQty) * CDbl(UCost)
'                                    oMatrix_BRM.FlushToDataSource()
'                                    oMatrix_BRM.LoadFromDataSourceEx()
'                            End Select
'                        Catch ex As Exception
'                            objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                        End Try
'                    Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
'                        Select Case pVal.ItemUID
'                            Case "LMatrix"
'                                ' Link button on the Labour
'                                Try
'                                    Dim frm As SAPbouiCOM.Form
'                                    objAddOn.objApplication.Menus.Item("LGR").Activate()
'                                    frm = objAddOn.objApplication.Forms.ActiveForm
'                                    frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
'                                    frm.Items.Item("t_Code").Specific.Value = oMatrix_SBLR.GetCellSpecific("LbrGrpCode", pVal.Row).Value
'                                    frm.Items.Item("1").Click()
'                                Catch ex As Exception
'                                    objAddOn.objApplication.StatusBar.SetText("Link Pressed Event Failed:" & ex.Message)
'                                End Try
'                        End Select

'                End Select
'            End If
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("Item Event (BRM) Methord Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        End Try
'    End Sub

'#End Region

'    Sub OperationCal(ByVal Row As Integer)
'        Try
'            Dim RMCost = oMatrix.Columns.Item("U_RMCost").Cells.Item(Row).Specific.Value
'            Dim WrkCost = oMatrix.Columns.Item("U_WrkCost").Cells.Item(Row).Specific.Value
'            Dim MCCost = oMatrix.Columns.Item("U_MCCost").Cells.Item(Row).Specific.Value
'            Dim ToolCost = oMatrix.Columns.Item("U_ToolCost").Cells.Item(Row).Specific.Value
'            Dim ScrapCost = oMatrix.Columns.Item("U_ScrapCost").Cells.Item(Row).Specific.Value
'            Dim LaborCost = oMatrix.Columns.Item("U_LaborCost").Cells.Item(Row).Specific.Value
'            Dim ConCost = oMatrix.Columns.Item("U_ConCost").Cells.Item(Row).Specific.Value
'            Dim OthrCost = oMatrix.Columns.Item("U_OthrCost").Cells.Item(Row).Specific.Value
'            Dim InsCost = oMatrix.Columns.Item("U_InsCost").Cells.Item(Row).Specific.Value
'            Dim LeadTime = oMatrix.Columns.Item("U_LeadTime").Cells.Item(Row).Specific.Value
'            Dim Qty = oMatrix.Columns.Item("2").Cells.Item(Row).Specific.Value
'            Dim WareHouse = oMatrix.Columns.Item("3").Cells.Item(Row).Specific.Value

'            ' Work Center Cost Calculation
'            Dim PerHourRate = objAddOn.getSingleValue("Select isnull(U_PerHrRt,0) from OWHS where WhsCode = '" & WareHouse & "' ")
'            PerHourRate = IIf(PerHourRate.Trim = "", 0, PerHourRate)
'            oMatrix.Columns.Item("U_WrkCost").Cells.Item(Row).Specific.Value() = CDbl(PerHourRate) * CDbl(LeadTime)
'            WrkCost = oMatrix.Columns.Item("U_WrkCost").Cells.Item(Row).Specific.Value


'            RMCost = IIf(RMCost.ToString.Trim = "", 0, RMCost)
'            WrkCost = IIf(WrkCost.ToString.Trim = "", 0, WrkCost)
'            MCCost = IIf(MCCost.ToString.Trim = "", 0, MCCost)
'            ToolCost = IIf(ToolCost.ToString.Trim = "", 0, ToolCost)
'            ScrapCost = IIf(ScrapCost.ToString.Trim = "", 0, ScrapCost)
'            LaborCost = IIf(LaborCost.ToString.Trim = "", 0, LaborCost)
'            ConCost = IIf(ConCost.ToString.Trim = "", 0, ConCost)
'            OthrCost = IIf(OthrCost.ToString.Trim = "", 0, OthrCost)
'            InsCost = IIf(InsCost.ToString.Trim = "", 0, InsCost)
'            Qty = IIf(Qty.ToString.Trim = "", 0, Qty)

'            Dim TotalCost As Double = CDbl(RMCost) + CDbl(WrkCost) + CDbl(MCCost) + CDbl(ToolCost) + _
'                                      CDbl(LaborCost) + CDbl(ConCost) + CDbl(InsCost) + CDbl(OthrCost) '- CDbl(ScrapCost)

'            Dim UnitCost As Double = TotalCost / CDbl(Qty)

'            oMatrix.Columns.Item("4").Cells.Item(Row).Specific.Value() = UnitCost

'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("Operation Cast Methord Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        End Try
'    End Sub

'#Region "--- Machinery ----"

'    Sub LoadForm_BMC()
'        Try
'            frmBMC = objAddOn.objUIXml.LoadScreenXML(BOMMachineryXML, Ananth.SBOLib.UIXML.enuResourceType.Embeded, BOMMachineryFormId)
'            'Assign Data Source
'            oDBDSDetail_BMC = frmBMC.DataSources.DBDataSources.Item(0)
'            'Assign Matrix
'            oMatrix_BMC = frmBMC.Items.Item("Matrix").Specific
'            Me.InitForm_BMC()
'        Catch ex As Exception
'            objAddOn.Msg("Load (BMC) Form Method Failed:" & ex.Message)
'        End Try
'    End Sub



'    Sub ItemEvent_BMC(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
'        Try
'            Select Case pVal.EventType

'                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
'                    Select Case pVal.ItemUID

'                    End Select

'            End Select
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("Item Event (BMC) Methord Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        End Try
'    End Sub

'    Sub MCCal(ByVal Row As Integer)
'        Try
'            ' Setup Time Calculation
'            Dim SetupMs = oMatrix_BMC.GetCellSpecific("SetupMs", Row).Value
'            Dim SetTime = oMatrix_BMC.GetCellSpecific("SetTime", Row).Value

'            SetTime = IIf(SetTime.ToString.Trim = "", 0, SetTime)
'            Dim SetTimeHr As Double = objAddOn.ConvertHrs(SetTime, SetupMs)

'            ' Process Time Calculation
'            Dim ProceMs = oMatrix_BMC.GetCellSpecific("MProceMs", Row).Value
'            Dim ProsTime = oMatrix_BMC.GetCellSpecific("MProsTime", Row).Value

'            ProsTime = IIf(ProsTime.ToString.Trim = "", 0, ProsTime)
'            Dim ProsTimeHr As Double = objAddOn.ConvertHrs(ProsTime, ProceMs)

'            Dim PerHrRt = oMatrix_BMC.GetCellSpecific("PerHrRt", Row).Value
'            PerHrRt = IIf(PerHrRt.ToString.Trim = "", 0, PerHrRt)

'            'Total Time Calculation
'            oMatrix_BMC.GetCellSpecific("TotTime", Row).Value = CDbl(SetTimeHr) + CDbl(ProsTimeHr)
'            'Total Amount Calculation
'            oMatrix_BMC.GetCellSpecific("STotal", Row).Value = (CDbl(SetTimeHr) + CDbl(ProsTimeHr)) * PerHrRt

'            oMatrix_BMC.FlushToDataSource()
'            oMatrix_BMC.LoadFromDataSourceEx()
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("MCCal Methord Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        End Try
'    End Sub

'#End Region

'#Region "--- Tools ----"

'    Sub LoadForm_BTL()
'        Try
'            frmBTL = objAddOn.objUIXml.LoadScreenXML(BOMToolsXML, Ananth.SBOLib.UIXML.enuResourceType.Embeded, BOMToolsFormId)
'            'Assign Data Source
'            oDBDSDetail_BTL = frmBTL.DataSources.DBDataSources.Item(0)
'            oDBDSDetail_BLR = frmBTL.DataSources.DBDataSources.Item(1)
'            oDBDSDetail_BMC = frmBTL.DataSources.DBDataSources.Item(2)
'            'Assign Matrix
'            oMatrix_BTL = frmBTL.Items.Item("TMatrix").Specific
'            oMatrix_BMC = frmBTL.Items.Item("MMatrix").Specific
'            oMatrix_BLR = frmBTL.Items.Item("LMatrix").Specific
'            Me.InitForm_BTL()
'            InitForm_BMC()
'            'InitForm_BLR()


'        Catch ex As Exception
'            objAddOn.Msg("Load (BTL) Form Method Failed:" & ex.Message)
'        End Try
'    End Sub



'    Sub ItemEvent_master(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)

'    End Sub

'#End Region

'#Region "--- Scrap ----"

'    Sub LoadForm_BSC()
'        Try
'            frmBSC = objAddOn.objUIXml.LoadScreenXML(BOMScrapXML, Ananth.SBOLib.UIXML.enuResourceType.Embeded, BOMScrapFormId)
'            'Assign Data Source
'            oDBDSDetail_BSC = frmBSC.DataSources.DBDataSources.Item(0)
'            'Assign Matrix
'            oMatrix_BSC = frmBSC.Items.Item("Matrix").Specific
'            Me.InitForm_BSC()
'        Catch ex As Exception
'            objAddOn.Msg("Load (BSC) Form Method Failed:" & ex.Message)
'        End Try
'    End Sub

'    Sub InitForm_BSC()
'        Try
'            frmBSC.Freeze(True)

'            oMatrix_BSC.Columns.Item("STotal").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
'            objAddOn.SetNewLine(oMatrix_BSC, oDBDSDetail_BSC)

'            For i As Integer = 0 To DataTable_BSC.Rows.Count - 1

'                If DataTable_BSC.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BSC Then
'                    Dim row As Integer = oMatrix_BSC.VisualRowCount
'                    ' Matrix Filling
'                    oMatrix_BSC.GetCellSpecific("ParentCode", row).Value = DataTable_BSC.Rows.Item(i)("ParentCode").ToString
'                    oMatrix_BSC.GetCellSpecific("PLineId", row).Value = DataTable_BSC.Rows.Item(i)("PLineId").ToString
'                    oMatrix_BSC.GetCellSpecific("ItemCode", row).Value = DataTable_BSC.Rows.Item(i)("ItemCode").ToString
'                    oMatrix_BSC.GetCellSpecific("ItemName", row).Value = DataTable_BSC.Rows.Item(i)("ItemName").ToString
'                    oMatrix_BSC.GetCellSpecific("UOM", row).Value = DataTable_BSC.Rows.Item(i)("UOM").ToString
'                    oMatrix_BSC.GetCellSpecific("BQty", row).Value = DataTable_BSC.Rows.Item(i)("BQty").ToString
'                    oMatrix_BSC.GetCellSpecific("UCost", row).Value = DataTable_BSC.Rows.Item(i)("UCost").ToString
'                    oMatrix_BSC.GetCellSpecific("STotal", row).Value = DataTable_BSC.Rows.Item(i)("STotal").ToString

'                End If

'            Next

'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("InitForm (BSC) Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        Finally
'            frmBSC.Freeze(False)

'        End Try
'    End Sub

'    Sub ItemEvent_BSC(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
'        Try
'            Select Case pVal.EventType
'                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
'                    Try

'                        'Assign Selected Rows
'                        Dim oDataTable As SAPbouiCOM.DataTable
'                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
'                        oDataTable = oCFLE.SelectedObjects
'                        Dim rset As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
'                        'Filter before open the CFL
'                        If pVal.BeforeAction Then
'                            Select Case oCFLE.ChooseFromListUID
'                                Case "cfl_ITEM"
'                                    objAddOn.ChooseFromListFilteration(frmBSC, oCFLE.ChooseFromListUID, "ItemCode", "Select Itemcode from OITM where  ItmsGrpCod in (Select ItmsGrpCod  from oitb where U_ItmGrp = 'S')")
'                            End Select
'                        End If
'                        If pVal.BeforeAction = False Then
'                            'Assign Value to selected control
'                            Select Case oCFLE.ChooseFromListUID
'                                'Assign Custemer
'                                Case "cfl_ITEM"
'                                    oMatrix_BSC.FlushToDataSource()
'                                    Dim n As Integer = pVal.Row - 1
'                                    'For Multi Row Select
'                                    For i As Integer = 0 To oDataTable.Rows.Count - 1
'                                        oDBDSDetail_BSC.SetValue("U_ItemCode", n, oDataTable.GetValue("ItemCode", i))
'                                        oDBDSDetail_BSC.SetValue("U_ItemName", n, oDataTable.GetValue("ItemName", i))
'                                        oDBDSDetail_BSC.SetValue("U_UOM", n, oDataTable.GetValue("InvntryUom", i))
'                                        n = n + 1
'                                        If oDBDSDetail_BSC.Size <= n Then _
'                                            oDBDSDetail_BSC.InsertRecord(n)
'                                        oDBDSDetail_BSC.Offset = n
'                                        oDBDSDetail_BSC.SetValue("LineId", n, n + 1)
'                                    Next
'                                    oMatrix_BSC.LoadFromDataSourceEx()
'                            End Select
'                        End If
'                    Catch ex As Exception
'                        objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                    Finally
'                    End Try
'                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
'                    Try
'                        Select Case pVal.ColUID
'                            Case "BQty", "UCost"
'                                If pVal.BeforeAction = False Then
'                                    Dim BQty = oMatrix_BSC.GetCellSpecific("BQty", pVal.Row).Value
'                                    Dim UCost = oMatrix_BSC.GetCellSpecific("UCost", pVal.Row).Value
'                                    BQty = IIf(BQty.ToString.Trim = "", 0, BQty)
'                                    UCost = IIf(UCost.ToString.Trim = "", 0, UCost)
'                                    oMatrix_BSC.GetCellSpecific("STotal", pVal.Row).Value = CDbl(BQty) * CDbl(UCost)
'                                    oMatrix_BSC.FlushToDataSource()
'                                    oMatrix_BSC.LoadFromDataSourceEx()
'                                End If
'                        End Select
'                    Catch ex As Exception
'                        objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                    End Try
'                Case SAPbouiCOM.BoEventTypes.et_CLICK
'                    Try
'                        Select Case pVal.ItemUID
'                            Case "1"
'                                '----------- Row Validation-----------------------------------------------------------------------------------------------
'                                If pVal.BeforeAction Then
'                                    For i As Integer = 1 To oMatrix_BSC.VisualRowCount
'                                        Dim BQty = oMatrix_BSC.GetCellSpecific("BQty", 1).Value
'                                        Dim UCost = oMatrix_BSC.GetCellSpecific("UCost", 1).Value
'                                        BQty = IIf(BQty.ToString.Trim = "", 0, BQty)
'                                        UCost = IIf(UCost.ToString.Trim = "", 0, UCost)

'                                        If CDbl(BQty) = 0 Then
'                                            objAddOn.Msg("Line No : " & i.ToString & " Please Enter Base Qty. ")
'                                            BubbleEvent = False
'                                            Return
'                                        End If

'                                        If CDbl(UCost) = 0 Then
'                                            objAddOn.Msg("Line No : " & i.ToString & " Please Enter Unit Cost ")
'                                            BubbleEvent = False
'                                            Return
'                                        End If

'                                    Next
'                                End If
'                                '-----------------------------------------------------------------------------------------------------------------------------


'                                If pVal.BeforeAction = False Then
'                                    'clear Data Table
'                                    If oMatrix_BSC.GetCellSpecific("ItemCode", oMatrix_BSC.VisualRowCount).Value = "" Then
'                                        oMatrix_BSC.DeleteRow(oMatrix_BSC.VisualRowCount)
'                                    End If
'10:
'                                    For i As Integer = 0 To DataTable_BSC.Rows.Count - 1
'                                        If DataTable_BSC.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BSC Then
'                                            DataTable_BSC.Rows.RemoveAt(i)
'                                            GoTo 10
'                                        End If
'                                    Next

'                                    'Add Data Table
'                                    Dim n As Integer = 0
'                                    Dim SCCostSum As Double = 0
'                                    Dim RowCount As Integer = DataTable_BSC.Rows.Count
'                                    For i As Integer = RowCount To RowCount + oMatrix_BSC.VisualRowCount - 1

'                                        DataTable_BSC.Rows.Add()
'                                        n = n + 1
'                                        DataTable_BSC.Rows.Item(i)("ParentCode") = ParentCode
'                                        DataTable_BSC.Rows.Item(i)("PLineId") = PLineId_BSC
'                                        DataTable_BSC.Rows.Item(i)("ItemCode") = oMatrix_BSC.GetCellSpecific("ItemCode", n).Value
'                                        DataTable_BSC.Rows.Item(i)("ItemName") = oMatrix_BSC.GetCellSpecific("ItemName", n).Value
'                                        DataTable_BSC.Rows.Item(i)("UOM") = oMatrix_BSC.GetCellSpecific("UOM", n).Value
'                                        DataTable_BSC.Rows.Item(i)("BQty") = oMatrix_BSC.GetCellSpecific("BQty", n).Value
'                                        DataTable_BSC.Rows.Item(i)("UCost") = oMatrix_BSC.GetCellSpecific("UCost", n).Value
'                                        DataTable_BSC.Rows.Item(i)("STotal") = oMatrix_BSC.GetCellSpecific("STotal", n).Value

'                                        Dim STotal = oMatrix_BSC.GetCellSpecific("STotal", n).Value
'                                        STotal = IIf(STotal.ToString.Trim = "", 0, STotal)
'                                        SCCostSum = SCCostSum + CDbl(STotal)

'                                    Next

'                                    oMatrix.Columns.Item("U_ScrapCost").Cells.Item(PLineId_BSC).Specific.Value = SCCostSum
'                                    OperationCal(PLineId_BSC)

'                                    If frmUOM.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmUOM.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
'                                    frmBSC.Close()
'                                End If
'                        End Select
'                    Catch ex As Exception
'                        objAddOn.objApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                    End Try
'            End Select
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("Item Event (BSC) Methord Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        End Try
'    End Sub

'#End Region

'#Region "--- Consumables ----"

'    Sub LoadForm_BCO()
'        Try
'            frmBCO = objAddOn.objUIXml.LoadScreenXML(BOMConsumablesXML, Ananth.SBOLib.UIXML.enuResourceType.Embeded, BOMConsumablesFormId)
'            'Assign Data Source
'            oDBDSDetail_BCO = frmBCO.DataSources.DBDataSources.Item(0)
'            'Assign Matrix
'            oMatrix_BCO = frmBCO.Items.Item("Matrix").Specific
'            Me.InitForm_BCO()
'        Catch ex As Exception
'            objAddOn.Msg("Load (BCO) Form Method Failed:" & ex.Message)
'        End Try
'    End Sub

'    Sub InitForm_BCO()
'        Try
'            frmBCO.Freeze(True)

'            oMatrix_BCO.Columns.Item("STotal").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto

'            objAddOn.SetNewLine(oMatrix_BCO, oDBDSDetail_BCO)

'            For i As Integer = 0 To DataTable_BCO.Rows.Count - 1

'                If DataTable_BCO.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BCO Then
'                    Dim row As Integer = oMatrix_BCO.VisualRowCount
'                    ' Matrix Filling
'                    oMatrix_BCO.GetCellSpecific("ParentCode", row).Value = DataTable_BCO.Rows.Item(i)("ParentCode").ToString
'                    oMatrix_BCO.GetCellSpecific("PLineId", row).Value = DataTable_BCO.Rows.Item(i)("PLineId").ToString
'                    oMatrix_BCO.GetCellSpecific("ItemCode", row).Value = DataTable_BCO.Rows.Item(i)("ItemCode").ToString
'                    oMatrix_BCO.GetCellSpecific("ItemName", row).Value = DataTable_BCO.Rows.Item(i)("ItemName").ToString
'                    oMatrix_BCO.GetCellSpecific("UOM", row).Value = DataTable_BCO.Rows.Item(i)("UOM").ToString
'                    oMatrix_BCO.GetCellSpecific("BQty", row).Value = DataTable_BCO.Rows.Item(i)("BQty").ToString
'                    oMatrix_BCO.GetCellSpecific("UCost", row).Value = DataTable_BCO.Rows.Item(i)("UCost").ToString
'                    oMatrix_BCO.GetCellSpecific("STotal", row).Value = DataTable_BCO.Rows.Item(i)("STotal").ToString

'                End If

'            Next

'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("InitForm (BCO) Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        Finally
'            frmBCO.Freeze(False)

'        End Try
'    End Sub

'    Sub ItemEvent_BCO(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
'        Try
'            Select Case pVal.EventType
'                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
'                    Try
'                        'Assign Selected Rows
'                        Dim oDataTable As SAPbouiCOM.DataTable
'                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
'                        oDataTable = oCFLE.SelectedObjects
'                        Dim rset As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
'                        'Filter before open the CFL
'                        If pVal.BeforeAction Then
'                            Select Case oCFLE.ChooseFromListUID
'                                Case "cfl_ITEM"
'                                    objAddOn.ChooseFromListFilteration(frmBCO, oCFLE.ChooseFromListUID, "ItemCode", "Select Itemcode from OITM where  ItmsGrpCod in (Select ItmsGrpCod  from oitb where U_ItmGrp = 'C')")
'                            End Select
'                        End If
'                        If pVal.BeforeAction = False Then
'                            'Assign Value to selected control
'                            Select Case oCFLE.ChooseFromListUID
'                                'Assign Custemer
'                                Case "cfl_ITEM"
'                                    oMatrix_BCO.FlushToDataSource()
'                                    Dim n As Integer = pVal.Row - 1
'                                    'For Multi Row Select
'                                    For i As Integer = 0 To oDataTable.Rows.Count - 1
'                                        oDBDSDetail_BCO.SetValue("U_ItemCode", n, oDataTable.GetValue("ItemCode", i))
'                                        oDBDSDetail_BCO.SetValue("U_ItemName", n, oDataTable.GetValue("ItemName", i))
'                                        oDBDSDetail_BCO.SetValue("U_UOM", n, oDataTable.GetValue("InvntryUom", i))
'                                        n = n + 1
'                                        If oDBDSDetail_BCO.Size <= n Then _
'                                            oDBDSDetail_BCO.InsertRecord(n)
'                                        oDBDSDetail_BCO.Offset = n
'                                        oDBDSDetail_BCO.SetValue("LineId", n, n + 1)
'                                    Next
'                                    oMatrix_BCO.LoadFromDataSourceEx()
'                            End Select
'                        End If
'                    Catch ex As Exception
'                        objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                    Finally
'                    End Try
'                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
'                    Try
'                        Select Case pVal.ColUID
'                            Case "BQty", "UCost"
'                                If pVal.BeforeAction = False Then
'                                    Dim BQty = oMatrix_BCO.GetCellSpecific("BQty", pVal.Row).Value
'                                    Dim UCost = oMatrix_BCO.GetCellSpecific("UCost", pVal.Row).Value
'                                    BQty = IIf(BQty.ToString.Trim = "", 0, BQty)
'                                    UCost = IIf(UCost.ToString.Trim = "", 0, UCost)
'                                    oMatrix_BCO.GetCellSpecific("STotal", pVal.Row).Value = CDbl(BQty) * CDbl(UCost)
'                                    oMatrix_BCO.FlushToDataSource()
'                                    oMatrix_BCO.LoadFromDataSourceEx()
'                                End If
'                        End Select
'                    Catch ex As Exception
'                        objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                    End Try
'                Case SAPbouiCOM.BoEventTypes.et_CLICK
'                    Try
'                        Select Case pVal.ItemUID
'                            Case "1"
'                                '----------- Row Validation-----------------------------------------------------------------------------------------------
'                                If pVal.BeforeAction Then
'                                    For i As Integer = 1 To oMatrix_BCO.VisualRowCount
'                                        Dim BQty = oMatrix_BCO.GetCellSpecific("BQty", 1).Value
'                                        Dim UCost = oMatrix_BCO.GetCellSpecific("UCost", 1).Value
'                                        BQty = IIf(BQty.ToString.Trim = "", 0, BQty)
'                                        UCost = IIf(UCost.ToString.Trim = "", 0, UCost)

'                                        If CDbl(BQty) = 0 Then
'                                            objAddOn.Msg("Line No : " & i.ToString & " Please Enter Base Qty. ")
'                                            BubbleEvent = False
'                                            Return
'                                        End If

'                                        If CDbl(UCost) = 0 Then
'                                            objAddOn.Msg("Line No : " & i.ToString & " Please Enter Unit Cost ")
'                                            BubbleEvent = False
'                                            Return
'                                        End If

'                                    Next
'                                End If
'                                '-----------------------------------------------------------------------------------------------------------------------------


'                                If pVal.BeforeAction = False Then
'                                    'clear Data Table
'                                    oMatrix_BCO.DeleteRow(oMatrix_BCO.VisualRowCount)
'10:
'                                    For i As Integer = 0 To DataTable_BCO.Rows.Count - 1
'                                        If DataTable_BCO.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BCO Then
'                                            DataTable_BCO.Rows.RemoveAt(i)
'                                            GoTo 10
'                                        End If
'                                    Next

'                                    'Add Data Table
'                                    Dim n As Integer = 0
'                                    Dim COCostSum As Double = 0
'                                    Dim RowCount As Integer = DataTable_BCO.Rows.Count
'                                    For i As Integer = RowCount To RowCount + oMatrix_BCO.VisualRowCount - 1

'                                        DataTable_BCO.Rows.Add()
'                                        n = n + 1

'                                        DataTable_BCO.Rows.Item(i)("ParentCode") = ParentCode
'                                        DataTable_BCO.Rows.Item(i)("PLineId") = PLineId_BCO
'                                        DataTable_BCO.Rows.Item(i)("ItemCode") = oMatrix_BCO.GetCellSpecific("ItemCode", n).Value
'                                        DataTable_BCO.Rows.Item(i)("ItemName") = oMatrix_BCO.GetCellSpecific("ItemName", n).Value
'                                        DataTable_BCO.Rows.Item(i)("UOM") = oMatrix_BCO.GetCellSpecific("UOM", n).Value
'                                        DataTable_BCO.Rows.Item(i)("BQty") = oMatrix_BCO.GetCellSpecific("BQty", n).Value
'                                        DataTable_BCO.Rows.Item(i)("UCost") = oMatrix_BCO.GetCellSpecific("UCost", n).Value
'                                        DataTable_BCO.Rows.Item(i)("STotal") = oMatrix_BCO.GetCellSpecific("STotal", n).Value


'                                        Dim STotal = oMatrix_BCO.GetCellSpecific("STotal", n).Value
'                                        STotal = IIf(STotal.ToString.Trim = "", 0, STotal)
'                                        COCostSum = COCostSum + CDbl(STotal)

'                                    Next
'                                    oMatrix.Columns.Item("U_ConCost").Cells.Item(PLineId_BCO).Specific.Value = COCostSum
'                                    OperationCal(PLineId_BCO)


'                                    If frmUOM.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmUOM.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
'                                    frmBCO.Close()
'                                End If
'                        End Select
'                    Catch ex As Exception
'                        objAddOn.objApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                    End Try
'            End Select
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("Item Event (BCO) Methord Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        End Try
'    End Sub

'#End Region

'#Region "Master"

'    Sub LoadForm_Master()
'        Try
'            frmBLR = objAddOn.objUIXml.LoadScreenXML(BOMLaborXML, Ananth.SBOLib.UIXML.enuResourceType.Embeded, BOMLaborFormId)
'            'Assign Data Source
'            oDBDSDetail_BLR = frmBLR.DataSources.DBDataSources.Item(0)
'            oDBDSDetail_BTL = frmBLR.DataSources.DBDataSources.Item(1)
'            oDBDSDetail_BMC = frmBLR.DataSources.DBDataSources.Item(2)

'            'Assign Matrix
'            'Assign Matrix
'            oMatrix_BTL = frmBLR.Items.Item("TMatrix").Specific
'            oMatrix_BMC = frmBLR.Items.Item("MMatrix").Specific
'            oMatrix_BLR = frmBLR.Items.Item("LMatrix").Specific
'            frmBLR.Freeze(True)
'            frmBLR.Items.Item("LMatrix").Visible = False
'            frmBLR.Items.Item("5").Visible = False
'            Me.InitForm_BTL()
'            InitForm_BMC()
'            'InitForm_BLR()
'            frmBLR.Items.Item("4").Click()
'            frmBLR.Freeze(False)
'        Catch ex As Exception
'            frmBLR.Freeze(False)
'            objAddOn.Msg("Load (BLR) Form Method Failed:" & ex.Message)
'        End Try
'    End Sub

'    Sub InitForm_BMC()
'        Try
'            frmBLR.Freeze(True)

'            objAddOn.SetNewLine(oMatrix_BMC, oDBDSDetail_BMC)
'            oMatrix_BMC.Columns.Item("STotal").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto

'            For i As Integer = 0 To DataTable_BMC.Rows.Count - 1
'                If DataTable_BMC.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BMC Then
'                    objAddOn.SetNewLine(oMatrix_BMC, oDBDSDetail_BMC)
'                    Dim row As Integer = oMatrix_BMC.VisualRowCount - 1
'                    ' Matrix Filling
'                    oMatrix_BMC.GetCellSpecific("ParentCode", row).Value = DataTable_BMC.Rows.Item(i)("ParentCode").ToString
'                    oMatrix_BMC.GetCellSpecific("PLineId", row).Value = DataTable_BMC.Rows.Item(i)("PLineId").ToString
'                    oMatrix_BMC.GetCellSpecific("MOpCode", row).Value = DataTable_BMC.Rows.Item(i)("Oprn").ToString
'                    oMatrix_BMC.GetCellSpecific("ItemCode", row).Value = DataTable_BMC.Rows.Item(i)("ItemCode").ToString
'                    oMatrix_BMC.GetCellSpecific("ItemName", row).Value = DataTable_BMC.Rows.Item(i)("ItemName").ToString
'                    oMatrix_BMC.GetCellSpecific("SetTime", row).Value = DataTable_BMC.Rows.Item(i)("SetTime").ToString
'                    oMatrix_BMC.GetCellSpecific("SetupMs", row).Select(DataTable_BMC.Rows.Item(i)("SetupMs").ToString)
'                    oMatrix_BMC.GetCellSpecific("MProsTime", row).Value = DataTable_BMC.Rows.Item(i)("MProsTime").ToString
'                    oMatrix_BMC.GetCellSpecific("MProceMs", row).Select(DataTable_BMC.Rows.Item(i)("MProceMs").ToString)
'                    oMatrix_BMC.GetCellSpecific("TotTime", row).Value = DataTable_BMC.Rows.Item(i)("TotTime").ToString
'                    oMatrix_BMC.GetCellSpecific("PerHrRt", row).Value = DataTable_BMC.Rows.Item(i)("PerHrRt").ToString
'                    oMatrix_BMC.GetCellSpecific("STotal", row).Value = DataTable_BMC.Rows.Item(i)("STotal").ToString
'                End If

'            Next

'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("InitForm (BMC) Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        Finally
'            frmBLR.Freeze(False)

'        End Try
'    End Sub

'    Sub InitForm_BTL()
'        Try
'            frmBLR.Freeze(True)

'            oMatrix_BTL.Columns.Item("STotal").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
'            objAddOn.SetNewLine(oMatrix_BTL, oDBDSDetail_BTL)


'            For i As Integer = 0 To DataTable_BTL.Rows.Count - 1

'                If DataTable_BTL.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BTL Then
'                    objAddOn.SetNewLine(oMatrix_BTL, oDBDSDetail_BTL)
'                    Dim row As Integer = oMatrix_BTL.VisualRowCount - 1

'                    ' Matrix Filling

'                    oMatrix_BTL.GetCellSpecific("ParentCode", row).Value = DataTable_BTL.Rows.Item(i)("ParentCode").ToString
'                    oMatrix_BTL.GetCellSpecific("PLineId", row).Value = DataTable_BTL.Rows.Item(i)("PLineId").ToString
'                    oMatrix_BTL.GetCellSpecific("ItemCode", row).Value = DataTable_BTL.Rows.Item(i)("ItemCode").ToString
'                    oMatrix_BTL.GetCellSpecific("ItemName", row).Value = DataTable_BTL.Rows.Item(i)("ItemName").ToString
'                    oMatrix_BTL.GetCellSpecific("ToolType", row).Select(DataTable_BTL.Rows.Item(i)("ToolType").ToString)
'                    oMatrix_BTL.GetCellSpecific("Rate", row).Value = DataTable_BTL.Rows.Item(i)("Rate").ToString
'                    oMatrix_BTL.GetCellSpecific("LifeTime", row).Value = DataTable_BTL.Rows.Item(i)("LifeTime").ToString
'                    oMatrix_BTL.GetCellSpecific("Qty", row).Value = DataTable_BTL.Rows.Item(i)("Qty").ToString
'                    oMatrix_BTL.GetCellSpecific("UCost", row).Value = DataTable_BTL.Rows.Item(i)("UCost").ToString
'                    oMatrix_BTL.GetCellSpecific("STotal", row).Value = DataTable_BTL.Rows.Item(i)("STotal").ToString

'                End If

'            Next

'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("InitForm (BTL) Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        Finally
'            frmBLR.Freeze(False)

'        End Try
'    End Sub

'    'Sub InitForm_BLR()
'    '    Try
'    '        frmBLR.Freeze(True)

'    '        objAddOn.SetNewLine(oMatrix_BLR, oDBDSDetail_BLR)
'    '        oMatrix_BLR.Columns.Item("STotal").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto

'    '        For i As Integer = 0 To DataTable_BLR.Rows.Count - 1

'    '            If DataTable_BLR.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BLR Then
'    '                objAddOn.SetNewLine(oMatrix_BLR, oDBDSDetail_BLR)
'    '                Dim row As Integer = oMatrix_BLR.VisualRowCount - 1
'    '                ' Matrix Filling
'    '                oMatrix_BLR.GetCellSpecific("ParentCode", row).Value = DataTable_BLR.Rows.Item(i)("ParentCode").ToString
'    '                oMatrix_BLR.GetCellSpecific("PLineId", row).Value = DataTable_BLR.Rows.Item(i)("PLineId").ToString
'    '                oMatrix_BLR.GetCellSpecific("LbrGrpCode", row).Value = DataTable_BLR.Rows.Item(i)("LbrGrpCode").ToString
'    '                oMatrix_BLR.GetCellSpecific("LbrGrpName", row).Value = DataTable_BLR.Rows.Item(i)("LbrGrpName").ToString
'    '                oMatrix_BLR.GetCellSpecific("LProsTime", row).Value = DataTable_BLR.Rows.Item(i)("LProsTime").ToString
'    '                oMatrix_BLR.GetCellSpecific("LProceMs", row).Select(DataTable_BLR.Rows.Item(i)("LProceMs").ToString)
'    '                oMatrix_BLR.GetCellSpecific("PerHrRt", row).Value = DataTable_BLR.Rows.Item(i)("PerHrRt").ToString
'    '                oMatrix_BLR.GetCellSpecific("STotal", row).Value = DataTable_BLR.Rows.Item(i)("STotal").ToString
'    '            End If

'    '        Next

'    '    Catch ex As Exception
'    '        objAddOn.objApplication.StatusBar.SetText("InitForm (BLR) Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'    '    Finally
'    '        frmBLR.Freeze(False)

'    '    End Try
'    'End Sub

'    Function Master_Validation() As Boolean
'        If oMatrix_BTL.RowCount > 0 Then
'            For i = 1 To oMatrix_BTL.RowCount
'                If oMatrix_BTL.Columns.Item("ItemCode").Cells.Item(i).Specific.String <> "" Then
'                    For j = 1 To oMatrix_BTL.RowCount
'                        If i <> j Then
'                            If oMatrix_BTL.Columns.Item("ItemCode").Cells.Item(j).Specific.String = oMatrix_BTL.Columns.Item("ItemCode").Cells.Item(i).Specific.String Then
'                                frmBLR.Items.Item("4").Click()
'                                objAddOn.objApplication.SetStatusBarMessage("  This " & oMatrix_BTL.Columns.Item("ItemCode").Cells.Item(i).Specific.String & " Enter More Than Once ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
'                                Return True
'                            End If
'                        End If

'                    Next
'                    If oMatrix_BTL.Columns.Item("Qty").Cells.Item(i).Specific.value = 0 Then
'                        frmBLR.Items.Item("4").Click()
'                        objAddOn.objApplication.SetStatusBarMessage("Please Enter The Quantity For " & oMatrix_BTL.Columns.Item("ItemCode").Cells.Item(i).Specific.String, SAPbouiCOM.BoMessageTime.bmt_Short, True)
'                        Return True
'                    End If
'                End If
'            Next

'        End If
'        If oMatrix_BLR.RowCount > 0 Then
'            For i = 1 To oMatrix_BLR.RowCount
'                If oMatrix_BLR.Columns.Item("LbrGrpCode").Cells.Item(i).Specific.String <> "" Then
'                    For j = 1 To oMatrix_BLR.RowCount
'                        If i <> j Then
'                            If oMatrix_BLR.Columns.Item("LbrGrpCode").Cells.Item(j).Specific.String = oMatrix_BLR.Columns.Item("LbrGrpCode").Cells.Item(i).Specific.String Then
'                                frmBLR.Items.Item("5").Click()
'                                objAddOn.objApplication.SetStatusBarMessage("  This " & oMatrix_BLR.Columns.Item("LbrGrpCode").Cells.Item(i).Specific.String & " Enter More Than Once ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
'                                Return True
'                            End If
'                        End If

'                    Next
'                    If oMatrix_BLR.Columns.Item("LProsTime").Cells.Item(i).Specific.value = 0 Then
'                        frmBLR.Items.Item("5").Click()
'                        objAddOn.objApplication.SetStatusBarMessage("Please Enter Process Time For This " & oMatrix_BLR.Columns.Item("LbrGrpCode").Cells.Item(i).Specific.String & " Item", SAPbouiCOM.BoMessageTime.bmt_Short, True)
'                        Return True
'                    End If
'                End If
'            Next

'        End If
'        If oMatrix_BMC.RowCount > 0 Then
'            For i = 1 To oMatrix_BMC.RowCount
'                If oMatrix_BMC.Columns.Item("ItemCode").Cells.Item(i).Specific.String <> "" Then
'                    For j = 1 To oMatrix_BMC.RowCount
'                        If i <> j Then
'                            If oMatrix_BMC.Columns.Item("ItemCode").Cells.Item(j).Specific.String = oMatrix_BMC.Columns.Item("ItemCode").Cells.Item(i).Specific.String Then
'                                frmBLR.Items.Item("6").Click()
'                                objAddOn.objApplication.SetStatusBarMessage("  This " & oMatrix_BMC.Columns.Item("ItemCode").Cells.Item(i).Specific.String & " Enter More Than Once ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
'                                Return True
'                            End If
'                        End If

'                    Next
'                    If oMatrix_BMC.Columns.Item("MOpCode").Cells.Item(i).Specific.String = "" Then
'                        objAddOn.objApplication.SetStatusBarMessage("  This  Enter OpCode For Line No " & i & " ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
'                        Return True
'                    End If
'                End If

'            Next
'        End If

'        Return False


'    End Function

'    Sub ItemEvent_Masters(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
'        Try
'            'If frmBLR Is Nothing Then
'            '    frmBLR = objAddOn.objApplication.Forms.Item(FormUID)
'            '    'oMatrix_BTL = frmBLR.Items.Item("TMatrix").Specific
'            '    'oMatrix_BMC = frmBLR.Items.Item("MMatrix").Specific
'            '    'oMatrix_BLR = frmBLR.Items.Item("LMatrix").Specific
'            'End If
'            If pVal.BeforeAction = True Then
'                Select Case pVal.EventType
'                    'Case SAPbouiCOM.BoEventTypes.et_FORM_KEY_DOWN
'                    '    Try
'                    '        oCFLE = pVal

'                    '        Dim rset As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
'                    '        Select Case oCFLE.ChooseFromListUID
'                    '            Case "cfl_LGR"
'                    '                objAddOn.ChooseFromListFilteration(frmBLR, oCFLE.ChooseFromListUID, "U_Code", "Select U_Code from [@AIS_OLBR] where U_Active = 'Y'")
'                    '            Case "cfl_MITEM"
'                    '                objAddOn.ChooseFromListFilteration(frmBLR, oCFLE.ChooseFromListUID, "ItemCode", "Select Itemcode from OITM where  ItmsGrpCod in (Select ItmsGrpCod  from oitb where U_ItmGrp = 'M') ")
'                    '            Case "SubItem"
'                    '                objAddOn.ChooseFromListFilteration(frmBLR, oCFLE.ChooseFromListUID, "ItemCode", "Select U_Code from [@AIS_OPRN]")
'                    '            Case "cfl_ITEM"
'                    '                objAddOn.ChooseFromListFilteration(frmBLR, oCFLE.ChooseFromListUID, "ItemCode", "Select Itemcode from OITM where  ItmsGrpCod in (Select ItmsGrpCod  from oitb where U_ItmGrp = 'T')")
'                    '        End Select
'                    '    Catch ex As Exception
'                    '        objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                    '    End Try
'                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
'                        Try
'                            oCFLE = pVal

'                            Dim rset As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
'                            Select Case oCFLE.ChooseFromListUID
'                                Case "Cfl_Oprn"
'                                    objAddOn.ChooseFromListFilteration(frmBLR, oCFLE.ChooseFromListUID, "ItemCode", "Select U_Code from [@AIS_OPRN]")
'                                    'objAddOn.ChooseFromListFilteration(frmBLR, oCFLE.ChooseFromListUID, "U_Code", "Select U_Code from [@AIS_OLBR] where U_Active = 'Y'")
'                                Case "cfl_LGR"
'                                    objAddOn.ChooseFromListFilteration(frmBLR, oCFLE.ChooseFromListUID, "U_Code", "Select U_Code from [@AIS_OLBR] where U_Active = 'Y'")
'                                Case "cfl_MITEM"
'                                    objAddOn.ChooseFromListFilteration(frmBLR, oCFLE.ChooseFromListUID, "ItemCode", "Select Itemcode from OITM where  ItmsGrpCod in (Select ItmsGrpCod  from oitb where U_ItmGrp = 'M') ")
'                                Case "SubItem"
'                                    objAddOn.ChooseFromListFilteration(frmBLR, oCFLE.ChooseFromListUID, "ItemCode", "Select U_Code from [@AIS_OPRN]")
'                                Case "cfl_ITEM"
'                                    objAddOn.ChooseFromListFilteration(frmBLR, oCFLE.ChooseFromListUID, "ItemCode", "Select Itemcode from OITM where  ItmsGrpCod in (Select ItmsGrpCod  from oitb where U_ItmGrp = 'T')")
'                            End Select
'                        Catch ex As Exception
'                            objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                        End Try
'                    Case SAPbouiCOM.BoEventTypes.et_CLICK
'                        If pVal.ItemUID = "1" Then
'                            If Master_Validation() = True Then
'                                BubbleEvent = False
'                            End If

'                        End If
'                    Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
'                        Select Case pVal.ItemUID
'                            Case "Matrix"
'                                ' Link button on the Labour
'                                'Try
'                                '    objAddOn.objApplication.Menus.Item("LGR").Activate()
'                                'Catch ex As Exception
'                                '    objAddOn.objApplication.StatusBar.SetText("Link Pressed Event Failed:" & ex.Message)
'                                'End Try
'                        End Select
'                End Select
'            Else
'                Select Case pVal.EventType
'                    Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
'                        If pVal.ItemUID = "MMatrix" And pVal.ColUID = "OpCode" Then
'                            CurrentMaterial = oMatrix_BMC.Columns.Item("ItemCode").Cells.Item(pVal.Row).Specific.string
'                            PLineId_BRM = pVal.Row
'                            LoadForm_BRM()
'                        End If
'                    Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
'                        Try
'                            Select Case pVal.ColUID
'                                Case "SetupMs", "MProceMs"
'                                    MCCal(pVal.Row)
'                                Case "LProceMs"
'                                    LBRCal(pVal.Row)
'                            End Select
'                        Catch ex As Exception
'                            objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                        End Try
'                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
'                        Try
'                            oCFLE = pVal
'                            oDataTable = oCFLE.SelectedObjects
'                            'Assign Value to selected control
'                            Select Case pVal.ChooseFromListUID
'                                'Case "cfl_LGR"
'                                '    oMatrix_BLR.FlushToDataSource()
'                                '    Dim n As Integer = pVal.Row - 1
'                                '    For i As Integer = 0 To oDataTable.Rows.Count - 1
'                                '        oDBDSDetail_BLR.SetValue("U_LbrGrpCo", n, oDataTable.GetValue("U_Code", i))
'                                '        oDBDSDetail_BLR.SetValue("U_LbrGrpNa", n, oDataTable.GetValue("Name", i))
'                                '        oDBDSDetail_BLR.SetValue("U_PerHrRt", n, oDataTable.GetValue("U_PerHrRt", i))
'                                '        'Assign Custemer
'                                '    Next
'                                '    oMatrix_BLR.LoadFromDataSourceEx()
'                                Case "SubItem"
'                                    oMatrix_BMC.FlushToDataSource()
'                                    Dim n As Integer = pVal.Row - 1
'                                    For i As Integer = 0 To oDataTable.Rows.Count - 1
'                                        oDBDSDetail_BMC.SetValue("U_OpCode", n, oDataTable.GetValue("ItemCode", i))
'                                        'Assign Custemer
'                                    Next
'                                    oMatrix_BMC.LoadFromDataSourceEx()
'                                Case "Cfl_Oprn"
'                                    oMatrix_BMC.FlushToDataSource()
'                                    oDBDSDetail_BMC.SetValue("U_Oprn", pVal.Row - 1, oDataTable.GetValue("ItemCode", 0))
'                                    oMatrix_BMC.LoadFromDataSourceEx()
'                                Case "cfl_MITEM"
'                                    oMatrix_BMC.FlushToDataSource()
'                                    Dim n As Integer = pVal.Row - 1
'                                    'For Multi Row Select
'                                    For i As Integer = 0 To oDataTable.Rows.Count - 1
'                                        oDBDSDetail_BMC.SetValue("U_ItemCode", n, oDataTable.GetValue("ItemCode", i))
'                                        oDBDSDetail_BMC.SetValue("U_ItemName", n, oDataTable.GetValue("ItemName", i))

'                                        Dim PerHourRate = objAddOn.getSingleValue("Select isnull(U_PerHrRt,0) from OITM where ItemCode = '" & oDataTable.GetValue("ItemCode", i) & "' ")
'                                        PerHourRate = IIf(PerHourRate.Trim = "", 0, PerHourRate)
'                                        oDBDSDetail_BMC.SetValue("U_PerHrRt", n, PerHourRate)

'                                        n = n + 1
'                                        If oDBDSDetail_BMC.Size <= n Then _
'                                            oDBDSDetail_BMC.InsertRecord(n)
'                                        oDBDSDetail_BMC.Offset = n
'                                        oDBDSDetail_BMC.SetValue("LineId", n, n + 1)
'                                    Next
'                                    oMatrix_BMC.LoadFromDataSourceEx()
'                                    'Assign Custemer Labour
'                                Case "cfl_LGR"
'                                    oMatrix_BLR.FlushToDataSource()
'                                    Dim n As Integer = pVal.Row - 1

'                                    'oMatrix_BLR.GetCellSpecific("LaborCode", pVal.Row).Value = oDataTable.GetValue("U_Code", 0)
'                                    'oMatrix_BLR.GetCellSpecific("LaborCode", pVal.Row).Value = oDataTable.GetValue("U_Code", 0)

'                                    'For Multi Row Select
'                                    Dim i As Integer = 0
'                                    oDBDSDetail_BLR.SetValue("U_LbrGrpCo", n, oDataTable.GetValue("U_Code", i))
'                                    oDBDSDetail_BLR.SetValue("U_LbrGrpNa", n, oDataTable.GetValue("Name", i))
'                                    oDBDSDetail_BLR.SetValue("U_PerHrRt", n, oDataTable.GetValue("U_PerHrRt", i))
'                                    'Dim LaborGroup = oDataTable.GetValue("U_Code", i)

'                                    'Dim PerHourRate = objAddOn.getSingleValue("Select U_PerHrRt from [@AIS_OLBR] where U_Code = '" & LaborGroup & "' ")
'                                    'PerHourRate = IIf(PerHourRate.Trim = "", 0, PerHourRate)
'                                    'oDBDSDetail_BLR.SetValue("U_PerHrRt", n, PerHourRate)

'                                    n = n + 1
'                                    If oDBDSDetail_BLR.Size <= n Then _
'                                        oDBDSDetail_BLR.InsertRecord(n)
'                                    oDBDSDetail_BLR.Offset = n
'                                    oDBDSDetail_BLR.SetValue("LineId", n, n + 1)



'                                    oMatrix_BLR.LoadFromDataSourceEx()
'                                    'Assign Custemer BTL
'                                Case "cfl_ITEM"

'                                    oMatrix_BTL.FlushToDataSource()
'                                    Dim n As Integer = pVal.Row - 1
'                                    'For Multi Row Select
'                                    For i As Integer = 0 To oDataTable.Rows.Count - 1
'                                        oDBDSDetail_BTL.SetValue("U_ItemCode", n, oDataTable.GetValue("ItemCode", i))
'                                        oDBDSDetail_BTL.SetValue("U_ItemName", n, oDataTable.GetValue("ItemName", i))
'                                        'Amount Allocation
'                                        Dim Amount = objAddOn.getSingleValue("Select isnull(U_Amount,0) from OITM where ItemCode = '" & oDataTable.GetValue("ItemCode", i) & "' ")
'                                        Amount = IIf(Amount.Trim = "", 0, Amount)
'                                        oDBDSDetail_BTL.SetValue("U_Rate", n, Amount)
'                                        'Life Time Allocation
'                                        Dim LifeTime = objAddOn.getSingleValue("Select isnull(U_LifeTime,0) from OITM where ItemCode = '" & oDataTable.GetValue("ItemCode", i) & "' ")
'                                        LifeTime = IIf(LifeTime.Trim = "", 0, LifeTime)
'                                        oDBDSDetail_BTL.SetValue("U_LifeTime", n, LifeTime)

'                                        n = n + 1
'                                        If oDBDSDetail_BTL.Size <= n Then _
'                                            oDBDSDetail_BTL.InsertRecord(n)
'                                        oDBDSDetail_BTL.Offset = n
'                                        oDBDSDetail_BTL.SetValue("LineId", n, n + 1)
'                                    Next
'                                    oMatrix_BTL.LoadFromDataSourceEx()
'                            End Select
'                        Catch ex As Exception
'                            objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                        End Try
'                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
'                        Try
'                            Select Case pVal.ColUID
'                                'Case "ItemCode"
'                                '    If pVal.ItemUID = "MMatrix" And oMatrix_BMC.Columns.Item("ItemCode").Cells.Item(oMatrix_BMC.VisualRowCount).Specific.String <> "" Then
'                                '        objAddOn.SetNewLine(oMatrix_BMC, oDBDSDetail_BMC)
'                                '    ElseIf pVal.ItemUID = "TMatrix" And oMatrix_BTL.Columns.Item("ItemCode").Cells.Item(oMatrix_BTL.VisualRowCount).Specific.String <> "" Then
'                                '        objAddOn.SetNewLine(oMatrix_BTL, oDBDSDetail_BTL)
'                                '    End If
'                                'Case "LbrGrpCode"
'                                '    If oMatrix_BLR.Columns.Item("LbrGrpCode").Cells.Item(oMatrix_BLR.VisualRowCount).Specific.String <> "" Then
'                                '        objAddOn.SetNewLine(oMatrix_BLR, oDBDSDetail_BLR)
'                                '    End If
'                                Case "ItemCode"
'                                    If pVal.ItemUID = "MMatrix" And oMatrix_BMC.Columns.Item("ItemCode").Cells.Item(pVal.Row).Specific.String <> "" Then
'                                        'oMatrix_BMC.FlushToDataSource()
'                                        oMatrix_BMC.Columns.Item("OpCode").Cells.Item(pVal.Row).Specific.String = "Link"
'                                        'oMatrix_BMC.LoadFromDataSourceEx()
'                                    End If
'                                Case "SetTime", "MProsTime"
'                                    MCCal(pVal.Row)
'                                Case "LProsTime"
'                                    LBRCal(pVal.Row)
'                                Case "Qty"
'                                    Dim Qty = oMatrix_BTL.GetCellSpecific("Qty", pVal.Row).Value
'                                    Dim Rate = oMatrix_BTL.GetCellSpecific("Rate", pVal.Row).Value
'                                    Dim LifeTime = oMatrix_BTL.GetCellSpecific("LifeTime", pVal.Row).Value


'                                    LifeTime = IIf(LifeTime.ToString.Trim = "", 0, LifeTime)

'                                    'Unit Cost calculation
'                                    Qty = IIf(Qty.ToString.Trim = "", 0, Qty)
'                                    Rate = IIf(Rate.ToString.Trim = "", 0, Rate)
'                                    If LifeTime = 0 Then
'                                        oMatrix_BTL.GetCellSpecific("UCost", pVal.Row).Value = 0
'                                    Else
'                                        oMatrix_BTL.GetCellSpecific("UCost", pVal.Row).Value = CDbl(Rate) / CDbl(LifeTime)
'                                    End If


'                                    'Sub Total Calculation
'                                    Dim UCost = oMatrix_BTL.GetCellSpecific("UCost", pVal.Row).Value
'                                    UCost = IIf(UCost.ToString.Trim = "", 0, UCost)

'                                    oMatrix_BTL.GetCellSpecific("STotal", pVal.Row).Value = CDbl(Qty) * CDbl(UCost)
'                                    oMatrix_BTL.FlushToDataSource()
'                                    oMatrix_BTL.LoadFromDataSourceEx()
'                            End Select
'                        Catch ex As Exception
'                            objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                        End Try
'                    Case SAPbouiCOM.BoEventTypes.et_CLICK
'                        Try

'                            Select Case pVal.ItemUID
'                                Case "4"
'                                    frmBLR.PaneLevel = "1"
'                                Case "5"
'                                    frmBLR.PaneLevel = "2"
'                                Case "6"
'                                    frmBLR.PaneLevel = "3"
'                                Case "1"
'                                    'clear Data Table
'                                    oMatrix_BMC.DeleteRow(oMatrix_BMC.VisualRowCount)
'c:
'                                    For i As Integer = 0 To DataTable_BMC.Rows.Count - 1
'                                        If DataTable_BMC.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BLR Then
'                                            DataTable_BMC.Rows.RemoveAt(i)
'                                            GoTo c
'                                        End If
'                                    Next

'                                    'Add Data Table
'                                    Dim n As Integer = 0
'                                    Dim MCCostSum As Double = 0
'                                    Dim MCTimeSum As Double = 0
'                                    Dim RowCount As Integer = DataTable_BMC.Rows.Count
'                                    For i As Integer = RowCount To RowCount + oMatrix_BMC.VisualRowCount - 1

'                                        DataTable_BMC.Rows.Add()
'                                        n = n + 1

'                                        DataTable_BMC.Rows.Item(i)("ParentCode") = ParentCode
'                                        DataTable_BMC.Rows.Item(i)("CCode") = CCode
'                                        DataTable_BMC.Rows.Item(i)("PLineId") = PLineId_BLR
'                                        DataTable_BMC.Rows.Item(i)("ItemCode") = oMatrix_BMC.GetCellSpecific("ItemCode", n).Value
'                                        DataTable_BMC.Rows.Item(i)("ItemCode") = oMatrix_BMC.GetCellSpecific("ItemCode", n).Value
'                                        DataTable_BMC.Rows.Item(i)("Oprn") = oMatrix_BMC.GetCellSpecific("MOpCode", n).Value
'                                        DataTable_BMC.Rows.Item(i)("ItemName") = oMatrix_BMC.GetCellSpecific("ItemName", n).Value
'                                        DataTable_BMC.Rows.Item(i)("SetTime") = oMatrix_BMC.GetCellSpecific("SetTime", n).Value
'                                        DataTable_BMC.Rows.Item(i)("SetupMs") = oMatrix_BMC.GetCellSpecific("SetupMs", n).Value
'                                        DataTable_BMC.Rows.Item(i)("MProsTime") = oMatrix_BMC.GetCellSpecific("MProsTime", n).Value
'                                        DataTable_BMC.Rows.Item(i)("MProceMs") = oMatrix_BMC.GetCellSpecific("MProceMs", n).Value
'                                        DataTable_BMC.Rows.Item(i)("TotTime") = oMatrix_BMC.GetCellSpecific("TotTime", n).Value
'                                        DataTable_BMC.Rows.Item(i)("PerHrRt") = oMatrix_BMC.GetCellSpecific("PerHrRt", n).Value
'                                        DataTable_BMC.Rows.Item(i)("STotal") = oMatrix_BMC.GetCellSpecific("STotal", n).Value

'                                        Dim STotal = oMatrix_BMC.GetCellSpecific("STotal", n).Value
'                                        STotal = IIf(STotal.ToString.Trim = "", 0, STotal)
'                                        MCCostSum = MCCostSum + CDbl(STotal)

'                                        Dim TimeTotal = oMatrix_BMC.GetCellSpecific("TotTime", n).Value
'                                        TimeTotal = IIf(TimeTotal.ToString.Trim = "", 0, TimeTotal)
'                                        MCTimeSum = MCTimeSum + CDbl(TimeTotal)

'                                    Next




'                                    '-----------------------------------------------------------------------------------------------------------------------------
'                                    ''clear Data Table
'                                    oMatrix_BTL.DeleteRow(oMatrix_BTL.VisualRowCount)
'n:
'                                    For i As Integer = 0 To DataTable_BTL.Rows.Count - 1
'                                        If DataTable_BTL.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BLR Then
'                                            DataTable_BTL.Rows.RemoveAt(i)
'                                            GoTo n
'                                        End If
'                                    Next

'                                    'Add Data Table
'                                    n = 0
'                                    Dim ToolCostSum As Double = 0
'                                    RowCount = DataTable_BTL.Rows.Count
'                                    For i As Integer = RowCount To RowCount + oMatrix_BTL.VisualRowCount - 1

'                                        DataTable_BTL.Rows.Add()
'                                        n = n + 1

'                                        DataTable_BTL.Rows.Item(i)("ParentCode") = ParentCode
'                                        DataTable_BTL.Rows.Item(i)("CCode") = CCode
'                                        DataTable_BTL.Rows.Item(i)("PLineId") = PLineId_BLR
'                                        DataTable_BTL.Rows.Item(i)("ItemCode") = oMatrix_BTL.GetCellSpecific("ItemCode", n).Value
'                                        DataTable_BTL.Rows.Item(i)("ItemName") = oMatrix_BTL.GetCellSpecific("ItemName", n).Value
'                                        DataTable_BTL.Rows.Item(i)("ToolType") = oMatrix_BTL.GetCellSpecific("ToolType", n).Value
'                                        DataTable_BTL.Rows.Item(i)("Rate") = oMatrix_BTL.GetCellSpecific("Rate", n).Value
'                                        DataTable_BTL.Rows.Item(i)("LifeTime") = oMatrix_BTL.GetCellSpecific("LifeTime", n).Value
'                                        DataTable_BTL.Rows.Item(i)("Qty") = oMatrix_BTL.GetCellSpecific("Qty", n).Value
'                                        DataTable_BTL.Rows.Item(i)("UCost") = oMatrix_BTL.GetCellSpecific("UCost", n).Value
'                                        DataTable_BTL.Rows.Item(i)("STotal") = oMatrix_BTL.GetCellSpecific("STotal", n).Value

'                                        Dim STotal = oMatrix_BTL.GetCellSpecific("STotal", n).Value
'                                        STotal = IIf(STotal.ToString.Trim = "", 0, STotal)
'                                        ToolCostSum = ToolCostSum + CDbl(STotal)

'                                    Next



'                                    'clear Data Table
'                                    oMatrix_BLR.DeleteRow(oMatrix_BLR.VisualRowCount)
'10:
'                                    For i As Integer = 0 To DataTable_BLR.Rows.Count - 1
'                                        If DataTable_BLR.Rows.Item(i)("PLineId").ToString.Trim = PLineId_BLR Then
'                                            DataTable_BLR.Rows.RemoveAt(i)
'                                            GoTo 10
'                                        End If
'                                    Next

'                                    'Add Data Table
'                                    n = 0
'                                    Dim LRCostSum As Double = 0
'                                    RowCount = DataTable_BLR.Rows.Count
'                                    For i As Integer = RowCount To RowCount + oMatrix_BLR.VisualRowCount - 1

'                                        DataTable_BLR.Rows.Add()
'                                        n = n + 1

'                                        DataTable_BLR.Rows.Item(i)("ParentCode") = ParentCode
'                                        DataTable_BLR.Rows.Item(i)("CCode") = CCode
'                                        DataTable_BLR.Rows.Item(i)("PLineId") = PLineId_BLR
'                                        DataTable_BLR.Rows.Item(i)("LbrGrpCode") = oMatrix_BLR.GetCellSpecific("LbrGrpCode", n).Value
'                                        DataTable_BLR.Rows.Item(i)("LbrGrpName") = oMatrix_BLR.GetCellSpecific("LbrGrpName", n).Value
'                                        DataTable_BLR.Rows.Item(i)("LProsTime") = oMatrix_BLR.GetCellSpecific("LProsTime", n).Value
'                                        DataTable_BLR.Rows.Item(i)("LProceMs") = oMatrix_BLR.GetCellSpecific("LProceMs", n).Value
'                                        DataTable_BLR.Rows.Item(i)("PerHrRt") = oMatrix_BLR.GetCellSpecific("PerHrRt", n).Value
'                                        DataTable_BLR.Rows.Item(i)("STotal") = oMatrix_BLR.GetCellSpecific("STotal", n).Value

'                                        Dim STotal = oMatrix_BLR.GetCellSpecific("STotal", n).Value
'                                        STotal = IIf(STotal.ToString.Trim = "", 0, STotal)
'                                        LRCostSum = LRCostSum + CDbl(STotal)

'                                    Next

'                                    'oMatrix.Columns.Item("U_LaborCost").Cells.Item(PLineId_BLR).Specific.Value = LRCostSum
'                                    oMatrix.Columns.Item("U_MCCost").Cells.Item(PLineId_BLR).Specific.Value = MCCostSum
'                                    oMatrix.Columns.Item("U_LeadTime").Cells.Item(PLineId_BLR).Specific.Value = MCTimeSum
'                                    oMatrix.Columns.Item("U_ToolCost").Cells.Item(PLineId_BLR).Specific.Value = ToolCostSum
'                                    OperationCal(PLineId_BLR)
'                                    'OperationCal(PLineId_BLR)
'                                    'OperationCal(PLineId_BLR)


'                                    If frmUOM.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmUOM.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
'                                    frmBLR.Close()
'                            End Select
'                        Catch ex As Exception
'                            objAddOn.objApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                        End Try
'                    Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
'                        Select Case pVal.ItemUID
'                            Case "MMatrix"
'                                'Link button on the Machinery
'                                Try
'                                    If pVal.ColUID = "ItemCode" And pVal.BeforeAction = False Then
'                                        oMachineMaster.LoadForm()
'                                        Dim frm As SAPbouiCOM.Form
'                                        frm = objAddOn.objApplication.Forms.ActiveForm
'                                        frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
'                                        frm.Items.Item("5").Specific.Value = oMatrix_BMC.GetCellSpecific("ItemCode", pVal.Row).Value
'                                        frm.Items.Item("1").Click()
'                                    End If
'                                Catch ex As Exception
'                                    objAddOn.objApplication.StatusBar.SetText("Link Pressed Event Failed:" & ex.Message)
'                                End Try
'                            Case "TMatrix"
'                                Try
'                                    'Link button on Tools 
'                                    If pVal.ColUID = "ItemCode" Then
'                                        oToolsMaster.LoadForm()
'                                        Dim frm As SAPbouiCOM.Form
'                                        frm = objAddOn.objApplication.Forms.ActiveForm
'                                        frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
'                                        frm.Items.Item("5").Specific.Value = oMatrix_BTL.GetCellSpecific("ItemCode", pVal.Row).Value
'                                        frm.Items.Item("1").Click()
'                                    End If
'                                Catch ex As Exception
'                                    objAddOn.objApplication.StatusBar.SetText("Link Pressed Event Failed:" & ex.Message)
'                                End Try
'                            Case "LMatrix"
'                                ' Link button on the Labour
'                                Try
'                                    objAddOn.objApplication.Menus.Item("LGR").Activate()
'                                    Dim frm As SAPbouiCOM.Form
'                                    frm = objAddOn.objApplication.Forms.ActiveForm
'                                    frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
'                                    frm.Items.Item("t_Code").Specific.Value = oMatrix_BLR.GetCellSpecific("LbrGrpCode", pVal.Row).Value
'                                    frm.Items.Item("1").Click()
'                                Catch ex As Exception
'                                    objAddOn.objApplication.StatusBar.SetText("Link Pressed Event Failed:" & ex.Message)
'                                End Try
'                        End Select
'                End Select

'            End If
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("Item Event (BTL) Methord Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        End Try
'    End Sub

'#End Region

'#Region "--- Labor ----"


'    Sub LoadForm_BLR()
'        Try
'            frmBLR = objAddOn.objUIXml.LoadScreenXML(BOMLaborXML, Ananth.SBOLib.UIXML.enuResourceType.Embeded, BOMLaborFormId)
'            'Assign Data Source
'            oDBDSDetail_BTL = frmBLR.DataSources.DBDataSources.Item(0)
'            oDBDSDetail_BLR = frmBLR.DataSources.DBDataSources.Item(1)
'            oDBDSDetail_BMC = frmBLR.DataSources.DBDataSources.Item(2)
'            'Assign Matrix
'            'Assign Matrix
'            oMatrix_BTL = frmBLR.Items.Item("TMatrix").Specific
'            oMatrix_BMC = frmBLR.Items.Item("MMatrix").Specific
'            oMatrix_BLR = frmBLR.Items.Item("LMatrix").Specific
'            Me.InitForm_BTL()
'            InitForm_BMC()
'            'InitForm_BLR()
'        Catch ex As Exception
'            objAddOn.Msg("Load (BLR) Form Method Failed:" & ex.Message)
'        End Try
'    End Sub


'    Sub ItemEvent_BLR(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
'        Try
'            Select Case pVal.EventType
'                Case SAPbouiCOM.BoEventTypes.et_CLICK
'                    Try
'                        Select Case pVal.ItemUID
'                            Case "1"
'                                '----------- Row Validation-----------------------------------------------------------------------------------------------
'                                If pVal.BeforeAction Then
'                                    'For i As Integer = 1 To oMatrix_BLR.VisualRowCount
'                                    '    Dim BQty = oMatrix_BLR.GetCellSpecific("BQty", 1).Value
'                                    '    Dim UCost = oMatrix_BLR.GetCellSpecific("UCost", 1).Value
'                                    '    BQty = IIf(BQty.ToString.Trim = "", 0, BQty)
'                                    '    UCost = IIf(UCost.ToString.Trim = "", 0, UCost)

'                                    '    If CDbl(BQty) = 0 Then
'                                    '        objaddon.Msg("Line No : " & i.ToString & " Please Enter Base Qty. ")
'                                    '        BubbleEvent = False
'                                    '        Return
'                                    '    End If

'                                    '    If CDbl(UCost) = 0 Then
'                                    '        objaddon.Msg("Line No : " & i.ToString & " Please Enter Unit Cost ")
'                                    '        BubbleEvent = False
'                                    '        Return
'                                    '    End If

'                                    'Next
'                                End If
'                                '-----------------------------------------------------------------------------------------------------------------------------


'                                If pVal.BeforeAction = False Then


'                                    If frmUOM.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmUOM.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
'                                    frmBLR.Close()
'                                End If
'                        End Select
'                    Catch ex As Exception
'                        objAddOn.objApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                    End Try

'                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
'                    Select Case pVal.ItemUID

'                    End Select
'            End Select
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("Item Event (BLR) Methord Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        End Try
'    End Sub

'    Sub LBRCal(ByVal Row As Integer)
'        Try

'            ' Process Time Calculation
'            Dim ProceMs = oMatrix_BLR.GetCellSpecific("LProceMs", Row).Value
'            Dim ProsTime = oMatrix_BLR.GetCellSpecific("LProsTime", Row).Value

'            ProsTime = IIf(ProsTime.ToString.Trim = "", 0, ProsTime)
'            Dim ProsTimeHr As Double = objAddOn.ConvertHrs(ProsTime, ProceMs)

'            Dim PerHrRt = oMatrix_BLR.GetCellSpecific("PerHrRt", Row).Value
'            PerHrRt = IIf(PerHrRt.ToString.Trim = "", 0, PerHrRt)

'            'Total Amount Calculation
'            oMatrix_BLR.GetCellSpecific("STotal", Row).Value = CDbl(ProsTimeHr) * PerHrRt

'            oMatrix_BLR.FlushToDataSource()
'            oMatrix_BLR.LoadFromDataSourceEx()
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("MCCal Methord Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        End Try
'    End Sub

'    Sub SUBLBRCal(ByVal Row As Integer)
'        Try

'            ' Process Time Calculation
'            Dim ProceMs = oMatrix_SBLR.GetCellSpecific("LProceMs", Row).Value
'            Dim ProsTime = oMatrix_SBLR.GetCellSpecific("LProsTime", Row).Value

'            ProsTime = IIf(ProsTime.ToString.Trim = "", 0, ProsTime)
'            Dim ProsTimeHr As Double = objAddOn.ConvertHrs(ProsTime, ProceMs)

'            Dim PerHrRt = oMatrix_SBLR.GetCellSpecific("PerHrRt", Row).Value
'            PerHrRt = IIf(PerHrRt.ToString.Trim = "", 0, PerHrRt)

'            'Total Amount Calculation
'            oMatrix_SBLR.GetCellSpecific("STotal", Row).Value = CDbl(ProsTimeHr) * PerHrRt

'            oMatrix_SBLR.FlushToDataSource()
'            oMatrix_SBLR.LoadFromDataSourceEx()
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("MCCal Methord Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        End Try
'    End Sub

'#End Region

'    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
'        Try
'            Select Case BusinessObjectInfo.EventType

'                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE

'                    Try
'                        If BusinessObjectInfo.BeforeAction Then
'                            If Me.ValidateAll() = False Then
'                                System.Media.SystemSounds.Asterisk.Play()
'                                BubbleEvent = False
'                                Exit Sub
'                            Else
'                                If Not InsertBOMDetails() Then
'                                    BubbleEvent = False
'                                    Return
'                                End If
'                            End If
'                        End If
'                    Catch ex As Exception
'                        BubbleEvent = False
'                    Finally
'                    End Try

'                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
'                    If BusinessObjectInfo.ActionSuccess = True And UOMTypeEx <> "" Then
'                        Try
'                            LoadParentDetails()
'                        Catch ex As Exception
'                            objAddOn.objApplication.StatusBar.SetText("Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'                        End Try
'                    End If
'            End Select
'        Catch ex As Exception
'            objAddOn.objApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
'        Finally
'        End Try
'    End Sub

'    Function InsertBOMDetails() As Boolean
'        Try
'            Dim strSQL As String = ""
'            Dim docEntry As String = ""
'            '------Raw Material-----------------------------------------------------------------------------------------------
'            strSQL = " Delete [@AIS_OBRM] where U_ParentCode = '" & ParentCode & "' ; "

'            docEntry = objAddOn.getSingleValue(" Select Max(DocEntry) from [@AIS_OBRM] ")
'            docEntry = IIf(docEntry.Trim = "" Or docEntry.Trim = "0", 1, docEntry.Trim)
'            docEntry = CInt(docEntry) + 1

'            Dim LineId As Integer = 1

'            For i As Integer = 0 To DataTable_BRM.Rows.Count - 1

'                strSQL = strSQL + " Insert Into [@AIS_OBRM] ([DocEntry],[LineId],[VisOrder],[Object],[LogInst],[U_WhsCode]," & _
'                         " [U_ParentMac],[U_ParentCode],[U_CCode],[U_PLineId],[U_ItemCode],[U_ItemName],[U_UOM],[U_BQty],[U_UCost],[U_STotal]) Values " & _
'                         " ( '" & docEntry & "' ," & LineId & ",null,null,null,'" & _
'                         DataTable_BRM.Rows.Item(i)("WhsCode").ToString & "','" & _
'                         DataTable_BRM.Rows.Item(i)("ParentMac").ToString & "','" & _
'                         DataTable_BRM.Rows.Item(i)("ParentCode").ToString & "','" & _
'                         DataTable_BRM.Rows.Item(i)("CCode").ToString & "','" & _
'                         DataTable_BRM.Rows.Item(i)("PLineId").ToString & "','" & _
'                         DataTable_BRM.Rows.Item(i)("ItemCode").ToString & "','" & _
'                         DataTable_BRM.Rows.Item(i)("ItemName").ToString & "','" & _
'                         DataTable_BRM.Rows.Item(i)("UOM").ToString & "','" & _
'                         DataTable_BRM.Rows.Item(i)("BQty").ToString & "','" & _
'                         DataTable_BRM.Rows.Item(i)("UCost").ToString & "','" & _
'                         DataTable_BRM.Rows.Item(i)("STotal").ToString & "') ; "

'                LineId = LineId + 1
'            Next

'            '--------Machine Details-------------------------------------------------------------------------------------------------

'            strSQL = strSQL + " Delete [@AIS_OBMC] where U_ParentCode = '" & ParentCode & "' ; "

'            docEntry = objAddOn.getSingleValue(" Select Max(DocEntry) from [@AIS_OBMC] ")
'            docEntry = IIf(docEntry.Trim = "" Or docEntry.Trim = "0", 1, docEntry.Trim)
'            docEntry = CInt(docEntry) + 1


'            For i As Integer = 0 To DataTable_BMC.Rows.Count - 1

'                strSQL = strSQL + " Insert Into [@AIS_OBMC] ([DocEntry],[LineId],[VisOrder],[Object],[LogInst]," & _
'                         " [U_ParentCode],[U_CCode],[U_Oprn],[U_PLineId],[U_OpCode],[U_ItemCode],[U_ItemName],[U_SetTime],[U_SetupMs],[U_ProsTime],[U_ProceMs],[U_TotTime],[U_PerHrRt],[U_STotal]) Values " & _
'                         " ( '" & docEntry & "' ," & LineId & ",null,null,null,'" & _
'                         DataTable_BMC.Rows.Item(i)("ParentCode").ToString & "','" & _
'                          DataTable_BMC.Rows.Item(i)("CCode").ToString & "','" & _
'                          DataTable_BMC.Rows.Item(i)("Oprn").ToString & "','" & _
'                         DataTable_BMC.Rows.Item(i)("PLineId").ToString & "','" & _
'                         DataTable_BMC.Rows.Item(i)("OpCode").ToString & "','" & _
'                         DataTable_BMC.Rows.Item(i)("ItemCode").ToString & "','" & _
'                         DataTable_BMC.Rows.Item(i)("ItemName").ToString & "','" & _
'                         DataTable_BMC.Rows.Item(i)("SetTime").ToString & "','" & _
'                         DataTable_BMC.Rows.Item(i)("SetupMs").ToString & "','" & _
'                         DataTable_BMC.Rows.Item(i)("MProsTime").ToString & "','" & _
'                          DataTable_BMC.Rows.Item(i)("MProceMs").ToString & "','" & _
'                           DataTable_BMC.Rows.Item(i)("TotTime").ToString & "','" & _
'                         DataTable_BMC.Rows.Item(i)("PerHrRt").ToString & "','" & _
'                         DataTable_BMC.Rows.Item(i)("STotal").ToString & "') ; "

'                LineId = LineId + 1
'            Next

'            '--------Tools Details-------------------------------------------------------------------------------------------------

'            strSQL = strSQL + " Delete [@AIS_OBTL] where U_ParentCode = '" & ParentCode & "' ; "

'            docEntry = objAddOn.getSingleValue(" Select Max(DocEntry) from [@AIS_OBTL] ")
'            docEntry = IIf(docEntry.Trim = "" Or docEntry.Trim = "0", 1, docEntry.Trim)
'            docEntry = CInt(docEntry) + 1
'            For i As Integer = 0 To DataTable_BTL.Rows.Count - 1

'                Dim LifeTime = DataTable_BTL.Rows.Item(i)("LifeTime").ToString
'                LifeTime = IIf(LifeTime.Trim = "", 0, LifeTime)

'                strSQL = strSQL + " Insert Into [@AIS_OBTL] ([DocEntry],[LineId],[VisOrder],[Object],[LogInst]," & _
'                         " [U_ParentCode],[U_CCode],[U_PLineId],[U_ItemCode],[U_ItemName],[U_ToolType],[U_Rate],[U_LifeTime],[U_Qty],[U_UCost],[U_STotal]) Values " & _
'                         " ( '" & docEntry & "' ," & LineId & ",null,null,null,'" & _
'                         DataTable_BTL.Rows.Item(i)("ParentCode").ToString & "','" & _
'                         DataTable_BTL.Rows.Item(i)("CCode").ToString & "','" & _
'                         DataTable_BTL.Rows.Item(i)("PLineId").ToString & "','" & _
'                         DataTable_BTL.Rows.Item(i)("ItemCode").ToString & "','" & _
'                         DataTable_BTL.Rows.Item(i)("ItemName").ToString & "','" & _
'                         DataTable_BTL.Rows.Item(i)("ToolType").ToString & "','" & _
'                          DataTable_BTL.Rows.Item(i)("Rate").ToString & "','" & _
'                          LifeTime & "','" & _
'                            DataTable_BTL.Rows.Item(i)("Qty").ToString & "','" & _
'                         DataTable_BTL.Rows.Item(i)("UCost").ToString & "','" & _
'                         DataTable_BTL.Rows.Item(i)("STotal").ToString & "') ; "

'                LineId = LineId + 1
'            Next

'            '--------Scrap Details-------------------------------------------------------------------------------------------------

'            strSQL = strSQL + " Delete [@AIS_OBSC] where U_ParentCode = '" & ParentCode & "' ; "

'            docEntry = objAddOn.getSingleValue(" Select Max(DocEntry) from [@AIS_OBSC] ")
'            docEntry = IIf(docEntry.Trim = "" Or docEntry.Trim = "0", 1, docEntry.Trim)
'            docEntry = CInt(docEntry) + 1

'            For i As Integer = 0 To DataTable_BSC.Rows.Count - 1

'                strSQL = strSQL + " Insert Into [@AIS_OBSC] ([DocEntry],[LineId],[VisOrder],[Object],[LogInst]," & _
'                         " [U_ParentCode],[U_CCode],[U_PLineId],[U_ItemCode],[U_ItemName],[U_UOM],[U_BQty],[U_UCost],[U_STotal]) Values " & _
'                         " ( '" & docEntry & "' ," & LineId & ",null,null,null,'" & _
'                         DataTable_BSC.Rows.Item(i)("ParentCode").ToString & "','" & _
'                          DataTable_BSC.Rows.Item(i)("CCode").ToString & "','" & _
'                         DataTable_BSC.Rows.Item(i)("PLineId").ToString & "','" & _
'                         DataTable_BSC.Rows.Item(i)("ItemCode").ToString & "','" & _
'                         DataTable_BSC.Rows.Item(i)("ItemName").ToString & "','" & _
'                         DataTable_BSC.Rows.Item(i)("UOM").ToString & "','" & _
'                         DataTable_BSC.Rows.Item(i)("BQty").ToString & "','" & _
'                          DataTable_BSC.Rows.Item(i)("UCost").ToString & "','" & _
'                         DataTable_BSC.Rows.Item(i)("STotal").ToString & "') ; "

'                LineId = LineId + 1
'            Next

'            '--------Consumable Details-------------------------------------------------------------------------------------------------

'            strSQL = strSQL + " Delete [@AIS_OBCO] where U_ParentCode = '" & ParentCode & "' ; "

'            docEntry = objAddOn.getSingleValue(" Select Max(DocEntry) from [@AIS_OBCO] ")
'            docEntry = IIf(docEntry.Trim = "" Or docEntry.Trim = "0", 1, docEntry.Trim)
'            docEntry = CInt(docEntry) + 1

'            For i As Integer = 0 To DataTable_BCO.Rows.Count - 1

'                strSQL = strSQL + " Insert Into [@AIS_OBCO] ([DocEntry],[LineId],[VisOrder],[Object],[LogInst]," & _
'                         " [U_ParentCode],[U_CCode],[U_PLineId],[U_ItemCode],[U_ItemName],[U_UOM],[U_BQty],[U_UCost],[U_STotal]) Values " & _
'                         " ( '" & docEntry & "' ," & LineId & ",null,null,null,'" & _
'                         DataTable_BCO.Rows.Item(i)("ParentCode").ToString & "','" & _
'                         DataTable_BCO.Rows.Item(i)("CCode").ToString & "','" & _
'                         DataTable_BCO.Rows.Item(i)("PLineId").ToString & "','" & _
'                         DataTable_BCO.Rows.Item(i)("ItemCode").ToString & "','" & _
'                         DataTable_BCO.Rows.Item(i)("ItemName").ToString & "','" & _
'                         DataTable_BCO.Rows.Item(i)("UOM").ToString & "','" & _
'                         DataTable_BCO.Rows.Item(i)("BQty").ToString & "','" & _
'                          DataTable_BCO.Rows.Item(i)("UCost").ToString & "','" & _
'                         DataTable_BCO.Rows.Item(i)("STotal").ToString & "') ; "

'                LineId = LineId + 1
'            Next

'            '--------Labor Details-------------------------------------------------------------------------------------------------

'            strSQL = strSQL + " Delete [@AIS_OBLR] where U_ParentCode = '" & ParentCode & "' ; "

'            docEntry = objAddOn.getSingleValue(" Select Max(DocEntry) from [@AIS_OBLR] ")
'            docEntry = IIf(docEntry.Trim = "" Or docEntry.Trim = "0", 1, docEntry.Trim)
'            docEntry = CInt(docEntry) + 1


'            For i As Integer = 0 To DataTable_BLR.Rows.Count - 1

'                strSQL = strSQL + " Insert Into [@AIS_OBLR] ([DocEntry],[LineId],[VisOrder],[Object],[LogInst]," & _
'                         " [U_ParentCode],[U_CCode],[U_PLineId],[U_LbrGrpCo],[U_LbrGrpNa],[U_ProsTime],[U_ProceMs],[U_PerHrRt],[U_STotal]) Values " & _
'                         " ( '" & docEntry & "' ," & LineId & ",null,null,null,'" & _
'                         DataTable_BLR.Rows.Item(i)("ParentCode").ToString & "','" & _
'                         DataTable_BLR.Rows.Item(i)("CCode").ToString & "','" & _
'                         DataTable_BLR.Rows.Item(i)("PLineId").ToString & "','" & _
'                         DataTable_BLR.Rows.Item(i)("LbrGrpCode").ToString & "','" & _
'                         DataTable_BLR.Rows.Item(i)("LbrGrpName").ToString & "','" & _
'                         DataTable_BLR.Rows.Item(i)("LProsTime").ToString & "','" & _
'                         DataTable_BLR.Rows.Item(i)("LProceMs").ToString & "','" & _
'                         DataTable_BLR.Rows.Item(i)("PerHrRt").ToString & "','" & _
'                         DataTable_BLR.Rows.Item(i)("STotal").ToString & "') ; "

'                LineId = LineId + 1
'            Next



'            '--------Sub Labor Details-------------------------------------------------------------------------------------------------

'            strSQL = strSQL + " Delete [@AIS_SBLR] where U_ParentCode = '" & ParentCode & "' ; "

'            docEntry = objAddOn.getSingleValue(" Select Max(DocEntry) from [@AIS_SBLR] ")
'            docEntry = IIf(docEntry.Trim = "" Or docEntry.Trim = "0", 1, docEntry.Trim)
'            docEntry = CInt(docEntry) + 1


'            For i As Integer = 0 To DataTable_SBLR.Rows.Count - 1

'                strSQL = strSQL + " Insert Into [@AIS_SBLR] ([DocEntry],[LineId],[VisOrder],[Object],[LogInst]," & _
'                         " [U_ParentMac],[U_ParentCode],[U_CCode],[U_PLineId],[U_LbrGrpCo],[U_LbrGrpNa],[U_ProsTime],[U_ProceMs],[U_PerHrRt],[U_STotal]) Values " & _
'                         " ( '" & docEntry & "' ," & LineId & ",null,null,null,'" & _
'                         DataTable_SBLR.Rows.Item(i)("ParentMac").ToString & "','" & _
'                         DataTable_SBLR.Rows.Item(i)("ParentCode").ToString & "','" & _
'                         DataTable_SBLR.Rows.Item(i)("CCode").ToString & "','" & _
'                         DataTable_SBLR.Rows.Item(i)("PLineId").ToString & "','" & _
'                         DataTable_SBLR.Rows.Item(i)("LbrGrpCode").ToString & "','" & _
'                         DataTable_SBLR.Rows.Item(i)("LbrGrpName").ToString & "','" & _
'                         DataTable_SBLR.Rows.Item(i)("LProsTime").ToString & "','" & _
'                         DataTable_SBLR.Rows.Item(i)("LProceMs").ToString & "','" & _
'                         DataTable_SBLR.Rows.Item(i)("PerHrRt").ToString & "','" & _
'                         DataTable_SBLR.Rows.Item(i)("STotal").ToString & "') ; "

'                LineId = LineId + 1
'            Next


'            objAddOn.DoQuery(strSQL)

'            Return True

'        Catch ex As Exception
'            objAddOn.Msg(" Insert BOM Details Methord Failed : " + ex.Message)
'            Return False
'        End Try

'    End Function

'    Sub LoadParentDetails()
'        Try
'            ParentCode = frmUOM.Items.Item("4").Specific.Value
'            Dim rs As SAPbobsCOM.Recordset
'            '------Raw Material-----------------------------------------------------------------------------------------------


'            rs = objAddOn.DoQuery(" Select U_ParentCode ParentCode,U_CCode CCode,U_PLineId PLineId, U_ItemCode ItemCode,U_WhsCode 'WhsCode'" & _
'                               " ,U_ItemName ItemName, U_UOM UOM, U_BQty BQty, U_UCost UCost, U_STotal STotal,U_ParentMac ParentMac from [@AIS_OBRM] " & _
'                               " where  U_ParentCode ='" & ParentCode & "' ")
'            DataTable_BRM.Clear()
'            For i As Integer = 1 To rs.RecordCount

'                ' Data Table Filling
'                DataTable_BRM.Rows.Add()

'                DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("ParentCode") = rs.Fields.Item("ParentCode").Value.ToString
'                DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("CCode") = rs.Fields.Item("CCode").Value.ToString
'                DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("PLineId") = rs.Fields.Item("PLineId").Value.ToString
'                DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("ItemCode") = rs.Fields.Item("ItemCode").Value.ToString
'                DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("ItemName") = rs.Fields.Item("ItemName").Value.ToString
'                DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("UOM") = rs.Fields.Item("UOM").Value.ToString
'                DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("BQty") = rs.Fields.Item("BQty").Value.ToString
'                DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("UCost") = rs.Fields.Item("UCost").Value.ToString
'                DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("STotal") = rs.Fields.Item("STotal").Value.ToString
'                DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("ParentMac") = rs.Fields.Item("ParentMac").Value.ToString
'                DataTable_BRM.Rows.Item(DataTable_BRM.Rows.Count - 1)("WhsCode") = rs.Fields.Item("WhsCode").Value.ToString

'                rs.MoveNext()
'            Next


'            '--------Sub Labor Details-------------------------------------------------------------------------------------------------

'            rs = objAddOn.DoQuery(" Select U_ParentCode ParentCode,U_CCode CCode,U_PLineId PLineId, U_LbrGrpCo LbrGrpCode," & _
'                               " U_LbrGrpNa LbrGrpName, U_ProsTime ProsTime, " & _
'                               " U_ProceMs ProceMs, U_PerHrRt PerHrRt, U_STotal STotal,U_ParentMac ParentMac from [@AIS_SBLR] " & _
'                               " where  U_ParentCode ='" & ParentCode & "'")

'            DataTable_SBLR.Clear()
'            For i As Integer = 1 To rs.RecordCount

'                ' Data Table Filling
'                DataTable_SBLR.Rows.Add()

'                DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("ParentCode") = rs.Fields.Item("ParentCode").Value.ToString
'                DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("CCode") = rs.Fields.Item("CCode").Value.ToString
'                DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("PLineId") = rs.Fields.Item("PLineId").Value.ToString
'                DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("LbrGrpCode") = rs.Fields.Item("LbrGrpCode").Value.ToString
'                DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("LbrGrpName") = rs.Fields.Item("LbrGrpName").Value.ToString
'                DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("LProsTime") = rs.Fields.Item("ProsTime").Value.ToString
'                DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("LProceMs") = rs.Fields.Item("ProceMs").Value.ToString
'                DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("PerHrRt") = rs.Fields.Item("PerHrRt").Value.ToString
'                DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("STotal") = rs.Fields.Item("STotal").Value.ToString
'                DataTable_SBLR.Rows.Item(DataTable_SBLR.Rows.Count - 1)("ParentMac") = rs.Fields.Item("ParentMac").Value.ToString

'                rs.MoveNext()
'            Next


'            '--------Machine Details-------------------------------------------------------------------------------------------------

'            rs = objAddOn.DoQuery(" Select U_ParentCode ParentCode,U_Oprn 'Oprn',U_CCode CCode,U_PLineId PLineId, U_ItemCode ItemCode,U_OpCode 'OpCode'," & _
'                               " U_ItemName ItemName, U_SetTime SetTime, U_SetupMs SetupMs, U_ProsTime ProsTime, " & _
'                               " U_ProceMs ProceMs,U_TotTime TotTime, U_PerHrRt PerHrRt, U_STotal STotal from [@AIS_OBMC] " & _
'                               " where  U_ParentCode ='" & ParentCode & "' ")

'            DataTable_BMC.Clear()
'            For i As Integer = 1 To rs.RecordCount

'                ' Data Table Filling
'                DataTable_BMC.Rows.Add()

'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("ParentCode") = rs.Fields.Item("ParentCode").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("Oprn") = rs.Fields.Item("Oprn").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("CCode") = rs.Fields.Item("CCode").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("PLineId") = rs.Fields.Item("PLineId").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("ItemCode") = rs.Fields.Item("ItemCode").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("OpCode") = rs.Fields.Item("OpCode").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("ItemName") = rs.Fields.Item("ItemName").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("SetTime") = rs.Fields.Item("SetTime").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("SetupMs") = rs.Fields.Item("SetupMs").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("MProsTime") = rs.Fields.Item("ProsTime").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("MProceMs") = rs.Fields.Item("ProceMs").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("TotTime") = rs.Fields.Item("TotTime").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("PerHrRt") = rs.Fields.Item("PerHrRt").Value.ToString
'                DataTable_BMC.Rows.Item(DataTable_BMC.Rows.Count - 1)("STotal") = rs.Fields.Item("STotal").Value.ToString

'                rs.MoveNext()
'            Next

'            '--------Tools Details-------------------------------------------------------------------------------------------------

'            rs = objAddOn.DoQuery(" Select U_ParentCode ParentCode,U_CCode CCode,U_PLineId PLineId, U_ItemCode ItemCode," & _
'                               " U_ItemName ItemName, U_ToolType ToolType,U_Rate Rate ,U_LifeTime LifeTime ,U_Qty Qty , U_UCost UCost, U_STotal STotal " & _
'                               " from [@AIS_OBTL] " & _
'                               " where  U_ParentCode ='" & ParentCode & "' ")

'            DataTable_BTL.Clear()
'            For i As Integer = 1 To rs.RecordCount

'                ' Data Table Filling
'                DataTable_BTL.Rows.Add()

'                DataTable_BTL.Rows.Item(DataTable_BTL.Rows.Count - 1)("ParentCode") = rs.Fields.Item("ParentCode").Value.ToString
'                DataTable_BTL.Rows.Item(DataTable_BTL.Rows.Count - 1)("CCode") = rs.Fields.Item("CCode").Value.ToString
'                DataTable_BTL.Rows.Item(DataTable_BTL.Rows.Count - 1)("PLineId") = rs.Fields.Item("PLineId").Value.ToString
'                DataTable_BTL.Rows.Item(DataTable_BTL.Rows.Count - 1)("ItemCode") = rs.Fields.Item("ItemCode").Value.ToString
'                DataTable_BTL.Rows.Item(DataTable_BTL.Rows.Count - 1)("ItemName") = rs.Fields.Item("ItemName").Value.ToString
'                DataTable_BTL.Rows.Item(DataTable_BTL.Rows.Count - 1)("ToolType") = rs.Fields.Item("ToolType").Value.ToString
'                DataTable_BTL.Rows.Item(DataTable_BTL.Rows.Count - 1)("Rate") = rs.Fields.Item("Rate").Value.ToString
'                DataTable_BTL.Rows.Item(DataTable_BTL.Rows.Count - 1)("LifeTime") = rs.Fields.Item("LifeTime").Value.ToString
'                DataTable_BTL.Rows.Item(DataTable_BTL.Rows.Count - 1)("Qty") = rs.Fields.Item("Qty").Value.ToString
'                DataTable_BTL.Rows.Item(DataTable_BTL.Rows.Count - 1)("UCost") = rs.Fields.Item("UCost").Value.ToString
'                DataTable_BTL.Rows.Item(DataTable_BTL.Rows.Count - 1)("STotal") = rs.Fields.Item("STotal").Value.ToString

'                rs.MoveNext()
'            Next

'            ''--------Scrap Details-------------------------------------------------------------------------------------------------

'            'rs = objAddOn.DoQuery(" Select U_ParentCode ParentCode,U_PLineId PLineId, U_ItemCode ItemCode," & _
'            '                   " U_ItemName ItemName, U_UOM UOM, U_BQty BQty, U_UCost UCost, U_STotal STotal " & _
'            '                   " from [@AIS_OBSC] " & _
'            '                   " where  U_ParentCode ='" & ParentCode & "' ")

'            'DataTable_BSC.Clear()
'            'For i As Integer = 1 To rs.RecordCount

'            '    ' Data Table Filling
'            '    DataTable_BSC.Rows.Add()

'            '    DataTable_BSC.Rows.Item(DataTable_BSC.Rows.Count - 1)("ParentCode") = rs.Fields.Item("ParentCode").Value.ToString
'            '    DataTable_BSC.Rows.Item(DataTable_BSC.Rows.Count - 1)("PLineId") = rs.Fields.Item("PLineId").Value.ToString
'            '    DataTable_BSC.Rows.Item(DataTable_BSC.Rows.Count - 1)("ItemCode") = rs.Fields.Item("ItemCode").Value.ToString
'            '    DataTable_BSC.Rows.Item(DataTable_BSC.Rows.Count - 1)("ItemName") = rs.Fields.Item("ItemName").Value.ToString
'            '    DataTable_BSC.Rows.Item(DataTable_BSC.Rows.Count - 1)("UOM") = rs.Fields.Item("UOM").Value.ToString
'            '    DataTable_BSC.Rows.Item(DataTable_BSC.Rows.Count - 1)("BQty") = rs.Fields.Item("BQty").Value.ToString
'            '    DataTable_BSC.Rows.Item(DataTable_BSC.Rows.Count - 1)("UCost") = rs.Fields.Item("UCost").Value.ToString
'            '    DataTable_BSC.Rows.Item(DataTable_BSC.Rows.Count - 1)("STotal") = rs.Fields.Item("STotal").Value.ToString
'            '    rs.MoveNext()
'            'Next

'            ''--------Consumable Details-------------------------------------------------------------------------------------------------

'            'rs = objAddOn.DoQuery(" Select U_ParentCode ParentCode,U_PLineId PLineId, U_ItemCode ItemCode," & _
'            '                   " U_ItemName ItemName, U_UOM UOM, U_BQty BQty, U_UCost UCost, U_STotal STotal " & _
'            '                   " from [@AIS_OBCO] " & _
'            '                   " where  U_ParentCode ='" & ParentCode & "' ")

'            'DataTable_BCO.Clear()
'            'For i As Integer = 1 To rs.RecordCount

'            '    ' Data Table Filling
'            '    DataTable_BCO.Rows.Add()

'            '    DataTable_BCO.Rows.Item(DataTable_BCO.Rows.Count - 1)("ParentCode") = rs.Fields.Item("ParentCode").Value.ToString
'            '    DataTable_BCO.Rows.Item(DataTable_BCO.Rows.Count - 1)("PLineId") = rs.Fields.Item("PLineId").Value.ToString
'            '    DataTable_BCO.Rows.Item(DataTable_BCO.Rows.Count - 1)("ItemCode") = rs.Fields.Item("ItemCode").Value.ToString
'            '    DataTable_BCO.Rows.Item(DataTable_BCO.Rows.Count - 1)("ItemName") = rs.Fields.Item("ItemName").Value.ToString
'            '    DataTable_BCO.Rows.Item(DataTable_BCO.Rows.Count - 1)("UOM") = rs.Fields.Item("UOM").Value.ToString
'            '    DataTable_BCO.Rows.Item(DataTable_BCO.Rows.Count - 1)("BQty") = rs.Fields.Item("BQty").Value.ToString
'            '    DataTable_BCO.Rows.Item(DataTable_BCO.Rows.Count - 1)("UCost") = rs.Fields.Item("UCost").Value.ToString
'            '    DataTable_BCO.Rows.Item(DataTable_BCO.Rows.Count - 1)("STotal") = rs.Fields.Item("STotal").Value.ToString
'            '    rs.MoveNext()
'            'Next

'            '--------Labor Details-------------------------------------------------------------------------------------------------

'            rs = objAddOn.DoQuery(" Select U_ParentCode ParentCode,U_CCode CCode,U_PLineId PLineId, U_LbrGrpCo LbrGrpCode," & _
'                               " U_LbrGrpNa LbrGrpName, U_ProsTime ProsTime, " & _
'                               " U_ProceMs ProceMs, U_PerHrRt PerHrRt, U_STotal STotal from [@AIS_OBLR] " & _
'                               " where  U_ParentCode ='" & ParentCode & "' ")

'            DataTable_BLR.Clear()
'            For i As Integer = 1 To rs.RecordCount

'                ' Data Table Filling
'                DataTable_BLR.Rows.Add()

'                DataTable_BLR.Rows.Item(DataTable_BLR.Rows.Count - 1)("ParentCode") = rs.Fields.Item("ParentCode").Value.ToString
'                DataTable_BLR.Rows.Item(DataTable_BLR.Rows.Count - 1)("CCode") = rs.Fields.Item("CCode").Value.ToString
'                DataTable_BLR.Rows.Item(DataTable_BLR.Rows.Count - 1)("PLineId") = rs.Fields.Item("PLineId").Value.ToString
'                DataTable_BLR.Rows.Item(DataTable_BLR.Rows.Count - 1)("LbrGrpCode") = rs.Fields.Item("LbrGrpCode").Value.ToString
'                DataTable_BLR.Rows.Item(DataTable_BLR.Rows.Count - 1)("LbrGrpName") = rs.Fields.Item("LbrGrpName").Value.ToString
'                DataTable_BLR.Rows.Item(DataTable_BLR.Rows.Count - 1)("LProsTime") = rs.Fields.Item("ProsTime").Value.ToString
'                DataTable_BLR.Rows.Item(DataTable_BLR.Rows.Count - 1)("LProceMs") = rs.Fields.Item("ProceMs").Value.ToString
'                DataTable_BLR.Rows.Item(DataTable_BLR.Rows.Count - 1)("PerHrRt") = rs.Fields.Item("PerHrRt").Value.ToString
'                DataTable_BLR.Rows.Item(DataTable_BLR.Rows.Count - 1)("STotal") = rs.Fields.Item("STotal").Value.ToString

'                rs.MoveNext()
'            Next

'        Catch ex As Exception
'            objAddOn.Msg(" Load Parent Method Faild : " + ex.Message)
'        End Try
'    End Sub



'End Class
