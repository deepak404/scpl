﻿Public Class ClsQualityParam
    Dim oDBDSHeader, oDBDSLine As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim oQualityForm As SAPbouiCOM.Form
    Public Const FormType = "AIS_PARAM"
    Dim oDataTable As SAPbouiCOM.DataTable
    Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
    Dim Ors, Ors1 As SAPbobsCOM.Recordset
    'Public Sub LoadScreen(ByVal opcod As String, ByVal opnam As String, ByVal WoNo As String)
    Public Function LoadScreen(ByVal opcod As String, ByVal opnam As String, ByVal WoNo As String)

        Try
            Dim str, opstr As String
            oQualityForm = objAddOn.objUIXml.LoadScreenXML("Quality Param.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            oDBDSHeader = oQualityForm.DataSources.DBDataSources.Item(0)
            oDBDSLine = oQualityForm.DataSources.DBDataSources.Item(1)
            oQualityForm.DataBrowser.BrowseBy = "9"



            'oForm.EnableMenu("1292", True)
            'oForm.EnableMenu("1293", True)
            'oForm.EnableMenu("1282", True)
            oMatrix = oQualityForm.Items.Item("20").Specific

            Ors = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Ors1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            str = "SELECT [DocEntry] 'docentry' FROM [dbo].[@AIS_PARAM] where U_OpCod = '" & opcod & "' and U_WoNo = '" & WoNo & "'"
            Ors = objAddOn.DoQuery(str)
            If Ors.RecordCount > 0 Then
                oQualityForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                oQualityForm.Items.Item("WoNo").Enabled = True
                oQualityForm.Items.Item("Code").Enabled = True
                oQualityForm.Items.Item("WoNo").Specific.string = WoNo
                oQualityForm.Items.Item("Code").Specific.string = opcod
                oQualityForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                oQualityForm.Items.Item("Code").Enabled = False
                oQualityForm.Items.Item("Name").Enabled = False
                oQualityForm.Items.Item("WoNo").Enabled = False
            Else
                oQualityForm.Items.Item("9").Specific.value = oQualityForm.BusinessObject.GetNextSerialNumber("-1", "AIS_PARAM")
                oQualityForm.Items.Item("10").Specific.value = oQualityForm.BusinessObject.GetNextSerialNumber("-1", "AIS_PARAM")
                oQualityForm.Items.Item("WoNo").Specific.string = WoNo
                oQualityForm.Items.Item("Code").Specific.string = opcod
                oQualityForm.Items.Item("Name").Specific.string = opnam
               

                opstr = "SELECT T1.[U_Param], T1.[U_Min], T1.[U_Max] FROM [dbo].[@AS_OPRN] T0  INNER JOIN [dbo].[@AS_PRN1]  T1 ON T0.[Code]=T1.[Code] WHERE T1.[Code] ='" & opcod & "'"
                Ors1 = objAddOn.DoQuery(opstr)
                If Ors1.RecordCount > 0 Then
                    oMatrix = oQualityForm.Items.Item("20").Specific
                    Dim op As Integer
                    For op = 0 To Ors1.RecordCount - 1
                        ' objAddOn.SetNewLine(oMatrix, oDBDSLine)
                        Try
                            objAddOn.SetNewLine(oMatrix, oDBDSLine)
                            oMatrix.FlushToDataSource()
                            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.String = oMatrix.VisualRowCount
                            ' oDBDSLine.SetValue("V_-1", op, oMatrix.VisualRowCount)
                            oDBDSLine.SetValue("U_Param", op, Ors1.Fields.Item("U_Param").Value)
                            oDBDSLine.SetValue("U_Min", op, Ors1.Fields.Item("U_Min").Value)
                            oDBDSLine.SetValue("U_Max", op, Ors1.Fields.Item("U_Max").Value)
                            'oMatrix.Columns.Item("Param").Cells.Item(oMatrix.VisualRowCount).Specific.String = Ors1.Fields.Item("U_Param").Value
                            'oMatrix.Columns.Item("Min").Cells.Item(oMatrix.VisualRowCount).Specific.String = Ors1.Fields.Item("U_Min").Value
                            'oMatrix.Columns.Item("Max").Cells.Item(oMatrix.VisualRowCount).Specific.String = Ors1.Fields.Item("U_Max").Value
                            oMatrix.LoadFromDataSourceEx()
                        Catch ex As Exception

                        End Try
                        Ors1.MoveNext()
                    Next

                    'oQualityForm.Items.Item("WoNo").Enabled = False
                    'oQualityForm.Items.Item("Code").Enabled = False
                    ' oQualityForm.Items.Item("Name").Enabled = False
                    'oMatrix = oQualityForm.Items.Item("20").Specific
                    'oMatrix.Columns.Item("Param").Editable = False
                    'oMatrix.Columns.Item("Min").Editable = False
                    'oMatrix.Columns.Item("Max").Editable = False
                    'oMatrix.Columns.Item("Lot").Editable = True
                    'oMatrix.Columns.Item("Actual").Editable = True
                    ' oQualityForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                End If
            End If




        Catch ex As Exception

        End Try
        Return True
        'End Function
    End Function
    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        If oQualityForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            If pVal.ItemUID = "1" Then
                                If Validation() = False Then
                                    'BubbleEvent = False
                                    'Exit Sub
                                End If
                            End If
                        End If
                    Catch ex As Exception
                    End Try
            End Select
        End If

    End Sub

    Public Sub FormDataEvent(ByVal BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If oQualityForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Or oQualityForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        oQualityForm.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                    End If
            End Select
        Catch ex As Exception

        End Try
    End Sub


    Sub Menu_event(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)

    End Sub
    Function Validation()

    End Function
End Class
