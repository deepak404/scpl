﻿Public Class ClsLabourGroupMaster

    Public oForm As SAPbouiCOM.Form
    ' Dim oForm As SAPbouiCOM.Form
    Public Const FormType = "OLGM"
    Public Sub LoadScreen()
        Try
            oForm = objAddOn.objUIXml.LoadScreenXML("LabourGroupMaster.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            oForm.DataBrowser.BrowseBy = "Code"
            ' Me.InitForm()
        Catch ex As Exception
            '   objAddOn.objApplication.StatusBar.SetText("Loadscreen Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
    Public Sub FormDataEvent(ByVal BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Or oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        oForm.Items.Item("Code").Enabled = False
                    End If
            End Select
        Catch ex As Exception

        End Try
    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByVal pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try


        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Item Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        If pVal.MenuUID = "1282" Or pVal.MenuUID = "1281" Then
            'InitForm()
        Else
            oForm.Items.Item("Code").Enabled = False
        End If
    End Sub
    'Sub InitForm()
    '    oForm.Items.Item("t_Code").Enabled = True
    'End Sub
End Class
