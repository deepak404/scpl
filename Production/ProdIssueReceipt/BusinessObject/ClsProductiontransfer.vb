﻿Public Class ClsProductiontransfer
    'Public Const FormType = "PORDER"
    Dim oDBDSHeader, oDBDSLine, oDBDSLine1 As SAPbouiCOM.DBDataSource
    Dim oMatrix, oMatrix1 As SAPbouiCOM.Matrix
    Dim oForm As SAPbouiCOM.Form
    Dim oDataTable As SAPbouiCOM.DataTable
    Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
    Dim DocEntry, str, TotTime, fromDate As String
    Dim ors, ors1 As SAPbobsCOM.Recordset
    Dim Ocom As SAPbouiCOM.ComboBox
    Dim CurrentLineID As Integer

    Sub LoadScreen(ByVal LineID As Integer, ByVal RouteCode As String, ByVal Status As String, ByVal NoUnit As String, ByVal LotNo As String)
        Try
            oForm = objAddOn.objUIXml.LoadScreenXML("ProDuction.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, "PORDER")

            oDBDSHeader = oForm.DataSources.DBDataSources.Item(0)
            oDBDSLine = oForm.DataSources.DBDataSources.Item(2)
            oDBDSLine1 = oForm.DataSources.DBDataSources.Item(1)
            oMatrix = oForm.Items.Item("Matrix").Specific
            oMatrix1 = oForm.Items.Item("Matrix1").Specific
            oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            oForm.DataBrowser.BrowseBy = "DocEntry"

            CurrentLineID = LineID
            oForm.Items.Item("Won").Specific.string = RouteCode
            Ocom = oForm.Items.Item("32").Specific
            oForm.Items.Item("Idle").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            If Status = "C" Then
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                oForm.Items.Item("DocNum").Enabled = True
                ors.DoQuery("select U_PDEntry from [@AS_WORD1] where DocEntry ='" & RouteCode & "' and LineId =" & LineID & "")
                oForm.Items.Item("DocNum").Specific.value = ors.Fields.Item("U_PDEntry").Value
                oForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                oForm.Items.Item("DocNum").Enabled = False
            Else
                Add_Mode()
            End If
            Dim oco As SAPbouiCOM.ComboBox
            oco = oForm.Items.Item("34").Specific
            ors.DoQuery("Select Code,Name from  [@AS_TANK] Order by Code")
            If ors.RecordCount > 0 Then
                Dim i As Integer
                For i = 1 To ors.RecordCount
                    oco.ValidValues.Add(ors.Fields.Item("Code").Value, ors.Fields.Item("Name").Value)
                    ors.MoveNext()
                Next
            End If
            oco = oForm.Items.Item("36").Specific
            If ors.RecordCount > 0 Then
                Dim i As Integer
                ors.MoveFirst()
                For i = 1 To ors.RecordCount
                    oco.ValidValues.Add(ors.Fields.Item("Code").Value, ors.Fields.Item("Name").Value)
                    ors.MoveNext()
                Next
            End If
        Catch ex As Exception
            '    objAddOn.objApplication.SetStatusBarMessage("Form Load " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try


    End Sub

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            If pVal.ItemUID = "1" Then
                                If Validation() = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                ElseIf labour_Cost() = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                            End If
                        End If

                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Click Event " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    If pVal.ItemUID = "Matrix1" And pVal.ColUID = "LCode" Then
                        objAddOn.ChooseFromListFilteration(oForm, "Cfl_lab", "Code", "Select Code From [@AS_OLBR]  where U_lact  ='Y'")
                    End If
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "Idle" Then
                        oForm.PaneLevel = 1
                    End If
                    If pVal.ItemUID = "Labour" Then
                        oForm.PaneLevel = 2
                    End If

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        oForm.Items.Item("DocNum").Enabled = False
                    End If
                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        ' Add_Mode()
                        objAddOn.objApplication.Menus.Item("1289").Activate()
                    End If
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try
                        If oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            If pVal.ItemUID = "Matrix1" And pVal.ColUID = "LCode" Then
                                If oMatrix1.Columns.Item("LCode").Cells.Item(pVal.Row).Specific.String <> "" And pVal.Row = oMatrix1.VisualRowCount Then
                                    objAddOn.SetNewLine(oMatrix1, oDBDSLine1)
                                End If
                            End If
                            If pVal.ItemUID = "Matrix" And pVal.ColUID = "Time" Then
                                If oMatrix.Columns.Item("Time").Cells.Item(pVal.Row).Specific.VALUE <> "0.0" And pVal.Row = oMatrix.VisualRowCount Then
                                    objAddOn.SetNewLine(oMatrix, oDBDSLine)
                                End If
                            End If
                            If pVal.ItemUID = "Won" Then
                                If oForm.Items.Item("Won").Specific.ToString <> "" Then
                                    If oMatrix.VisualRowCount < 1 Then
                                        objAddOn.SetNewLine(oMatrix, oDBDSLine)
                                        objAddOn.SetNewLine(oMatrix1, oDBDSLine1)
                                    End If
                                End If
                            End If
                            If pVal.ItemUID = "22" Then
                                Dim InDate As String = oForm.Items.Item("18").Specific.value
                                Dim OutDate As String = oForm.Items.Item("20").Specific.value
                                Dim InTime As String = oForm.Items.Item("21").Specific.value
                                Dim OutTime As String = oForm.Items.Item("22").Specific.value
                                Dim Diff As String = TimeCalculation(InDate, OutDate, InTime, OutTime)
                                oDBDSHeader.SetValue("U_THour", 0, Diff)
                                ' CalculateTime(oForm.Items.Item("21").Specific.string, oForm.Items.Item("22").Specific.string)

                            End If
                            If pVal.ItemUID = "Matrix1" And pVal.ColUID = "TTime" Then
                                Dim InDate As String = oMatrix1.Columns.Item("FDate").Cells.Item(pVal.Row).Specific.value
                                Dim OutDate As String = oMatrix1.Columns.Item("TDate").Cells.Item(pVal.Row).Specific.value
                                Dim InTime As String = oMatrix1.Columns.Item("FTime").Cells.Item(pVal.Row).Specific.value
                                Dim OutTime As String = oMatrix1.Columns.Item("TTime").Cells.Item(pVal.Row).Specific.value
                                Dim Diff As String = TimeCalculation(InDate, OutDate, InTime, OutTime)

                                oMatrix1.FlushToDataSource()
                                oDBDSLine1.SetValue("U_THour", pVal.Row - 1, Diff)
                                oMatrix1.LoadFromDataSourceEx()
                                oMatrix1.FlushToDataSource()
                                Dim LCost As Double = oMatrix1.Columns.Item("Per").Cells.Item(pVal.Row).Specific.value * oMatrix1.Columns.Item("Hours").Cells.Item(pVal.Row).Specific.value
                                oDBDSLine1.SetValue("U_LCost", pVal.Row - 1, LCost)
                                oMatrix1.LoadFromDataSourceEx()
                            End If
                        End If

                        If pVal.ItemUID = "Matrix1" Then
                            HeaderTotal()
                        End If

                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Lost Focus " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Select Case oCFLE.ChooseFromListUID
                            Case "Cfl_lab"
                                oMatrix1.FlushToDataSource()
                                oDBDSLine1.SetValue("U_LCode", pVal.Row - 1, oDataTable.GetValue("Code", 0))
                                oDBDSLine1.SetValue("U_LName", pVal.Row - 1, oDataTable.GetValue("U_lname", 0))
                                Dim PerHr As String = objAddOn.getSingleValue("Select U_lCost from [@AS_OLBR] where Code = '" & oDataTable.GetValue("Code", 0) & "'")
                                oDBDSLine1.SetValue("U_PerHr", pVal.Row - 1, PerHr)
                                oMatrix1.LoadFromDataSourceEx()
                            Case "Cfl_Wo"
                                DocEntry = oDataTable.GetValue("DocEntry", 0)
                                'oMatrix.FlushToDataSource()
                                str = "Select U_RuleC ,U_RuleN   from [@AS_OWORD] where DocEntry = '" & DocEntry & "'"
                                ors = objAddOn.DoQuery(str)
                                oDBDSHeader.SetValue("U_WONo", 0, oDataTable.GetValue("DocNum", 0))
                                oDBDSHeader.SetValue("U_WODocE", 0, DocEntry)
                                oDBDSHeader.SetValue("U_RuleC", 0, ors.Fields.Item("U_RuleC").Value)
                                oDBDSHeader.SetValue("U_RuleN", 0, ors.Fields.Item("U_RuleN").Value)
                                'oDBDSHeader.SetValue("U_WONa", 0, oDataTable.GetValue("U_RuleN", 0))
                                'oDBDSHeader.SetValue("U_WONa", 0, oDataTable.GetValue("U_RuleN", 0))
                                Process()
                                'oMatrix.LoadFromDataSourceEx()
                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Cfl " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try

            End Select

        End If

    End Sub

#Region "Header Total"
    Public Sub HeaderTotal()
        Dim tot As Double
        Dim i As Integer
        For i = 1 To oMatrix1.VisualRowCount
            If CDbl(oMatrix1.Columns.Item("V_0").Cells.Item(i).Specific.value) > 0 Then
                tot = tot + oMatrix1.GetCellSpecific("V_0", i).value
                oForm.Items.Item("TotCost").Specific.value = tot
            End If
        Next i
        oForm.Refresh()
    End Sub
#End Region

    Function labour_Cost()
        If oMatrix.Columns.Item("Time").Cells.Item(oMatrix.VisualRowCount).Specific.value = "0.0" Then
            oMatrix.DeleteRow(oMatrix.VisualRowCount)
            oDBDSLine.RemoveRecord(oDBDSLine.Size - 1)
            oMatrix.FlushToDataSource()
        End If
        objAddOn.DeleteEmptyRowInFormDataEvent(oMatrix1, "LCode", oDBDSLine1)


        Dim LCOST As Double
        Dim i As Integer

        Try
            If oMatrix1.VisualRowCount > 0 Then
                For i = 1 To oMatrix1.VisualRowCount
                    LCOST = LCOST + oMatrix1.Columns.Item("V_0").Cells.Item(i).Specific.value
                Next
            End If
            '  str = "Update [@AS_WORD1] Set U_Labour = '" & LCOST & "' , U_Status ='C' where DocEntry = '" & DocEntry & "' and LineID = '" & Ocom.Selected.Value & "'"
            str = "update [@AS_WORD1] set U_Labour =" & LCOST & ",U_Status ='C',U_PDEntry =" & oForm.Items.Item("DocEntry").Specific.string & " where DocEntry =" & DocEntry & " and LineId =" & CurrentLineID & ""

            ors.DoQuery(str)
            Return True
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Update Failure " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return False
        End Try
    End Function

    Function Validation()
        Try
            If oForm.Items.Item("Won").Specific.String = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select WorkOrder No", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                Return False
            End If
            'If oForm.Items.Item("WonA").Specific.String = "" Then
            '    objAddOn.objApplication.SetStatusBarMessage("Please Select WorkOrder Name", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            '    Return False
            'End If
            If oForm.Items.Item("DocDate").Specific.String = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select DocDate", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                Return False
            End If
            Ocom = oForm.Items.Item("32").Specific
            If Ocom.Selected Is Nothing Then
                objAddOn.objApplication.SetStatusBarMessage("Please Choose Process", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                Return False
            End If
            Ocom = oForm.Items.Item("34").Specific
            If Ocom.Selected Is Nothing Then
                objAddOn.objApplication.SetStatusBarMessage("Please Choose From Tank", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                Return False
            End If
            Ocom = oForm.Items.Item("36").Specific
            If Ocom.Selected Is Nothing Then
                objAddOn.objApplication.SetStatusBarMessage("Please Choose To Tank", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                Return False
            End If
            Dim InDate As String = oForm.Items.Item("18").Specific.string
            Dim OutDate As String = oForm.Items.Item("20").Specific.string
            Dim InTime As String = oForm.Items.Item("21").Specific.string
            Dim OutTime As String = oForm.Items.Item("22").Specific.string
            If InDate = "" Or OutDate = "" Or InTime = "" Or OutTime = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Check Date & Time", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                Return False
            End If
            If InDate > OutDate Then
                objAddOn.objApplication.SetStatusBarMessage("Please Check Start Date & End Date", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                Return False
            End If
            If oMatrix1.VisualRowCount > 0 Then
                Dim i As Integer
                For i = 1 To oMatrix1.VisualRowCount
                    If oMatrix1.Columns.Item("LCode").Cells.Item(i).Specific.String <> "" Then
                        InDate = oMatrix1.Columns.Item("FDate").Cells.Item(i).Specific.value
                        OutDate = oMatrix1.Columns.Item("TDate").Cells.Item(i).Specific.value
                        InTime = oMatrix1.Columns.Item("FTime").Cells.Item(i).Specific.value
                        OutTime = oMatrix1.Columns.Item("TTime").Cells.Item(i).Specific.value
                        If InDate = "" Or OutDate = "" Or InTime = "" Or OutTime = "" Then
                            objAddOn.objApplication.SetStatusBarMessage("Please Check Date & Time", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                            Return False
                        End If
                        If InDate > OutDate Then
                            objAddOn.objApplication.SetStatusBarMessage("Please Check Start Date & End Date", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                            Return False
                        End If
                    End If
                Next


            End If
            Return True
            'If oMatrix.VisualRowCount > 0 Then
            '    If oMatrix.Columns.Item("ItemCode").Cells.Item(1).Specific.String = "" Then
            '        objAddOn.objApplication.SetStatusBarMessage("Select ItemCode Atleast One", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            '        Return False
            '    End If

            'End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Validate Failed", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return False
        End Try
      

    End Function

    Sub Add_Mode()
        Try
            Dim strDocNum As String = oForm.BusinessObject.GetNextSerialNumber("-1", "AS_OPORD")
            oDBDSHeader.SetValue("DocNum", 0, strDocNum)
            oDBDSHeader.SetValue("DocEntry", 0, strDocNum)
            oForm.Items.Item("DocDate").Specific.string = "s"
            oForm.Items.Item("DocNum").Enabled = False
            oForm.Items.Item("32").Enabled = True

            Try
                'oForm.Items.Item("DocDate").Specific.string = Now.Date
            Catch ex As Exception

            End Try
            'oDBDSHeader.SetValue("U_DDate", 0, Now.Date.ToString)

        Catch ex As Exception

        End Try

        'oForm.Items.Item("DocDate").Specific.string = Now.Date
    End Sub

    Sub Process()
        Dim i As Integer
        Try
            Ocom = oForm.Items.Item("32").Specific
            If Ocom.ValidValues.Count > 0 Then
                For i = 0 To Ocom.ValidValues.Count - 1
                    Ocom.ValidValues.Remove(Ocom.ValidValues.Count - 1, SAPbouiCOM.BoSearchKey.psk_Index)
                Next
            End If
        Catch ex As Exception
        End Try
        Try
            str = "Select U_OpCode,LineId ,U_OpName ,(Select Sum(U_PCyc) from [@AS_WORD1] where DocEntry = a.docentry )'Sum'  from [@AS_WORD1] a where U_Status ='O' and U_Process = 'P' and LineId  not in (Select U_Process from [@AS_OPORD] where U_WODocE = a.DocEntry ) and a.DocEntry = '" & DocEntry & "' and LineId ='" & CurrentLineID & "' "
            ors = objAddOn.DoQuery(str)
            If ors.RecordCount > 0 Then
                For i = 0 To ors.RecordCount - 1
                    Dim Description As String = ors.Fields.Item("LineId").Value.ToString + "_" + ors.Fields.Item("U_OpCode").Value.ToString
                    Ocom.ValidValues.Add(ors.Fields.Item("LineId").Value, Description)
                    oDBDSHeader.SetValue("U_PCyc", 0, ors.Fields.Item("Sum").Value)
                    'oForm.Items.Item("28").Specific.String = ors.Fields.Item("Sum").Value
                    ors.MoveNext()
                Next
            End If
        Catch ex As Exception

        End Try
      

    End Sub

    Sub Menu_event(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        If pVal.MenuUID = "1282" Then
            Add_Mode()
        ElseIf pVal.MenuUID = "1281" Then
            oForm.Items.Item("DocNum").Enabled = True
            oForm.Items.Item("32").Enabled = False
        Else

            oForm.Items.Item("DocNum").Enabled = False
            oForm.Items.Item("32").Enabled = False
        End If
    End Sub

    Function TimeCalculation(ByVal fromDate As String, ByVal ToDate As String, ByVal InTime As String, ByVal OutTime As String) As String
        Try
            Dim minin, minou, hoin, hoou As String
            ' Dim DoubleHour, diff1, diff2, mindiff As String
            minin = Right(InTime, 2)
            hoin = Left(InTime, 2)
            minin = hoin + ":" + minin
            fromDate = fromDate + " " + minin + ":" + "00"
            minou = Right(OutTime, 2)
            hoou = Left(OutTime, 2)
            minou = hoou + ":" + minou
            ToDate = ToDate + " " + minou + ":" + "00"
            ors1 = objAddOn.DoQuery("Select (Select DateDiff (MINUTE,'" & fromDate & "','" & ToDate & "')/60)'Hour',(Select DateDiff " & _
                                    " (MINUTE,'" & fromDate & "','" & ToDate & "')%60)'Min'")
            Dim Hour As String = ors1.Fields.Item("Hour").Value
            Dim Min As String = ors1.Fields.Item("Min").Value
            If Len(Hour) = 1 Then
                Hour = "0" + Hour
            ElseIf Hour = 0 Then
                Hour = "00"
            Else
                'Hour = Hour
            End If
            If Len(Min) = 1 And Min <> "0" Then
                Min = "0" + Min
            ElseIf Min = 0 Then
                Min = "00"
            Else
                'Min = ors.Fields.Item("Min").Value
            End If
            Dim Diff As String = Hour + "." + Min
            Return Diff

        Catch ex As Exception
            'objAddOn.objApplication.SetStatusBarMessage("Time Calculation Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
        Return ""
    End Function

    
End Class
