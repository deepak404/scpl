Public Class clsProductionReceipt
    Dim frmProductionReceipt, frmSubPlanningDetailsSubGrid As SAPbouiCOM.Form
    Public Const FormType = "AS_OIGN"
    Dim oMatrix, oMatrix1 As SAPbouiCOM.Matrix
    Public omatrixRM As SAPbouiCOM.Matrix
    Dim oRS, oRecSet As SAPbobsCOM.Recordset
    Dim TotTime, ITime, OTime, TotMCost, TotLCost As String
    Dim lretcode, ProTyp1Cost, ProTyp2Cost, ProcessCost As Double
    Dim IssueNum As Integer
    Public ProdType, CurrLineID, DocEntry As Integer
    Public WorkOrderOperationType As String
    Public WorkOrderOperationCode As String

    Dim bundleQty As Double = 0
    Dim patchQty As Double = 0
    Dim obQty As Double = 0
    Dim mchnCost As Double = 0
    Dim lbrCost As Double = 0
    Public objRmEntry As clsRMEntry
    Public objBuyPdt As ClsBuyProductDetails
    Public dyncode, WODocEntry As String
    Dim oDBDSBuy, oDBDSHeader, oDBDSShrt, oDBDSRM, oDBDSMac, oDBDSLab, oDBDSIdl, oDBDParameter, oDBDSpindle, oDBDSSubPlanDetails, oDBDSMainPlanDetails As SAPbouiCOM.DBDataSource
    Dim oCombo As ComboBox
    Dim SubRowID_PlanningDetails As Integer
    Private frmOPE2, frmOPE3 As SAPbouiCOM.Form
    Private oDBOPE2, oDBOPE3 As SAPbouiCOM.DBDataSource
    Private oMatOPE2, oMatOPE3, oMatrixLab, oMatrixIdl, oMatrixSD, oMatrixP, oMatSubPlanSetails, oMatMainPlanSetails As SAPbouiCOM.Matrix
    Private Row2, Row3 As Integer
    Dim cntlMat As String = ""
    Public sStandardCycleTime, sSalesOrderNo, sSalesOrderEntry, sCustomerCode As String
    Public DisableWhareHouse As Boolean

    Sub LoadXML()
        Try


            frmProductionReceipt = objAddOn.objUIXml.LoadScreenXML("ProductionReceipt.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            frmProductionReceipt.Freeze(True)
            oDBDSRM = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN1")
            objAddOn.LoadComboBoxSeries(frmProductionReceipt.Items.Item("c_Series").Specific, "AS_OIGN") ' Load the Combo Box Series
            frmProductionReceipt.Items.Item("12").Specific.string = CDate(Today.Date).ToString("yyyyMMdd")
            dyncode = DateTime.Now.ToString("yyMMddmmss")
            oDBDSHeader = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_OIGN")
            oDBDSRM = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN1")
            oDBDSMac = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN2")
            oDBDSLab = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN3")
            oDBDSIdl = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN4")
            oDBDSBuy = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN5")
            oDBDParameter = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN6")
            oDBDSpindle = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN7")



            oDBDSMainPlanDetails = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN8")
            oMatMainPlanSetails = frmProductionReceipt.Items.Item("submatrix").Specific
            oMatrix = frmProductionReceipt.Items.Item("16").Specific
            omatrixRM = frmProductionReceipt.Items.Item("16").Specific
            oMatrixSD = frmProductionReceipt.Items.Item("oMatrixSD").Specific
            oMatrixP = frmProductionReceipt.Items.Item("oMatrixP").Specific


            frmProductionReceipt.Items.Item("Fol_FG").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

            frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
        Catch ex As Exception
            '   objSBOAPI.SBO_Appln.SetStatusBarMessage("Form Load " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Finally
            frmProductionReceipt.Freeze(False)
        End Try

    End Sub
    Public Sub LoadScreen(ByVal type As Integer, ByVal LineID As Integer, ByVal WorkORderDocNum As String, ByVal Status As String, ByVal Cost As Double, ByVal NoUnit As String, ByVal OpCode As String, FGCode As String, FGName As String, sRouteCode As String, sOperationType As String, pStandardCycleTime As String, pSalesOrderNo As String, pSalesOrderEntry As String, pCustomerCode As String, WorkORderDocEntry As String)
        Try
            frmProductionReceipt = objAddOn.objUIXml.LoadScreenXML("ProductionReceipt.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, "AS_OIGN")
            'oForm.Freeze(True)
            oDBDSHeader = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_OIGN")
            oDBDSRM = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN1")
            oDBDSMac = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN2")
            oDBDSLab = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN3")
            oDBDSIdl = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN4")
            oDBDSBuy = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN5")
            oDBDParameter = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN6")
            oDBDSpindle = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN7")


            oDBDSMainPlanDetails = frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN8")
            oMatMainPlanSetails = frmProductionReceipt.Items.Item("submatrix").Specific



            sStandardCycleTime = pStandardCycleTime
            sSalesOrderNo = pSalesOrderNo
            sSalesOrderEntry = pSalesOrderEntry
            sCustomerCode = pCustomerCode
            WODocEntry = WorkORderDocEntry
            oMatrix = frmProductionReceipt.Items.Item("16").Specific
            omatrixRM = frmProductionReceipt.Items.Item("16").Specific
            oMatrixSD = frmProductionReceipt.Items.Item("oMatrixSD").Specific
            oMatrixP = frmProductionReceipt.Items.Item("oMatrixP").Specific

            frmProductionReceipt.EnableMenu("1292", True)
            Try
                ProdType = type
                WorkOrderOperationType = sOperationType
                WorkOrderOperationCode = OpCode

                ProcessCost = Cost
                CurrLineID = LineID
                DocEntry = CInt(WorkORderDocNum)
                objRmEntry = New clsRMEntry
                frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                frmProductionReceipt.Items.Item("10").Specific.value = frmProductionReceipt.BusinessObject.GetNextSerialNumber("-1", "AS_OIGN")
                frmProductionReceipt.Items.Item("12").Specific.string = CDate(Today.Date).ToString("yyyyMMdd")
                dyncode = DateTime.Now.ToString("yyMMddmmss")
                frmProductionReceipt.Items.Item("54").Specific.value = CInt(WorkORderDocNum)
            Catch ex As Exception

            End Try

            Dim Str As String
            Str = "Select  distinct T0.U_RuleC ,T0.U_RuleN ,T1.U_BaseNum  ,T1.U_CardName     from [@AS_OWORD] T0 Inner Join [@AS_WORD2] T1 on T0.DocEntry =T1.DocEntry Where isnull(T1.U_BaseNum,'')!='' AND T0.DocEntry = '" & WODocEntry & "'"
            oRS = objAddOn.DoQuery(Str)

            oDBDSHeader.SetValue("U_WONo", 0, WorkORderDocNum)
            oDBDSHeader.SetValue("U_RuleC", 0, oRS.Fields.Item("U_RuleC").Value)
            oDBDSHeader.SetValue("U_RuleN", 0, oRS.Fields.Item("U_RuleN").Value)
            oDBDSHeader.SetValue("U_WODocE", 0, WODocEntry)
            oDBDSHeader.SetValue("U_SONum", 0, oRS.Fields.Item("U_BaseNum").Value)
            oDBDSHeader.SetValue("U_CustName", 0, oRS.Fields.Item("U_CardName").Value)
            Process()

            frmProductionReceipt.Items.Item("60").Specific.value = pStandardCycleTime.ToString()

            frmProductionReceipt.Items.Item("t_BaseLine").Specific.value = CurrLineID.ToString()

            objAddOn.LoadComboBoxSeries(frmProductionReceipt.Items.Item("c_Series").Specific, "AS_OIGN") '
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount

            oMatrix = frmProductionReceipt.Items.Item("17").Specific
            oMatrix.AddRow() ''Added by Bhavani
            objAddOn.setComboBoxValue(oMatrix.Columns.Item("Shift").Cells.Item(oMatrix.VisualRowCount).Specific, "EXEC [dbo].[@AIS_ProductionReceipt_GetShiftMaster]")


            oMatrix = frmProductionReceipt.Items.Item("52").Specific
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount


            oMatrixSD.AddRow()
            oMatrixSD.Columns.Item("LineId").Cells.Item(oMatrixSD.VisualRowCount).Specific.value = oMatrixSD.VisualRowCount

            oMatrixP.AddRow()
            oMatrixP.Columns.Item("LineId").Cells.Item(oMatrixP.VisualRowCount).Specific.value = oMatrixP.VisualRowCount


            CFLCondition()
            RMDetails(OpCode)
            SpindleDetails(OpCode)
            MachineDetails(OpCode)
            IdleDetails(OpCode)
            LabourDetails(OpCode)


            ByProduct(OpCode, sRouteCode, FGCode, FGName, sOperationType, LineID, WorkORderDocEntry)

            'FormEnable()
            DefineModesForFields()

            frmProductionReceipt.PaneLevel = 1
            frmProductionReceipt.Items.Item("Fol_FG").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

            'oForm.Freeze(False)
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("LoadScreen  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            'oForm.Freeze(False)
        End Try
    End Sub

    Public Sub SpindleDetails(ByVal OpCode As String)
        Try
            Dim i As Integer
            oMatrixSD = frmProductionReceipt.Items.Item("oMatrixSD").Specific
            oMatrixSD.Clear()
            Dim oRSSpindle As SAPbobsCOM.Recordset
            oRSSpindle = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim Squery As String = "SELECT T1.U_ASpindle As AvailSpindle  FROM  [@AS_OPRN] T1 WHERE T1.Code = '" & OpCode & "'"

            oRSSpindle.DoQuery(Squery)
            Dim no = oRSSpindle.RecordCount
            For i = 1 To oRSSpindle.RecordCount
                oMatrixSD.AddRow()
                oMatrixSD.Columns.Item("LineId").Cells.Item(oMatrixSD.VisualRowCount).Specific.value = oMatrixSD.VisualRowCount
                oMatrixSD.Columns.Item("ASpind").Cells.Item(i).Specific.value = oRSSpindle.Fields.Item("AvailSpindle").Value
                oRSSpindle.MoveNext()
            Next
            oMatrixSD.AddRow()
            oMatrixSD.Columns.Item("LineId").Cells.Item(oMatrixSD.VisualRowCount).Specific.value = oMatrixSD.VisualRowCount
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Import Spindle Details Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
    Public Sub DefineModesForFields()
        Try
            frmProductionReceipt.Items.Item("10").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            ' frmIssueToSubAuto.Items.Item("t_DocDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            'c_Series

            frmProductionReceipt.Items.Item("10").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)


        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("DefineModesForFields  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub


    Public Sub LoadForm_RM(ByVal TotQty As Double)
        Try
            frmOPE2 = objAddOn.objUIXml.LoadScreenXML("InsBatchSet.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, "BIN")
            'frmOPE2 = objAddOn.objApplication.Forms.Item(GenInsBINFormId)

            oDBOPE2 = frmOPE2.DataSources.DBDataSources.Item(0)
            oMatOPE2 = frmOPE2.Items.Item("Matrix2").Specific

            If frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then


                frmOPE2.EnableMenu("1293", True)
                frmOPE2.AutoManaged = True
            Else
                frmOPE2.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE

            End If


        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("LoadForm_RM  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
    Public Sub LoadForm_RMBIN(ByVal TotQty As Double)
        Try
            frmOPE3 = objAddOn.objUIXml.LoadScreenXML("RMBin.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, "RMBIN")
            'frmOPE2 = objAddOn.objApplication.Forms.Item(GenInsBINFormId)

            oDBOPE3 = frmOPE3.DataSources.DBDataSources.Item(0)
            oMatOPE3 = frmOPE3.Items.Item("Matrix3").Specific

            If frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then


                frmOPE3.EnableMenu("1293", True)
                frmOPE3.AutoManaged = True
            Else
                frmOPE3.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE

            End If


        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("LoadForm_RMBIN  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub

    Sub RMExportMatrix(ByVal SourceMatrix As SAPbouiCOM.Matrix, _
                   ByRef SourceDBS As SAPbouiCOM.DBDataSource, _
                  ByRef DestDBS As SAPbouiCOM.DBDataSource, _
                  ByVal DestMatrix As SAPbouiCOM.Matrix, _
                  ByVal Parent1 As Integer, Optional ByVal Parent2 As Integer = 0, Optional ByVal TotQty As Double = 0, Optional ByVal batchno As String = "", Optional ByVal docType As String = "")


        DestMatrix.Clear()
        DestDBS.Clear()
        Dim n As Integer = 0

        Try
            frmOPE3.Freeze(True)

            For i As Integer = 0 To SourceMatrix.VisualRowCount - 1
                If Parent2 = 0 Then
                    SourceDBS.Offset = SourceDBS.Size - 1
                    If (CInt(SourceMatrix.GetCellSpecific("Parent1", i + 1).value.ToString.Trim) = Parent1) Then


                        DestMatrix.AddRow()

                        For j As Integer = 0 To SourceDBS.Fields.Count - 1
                            Try
                                Dim Arrvalue() As String = Split(DestDBS.Fields.Item(j).Name, "_")
                                'If (Arrvalue.Count > 1) Then

                                DestMatrix.FlushToDataSource()
                                If (DestDBS.Fields.Item(j).Name = "U_BatchNo") Then
                                    DestDBS.SetValue(DestDBS.Fields.Item(j).Name, n, batchno)
                                ElseIf (DestDBS.Fields.Item(j).Name = "U_TotQty") Then
                                    DestDBS.SetValue(DestDBS.Fields.Item(j).Name, n, TotQty)
                                Else


                                    DestDBS.SetValue(DestDBS.Fields.Item(j).Name, n, SourceMatrix.GetCellSpecific(Arrvalue(1), i + 1).value.ToString.Trim)
                                End If

                                '  DestDBS.SetValue(DestDBS.Fields.Item(j).Name, n, SourceMatrix.GetCellSpecific(Arrvalue(1), i + 1).value.ToString.Trim)
                                '  DestMatrix.Columns.Item(Mid(DestDBS.Fields.Item(j).Name, 3, Len(DestDBS.Fields.Item(j).Name))).Cells.Item(n).Specific.value = SourceMatrix.GetCellSpecific(Arrvalue(1), i + 1).value.ToString.Trim
                                DestMatrix.LoadFromDataSource()

                                ' End If

                            Catch ex As Exception
                                '  DestDBS.SetValue(DestDBS.Fields.Item(j).Name, n, SourceMatrix.GetCellSpecific(DestDBS.Fields.Item(j).Name, i + 1).value.ToString.Trim)
                            End Try

                        Next
                        n = n + 1

                    End If
                Else
                    If CInt(SourceDBS.GetValue("U_Parent1", i)) = Parent1 And CInt(SourceDBS.GetValue("U_Parent2", i)) = Parent2 Then
                        DestMatrix.LoadFromDataSource()
                        DestMatrix.AddRow()
                        DestMatrix.FlushToDataSource()
                        For j As Integer = 0 To SourceDBS.Fields.Count - 1
                            DestDBS.SetValue(DestDBS.Fields.Item(j).Name, n, SourceDBS.GetValue(DestDBS.Fields.Item(j).Name, i).ToString.Trim)
                        Next
                        n = n + 1
                    End If
                End If

            Next



        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("RMExportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)


        Finally
            frmOPE3.Freeze(False)

        End Try


        Try
            If (frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then

                If (oMatOPE3.VisualRowCount = 0) Then

                    objAddOn.Sub_SetNewLine(DestMatrix, DestDBS, Parent1, Parent2, DestMatrix.VisualRowCount)

                    If (oMatOPE3.VisualRowCount <= 1 And oMatOPE3.Columns.Item("ItemCode").Cells.Item(1).Specific.value = "") Then

                        Try

                            frmProductionReceipt.Freeze(True)
                            oMatOPE3.FlushToDataSource()
                            oMatrix = frmProductionReceipt.Items.Item("16").Specific
                            oDBOPE3.SetValue("U_TotQty", 0, TotQty)
                            oDBOPE3.SetValue("U_ItemCode", 0, oMatrix.Columns.Item("V_4").Cells.Item(Row3).Specific.value.ToString.Trim)
                            oDBOPE3.SetValue("U_ItemName", 0, oMatrix.Columns.Item("V_3").Cells.Item(Row3).Specific.value.ToString.Trim)
                            'oDBOPE2.SetValue("U_Parent1", 0, Parent1)
                            'oDBOPE2.SetValue("U_BatchNo", 0, batchno)
                            oDBOPE3.SetValue("U_WhsCode", 0, oMatrix.Columns.Item("V_1").Cells.Item(Row3).Specific.value.ToString.Trim)
                            oDBOPE3.SetValue("U_WhsNam", 0, oMatrix.Columns.Item("WName").Cells.Item(Row3).Specific.value.ToString.Trim)
                            oMatOPE3.LoadFromDataSource()

                        Catch ex As Exception

                        Finally
                            frmProductionReceipt.Freeze(False)

                        End Try

                    End If


                End If
            End If

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("RMExportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
    Sub ExportMatrix(ByVal SourceMatrix As SAPbouiCOM.Matrix, _
                     ByRef SourceDBS As SAPbouiCOM.DBDataSource, _
                    ByRef DestDBS As SAPbouiCOM.DBDataSource, _
                    ByVal DestMatrix As SAPbouiCOM.Matrix, _
                    ByVal Parent1 As Integer, Optional ByVal Parent2 As Integer = 0, Optional ByVal TotQty As Double = 0, Optional ByVal batchno As String = "", Optional ByVal docType As String = "")


        DestMatrix.Clear()
        DestDBS.Clear()
        Dim n As Integer = 0

        Try
            frmOPE2.Freeze(True)

            For i As Integer = 0 To SourceMatrix.VisualRowCount - 1
                If Parent2 = 0 Then
                    SourceDBS.Offset = SourceDBS.Size - 1
                    If (CInt(SourceMatrix.GetCellSpecific("Parent1", i + 1).value.ToString.Trim) = Parent1) Then


                        DestMatrix.AddRow()

                        For j As Integer = 0 To SourceDBS.Fields.Count - 1
                            Try
                                Dim Arrvalue() As String = Split(DestDBS.Fields.Item(j).Name, "_")
                                'If (Arrvalue.Count > 1) Then

                                DestMatrix.FlushToDataSource()
                                If (DestDBS.Fields.Item(j).Name = "U_BatchNo") Then
                                    DestDBS.SetValue(DestDBS.Fields.Item(j).Name, n, batchno)
                                ElseIf (DestDBS.Fields.Item(j).Name = "U_TotQty") Then
                                    DestDBS.SetValue(DestDBS.Fields.Item(j).Name, n, TotQty)
                                Else


                                    DestDBS.SetValue(DestDBS.Fields.Item(j).Name, n, SourceMatrix.GetCellSpecific(Arrvalue(1), i + 1).value.ToString.Trim)
                                End If

                                '  DestDBS.SetValue(DestDBS.Fields.Item(j).Name, n, SourceMatrix.GetCellSpecific(Arrvalue(1), i + 1).value.ToString.Trim)
                                '  DestMatrix.Columns.Item(Mid(DestDBS.Fields.Item(j).Name, 3, Len(DestDBS.Fields.Item(j).Name))).Cells.Item(n).Specific.value = SourceMatrix.GetCellSpecific(Arrvalue(1), i + 1).value.ToString.Trim
                                DestMatrix.LoadFromDataSource()

                                ' End If

                            Catch ex As Exception
                                objAddOn.objApplication.SetStatusBarMessage("RMExportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                            End Try

                        Next
                        n = n + 1

                    End If
                Else
                    If CInt(SourceDBS.GetValue("U_Parent1", i)) = Parent1 And CInt(SourceDBS.GetValue("U_Parent2", i)) = Parent2 Then
                        DestMatrix.LoadFromDataSource()
                        DestMatrix.AddRow()
                        DestMatrix.FlushToDataSource()
                        For j As Integer = 0 To SourceDBS.Fields.Count - 1
                            DestDBS.SetValue(DestDBS.Fields.Item(j).Name, n, SourceDBS.GetValue(DestDBS.Fields.Item(j).Name, i).ToString.Trim)
                        Next
                        n = n + 1
                    End If
                End If

            Next


        Catch ex As Exception

            objAddOn.objApplication.SetStatusBarMessage("RMExportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)

        Finally
            frmOPE2.Freeze(False)

        End Try


        Try

            If (frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then

                If (oMatOPE2.VisualRowCount = 0) Then

                    objAddOn.Sub_SetNewLine(DestMatrix, DestDBS, Parent1, Parent2, DestMatrix.VisualRowCount)

                    If (oMatOPE2.VisualRowCount <= 1 And oMatOPE2.Columns.Item("ItemCode").Cells.Item(1).Specific.value = "") Then

                        Try

                            frmProductionReceipt.Freeze(True)
                            oMatOPE2.FlushToDataSource()
                            oMatrix = frmProductionReceipt.Items.Item("52").Specific
                            oDBOPE2.SetValue("U_TotQty", 0, TotQty)
                            oDBOPE2.SetValue("U_ItemCode", 0, oMatrix.Columns.Item("V_4").Cells.Item(Row2).Specific.value.ToString.Trim)
                            oDBOPE2.SetValue("U_ItemName", 0, oMatrix.Columns.Item("V_3").Cells.Item(Row2).Specific.value.ToString.Trim)
                            'oDBOPE2.SetValue("U_Parent1", 0, Parent1)
                            'oDBOPE2.SetValue("U_BatchNo", 0, batchno)
                            oDBOPE2.SetValue("U_WhsCode", 0, oMatrix.Columns.Item("V_0").Cells.Item(Row2).Specific.value.ToString.Trim)
                            oDBOPE2.SetValue("U_WhsNam", 0, oMatrix.Columns.Item("WName").Cells.Item(Row2).Specific.value.ToString.Trim)
                            oMatOPE2.LoadFromDataSource()

                        Catch ex As Exception

                        Finally
                            frmProductionReceipt.Freeze(False)

                        End Try

                    End If


                End If
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("RMExportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try


    End Sub
    Public Sub ItemEvent_RM(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        If pVal.BeforeAction Then
                            Select Case oCFLE.ChooseFromListUID
                                Case "CFL_Param"
                                    Dim query As String = "select d.U_ParCode ,d.U_ParDesc ,d.U_Spec,m.U_ItemCode,m.U_ItemName   from [@SHJ_QC_OCON] m left outer join [@SHJ_QC_CON1] d on m.Code =d.Code where U_ItemCode ='" & frmProductionReceipt.Items.Item("t_OpeCode").Specific.value.ToString.Trim & "'"
                                    objAddOn.ChooseFromListFilteration(frmOPE2, "CFL_Param", "Code", query)

                                Case "CFL_ITM_RM"


                            End Select
                        End If
                        If Not oDataTable Is Nothing And pVal.BeforeAction = False Then
                            Select Case oCFLE.ChooseFromListUID

                                Case "CFL_Param"

                                    ' oMatrix2.FlushToDataSource()
                                    oMatOPE2.FlushToDataSource()
                                    oDBOPE2.SetValue("U_ParmCode", pVal.Row - 1, oDataTable.GetValue("Code", 0))
                                    oDBOPE2.SetValue("U_ParmName", pVal.Row - 1, oDataTable.GetValue("Name", 0))
                                    Dim value As String = objAddOn.getSingleValue("select d.U_ActVal from [@SHJ_QC_OCON] m left outer join [@SHJ_QC_CON1] d on m.Code =d.Code where U_ItemCode ='" & frmProductionReceipt.Items.Item("t_OpeCode").Specific.value.ToString.Trim & "'")
                                    oDBOPE2.SetValue("U_Specn", pVal.Row - 1, value)
                                    oMatOPE2.LoadFromDataSource()
                                    ' oMatrix2.LoadFromDataSource()
                                Case "CFL_ITM_RM"
                                    oDBOPE2.SetValue("U_ItemCode", pVal.Row - 1, oDataTable.GetValue("ItemCode", 0))
                                    oDBOPE2.SetValue("U_ItemName", pVal.Row - 1, oDataTable.GetValue("ItemName", 0))


                                    oDBOPE2.SetValue("U_OLength", pVal.Row - 1, oDataTable.GetValue("BLength1", 0))

                                    oDBOPE2.SetValue("U_OWidth", pVal.Row - 1, oDataTable.GetValue("BWidth1", 0))

                                    oDBOPE2.SetValue("U_OThick", pVal.Row - 1, oDataTable.GetValue("U_Thickness", 0))


                                    ' Dim value As String = getSingleValue("select d.U_ActVal from [@SHJ_QC_OCON] m left outer join [@SHJ_QC_CON1] d on m.Code =d.Code where U_ItemCode ='" & frmInspection.Items.Item("t_ProCode").Specific.value.ToString.Trim & "' and U_OpeCode ='" & oMax_Ord.GetCellSpecific("OpeCode", 1).value.ToString.Trim & "' and   d.U_ParCode ='" & oDataTable.GetValue("Code", 0) & "'")
                                    ' oDBOPE2.SetValue("U_Specn", pVal.Row - 1, value)

                                    oMatOPE2.SetLineData(pVal.Row)
                                    objAddOn.Sub_SetNewLine(oMatOPE2, oDBOPE2, Row2, 0, pVal.Row, "ItemCode")





                            End Select
                        End If

                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("et_CHOOSE_FROM_LIST  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try



                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try
                        Select Case pVal.ColUID

                            Case "Bin"
                                If (pVal.BeforeAction = False) Then
                                    If oMatOPE2.VisualRowCount = pVal.Row And oMatOPE2.Columns.Item("Bin").Cells.Item(pVal.Row).Specific.value.ToString.Trim <> "" Then

                                        objAddOn.SetNewLine(oMatOPE2, oDBOPE2, pVal.Row, "Bin")

                                        Try
                                            frmProductionReceipt.Freeze(True)
                                            oMatrix = frmProductionReceipt.Items.Item("52").Specific
                                            ' oMatrix1.Columns.Item("ItmCode").Cells.Item(pVal.Row).Click()
                                            oMatOPE2.FlushToDataSource()

                                            'oMatOPE2.Columns.Item("TotQty").Cells.Item(pVal.Row + 1).Specific.value = frmInspection.Items.Item("32").Specific.value.ToString.Trim
                                            'oMatOPE2.Columns.Item("ItemCode").Cells.Item(pVal.Row + 1).Specific.value = frmInspection.Items.Item("24").Specific.value.ToString.Trim
                                            'oMatOPE2.Columns.Item("ItemName").Cells.Item(pVal.Row + 1).Specific.value = frmInspection.Items.Item("26").Specific.value.ToString.Trim
                                            'oMatOPE2.Columns.Item("Parent1").Cells.Item(pVal.Row + 1).Specific.value = oMatOPE2.Columns.Item("Parent1").Cells.Item(pVal.Row).Specific.value
                                            'oMatOPE2.Columns.Item("Whscode").Cells.Item(pVal.Row + 1).Specific.value = "01"

                                            oDBOPE2.SetValue("U_TotQty", oMatOPE2.VisualRowCount - 1, oMatOPE2.Columns.Item("TotQty").Cells.Item(pVal.Row).Specific.value)
                                            '   oDBOPE2.SetValue("U_BatchNo", oMatOPE2.VisualRowCount - 1, oMatOPE2.Columns.Item("BatchNo").Cells.Item(pVal.Row).Specific.value)
                                            ' oDBOPE2.SetValue("U_WhsCode", oMatOPE2.VisualRowCount - 1, oMatOPE2.Columns.Item("Whscode").Cells.Item(pVal.Row).Specific.value)

                                            oDBOPE2.SetValue("U_ItemCode", oMatOPE2.VisualRowCount - 1, oMatrix.Columns.Item("V_4").Cells.Item(Row2).Specific.value.ToString.Trim)
                                            oDBOPE2.SetValue("U_ItemName", oMatOPE2.VisualRowCount - 1, oMatrix.Columns.Item("V_3").Cells.Item(Row2).Specific.value.ToString.Trim)
                                            oDBOPE2.SetValue("U_Parent1", oMatOPE2.VisualRowCount - 1, oMatOPE2.Columns.Item("Parent1").Cells.Item(pVal.Row).Specific.value)
                                            oDBOPE2.SetValue("U_WhsCode", oMatOPE2.VisualRowCount - 1, oMatrix.Columns.Item("V_0").Cells.Item(Row2).Specific.value.ToString.Trim)
                                            oDBOPE2.SetValue("U_WhsNam", oMatOPE2.VisualRowCount - 1, oMatrix.Columns.Item("WName").Cells.Item(Row2).Specific.value.ToString.Trim)
                                            oMatOPE2.LoadFromDataSource()
                                        Catch ex As Exception

                                        Finally

                                            frmProductionReceipt.Freeze(False)

                                        End Try
                                    End If

                                End If
                            Case "ItemCode"
                                If pVal.BeforeAction = False And oMatOPE2.GetCellSpecific("ItemCode", pVal.Row).Value.ToString.Trim = "" Then
                                    oMatOPE2.GetCellSpecific("ItemName", pVal.Row).Value = ""
                                End If

                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("et_LOST_FOCUS  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try

                        If pVal.BeforeAction = True And pVal.ItemUID = "b_OK" Then

                            Dim TotQty As Double = 0
                            Dim qty As Double = 0
                            TotQty = oMatOPE2.Columns.Item("TotQty").Cells.Item(1).Specific.value
                            For rcount As Integer = 0 To oMatOPE2.VisualRowCount - 1
                                If (oMatOPE2.Columns.Item("Bin").Cells.Item(rcount + 1).Specific.value = "") And (oMatOPE2.Columns.Item("Qty").Cells.Item(rcount + 1).Specific.value > 0) Then
                                    objAddOn.Msg("Bin Should not be Empty...")
                                    BubbleEvent = False
                                    Exit Sub
                                ElseIf (oMatOPE2.Columns.Item("Bin").Cells.Item(rcount + 1).Specific.value <> "") Then
                                    qty = qty + IIf(oMatOPE2.Columns.Item("Qty").Cells.Item(rcount + 1).Specific.value = "", 0, oMatOPE2.Columns.Item("Qty").Cells.Item(rcount + 1).Specific.value)
                                End If
                            Next
                            If (TotQty = 0) Then
                                objAddOn.Msg("Total Qty should not allow zero...")
                                BubbleEvent = False
                                Exit Sub
                            End If

                            If (TotQty <> qty) Then
                                objAddOn.Msg("Total Qty and Sum of Qty should be equal")
                                BubbleEvent = False
                                Exit Sub

                            End If

                        End If

                        If pVal.BeforeAction = False And pVal.ItemUID = "b_OK" And pVal.ActionSuccess Then

                            'Dim TotQty As Double = 0
                            'Dim qty As Double = 0
                            'TotQty = oMatOPE2.Columns.Item("TotQty").Cells.Item(1).Specific.value
                            'For rcount As Integer = 0 To oMatOPE2.VisualRowCount - 1
                            '    If (oMatOPE2.Columns.Item("Bin").Cells.Item(rcount + 1).Specific.value = "") Then
                            '        qty = qty + IIf(oMatOPE2.Columns.Item("TotQty").Cells.Item(rcount + 1).Specific.value = "", 0, oMatOPE2.Columns.Item("TotQty").Cells.Item(rcount + 1).Specific.value)
                            '    End If
                            'Next

                            'If (TotQty <> qty) Then
                            '    objAddOn.objApplication.StatusBar.SetText("Total Qty and Sum of Qty should be equal")
                            '    BubbleEvent = False

                            'End If



                            'DeleteExsitMatrix(omatrix2, Row2)
                            'For lcount As Integer = 0 To oMatOPE2.VisualRowCount - 1
                            '    If (oMatOPE2.Columns.Item("Bin").Cells.Item(lcount + 1).Specific.value = "") Then
                            '        oMatOPE2.DeleteRow(oMatOPE2.VisualRowCount)
                            '    End If

                            'Next
                            'oMatOPE2.FlushToDataSource()
                            'ImportMatrix(oMatOPE2, oDBOPE2, omatrix2)
                            'frmOPE2.Close()
                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("et_ITEM_PRESSED  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try
            End Select
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("iTEM eVENT RM  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
    Sub ImportMatrix(ByVal SourceMatrix As SAPbouiCOM.Matrix, _
                     ByRef SourceDBS As SAPbouiCOM.DBDataSource, _
                    ByVal DestMatrix As SAPbouiCOM.Matrix)
        Try


            Dim n As Integer = 0
            Dim Dest_n As Integer = DestMatrix.VisualRowCount + 1
            Dim oEdit As SAPbouiCOM.EditText
            Dim oCmb As SAPbouiCOM.ComboBox
            ' SourceMatrix.FlushToDataSource()
            For i As Integer = n To (n + SourceMatrix.VisualRowCount) - 1
                DestMatrix.AddRow()
                For j As Integer = 0 To DestMatrix.Columns.Count - 1
                    If DestMatrix.Columns.Item(j).Type.ToString = "it_EDIT" Or DestMatrix.Columns.Item(j).Type.ToString = "it_LINKED_BUTTON" Then
                        oEdit = DestMatrix.GetCellSpecific(DestMatrix.Columns.Item(j).UniqueID, Dest_n)
                        Dim ss = DestMatrix.Columns.Item(j).DataBind.Alias
                        oEdit.Value = SourceDBS.GetValue(DestMatrix.Columns.Item(j).DataBind.Alias, n)
                    End If
                    If DestMatrix.Columns.Item(j).Type.ToString = "it_COMBO_BOX" Then
                        oCmb = DestMatrix.GetCellSpecific(DestMatrix.Columns.Item(j).UniqueID, Dest_n)
                        Dim ss = DestMatrix.Columns.Item(j).DataBind.Alias
                        oCmb.Select(SourceDBS.GetValue(DestMatrix.Columns.Item(j).DataBind.Alias, n), SAPbouiCOM.BoSearchKey.psk_ByValue)
                    End If
                Next
                Dest_n = Dest_n + 1
                n = n + 1
            Next
            For i As Integer = 1 To DestMatrix.VisualRowCount
                oEdit = DestMatrix.GetCellSpecific("LineId", i)
                oEdit.Value = i
            Next
            DestMatrix.FlushToDataSource()
            If frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then _
               frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("ImportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
    Public Sub ItemEvent_RMBIN(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        If pVal.BeforeAction Then
                            Select Case oCFLE.ChooseFromListUID
                                Case "CFL_Param"
                                    Dim query As String = "select d.U_ParCode ,d.U_ParDesc ,d.U_Spec,m.U_ItemCode,m.U_ItemName   from [@SHJ_QC_OCON] m left outer join [@SHJ_QC_CON1] d on m.Code =d.Code where U_ItemCode ='" & frmProductionReceipt.Items.Item("t_OpeCode").Specific.value.ToString.Trim & "'"
                                    objAddOn.ChooseFromListFilteration(frmOPE3, "CFL_Param", "Code", query)

                                Case "CFL_ITM_RM"


                            End Select
                        End If
                        If Not oDataTable Is Nothing And pVal.BeforeAction = False Then
                            Select Case oCFLE.ChooseFromListUID

                                Case "CFL_Param"

                                    ' oMatrix2.FlushToDataSource()
                                    oMatOPE3.FlushToDataSource()
                                    oDBOPE3.SetValue("U_ParmCode", pVal.Row - 1, oDataTable.GetValue("Code", 0))
                                    oDBOPE3.SetValue("U_ParmName", pVal.Row - 1, oDataTable.GetValue("Name", 0))
                                    Dim value As String = objAddOn.getSingleValue("select d.U_ActVal from [@SHJ_QC_OCON] m left outer join [@SHJ_QC_CON1] d on m.Code =d.Code where U_ItemCode ='" & frmProductionReceipt.Items.Item("t_OpeCode").Specific.value.ToString.Trim & "'")
                                    oDBOPE3.SetValue("U_Specn", pVal.Row - 1, value)
                                    oMatOPE3.LoadFromDataSource()
                                    ' oMatrix2.LoadFromDataSource()
                                Case "CFL_ITM_RM"
                                    oDBOPE3.SetValue("U_ItemCode", pVal.Row - 1, oDataTable.GetValue("ItemCode", 0))
                                    oDBOPE3.SetValue("U_ItemName", pVal.Row - 1, oDataTable.GetValue("ItemName", 0))


                                    oDBOPE3.SetValue("U_OLength", pVal.Row - 1, oDataTable.GetValue("BLength1", 0))

                                    oDBOPE3.SetValue("U_OWidth", pVal.Row - 1, oDataTable.GetValue("BWidth1", 0))

                                    oDBOPE3.SetValue("U_OThick", pVal.Row - 1, oDataTable.GetValue("U_Thickness", 0))


                                    ' Dim value As String = getSingleValue("select d.U_ActVal from [@SHJ_QC_OCON] m left outer join [@SHJ_QC_CON1] d on m.Code =d.Code where U_ItemCode ='" & frmInspection.Items.Item("t_ProCode").Specific.value.ToString.Trim & "' and U_OpeCode ='" & oMax_Ord.GetCellSpecific("OpeCode", 1).value.ToString.Trim & "' and   d.U_ParCode ='" & oDataTable.GetValue("Code", 0) & "'")
                                    ' oDBOPE2.SetValue("U_Specn", pVal.Row - 1, value)

                                    oMatOPE3.SetLineData(pVal.Row)
                                    objAddOn.Sub_SetNewLine(oMatOPE3, oDBOPE3, Row3, 0, pVal.Row, "ItemCode")





                            End Select
                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("ImportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)

                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try
                        Select Case pVal.ColUID
                            Case "Qty"


                                Dim quantity As Double = 0
                                Dim BalQty As Double = 0
                                For count As Integer = 1 To oMatOPE3.VisualRowCount
                                    quantity = quantity + IIf(oMatOPE3.Columns.Item("Qty").Cells.Item(count).Specific.value = "", 0, oMatOPE3.Columns.Item("Qty").Cells.Item(count).Specific.value)
                                Next

                                BalQty = (oMatOPE3.Columns.Item("TotQty").Cells.Item(pVal.Row).Specific.value) - quantity
                                oMatOPE3.Columns.Item("BatchNo").Cells.Item(pVal.Row).Specific.value = BalQty

                            Case "Bin"
                                If (pVal.BeforeAction = False) Then

                                    Dim WhsCode = oMatrix.Columns.Item("V_1").Cells.Item(Row3).Specific.value.ToString.Trim
                                    Dim ItemCode = oMatrix.Columns.Item("V_4").Cells.Item(Row3).Specific.value.ToString.Trim
                                    Dim bin = oMatOPE3.Columns.Item("Bin").Cells.Item(pVal.Row).Specific.value.ToString.Trim
                                    Dim BinQty As String = objAddOn.getSingleValue("select OnHandQty from oibq where whscode='" & WhsCode & "' and ItemCode='" & ItemCode & "' and BinAbs='" & bin & "'")
                                    oMatOPE3.Columns.Item("BQty").Cells.Item(pVal.Row).Specific.value = BinQty


                                    If oMatOPE3.VisualRowCount = pVal.Row And oMatOPE3.Columns.Item("Bin").Cells.Item(pVal.Row).Specific.value.ToString.Trim <> "" Then


                                        objAddOn.SetNewLine(oMatOPE3, oDBOPE3, pVal.Row, "Bin")

                                        Try
                                            frmProductionReceipt.Freeze(True)
                                            oMatrix = frmProductionReceipt.Items.Item("16").Specific
                                            ' oMatrix1.Columns.Item("ItmCode").Cells.Item(pVal.Row).Click()
                                            oMatOPE3.FlushToDataSource()

                                            'oMatOPE2.Columns.Item("TotQty").Cells.Item(pVal.Row + 1).Specific.value = frmInspection.Items.Item("32").Specific.value.ToString.Trim
                                            'oMatOPE2.Columns.Item("ItemCode").Cells.Item(pVal.Row + 1).Specific.value = frmInspection.Items.Item("24").Specific.value.ToString.Trim
                                            'oMatOPE2.Columns.Item("ItemName").Cells.Item(pVal.Row + 1).Specific.value = frmInspection.Items.Item("26").Specific.value.ToString.Trim
                                            'oMatOPE2.Columns.Item("Parent1").Cells.Item(pVal.Row + 1).Specific.value = oMatOPE2.Columns.Item("Parent1").Cells.Item(pVal.Row).Specific.value
                                            'oMatOPE2.Columns.Item("Whscode").Cells.Item(pVal.Row + 1).Specific.value = "01"

                                            oDBOPE3.SetValue("U_TotQty", oMatOPE3.VisualRowCount - 1, oMatOPE3.Columns.Item("TotQty").Cells.Item(pVal.Row).Specific.value)
                                            '   oDBOPE2.SetValue("U_BatchNo", oMatOPE2.VisualRowCount - 1, oMatOPE2.Columns.Item("BatchNo").Cells.Item(pVal.Row).Specific.value)
                                            ' oDBOPE2.SetValue("U_WhsCode", oMatOPE2.VisualRowCount - 1, oMatOPE2.Columns.Item("Whscode").Cells.Item(pVal.Row).Specific.value)


                                            oDBOPE3.SetValue("U_ItemCode", oMatOPE3.VisualRowCount - 1, oMatrix.Columns.Item("V_4").Cells.Item(Row3).Specific.value.ToString.Trim)
                                            oDBOPE3.SetValue("U_ItemName", oMatOPE3.VisualRowCount - 1, oMatrix.Columns.Item("V_3").Cells.Item(Row3).Specific.value.ToString.Trim)
                                            oDBOPE3.SetValue("U_Parent1", oMatOPE3.VisualRowCount - 1, oMatOPE3.Columns.Item("Parent1").Cells.Item(pVal.Row).Specific.value)
                                            oDBOPE3.SetValue("U_WhsCode", oMatOPE3.VisualRowCount - 1, oMatrix.Columns.Item("V_1").Cells.Item(Row3).Specific.value.ToString.Trim)
                                            oDBOPE3.SetValue("U_WhsNam", oMatOPE3.VisualRowCount - 1, oMatrix.Columns.Item("WName").Cells.Item(Row3).Specific.value.ToString.Trim)

                                            oDBOPE3.SetValue("U_Qty", oMatOPE3.VisualRowCount - 1, oMatOPE3.Columns.Item("BatchNo").Cells.Item(pVal.Row).Specific.value)
                                            oMatOPE3.LoadFromDataSource()
                                        Catch ex As Exception

                                        Finally

                                            frmProductionReceipt.Freeze(False)

                                        End Try
                                    End If

                                End If
                            Case "ItemCode"
                                If pVal.BeforeAction = False And oMatOPE3.GetCellSpecific("ItemCode", pVal.Row).Value.ToString.Trim = "" Then
                                    oMatOPE3.GetCellSpecific("ItemName", pVal.Row).Value = ""
                                End If

                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("ImportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try

                        If pVal.BeforeAction = True And pVal.ItemUID = "b_OK" Then

                            Dim TotQty As Double = 0
                            Dim qty As Double = 0
                            TotQty = oMatOPE3.Columns.Item("TotQty").Cells.Item(1).Specific.value
                            For rcount As Integer = 0 To oMatOPE3.VisualRowCount - 1
                                If (oMatOPE3.Columns.Item("Bin").Cells.Item(rcount + 1).Specific.value = "") And (oMatOPE3.Columns.Item("Qty").Cells.Item(rcount + 1).Specific.value > 0) Then
                                    objAddOn.Msg("Bin Should not be Empty...")
                                    BubbleEvent = False
                                    Exit Sub
                                ElseIf (oMatOPE3.Columns.Item("Bin").Cells.Item(rcount + 1).Specific.value <> "") Then
                                    qty = qty + IIf(oMatOPE3.Columns.Item("Qty").Cells.Item(rcount + 1).Specific.value = "", 0, oMatOPE3.Columns.Item("Qty").Cells.Item(rcount + 1).Specific.value)
                                End If
                            Next
                            If (TotQty = 0) Then
                                objAddOn.Msg("Total Qty should not allow zero...")
                                BubbleEvent = False
                                Exit Sub
                            End If

                            If (TotQty <> qty) Then
                                objAddOn.Msg("Total Qty and Sum of Qty should be equal")
                                BubbleEvent = False
                                Exit Sub

                            End If

                        End If

                        If pVal.BeforeAction = False And pVal.ItemUID = "b_OK" And pVal.ActionSuccess Then

                            'Dim TotQty As Double = 0
                            'Dim qty As Double = 0
                            'TotQty = oMatOPE2.Columns.Item("TotQty").Cells.Item(1).Specific.value
                            'For rcount As Integer = 0 To oMatOPE2.VisualRowCount - 1
                            '    If (oMatOPE2.Columns.Item("Bin").Cells.Item(rcount + 1).Specific.value = "") Then
                            '        qty = qty + IIf(oMatOPE2.Columns.Item("TotQty").Cells.Item(rcount + 1).Specific.value = "", 0, oMatOPE2.Columns.Item("TotQty").Cells.Item(rcount + 1).Specific.value)
                            '    End If
                            'Next

                            'If (TotQty <> qty) Then
                            '    objAddOn.objApplication.StatusBar.SetText("Total Qty and Sum of Qty should be equal")
                            '    BubbleEvent = False

                            'End If




                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("ImportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try
            End Select
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("ImportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub

    Sub DeleteExsitMatrix(ByRef SourceMatrix As SAPbouiCOM.Matrix, ByVal Parent1 As Integer, Optional ByVal Parent2 As Integer = 0)
        Try
            Dim n As Integer = 1
            For i As Integer = 1 To SourceMatrix.VisualRowCount
                If Parent2 = 0 Then
                    If (SourceMatrix.GetCellSpecific("Parent1", n).Value <> "") Then
                        If CInt(SourceMatrix.GetCellSpecific("Parent1", n).Value) = Parent1 Then
                            SourceMatrix.DeleteRow(n)
                        Else
                            n = n + 1
                        End If
                    End If

                Else
                    If CInt(SourceMatrix.GetCellSpecific("Parent1", n).Value) = Parent1 And _
                    CInt(SourceMatrix.GetCellSpecific("Parent2", n).Value) = Parent2 Then
                        SourceMatrix.DeleteRow(n)
                    Else
                        n = n + 1
                    End If
                End If
            Next
            For i As Integer = 1 To SourceMatrix.VisualRowCount
                SourceMatrix.Columns.Item("LineId").Cells.Item(SourceMatrix.VisualRowCount).Specific.Value = i
            Next
            SourceMatrix.FlushToDataSource()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("ImportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub

    Public Sub BIN_MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"



                Case "1293"  ' Delete Row
                    Try
                        frmOPE2.Freeze(True)
                        If pVal.BeforeAction = False Then
                            If cntlMat = "Matrix2" Then
                                oMatOPE2.FlushToDataSource()

                                objAddOn.DeleteEmptyRowInFormDataEvent(oMatOPE2, "ItemCode", oDBOPE2)
                                objAddOn.SetNewLine(oMatOPE2, oDBOPE2, oMatOPE2.VisualRowCount, "ItemCode")
                                'Rearrage the line nos
                                For i As Integer = 1 To oMatOPE2.VisualRowCount
                                    ' oDBDSDetail1.SetValue("LineID", i - 1, i)
                                    oMatOPE2.Columns.Item("LineId").Cells.Item(i).Specific.value = i
                                Next
                                'oMatrix1.LoadFromDataSource()


                            End If







                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.MessageBox(ex.Message)
                    Finally
                        frmOPE2.Freeze(False)
                    End Try

                    Exit Select


            End Select
        Catch ex As Exception
            ' LogInfo("MenuEvent :" + ex.Message, "E")
        End Try
    End Sub
    Public Sub MachineDetails(ByVal OpCode As String)
        Try
            Dim i As Integer
            oMatrix = frmProductionReceipt.Items.Item("17").Specific
            oMatrix.Clear()
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery(" SELECT U_mcode, U_mname FROM [@AS_PRN2] WHERE Code = '" & OpCode & "'")
            oRS.MoveFirst()
            For i = 1 To oRS.RecordCount
                If oRS.Fields.Item("U_mcode").Value.ToString() <> String.Empty Then
                    oMatrix.AddRow()
                    oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
                    oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value = oRS.Fields.Item("U_mcode").Value
                    oMatrix.Columns.Item("V_3").Cells.Item(i).Specific.value = oRS.Fields.Item("U_mname").Value

                    LoadMachineParameterName(oRS.Fields.Item("U_mcode").Value.ToString().Trim())
                End If

                oRS.MoveNext()
            Next
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount

        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Load Machine Details Failed: " & ex.Message & "...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

        End Try

    End Sub
    Public Sub IdleDetails(ByVal OpCode As String)
        Try
            Dim i As Integer
            oMatrix = frmProductionReceipt.Items.Item("28").Specific
            oMatrix.Clear()
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery(" SELECT U_mcode, U_mname FROM [@AS_PRN2] WHERE Code = '" & OpCode & "'")
            oRS.MoveFirst()
            For i = 1 To oRS.RecordCount
                If oRS.Fields.Item("U_mcode").Value.ToString() <> String.Empty Then
                    oMatrix.AddRow()
                    oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
                    Dim MCode As String = oRS.Fields.Item("U_mcode").Value
                    oMatrix.Columns.Item("V_2").Cells.Item(i).Specific.value = MCode



                End If

                oRS.MoveNext()
            Next
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount

        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Load Idle Details Failed: " & ex.Message & "...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

        End Try

    End Sub
    Public Sub LabourDetails(ByVal OpCode As String)
        Try
            Dim i As Integer
            oMatrixLab = frmProductionReceipt.Items.Item("18").Specific
            oMatrixLab.Clear()
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery(" SELECT U_LabourCode, U_LabourName FROM [@AS_PRN1] WHERE Code = '" & OpCode & "'")
            oRS.MoveFirst()
            For i = 1 To oRS.RecordCount
                If oRS.Fields.Item("U_LabourCode").Value.ToString() <> String.Empty Then
                    oMatrixLab.AddRow()
                    oMatrixLab.Columns.Item("V_-1").Cells.Item(oMatrixLab.VisualRowCount).Specific.value = oMatrixLab.VisualRowCount
                    oMatrixLab.Columns.Item("V_4").Cells.Item(i).Specific.value = oRS.Fields.Item("U_LabourCode").Value
                    oMatrixLab.Columns.Item("V_3").Cells.Item(i).Specific.value = oRS.Fields.Item("U_LabourName").Value

                End If

                oRS.MoveNext()
            Next
            oMatrixLab.AddRow()
            oMatrixLab.Columns.Item("V_-1").Cells.Item(oMatrixLab.VisualRowCount).Specific.value = oMatrixLab.VisualRowCount

        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Load Machine Details Failed: " & ex.Message & "...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

        End Try

    End Sub
    Public Sub Labour()
        Try
            oMatrixLab = frmProductionReceipt.Items.Item("18").Specific
            oMatrixLab.Clear()
            oMatrixLab.AddRow()
            oMatrixLab.Columns.Item("V_-1").Cells.Item(oMatrixLab.VisualRowCount).Specific.value = oMatrixLab.VisualRowCount
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("ImportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)

        End Try
    End Sub

    Public Sub RMDetails(ByVal OpCode As String)
        Try
            Dim i As Integer
            oMatrix = frmProductionReceipt.Items.Item("16").Specific
            oMatrix.Clear()
            Dim oRS As SAPbobsCOM.Recordset
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim Squery As String = " SELECT U_ItmCde,T2.ItemName as  U_Desc,  U_Qty,U_Whs,U_WName FROM [@AS_PRN3] T0 Inner Join OITM T2 On T2.ItemCode =T0.U_ItmCde WHERE Code = '" & OpCode & "'"

            oRS.DoQuery(Squery)
            Dim no = oRS.RecordCount
            For i = 1 To oRS.RecordCount

                If oRS.Fields.Item("U_ItmCde").Value <> "" Then
                    oMatrix.AddRow()
                    oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
                    oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value = oRS.Fields.Item("U_ItmCde").Value
                    oMatrix.Columns.Item("V_3").Cells.Item(i).Specific.value = oRS.Fields.Item("U_Desc").Value
                    oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.value = oRS.Fields.Item("U_Whs").Value
                    oMatrix.Columns.Item("WName").Cells.Item(i).Specific.value = oRS.Fields.Item("U_WName").Value
                    Dim perbag = objAddOn.getSingleValue("select isnull(InvntryUom,'')'KGs' from oitm where ItemCode='" & oRS.Fields.Item("U_ItmCde").Value & "' ")
                    oMatrix.Columns.Item("V_5").Cells.Item(i).Specific.value = perbag.ToString()
                    Dim stk = objAddOn.getSingleValue("select OnHand from OITW where ItemCode='" & oRS.Fields.Item("U_ItmCde").Value & "' and WhsCode='" & oRS.Fields.Item("U_Whs").Value & "' ")
                    oMatrix.Columns.Item("V_6").Cells.Item(i).Specific.value = stk
                    oMatrix.Columns.Item("V_2").Cells.Item(i).Specific.value = oRS.Fields.Item("U_Qty").Value
                    oMatrix.Columns.Item("V_8").Cells.Item(i).Specific.value = "Link"
                    'oMatrix.Columns.Item("V_5").Cells.Item(i).Specific.value = objAddOn.getSingleValue("select InvntryUom from oitm where ItemCode= '" & oRS.Fields.Item("U_ItmCde").Value & "' ")
                End If

                oRS.MoveNext()
            Next
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("ImportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        'Try
        '    Select Case BusinessObjectInfo.EventType

        '        Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
        '            Try
        '                If BusinessObjectInfo.BeforeAction = True Then


        '                Else
        '                    If (BusinessObjectInfo.ActionSuccess) Then
        '                        Try
        '                            Dim WOLineId = oForm.Items.Item("62").Specific.value
        '                            Dim GI = objAddOn.getSingleValue("select Max(docentry) from OIGE")
        '                            Dim GR = objAddOn.getSingleValue("select Max(docentry) from OIGN")
        '                            Dim GIdocno = objAddOn.getSingleValue("select DocNum from OIGE where DocEntry='" & GI & "'")
        '                            Dim GRdocno = objAddOn.getSingleValue("select DocNum from OIGN where DocEntry='" & GR & "'")
        '                            Dim PReceiptEntry = objAddOn.getSingleValue("select max(docentry) from [@AS_OIGN]")
        '                            objAddOn.objWorkOrder.Matrix1.Columns.Item("V_6").Cells.Item(CInt(WOLineId)).Specific.value = GIdocno
        '                            objAddOn.objWorkOrder.Matrix1.Columns.Item("V_7").Cells.Item(CInt(WOLineId)).Specific.value = GRdocno
        '                            objAddOn.objWorkOrder.Matrix1.Columns.Item("Prod").Cells.Item(CInt(WOLineId)).Specific.value = PReceiptEntry
        '                            objAddOn.objWorkOrder.Matrix1.Columns.Item("Status").Cells.Item(CInt(WOLineId)).Specific.value = "C"
        '                        Catch ex As Exception
        '                            objAddOn.objApplication.SetStatusBarMessage("ImportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        '                        End Try

        '                    End If
        '                End If


        '                If BusinessObjectInfo.ActionSuccess = True And oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
        '                    Try
        '                        If BusinessObjectInfo.ActionSuccess = True And oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then

        '                            Dim GI = objAddOn.getSingleValue("select Max(docentry) from OIGE")
        '                            Dim GR = objAddOn.getSingleValue("select Max(docentry) from OIGN")
        '                            Dim PReceiptEntry = objAddOn.getSingleValue("select max(docentry) from [@AS_OIGN]")

        '                            Dim GIdocno = objAddOn.getSingleValue("select DocNum from OIGE where DocEntry='" & GI & "'")
        '                            Dim GRdocno = objAddOn.getSingleValue("select DocNum from OIGN where DocEntry='" & GR & "'")
        '                            objAddOn.DoQuery("update [@AS_WORD1] set  U_Issue='" & GIdocno & "', U_Recpt='" & GRdocno & "',U_Prod='" & PReceiptEntry & "',U_Status='C'  where DocEntry =" & DocEntry & " and LineId =" & CurrLineID & "")
        '                        End If
        '                    Catch ex As Exception
        '                        objAddOn.objApplication.SetStatusBarMessage("ImportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        '                    End Try

        '                End If



        '            Catch ex As Exception
        '                objAddOn.objApplication.StatusBar.SetText("FormDataEvent Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        '                'If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
        '                'BubbleEvent = False
        '                'Exit Sub
        '            Finally
        '            End Try

        '        Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD


        '    End Select
        'Catch ex As Exception
        '    objAddOn.objApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        'Finally
        'End Try
    End Sub
    Public Sub FormDataEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        ''Public Sub FormDataEvent(ByVal BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        'Try
        '    oForm = objAddOn.objApplication.Forms.Item(FormUID)
        '    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Or oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
        '        oForm.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
        '    End If
        '    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
        '        oForm.Items.Item("10").Editable = True
        '    End If
        'Catch ex As Exception
        '    objAddOn.objApplication.SetStatusBarMessage("ImportMatrix  Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        'End Try
        'Select Case pVal.EventType
        '    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD

        'End Select

    End Sub


    Public Function FormExist(ByVal FormID As String) As Boolean
        FormExist = False
        For Each uid As SAPbouiCOM.Form In objAddOn.objApplication.Forms
            If uid.UniqueID = FormID Then
                FormExist = True
                Exit For
            End If
        Next
        If FormExist Then
            objAddOn.objApplication.Forms.Item(FormID).Visible = True
            objAddOn.objApplication.Forms.Item(FormID).Select()
        End If
    End Function

#Region "       ... Sub Grid Plan Details ...        "
    Sub LoadSubGridDetails(ByVal Row As Integer)
        Try
            If FormExist(PlanningDetailsFormID) = False Then
                'frmSubPlanningDetailsSubGrid = objAddOn.objUIXml.LoadScreenXML(PlanningDetailsXML, Ananthi.SBOLib.UIXML.enuResourceType.Embeded, FormType)
                objAddOn.objUIXml.AddXML(PlanningDetailsXML)
                objAddOn.objApplication.Forms.Item(PlanningDetailsFormID).Select()
            End If
            frmSubPlanningDetailsSubGrid = objAddOn.objApplication.Forms.Item(PlanningDetailsFormID)
            oDBDSSubPlanDetails = frmSubPlanningDetailsSubGrid.DataSources.DBDataSources.Item("@AS_IGN8")
            oMatSubPlanSetails = frmSubPlanningDetailsSubGrid.Items.Item("submatrix").Specific
            '   oMatrix.FlushToDataSource()
            'Load Sub Grid

            Dim x2 As Integer = oDBDSMainPlanDetails.Size

            frmSubPlanningDetailsSubGrid.Freeze(True)

            LoadSubGrid(oMatSubPlanSetails, oDBDSSubPlanDetails, oDBDSMainPlanDetails, Row)

            Dim boolDataAvl As Boolean = False
            oMatSubPlanSetails.FlushToDataSource()
            'For i As Integer = 0 To oMatSubPlanSetails.VisualRowCount - 1
            '    Dim x As String = Trim(oDBDSSubPlanDetails.GetValue("U_ShiftCode", i))
            '    If Trim(oDBDSSubPlanDetails.GetValue("U_ShiftCode", i)).Equals("") = False Then
            '        boolDataAvl = True
            '        Exit For
            '    End If
            'Next
            '  Load the details...

            If boolDataAvl = False Then
                'oMatSubPlanSetails.Clear()
                'oDBDSSubPlanDetails.Clear()
                'SetNewLineSubGrid(SubRowID_PlanningDetails, oMatSubPlanSetails, oDBDSSubPlanDetails, Row)
                oMatSubPlanSetails.LoadFromDataSource()
            End If

            boolModelForm = True
            boolModelFormID = PlanningDetailsFormID
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Failed to Load  Sub Grid" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmSubPlanningDetailsSubGrid.Freeze(False)

        End Try
    End Sub
    Sub SetNewLineSubGrid(ByVal UniqID As Integer, ByVal oMatSubGrid As SAPbouiCOM.Matrix, ByVal oDBDSSubGrid As SAPbouiCOM.DBDataSource, Optional ByVal RowID As Integer = 1, Optional ByVal ColumnUID As String = "", Optional ByVal DefaulFields As String(,) = Nothing)
        Try
            If ColumnUID.Equals("") = False Then
                If oMatSubGrid.Columns.Item(ColumnUID).Cells.Item(RowID).Specific.Value.Equals("") = False And RowID = oMatSubGrid.VisualRowCount Then
                    oMatSubGrid.FlushToDataSource()
                    oMatSubGrid.AddRow()
                    oDBDSSubGrid.InsertRecord(oDBDSSubGrid.Size)
                    oDBDSSubGrid.Offset = oMatSubGrid.VisualRowCount - 1
                    oDBDSSubGrid.SetValue("LineID", oDBDSSubGrid.Offset, oMatSubGrid.VisualRowCount)
                    oDBDSSubGrid.SetValue("U_UniqueID", oDBDSSubGrid.Offset, UniqID)
                    If Not DefaulFields Is Nothing Then
                        For f As Int16 = 0 To DefaulFields.GetLength(0) - 1
                            oDBDSSubGrid.SetValue(DefaulFields(f, 0), oDBDSSubGrid.Offset, DefaulFields(f, 1))
                        Next
                    End If
                    oMatSubGrid.SetLineData(oMatSubGrid.VisualRowCount)
                    oMatSubGrid.FlushToDataSource()
                    oMatSubGrid.LoadFromDataSource()
                End If
            Else
                oMatSubGrid.FlushToDataSource()
                oMatSubGrid.AddRow()
                oDBDSSubGrid.InsertRecord(oDBDSSubGrid.Size)
                oDBDSSubGrid.Offset = oMatSubGrid.VisualRowCount - 1
                oDBDSSubGrid.SetValue("LineID", oDBDSSubGrid.Offset, oMatSubGrid.VisualRowCount)
                oDBDSSubGrid.SetValue("U_UniqueID", oDBDSSubGrid.Offset, UniqID)
                If Not DefaulFields Is Nothing Then
                    For f As Int16 = 0 To DefaulFields.GetLength(0) - 1
                        oDBDSSubGrid.SetValue(DefaulFields(f, 0), oDBDSSubGrid.Offset, DefaulFields(f, 1))
                    Next
                End If
                oMatSubGrid.SetLineData(oMatSubGrid.VisualRowCount)
                oMatSubGrid.FlushToDataSource()
                oMatSubGrid.LoadFromDataSource()
            End If
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("SetNewLine Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


    Function LoadSubGrid(ByVal oMatSubGrid As SAPbouiCOM.Matrix, ByVal oDBDSSubGrid As SAPbouiCOM.DBDataSource, ByVal oDBDSMainSubGrid As SAPbouiCOM.DBDataSource, ByVal UniqID As Integer, Optional ByVal DefaulFields As String(,) = Nothing) As Boolean
        Try
            oMatSubGrid.Clear()
            oDBDSSubGrid.Clear()

            Dim strGetColUID As String = "Select AliasID From CUFD Where  AliasID <> 'UniqID' AND TableID ='" & oDBDSSubGrid.TableName & "' "
            Dim rsetGetColUID As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            rsetGetColUID.DoQuery(strGetColUID)

            Dim strColUID As String = ""

            Dim boolUniqID As Boolean = False
            For x As Integer = 0 To oDBDSMainSubGrid.Size - 1
                If Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", x)).Equals(UniqID.ToString) Then
                    boolUniqID = True
                    Exit For
                End If
            Next
            If boolUniqID = False Then
                ' Add the Rows in Sub Grid ...
                SetNewLineSubGrid(UniqID, oMatSubGrid, oDBDSSubGrid, , , DefaulFields)

            ElseIf oDBDSMainSubGrid.Size >= 1 And boolUniqID = True Then
                For i As Integer = 0 To oDBDSMainSubGrid.Size - 1
                    oDBDSMainSubGrid.Offset = i
                    If Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset)).Equals("") = False And oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset).Trim.Equals("") = False Then
                        If CInt(oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset)) = UniqID Then
                            oDBDSSubGrid.InsertRecord(oDBDSSubGrid.Size)
                            oDBDSSubGrid.Offset = oDBDSSubGrid.Size - 1
                            oDBDSSubGrid.SetValue("LineID", oDBDSSubGrid.Offset, oDBDSSubGrid.Offset + 1)
                            oDBDSSubGrid.SetValue("U_UniqueID", oDBDSSubGrid.Offset, UniqID)
                            If Not DefaulFields Is Nothing Then
                                For f As Int16 = 0 To DefaulFields.GetLength(0) - 1
                                    oDBDSSubGrid.SetValue(DefaulFields(f, 0), oDBDSSubGrid.Offset, DefaulFields(f, 1))
                                Next
                            End If
                            rsetGetColUID.MoveFirst()
                            For j As Integer = 0 To rsetGetColUID.RecordCount - 1
                                strColUID = "U_" & rsetGetColUID.Fields.Item(0).Value
                                oDBDSSubGrid.SetValue(strColUID, oDBDSSubGrid.Offset, oDBDSMainSubGrid.GetValue(strColUID, oDBDSMainSubGrid.Offset).Trim)
                                rsetGetColUID.MoveNext()
                            Next
                        End If



                    End If

                Next
                oMatSubGrid.LoadFromDataSource()
                oMatSubGrid.FlushToDataSource()
                ' Add the Rows in Sub Grid ...
                'Me.SetNewLineSubGrid(UniqID, oMatSubGrid, oDBDSSubGrid, , , DefaulFields)
            End If
            Return True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Global Fun. : Load Sub Grid " & ex.Message)
            Return False
        Finally
        End Try
    End Function

    'Function DoQuery(ByVal strSql As String) As SAPbobsCOM.Recordset
    '    Try
    '        Dim rsetCode As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
    '        rsetCode.DoQuery(strSql)
    '        Return rsetCode
    '    Catch ex As Exception
    '        objAddOn.objApplication.StatusBar.SetText("Execute Query Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function

    Function SaveSubGrid(ByVal oMatMainGrid As SAPbouiCOM.Matrix, ByVal oDBDSMainSubGrid As SAPbouiCOM.DBDataSource, ByVal oMatSubGrid As SAPbouiCOM.Matrix, ByVal oDBDsSubGrid As SAPbouiCOM.DBDataSource, ByVal RowID As Integer) As Boolean
        Try
            Dim strGetColUID As String = "Select AliasID From CUFD Where  AliasID <> 'UniqID' AND TableID ='" & oDBDsSubGrid.TableName & "' "
            Dim rsetGetColUID As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            rsetGetColUID.DoQuery(strGetColUID)
            Dim strColUID As String = ""

            Dim intInitSize As Integer = oDBDSMainSubGrid.Size
            Dim intCurrSize As Integer = oDBDSMainSubGrid.Size
            Dim oEmpty As Boolean = True


            For i As Integer = 0 To intInitSize - 1
                oDBDSMainSubGrid.Offset = i - (intInitSize - intCurrSize)
                Dim aa = oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset).Trim
                If oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset).Trim <> "" Then
                    If CInt(oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset)) = RowID Then
                        oDBDSMainSubGrid.RemoveRecord(oDBDSMainSubGrid.Offset)
                    End If
                ElseIf oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset).Trim.Equals("") Then
                    oDBDSMainSubGrid.RemoveRecord(oDBDSMainSubGrid.Offset)
                End If
                intCurrSize = oDBDSMainSubGrid.Size
            Next

            If oDBDSMainSubGrid.Size = 0 Then
                oDBDSMainSubGrid.InsertRecord(oDBDSMainSubGrid.Size)
                oDBDSMainSubGrid.Offset = 0
            End If

            If oDBDSMainSubGrid.Size = 1 And Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset)).Equals("") Then
                oEmpty = True
            Else
                oEmpty = False
            End If
            oMatSubGrid.FlushToDataSource()
            oMatSubGrid.LoadFromDataSource()
            For i As Integer = 0 To oMatSubGrid.VisualRowCount - 1
                oDBDsSubGrid.Offset = i
                If oEmpty = True Then
                    oDBDSMainSubGrid.Offset = oDBDSMainSubGrid.Size - 1
                    oDBDSMainSubGrid.SetValue("U_UniqueID", oDBDSMainSubGrid.Offset, RowID)
                    oDBDSMainSubGrid.SetValue("LineID", oDBDSMainSubGrid.Offset, oDBDSMainSubGrid.Size)
                    rsetGetColUID.MoveFirst()
                    For j As Integer = 0 To rsetGetColUID.RecordCount - 1
                        strColUID = "U_" & rsetGetColUID.Fields.Item(0).Value
                        oDBDSMainSubGrid.SetValue(strColUID, oDBDSMainSubGrid.Offset, oDBDsSubGrid.GetValue(strColUID, i).Trim)
                        rsetGetColUID.MoveNext()
                    Next
                    If i <> (oMatSubGrid.VisualRowCount - 1) Then
                        oDBDSMainSubGrid.InsertRecord(oDBDSMainSubGrid.Size)
                    End If
                ElseIf oEmpty = False Then

                    oDBDSMainSubGrid.InsertRecord(oDBDSMainSubGrid.Size)
                    oDBDSMainSubGrid.Offset = oDBDSMainSubGrid.Size - 1
                    oDBDSMainSubGrid.SetValue("U_UniqueID", oDBDSMainSubGrid.Offset, RowID)
                    oDBDSMainSubGrid.SetValue("LineID", oDBDSMainSubGrid.Offset, oDBDSMainSubGrid.Size)
                    rsetGetColUID.MoveFirst()
                    For j As Integer = 0 To rsetGetColUID.RecordCount - 1
                        strColUID = "U_" & rsetGetColUID.Fields.Item(0).Value
                        oDBDSMainSubGrid.SetValue(strColUID, oDBDSMainSubGrid.Offset, oDBDsSubGrid.GetValue(strColUID, i).Trim)
                        rsetGetColUID.MoveNext()
                    Next
                End If
            Next
            oMatMainGrid.LoadFromDataSource()

            Return True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Save Sub Grid Method Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        Finally
        End Try
    End Function



    Function Validation_SubGrid() As Boolean
        Try
            'oMatSubPlanSetails.FlushToDataSource()
            'For i As Integer = 1 To oMatSubPlanSetails.VisualRowCount - 1
            '    Dim Type As String = oMatSubPlanSetails.Columns.Item("type").Cells.Item(i).Specific.Value
            '    If Type.Equals("LV") Then
            '        If oMatSubPlanSetails.Columns.Item("lvecod").Cells.Item(i).Specific.Value() = "" Then
            '            objAddOn.objApplication.StatusBar.SetText("Plan Details Line :" & i & " : Leave Code Should Not Left Empty !!!")
            '        End If
            '    End If
            'Next

            Return True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("SubGrid Validation Failed" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Function
    Sub DeleteRowSubGrid(ByVal oMatMainSubGrid As SAPbouiCOM.Matrix, ByVal oDBDSMainSubGrid As SAPbouiCOM.DBDataSource, ByVal SubRowID As Integer)
        Try
            oMatMainSubGrid.FlushToDataSource()
            For i As Integer = 0 To oDBDSMainSubGrid.Size - 1
                If i <= oDBDSMainSubGrid.Size - 1 Then
                    oDBDSMainSubGrid.Offset = i
                    oDBDSMainSubGrid.SetValue("LineID", oDBDSMainSubGrid.Offset, i + 1)

                    If Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", i)).Equals("") = False Then
                        Dim aa = Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", i))
                        If CDbl(oDBDSMainSubGrid.GetValue("U_UniqueID", i)) = SubRowID Then
                            If Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", i)).Equals(SubRowID.ToString().Trim) Then
                                oDBDSMainSubGrid.RemoveRecord(i)
                                i -= 1
                            Else
                                oDBDSMainSubGrid.SetValue("U_UniqueID", oDBDSMainSubGrid.Offset, CDbl(oDBDSMainSubGrid.GetValue("U_UniqueID", i)) - 1)
                            End If
                        End If
                    Else
                        oDBDSMainSubGrid.RemoveRecord(i)
                        i -= 1
                    End If
                End If
            Next
            oMatMainSubGrid.LoadFromDataSource()
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Delete Row SubGrid Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub ItemEvent_SubGrid(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "add"
                                If pVal.BeforeAction Then
                                    If Me.Validation_SubGrid = False Then BubbleEvent = False
                                ElseIf pVal.BeforeAction = False Then


                                    If frmProductionReceipt.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                        DeleteRowSubGrid(oMatMainPlanSetails, oDBDSMainPlanDetails, SubRowID_PlanningDetails)
                                    End If
                                    If SaveSubGrid(oMatMainPlanSetails, oDBDSMainPlanDetails, oMatSubPlanSetails, oDBDSSubPlanDetails, SubRowID_PlanningDetails) Then
                                        'Dim strTName As String = ""
                                        'For i As Integer = 1 To oMatMainPlanSetails.RowCount
                                        '    Dim s As Integer = oMatMainPlanSetails.Columns.Item("UID").Cells.Item(i).Specific.Value
                                        '    If SubRowID_PlanningDetails = oMatMainPlanSetails.Columns.Item("UID").Cells.Item(i).Specific.Value Then
                                        '        If i <> oMatMainPlanSetails.RowCount Then
                                        '            strTName = strTName + oMatMainPlanSetails.Columns.Item("TName").Cells.Item(i).Specific.Value + ","
                                        '        End If
                                        '    End If
                                        'Next

                                        'oDBDSBuy.SetValue("U_BoxNum", SubRowID_PlanningDetails - 1, strTName.TrimEnd(","))

                                        'oMatrix = frmProductionReceipt.Items.Item("52").Specific
                                        'oMatrix.LoadFromDataSource()
                                        'oMatrix = frmProductionReceipt.Items.Item("52").Specific
                                        'oMatrix.Columns.Item("BNo").Cells.Item(pVal.Row).
                                        'Specific.value = strTName.ToString()
                                        'oMatrix.LoadFromDataSource()
                                        frmSubPlanningDetailsSubGrid.Close()
                                        If frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                    End If
                                End If
                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("Click Event Failed : " & ex.Message)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing And pVal.BeforeAction = False And frmSubPlanningDetailsSubGrid.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            Select Case pVal.ItemUID
                                Case "submatrix"
                                    Select Case pVal.ColUID

                                    End Select
                                    Select Case pVal.ColUID

                                    End Select
                            End Select
                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("Sub-grid Cost Center : Item event failed : " & ex.Message)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "submatrix"
                                Select Case pVal.ColUID
                                    Case "Dayoff"


                                End Select
                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("Item Pressed  Event Failed : " & ex.Message)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "submatrix"
                                Select Case pVal.ColUID

                                End Select
                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("matrix link pressed Event Failed : " & ex.Message)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID
                            Case "submatrix"
                                Select Case pVal.ColUID
                                    Case "type"
                                End Select
                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "submatrix"
                                Select Case pVal.ColUID
                                    Case "Qty"
                                        SetNewLineSubGrid(SubRowID_PlanningDetails, oMatSubPlanSetails, oDBDSSubPlanDetails, pVal.Row, pVal.ColUID)
                                End Select
                        End Select
                    Catch ex As Exception
                        objAddOn.objApplication.StatusBar.SetText("Lost Focus Event  : " & ex.Message)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("SubGrid Item Event Failed" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub
    Sub MenuEvent_SubGrid(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1293"
                    ' oGfun.DeleteRow(oMatSubPlanSetails, oDBDSSubPlanDetails)
            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function Format_StringToDate(ByVal sDate As String) As DateTime
        Try
            Dim rsetBob As SAPbobsCOM.SBObob = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge)
            Dim rsetServerDate As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            rsetServerDate = rsetBob.Format_StringToDate(sDate)
            Return rsetServerDate.Fields.Item(0).Value

        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Format_StringToDate:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return ""
        Finally
        End Try
    End Function

#End Region
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try


            If pVal.BeforeAction = True Then
                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_CLICK

                        Try
                            If pVal.ItemUID = "LoadBatch" Then
                                Dim sMatrix As SAPbouiCOM.Matrix = frmProductionReceipt.Items.Item("52").Specific

                                Dim LineID As Integer = sMatrix.VisualRowCount
                                Dim pFGCode As String = sMatrix.Columns.Item("V_4").Cells.Item(1).Specific.value
                                Dim pFGName As String = sMatrix.Columns.Item("V_3").Cells.Item(1).Specific.value
                                Dim UOM As String = sMatrix.Columns.Item("Uom").Cells.Item(1).Specific.value
                                Dim sWharehouseCode As String = sMatrix.Columns.Item("WCode").Cells.Item(1).Specific.value
                                Dim sWharehouseName As String = sMatrix.Columns.Item("WName").Cells.Item(1).Specific.value
                                Dim sBatchNum As String = sMatrix.Columns.Item("V_1").Cells.Item(1).Specific.value
                                Dim dBatchTotalSplitQty As Double = frmProductionReceipt.Items.Item("t_NofDoff").Specific.value
                                Dim dBatchTotalQty As Double = frmProductionReceipt.Items.Item("t_DQty").Specific.value
                                Dim dSplitQty As Double = dBatchTotalQty / dBatchTotalSplitQty
                                sMatrix.Clear()

                                Dim sLineid As Integer = 1
                                Dim WODocentry As Integer = oDBDSHeader.GetValue("U_WODocE", 0).ToString().Trim()

                                Dim strSqlWOType = objAddOn.getSingleValue(" Select T0.U_Type from [@AS_OWORD] T0 Where T0.Docentry='" & oDBDSHeader.GetValue("U_WODocE", 0).ToString().Trim() & "' ")
                                Dim oRS_RouteDetails As SAPbobsCOM.Recordset
                                oRS_RouteDetails = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRS_RouteDetails.DoQuery("Select U_WeightLoad  from [@AIS_WRKTYP]  Where code  = '" & strSqlWOType & "'")
                                oRS_RouteDetails.MoveFirst()
                                Dim U_WeightLoad As String = String.Empty
                                For i As Integer = 1 To oRS_RouteDetails.RecordCount
                                    U_WeightLoad = oRS_RouteDetails.Fields.Item("U_WeightLoad").Value.ToString().Trim()
                                Next
                                If U_WeightLoad = "Y" Then
                                    Dim StartNo As Integer = frmProductionReceipt.Items.Item("t_StartNo").Specific.value
                                    For i As Integer = 0 To dBatchTotalSplitQty - 1
                                        sMatrix.AddRow()

                                        sMatrix.Columns.Item("V_-1").Cells.Item(sLineid).Specific.value = sLineid
                                        Dim cmbFGType As SAPbouiCOM.ComboBox = sMatrix.Columns.Item("V_10").Cells.Item(sLineid).Specific
                                        cmbFGType.Select("M", SAPbouiCOM.BoSearchKey.psk_ByValue)
                                        sMatrix.Columns.Item("V_4").Cells.Item(sLineid).Specific.value = pFGCode
                                        sMatrix.Columns.Item("V_3").Cells.Item(sLineid).Specific.value = pFGName
                                        sMatrix.Columns.Item("WCode").Cells.Item(sLineid).Specific.value = sWharehouseCode
                                        Try
                                            sMatrix.Columns.Item("WName").Cells.Item(sLineid).Specific.value = sWharehouseName
                                        Catch ex As Exception

                                        End Try

                                        sMatrix.Columns.Item("V_1").Cells.Item(sLineid).Specific.value = sBatchNum
                                        Try
                                            sMatrix.Columns.Item("Uom").Cells.Item(sLineid).Specific.value = UOM
                                        Catch ex As Exception

                                        End Try

                                        Try
                                            sMatrix.Columns.Item("qty").Cells.Item(sLineid).Specific.value = dSplitQty.ToString()
                                        Catch ex As Exception

                                        End Try
                                        Try
                                            sMatrix.Columns.Item("BNo").Cells.Item(sLineid).Specific.value = StartNo
                                        Catch ex As Exception

                                        End Try
                                        sLineid = sLineid + 1
                                        StartNo = StartNo + 1

                                    Next
                                    sMatrix.Columns.Item("V_-1").Cells.Item(sMatrix.VisualRowCount).Specific.value = sMatrix.VisualRowCount
                                Else

                                    For i As Integer = 0 To dBatchTotalSplitQty - 1
                                        sMatrix.AddRow()

                                        sMatrix.Columns.Item("V_-1").Cells.Item(sLineid).Specific.value = sLineid
                                        sMatrix.Columns.Item("V_4").Cells.Item(sLineid).Specific.value = pFGCode
                                        sMatrix.Columns.Item("V_3").Cells.Item(sLineid).Specific.value = pFGName
                                        sMatrix.Columns.Item("WCode").Cells.Item(sLineid).Specific.value = sWharehouseCode
                                        Try
                                            sMatrix.Columns.Item("WName").Cells.Item(sLineid).Specific.value = sWharehouseName
                                        Catch ex As Exception

                                        End Try

                                        sMatrix.Columns.Item("V_1").Cells.Item(sLineid).Specific.value = sBatchNum
                                        Try
                                            sMatrix.Columns.Item("Uom").Cells.Item(sLineid).Specific.value = UOM
                                        Catch ex As Exception

                                        End Try

                                        Try
                                            sMatrix.Columns.Item("qty").Cells.Item(sLineid).Specific.value = dSplitQty.ToString()
                                        Catch ex As Exception

                                        End Try
                                        sLineid = sLineid + 1


                                    Next
                                    sMatrix.Columns.Item("V_-1").Cells.Item(sMatrix.VisualRowCount).Specific.value = sMatrix.VisualRowCount

                                End If

                            End If

                            'If pVal.ItemUID = "LoadBatch" Then
                            '    Dim sMatrix As SAPbouiCOM.Matrix = frmProductionReceipt.Items.Item("52").Specific

                            '    Dim LineID As Integer = sMatrix.VisualRowCount
                            '    Dim pFGCode As String = sMatrix.Columns.Item("V_4").Cells.Item(1).Specific.value
                            '    Dim pFGName As String = sMatrix.Columns.Item("V_3").Cells.Item(1).Specific.value
                            '    Dim UOM As String = sMatrix.Columns.Item("Uom").Cells.Item(1).Specific.value
                            '    Dim sWharehouseCode As String = sMatrix.Columns.Item("WCode").Cells.Item(1).Specific.value
                            '    Dim sWharehouseName As String = sMatrix.Columns.Item("WName").Cells.Item(1).Specific.value
                            '    Dim sBatchNum As String = sMatrix.Columns.Item("V_1").Cells.Item(1).Specific.value
                            '    Dim dBatchTotalSplitQty As Double = frmProductionReceipt.Items.Item("t_NofDoff").Specific.value
                            '    Dim dBatchTotalQty As Double = frmProductionReceipt.Items.Item("t_DQty").Specific.value
                            '    Dim dSplitQty As Double = dBatchTotalQty / dBatchTotalSplitQty
                            '    sMatrix.Clear()

                            '    Dim sLineid As Integer = 1
                            '    For i As Integer = 0 To dBatchTotalSplitQty - 1
                            '        sMatrix.AddRow()

                            '        sMatrix.Columns.Item("V_-1").Cells.Item(sLineid).Specific.value = sLineid
                            '        sMatrix.Columns.Item("V_4").Cells.Item(sLineid).Specific.value = pFGCode
                            '        sMatrix.Columns.Item("V_3").Cells.Item(sLineid).Specific.value = pFGName
                            '        sMatrix.Columns.Item("WCode").Cells.Item(sLineid).Specific.value = sWharehouseCode
                            '        Try
                            '            sMatrix.Columns.Item("WName").Cells.Item(sLineid).Specific.value = sWharehouseName
                            '        Catch ex As Exception

                            '        End Try

                            '        sMatrix.Columns.Item("V_1").Cells.Item(sLineid).Specific.value = sBatchNum
                            '        Try
                            '            sMatrix.Columns.Item("Uom").Cells.Item(sLineid).Specific.value = UOM
                            '        Catch ex As Exception

                            '        End Try

                            '        Try
                            '            sMatrix.Columns.Item("qty").Cells.Item(sLineid).Specific.value = dSplitQty.ToString()
                            '        Catch ex As Exception

                            '        End Try
                            '        sLineid = sLineid + 1


                            '    Next
                            '    sMatrix.Columns.Item("V_-1").Cells.Item(sMatrix.VisualRowCount).Specific.value = sMatrix.VisualRowCount

                            'End If
                            If pVal.ItemUID = "1" And frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                If ProdType = 2 Then
                                    If LoadRM(FormUID) = False Or LoadLabour(FormUID) = False Or LoadIdleTime(FormUID) = False Then
                                        BubbleEvent = False
                                        Exit Sub
                                    Else
                                        Dim SQL As String = "update [@AS_WORD1] set U_Labour =" & ProTyp2Cost & ",U_Status ='C',U_PDEntry =" & frmProductionReceipt.Items.Item("10").Specific.string & " where DocEntry =" & DocEntry & " and LineId =" & CurrLineID & ""
                                        oRS.DoQuery(SQL)
                                    End If
                                End If
                                If ProdType = 1 Then
                                    If Validate() = True Then
                                        If objAddOn.objCompany.InTransaction = False Then objAddOn.objCompany.StartTransaction()

                                        If Goods_Receipt() = False Then
                                            objAddOn.objApplication.StatusBar.SetText("Validate:" & objAddOn.objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                            If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                            BubbleEvent = False
                                            Exit Sub
                                        Else
                                            If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                        End If
                                    Else
                                        BubbleEvent = False
                                        Exit Sub
                                    End If
                                End If

                            End If

                            If pVal.ItemUID = "2" And frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                objAddOn.objWorkOrder.Refresh(WODocEntry)
                            End If


                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("iTEM EVENTS   Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_FORM_CLOSE
                        If frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            objAddOn.objWorkOrder.Refresh(WODocEntry)
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        If pVal.ItemUID = "52" And pVal.ColUID = "WCode" Then
                            Dim sMatrix As SAPbouiCOM.Matrix = frmProductionReceipt.Items.Item("52").Specific
                            Dim WhsCode As String = sMatrix.Columns.Item("WCode").Cells.Item(pVal.Row).Specific.value
                            If WhsCode = "WAH0020" Then
                                DisableWhareHouse = True
                            Else
                                DisableWhareHouse = False
                            End If

                        End If


                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.ItemUID = "48" Then
                            frmProductionReceipt.PaneLevel = 1
                            Dim fSettings As SAPbouiCOM.FormSettings
                            fSettings = frmProductionReceipt.Settings
                            fSettings.MatrixUID = "16"


                        ElseIf pVal.ItemUID = "14" Then
                            frmProductionReceipt.PaneLevel = 2

                            Dim fSettings As SAPbouiCOM.FormSettings
                            fSettings = frmProductionReceipt.Settings
                            fSettings.MatrixUID = "17"

                        ElseIf pVal.ItemUID = "15" Then
                            frmProductionReceipt.PaneLevel = 3

                            Dim fSettings As SAPbouiCOM.FormSettings
                            fSettings = frmProductionReceipt.Settings
                            fSettings.MatrixUID = "18"


                        ElseIf pVal.ItemUID = "27" Then
                            frmProductionReceipt.PaneLevel = 4
                            Dim fSettings As SAPbouiCOM.FormSettings
                            fSettings = frmProductionReceipt.Settings
                            fSettings.MatrixUID = "28"


                        ElseIf pVal.ItemUID = "Fol_FG" Then
                            frmProductionReceipt.PaneLevel = 6
                            Dim fSettings As SAPbouiCOM.FormSettings
                            fSettings = frmProductionReceipt.Settings
                            fSettings.MatrixUID = "52"


                        ElseIf pVal.ItemUID = "79" Then
                            frmProductionReceipt.PaneLevel = 5



                        ElseIf pVal.ItemUID = "MC" Then
                            frmProductionReceipt.PaneLevel = 7
                            Dim fSettings As SAPbouiCOM.FormSettings
                            fSettings = frmProductionReceipt.Settings
                            fSettings.MatrixUID = "oMatrixP"



                        ElseIf pVal.ItemUID = "SD" Then
                            frmProductionReceipt.PaneLevel = 8
                            Dim fSettings As SAPbouiCOM.FormSettings
                            fSettings = frmProductionReceipt.Settings
                            fSettings.MatrixUID = "oMatrixSD"

                        End If
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If pVal.ItemUID = "54" Then
                            'obj()
                        End If
                        'Dim OPId = oForm.Items.Item("62").Specific.value
                        'Dim OpCode = objAddOn.objWorkOrder.Matrix1.Columns.Item("OpCode").Cells.Item(CInt(OPId)).Specific.value
                        Select Case oCFLE.ChooseFromListUID


                            'Case "CFL_4"
                            '    objAddOn.ChooseFromListFilteration(oForm, "CFL_4", "ItemCode", "select ItemCode from OITM where  oitm.ItmsGrpCod in (select U_ItmGrp from [@AIS_ITMGRP] where U_Type='R' and U_OPCode='" & OpCode & "')")

                            'Case "Cfl_Buy"
                            '    objAddOn.ChooseFromListFilteration(oForm, "Cfl_Buy", "ItemCode", "select ItemCode from OITM where  oitm.ItmsGrpCod in (select U_ItmGrp from [@AIS_ITMGRP] where U_Type='F' and U_OPCode='" & OpCode & "')")

                            Case "Cfl_item"
                                objAddOn.ChooseFromListFilteration(frmProductionReceipt, "Cfl_item", "ItemCode", "select ItemCode from OITM ")

                            Case "Cfl_mach"
                                objAddOn.ChooseFromListFilteration(frmProductionReceipt, "Cfl_mach", "ResCode", "Select  ResCode  From ORSC Where ResType ='M'")

                            Case "CFL_6"
                                objAddOn.ChooseFromListFilteration(frmProductionReceipt, "CFL_6", "ResCode", "Select  ResCode  From ORSC Where ResType ='M'")

                            Case "CFL_7"
                                objAddOn.ChooseFromListFilteration(frmProductionReceipt, "CFL_7", "Code", "select code from [@AS_OLBR]")
                        End Select


                End Select
            Else

                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.ItemUID = "54" Or pVal.ItemUID = "4" Or pVal.ItemUID = "6" Or pVal.ItemUID = "40" Or pVal.ItemUID = "32" Or pVal.ItemUID = "16" Or pVal.ItemUID = "17" Or pVal.ItemUID = "18" Or pVal.ColUID = "V_4" Or pVal.ColUID = "V_1" Or pVal.ColUID = "V_0" Or pVal.ItemUID = "74" Then
                            ChooseItem(FormUID, pVal)
                        End If

                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Select Case oCFLE.ChooseFromListUID
                            Case "Cfl_BDRNo"
                                oMatrix = frmProductionReceipt.Items.Item("28").Specific
                                Try
                                    oMatrix.Columns.Item("BDRE").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("DocNum", 0)

                                Catch ex As Exception
                                End Try

                                Try
                                    oMatrix.Columns.Item("BDRNo").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("DocEntry", 0)

                                Catch ex As Exception
                                End Try



                            Case "Cfl_mach"
                                oMatrix = frmProductionReceipt.Items.Item("28").Specific
                                Try
                                    oMatrix.Columns.Item("V_2").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("ResCode", 0)

                                Catch ex As Exception
                                End Try
                                oMatrix.AddRow()
                                oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value() = oMatrix.VisualRowCount

                            Case "CFL_Lbr"
                                oMatrix = frmProductionReceipt.Items.Item("18").Specific
                                Try

                                    Try
                                        oMatrix.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("ResName", 0)
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("ResCode", 0)
                                    Catch ex As Exception

                                    End Try


                                    oMatrix.AddRow()
                                    oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value() = oMatrix.VisualRowCount
                                Catch ex As Exception
                                    objAddOn.objApplication.SetStatusBarMessage("iTEM EVENTS   Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                End Try

                                'If pVal.ColUID = "V_4" Then
                                '    If oMatrix.Columns.Item("V_4").Cells.Item(oMatrix.VisualRowCount).Specific.value <> "" Then
                                '        oMatrix.AddRow()
                                '        oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value() = oMatrix.VisualRowCount
                                '    End If
                                'End If


                            Case "CFL_4"
                                oMatrix = frmProductionReceipt.Items.Item("52").Specific
                                Try
                                    oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("ItemCode", 0)

                                Catch ex As Exception
                                End Try
                                Try
                                    oMatrix.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("ItemCode", 0)

                                Catch ex As Exception
                                End Try


                            Case "CFL_4"
                                oMatrix = frmProductionReceipt.Items.Item("16").Specific
                                Try
                                    oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("ItemCode", 0)


                                Catch ex As Exception
                                End Try
                                If pVal.ColUID = "V_4" Then
                                    If oMatrix.Columns.Item("V_4").Cells.Item(oMatrix.VisualRowCount).Specific.value <> "" Then
                                        oMatrix.AddRow()
                                        oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value() = oMatrix.VisualRowCount
                                    End If
                                End If
                                oMatrix.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("ItemName", 0)
                                Dim itmcde = oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value
                                Dim whscde = oMatrix.Columns.Item("V_1").Cells.Item(pVal.Row).Specific.value
                                Dim stk = objAddOn.getSingleValue("select OnHand from OITW where ItemCode='" & itmcde & "' and WhsCode='" & whscde & "' ")
                                oMatrix.Columns.Item("V_6").Cells.Item(pVal.Row).Specific.value = stk
                                oMatrix.Columns.Item("V_8").Cells.Item(pVal.Row).Specific.value = "Link"

                            Case "CFL_5"
                                oMatrix = frmProductionReceipt.Items.Item("16").Specific
                                Try
                                    oMatrix.Columns.Item("V_1").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("WhsCode", 0)


                                Catch ex As Exception
                                    objAddOn.objApplication.SetStatusBarMessage("iTEM EVENTS   Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                End Try
                                oMatrix.Columns.Item("WName").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("WhsName", 0)
                                Dim itmcde = oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value
                                Dim whscde = oMatrix.Columns.Item("V_1").Cells.Item(pVal.Row).Specific.value
                                Dim stk = objAddOn.getSingleValue("select OnHand from OITW where ItemCode='" & itmcde & "' and WhsCode='" & whscde & "' ")
                                oMatrix.Columns.Item("V_6").Cells.Item(pVal.Row).Specific.value = stk
                                oMatrix.Columns.Item("V_8").Cells.Item(pVal.Row).Specific.value = "Link"

                            Case "Cfl_Buy"
                                oMatrix = frmProductionReceipt.Items.Item("52").Specific
                                Try
                                    oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("ItemCode", 0)


                                Catch ex As Exception
                                    objAddOn.objApplication.SetStatusBarMessage("iTEM EVENTS   Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                End Try
                                oMatrix.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("ItemName", 0)
                            Case "Cfl_whs"
                                oMatrix = frmProductionReceipt.Items.Item("52").Specific
                                Try
                                    oMatrix.Columns.Item("WCode").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("WhsCode", 0)


                                Catch ex As Exception
                                End Try
                                oMatrix.Columns.Item("WName").Cells.Item(pVal.Row).Specific.value = oDataTable.GetValue("WhsName", 0)


                        End Select

                    Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED

                        If pVal.ItemUID = "52" And pVal.ColUID = "BNo" Then
                            SubRowID_PlanningDetails = pVal.Row
                            Me.LoadSubGridDetails(SubRowID_PlanningDetails)

                        End If
                        If pVal.ItemUID = "16" And pVal.ColUID = "V_8" Then
                            If (pVal.BeforeAction = False) Then
                                oMatrix = frmProductionReceipt.Items.Item("16").Specific
                                Dim itm As String = oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value.ToString.Trim
                                Dim ManagBatch = objAddOn.getSingleValue("select ManBtchNum from oitm where itemcode='" & itm & "'")
                                If ManagBatch = "Y" Then
                                    Dim LineID As String = oMatrix.Columns.Item("V_-1").Cells.Item(pVal.Row).Specific.value
                                    Dim DocNum As String = frmProductionReceipt.Items.Item("10").Specific.value

                                    obatserno.LoadForm(LineID, oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value, oMatrix.Columns.Item("TQty").Cells.Item(pVal.Row).Specific.value, oMatrix.Columns.Item("V_8").Cells.Item(pVal.Row).Specific.value, DocNum, frmProductionReceipt.Mode, oMatrix.Columns.Item("V_1").Cells.Item(pVal.Row).Specific.value, oMatrix.Columns.Item("WName").Cells.Item(pVal.Row).Specific.value)

                                    'Dim sGetBatchScreenDocNum = objAddOn.getSingleValue("select DocNum from [@AIS_RVL_BSN1] where DocNum='" & DocNum & "' and LineID ='" + LineID + "'")
                                    'oMatrix.Columns.Item("V_8").Cells.Item(pVal.Row).Specific.value = sGetBatchScreenDocNum

                                End If

                            End If
                        End If

                        If pVal.ItemUID = "52" And pVal.ColUID = "V_2" Then
                            Dim whscde = oMatrix.Columns.Item("V_0").Cells.Item(pVal.Row).Specific.value.ToString.Trim
                            Dim managedby As String = objAddOn.getSingleValue("select BinActivat from owhs where WhsCode='" & whscde & "'")
                            If managedby = "Y" Then
                                oMatrix = frmProductionReceipt.Items.Item("52").Specific
                                Row2 = pVal.Row
                                LoadForm_RM(oMatrix.Columns.Item("V_2").Cells.Item(pVal.Row).Specific.value.ToString.Trim)
                            End If

                        End If

                        If pVal.ItemUID = "16" And pVal.ColUID = "V_2" Then
                            Try
                                oMatrix = frmProductionReceipt.Items.Item("16").Specific
                                Dim whscde = oMatrix.Columns.Item("V_1").Cells.Item(pVal.Row).Specific.value.ToString.Trim
                                Dim managedby As String = objAddOn.getSingleValue("select BinActivat from owhs where WhsCode='" & whscde & "'")
                                If managedby = "Y" Then
                                    Row3 = pVal.Row
                                    LoadForm_RMBIN(oMatrix.Columns.Item("V_2").Cells.Item(pVal.Row).Specific.value.ToString.Trim)
                                End If

                            Catch ex As Exception
                                objAddOn.objApplication.SetStatusBarMessage("iTEM EVENTS   Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                            End Try

                        End If



                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        Try






                            If pVal.ItemUID = "oMatrixSD" And pVal.ColUID = "No" Then
                                oMatrixSD = frmProductionReceipt.Items.Item("oMatrixSD").Specific
                                If oMatrixSD.Columns.Item("No").Cells.Item(pVal.Row).Specific.string <> "" And pVal.Row = oMatrixSD.VisualRowCount Then
                                    objAddOn.SetNewLine(oMatrixSD, oDBDSpindle)
                                End If
                            End If
                            If pVal.ItemUID = "oMatrixP" And pVal.ColUID = "PName" Then
                                oMatrixP = frmProductionReceipt.Items.Item("oMatrixP").Specific
                                If oMatrixP.Columns.Item("PName").Cells.Item(pVal.Row).Specific.string <> "" And pVal.Row = oMatrixP.VisualRowCount Then
                                    objAddOn.SetNewLine(oMatrixP, oDBDParameter)
                                End If
                            End If

                            If (pVal.ItemUID = "17" Or pVal.ItemUID = "18") And (pVal.ColUID = "V_1" Or pVal.ColUID = "V_6" Or pVal.ColUID = "V_7" Or pVal.ColUID = "V_9") Then
                                Try

                                    Dim sQuery2 As String = String.Empty
                                    If pVal.ItemUID = "17" Then
                                        Dim oMatrixMachine As SAPbouiCOM.Matrix

                                        oMatrixMachine = frmProductionReceipt.Items.Item("17").Specific
                                        Dim sFromDate As String = oMatrixMachine.Columns.Item("V_5").Cells.Item(pVal.Row).Specific.string
                                        Dim sToDate As String = oMatrixMachine.Columns.Item("V_2").Cells.Item(pVal.Row).Specific.string
                                        Dim sFromTime As String = oMatrixMachine.Columns.Item("V_1").Cells.Item(pVal.Row).Specific.string
                                        Dim sToTime As String = oMatrixMachine.Columns.Item("V_6").Cells.Item(pVal.Row).Specific.string
                                        Dim rsetEmpDets As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                        If sFromDate = String.Empty Then
                                            Return
                                        End If
                                        If sToDate = String.Empty Then
                                            Return
                                        End If
                                        If sFromTime = String.Empty Then
                                            Return
                                        End If
                                        If sToTime = String.Empty Then
                                            Return
                                        End If

                                        sQuery2 = "EXEC DBO.[@AIS_ProductionOrder_CalculateCycleTime]   '" + Format_StringToDate(sFromDate).ToString("yyyyMMdd") + "','" + Format_StringToDate(sToDate).ToString("yyyyMMdd") + "','" + Trim(sFromTime) + "','" + Trim(sToTime) + "'"
                                        rsetEmpDets.DoQuery(sQuery2)
                                        If rsetEmpDets.RecordCount > 0 Then
                                            Dim sQ As String = Trim(rsetEmpDets.Fields.Item(0).Value)
                                            oMatrixMachine.Columns.Item("V_7").Cells.Item(pVal.Row).Specific.string = sQ
                                        End If
                                    End If




                                    'oMatrix = frmProductionReceipt.Items.Item(pVal.ItemUID).Specific
                                    'If oMatrix.Columns.Item("V_6").Cells.Item(pVal.Row).Specific.string <> "" Then

                                    '    ITime = oMatrix.Columns.Item("V_1").Cells.Item(pVal.Row).Specific.string
                                    '    OTime = oMatrix.Columns.Item("V_6").Cells.Item(pVal.Row).Specific.string

                                    '    If CalculateTime(FormUID, pVal.ItemUID, ITime, OTime) <> "" Then
                                    '        If pVal.ItemUID = "17" Then
                                    '        ElseIf pVal.ItemUID = "18" Then
                                    '            If CalculateLabourCost(FormUID, oMatrix.Columns.Item("V_7").Cells.Item(pVal.Row).Specific.value, oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.string) <> "" Then
                                    '                oMatrix.Columns.Item("V_0").Cells.Item(pVal.Row).Specific.string = CDbl(oMatrix.Columns.Item("V_9").Cells.Item(pVal.Row).Specific.value) * TotLCost
                                    '                AddRow(FormUID, pVal.ItemUID)
                                    '            End If
                                    '        End If
                                    '    End If
                                    'End If
                                Catch ex As Exception
                                    objAddOn.objApplication.SetStatusBarMessage("iTEM EVENTS   Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                End Try
                            End If
                            If pVal.ItemUID = "52" And pVal.ColUID = "V_4" Then
                                oMatrix = frmProductionReceipt.Items.Item("52").Specific
                                If oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.string <> "" And pVal.Row = oMatrix.VisualRowCount Then
                                    objAddOn.SetNewLine(oMatrix, oDBDSBuy)
                                End If
                            End If


                            If pVal.ItemUID = "23" Then
                                Try
                                    '  Dim str = objAddOn.getSingleValue("Select U_OpCode from [@AS_WORD1] a where U_Status ='O' and LineId  not in (Select U_Process from [@AS_OPORD] where U_WODocE = a.DocEntry ) and a.DocEntry = '" & WODocEntry & "' and a.lineId ='" & CurrLineID & "' ")
                                    Dim str = objAddOn.getSingleValue("Select U_OpCode from [@AS_WORD1] a where U_Status ='O'   and a.DocEntry = '" & WODocEntry & "' and a.lineId ='" & CurrLineID & "' ")


                                    oMatrix = frmProductionReceipt.Items.Item("52").Specific
                                    oMatrix1 = frmProductionReceipt.Items.Item("16").Specific
                                    oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                    oRS.DoQuery(" SELECT isnull(U_ItmCde,'')'Item',U_BQty FROM [@AS_PRN4] WHERE Code = '" & str & "'")
                                    oRS.MoveFirst()
                                    Dim no = oRS.RecordCount
                                    For i As Integer = 1 To oRS.RecordCount
                                        Dim Bags = CDbl(frmProductionReceipt.Items.Item("23").Specific.string) * 1000
                                        Dim perbag = objAddOn.getSingleValue("select isnull(U_UoMKGs,0)'KGs' from oitm where ItemCode='" & oRS.Fields.Item("Item").Value & "' ")
                                        oMatrix1.Columns.Item("V_2").Cells.Item(i).Specific.value = Math.Round((CDbl(Bags) / CDbl(perbag)), 2)
                                        oRS.MoveNext()
                                    Next



                                    Dim Ton = frmProductionReceipt.Items.Item("23").Specific.value
                                    Dim KGs = CDbl(Ton) * 1000
                                    Dim oRS1 As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

                                    oRS1.DoQuery("select U_ItmCde,U_WillPerc from [@AS_PRN3] WHERE Code = '" & str & "'")

                                    Dim no1 = oRS1.RecordCount
                                    For j As Integer = 1 To oRS1.RecordCount
                                        oMatrix.Columns.Item("V_-1").Cells.Item(j).Specific.value = j
                                        Dim tst = oRS1.Fields.Item("U_ItmCde").Value
                                        If oMatrix.Columns.Item("V_4").Cells.Item(j).Specific.value = oRS1.Fields.Item("U_ItmCde").Value Then
                                            oMatrix.Columns.Item("WillOP").Cells.Item(j).Specific.value = oRS1.Fields.Item("U_WillPerc").Value
                                            Dim willperc = oRS1.Fields.Item("U_WillPerc").Value
                                            Dim WillQty = ((CDbl(willperc) / 100) * (CDbl(KGs)))
                                            oMatrix.Columns.Item("WillQty").Cells.Item(j).Specific.value = Math.Round(WillQty, 2)
                                        End If
                                        oRS1.MoveNext()
                                    Next



                                Catch ex As Exception
                                    objAddOn.objApplication.SetStatusBarMessage("iTEM EVENTS   Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                End Try
                            End If




                            If pVal.ItemUID = "52" And pVal.ColUID = "V_2" Then
                                Try
                                    oMatrix = frmProductionReceipt.Items.Item("52").Specific
                                    oMatrix1 = frmProductionReceipt.Items.Item("16").Specific
                                    Dim Ton = frmProductionReceipt.Items.Item("23").Specific.value
                                    Dim KGs = CDbl(Ton) * 1000
                                    Dim Itm = oMatrix1.Columns.Item("V_4").Cells.Item(1).Specific.value
                                    Dim perbag = objAddOn.getSingleValue("select isnull(U_UoMKGs,0)'KGs' from oitm where ItemCode='" & Itm & "' ")
                                    Dim Qty = CDbl(KGs) / CDbl(perbag)
                                    Dim exact = oMatrix.Columns.Item("V_2").Cells.Item(pVal.Row).Specific.value
                                    Dim exactperc = (CDbl(exact) / (CDbl(KGs) / 100))
                                    oMatrix.Columns.Item("ExctOP").Cells.Item(pVal.Row).Specific.value = exactperc

                                    Dim value = oMatrix.Columns.Item("WillQty").Cells.Item(pVal.Row).Specific.value

                                    oMatrix.Columns.Item("Diff").Cells.Item(pVal.Row).Specific.value = Math.Round((exact - value), 2)

                                Catch ex As Exception
                                    objAddOn.objApplication.SetStatusBarMessage("iTEM EVENTS   Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                End Try
                            End If


                            'If pVal.ItemUID = "16" And pVal.ColUID = "V_4" Then
                            '    Try
                            '        Dim itm = oMatrix1.Columns.Item("V_4").Cells.Item(oMatrix1.VisualRowCount - 1).Specific.value
                            '        Dim cost = objAddOn.getSingleValue("select avgprice from oitw where ItemCode ='" & itm & "'")
                            '        oForm.Items.Item("30").Specific.value = cost
                            '    Catch ex As Exception

                            '    End Try

                            'End If

                            If pVal.ItemUID = "16" And pVal.ColUID = "V_4" Then
                                Dim itm = oMatrix.Columns.Item("V_4").Cells.Item(oMatrix1.VisualRowCount - 1).Specific.value
                                Dim cost = objAddOn.getSingleValue("select avgprice from oitw where ItemCode ='" & itm & "'")
                                frmProductionReceipt.Items.Item("30").Specific.value = cost
                            End If
                            If pVal.ItemUID = "16" Then
                                QuantityHeaderCalc(FormUID, "16", "49")
                                'AddRow(FormUID, pVal.ItemUID)
                            End If

                            If pVal.ItemUID = "52" Then
                                BuyPdtQtyHeaderCalc(FormUID, "52", "72")
                                BuyPdtQtyHeaderCalcNetWeight(FormUID, "52", "t_NWT")

                                ' AddRow(FormUID, pVal.ItemUID)
                            End If


                            If pVal.ItemUID = "16" And pVal.ColUID = "V_9" Then
                                Dim bal As Double
                                oMatrix = frmProductionReceipt.Items.Item("16").Specific
                                If oMatrix.GetCellSpecific("V_9", pVal.Row).value <> "" Then
                                    Dim a = oMatrix.GetCellSpecific("V_2", pVal.Row).value
                                    Dim b = oMatrix.GetCellSpecific("V_9", pVal.Row).value
                                    bal = oMatrix.GetCellSpecific("V_9", pVal.Row).value - oMatrix.GetCellSpecific("V_2", pVal.Row).value
                                    oMatrix.Columns.Item("V_10").Cells.Item(pVal.Row).Specific.value = bal
                                Else
                                    bal = oMatrix.GetCellSpecific("V_2", pVal.Row).value
                                    oMatrix.Columns.Item("V_10").Cells.Item(pVal.Row).Specific.value = bal
                                End If

                            End If


                            Dim sQuery As String = String.Empty
                            If pVal.ItemUID = "28" Then
                                Dim oMatrixMachine As SAPbouiCOM.Matrix

                                oMatrixMachine = frmProductionReceipt.Items.Item("28").Specific
                                Dim sFromDate As String = oMatrixMachine.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.string
                                Dim sToDate As String = oMatrixMachine.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.string
                                Dim sFromTime As String = oMatrixMachine.Columns.Item("V_5").Cells.Item(pVal.Row).Specific.string
                                Dim sToTime As String = oMatrixMachine.Columns.Item("V_6").Cells.Item(pVal.Row).Specific.string
                                Dim rsetEmpDets As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                If sFromDate = String.Empty Then
                                    Return
                                End If
                                If sToDate = String.Empty Then
                                    Return
                                End If
                                If sFromTime = String.Empty Then
                                    Return
                                End If
                                If sToTime = String.Empty Then
                                    Return
                                End If

                                sQuery = "EXEC DBO.[@AIS_ProductionOrder_CalculateCycleTime]   '" + Format_StringToDate(sFromDate).ToString("yyyyMMdd") + "','" + Format_StringToDate(sToDate).ToString("yyyyMMdd") + "','" + Trim(sFromTime) + "','" + Trim(sToTime) + "'"
                                rsetEmpDets.DoQuery(sQuery)
                                If rsetEmpDets.RecordCount > 0 Then
                                    Dim sQ As String = Trim(rsetEmpDets.Fields.Item(0).Value)
                                    oMatrixMachine.Columns.Item("V_0").Cells.Item(pVal.Row).Specific.string = sQ
                                End If
                            End If


                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("iTEM EVENTS   Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK

                        If pVal.ItemUID = "17" And pVal.ColUID = "V_4" Then
                            oMatrix = frmProductionReceipt.Items.Item("17").Specific
                            Dim prntItem As String = frmProductionReceipt.Items.Item("6").Specific.value
                            Dim whs As String = frmProductionReceipt.Items.Item("32").Specific.value
                            Dim mchncode As String = oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value
                            ' Dim dynCode As String = DateTime.Now().ToString("yyMMddhhmmss")
                            If mchncode <> "" And frmProductionReceipt.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                objRmEntry.LoadScreen(mchncode, dyncode, prntItem, whs)
                            End If
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED

                        frmProductionReceipt.Items.Item("btnCal").Enabled = True
                        If pVal.ItemUID = "btnCal" Then
                            Dim WoNum As String = frmProductionReceipt.Items.Item("54").Specific.string
                            Dim rotcod As String = frmProductionReceipt.Items.Item("56").Specific.string

                            objAddOn.objBuyProduct.LoadScreen(WoNum, rotcod)

                        End If

                End Select

                'If pVal.ItemUID = "1" And oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                '    Try
                '        If pVal.ActionSuccess = True Then
                '            Select Case pVal.EventType
                '                Case SAPbouiCOM.BoEventTypes.et_CLICK
                '                    Dim GI = objAddOn.getSingleValue("select Max(docentry) from OIGE")
                '                    Dim Total = objAddOn.getSingleValue("select (sum(Quantity)*sum(StockPrice))'Cost' from ige1 where docentry='" & GI & "'")
                '                    Dim PReceiptEntry = objAddOn.getSingleValue("select max(docentry)+1 from [@AS_OIGN]")
                '                    Dim Qty = objAddOn.getSingleValue("select sum(U_qty ) from [@AS_IGN5] where docentry='" & PReceiptEntry & "'")
                '                    Dim PerCost = Total / Qty
                '                    Dim SQL As String = "update [@AS_OIGN] set U_cost ='" & PerCost & "' where  docentry='" & PReceiptEntry & "'"
                '                    oRS.DoQuery(SQL)
                '                    oRecSet = objAddOn.DoQuery("select U_WillPerc,U_ExctOP  from [@AS_IGN5] where DocEntry='" & PReceiptEntry & "'")
                '                    For i As Integer = 1 To oRecSet.RecordCount
                '                        Dim willperc = oRecSet.Fields.Item("U_WillPerc").Value
                '                        Dim Exctop = oRecSet.Fields.Item("U_ExctOP").Value
                '                        Dim sql1 = objAddOn.DoQuery("Update [@AS_IGN5] set U_WillVal=(('" & willperc & "'/100)* '" & PerCost & "') where docentry=='" & PReceiptEntry & "' and LineId='" & i & "' ")
                '                        Dim sql2 = objAddOn.DoQuery("Update [@AS_IGN5] set U_ExtOPVal=(('" & Exctop & "'/100)* '" & PerCost & "') where docentry=='" & PReceiptEntry & "' and LineId='" & i & "' ")
                '                    Next
                '            End Select
                '        End If
                '    Catch ex As Exception
                '    End Try
                'End If
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("iTEM EVENTS   Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
    Public Sub ByProduct(ByVal OpCode As String, pRouteCode As String, pFGCode As String, pFGName As String, sOperationType As String, pLineID As String, PWorkORderDocEntry As String)
        Try
            Dim i As Integer
            Dim oComb As SAPbouiCOM.ComboBox
            oMatrix = frmProductionReceipt.Items.Item("52").Specific
            oMatrix.Clear()


            Dim oRS_RouteDetails As SAPbobsCOM.Recordset
            oRS_RouteDetails = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS_RouteDetails.DoQuery("Select U_WhsCode,U_WhsName  from [@AIS_WRKTYP]  Where code  = '" & sOperationType & "'")
            oRS_RouteDetails.MoveFirst()
            Dim sWharehouseCode As String = String.Empty
            Dim sWharehouseName As String = String.Empty
            For i = 1 To oRS_RouteDetails.RecordCount
                sWharehouseCode = oRS_RouteDetails.Fields.Item("U_WhsCode").Value
                sWharehouseName = oRS_RouteDetails.Fields.Item("U_WhsName").Value
            Next

            Dim oRS As SAPbobsCOM.Recordset
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim squery As String = "EXEC DBO.[@AIS_Production_ProductionReceipt_GetFGItemDetails] '" & OpCode & "','" & sOperationType & "','" + pLineID + "','" + PWorkORderDocEntry + "'"
            oRS.DoQuery(squery)
            oRS.MoveFirst()
            For i = 1 To oRS.RecordCount
                oMatrix.AddRow()
                If pFGCode <> "" Then
                    oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
                    oMatrix.Columns.Item("V_4").Cells.Item(oMatrix.VisualRowCount).Specific.value = oRS.Fields.Item("U_FgCode").Value
                    oMatrix.Columns.Item("V_3").Cells.Item(oMatrix.VisualRowCount).Specific.value = oRS.Fields.Item("Desc").Value
                    oMatrix.Columns.Item("Uom").Cells.Item(oMatrix.VisualRowCount).Specific.value = objAddOn.getSingleValue("select InvntryUom from oitm where ItemCode='" & oRS.Fields.Item("U_FgCode").Value & "'")
                    oMatrix.Columns.Item("WCode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oRS.Fields.Item("U_Whs").Value
                    oMatrix.Columns.Item("WName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oRS.Fields.Item("U_WName").Value
                    oMatrix.Columns.Item("BNo").Cells.Item(oMatrix.VisualRowCount).Specific.value = "Clik Link"

                    oComb = oMatrix.Columns.Item("V_10").Cells.Item(oMatrix.VisualRowCount).Specific
                    Dim val As String
                    val = oRS.Fields.Item("U_Type").Value
                    oComb.Select(val, SAPbouiCOM.BoSearchKey.psk_ByValue)
                End If

                oRS.MoveNext()
            Next
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Load ByProduct Failed: " & ex.Message & "...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

        End Try

    End Sub
    Public Sub FormEnable()
        Try

            frmProductionReceipt.Freeze(True)
            If ProdType = 2 Then


                frmProductionReceipt.Items.Item("41").Visible = True
                frmProductionReceipt.Items.Item("42").Visible = True


                frmProductionReceipt.Items.Item("16").Enabled = False
                frmProductionReceipt.Items.Item("18").Enabled = False
                frmProductionReceipt.Items.Item("28").Enabled = False
                frmProductionReceipt.Items.Item("14").Click()
                frmProductionReceipt.Items.Item("Fol_FG").Visible = False
                frmProductionReceipt.Items.Item("53").Visible = False
                frmProductionReceipt.Items.Item("54").Visible = False
                frmProductionReceipt.Items.Item("55").Visible = False
                frmProductionReceipt.Items.Item("56").Visible = False
                frmProductionReceipt.Items.Item("57").Visible = False
                frmProductionReceipt.Items.Item("58").Visible = False
                frmProductionReceipt.Items.Item("59").Visible = False
                frmProductionReceipt.Items.Item("60").Visible = False
                frmProductionReceipt.Items.Item("61").Visible = False
                frmProductionReceipt.Items.Item("62").Visible = False


            ElseIf ProdType = 1 Then

                frmProductionReceipt.Items.Item("53").Visible = True
                frmProductionReceipt.Items.Item("54").Visible = True
                frmProductionReceipt.Items.Item("55").Visible = True
                frmProductionReceipt.Items.Item("56").Visible = True
                frmProductionReceipt.Items.Item("57").Visible = True
                frmProductionReceipt.Items.Item("58").Visible = True
                frmProductionReceipt.Items.Item("59").Visible = True
                frmProductionReceipt.Items.Item("60").Visible = True
                frmProductionReceipt.Items.Item("Fol_FG").Visible = True



                frmProductionReceipt.Items.Item("16").Enabled = True
                frmProductionReceipt.Items.Item("18").Enabled = True
                frmProductionReceipt.Items.Item("28").Enabled = True



                frmProductionReceipt.Items.Item("48").Visible = False

                oMatrix = frmProductionReceipt.Items.Item("16").Specific
                oMatrix.Columns.Item("V_7").Visible = False
                oMatrix = frmProductionReceipt.Items.Item("17").Specific


                oMatrix.Columns.Item("V_11").Visible = False
                oMatrix.Columns.Item("V_15").Visible = False
                oMatrix.Columns.Item("V_14").Visible = False
                oMatrix.Columns.Item("V_8").Visible = False
                oMatrix = frmProductionReceipt.Items.Item("18").Specific
                oMatrix.Columns.Item("V_8").Visible = False


            Else

                frmProductionReceipt.Items.Item("53").Visible = True
                frmProductionReceipt.Items.Item("54").Visible = True
                frmProductionReceipt.Items.Item("55").Visible = True
                frmProductionReceipt.Items.Item("56").Visible = True
                frmProductionReceipt.Items.Item("57").Visible = True
                frmProductionReceipt.Items.Item("58").Visible = True
                frmProductionReceipt.Items.Item("59").Visible = True
                frmProductionReceipt.Items.Item("60").Visible = True
                frmProductionReceipt.Items.Item("61").Visible = True
                frmProductionReceipt.Items.Item("62").Visible = True
                frmProductionReceipt.Items.Item("Fol_FG").Visible = True
                frmProductionReceipt.Items.Item("39").Visible = False
                frmProductionReceipt.Items.Item("40").Visible = False
                frmProductionReceipt.Items.Item("41").Visible = False
                frmProductionReceipt.Items.Item("42").Visible = False

                frmProductionReceipt.Items.Item("16").Enabled = True
                frmProductionReceipt.Items.Item("18").Enabled = True
                frmProductionReceipt.Items.Item("28").Enabled = True


                frmProductionReceipt.Items.Item("47").Visible = False
                frmProductionReceipt.Items.Item("48").Visible = False

                oMatrix = frmProductionReceipt.Items.Item("16").Specific
                oMatrix.Columns.Item("V_7").Visible = False
                oMatrix = frmProductionReceipt.Items.Item("17").Specific
                oMatrix.Columns.Item("V_13").Visible = False
                oMatrix.Columns.Item("V_12").Visible = False
                oMatrix.Columns.Item("V_11").Visible = False
                oMatrix.Columns.Item("V_15").Visible = False
                oMatrix.Columns.Item("V_14").Visible = False
                oMatrix.Columns.Item("V_8").Visible = False
                oMatrix = frmProductionReceipt.Items.Item("18").Specific
                oMatrix.Columns.Item("V_8").Visible = False


                frmProductionReceipt.Items.Item("63").Visible = True

                frmProductionReceipt.Items.Item("68").Visible = True
                frmProductionReceipt.Items.Item("69").Visible = True
                frmProductionReceipt.Items.Item("70").Visible = True
                frmProductionReceipt.Items.Item("71").Visible = True
                frmProductionReceipt.Items.Item("72").Visible = True

            End If

        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("FormEnable   Failed: " & ex.Message & "...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmProductionReceipt.Freeze(False)
        End Try

    End Sub
    Private Function LoadRM(ByVal FormUID As String) As Boolean
        Try
            Dim i As Integer
            frmProductionReceipt = objAddOn.objApplication.Forms.Item(FormUID)
            oMatrix = frmProductionReceipt.Items.Item("16").Specific
            oMatrix.Clear()
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select U_itemcode,U_itemname,U_whscode,U_dyncode,U_mchncode,U_qty,U_remarks,U_batch from [@AS_RMEN] where U_dyncode='" & dyncode & "'")
            oRS.MoveFirst()
            For i = 1 To oRS.RecordCount
                oMatrix.AddRow()
                oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value = oRS.Fields.Item("U_itemcode").Value
                oMatrix.Columns.Item("V_3").Cells.Item(i).Specific.value = oRS.Fields.Item("U_itemname").Value
                oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.value = oRS.Fields.Item("U_whscode").Value
                oMatrix.Columns.Item("V_2").Cells.Item(i).Specific.value = oRS.Fields.Item("U_qty").Value
                oMatrix.Columns.Item("V_7").Cells.Item(i).Specific.value = oRS.Fields.Item("U_mchncode").Value
                oMatrix.Columns.Item("V_6").Cells.Item(i).Specific.value = oRS.Fields.Item("U_qty").Value
                oMatrix.Columns.Item("V_8").Cells.Item(i).Specific.value = oRS.Fields.Item("U_batch").Value

                oRS.MoveNext()
            Next
            Return True

        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Load RM Failed: " & ex.Message & "...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End Try
    End Function
    Private Function LoadLabour(ByVal FormUID As String) As Boolean
        Try
            Dim i As Integer
            frmProductionReceipt = objAddOn.objApplication.Forms.Item(FormUID)
            oMatrix = frmProductionReceipt.Items.Item("18").Specific
            oMatrix.Clear()
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("SELECT T0.[U_lbrcode], T0.[U_lbrname], T0.[U_mchncode], T0.[U_dyncode], T0.[U_nooflbr], convert(nvarchar(12),T0.[U_fdate],103) U_fdate,  convert(nvarchar(12),T0.[U_tdate],103) U_tdate, T0.[U_fromtime], " &
                        " T0.[U_totime], T0.[U_tothrs], T0.[U_totcost] FROM [dbo].[@AS_PLBR] T0 where T0.U_dyncode='" & dyncode & "'")
            oRS.MoveFirst()
            For i = 1 To oRS.RecordCount
                oMatrix.AddRow()
                Dim fdate As String = oRS.Fields.Item("U_fdate").Value
                oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value = oRS.Fields.Item("U_lbrcode").Value
                oMatrix.Columns.Item("V_3").Cells.Item(i).Specific.value = oRS.Fields.Item("U_lbrname").Value
                oMatrix.Columns.Item("V_5").Cells.Item(i).Specific.string = fdate 'oRS.Fields.Item("U_fdate").Value
                oMatrix.Columns.Item("V_2").Cells.Item(i).Specific.string = oRS.Fields.Item("U_tdate").Value
                oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.value = oRS.Fields.Item("U_fromtime").Value
                oMatrix.Columns.Item("V_6").Cells.Item(i).Specific.value = oRS.Fields.Item("U_totime").Value
                oMatrix.Columns.Item("V_7").Cells.Item(i).Specific.value = oRS.Fields.Item("U_tothrs").Value
                oMatrix.Columns.Item("V_0").Cells.Item(i).Specific.value = oRS.Fields.Item("U_totcost").Value
                oMatrix.Columns.Item("V_8").Cells.Item(i).Specific.value = oRS.Fields.Item("U_mchncode").Value
                oMatrix.Columns.Item("V_9").Cells.Item(i).Specific.value = oRS.Fields.Item("U_nooflbr").Value

                oRS.MoveNext()
            Next
            Return True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Load Labours Failed: " & ex.Message & "...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End Try
    End Function
    Private Function LoadIdleTime(ByVal FormUID As String) As Boolean
        Try
            Dim i As Integer
            frmProductionReceipt = objAddOn.objApplication.Forms.Item(FormUID)
            oMatrix = frmProductionReceipt.Items.Item("28").Specific
            oMatrix.Clear()
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("SELECT T0.[U_mchncode], T0.[U_idltime], T0.[U_reason] FROM [dbo].[@AS_IDLE]  T0 where T0.U_dyncode='" & dyncode & "'")
            oRS.MoveFirst()
            For i = 1 To oRS.RecordCount
                oMatrix.AddRow()
                oMatrix.Columns.Item("V_2").Cells.Item(i).Specific.value = oRS.Fields.Item("U_mchncode").Value
                oMatrix.Columns.Item("V_0").Cells.Item(i).Specific.value = oRS.Fields.Item("U_idltime").Value
                oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.value = oRS.Fields.Item("U_reason").Value
                oRS.MoveNext()

            Next
            Return True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Load RM Failed: " & ex.Message & "...", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End Try
    End Function

#Region " Goods Receipt for Production Receipt"
    Public Function Goods_Receipt() As Boolean
        Dim PackingItemCost As Double = 0
        Dim GoodReceiptPrice As Double = 0
        Dim manBatch As String = ""
        Dim sCurrentDocNum As String = oDBDSHeader.GetValue("DocNum", 0)
        Try

            Dim oGoodsIssue As SAPbobsCOM.Documents


            oGoodsIssue = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit)
            oGoodsIssue.DocDate = objAddOn.DateConvertion(frmProductionReceipt.Items.Item("12").Specific.value)
            oGoodsIssue.DocDueDate = objAddOn.DateConvertion(frmProductionReceipt.Items.Item("12").Specific.value)
            oGoodsIssue.TaxDate = objAddOn.DateConvertion(frmProductionReceipt.Items.Item("12").Specific.value)

            'oGoodsIssue.UserFields.Fields.Item("U_BaseNum").Value = sCurrentDocNum
            'oGoodsIssue.UserFields.Fields.Item("U_BaseObject").Value = "MNU_OIGN"
            'oGoodsIssue.UserFields.Fields.Item("U_Series").Value = oDBDSHeader.GetValue("Series", 0)
            'oGoodsIssue.UserFields.Fields.Item("U_BaseLine").Value = CurrLineID.ToString()
            'oGoodsIssue.UserFields.Fields.Item("U_WBaseNum").Value = frmProductionReceipt.Items.Item("54").Specific.string

            oGoodsIssue.Comments = "Based On Production Receipt "

            oMatrix = frmProductionReceipt.Items.Item("16").Specific
            oGoodsIssue.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items
            Dim j As Integer = 0
            For IntI As Integer = 1 To oMatrix.RowCount

                If oMatrix.Columns.Item("V_4").Cells.Item(IntI).Specific.string = "" Then

                Else
                    Dim sGetBatchScreenDocNum As String = oMatrix.Columns.Item("V_8").Cells.Item(IntI).Specific.value
                    Dim LineID As String = oMatrix.Columns.Item("V_-1").Cells.Item(IntI).Specific.value
                    Dim DocNum As String = frmProductionReceipt.Items.Item("10").Specific.value
                    'Dim sGetBatchScreenDocNum  = objAddOn.getSingleValue("select top 1  DocNum from [@ais_RVL_OBSN] where U_SubGrnNo='" & DocNum & "' and U_lineid ='" + LineID + "' Order by DocNum Desc")

                    Dim sQty As Double = 0
                    Dim sBatchQuery2 As String = "select sum(Convert(decimal(18,5), U_Qty) ) as Qty    from [@ais_RVL_OBSN] m left outer join [@ais_rvl_bsn1] d on m.DocEntry =d.DocEntry where DocNum ='" & sGetBatchScreenDocNum & "' and U_ItemCode ='" & oMatrix.GetCellSpecific("V_4", IntI).Value & "'"
                    Dim BatchNoall2 As SAPbobsCOM.Recordset = objAddOn.DoQuery(sBatchQuery2)
                    For Bcount As Integer = 0 To BatchNoall2.RecordCount - 1
                        sQty = sQty + Convert.ToDouble(BatchNoall2.Fields.Item("Qty").Value.ToString.Trim())
                        BatchNoall2.MoveNext()
                    Next

                    oGoodsIssue.Lines.ItemCode = oMatrix.Columns.Item("V_4").Cells.Item(IntI).Specific.string
                    oGoodsIssue.Lines.Quantity = sQty
                    oGoodsIssue.Lines.WarehouseCode = oMatrix.Columns.Item("V_1").Cells.Item(IntI).Specific.string
                    manBatch = FindIsBatchItem(oMatrix.Columns.Item("V_4").Cells.Item(IntI).Specific.string)
                    If manBatch = "Y" Then

                        Dim sBatchQuery As String = "select U_BatSerNo ,U_Qty  from [@ais_RVL_OBSN] m left outer join [@ais_rvl_bsn1] d on m.DocEntry =d.DocEntry where DocNum ='" & sGetBatchScreenDocNum & "' and U_ItemCode ='" & oMatrix.GetCellSpecific("V_4", IntI).Value & "'"
                        Dim BatchNoall As SAPbobsCOM.Recordset = objAddOn.DoQuery(sBatchQuery)
                        For Bcount As Integer = 0 To BatchNoall.RecordCount - 1
                            If (BatchNoall.Fields.Item("U_BatSerNo").Value.ToString.Trim <> "") Then
                                oGoodsIssue.Lines.BatchNumbers.BatchNumber = BatchNoall.Fields.Item("U_BatSerNo").Value.ToString.Trim
                                oGoodsIssue.Lines.BatchNumbers.Quantity = BatchNoall.Fields.Item("U_Qty").Value.ToString.Trim
                            End If
                            oGoodsIssue.Lines.BatchNumbers.Add()
                            BatchNoall.MoveNext()
                        Next



                    End If

                    oGoodsIssue.Lines.Add()
                    j = +1
                End If

            Next
            If j > 0 Then
                lretcode = oGoodsIssue.Add()
                If lretcode <> 0 Then
                    objAddOn.objApplication.SetStatusBarMessage("Goods Issue Failure - " & objAddOn.objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Else
                    objAddOn.objCompany.GetNewObjectCode(IssueNum)
                    frmProductionReceipt.Items.Item("t_GIEntry").Specific.string = IssueNum
                    Dim GoodsissueDocNum As String = objAddOn.getSingleValue("select DocNum from OIGE where DocEntry ='" & IssueNum & "'")
                    GoodReceiptPrice = objAddOn.getSingleValue("[@AIS_ProductionIssueReceipt_JournalEntryPrice] '" & GoodsissueDocNum & "'")

                End If
            Else
                lretcode = 0
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Goods Issue Failure - " & objAddOn.objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End Try
        If lretcode <> 0 Then
            objAddOn.objApplication.SetStatusBarMessage("Goods Issue Failure - " & objAddOn.objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        Else
            Try

                objAddOn.objApplication.SetStatusBarMessage("Goods Issue Process Completed1 ", SAPbouiCOM.BoMessageTime.bmt_Short, True)


                'frmProductionReceipt.Items.Item("37").Specific.value = IssueNum
                PackingItemCost = FindStockPrice(frmProductionReceipt.UniqueID, IssueNum)
                objAddOn.objApplication.SetStatusBarMessage("Please Wait while Document Posting.....", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                Dim oGoodsReceipt As SAPbobsCOM.Documents
                oGoodsReceipt = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenEntry)
                oGoodsReceipt.DocDate = objAddOn.DateConvertion(frmProductionReceipt.Items.Item("12").Specific.value)
                oGoodsReceipt.DocDueDate = objAddOn.DateConvertion(frmProductionReceipt.Items.Item("12").Specific.value)
                oGoodsReceipt.TaxDate = objAddOn.DateConvertion(frmProductionReceipt.Items.Item("12").Specific.value)
                oGoodsReceipt.Comments = "Based On Production Receipt DocNum   "
                'oGoodsReceipt.UserFields.Fields.Item("U_BaseNum").Value = sCurrentDocNum
                'oGoodsReceipt.UserFields.Fields.Item("U_BaseObject").Value = "MNU_OIGN"
                'oGoodsReceipt.UserFields.Fields.Item("U_Series").Value = oDBDSHeader.GetValue("Series", 0)
                'oGoodsReceipt.UserFields.Fields.Item("U_BaseLine").Value = CurrLineID.ToString()
                'oGoodsReceipt.UserFields.Fields.Item("U_WBaseNum").Value = frmProductionReceipt.Items.Item("54").Specific.string
                oGoodsReceipt.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Items
                objAddOn.objApplication.SetStatusBarMessage("Goods Issue Process Completed2  ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Dim LineTotal As Double = CDbl(CDbl(frmProductionReceipt.Items.Item("49").Specific.value) + PackingItemCost + CDbl(frmProductionReceipt.Items.Item("72").Specific.value))
                oMatrix = frmProductionReceipt.Items.Item("52").Specific
                Dim i, j As Integer
                j = 0
                For i = 1 To oMatrix.VisualRowCount
                    If 1 > 1 Then


                    Else
                        objAddOn.objApplication.SetStatusBarMessage("Goods Issue Process Completed3  ", SAPbouiCOM.BoMessageTime.bmt_Short, True)


                        If Not oMatrix.Columns.Item("wst").Cells.Item(i).Specific.string = "Y" Then
                            Dim ItemQty As Double = oMatrix.Columns.Item("qty").Cells.Item(i).Specific.string
                            If ItemQty > 0 Then
                                objAddOn.objApplication.SetStatusBarMessage("Goods Issue Process Completed4 ", SAPbouiCOM.BoMessageTime.bmt_Short, True)


                                Dim sItemCode As String = oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.string
                                oGoodsReceipt.Lines.ItemCode = sItemCode
                                oGoodsReceipt.Lines.Quantity = oMatrix.Columns.Item("qty").Cells.Item(i).Specific.string

                                Dim oMatrix_Machine As SAPbouiCOM.Matrix
                                oMatrix_Machine = frmProductionReceipt.Items.Item("28").Specific
                                Dim strInspectionRequired As Boolean = False
                                For IntI As Integer = 1 To oMatrix_Machine.RowCount
                                    Dim oCmbType As SAPbouiCOM.ComboBox = oMatrix_Machine.Columns.Item("Type").Cells.Item(IntI).Specific
                                    If Not oCmbType.Selected Is Nothing Then
                                        If oCmbType.Selected.Value = "M" Or oCmbType.Selected.Value = "B" Then
                                            strInspectionRequired = True
                                            Exit For
                                        End If
                                    End If
                                Next


                                Dim sProInspectionRequired = objAddOn.getSingleValue(" select Isnull(U_InspReq,'N')  from [@AIS_WRKTYP] Where   Code ='" & WorkOrderOperationType & "' ")

                                If strInspectionRequired = True And sProInspectionRequired = "F" Then

                                    Dim sBatchQuery As String = "[@AIS_Production_ProductionReceipt_GetFGItemDetailsInspection]  '" & sItemCode & "','" & WorkOrderOperationType & "'"
                                    Dim BatchNoall As SAPbobsCOM.Recordset = objAddOn.DoQuery(sBatchQuery)
                                    For Bcount As Integer = 0 To BatchNoall.RecordCount - 1
                                        oGoodsReceipt.Lines.WarehouseCode = BatchNoall.Fields.Item("WhsCode").Value.ToString.Trim
                                    Next
                                Else
                                    oGoodsReceipt.Lines.WarehouseCode = oMatrix.Columns.Item("WCode").Cells.Item(i).Specific.string
                                End If




                                oGoodsReceipt.Lines.LineTotal = GoodReceiptPrice

                                manBatch = FindIsBatchItem(oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.string)
                                If manBatch = "Y" Then
                                    objAddOn.objApplication.SetStatusBarMessage("Goods Issue Process Completed5 ", SAPbouiCOM.BoMessageTime.bmt_Short, True)


                                    If oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.string <> String.Empty Then
                                        objAddOn.objApplication.SetStatusBarMessage("Goods Issue Process Completed6 ", SAPbouiCOM.BoMessageTime.bmt_Short, True)


                                        oGoodsReceipt.Lines.BatchNumbers.BatchNumber = oMatrix.Columns.Item("V_1").Cells.Item(i).Specific.string
                                        oGoodsReceipt.Lines.BatchNumbers.Quantity = oMatrix.Columns.Item("qty").Cells.Item(i).Specific.string
                                        oGoodsReceipt.Lines.BatchNumbers.Add()
                                    Else
                                        objAddOn.objApplication.SetStatusBarMessage("Goods Issue Process Completed7 ", SAPbouiCOM.BoMessageTime.bmt_Short, True)


                                        'Dim oMatrixBatchDetails As SAPbouiCOM.Matrix = frmProductionReceipt.Items.Item("submatrix").Specific
                                        'For IntBatchRowCount As Integer = 1 To oMatrixBatchDetails.VisualRowCount
                                        '    Dim UID As String = oMatrixBatchDetails.Columns.Item("UID").Cells.Item(IntBatchRowCount).Specific.string
                                        '    Dim BatchsNo As String = oMatrixBatchDetails.Columns.Item("Qty").Cells.Item(IntBatchRowCount).Specific.string
                                        '    Dim TName As String = oMatrixBatchDetails.Columns.Item("TName").Cells.Item(IntBatchRowCount).Specific.string
                                        '    If UID = i And BatchsNo <> "Clik Link" Then
                                        oGoodsReceipt.Lines.BatchNumbers.BatchNumber = oMatrix.Columns.Item("BNo").Cells.Item(i).Specific.string
                                        oGoodsReceipt.Lines.BatchNumbers.Quantity = oMatrix.Columns.Item("qty").Cells.Item(i).Specific.string
                                        oGoodsReceipt.Lines.BatchNumbers.Add()
                                        'End If
                                        '    Next
                                        objAddOn.objApplication.SetStatusBarMessage("Goods Issue Process Completed8 ", SAPbouiCOM.BoMessageTime.bmt_Short, True)

                                    End If

                                End If
                                oGoodsReceipt.Lines.Add()

                            End If
                            j = +1


                        End If
                    End If

                Next
                objAddOn.objApplication.SetStatusBarMessage("Goods Issue Process Completeds ", SAPbouiCOM.BoMessageTime.bmt_Short, True)


                lretcode = oGoodsReceipt.Add



                If lretcode <> 0 Then
                    objAddOn.objApplication.SetStatusBarMessage("Goods Receipt Failure - " & objAddOn.objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                Else
                    Dim GoodsReceiptNo As Integer
                    objAddOn.objCompany.GetNewObjectCode(GoodsReceiptNo)
                    frmProductionReceipt.Items.Item("t_GREntry").Specific.string = GoodsReceiptNo

                End If

            Catch ex As Exception
                objAddOn.objApplication.SetStatusBarMessage("Main Exception Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
                Exit Function
            End Try
            objAddOn.objApplication.SetStatusBarMessage("Document Added Successfully ", SAPbouiCOM.BoMessageTime.bmt_Short, False)

            Return True
        End If
    End Function
#End Region

#Region "To Load the Values from Goods Issue Documents"
    Public Sub LoadValues(ByVal FormUID As String, ByVal DocNum As String)
        Try
            If DocNum <> "" Then
                oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                oRS.DoQuery("select U_itemcode ,U_itemname ,U_invuom ,U_qty ,U_instock ,U_whscode ,T1.U_remarks  from [@AS_OIGE] T0 " & _
                            " left join [@AS_IGE1] T1 on t0.DocEntry =t1.DocEntry where T0.DocNum =" & DocNum & "")
                oMatrix = frmProductionReceipt.Items.Item("16").Specific
                oMatrix.Columns.Item("V_3").Editable = True
                oMatrix.Columns.Item("V_5").Editable = True

                For IntI As Integer = 1 To oRS.RecordCount
                    oMatrix.AddRow()
                    oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
                    oMatrix.Columns.Item("V_4").Cells.Item(IntI).Specific.string = oRS.Fields.Item("U_itemcode").Value
                    oMatrix.Columns.Item("V_3").Cells.Item(IntI).Specific.value = oRS.Fields.Item("U_itemname").Value
                    oMatrix.Columns.Item("V_5").Cells.Item(IntI).Specific.value = oRS.Fields.Item("U_invuom").Value
                    oMatrix.Columns.Item("V_2").Cells.Item(IntI).Specific.value = oRS.Fields.Item("U_qty").Value
                    oMatrix.Columns.Item("V_6").Cells.Item(IntI).Specific.value = oRS.Fields.Item("U_instock").Value
                    oMatrix.Columns.Item("V_1").Cells.Item(IntI).Specific.string = oRS.Fields.Item("U_whscode").Value
                    oMatrix.Columns.Item("V_0").Cells.Item(IntI).Specific.value = oRS.Fields.Item("U_remarks").Value
                    frmProductionReceipt.DataSources.DBDataSources.Item("@AS_IGN1").Clear()

                Next
                frmProductionReceipt.ActiveItem = 12
                frmProductionReceipt.Items.Item("14").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            End If


        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Values Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            frmProductionReceipt.Freeze(False)
            'oForm.Refresh()
            objAddOn.objApplication.SetStatusBarMessage("Load Values Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region

#Region "TO Change Editable Mode to Non-Editable Mode"
    Public Sub NonEditableMode()
        Try
            frmProductionReceipt.Items.Item("10").Enabled = False
            frmProductionReceipt.Items.Item("12").Enabled = False
            frmProductionReceipt.Items.Item("4").Enabled = False
            frmProductionReceipt.Items.Item("6").Enabled = False
            frmProductionReceipt.Items.Item("8").Enabled = False
            frmProductionReceipt.Items.Item("23").Enabled = False

            oMatrix = frmProductionReceipt.Items.Item("16").Specific
            For I As Integer = 1 To oMatrix.Columns.Count - 1
                oMatrix.Columns.Item("V_" & I - 1).Editable = False
            Next
            oMatrix = frmProductionReceipt.Items.Item("17").Specific
            For I As Integer = 1 To oMatrix.Columns.Count - 1
                oMatrix.Columns.Item("V_" & I - 1).Editable = False
            Next
            oMatrix = frmProductionReceipt.Items.Item("18").Specific
            For I As Integer = 1 To oMatrix.Columns.Count - 1
                oMatrix.Columns.Item("V_" & I - 1).Editable = False
            Next

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Non Editable Mode Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try


    End Sub
#End Region

#Region "Delete an Empty Row"

    Public Sub DeleteEmptyRow(ByVal FormUID As String)
        Try
            frmProductionReceipt = objAddOn.objApplication.Forms.Item(FormUID)
            oMatrix = frmProductionReceipt.Items.Item("16").Specific
            If oMatrix.VisualRowCount > 0 Then
                If oMatrix.Columns.Item("V_4").Cells.Item(oMatrix.VisualRowCount).Specific.value = "" Then
                    oMatrix.DeleteRow(oMatrix.VisualRowCount)
                End If
            End If


            oMatrix = frmProductionReceipt.Items.Item("17").Specific
            If oMatrix.VisualRowCount > 0 Then
                If oMatrix.Columns.Item("V_4").Cells.Item(oMatrix.VisualRowCount).Specific.value = "" Then
                    oMatrix.DeleteRow(oMatrix.VisualRowCount)
                End If
            End If


            oMatrix = frmProductionReceipt.Items.Item("18").Specific
            If oMatrix.VisualRowCount > 0 Then
                If oMatrix.Columns.Item("V_4").Cells.Item(oMatrix.VisualRowCount).Specific.value = "" Then
                    oMatrix.DeleteRow(oMatrix.VisualRowCount)
                End If
            End If


            oMatrix = frmProductionReceipt.Items.Item("28").Specific
            If oMatrix.VisualRowCount > 0 Then
                If oMatrix.Columns.Item("V_2").Cells.Item(oMatrix.VisualRowCount).Specific.value = "" Then
                    oMatrix.DeleteRow(oMatrix.VisualRowCount)
                End If

            End If

            oMatrix = frmProductionReceipt.Items.Item("52").Specific
            If oMatrix.VisualRowCount > 0 Then
                If oMatrix.Columns.Item("V_4").Cells.Item(oMatrix.VisualRowCount).Specific.value = "" Then
                    oMatrix.DeleteRow(oMatrix.VisualRowCount)
                End If
            End If



        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Delete an Empty Row Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub
#End Region

    Function Validate() As Boolean
        Try


            If frmProductionReceipt.Items.Item("54").Specific.String = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select WorkOrder No", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                Return False
            End If


            oMatrix = frmProductionReceipt.Items.Item("16").Specific
            objAddOn.DeleteEmptyRowInFormDataEvent(oMatrix, "V_4", oDBDSRM)
            If oMatrix.VisualRowCount > 0 Then
                Dim j As Integer
                For j = 1 To oMatrix.VisualRowCount
                    Dim iTEMcODE As String = oMatrix.Columns.Item("V_4").Cells.Item(j).Specific.value
                    If iTEMcODE <> String.Empty Then
                        If oMatrix.Columns.Item("V_2").Cells.Item(j).Specific.value = "0.0" Then
                            objAddOn.objApplication.SetStatusBarMessage("RM Tab: Item Quantity Should be Greater Than Zero", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                            Return False
                        End If
                        If oMatrix.Columns.Item("V_1").Cells.Item(j).Specific.string = "" Then
                            objAddOn.objApplication.SetStatusBarMessage("RM Tab: Please Select WareHouse", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                            Return False
                        End If

                        Dim manBatch = FindIsBatchItem(oMatrix.Columns.Item("V_4").Cells.Item(j).Specific.string)
                        If manBatch = "Y" Then
                            If oMatrix.Columns.Item("V_1").Cells.Item(j).Specific.string = "Link" Then
                                objAddOn.objApplication.SetStatusBarMessage("RM Tab: RM-Allocate Batch...at LineID" & j, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                Return False
                            End If
                        End If

                    End If


                Next
            End If
            Try


                Dim oMatrix_Machine As SAPbouiCOM.Matrix
                oMatrix_Machine = frmProductionReceipt.Items.Item("28").Specific
                Dim strInspectionRequired As Boolean = False
                For IntI As Integer = 1 To oMatrix_Machine.RowCount
                    Dim oCmbType As SAPbouiCOM.ComboBox = oMatrix_Machine.Columns.Item("Type").Cells.Item(IntI).Specific
                    If Not oCmbType.Selected Is Nothing Then
                        If oCmbType.Selected.Value = "M" Or oCmbType.Selected.Value = "B" Then
                            strInspectionRequired = True
                            Exit For
                        End If
                    End If
                Next


                oMatrix = frmProductionReceipt.Items.Item("52").Specific
                objAddOn.DeleteEmptyRowInFormDataEvent(oMatrix, "V_4", oDBDSBuy)
                If oMatrix.VisualRowCount > 0 Then
                    Dim i As Integer
                    For i = 1 To oMatrix.VisualRowCount
                        Dim oCmbType As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("V_10").Cells.Item(i).Specific
                        If oCmbType.Selected.Value = "M" Then
                            If oMatrix.Columns.Item("qty").Cells.Item(i).Specific.value = "0.0" Then
                                objAddOn.objApplication.SetStatusBarMessage("FG Tab:Quantity must be Greater Then 0", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                Return False
                            End If
                            Dim WhsCode As String = oMatrix.Columns.Item("WCode").Cells.Item(i).Specific.value.ToString().Trim()
                            Dim sProInspectionRequired = objAddOn.getSingleValue(" select Isnull(U_InspReq,'N')  from [@AIS_WRKTYP] Where   Code ='" & WorkOrderOperationType & "' ")
                            Dim pacingCost As Double = 0
                            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRS.DoQuery(" select U_InsReq from oitm where itemcode = '" & oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value & "'")

                            Dim strProductionIssueReceiptCount As String = objAddOn.getSingleValue("Select Count(U_LineID) from [@AS_WORD4] a where    DocEntry = '" & WODocEntry & "' and U_LineID ='" & CurrLineID & "' ")


                            If oRS.Fields.Item("U_InsReq").Value = "Y" Then
                                Dim WODocentry As Integer = oDBDSHeader.GetValue("U_WODocE", 0).ToString().Trim()
                                Dim strSqlWOType = objAddOn.getSingleValue(" Select T0.U_Type from [@AS_OWORD] T0 Where T0.Docentry='" & oDBDSHeader.GetValue("U_WODocE", 0).ToString().Trim() & "' ")
                                If sProInspectionRequired = "Y" Then
                                    If WhsCode <> "WAH0020" Then
                                        objAddOn.objApplication.SetStatusBarMessage("FG Tab:Item Warehouse Should be QC Warehouse -WAH0020 ", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                        Return False
                                    End If
                                End If
                                If sProInspectionRequired = "F" Then
                                    If strProductionIssueReceiptCount = String.Empty Then
                                        strProductionIssueReceiptCount = "0"
                                    End If
                                    If WhsCode <> "WAH0020" And CurrLineID = 1 And Convert.ToInt32(strProductionIssueReceiptCount) = 0 Then
                                        objAddOn.objApplication.SetStatusBarMessage("FG Tab:Item Warehouse Should be QC Warehouse -WAH0020 ", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                        Return False
                                    End If
                                End If
                                If sProInspectionRequired = "A" Then
                                    If WhsCode <> "WAH0020" And CurrLineID = 1 Then
                                        objAddOn.objApplication.SetStatusBarMessage("FG Tab:Item Warehouse Should be QC Warehouse -WAH0020 ", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                        Return False
                                    End If
                                End If

                            End If

                        End If
                        If oMatrix.Columns.Item("WCode").Cells.Item(i).Specific.string = "" Then
                            objAddOn.objApplication.SetStatusBarMessage("FG Tab:Please Select WareHouse", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                            Return False
                        End If
                    Next

                End If
            Catch ex As Exception
                objAddOn.objApplication.SetStatusBarMessage("Validate Function FG Tab Fail : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)

            End Try

            Return True
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Validate Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Function

#Region "Add Row Function"
    Public Sub AddRow(ByVal FormUID As String, ByVal ItemID As String)

        Try
            oMatrix = frmProductionReceipt.Items.Item("" & ItemID & "").Specific
            If oMatrix.Columns.Item("V_4").Cells.Item(oMatrix.VisualRowCount).Specific.value <> "" Then
                oMatrix.AddRow()
                oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Add Row Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
#End Region

#Region "Validate Event"

#End Region

#Region "Calculate Total Time Based on Given Time"
    Public Function CalculateTime(ByVal FormUID As String, ByVal ItemID As String, ByVal InTime As String, ByVal OutTime As String) As String
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select  CAST((x.Total +480)/ 60 AS VARCHAR(10)) + RIGHT('0' + CAST((x.Total +480) % 60 AS VARCHAR(2)), 2) TotTime " & _
                        "from (select DATEdiff(MINUTE,'08:00',(select left((Convert(time,CAST(((select Datediff(mi,convert(datetime,'" & InTime & "', 108) " & _
                        ",convert(datetime,'" & OutTime & "',108))) / 60) AS VARCHAR(8 )) + ':' + CAST(((select  Datediff(mi,convert(datetime,'" & InTime & "', 108), " & _
                        " convert(datetime,'" & OutTime & "',108))) % 60)  AS varchar(2)),5)),5) )) as Total)x")
            TotTime = oRS.Fields.Item("TotTime").Value
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Time Calculation Function Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return ""
        End Try
        Return TotTime
    End Function
#End Region

#Region "To Find the StockPrice"
    Private Function FindIsBatchItem(ByVal ItemCode As String) As String
        Try
            Dim pacingCost As Double = 0
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery(" select ManBtchNum from oitm where itemcode = '" & ItemCode & "'")
            Return oRS.Fields.Item("ManBtchNum").Value
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Values Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return "N"
        End Try
    End Function

#End Region

#Region "To Find the StockPrice"
    Private Function FindStockPrice(ByVal FormUID As String, ByVal DocNum As String) As Double
        Try
            If DocNum <> "" Then

                Return 0
            End If

        Catch ex As Exception
            Return 0
        End Try
    End Function

#End Region

#Region "Calculate Total Cost for Labour"

    Public Function CalculateLabourCost(ByVal FormUID As String, ByVal TotTime As String, ByVal MCode As String) As String
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select U_lcost ,round((U_lcost/60) *datediff(mi,0,cast('" & Left(TotTime, 2) & ":" & Right(TotTime, 2) & "' as datetime)),2) [LCost] from [@AS_OLBR] where Code ='" & MCode & "'")
            'If TotTime.Length = 3 Then
            '    TotTime = "0" + TotTime
            'End If
            TotLCost = oRS.Fields.Item("LCost").Value
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Machine Cost Calculation Function Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return ""
        End Try
        Return TotLCost
    End Function

#End Region

#Region "Calculate Total Cost for Machine"

    Public Function CalculateMachineCost(ByVal FormUID As String, ByVal TotTime As String, ByVal MCode As String) As String
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim SQL As String = "select U_mcost ,round((U_mcost/60) *datediff(mi,0,cast('" & Left(TotTime, 2) & ":" & Right(TotTime, 2) & "' as datetime)),2) [MCost] from [@AS_OMCN] where Code ='" & MCode & "'"
            oRS.DoQuery(SQL)
            'If TotTime.Length = 3 Then
            '    TotTime = "0" + TotTime
            'End If
            TotMCost = oRS.Fields.Item("MCost").Value
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Machine Cost Calculation Function Faliure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return ""
        End Try
        Return TotMCost
    End Function

#End Region

#Region "Machine and Labour cost Calculation"
    Public Sub MachineAndLabourCost(ByVal rownum As Integer, ByVal matrxId As String)
        Dim sqlStr, code As String
        Dim fDate, tDate, fTime, tTime As String
        'Dim totHours As String
        Dim fDateTime, tDateTime As DateTime
        Dim cost As Double = 0.0
        oMatrix = frmProductionReceipt.Items.Item(matrxId).Specific
        code = oMatrix.Columns.Item("V_4").Cells.Item(rownum).Specific.string
        fDate = oMatrix.Columns.Item("V_5").Cells.Item(rownum).Specific.string
        tDate = oMatrix.Columns.Item("V_2").Cells.Item(rownum).Specific.string
        fTime = oMatrix.Columns.Item("V_1").Cells.Item(rownum).Specific.string
        tTime = oMatrix.Columns.Item("V_6").Cells.Item(rownum).Specific.string
        'code = oMatrix.Columns.Item("V_4").Cells.Item(rownum).Specific.ToString

        fDateTime = fDate & " " & fTime
        tDateTime = tDate & " " & tTime

        tDateTime = tDateTime.AddSeconds(75)

        Dim span As TimeSpan = tDateTime.Subtract(fDateTime)
        oMatrix.Columns.Item("V_7").Cells.Item(rownum).Specific.value = Math.Round(span.Minutes / 60, 1)

        If matrxId = "18" Then
            sqlStr = "SELECT T0.[U_lcost] cost FROM [dbo].[@AS_OLBR]  T0 WHERE T0.[Code] ='" & code & "'"
        ElseIf matrxId = "17" Then
            sqlStr = "SELECT T0.[U_mcost] cost FROM [dbo].[@AS_OMCN]  T0 WHERE T0.[Code]='" & code & "'"
        End If
        Try
            Dim objRS As SAPbobsCOM.Recordset
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS.DoQuery(sqlStr)
            If Not objRS.EoF Then
                cost = objRS.Fields.Item("cost").Value
            End If
            oMatrix.Columns.Item("V_0").Cells.Item(rownum).Specific.value = oMatrix.Columns.Item("V_7").Cells.Item(rownum).Specific.value * cost
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Values Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
#End Region

    Public Sub CFLCondition()
        Try
            'Dim Objcfl As SAPbouiCOM.ChooseFromList
            'Dim objChooseCollection As SAPbouiCOM.ChooseFromListCollection
            'Dim objConditions As SAPbouiCOM.Conditions
            'Dim objcondition As SAPbouiCOM.Condition

            'objChooseCollection = oForm.ChooseFromLists
            'Objcfl = objChooseCollection.Item("CFL_2")
            'objConditions = Objcfl.GetConditions()
            'objcondition = objConditions.Add()
            'objcondition.Alias = "U_dstatus"
            'objcondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
            'objcondition.CondVal = "O"
            'Objcfl.SetConditions(objConditions)
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Values Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub



    Sub LoadMachineParameterName(MachineCode As String)

        Try


            Dim oRS As SAPbobsCOM.Recordset
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim squery As String = "EXEC DBO.[@AIS_Production_GetMachineParameterDetails] '" & MachineCode & "'"
            oRS.DoQuery(squery)
            oRS.MoveFirst()
            Dim oMatrixP As SAPbouiCOM.Matrix = frmProductionReceipt.Items.Item("oMatrixP").Specific
            If oRS.RecordCount > 0 Then
                oMatrixP.Clear()
            End If

            For i As Integer = 0 To oRS.RecordCount - 1
                oMatrixP.AddRow()
                oMatrixP.Columns.Item("LineId").Cells.Item(i + 1).Specific.value = i + 1
                oMatrixP.Columns.Item("PName").Cells.Item(i + 1).Specific.value = oRS.Fields.Item("U_Parameter").Value
                oRS.MoveNext()
            Next
            oMatrixP.AddRow()
            oMatrixP.Columns.Item("LineId").Cells.Item(oMatrixP.VisualRowCount).Specific.value = oMatrixP.VisualRowCount
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("LoadMachineParameterName : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub

#Region "Choose From List"
    Public Sub ChooseItem(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        Try


            Dim objcfl As SAPbouiCOM.ChooseFromListEvent
            Dim objdt As SAPbouiCOM.DataTable
            objcfl = pval
            objdt = objcfl.SelectedObjects
            If objdt Is Nothing Then
            Else
                Select Case pval.ItemUID
                    Case "74"
                        oDBDSHeader.SetValue("U_SONum", 0, objdt.GetValue("DocNum", 0))
                        oDBDSHeader.SetValue("U_CustName", 0, objdt.GetValue("CardName", 0))
                    Case "54"
                        WODocEntry = objdt.GetValue("DocEntry", 0)
                        'oMatrix.FlushToDataSource()
                        Dim Str As String
                        ' Str = "Select T0.U_RuleC ,T0.U_RuleN ,T0.U_SONum, T0.U_CustName,T1.Name  from [@AS_OWORD] T0 left outer join [@AIS_COLOUR] T1 on T0.U_Colour=T1.code where T0.DocEntry =  '" & WODocEntry & "'"
                        Str = "Select  distinct T0.U_RuleC ,T0.U_RuleN ,T1.U_BaseNum  ,T1.U_CardName     from [@AS_OWORD] T0 Inner Join [@AS_WORD2] T1 on T0.DocEntry =T1.DocEntry Where isnull(T1.U_BaseNum,'')!='' AND T0.DocEntry = '" & WODocEntry & "'"
                        oRS = objAddOn.DoQuery(Str)

                        oDBDSHeader.SetValue("U_WONo", 0, objdt.GetValue("DocNum", 0))
                        oDBDSHeader.SetValue("U_RuleC", 0, oRS.Fields.Item("U_RuleC").Value)
                        oDBDSHeader.SetValue("U_RuleN", 0, oRS.Fields.Item("U_RuleN").Value)
                        oDBDSHeader.SetValue("U_WODocE", 0, WODocEntry)
                        oDBDSHeader.SetValue("U_SONum", 0, oRS.Fields.Item("U_BaseNum").Value)
                        oDBDSHeader.SetValue("U_CustName", 0, oRS.Fields.Item("U_CardName").Value)
                        'oDBDSHeader.SetValue("U_Colour", 0, oRS.Fields.Item("Name").Value)
                        Process()
                    Case "52"
                        oMatrix = frmProductionReceipt.Items.Item("52").Specific
                        Try

                            Select Case pval.ColUID
                                Case "V_4"
                                    oMatrix.FlushToDataSource()
                                    oDBDSBuy.SetValue("U_itemcode", pval.Row - 1, objdt.GetValue("ItemCode", 0))
                                    oDBDSBuy.SetValue("U_itemname", pval.Row - 1, objdt.GetValue("FrgnName", 0))
                                    oDBDSBuy.SetValue("U_BoxNum", pval.Row - 1, "Clik Link")
                                    oMatrix.LoadFromDataSourceEx()
                                    'oMatrix.Columns.Item("V_4").Cells.Item(pval.Row).Specific.value = objdt.GetValue("ItemCode", 0)
                                    'oMatrix.Columns.Item("V_3").Cells.Item(pval.Row).Specific.value = objdt.GetValue("ItemName", 0)
                                    'oMatrix.Columns.Item("V_4").Cells.Item(pval.Row).Specific.value = objdt.GetValue("ItemCode", 0)
                                Case "V_0"
                                    oMatrix.FlushToDataSource()
                                    oDBDSBuy.SetValue("U_whscode", pval.Row - 1, objdt.GetValue("WhsCode", 0))
                                    oMatrix.LoadFromDataSourceEx()
                                    'oMatrix.Columns.Item("V_0").Cells.Item(pval.Row).Specific.value = objdt.GetValue("WhsCode", 0)
                            End Select
                        Catch ex As Exception

                        End Try
                    Case "16"
                        oMatrix = frmProductionReceipt.Items.Item("16").Specific
                        Try
                            Select Case pval.ColUID
                                Case "V_4"
                                    oMatrix.Columns.Item("V_5").Cells.Item(pval.Row).Specific.value = objdt.GetValue("InvntryUom", 0)
                                    oMatrix.Columns.Item("V_3").Cells.Item(pval.Row).Specific.value = objdt.GetValue("ItemName", 0)
                                    oMatrix.Columns.Item("V_4").Cells.Item(pval.Row).Specific.value = objdt.GetValue("ItemCode", 0)
                                Case "V_1"
                                    oMatrix.Columns.Item("V_1").Cells.Item(pval.Row).Specific.value = objdt.GetValue("WhsCode", 0)
                            End Select
                        Catch ex As Exception

                        End Try
                    Case "17"
                        oMatrix = frmProductionReceipt.Items.Item("17").Specific
                        Try
                            Select Case pval.ColUID
                                Case "V_4"
                                    Try
                                        oMatrix.Columns.Item("V_3").Cells.Item(pval.Row).Specific.value = objdt.GetValue("ResName", 0)
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        oMatrix.Columns.Item("V_4").Cells.Item(pval.Row).Specific.value = objdt.GetValue("ResCode", 0)
                                    Catch ex As Exception

                                    End Try
                                    oMatrix.AddRow()
                                    oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value() = oMatrix.VisualRowCount
                                    LoadMachineParameterName(objdt.GetValue("ResCode", 0).ToString())



                            End Select
                        Catch ex As Exception

                        End Try
                        'Case "18"
                        '    oMatrix = oForm.Items.Item("18").Specific
                        '    Try
                        '        Select Case pval.ColUID
                        '            Case "V_4"
                        '                oMatrix.Columns.Item("V_3").Cells.Item(pval.Row).Specific.value = objdt.GetValue("U_lname", 0)
                        '                oMatrix.Columns.Item("V_4").Cells.Item(pval.Row).Specific.value = objdt.GetValue("Code", 0)
                        '        End Select
                        '    Catch ex As Exception

                        '    End Try

                    Case "6"
                        Try
                            frmProductionReceipt.Items.Item("8").Specific.value = objdt.GetValue("ItemName", 0)
                            frmProductionReceipt.Items.Item("6").Specific.value = objdt.GetValue("ItemCode", 0)
                            '  oForm.Items.Item("4").Specific.value = objdt.GetValue("DocNum", 0)

                        Catch ex As Exception

                        End Try
                    Case "40"
                        Try
                            frmProductionReceipt.Items.Item("42").Specific.value = objdt.GetValue("ItemName", 0)
                            frmProductionReceipt.Items.Item("40").Specific.value = objdt.GetValue("ItemCode", 0)
                            '  oForm.Items.Item("4").Specific.value = objdt.GetValue("DocNum", 0)

                        Catch ex As Exception

                        End Try
                    Case "32"
                        Try
                            frmProductionReceipt.Items.Item("32").Specific.string = objdt.GetValue("WhsCode", 0)
                        Catch ex As Exception

                        End Try
                End Select

            End If

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Values Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
#End Region


#Region " Add  Mode"
    Public Sub AddModeFunction()
        Try
            frmProductionReceipt.Items.Item("10").Specific.value = frmProductionReceipt.BusinessObject.GetNextSerialNumber("-1", "AS_OPDN")
            frmProductionReceipt.Items.Item("12").Specific.string = CDate(Today.Date).ToString("yyyyMMdd")
            frmProductionReceipt.Items.Item("10").Enabled = False
            frmProductionReceipt.Items.Item("6").Enabled = False
            frmProductionReceipt.Items.Item("12").Enabled = True
            frmProductionReceipt.Items.Item("4").Enabled = True
            frmProductionReceipt.Items.Item("6").Enabled = False
            frmProductionReceipt.Items.Item("8").Enabled = True
            frmProductionReceipt.Items.Item("23").Enabled = True

            frmProductionReceipt.ActiveItem = 4
            oMatrix = frmProductionReceipt.Items.Item("16").Specific
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
            oMatrix.Columns.Item("V_4").Editable = True
            oMatrix.Columns.Item("V_2").Editable = True
            oMatrix.Columns.Item("V_1").Editable = True
            oMatrix.Columns.Item("V_0").Editable = True
            oMatrix.Columns.Item("V_6").Editable = True
            oMatrix = frmProductionReceipt.Items.Item("17").Specific
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
            oMatrix.Columns.Item("V_4").Editable = True
            oMatrix.Columns.Item("V_2").Editable = True
            oMatrix.Columns.Item("V_1").Editable = True
            oMatrix.Columns.Item("V_0").Editable = True
            oMatrix.Columns.Item("V_6").Editable = True
            oMatrix = frmProductionReceipt.Items.Item("18").Specific
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.VisualRowCount
            oMatrix.Columns.Item("V_4").Editable = True
            oMatrix.Columns.Item("V_2").Editable = True
            oMatrix.Columns.Item("V_1").Editable = True
            oMatrix.Columns.Item("V_0").Editable = True
            oMatrix.Columns.Item("V_6").Editable = True
            frmProductionReceipt.SupportedModes = -1
            frmProductionReceipt.PaneLevel = 1
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("AddMode Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub
#End Region


    Sub Process()



        Dim i As Integer
        Dim Str As String
        Dim ocom As SAPbouiCOM.ComboBox = frmProductionReceipt.Items.Item("62").Specific
        Try
            If ocom.ValidValues.Count > 0 Then
                For i = 0 To ocom.ValidValues.Count - 1
                    ocom.ValidValues.Remove(ocom.ValidValues.Count - 1, SAPbouiCOM.BoSearchKey.psk_Index)
                Next
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Process  Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
        'Try
        '    Str = "Select U_OpCode,LineID ,U_OpName ,(Select Sum(U_PCyc) from [@AS_WORD1] where DocEntry = a.docentry )'Sum'  from [@AS_WORD1] a where U_Status ='O'    and a.DocEntry = '" & WODocEntry & "' and a.lineId ='" & CurrLineID & "' "
        '    oRS = objAddOn.DoQuery(Str)
        '    If oRS.RecordCount > 0 Then
        '        For i = 0 To oRS.RecordCount - 1
        '            Dim Description As String = oRS.Fields.Item("LineId").Value.ToString + "_" + oRS.Fields.Item("U_OpCode").Value.ToString
        '            ocom.ValidValues.Add(oRS.Fields.Item("LineId").Value, Description)
        '            oDBDSHeader.SetValue("U_PCyc", 0, oRS.Fields.Item("Sum").Value)
        '            'oForm.Items.Item("28").Specific.String = ors.Fields.Item("Sum").Value
        '            ocom.Select(Description, SAPbouiCOM.BoSearchKey.psk_ByDescription)
        '            oRS.MoveNext()
        '        Next
        '    End If

        '    'ocom.Select(CurrLineID, SAPbouiCOM.BoSearchKey.psk_ByValue)
        'Catch ex As Exception
        '    objAddOn.objApplication.SetStatusBarMessage("Process Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        'End Try


    End Sub

#Region " FInd Mode Function"
    Public Sub FindMode()
        Try
            frmProductionReceipt.Items.Item("10").Enabled = True
            frmProductionReceipt.ActiveItem = 10
            frmProductionReceipt.Items.Item("12").Enabled = True
            frmProductionReceipt.Items.Item("4").Enabled = True
            frmProductionReceipt.Items.Item("6").Enabled = True
            frmProductionReceipt.Items.Item("8").Enabled = True
            frmProductionReceipt.Items.Item("23").Enabled = True
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Find Mode Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
#End Region

#Region "Header Total"
    Public Sub CalculateHeaderTotal(ByVal FormUID As String, ByVal MatrixID As String, ByVal ItmID As String)
        Try
            Dim tot As Double
            Dim i As Integer
            bundleQty = 0
            patchQty = 0
            obQty = 0
            mchnCost = 0
            oMatrix = frmProductionReceipt.Items.Item("" & MatrixID & "").Specific
            If MatrixID = "17" Then
                For i = 1 To oMatrix.VisualRowCount
                    If oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value <> "" Then
                        tot = tot + oMatrix.GetCellSpecific("V_0", i).value
                        Try
                            bundleQty = bundleQty + oMatrix.GetCellSpecific("V_13", i).value
                        Catch ex As Exception
                        End Try
                        Try
                            patchQty = patchQty + oMatrix.GetCellSpecific("V_12", i).value
                        Catch ex As Exception
                        End Try
                        Try
                            obQty = obQty + oMatrix.GetCellSpecific("V_11", i).value
                        Catch ex As Exception
                        End Try
                        Try
                            mchnCost = mchnCost + oMatrix.GetCellSpecific("V_0", i).value
                        Catch ex As Exception
                        End Try
                    End If
                Next i
                frmProductionReceipt.Items.Item("26").Specific.value = mchnCost
            ElseIf MatrixID = "18" Then
                For i = 1 To oMatrix.VisualRowCount
                    If oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value <> "" Then
                        tot = tot + oMatrix.GetCellSpecific("V_0", i).value
                        Try
                            lbrCost = lbrCost + oMatrix.GetCellSpecific("V_0", i).value
                        Catch ex As Exception
                        End Try
                    End If
                Next i
                frmProductionReceipt.Items.Item("36").Specific.value = lbrCost

            End If

            frmProductionReceipt.Items.Item("" & ItmID & "").Specific.value = tot
            'oForm.Refresh()


        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Header Calculation Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub
#End Region

#Region " Quantity Header calculation "
    Sub QuantityHeaderCalc(ByVal FormUID As String, ByVal MatID As String, ByVal ItmID As String)
        Try
            Dim i As Integer
            Dim tot As Double
            oMatrix = frmProductionReceipt.Items.Item("16").Specific
            For i = 1 To oMatrix.VisualRowCount
                If oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value <> "" Then
                    tot = tot + oMatrix.GetCellSpecific("TQty", i).value
                End If
            Next i
            frmProductionReceipt.Items.Item("49").Specific.value = tot
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Total Quantity Calculation Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub
#End Region
#Region " Buy Pdt Qty Header calculation "
    Sub BuyPdtQtyHeaderCalc(ByVal FormUID As String, ByVal MatID As String, ByVal ItmID As String)
        Try
            Dim i As Integer
            Dim tot As Double
            oMatrix = frmProductionReceipt.Items.Item("52").Specific
            For i = 1 To oMatrix.VisualRowCount
                If oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value <> "" Then
                    tot = tot + oMatrix.GetCellSpecific("qty", i).value
                End If
            Next i


            oMatrix = frmProductionReceipt.Items.Item("16").Specific
            For i = 1 To oMatrix.VisualRowCount
                If oMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value <> "" Then
                    oMatrix.Columns.Item("TQty").Cells.Item(i).Specific.value = (Convert.ToDouble(oMatrix.GetCellSpecific("V_2", i).value) * tot).ToString()
                End If
            Next i


            If ItmID = "t_NWT" Then
                frmProductionReceipt.Items.Item("t_NWT").Specific.value = tot
            Else
                frmProductionReceipt.Items.Item("72").Specific.value = tot
            End If

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Total Quantity Calculation Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub

    Sub BuyPdtQtyHeaderCalcNetWeight(ByVal FormUID As String, ByVal MatID As String, ByVal ItmID As String)
        Try
            Dim i As Integer
            Dim tot As Double
            oMatrix = frmProductionReceipt.Items.Item("52").Specific
            For i = 1 To oMatrix.VisualRowCount
                If oMatrix.Columns.Item("Aqty").Cells.Item(i).Specific.value <> "" Then
                    tot = tot + oMatrix.GetCellSpecific("Aqty", i).value
                End If
            Next i




            frmProductionReceipt.Items.Item("t_NWT").Specific.value = tot


        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Total Quantity Calculation Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub

#End Region

End Class
