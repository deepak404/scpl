﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Sql
Public Class clsPartsDetails
    Dim oDBDSHeader, oDBDSLine As SAPbouiCOM.DBDataSource
    'Public machin As New clsMachineMaster
    Dim oMatrix1 As SAPbouiCOM.Matrix
    Dim machincode As String
    Dim oForm As SAPbouiCOM.Form
    Dim oDataTable As SAPbouiCOM.DataTable
    Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
    Dim DocEntry, Str As String
    Dim Ors As SAPbobsCOM.Recordset
    Dim current As Integer
    Public Const FormType = "PARTS"
    Dim lretcode, IssueNum As Double
    Sub LoadScreen(ByVal mcode As String, ByVal mname As String)
        Try
            oForm = objAddOn.objUIXml.LoadScreenXML("PartsDetails.xml", Ananthi.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            oDBDSHeader = oForm.DataSources.DBDataSources.Item(0)
            oDBDSLine = oForm.DataSources.DBDataSources.Item(1)
            oMatrix1 = oForm.Items.Item("Matrix").Specific
            oForm.EnableMenu("1292", True)
            oForm.EnableMenu("1293", True)
            'oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            oMatrix1.Columns.Item("pname").Editable = False
            oMatrix1.Columns.Item("nmd").Editable = False
            Ors = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Str = "SELECT [DocNum] 'docnum' FROM [dbo].[@AIS_OPART]   where [U_mcode] = '" & mcode & "'"
            Ors = objAddOn.DoQuery(Str)
            If Ors.RecordCount > 0 Then
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                oForm.DataBrowser.BrowseBy = "8"
                oForm.Items.Item("8").Specific.value = Ors.Fields.Item("docnum").Value
                oForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                'oForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                'oForm.Items.Item("mcode").Enabled = True
                'oForm.Items.Item("mcode").Specific.value = mcode
                'oForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                'oForm.Items.Item("mcode").Enabled = False
                'AddModeFunction()
                'objAddOn.SetNewLine(oMatrix1, oDBDSLine)
                'oMatrix1.Columns.Item("V_-1").Cells.Item(oMatrix1.VisualRowCount).Specific.value = oMatrix1.VisualRowCount

            Else
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                oForm.Items.Item("mcode").Specific.value = mcode
                oForm.Items.Item("mname").Specific.value = mname
                ' AddModeFunction()
                oMatrix1.AddRow()
                oMatrix1.Columns.Item("pcode").Editable = True
                oMatrix1.Columns.Item("V_-1").Cells.Item(oMatrix1.VisualRowCount).Specific.value = 1
            End If
            'machincode = oForm.Items.Item("mcode").Specific.value
            'matrix_load(machincode)
            'oMatrix1.AddRow()
            'AddModeFunction()
            'oMatrix1.Columns.Item("V_-1").Cells.Item(oMatrix1.VisualRowCount).Specific.value = oMatrix1.VisualRowCount

        Catch ex As Exception
            '   objAddOn.objApplication.SetStatusBarMessage("Form Load " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
    'Sub initform(ByVal mcode As String, ByVal mname As String)
    '    oForm.Items.Item("mcode").Specific.value = mcode
    '    oForm.Items.Item("mname").Specific.value = mname
    'End Sub

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If oForm Is Nothing Then
            oForm = objAddOn.objApplication.Forms.Item(FormUID)
        End If
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK

                    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        Try
                            If pVal.ItemUID = "1" Then
                                'Dim Itrate As Integer
                                'Dim oMatrix As SAPbouiCOM.Matrix
                                'oForm = objAddOn.objApplication.Forms.ActiveForm
                                'oMatrix = oForm.Items.Item("Matrix").Specific

                                'For Itrate = 1 To oMatrix.VisualRowCount
                                '    If oMatrix.Columns.Item("pname").Cells.Item(Itrate).Specific.string = "" Then
                                '        oMatrix.DeleteRow(Itrate)
                                '    End If
                                'Next
                                'Dim oEdit As SAPbouiCOM.EditText
                                ''oMatrix.AddRow(1, Itrate)
                                'oMatrix = oForm.Items.Item("Matrix").Specific
                                'oEdit = oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific
                                'For Itrate = 1 To oMatrix.VisualRowCount
                                '    oEdit = oMatrix.Columns.Item("V_-1").Cells.Item(Itrate).Specific
                                '    oEdit.Value = Itrate
                                'Next


                                If Validate(FormUID) = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                                'DeleteEmptyRow(FormUID)
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.StatusBar.SetText("Click Event :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        End Try

                    End If
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oCFLEvento As SAPbouiCOM.IChooseFromListEvent = pVal
                        Dim sCFL_ID As String = oCFLEvento.ChooseFromListUID
                        Dim oForm As SAPbouiCOM.Form = objAddOn.objApplication.Forms.Item(FormUID)
                        Dim oCFL As SAPbouiCOM.ChooseFromList = oForm.ChooseFromLists.Item(sCFL_ID)
                        Dim oDataTable As SAPbouiCOM.DataTable = oCFLEvento.SelectedObjects
                        'Str = "select Distinct ItemCode 'ItemCode',ItemName 'ItemName',FrgnName 'FrgnName'  from OITM Where ItmsGrpCod=(Select U_ToolGrp from [@AS_WIPGT])"
                        Str = "select Distinct ItemCode 'ItemCode',ItemName 'ItemName',FrgnName 'FrgnName'  from OITM Where ItmsGrpCod=(Select U_ToolGrp from [@AS_WIPGT])"
                        If pVal.ItemUID = "Matrix" And pVal.ColUID = "pcode" Then
                            objAddOn.ChooseFromListFilteration(oForm, "CFL_Item", "ItemCode", "select Distinct ItemCode 'ItemCode',ItemName 'ItemName',FrgnName 'FrgnName'  from OITM Where ItmsGrpCod in (Select U_ToolGrp from [@AS_WIPGT])")
                        End If
                    Catch ex As Exception

                    End Try
                    '    'Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    '    '    If pVal.ItemUID = "Matrix" And pVal.ColUID = "pcode" Then
                    '    '        objAddOn.ChooseFromListFilteration(oForm, "CFL_Item", "ItemCode", "select ItemCode 'ItemCode',ItemName 'ItemName',FrgnName 'FrgnName' from OITM Where ItmsGrpCod=(Select U_ToolGrp from [@AS_WIPGT])")
                    '    '    End If
            End Select
            'Select Case oCFLE.ChooseFromListUID

            '    Case "CFL_Item"
            '        Dim str As String = ""
            '        str = "select Distinct ItemCode 'ItemCode',ItemName 'ItemName',FrgnName 'FrgnName'  from OITM Where ItmsGrpCod=(Select U_ToolGrp from [@AS_WIPGT])"
            '        If pVal.ItemUID = "Matrix" And pVal.ColUID = "pcode" Then
            '            objAddOn.ChooseFromListFilteration(oForm, "CFL_Item", "ItemCode", "Str")
            '        End If
            'End Select
             

        ElseIf pVal.BeforeAction = False Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    If pVal.ItemUID = "Matrix" And pVal.ColUID = "pcode" Then
                        
                        Try
                            Dim oCFLEvento As SAPbouiCOM.IChooseFromListEvent = pVal
                            Dim sCFL_ID As String = oCFLEvento.ChooseFromListUID
                            Dim oForm As SAPbouiCOM.Form = objAddOn.objApplication.Forms.Item(FormUID)
                            Dim oCFL As SAPbouiCOM.ChooseFromList = oForm.ChooseFromLists.Item(sCFL_ID)
                            Dim oDataTable As SAPbouiCOM.DataTable = oCFLEvento.SelectedObjects

                            Dim colid As String = "pcode"
                            Dim colid1 As String = "pname"
                            Dim val1 As String = ""
                            Dim val2 As String = ""
                            Dim val3 As String = ""
                            Try
                                val1 = oDataTable.GetValue(0, 0)
                                val2 = oDataTable.GetValue(1, 0)
                                val3 = oDataTable.GetValue(2, 0)

                            Catch ex As Exception
                                'SBO_Application.MessageBox(ex.Message)
                            End Try

                            Dim oEditC As SAPbouiCOM.EditText
                            Dim oEditC1 As SAPbouiCOM.EditText
                            Dim oEditC2 As SAPbouiCOM.EditText
                            Dim oMatrix1 As SAPbouiCOM.Matrix = oForm.Items.Item("Matrix").Specific
                            If oDataTable Is Nothing Then
                                Exit Sub
                            End If

                            oEditC = oMatrix1.Columns.Item(colid).Cells.Item(pVal.Row).Specific

                            Try
                                oEditC.String = val1.ToString

                            Catch ex As Exception
                            End Try

                            oEditC1 = oMatrix1.Columns.Item(colid1).Cells.Item(pVal.Row).Specific

                            Try
                                oEditC1.String = val2.ToString

                            Catch ex As Exception
                            End Try


                            oEditC2 = oMatrix1.Columns.Item("specifn").Cells.Item(pVal.Row).Specific

                            Try
                                oEditC2.String = val3.ToString

                            Catch ex As Exception
                            End Try
                            If oMatrix1.Columns.Item("pCode").Cells.Item(pVal.Row).Specific.String <> "" And pVal.Row = oMatrix1.VisualRowCount Then
                                objAddOn.SetNewLine(oMatrix1, oDBDSLine)
                            End If
                            'If pVal.Row = oMatrix1.RowCount Then
                            '    oMatrix1.AddRow()
                            '    oMatrix1.Columns.Item("V_-1").Cells.Item(oMatrix1.VisualRowCount).Specific.value = oMatrix1.VisualRowCount
                            'End If

                        Catch ex As Exception

                        End Try
                    End If
                   


                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try



                        If pVal.ItemUID = "Matrix" And pVal.ColUID = "nod" And pVal.BeforeAction = False Then
                            'If pVal.Row Then


                            oForm = objAddOn.objApplication.Forms.ActiveForm
                            oMatrix1 = oForm.Items.Item("Matrix").Specific
                            'If oMatrix1.Columns.Item("nod").Cells.Item(1).Specific.value <> 0 Then
                            Dim lmd As Date
                            'Dim nmd As Date
                            Dim nod As Integer
                            Dim lm As String = ""
                            Dim nm As String
                            Dim Oedt As SAPbouiCOM.EditText

                            lm = oMatrix1.Columns.Item("lmd").Cells.Item(pVal.Row).Specific.string
                            nm = oMatrix1.Columns.Item("nmd").Cells.Item(pVal.Row).Specific.string

                            'lmd = Date.ParseExact(lm, "dd/MM/yyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                            lmd = Date.Parse(lm)
                            nod = oMatrix1.Columns.Item("nod").Cells.Item(pVal.Row).Specific.value
                            nm = lmd.AddDays(nod)
                            oMatrix1.Columns.Item("nmd").Editable = True
                            Oedt = oMatrix1.Columns.Item("nmd").Cells.Item(pVal.Row).Specific
                            Oedt.String = Date.Parse(nm)
                            Try
                                oMatrix1.Columns.Item("nmd").Editable = False
                            Catch ex As Exception

                            End Try
                            Dim strpval As String = pVal.Row
                            Dim vrow As String = oMatrix1.VisualRowCount
                            Dim int1 As Integer = pVal.Row + 1
                            Dim pcd As String = oMatrix1.Columns.Item("pcode").Cells.Item(4).Specific.string

                            If strpval = vrow And pcd <> "" Then
                                oForm.DataSources.DBDataSources.Item("@AIS_PART1").Clear()
                                oMatrix1.AddRow(1, -1)

                            End If

                            ''If pVal.Row Then
                            ''    oMatrix1.Columns.Item("nmd").Cells.Item(pVal.Row).Specific.string = nm
                            ''    oMatrix1.AddRow()
                            ''    oMatrix1.Columns.Item("V_-1").Cells.Item(oMatrix1.VisualRowCount).Specific.value = oMatrix1.VisualRowCount
                            'If oMatrix1.Columns.Item("nod").Cells.Item(pVal.Row).Specific.value <> "" And pVal.Row = oMatrix1.VisualRowCount Then
                            '    objAddOn.SetNewLine(oMatrix1, oDBDSLine)
                            'End If
                            'If pVal.Row = oMatrix1.RowCount Then
                            '    objAddOn.SetNewLine(oMatrix1, oDBDSLine)
                            'End If

                        End If

                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Lost Focus " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try



                    'Case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK
                    '    Try

                    '        'oForm = objAddOn.objApplication.Forms.Item("PARTS")

                    '        If oMatrix1.Row <> oMatrix1.VisualRowCount And _
                    '            oMatrix1.BeforeAction Then

                    '            oMatrix1.setRightMenu("1292", "Add Row")
                    '            oMatrix1.setRightMenu("1293", "Delete Row")


                    '        End If


                    '    Catch ex As Exception
                    '        oMatrix1.objApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    '    End Try



            End Select
        End If
    End Sub
    Public Sub CFLITEM(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef FormUid As String, ByRef Itemid As String, ByRef colId As String, ByRef colId1 As String)

        Try
            Dim oCFLEvento As SAPbouiCOM.IChooseFromListEvent = pVal
            Dim sCFL_ID As String = oCFLEvento.ChooseFromListUID
            Dim oForm As SAPbouiCOM.Form = objAddOn.objApplication.Forms.Item(FormUid)
            Dim oCFL As SAPbouiCOM.ChooseFromList = oForm.ChooseFromLists.Item(sCFL_ID)

            Dim val1 As String = ""
            Dim val2 As String = ""
            Dim val3 As String = ""

            Dim oMatrix As SAPbouiCOM.Matrix = oForm.Items.Item("Matrix").Specific
            If oCFLEvento.BeforeAction = False Then



                Dim oDataTable As SAPbouiCOM.DataTable = oCFLEvento.SelectedObjects
                Try
                    val1 = oDataTable.GetValue(0, 0)
                    val2 = oDataTable.GetValue(1, 0)
                    val3 = oDataTable.GetValue(2, 0)

                Catch ex As Exception
                    'SBO_Application.MessageBox(ex.Message)
                End Try

                Dim oEditC As SAPbouiCOM.EditText
                Dim oEditC1 As SAPbouiCOM.EditText
                Dim oEditC2 As SAPbouiCOM.EditText

                If oDataTable Is Nothing Then
                    Exit Sub
                End If

                oEditC = oMatrix.Columns.Item(colId).Cells.Item(pVal.Row).Specific

                Try
                    oEditC.String = val1

                Catch ex As Exception
                End Try

                oEditC1 = oMatrix.Columns.Item(colId1).Cells.Item(pVal.Row).Specific

                Try
                    oEditC1.String = val2

                Catch ex As Exception
                End Try


                oEditC2 = oMatrix.Columns.Item("specifn").Cells.Item(pVal.Row).Specific

                Try
                    oEditC2.String = val2

                Catch ex As Exception
                End Try
            End If




        Catch ex As Exception

        End Try
    End Sub

    Sub Menu_event(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Select Case pVal.MenuUID

            Case "1292"
                oForm = objAddOn.objApplication.Forms.ActiveForm
                If oForm.TypeEx = "PARTS" Then
                    Try
                        Dim oEdit, otext As SAPbouiCOM.EditText
                        oForm = objAddOn.objApplication.Forms.ActiveForm
                        oMatrix1 = oForm.Items.Item("Matrix").Specific
                        oForm.DataSources.DBDataSources.Item("@AIS_PART1").Clear()
                        If oMatrix1.VisualRowCount = 0 Then
                            oMatrix1.AddRow(1, -1)
                            oMatrix1 = oForm.Items.Item("Matrix").Specific
                            oEdit = oMatrix1.Columns.Item("V_-1").Cells.Item(oMatrix1.VisualRowCount).Specific
                            oEdit.Value = oMatrix1.VisualRowCount

                        Else
                            oMatrix1 = oForm.Items.Item("Matrix").Specific
                            otext = oMatrix1.Columns.Item("pcode").Cells.Item(oMatrix1.VisualRowCount).Specific
                            If otext.Value <> "" Then
                                oMatrix1.AddRow(1, -1)
                                oEdit = oMatrix1.Columns.Item("V_-1").Cells.Item(oMatrix1.VisualRowCount).Specific
                                oEdit.Value = oMatrix1.VisualRowCount
                            Else
                                Exit Sub
                            End If
                        End If

                    Catch ex As Exception
                        'SBO_Application.MessageBox(ex.Message)
                    End Try

                End If
                'Case "1293"
                '    ' Dim otext As SAPbouiCOM.EditText
                '    oForm = objAddOn.objApplication.Forms.ActiveForm
                '    If oForm.UniqueID = "PARTS" Then
                '        Try
                '            oForm = objAddOn.objApplication.Forms.Item("PARTS")
                '            oMatrix1 = oForm.Items.Item("Matrix").Specific
                '            'oMatrixL = frmwioupdate.Items.Item("16").Specific
                '            oMatrix1.DeleteRow(oMatrix1.VisualRowCount)

                '        Catch ex As Exception

                '        End Try
                '    End If

        End Select
        If pVal.MenuUID = "1282" Then
            AddModeFunction()
            'ElseIf pVal.MenuUID = "1292" Then
            '    oMatrix1.AddRow()
            '    oMatrix1.Columns.Item("V_-1").Cells.Item(oMatrix1.VisualRowCount).Specific.value = oMatrix1.VisualRowCount
        ElseIf pVal.MenuUID = "1281" Then
            FindMode()
        Else
            'oForm.Items.Item("14").Enabled = False
            'oForm.Items.Item("Code").Enabled = False
        End If

    End Sub

#Region " Add  Mode"
    Public Sub AddModeFunction()
        Try
            oMatrix1.Item.Enabled = True
            oForm.Items.Item("lblmcod").Visible = True
            oForm.Items.Item("lblmname").Visible = False
            oForm.Items.Item("mcode").Visible = True
            oForm.Items.Item("mname").Visible = False
            oMatrix1.Columns.Item("pcode").Editable = True
            oMatrix1.Columns.Item("pname").Editable = False
            oMatrix1.Columns.Item("specifn").Editable = True
            oMatrix1.Columns.Item("qty").Editable = True
            oMatrix1.Columns.Item("life").Editable = True
            oMatrix1.Columns.Item("lmd").Editable = True
            oMatrix1.Columns.Item("nod").Editable = True
            oMatrix1.Columns.Item("nmd").Editable = False

            'oForm.ActiveItem = "pcode"

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("AddMode Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub
#End Region

#Region "Add Row Function"
    Public Sub AddRow(ByVal FormUID As String, ByVal ItemID As String)

        Try
            oMatrix1 = oForm.Items.Item("" & ItemID & "").Specific
            If oMatrix1.Columns.Item("pcode").Cells.Item(oMatrix1.VisualRowCount).Specific.value <> "" Then
                oMatrix1.AddRow()
                oMatrix1.Columns.Item("V_-1").Cells.Item(oMatrix1.VisualRowCount).Specific.value = oMatrix1.VisualRowCount
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Add Row Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
#End Region

    '#Region "Delete an Empty Row"
    '    Public Sub DeleteEmptyRow(ByVal FormUID As String)
    '        Try
    '            oForm = objAddOn.objApplication.Forms.Item(FormUID)
    '            'oForm.Items.Item("mcode").Specific.value = ""
    '            'oForm.Items.Item("mname").Specific.value = ""
    '            oMatrix1 = oForm.Items.Item("Matrix").Specific
    '            If oMatrix1.Columns.Item("pcode").Cells.Item(oMatrix1.VisualRowCount).Specific.value = "" Then
    '                oMatrix1.DeleteRow(oMatrix1.VisualRowCount)
    '            End If
    '        Catch ex As Exception
    '            objAddOn.objApplication.SetStatusBarMessage("Delete an Empty Row Function Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
    '        End Try

    '    End Sub
    '#End Region

#Region " FInd Mode Function"
    Public Sub FindMode()
        Try
            oForm.Items.Item("mcode").Enabled = True

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Find Mode Failure : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
#End Region

#Region "Validate Event"
    Public Function Validate(ByVal FormUID As String) As Boolean

        oForm = objAddOn.objApplication.Forms.Item(FormUID)
        oMatrix1 = oForm.Items.Item("Matrix").Specific

        If oMatrix1.RowCount = 0 Then
            objAddOn.objApplication.SetStatusBarMessage("Should Not Add Empty Matrix", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf oMatrix1.RowCount > 0 Then
            For IntI As Integer = 1 To oMatrix1.VisualRowCount - 1

                If oMatrix1.Columns.Item("pcode").Cells.Item(IntI).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("please select Part Code at Line NO : " & IntI, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf oMatrix1.Columns.Item("specifn").Cells.Item(IntI).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("please Enter specification at Line NO : " & IntI, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf oMatrix1.Columns.Item("qty").Cells.Item(IntI).Specific.value < 0 Then
                    objAddOn.objApplication.SetStatusBarMessage("please Enter Qty at Line NO : " & IntI, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf oMatrix1.Columns.Item("life").Cells.Item(IntI).Specific.value < 0 Then
                    objAddOn.objApplication.SetStatusBarMessage("please Enter Frequency  at Line NO : " & IntI, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf oMatrix1.Columns.Item("lmd").Cells.Item(IntI).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Last Maintenance Date at Line NO : " & IntI, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf oMatrix1.Columns.Item("nod").Cells.Item(IntI).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Enter Period at Line NO : " & IntI, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            Next
        End If



        Return True
    End Function

#End Region


    Sub RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case eventInfo.ItemUID
                Case "Matrix"
                    oMatrix1 = oForm.Items.Item("Matrix").Specific
                    If eventInfo.Row <> oMatrix1.VisualRowCount And _
                        eventInfo.BeforeAction Then

                        objAddOn.setRightMenu("1292", "Add Row")
                      
                    End If
            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub
 
End Class
