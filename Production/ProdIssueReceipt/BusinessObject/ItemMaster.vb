﻿Public Class ItemMaster

    Public frmItem As SAPbouiCOM.Form
    Public oItem, oNewItem As SAPbouiCOM.Item

    Sub LoadForm()
        Try
            frmItem = objAddOn.objApplication.Forms.ActiveForm
            'UOMTypeEx = frmItem.UniqueID
            Me.InitForm()
        Catch ex As Exception
            'objAddOn.Msg("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub

    Sub InitForm()
        'Try
        'frmItem = objAddOn.objApplication.Forms.GetForm("150", -1)
        'Dim oButton As SAPbouiCOM.Button
        'frmItem.Freeze(True)
        'oItem = frmItem.Items.Item("5")

        'oNewItem = frmItem.Items.Add("LinkFG", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
        'oNewItem.Left = oItem.Left + oItem.Width + 4
        'oNewItem.Width = 50
        'oNewItem.Height = oItem.Height
        'oNewItem.Top = oItem.Top

        'oButton = oNewItem.Specific
        'oButton.Caption = "Parts"


        'Catch ex As Exception
        '    'objAddOn.objApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        'Finally
        '    frmItem.Freeze(False)
        'End Try
    End Sub
    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD

                    Try
                        If BusinessObjectInfo.BeforeAction = False Then
                            InitForm()
                        End If
                    Catch ex As Exception
                        BubbleEvent = False
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD

            End Select
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    'Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
    '    Try
    '        If frmItem Is Nothing Then
    '            frmItem = objAddOn.objApplication.Forms.Item(FormUID)
    '        End If
    '        Select Case pVal.EventType
    '            Case SAPbouiCOM.BoEventTypes.et_CLICK
    '                If pVal.BeforeAction = False Then
    '                    If pVal.ItemUID = "PARTS" Then
    '                        Dim itemcode As String = frmItem.Items.Item("5").Specific.value
    '                        Dim itemname As String = frmItem.Items.Item("7").Specific.value
    '                        'frmItem.Items.Item("").Specific.value()
    '                        objAddOn.objPartsDetails.LoadScreen(itemcode, itemname)
    '                    End If
    '                End If
    '        End Select
    '    Catch ex As Exception
    '        objAddOn.objApplication.StatusBar.SetText("Form Item Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
    '    Finally
    '    End Try
    'End Sub


End Class
