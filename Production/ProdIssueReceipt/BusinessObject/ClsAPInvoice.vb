﻿
Public Class ClsAPInvoice
    Public APform As SAPbouiCOM.Form
    Public oMatrix As SAPbouiCOM.Matrix
    Dim oitem As SAPbouiCOM.Item
    Dim oButton As SAPbouiCOM.Button
    Public Sub FormDataEvent(ByVal BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            If BusinessObjectInfo.BeforeAction = True Then

            Else
                Select Case BusinessObjectInfo.EventType
                    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
                        If BusinessObjectInfo.ActionSuccess = True Then
                            If InventoryTransfer() = False Then
                                If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                BubbleEvent = False
                            Else
                                'If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                objAddOn.objApplication.StatusBar.SetText("Inventory Transfer created:", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                            End If
                        End If
                        'If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Or oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        '    oForm.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                        'End If
                End Select
            End If
           
        Catch ex As Exception

        End Try
    End Sub

    Public Function init()
        Dim oEdit As SAPbouiCOM.EditText
        Dim oLable As SAPbouiCOM.StaticText
        APform = objAddOn.objApplication.Forms.GetForm("141", -1)
        oitem = APform.Items.Add("b_InvTrans", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
        oitem.Left = APform.Items.Item("2").Left + APform.Items.Item("2").Width + 20
        oitem.Width = APform.Items.Item("2").Width + 20
        oitem.Height = APform.Items.Item("2").Height
        oitem.Top = APform.Items.Item("2").Top
        oButton = oitem.Specific
        'oitem.FromPane = 0
        'oitem.ToPane = 0
        'oButton.Pane = 3
        oButton.Caption = "Create Inventory Transfer"
    End Function
    'Public Sub FormDataEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
    '    Select Case BusinessObjectInfo.EventType
    '        Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
    '            If pVal.BeforeAction = True Then
    '            Else

    '            End If
    '    End Select
    'Public Sub FormDataEvent(ByVal BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
    'Try
    '    oForm = objAddOn.objApplication.Forms.Item(FormUID)
    '    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Or oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
    '        oForm.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
    '    End If
    '    If oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
    '        oForm.Items.Item("10").Editable = True
    '    End If
    'Catch ex As Exception

    'End Try
    'Select Case pVal.EventType
    '    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD

    'End Select

    ' End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByVal pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    APform = objAddOn.objApplication.Forms.GetForm("141", -1)
                    If pVal.ItemUID = "1" And APform.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        If objAddOn.objCompany.InTransaction = False Then objAddOn.objCompany.StartTransaction()
                        If InventoryTransfer() = False Then
                            If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                            BubbleEvent = False
                        Else
                            If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                            'If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                            'objAddOn.objApplication.StatusBar.SetText("Inventory Transfer created:", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                        End If
                    End If
            End Select
        ElseIf pVal.BeforeAction = False Then

            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
                    If pVal.FormType = "141" Then
                        init()
                    End If
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "b_InvTrans" Then
                        Dim seriescombo As SAPbouiCOM.ComboBox
                        seriescombo = APform.Items.Item("88").Specific
                        Dim val As String = objAddOn.getSingleValue("select docentry  from opch where series=" & seriescombo.Selected.Value.Trim & " and docnum=" & APform.Items.Item("8").Specific.value & " ")
                        Dim val1 As String = objAddOn.getSingleValue("select 1 from owtr  where U_APInvDEn='" & val & "' ")
                        If val1 = "" Or val1 = "0" Then
                            If APform.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                If InventoryTransfer() = False Then
                                    'objAddOn.objApplication.StatusBar.SetText("Inventory Transfer already created:", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                    If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                    BubbleEvent = False
                                Else
                                    If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                    objAddOn.objApplication.StatusBar.SetText("Inventory Transfer created Sucessfully:", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                                End If
                            Else
                                objAddOn.objApplication.StatusBar.SetText("Form should be in ok mode:", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            End If
                           
                        Else
                            objAddOn.objApplication.StatusBar.SetText("Inventory Transfer already created:", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        End If
                    End If
                    '    Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_A
                    '        If pVal.ItemUID = "1" And APform.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    '            If objAddOn.objCompany.InTransaction = False Then objAddOn.objCompany.StartTransaction()
                    '            If InventoryTransfer() = False Then
                    '                If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                    '                BubbleEvent = False
                    '            Else
                    '                If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                    '                'If objAddOn.objCompany.InTransaction Then objAddOn.objCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                    '                'objAddOn.objApplication.StatusBar.SetText("Inventory Transfer created:", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                    '            End If
                    '        End If
            End Select

        End If

    End Sub
    Public Function InventoryTransfer() As Boolean
        Try
            Dim flag As Boolean
            Dim oStockTransfer As SAPbobsCOM.StockTransfer = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oStockTransfer)
            APform = objAddOn.objApplication.Forms.ActiveForm
            oMatrix = APform.Items.Item("38").Specific
            Dim frmwhs As String = objAddOn.getSingleValue("select Whscode from owhs where U_GunnyBag='Y'")
            oStockTransfer.FromWarehouse = frmwhs
            oStockTransfer.ToWarehouse = oMatrix.Columns.Item("24").Cells.Item(1).Specific.value
            Dim Docnum = APform.Items.Item("8").Specific.value
            oStockTransfer.Comments = "Based on AP Invoice--" & Docnum
            For i As Integer = 1 To oMatrix.VisualRowCount
                If oMatrix.Columns.Item("U_GunnyItm").Cells.Item(i).Specific.value <> "" Then
                    Dim ItemCode As String = oMatrix.Columns.Item("1").Cells.Item(i).Specific.value
                    Dim GunnyBag As String = oMatrix.Columns.Item("U_GunyBg").Cells.Item(i).Specific.value
                    oStockTransfer.Lines.FromWarehouseCode = frmwhs
                    oStockTransfer.Lines.WarehouseCode = oMatrix.Columns.Item("24").Cells.Item(i).Specific.value
                    oStockTransfer.Lines.ItemCode = ItemCode
                    oStockTransfer.Lines.Quantity = GunnyBag
                    oStockTransfer.Lines.Add()
                    flag = True
                End If
            Next
           
            If flag = True Then
                Dim ErrorCode = oStockTransfer.Add()
              
                If ErrorCode <> 0 Then
                    objAddOn.objApplication.StatusBar.SetText("Inventory Transfer Posting Error:" & objAddOn.objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Return False
                Else
                    Dim seriescombo As SAPbouiCOM.ComboBox
                    seriescombo = APform.Items.Item("88").Specific
                    Dim nxtdocen As String = objAddOn.objCompany.GetNewObjectKey
                    'oStockTransfer.UserFields.Fields.Item("U_APInvDEn").Value = nxtdocen
                    'Dim dnum As String = objAddOn.getSingleValue("select docnum from owtr where docentry='" & nxtdocen & "'")
                    'Dim opchdocen As String = objAddOn.getSingleValue("select max(docentry) from opch")
                    'objAddOn.DoQuery("update opch set U_APInvDNo=" & dnum & " where docentry= '" & opchdocen & " ")
                    'objAddOn.DoQuery("update opch set APInvDEn=" & nxtdocen & " where docentry= '" & opchdocen & " ")
                    'objAddOn.DoQuery("update owtr set U_APInvDNo=" & APform.Items.Item("8").Specific.value & " where docentry=" & nxtdocen & " ")
                    '------------------
                    Dim dnum As String = objAddOn.getSingleValue("select docnum from owtr where docentry='" & nxtdocen & "'")
                    'oStockTransfer.UserFields.Fields.Item("U_APInvDNo").Value = dnum
                    Dim opchdocen As String = objAddOn.getSingleValue("select max(docentry) from opch")
                    Dim opchdocnum1 As String = objAddOn.getSingleValue("select docnum from opch where docentry='" & opchdocen & "'")
                    If APform.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        objAddOn.DoQuery("update opch set U_APInvDNo='" & dnum & "',U_APInvDEn='" & nxtdocen & "' where docentry='" & opchdocen & "' ")
                        ' Dim docnumopch As String = objAddOn.getSingleValue("select docentry from opch where series=" & seriescombo.Selected.Value.Trim & " and docnum=" & APform.Items.Item("8").Specific.value & " ")
                        objAddOn.DoQuery("update owtr set U_APInvDNo=" & opchdocnum1 & ",U_APInvDEn='" & opchdocen & "' where docentry=" & nxtdocen & " ")
                    End If
                    If APform.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        objAddOn.DoQuery("update opch set U_APInvDNo='" & dnum & "',U_APInvDEn='" & nxtdocen & "' where series=" & seriescombo.Selected.Value.Trim & " and docnum=" & APform.Items.Item("8").Specific.value & " ")
                        Dim docnumopch As String = objAddOn.getSingleValue("select docentry from opch where series=" & seriescombo.Selected.Value.Trim & " and docnum=" & APform.Items.Item("8").Specific.value & " ")
                        objAddOn.DoQuery("update owtr set U_APInvDNo=" & APform.Items.Item("8").Specific.value & ",U_APInvDEn='" & docnumopch & "' where docentry=" & nxtdocen & " ")
                    End If
                   
                    'objAddOn.DoQuery("update owtr set U_APInvDNo=" & APform.Items.Item("8").Specific.value & " where docentry=" & nxtdocen & " ")
                    '-----------------
                    Return True
                End If
            End If



        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Inventory Transfer Posting Error:" & ex.ToString, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        End Try


    End Function
End Class
