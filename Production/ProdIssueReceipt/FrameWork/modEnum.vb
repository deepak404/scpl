Module modEnum
    Public definenew As Boolean = False
    Public sPressed As Boolean
    Public Machinery As String = ""
    Public HWKey() As String = New String() {"H1909159896"}

    'Public RouterMenuId As String = "UOM"
    'Public UOMMenuId As String = "4353"
    'Public UOMTypeEx As String
    'Public oUOM As New UOM
    Public Parts As String = ""

    Public PartsMenuId As String = "PARTS"
    Public PartsTypeEx As String
    Public oParts As New ItemMaster

    Public MachineFormID = "AIS_OMCD"
    Public MachineXML As String = "MachineDetail.xml"
    Public objMachine As New MachineDetail
    Public ItemMaseterMenuId As String = "3073"
    Public ItemTypeEx As String
    Public oItm As New ItemMaster

    Public MachineMasterFormID As String = "MAC"
    Public MachineUniqueId As String
    Public oMachineMaster As New MachineMaster

    Public PartsMasterFormID As String = "MNU_TOOL"
    Public PartsUniqueId As String
    Public oPartsMaster As New ClsTool


    'Public SerialFormID As String = "AIS_SERIAL"
    'Public FormUniqueID As String
    'Public oSerial As New ClsSerial
    Public ItemOperationFormID As String = "IOD"
    Public ItemOperationUniqueId As String
    Public objItemOperation As New ItemOperation

    Public BatchFormID As String = "AIS_Batch"
    Public BatchUniqueID As String
    Public oBatch As New ClsBatch

    Public batserFormId As String = "BSN"
    Public obatserno As New BatchSerial
    Public batserxml As String = "BatchSerial.xml"
    Public boolModelForm As Boolean = False
    Public boolModelFormID As String = ""
    Public GenInsBINFormId = "BIN", GenInsBinXML As String = "InsBatchSet.xml"
    Public PlanningDetailsFormID = "DO2", PlanningDetailsXML As String = "QtyDespatch.xml"
    Public GenRMBINFormId = "RMBIN", GenRMBinXML As String = "RMBin.xml"

    Public APInvoiceFormID As String = "141"
    Public APInvoiceUniqueId As String
    Public objAPInvoice As New ClsAPInvoice


    Public WorkOrderFormID As String = "AS_OWORD"
    Public WorkOrderXMLID As String = "WorkOrder.xml"
    Public oClsWorkOrder As New ClsWorkOrder



End Module
