ALTER Procedure [dbo].[@AIS_Production_GoodsReceiptManaufactureDateUpdate](@DocEntry Int)

AS

Begin

Update T3 set MnfDate=T6.U_DocDate ,T3.U_SPL =T7.U_Spindle, T3.U_DN = T8.U_BoxNum  , T3.U_Nw =  T8.U_ActualQty  
From 
DBO.OIGN  T5 INNER JOIN  DBO.IGN1 AS T0 ON T5.DOCENTRY=T0.DOCENTRY  INNER JOIN
DBO.OILM AS T1 ON T1.DOCENTRY = T0.DOCENTRY AND T0.OBJTYPE = T1.TRANSTYPE AND T0.LINENUM = T1.DOCLINENUM INNER JOIN
DBO.ILM1 AS T2 ON T1.MESSAGEID = T2.MESSAGEID
INNER JOIN
DBO.OBTN AS T3 ON T2.ITEMCODE = T3.ITEMCODE AND T2.SYSNUMBER = T3.SYSNUMBER
INNER JOIN
DBO.OITM  AS T4 ON T2.ITEMCODE = T4.ITEMCODE
Inner Join [@AS_OIGN] T6 On T6.DocNum =T5.U_BaseNum and T5.U_BaseObject ='MNU_OIGN' 
Inner Join [@AS_IGN1] T66 On T66.DocEntry  =T6.DocEntry  
Inner Join [@AS_IGN8] T7 On T7.DocEntry  =T6.DocEntry   and t7.U_UniqueID =T66.LineId  
Inner Join [@AS_IGN5] T8 On T8.DocEntry  =T6.DocEntry     And T8.LineId =T7.U_UniqueID  and T8.U_Type ='M'

Where T6.DocEntry  =@DocEntry



END 