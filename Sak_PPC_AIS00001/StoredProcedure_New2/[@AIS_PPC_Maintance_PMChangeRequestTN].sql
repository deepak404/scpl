 

ALTER Procedure [dbo].[@AIS_PPC_Maintance_PMChangeRequestTN]( @DocEntry AS NVARCHAR(MAX) ,  @TransactionType nvarchar(20) ,  @error AS NVARCHAR(10) OUTPUT ,@error_message AS NVARCHAR(MAX) OUTPUT  )  
AS
Begin





If  Exists  (  Select  DocEntry from [@AIS_PMCR1]   T1 Where  DocEntry =@DocEntry   AND isnull(U_AvailDate    ,'') ='' AND LineId =0 )
Begin
		select @error = 20005
		 select @error_message = 'Details Should not Be empty'

End 

  


If  Exists  (  Select  DocEntry from [@AIS_OPMCR]  T1 Where  DocEntry =@DocEntry   AND isnull(U_AName   ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Approvar Name Should not be Empty'

End 


If  Exists  (  Select  DocEntry from [@AIS_OPMCR]  T1 Where  DocEntry =@DocEntry   AND isnull(U_MCCode ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Machine Code Should not be Empty'

End 

If  Exists  (  Select  DocEntry from [@AIS_OPMCR]  T1 Where  DocEntry =@DocEntry   AND isnull(U_ReqBy    ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Requested By Should not be Empty'

End 

If  Exists  (  Select  DocEntry from [@AIS_OPMCR]  T1 Where  DocEntry =@DocEntry   AND isnull(U_ToDept  ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'To Department Should not be Empty'

End 


 


END 
