 
 ALter Procedure [@AIS_PPC_GetMachineCycleTime](@ItemCode Varchar(50),@ResCode Varchar(max),@TotalQty Decimal(18,2))
 AS
 Begin
  --Select   floor(3.913) +convert(decimal(18,3), (3.913-floor(3.913)) * 60 /100.0)  from [@AIS_RSC8] Where U_ItemCode ='c'  AND U_Code ='M19019'  
  Select  ( floor(U_CycleTime) +convert(decimal(18,3), (U_CycleTime-floor(U_CycleTime)) * 60 /100.0) ) /100  as U_CycleTime ,
  (( floor(U_CycleTime) +convert(decimal(18,3), (U_CycleTime-floor(U_CycleTime)) * 60 /100.0) ) /100  ) * @TotalQty  as U_CycleTime 
  
   from [@AIS_RSC8] Where U_ItemCode =@ItemCode    AND U_Code =@ResCode  
  END 


 