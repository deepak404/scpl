 

ALTER Procedure [dbo].[@AIS_PPC_MachineAvailability_TransactionNotification]( @DocEntry AS NVARCHAR(MAX) ,  @TransactionType nvarchar(20) ,  @error AS NVARCHAR(10) OUTPUT ,@error_message AS NVARCHAR(MAX) OUTPUT  )  
AS
Begin


If Exists (  Select DocEntry from [@AIS_OMAV] T1 Where  DocEntry =@DocEntry and Isnull(U_MacCode,'')='')
Begin
		select @error = 20005
		select @error_message = 'Machine Code should not be empty'
End 


END 
