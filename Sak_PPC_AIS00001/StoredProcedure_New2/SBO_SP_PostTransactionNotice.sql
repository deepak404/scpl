--------------------------------------------------------------------------------------------------------------------------------
 If (@object_type ='59' AND @transaction_type IN (N'A'))
Begin
 EXEC [@AIS_BatchAllocation_GetIssueAndReceiptNo] @list_of_cols_val_tab_del ,	@object_type
 EXEC [@AIS_PowerSegregation_GetIssueAndReceiptNo] @list_of_cols_val_tab_del ,	@object_type
 EXEC [@AIS_Production_Lathing_GetIssueAndReceiptNo]  @list_of_cols_val_tab_del ,	@object_type
 END
 
  If (@object_type ='60' AND @transaction_type IN (N'A'))
Begin
 EXEC [@AIS_BatchAllocation_GetIssueAndReceiptNo] @list_of_cols_val_tab_del ,	@object_type	
  EXEC [@AIS_PowerSegregation_GetIssueAndReceiptNo] @list_of_cols_val_tab_del ,	@object_type
  EXEC [@AIS_Production_Lathing_GetIssueAndReceiptNo]  @list_of_cols_val_tab_del ,	@object_type
 END

  If (@object_type ='INS' AND @transaction_type IN (N'A'))
Begin
EXEC [@AIS_QCModule_Inpection_UpdateLathingQty] @list_of_cols_val_tab_del
END 

If (@object_type ='AS_OIGN' AND @transaction_type IN (N'A'))
Begin
EXEC [@AIS_ProductionReceipt_UpdateSerialNoDates] @list_of_cols_val_tab_del
END 