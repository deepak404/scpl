Alter Procedure [@AIS_PPC_GetRecommendationItemDetails](@ScenarioCode Varchar(50))

 AS

 Begin

 Select T1.DocEntry, T0.DocNum,T0.Object ,T1.LineId ,T1.U_CardCode as CardCode,T2.CardName,T4.ItmsGrpCod ,T4.ItmsGrpNam   ,T1.U_ItemCode,T1.U_ItemName  ,U_OrderQty,U_ChangeOverFil,U_PreFilType

  ,T1.U_WhsCode,T1.U_WhsName,U_MacCode ,U_MacName,U_PostingDate,U_ReqDate,U_ChangeOverFil,U_PItemCode as PItemCode ,U_PItemName as PItemName

   From [@AIS_OREC] T0 Inner Join [@AIS_REC1] T1 on T0.DocEntry =T1.DocEntry 

 Inner Join OCRD T2 on T2.CardCode =T1.U_CardCode 

 Inner Join OITM T3 On T3.ItemCode =T1.U_ItemCode 

 Inner Join OITB T4 On T4.ItmsGrpCod =T3.ItmsGrpCod 

 Where T0.U_ScenarioCode =@ScenarioCode  



 END 







  --EXEC [dbo].[@AIS_PPC_GetRecommendationItemDetails] 'LIVE'
