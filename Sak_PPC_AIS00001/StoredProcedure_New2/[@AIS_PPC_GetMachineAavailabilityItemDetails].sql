alter Procedure [@AIS_PPC_GetMachineAavailabilityItemDetails](@ScenarioCode Varchar(50))



 AS



 Begin



  Select T1.DocEntry, T0.DocNum,T0.Object ,T1.LineId ,T1.U_CardCode as CardCode,T2.CardName,T4.ItmsGrpCod ,T4.ItmsGrpNam   ,T1.U_ItemCode,T1.U_ItemName,



  T1.U_TotalPlanQty, T1.U_UOM ,T1.U_WhsCode ,T1.U_WhsName ,T1.U_MacCode, T1.U_MacName, 

  (Select   top 1 U_ChangeOverFil From [@AIS_SAV1]  Where DocEntry in (Select  Max(DocEntry) from    [@AIS_SAV1])   ) as U_ChangeOverFil,



  T1.U_ReqDate ,T1.U_PostingDate 

   



    From [@AIS_OMAV] T0 Inner Join [@AIS_MAV1] T1 on T0.DocEntry =T1.DocEntry 



  Inner Join OCRD T2 on T2.CardCode =T1.U_CardCode 



 Inner Join OITM T3 On T3.ItemCode =T1.U_ItemCode 



 Inner Join OITB T4 On T4.ItmsGrpCod =T3.ItmsGrpCod 



 Where T0.U_ScenarioCode =@ScenarioCode   AND Isnull(T1.U_Verify,'N') ='Y'







 END 


