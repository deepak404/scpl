 alter Procedure [@AIS_DaywisPlan_CalculateCycleTime](@Qty decimal(18,2),@FromDate Varchar(10),@ItemCode Varchar(50),@MCCode Varchar(50),@FromTime Varchar(10) )

 As

 Begin



Declare @CycleTime decimal(18,3)  ,@ToDate datetime
Select  @CycleTime =   ( floor(U_CycleTime) +convert(decimal(18,3), (U_CycleTime-floor(U_CycleTime)) * 60 /100.0) ) /100    from [@AIS_RSC8] Where U_ItemCode =@ItemCode    AND U_Code =@MCCode 


Declare @CycleTime1 Varchar(max) 



 Select @CycleTime=(U_CycleTime/100.0) * 60  + @Qty    ,@ToDate=DATEADD (	HOUR,( ((U_CycleTime/100.0) * 60  + @Qty)  )   ,Convert(datetime,@FromDate) ) 

  

    from [@AIS_RSC8] Where U_ItemCode =@ItemCode  AND U_Code =@MCCode

	set @CycleTime1 =@CycleTime




	SELECT @CycleTime CycleTime,  @ToDate ToDate  ,  Convert(Varchar(max) , SUBSTRING(@CycleTime1, 0  , CHARINDEX('.', @CycleTime1) )  

	+ SUBSTRING(@CycleTime1, CHARINDEX('.', @CycleTime1)+1  , 100) / 60 )    +':' +  Convert(Varchar(max),  SUBSTRING(@CycleTime1, CHARINDEX('.', @CycleTime1)+1  , 100)  % 60 ) as ToTime 

 ENd 