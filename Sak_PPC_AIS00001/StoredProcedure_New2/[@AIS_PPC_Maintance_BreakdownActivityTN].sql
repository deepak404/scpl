 


ALTER Procedure [dbo].[@AIS_PPC_Maintance_BreakdownActivityTN]( @DocEntry AS NVARCHAR(MAX) ,  @TransactionType nvarchar(20) ,  @error AS NVARCHAR(10) OUTPUT ,@error_message AS NVARCHAR(MAX) OUTPUT  )  
AS
Begin





If  Exists  (  Select  DocEntry from [@AIS_BDA1]  T1 Where  DocEntry =@DocEntry   AND isnull(U_SDate   ,'') ='' AND LineId =0 )
Begin
		select @error = 20005
		 select @error_message = 'Breakdown Issue Should not Be empty'

End 


If  Exists  (  Select  DocEntry from [@AIS_OBDA]  T1 Where  DocEntry =@DocEntry   AND isnull(U_Budget ,0) <=0 )
Begin
		select @error = 20005
		 select @error_message = 'Budget Should not be zero'

End 


If  Exists  (  Select  DocEntry from [@AIS_OBDA]  T1 Where  DocEntry =@DocEntry   AND isnull(U_IssueDB    ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Issue Done by Should not be Empty'

End 



If  Exists  (  Select  DocEntry from [@AIS_OBDA]  T1 Where  DocEntry =@DocEntry   AND isnull(U_AEng   ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Assign Engineer Should not be Empty'

End 




If  Exists  (  Select  DocEntry from [@AIS_OBDA]  T1 Where  DocEntry =@DocEntry   AND isnull(U_ProbCode    ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Problem Code Should not be Empty'

End 


If  Exists  (  Select  DocEntry from [@AIS_OBDA]  T1 Where  DocEntry =@DocEntry   AND isnull(U_Shift  ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Shift Should not be Empty'

End 


If  Exists  (  Select  DocEntry from [@AIS_OBDA]  T1 Where  DocEntry =@DocEntry   AND isnull(U_MCCode ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Machine Code Should not be Empty'

End 


If  Exists  (  Select  DocEntry from [@AIS_OBDA]  T1 Where  DocEntry =@DocEntry   AND isnull(U_Dept,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Department Should not be Empty'

End 


 


END 
