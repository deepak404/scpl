 



ALTER Procedure [dbo].[@AIS_PPC_Maintance_BreakdownRequestTN]( @DocEntry AS NVARCHAR(MAX) ,  @TransactionType nvarchar(20) ,  @error AS NVARCHAR(10) OUTPUT ,@error_message AS NVARCHAR(MAX) OUTPUT  )  
AS
Begin





If  Exists  (  Select  DocEntry from [@AIS_BDR1]  T1 Where  DocEntry =@DocEntry   AND isnull(U_BDIssue   ,'') ='' AND LineId =0 )
Begin
		select @error = 20005
		 select @error_message = 'Breakdown Issue Should not Be empty'

End 


If  Exists  (  Select  DocEntry from [@AIS_OBDR]  T1 Where  DocEntry =@DocEntry   AND isnull(U_Branch  ,0) ='' )
Begin
		select @error = 20005
		 select @error_message = 'Branch Should not be zero'

End 

 



If  Exists  (  Select  DocEntry from [@AIS_OBDR]  T1 Where  DocEntry =@DocEntry   AND isnull(U_AEng   ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Assign Engineer Should not be Empty'

End 




If  Exists  (  Select  DocEntry from [@AIS_OBDR]  T1 Where  DocEntry =@DocEntry   AND isnull(U_ProbCode    ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Problem Code Should not be Empty'

End 


If  Exists  (  Select  DocEntry from [@AIS_OBDR]  T1 Where  DocEntry =@DocEntry   AND isnull(U_Shift  ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Shift Should not be Empty'

End 


If  Exists  (  Select  DocEntry from [@AIS_OBDR]  T1 Where  DocEntry =@DocEntry   AND isnull(U_MCCode ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Machine Code Should not be Empty'

End 

If  Exists  (  Select  DocEntry from [@AIS_OBDR]  T1 Where  DocEntry =@DocEntry   AND isnull(U_ReqBy  ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Request By Should not be Empty'

End 

If  Exists  (  Select  DocEntry from [@AIS_OBDR]  T1 Where  DocEntry =@DocEntry   AND isnull(U_Dept,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Department Should not be Empty'

End 


 


END 
