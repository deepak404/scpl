 



ALTER Procedure [dbo].[@AIS_PPC_Maintance_PMActivityTN]( @DocEntry AS NVARCHAR(MAX) ,  @TransactionType nvarchar(20) ,  @error AS NVARCHAR(10) OUTPUT ,@error_message AS NVARCHAR(MAX) OUTPUT  )  
AS
Begin





If  Exists  (  Select  DocEntry from [@AIS_PMA1]  T1 Where  DocEntry =@DocEntry   AND isnull(U_Frequency   ,'') ='' AND LineId =0 )
Begin
		select @error = 20005
		 select @error_message = 'Frequency Should not Be empty'

End 


If  Exists  (  Select  DocEntry from [@AIS_OPMA]   T1 Where  DocEntry =@DocEntry   AND isnull(U_Branch  ,0) ='' )
Begin
		select @error = 20005
		 select @error_message = 'Branch Should not be zero'

End 

 



If  Exists  (  Select  DocEntry from [@AIS_OPMA]  T1 Where  DocEntry =@DocEntry   AND isnull(U_AEng   ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Assign Engineer Should not be Empty'

End 




 


If  Exists  (  Select  DocEntry from [@AIS_OPMA]  T1 Where  DocEntry =@DocEntry   AND isnull(U_Shift  ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Shift Should not be Empty'

End 


If  Exists  (  Select  DocEntry from [@AIS_OPMA]  T1 Where  DocEntry =@DocEntry   AND isnull(U_MCCode ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Machine Code Should not be Empty'

End 

If  Exists  (  Select  DocEntry from [@AIS_OPMA]  T1 Where  DocEntry =@DocEntry   AND isnull(U_EmpName   ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'Emp Name or Operator Name Should not be Empty'

End 

If  Exists  (  Select  DocEntry from [@AIS_OPMA]  T1 Where  DocEntry =@DocEntry   AND isnull(U_PM ,'') ='' )
Begin
		select @error = 20005
		 select @error_message = 'PM Should not be Empty'

End 


 


END 
