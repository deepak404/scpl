 
 
alter PROC  [dbo].[@AIS_TransactionNotification] ( @DocEntry AS NVARCHAR(MAX) ,  @object_type nvarchar(20) ,  @TransactionType nvarchar(20) ,  @error AS NVARCHAR(10) OUTPUT ,@error_message AS NVARCHAR(MAX) OUTPUT  ) 
AS
BEGIN
 
 If (@object_type ='AIS_PRP'  )
Begin


EXEC [dbo].[@AIS_PPC_MachineAvailability_TransactionNotification]  @DocEntry, @TransactionType ,@error OUTPUT , @error_message OUTPUT	

END 


 If (@object_type ='OSAV'  )
Begin


EXEC [dbo].[@AIS_PPC_StockPlanning_TransactionNotification]  @DocEntry, @TransactionType ,@error OUTPUT , @error_message OUTPUT	

END 



 If (@object_type ='OBDA'  )
Begin


EXEC [dbo].[@AIS_PPC_Maintance_BreakdownActivityTN]  @DocEntry, @TransactionType ,@error OUTPUT , @error_message OUTPUT	

END 

 If (@object_type ='OBDR'  )
Begin


EXEC [dbo].[@AIS_PPC_Maintance_BreakdownRequestTN]  @DocEntry, @TransactionType ,@error OUTPUT , @error_message OUTPUT	

END 


If (@object_type ='OPMA'  )
Begin


EXEC [dbo].[@AIS_PPC_Maintance_PMActivityTN]  @DocEntry, @TransactionType ,@error OUTPUT , @error_message OUTPUT	

END 


If (@object_type ='OPMCR'  )
Begin


EXEC [dbo].[@AIS_PPC_Maintance_PMChangeRequestTN]  @DocEntry, @TransactionType ,@error OUTPUT , @error_message OUTPUT	

END 


END
 
