﻿Imports SAPLib
''' <summary>
''' SAP has set of different events For access the controls.
''' In this module particularly using to control events.
''' 1) Menu Event using for while the User choose the menus to select the patricular form 
''' 2) Item Event using for to pass the event Function while user doing process
''' 3) Form Data Event Using to Insert,Update,Delete data on Date Base 
''' 4) Status Bar Event will be call when display message to user, message may be will come 
'''    Warring or Error
''' </summary>
''' <remarks></remarks>
''' 

Module EventHandler

    Public WithEvents oApplication As SAPbouiCOM.Application
    Public oForm As SAPbouiCOM.Form
 
#Region " ... 1) Menu Event ..."

    Private Sub oApplication_MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) Handles oApplication.MenuEvent
        Try
            oForm = oApplication.Forms.ActiveForm

            If pVal.BeforeAction = False Then

                Select Case pVal.MenuUID
                    Case PreSalesFormID
                        oPreSales.LoadPreSales()
                    Case MachineavailabilityFormID
                        oMachineavailability.LoadMachineavailability()
                    Case StockPlanningFormID
                        oStockPlanning.LoadStockPlanning()
                    Case DayWisePlanFormID
                        oDayWisePlan.LoadDayWisePlan()
                    Case ResourceCapacityFormID
                        oResourceCapacity.LoadPreSales()
                    Case LoginPageFormID
                        oLoginPage.LoadXMLForm()
                    Case RecommendationFormID
                        oRecommendation.LoadRecommendation()
                    Case DummySalesQuotationFormID
                        oDummySalesQuotation.LoadPreSales()
                    Case ProductionPlanFormID
                        oProductionPlan.LoadProductionPlan()
                End Select

                Select Case oForm.TypeEx
                    Case SalesOpportunityTypeEx
                        oSalesOpportunity.MenuEvent(pVal, BubbleEvent)
                End Select
                Select Case pVal.MenuUID
                    Case "1282", "1281", "1292", "1293", "1287", "519", "1284", "1286", "5890", "1290", "1289", "1288", "1291", "1294"
                        Select Case oForm.UniqueID
                            Case DummySalesQuotationFormID
                                oDummySalesQuotation.MenuEvent(pVal, BubbleEvent)

                            Case PreSalesFormID
                                oPreSales.MenuEvent(pVal, BubbleEvent)
                            Case MachineavailabilityFormID
                                oMachineavailability.MenuEvent(pVal, BubbleEvent)
                            Case StockPlanningFormID
                                oStockPlanning.MenuEvent(pVal, BubbleEvent)
                            Case DayWisePlanFormID
                                oDayWisePlan.MenuEvent(pVal, BubbleEvent)
                            Case ResourceCapacityFormID
                                oResourceCapacity.MenuEvent(pVal, BubbleEvent)
                            Case LoginPageFormID
                                oLoginPage.MenuEvent(pVal, BubbleEvent)

                            Case RecommendationFormID
                                oRecommendation.MenuEvent(pVal, BubbleEvent)
                            Case ProductionPlanFormID
                                oProductionPlan.MenuEvent(pVal, BubbleEvent)

                        End Select
                End Select

            Else
               
            End If


           
        Catch ex As Exception
            oApplication.StatusBar.SetText("Application Menu Event Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


#End Region

#Region " ... 2) Item Event ..."

    Private Sub oApplication_ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) Handles oApplication.ItemEvent
        Try
            Select Case pVal.FormUID
               
                Case LoginPageFormID
                    oLoginPage.ItemEvent(FormUID, pVal, BubbleEvent)
                Case DummySalesQuotationFormID
                    oDummySalesQuotation.ItemEvent(FormUID, pVal, BubbleEvent)
                Case PreSalesFormID
                    oPreSales.ItemEvent(FormUID, pVal, BubbleEvent)
                Case MachineavailabilityFormID
                    oMachineavailability.ItemEvent(FormUID, pVal, BubbleEvent)
                Case StockPlanningFormID
                    oStockPlanning.ItemEvent(FormUID, pVal, BubbleEvent)
                Case DayWisePlanFormID
                    oDayWisePlan.ItemEvent(FormUID, pVal, BubbleEvent)
                Case ResourceCapacityFormID
                    oResourceCapacity.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "WorkOrder"
                    oProductionPlan.ItemEvent_SubForm(FormUID, pVal, BubbleEvent)
                Case "Quotation"
                    oPreSales.ItemEvent_SubForm(FormUID, pVal, BubbleEvent)
                Case RecommendationFormID
                    oRecommendation.ItemEvent(FormUID, pVal, BubbleEvent)
                Case ProductionPlanFormID
                    oProductionPlan.ItemEvent(FormUID, pVal, BubbleEvent)
                Case PlanningDetailsFormID
                    oResourceCapacity.ItemEvent_SubGrid(PlanningDetailsFormID, pVal, BubbleEvent)
                Case BatchDetailsFormID
                    oProductionPlan.ItemEvent_SubGrid(BatchDetailsFormID, pVal, BubbleEvent)
            End Select
            Select Case pVal.FormTypeEx
                Case SalesOpportunityTypeEx
                    oSalesOpportunity.ItemEvent(FormUID, pVal, BubbleEvent)
            End Select

        Catch ex As Exception
            oApplication.StatusBar.SetText("Application ItemEvent Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

#End Region

#Region " ... 3) FormDataEvent ..."

    Private Sub oApplication_FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean) Handles oApplication.FormDataEvent
        Try
            Select Case BusinessObjectInfo.FormTypeEx
                Case SalesOpportunityTypeEx
                    oSalesOpportunity.FormDataEvent(BusinessObjectInfo, BubbleEvent)
            End Select

            Select Case BusinessObjectInfo.FormUID
                Case DummySalesQuotationFormID
                    oDummySalesQuotation.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case PreSalesFormID
                    oPreSales.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case MachineavailabilityFormID
                    oMachineavailability.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case StockPlanningFormID
                    oStockPlanning.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case DayWisePlanFormID
                    oDayWisePlan.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case ResourceCapacityFormID
                    oResourceCapacity.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case RecommendationFormID
                    oRecommendation.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case ProductionPlanFormID
                    oProductionPlan.FormDataEvent(BusinessObjectInfo, BubbleEvent)
            End Select

        Catch ex As Exception
            oApplication.StatusBar.SetText("Application FormDataEvent Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub

#End Region

#Region " ... 4) Status Bar Event ..."
    Public Sub oApplication_StatusBarEvent(ByVal Text As String, ByVal MessageType As SAPbouiCOM.BoStatusBarMessageType) Handles oApplication.StatusBarEvent
        Try
            If MessageType = SAPbouiCOM.BoStatusBarMessageType.smt_Warning Or MessageType = SAPbouiCOM.BoStatusBarMessageType.smt_Error Then
                System.Media.SystemSounds.Asterisk.Play()
            End If
        Catch ex As Exception
            oApplication.StatusBar.SetText(addonName & " StatusBarEvent Event Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
#End Region

#Region " ... 5) Set Event Filter ..."
    Public Sub SetEventFilter()
        Try
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        Finally
        End Try
    End Sub
#End Region

#Region " ... 6) Right Click Event ..."

    Private Sub oApplication_RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean) Handles oApplication.RightClickEvent
        Try
            Try
                RemoveRightMenu(Menu_DuplicateRow)
                RemoveRightMenu("Copy")
            Catch ex As Exception
            End Try
            Select Case eventInfo.FormUID
                Case MachineavailabilityFormID
                    oMachineavailability.RightClickEvent(eventInfo, BubbleEvent)
                Case DayWisePlanFormID
                    oDayWisePlan.RightClickEvent(eventInfo, BubbleEvent)
                Case StockPlanningFormID
                    oStockPlanning.RightClickEvent(eventInfo, BubbleEvent)

            End Select
            Select Case oApplication.Forms.ActiveForm.TypeEx
                Case SalesOpportunityTypeEx
                    oSalesOpportunity.RightClickEvent(eventInfo, BubbleEvent)
               

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText(addonName & " : Right Click Event Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
     
#End Region

#Region " ... 7) Application Event ..."
    Private Sub oApplication_AppEvent(ByVal EventType As SAPbouiCOM.BoAppEventTypes) Handles oApplication.AppEvent
        Try
            Select Case EventType
                Case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged, SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged, SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition, SAPbouiCOM.BoAppEventTypes.aet_ShutDown
                    System.Windows.Forms.Application.Exit()
            End Select

        Catch ex As Exception
            oApplication.StatusBar.SetText("Application Event Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub
#End Region

End Module
