﻿Imports SAPLib

Module LVariables

#Region " ... General Purpose ..."

    Public v_RetVal, v_ErrCode As Long
    Public v_ErrMsg As String = ""
    Public addonName As String = "PreSales"
    Public oCompany As SAPbobsCOM.Company
    Public ShowFolderBrowserThread As Threading.Thread
    Public BankFileName As String
    Public boolModelForm As Boolean = False
    Public boolModelFormID As String = ""
    Public ExpiryDate As String = "2017-10-01 00:00:00.000"
    Public HWKEY() As String = New String() {"L0287935425", "Z0464796071", "C0529684196"}

#End Region

#Region " ... Common For Module ..."

     

#End Region

#Region " ... Common For Forms ..."
 

#Region "Purchase Request"

    '   Tenant Slot ...

    Public PreSalesFormID As String = "OPRS"
    Public PreSalesXML As String = "PreSales.xml"
    Public oPreSales As New PreSalesOrder

    Public MachineavailabilityFormID As String = "OMAV"
    Public MachineavailabilityXML As String = "Machineavailability.xml"
    Public oMachineavailability As New Machineavailability

    Public StockPlanningFormID As String = "OSAV"
    Public StockPlanningXML As String = "StockPlanning.xml"
    Public oStockPlanning As New StockPlanning

    Public DayWisePlanFormID As String = "ODWP"
    Public DayWisePlanXML As String = "DayWisePlan.xml"
    Public oDayWisePlan As New DayWisePlan


    'AR Invoice     
    Public SalesOpportunityMenuId As String = "4372"
    Public SalesOpportunityTypeEx As String = "90000"
    Public oSalesOpportunity As New ResourceMaster

    Public ResourceCapacityFormID As String = "ORCP"
    Public ResourceCapacityXML As String = "ResourceCapacity.xml"
    Public oResourceCapacity As New ResourceCapacity

    '-------->>>>>>>Plan Details<<<<<<<<<------------
    Public PlanningDetailsFormID = "RCP2", PlanningDetailsXML As String = "ResourceAllocation.xml"
    Public BatchDetailsFormID As String = "PPL2", BatchDetailsDetailXML As String = "BatchDetails.xml"


    Public LoginPageFormID As String = "OSCE"
    Public LoginPageXML As String = "LoginPage.xml"
    Public oLoginPage As New LoginPage



    Public RecommendationFormID As String = "OREC"
    Public RecommendationXML As String = "Recommendation.xml"
    Public oRecommendation As New Recommendation


    Public ProductionPlanFormID As String = "OPPL"
    Public ProductionPlanXML As String = "ProductionPlan.xml"
    Public oProductionPlan As New ProductionPlan

    Public DummySalesQuotationFormID As String = "ODUQ"
    Public DummySalesQuotationXML As String = "DummyQuotation.xml"
    Public oDummySalesQuotation As New DummySalesQuotation

    Public Menu_DuplicateRow As String = "RDUP"
#End Region

#End Region

End Module