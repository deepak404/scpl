﻿ 
Imports SAPLib
Module Main

    Public t As Threading.Thread
    Sub Main()
        Try
            SetApplication() '1)
            oApplication = oApplication
            If Not CookieConnect() = 0 Then '3)
                oApplication.MessageBox("DI Api Conection Failed")
                End
            End If

            If Not ConnectionContext() = 0 Then '4)
                System.Windows.Forms.MessageBox.Show("Failed to Connect Company", addonName)
                If oCompany.Connected Then oCompany.Disconnect()
                System.Windows.Forms.Application.Exit()
                End
            End If
            oCompany = SAPLib.oCompany
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show("Application Not Found", addonName)
            System.Windows.Forms.Application.ExitThread()
        Finally
        End Try
        Try
            Try
                Dim oTableCreation As New TableCreation  '5) 
                EventHandler.SetEventFilter()
                oApplication = SAPLib.GFun.oApplication
                GVariables.addonName = "PPC"

                AddXML("Menu.xml")
            Catch ex As Exception
                System.Windows.Forms.MessageBox.Show(ex.Message)
                System.Windows.Forms.Application.ExitThread()
            Finally
            End Try
            oApplication.StatusBar.SetText("Connected.......", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

            Application.Run()
        Catch ex As Exception
            oApplication.StatusBar.SetText(addonName & " Main Method Failed : ", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        Finally
        End Try
    End Sub

End Module
