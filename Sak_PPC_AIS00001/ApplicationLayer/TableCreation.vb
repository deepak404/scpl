﻿Imports SAPLib
Public Class TableCreation
    Dim sTableName As String = String.Empty
    Dim cmb_YesOrNo = New String(,) {{"Y", "Yes"}, {"N", "No"}}
    Dim Frequency = New String(,) {{"D", "Daily"}, {"M", "Monthly"}, {"W", "Weekly"}, {"Y", "Yearly"}}
    Sub New()
        PreSales()
        DayWisePlan()
        Machineavailability()
        StockPlanning()
        ResourceCapacity()
        ScenarioMaster()
        Recommendation()
        DummyQuotation()
        ProductionPlan()
        ResourceMasterTableCreation()
        CreateUserFields("OITM", "GN", "Group Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields("OITM", "MC", "Machine Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields("OITM", "FT", "Fliment Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

        CreateUserFields("OITM", "NE", "NE", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields("OITM", "DM", "DM", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields("OITM", "ST", "ST", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

        CreateUserFields("ORDR", "PBaseline", "Pre Baseline", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields("ORDR", "PBaseNum", "Pre BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields("ORDR", "PBEntry", "Pre Base Entry", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

        Dim cmb_MacType = New String(,) {{"K", "Keddi"}, {"ZA", "Zell-A"}, {"ZB", "Zell-B"}, {"N", "None"}}
        CreateUserFieldsComboBox("ORSC", "MacType", "Machine Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, , , cmb_MacType, "N")

      

        Dim cmb_RawMaterial = New String(,) {{"S", "Spare"}, {"R", "Raw Materail"}, {"B", "Bonding Warehouse"}, {"N", "None"}}
        CreateUserFieldsComboBox("OITB", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, , , cmb_RawMaterial, "N")
        CreateUserFieldsComboBox("OWHS", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, , , cmb_RawMaterial, "N")

        CreateUserFieldsComboBox("ORSC", "RCapacity", "Resource Capacity", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, , , cmb_YesOrNo, "N")

        CreateUserFields("OWOR", "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields("OWOR", "BaseObject", "BaseObject", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields("OWOR", "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields("OWOR", "WDDocEntry", "Work. DocNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

        CreateUserFields("ORDR", "WDDocEntry", "Work. DocNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

        CreateUserFields("POR1", "Date", "Date", SAPbobsCOM.BoFieldTypes.db_Date)



        CreateTable("AIS_WrkTyp", "Work Order Type", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        sTableName = "@AIS_WrkTyp"
        CreateUserFields(sTableName, "WhsCode", "WhsCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields(sTableName, "WhsName", "WhsName", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
         



    End Sub

#Region "      Production Plan    "

    Sub ProductionPlan()
        Try
            Me.ProductionPlanHeader()
            Me.ProductionPlanDetail()
            Me.ProductionPlanDetail2()
            If Not UDOExists("OPPL") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("OPPL", "Production Plan", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OPPL", "AIS_PPL1", "AIS_PPL2")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub ProductionPlanHeader()
        Try
            CreateTable("AIS_OPPL", "Production Plan", SAPbobsCOM.BoUTBTableType.bott_Document)
            CreateUserFields("@AIS_OPPL", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OPPL", "WOType", "Work order Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OPPL", "DocDate", "Document Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OPPL", "RouteCode", "RouteCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OPPL", "RouteName", "RouteName", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub ProductionPlanDetail()
        Try
            CreateTable("AIS_PPL1", "Production Plan Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_PPL1", "BaseEntry", "BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PPL1", "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PPL1", "MCName", "MC Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PPL1", "MCCode", "MC Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PPL1", "NoOfEnd", "No of End", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PPL1", "Dimension", "Dimension", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PPL1", "SpoolType", "Spool Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PPL1", "PlanDate", "Plan Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_PPL1", "FGCode", "FG Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PPL1", "FGName", "FG Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PPL1", "RouteCode", "Route Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PPL1", "RouteName", "Route Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PPL1", "WhsCode", "Whs Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PPL1", "WhsName", "Whs Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PPL1", "ToWhsCode", "To Whs Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PPL1", "ToWhsName", "To Whs Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

            CreateUserFields("@AIS_PPL1", "CustCode", "Customer Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PPL1", "CustName", "Customer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PPL1", "Verify", "Verify", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_PPL1", "ReqQty", "Required Qty ", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_PPL1", "SFGCode", "SFG  Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PPL1", "SFGName", "SFG Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PPL1", "Instock", " Instock", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PPL1", "AllctdQty", "Allocated  Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_PPL1", "PDateFrm", "Plan Date From ", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_PPL1", "PDateTo", "Plan Date To", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_PPL1", "PTimeFrm", "Plan Time From", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_PPL1", "PTimeTo", "Plan Time To", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_PPL1", "BalReqQty", "Balanace Required  Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_PPL1", "AddReqQty", "Additional Required  Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_PPL1", "TotalQty", "Additional Total Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_PPL1", "TotalMin", "Total Minutes", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PPL1", "SDocNum", "SDocNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PPL1", "SDocEntry", "SDocEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PPL1", "WDocNum", "WDocNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PPL1", "WDocEntry", "WDocEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PPL1", "IDocNum", "WDocNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PPL1", "IDocEntry", "WDocEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PPL1", "Batch", "Batch", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub ProductionPlanDetail2()
        Try
            CreateTable("AIS_PPL2", "Production  Batch Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_PPL2", "UniqueID", "Unique Id", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PPL2", "ICode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PPL2", "IName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PPL2", "Batch", "Batch", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PPL2", "WhsCode", "Whs Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 10)
            CreateUserFields("@AIS_PPL2", "WhsName", "Whs Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PPL2", "Qty", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_PPL2", "ItemQty", "Item Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region

#Region "  ResourceCapacity "

    Sub ScenarioMaster()
        Try
            Me.ScenarioHeader()

            If Not UDOExists("OSCE") Then
                Dim findAliasNDescription = New String(,) {{"Code", "Code"}}
                RegisterUDO("OSCE", "Scenario Master", SAPbobsCOM.BoUDOObjType.boud_MasterData, findAliasNDescription, "AIS_OSCE")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub ScenarioHeader()
        Try
            CreateTable("AIS_OSCE", "Scenario  Header", SAPbobsCOM.BoUTBTableType.bott_MasterData)

            CreateUserFields("@AIS_OSCE", "ScenarioCode", "Scenario Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OSCE", "ScenarioName", "Scenario Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub



#End Region



#Region "  ResourceCapacity "

    Sub ResourceCapacity()
        Try
            Me.ResourceCapacityHeader()
            Me.ResourceCapacityDetail()
            Me.ResourceCapacityDetail1()

            If Not UDOExists("ORCP") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("ORCP", "Resource Capacity", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_ORCP", "AIS_RCP1", "AIS_RCP2")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub ResourceCapacityHeader()
        Try
            CreateTable("AIS_ORCP", "Resource Capacity Header", SAPbobsCOM.BoUTBTableType.bott_Document)
            Dim CapacityType = New String(,) {{"1", "Internal"}, {"2", "Commited"}, {"3", "Consumed"}, {"4", "Available"}, {"5", "All"}}
            CreateUserFieldsComboBox("@AIS_ORCP", "CapacityType", "Capacity Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", CapacityType, "1")
            CreateUserFields("@AIS_ORCP", "CPeriodFrom", "Capacity Period From", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_ORCP", "CPeriodTo", "Capacity Period To", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_ORCP", "WhsCodeFrom", "Warehouse Code From", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_ORCP", "WhsCodeTo", "Warehouse Code To", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_ORCP", "ResourceNoFrom", "Resource Number From", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_ORCP", "ResourceNoTo", "Resource Number To", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_ORCP", "RGroupFrom", "Resource Group From", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_ORCP", "RGroupTo", "Resource Group To", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_ORCP", "DocDate", "Document Date", SAPbobsCOM.BoFieldTypes.db_Date)
            Dim ResourceType = New String(,) {{"M", "Machine"}, {"L", "Labour"}, {"O", "Other"}}
            CreateUserFieldsComboBox("@AIS_ORCP", "ResourceType", "Resource Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", ResourceType, "M")
            Dim ResourceStatus = New String(,) {{"O", "Open"}, {"C", "Close"}}
            CreateUserFieldsComboBox("@AIS_ORCP", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", ResourceStatus, "O")

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub ResourceCapacityDetail()
        Try
            CreateTable("AIS_RCP1", "Resource Capacity Detail", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_RCP1", "ResourceNo", "Resource Number", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_RCP1", "RDescription", "Resource Description", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_RCP1", "WhsCode", "WhsCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_RCP1", "WhsName", "WhsName", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_RCP1", "Total", "Total", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D5", "D5", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D6", "D6", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D7", "D7", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D8", "D8", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D9", "D9", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D10", "D10", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D11", "D11", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D12", "D12", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D13", "D13", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D14", "D14", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D15", "D15", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D16", "D16", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D17", "D17", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D18", "D18", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D19", "D19", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D20", "D20", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D21", "D21", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D21", "D21", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D22", "D22", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D23", "D23", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D24", "D24", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D25", "D25", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D26", "D26", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D27", "D27", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D28", "D28", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D29", "D29", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D30", "D30", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D31", "D31", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D1", "D1", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D2", "D2", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D3", "D3", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "D4", "D4", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
             

            CreateUserFields("@AIS_RCP1", "A5", "A5", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A6", "A6", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A7", "A7", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A8", "A8", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A9", "A9", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A10", "A10", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A11", "A11", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A12", "A12", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A13", "A13", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A14", "A14", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A15", "A15", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A16", "A16", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A17", "A17", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A18", "A18", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A19", "A19", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A20", "A20", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A21", "A21", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A21", "A21", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A22", "A22", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A23", "A23", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A24", "A24", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A25", "A25", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A26", "A26", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A27", "A27", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A28", "A28", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A29", "A29", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A30", "A30", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A31", "A31", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A1", "A1", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A2", "A2", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A3", "A3", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "A4", "A4", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)



            CreateUserFields("@AIS_RCP1", "B5", "B5", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B6", "B6", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B7", "B7", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B8", "B8", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B9", "B9", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B10", "B10", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B11", "B11", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B12", "B12", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B13", "B13", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B14", "B14", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B15", "B15", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B16", "B16", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B17", "B17", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B18", "B18", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B19", "B19", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B20", "B20", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B21", "B21", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B21", "B21", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B22", "B22", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B23", "B23", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B24", "B24", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B25", "B25", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B26", "B26", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B27", "B27", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B28", "B28", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B29", "B29", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B30", "B30", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B31", "B31", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B1", "B1", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B2", "B2", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B3", "B3", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_RCP1", "B4", "B4", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)




            'CreateUserFields("@AIS_RCP1", "M5", "M5", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M6", "M6", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M7", "M7", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M8", "M8", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M9", "M9", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M10", "M10", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M11", "M11", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M12", "M12", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M13", "M13", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M14", "M14", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M15", "M15", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M16", "M16", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M17", "M17", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M18", "M18", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M19", "M19", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M20", "M20", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M21", "M21", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M21", "M21", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M22", "M22", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M23", "M23", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M24", "M24", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M25", "M25", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M26", "M26", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M27", "M27", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M28", "M28", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M29", "M29", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M30", "M30", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M31", "M31", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M1", "M1", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M2", "M2", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M3", "M3", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            'CreateUserFields("@AIS_RCP1", "M4", "M4", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)


        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub ResourceCapacityDetail1()
        Try
            CreateTable("AIS_RCP2", "Resource Capacity Detail2", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_RCP2", "FrmTime", "From Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_RCP2", "ToTime", "To Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_RCP2", "RAllocated", "Resource Allocated", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_RCP2", "UniqueID", "UniqueID", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_RCP2", "DayId", "DayId", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region

#Region "ResourceMasterData"
    Sub ResourceMasterTableCreation()
        ResourceMasterData()
        ResourceMasterData1()
        ResourceMasterData2()
        ResourceMasterData3()
        ResourceMasterData4()
        ResourceMasterData5()
    End Sub
    Sub ResourceMasterData()
        Try
            CreateTable("AIS_RSC8", "Sales Items Details", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_RSC8"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "Line ID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "CycleTime", "Cycle Time", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)


        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub ResourceMasterData1()
        Try
            CreateTable("AIS_RSC9", "Mech. Check List Details", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_RSC9"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "LineID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFieldsComboBox(sTableName, "Frequency", "Frequency", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Frequency, "D")
            CreateUserFields(sTableName, "FrequencyDay", "Frequency Day", SAPbobsCOM.BoFieldTypes.db_Numeric)
            CreateUserFields(sTableName, "Task", "Task", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "StepCode", "StepCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "LastUpdateDate", "Last Update Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "DefaultService", "Default Service", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Action", "Action", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Duration", "Duration", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields(sTableName, "PreparedDate", "Prepared Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "PreparedTDate", "Prepared To Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "SFromTime", "Schedule From Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            'CreateUserFields(sTableName, "SToTime", "Schedule To Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)


        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub ResourceMasterData2()
        Try
            CreateTable("AIS_RSC10", "M/C Spares List Details", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_RSC10"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "LineID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "SerialNo", "Serial No", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields(sTableName, "MainSpareCode", "Main Spare Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields(sTableName, "MainSpareName", "Main Spare Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields(sTableName, "SubSpareCode", "Sub Spare Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields(sTableName, "SubSpareName", "Sub Spare Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields(sTableName, "LastChangeDate", "Last Change Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "VerifiedBy", "Verified By", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields(sTableName, "AMC", "AMC", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields(sTableName, "SpareType", "Spare Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)



        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub ResourceMasterData3()
        Try
            CreateTable("AIS_RSC11", "Elec. Check List Details", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_RSC11"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "LineID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFieldsComboBox(sTableName, "Frequency", "Frequency", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Frequency, "D")
            CreateUserFields(sTableName, "Task", "Task", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "StepCode", "StepCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "FrequencyDay", "Frequency Day", SAPbobsCOM.BoFieldTypes.db_Numeric)
            CreateUserFields(sTableName, "LastUpdateDate", "Last Update Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "DefaultService", "Default Service", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Action", "Action", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Duration", "Duration", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields(sTableName, "PreparedDate", "Prepared Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "PreparedTDate", "Prepared To Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "SFromTime", "Schedule From Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            'CreateUserFields(sTableName, "SToTime", "Schedule To Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub ResourceMasterData4()
        Try
            CreateTable("AIS_RSC12", "Operator Check List Details", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_RSC12"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "LineID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFieldsComboBox(sTableName, "Frequency", "Frequency", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Frequency, "D")
            CreateUserFields(sTableName, "Task", "Task", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "StepCode", "StepCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "FrequencyDay", "Frequency Day", SAPbobsCOM.BoFieldTypes.db_Numeric)
            CreateUserFields(sTableName, "LastUpdateDate", "Last Update Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "DefaultService", "Default Service", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Action", "Action", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Duration", "Duration", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields(sTableName, "PreparedDate", "Prepared Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "PreparedTDate", "Prepared To Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "SFromTime", "Schedule From Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields(sTableName, "SToTime", "Schedule To Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub ResourceMasterData5()
        Try
            CreateTable("AIS_RSC13", "Resourec M/C Info. Det.", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_RSC13"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "LineID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields(sTableName, "VendorCode", "VendorCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "Manufacturing", "Manufacturing", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "InsulationDate", "Insulation Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "TechnicalSupp", "Technical Supporter", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Budget", "Budget", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)


        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

#End Region




#Region "      Dummy Quotation()    "

    Sub DummyQuotation()
        Try
            Me.DummyQuotationHeader()
            Me.DummyQuotationDetail()

            If Not UDOExists("ODUQ") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("ODUQ", "Pre Sales Quotation", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_ODUQ", "AIS_DUQ1")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub DummyQuotationHeader()
        Try
            CreateTable("AIS_ODUQ", "Pre Sales Quotation Header", SAPbobsCOM.BoUTBTableType.bott_Document)

            CreateUserFields("@AIS_ODUQ", "CardCode", "Customer", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_ODUQ", "CardName", "Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_ODUQ", "CRefNo", "CRefNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_ODUQ", "EPersonName", "EPersonName", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_ODUQ", "PostingDate", "Posting Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_ODUQ", "DocDate", "Doc Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_ODUQ", "ValidDate", "Valid Date", SAPbobsCOM.BoFieldTypes.db_Date)

            CreateUserFields("@AIS_ODUQ", "BaseAmount", "Base Amount", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_ODUQ", "TaxAmount", "Tax Amount", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Rate)
            CreateUserFields("@AIS_ODUQ", "Total", "Total", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub DummyQuotationDetail()
        Try
            CreateTable("AIS_DUQ1", "Pre Sales Quotation Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_DUQ1", "ItmsGrpCod", "Item Group Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields("@AIS_DUQ1", "ItmsGrpNam", "Item Group Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DUQ1", "Quantity", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_DUQ1", "UnitPrice", "Unit Price", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
            CreateUserFields("@AIS_DUQ1", "UOM", "UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DUQ1", "RequiredDate", "Required Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_DUQ1", "BaseAmount", "Base Amount", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_DUQ1", "TaxCode", "TaxCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DUQ1", "TaxRate", "Tax Amount", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_DUQ1", "WhsCode", "Warehouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DUQ1", "WhsName", "Warehouse Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DUQ1", "LineTotal", "LineTotal", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            Dim ValidValueType = New String(,) {{"O", "Open"}, {"C", "Close"}, {"L", "Cancel"}}
            CreateUserFieldsComboBox("@AIS_DUQ1", "LineStatus", "Line Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", ValidValueType, "O")
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region


#Region "      Pre Sales Order    "

    Sub PreSales()
        Try
            Me.PreSalesHeader()
            Me.PreSalesDetail()

            If Not UDOExists("OPRS") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("OPRS", "Pre Sales Order", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OPRS", "AIS_PRS1")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub PreSalesHeader()
        Try
            CreateTable("AIS_OPRS", "Pre Sales Order Header", SAPbobsCOM.BoUTBTableType.bott_Document)


            CreateUserFields("@AIS_OPRS", "CardCode", "Customer", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OPRS", "CardName", "Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OPRS", "PostingDate", "Posting Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OPRS", "DocDate", "Doc Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OPRS", "BaseAmount", "Base Amount", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_OPRS", "TaxAmount", "Tax Amount", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Rate)
            CreateUserFields("@AIS_OPRS", "Total", "Total", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_OPRS", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub PreSalesDetail()
        Try
            CreateTable("AIS_PRS1", "Pre Sales Order Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_PRS1", "BaseEntry", "BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PRS1", "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PRS1", "BaseObject", "BaseObject", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PRS1", "BaseLine", "BaseLine", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)

            CreateUserFields("@AIS_PRS1", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PRS1", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PRS1", "Quantity", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_PRS1", "UnitPrice", "Unit Price", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Price)
            CreateUserFields("@AIS_PRS1", "UOM", "UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PRS1", "RequiredDate", "Required Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_PRS1", "PostingDate", "Posting Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_PRS1", "BaseAmount", "Base Amount", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)

            CreateUserFields("@AIS_PRS1", "TaxCode", "TaxCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PRS1", "TaxRate", "Tax Amount", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            CreateUserFields("@AIS_PRS1", "WhsCode", "Warehouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PRS1", "WhsName", "Warehouse Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PRS1", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields("@AIS_PRS1", "LineTotal", "LineTotal", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
            Dim ValidValueType = New String(,) {{"O", "Open"}, {"C", "Close"}, {"L", "Cancel"}}
            CreateUserFieldsComboBox("@AIS_PRS1", "LineStatus", "Line Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", ValidValueType, "O")
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region

#Region "      Machine availability    "
    Sub Machineavailability()
        Try
            Me.MachineavailabilityHeader()
            Me.MachineavailabilityDetail()
            If Not UDOExists("OMAV") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("OMAV", "Machine Availability", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OMAV", "AIS_MAV1")
                findAliasNDescription = Nothing
            End If
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub


    Sub MachineavailabilityHeader()
        Try

            CreateTable("AIS_OMAV", "Machine Avail. Header", SAPbobsCOM.BoUTBTableType.bott_Document)
            CreateUserFields("@AIS_OMAV", "ScenarioCode", "Scenario Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OMAV", "ScenarioName", "Scenario Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OMAV", "DocDate", "Doc Date", SAPbobsCOM.BoFieldTypes.db_Date)


            CreateUserFields("@AIS_OMAV", "MacCode", "Machine Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OMAV", "MacName", "Machine Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OMAV", "Available1", "Available1", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_OMAV", "Available2", "Available2", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_OMAV", "Available3", "Available3", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)

            CreateUserFields("@AIS_OMAV", "Allocated1", "Allocated1", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_OMAV", "Allocated2", "Allocated2", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_OMAV", "Allocated3", "Allocated3", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)

            CreateUserFields("@AIS_OMAV", "Balance1", "Balance1", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_OMAV", "Balance2", "Balance2", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_OMAV", "Balance3", "Balance3", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)


        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub MachineavailabilityDetail()
        Try
            CreateTable("AIS_MAV1", "Machine Avail. Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_MAV1", "BaseEntry", "BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_MAV1", "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_MAV1", "BaseObject", "BaseObject", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_MAV1", "BaseLine", "BaseLine", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_MAV1", "WhsCode", "Warehouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_MAV1", "WhsName", "Warehouse Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_MAV1", "CardCode", "Customer Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_MAV1", "CardName", "Customer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_MAV1", "PostingDate", "Posting Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_MAV1", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_MAV1", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_MAV1", "Quantity", "Quantity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_MAV1", "ReqDate", "Required Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_MAV1", "UOM", "UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_MAV1", "CushionQty", "Cushion Quantity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_MAV1", "TotalPlanQty", "Total Plan Quantity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_MAV1", "MacCode", "Machine  Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_MAV1", "MacName", "Machine  Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_MAV1", "MacCapaity", "Machine Capacity", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_MAV1", "CycleTime", "Cycle Time", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_MAV1", "ReqHrs", "Required Hours", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_MAV1", "NoOfEnd", "No Of End", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_MAV1", "Dimention", "Dimention", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_MAV1", "SpoolType", "Spool Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_MAV1", "Verify", "Verify", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_MAV1", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)




        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    

#End Region



#Region "     Recommendation    "
    Sub Recommendation()
        Try
            Me.RecommendationHeader()
            Me.RecommendationDetail()


            If Not UDOExists("OREC") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("OREC", "Recommendation. Planning", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OREC", "AIS_REC1")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub RecommendationHeader()
        Try
            CreateTable("AIS_OREC", "Recommendation Header ", SAPbobsCOM.BoUTBTableType.bott_Document)
            CreateUserFields("@AIS_OREC", "DocDate", "Doc Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OREC", "ScenarioCode", "Scenario Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OREC", "ScenarioName", "Scenario Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub RecommendationDetail()
        Try
            CreateTable("AIS_REC1", "Recommendation Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_REC1", "BaseEntry", "BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_REC1", "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_REC1", "BaseObject", "BaseObject", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_REC1", "BaseLine", "BaseLine", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_REC1", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_REC1", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_REC1", "PBaseEntry", "Pre. BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_REC1", "PBaseNum", "Pre. BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_REC1", "PBaseLine", "Pre. BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_REC1", "PItemCode", "Previouse Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_REC1", "PItemName", "Previouse Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)


            CreateUserFields("@AIS_REC1", "MacCode", "Machine  Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_REC1", "MacName", "Machine  Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

            CreateUserFields("@AIS_REC1", "CardCode", "Customer Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_REC1", "CardName", "Customer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_REC1", "ItemGrpCode", "Item Group Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_REC1", "ItemGrpName", "Item Group Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_REC1", "OrderQty", "Order Quantity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_REC1", "PreFilType", "Previous Filament Tupe", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_REC1", "ChangeOverFil", "Change Over Filament", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_REC1", "WhsCode", "Warehouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_REC1", "WhsName", "Warehouse Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_REC1", "ReqDate", "Required Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_REC1", "PostingDate", "Posting Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_REC1", "Verify", "Verify", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_REC1", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub


#End Region



#Region "     Stock Planning    "
    Sub StockPlanning()
        Try
            Me.StockPlanningHeader()
            Me.StockPlanningDetail()
            Me.StockavailabilityDetail1()

            If Not UDOExists("OSAV") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("OSAV", "Stock Avail. Planning", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OSAV", "AIS_SAV1", "AIS_SAV2")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub StockPlanningHeader()
        Try
            CreateTable("AIS_OSAV", "Stock Avail. Header ", SAPbobsCOM.BoUTBTableType.bott_Document)
            CreateUserFields("@AIS_OSAV", "DocDate", "Doc Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OSAV", "ScenarioCode", "Scenario Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OSAV", "ScenarioName", "Scenario Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub StockPlanningDetail()
        Try
            CreateTable("AIS_SAV1", "Stock Avail. Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_SAV1", "BaseEntry", "BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_SAV1", "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_SAV1", "BaseObject", "BaseObject", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_SAV1", "BaseLine", "BaseLine", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)

            CreateUserFields("@AIS_SAV1", "PBaseEntry", "Pre. BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_SAV1", "PBaseNum", "Pre. BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_SAV1", "PBaseLine", "Pre. BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)

            CreateUserFields("@AIS_SAV1", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_SAV1", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV1", "CardCode", "Customer Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV1", "CardName", "Customer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV1", "MacCode", "Machine  Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV1", "MacName", "Machine  Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV1", "OrderQty", "Order Quantity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_SAV1", "PreFilType", "Previous Filament Tupe", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV1", "ChangeOverFil", "Change Over Filament", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV1", "WhsCode", "Warehouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV1", "WhsName", "Warehouse Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV1", "ReqDate", "Required Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_SAV1", "PostingDate", "Posting Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_SAV1", "Verify", "Verify", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_SAV1", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub StockavailabilityDetail1()
        Try
            CreateTable("AIS_SAV2", "Stock Avail. Details2", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_SAV2", "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_SAV2", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV2", "Instock", "Instock", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_SAV2", "TransitStock", "Transit Stock", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_SAV2", "TransitStock2", "Transit Stock2", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_SAV2", "TransitStock3", "Transit Stock3", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_SAV2", "TransitStock4", "Transit Stock4", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)

            CreateUserFields("@AIS_SAV2", "BondingStock", "Bonding Stock", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_SAV2", "Total", "Total", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_SAV2", "Allocated", "Allocated", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_SAV2", "CardCode", "Customer Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV2", "CardName", "Customer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV2", "ChangeOverFil", "Change Over Filament", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV2", "PreFilType", "Previous Filament Tupe", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV2", "OrderQty", "Order Quantity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_SAV2", "AlloctedQty", "Allocated Quantity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_SAV2", "FGItemCode", "FG Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_SAV2", "FGItemName", "FG Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV2", "CustCode", "Customer Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_SAV2", "CustName", "Customer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_SAV2", "OrdQty", "Order Quantity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

#End Region

#Region "Day Wise Plan"

    Sub DayWisePlan()

        Try
            Me.DayWisePlanHeader()
            Me.DayWisePlanDetail()

            If Not UDOExists("ODWP") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("ODWP", "Day Wise Plan", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_ODWP", "AIS_DWP1")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub DayWisePlanHeader()
        Try
            CreateTable("AIS_ODWP", "Day Wise Plan Header", SAPbobsCOM.BoUTBTableType.bott_Document)
            CreateUserFields("@AIS_ODWP", "DocDate", "Doc. Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_ODWP", "ScenarioCode", "Scenario Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_ODWP", "ScenarioName", "Scenario Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub DayWisePlanDetail()
        Try
            CreateTable("AIS_DWP1", "Day Wise Plan Detail", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_DWP1", "BaseEntry", "BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_DWP1", "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_DWP1", "BaseObject", "BaseObject", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_DWP1", "BaseLine", "BaseLine", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_DWP1", "CardCode", "Customer Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_DWP1", "CardName", "Customer Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DWP1", "PItemCode", "Previouse Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_DWP1", "PItemName", "Previouse Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DWP1", "PBaseEntry", "Pre. BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_DWP1", "PBaseNum", "Pre. BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_DWP1", "PBaseLine", "Pre. BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)

            CreateUserFields("@AIS_DWP1", "ItemCode", "Item Codes", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_DWP1", "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DWP1", "TotalQty", "Total Qunatity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_DWP1", "MCCode", "Machine Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DWP1", "MCName", "Machine Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DWP1", "FilamentType", "Filament Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_DWP1", "ReqDate", "Req Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_DWP1", "PostingDate", "Posting Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_DWP1", "ChangeOverFil", "Change Over Filament", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DWP1", "PlanningQty", "Planning Qunatity", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_DWP1", "ReqHours", "Requested Hours", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_DWP1", "FromDate", "Date From", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_DWP1", "ToDate", "To Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_DWP1", "FromTime", "From Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_DWP1", "ToTime", "To Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_DWP1", "WhsCode", "Warehouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DWP1", "WhsName", "Warehouse Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_DWP1", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region
End Class

