 
 CREATE Procedure [@AIS_PPC_GetStockPlannedItemDetails](@ScenarioCode Varchar(50))

  AS

 Begin 

 Select T1.DocEntry, T0.DocNum,T0.Object ,T1.LineId ,T1.U_CardCode as CardCode,T1.U_CardName,T1.U_ItemGrpCode ,T1.U_ItemGrpName   ,T2.U_ItemCode,T2.U_ItemName  ,

  T2.U_Allocated ,T1.U_ChangeOverFil,T1.U_PreFilType

   ,T1.U_WhsCode,T1.U_WhsName,T3.CardName 

 From [@AIS_OSAV] T0 Inner Join [@AIS_SAV1] T1 on T0.DocEntry =T1.DocEntry 

 Inner Join [@AIS_SAV2] T2 on T0.DocEntry =T1.DocEntry    AND t2.LineId =T1.LineId  

 Inner Join OCRD T3 on T3.CardCode =T1.U_CardCode 

 Inner Join OITM T4 On T4.ItemCode =T1.U_ItemCode 



 Where T0.U_ScenarioCode =@ScenarioCode  





 END 
