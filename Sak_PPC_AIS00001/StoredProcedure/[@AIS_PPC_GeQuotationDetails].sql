
Alter Procedure [@AIS_PPC_GeQuotationDetails](@DocEntry Varchar(50))
AS
Begin
 
Select T0.DocEntry,T0.DocNum ,T0.Object ,T1.LineId ,T0.U_CardCode ,T0.U_CardName  , U_ItmsGrpCod  ,U_ItmsGrpNam  ,U_Quantity ,U_UnitPrice ,U_UOM  ,U_RequiredDate  ,
T1.U_BaseAmount  ,U_Quotation as Quotation,U_TaxCode  ,U_TaxRate ,U_WhsCode  ,U_WhsName  ,U_LineTotal  ,
Case When U_LineStatus='O' then 'Open' Else 'Close' End  as LineStatus
 From [@AIS_ODUQ]  T0 Inner Join [@AIS_DUQ1] T1 On T0.DocEntry=T1.DocEntry 
Where T0.DocEntry  =@DocEntry 
End 
