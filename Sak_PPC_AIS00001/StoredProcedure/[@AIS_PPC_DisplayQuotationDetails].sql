
Alter Procedure [@AIS_PPC_DisplayQuotationDetails](@CardCode Varchar(50))
AS
Begin
 
Select  T0.DocEntry ,T0.U_CardCode as [Customer Code] ,T0.U_CardName  as [Customer Name]
 From [@AIS_ODUQ]  T0  
Where T0.U_CardCode =@CardCode AND T0.DocEntry Not In (Select Isnull(U_BaseEntry,'')   From [@AIS_PRS1])
End 