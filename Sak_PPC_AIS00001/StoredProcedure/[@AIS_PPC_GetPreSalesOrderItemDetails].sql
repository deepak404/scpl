 
 CREATE Procedure [@AIS_PPC_GetPreSalesOrderItemDetails] 



 AS



 Begin



 Select T1.DocEntry, T0.DocNum,T0.Object ,T1.LineId ,T0.U_CardCode as CardCode,T2.CardName,T4.ItmsGrpCod ,T4.ItmsGrpNam   ,T1.U_ItemCode,T1.U_ItemName,T1.U_Quantity,T1.U_UnitPrice,T1.U_UOM,T1.U_RequiredDate,T1.U_BaseAmount,T1.U_Quotation,T1.U_TaxCode,

 T1.U_TaxRate,T1.U_WhsCode



 ,T1.U_WhsName, T1.U_LineStatus,T1.U_LineTotal From [@AIS_OPRS] T0 Inner Join [@AIS_PRS1] T1 on T0.DocEntry =T1.DocEntry 



 Inner Join OCRD T2 on T2.CardCode =T0.U_CardCode 



 Inner Join OITM T3 On T3.ItemCode =T1.U_ItemCode 



 Inner Join OITB T4 On T4.ItmsGrpCod =T3.ItmsGrpCod 



 Where  T1.U_LineStatus ='O'







 END 