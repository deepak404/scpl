 
 CREATE Procedure [@AIS_PPC_GetMachineAavailabilityItemDetails](@ScenarioCode Varchar(50))

 AS

 Begin

  Select T1.DocEntry, T0.DocNum,T0.Object ,T1.LineId ,T1.U_CardCode as CardCode,T2.CardName,T4.ItmsGrpCod ,T4.ItmsGrpNam   ,T1.U_ItemCode,T1.U_ItemName,

  T1.U_TotalPlanQty, T1.U_UOM ,U_WhsCode ,U_WhsName  From [@AIS_OMAV] T0 Inner Join [@AIS_MAV1] T1 on T0.DocEntry =T1.DocEntry 

   Inner Join OCRD T2 on T2.CardCode =T1.U_CardCode 

 Inner Join OITM T3 On T3.ItemCode =T1.U_ItemCode 

 Inner Join OITB T4 On T4.ItmsGrpCod =T3.ItmsGrpCod 

 Where T0.U_ScenarioCode =@ScenarioCode   AND Isnull(T1.U_Verify,'N') ='Y'

 

 END 








