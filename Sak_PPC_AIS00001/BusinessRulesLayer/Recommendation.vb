﻿Imports SAPLib
Public Class Recommendation
    Dim frmRecommendation As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim boolFormLoaded As Boolean = False
    Dim sFormUID As String
    Dim UDOID As String = "OREC"
    Dim sQuery As String
    Sub LoadRecommendation()
        Try


            boolFormLoaded = False
            LoadXML(frmRecommendation, RecommendationFormID, RecommendationXML)
            frmRecommendation = oApplication.Forms.Item(RecommendationFormID)
            oDBDSHeader = frmRecommendation.DataSources.DBDataSources.Item("@AIS_OREC")
            oDBDSDetail = frmRecommendation.DataSources.DBDataSources.Item("@AIS_REC1")
            oMatrix = frmRecommendation.Items.Item("Matrix").Specific
            frmRecommendation.PaneLevel = 1
            frmRecommendation.Freeze(True)
            ' 

            Me.InitForm()
            Me.DefineModeForFields()


        Catch ex As Exception
            oApplication.StatusBar.SetText("Load Stock Planning Form Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmRecommendation.Freeze(False)
        End Try
    End Sub

    Sub AutoSumEnable()
        Try
            Try
                oMatrix.Columns.Item("V_7").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto


            Catch ex As Exception
                oApplication.StatusBar.SetText("AutoSumEnable " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            End Try

        Catch ex As Exception
            oApplication.StatusBar.SetText("AutoSumEnable " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()
                Case "1294"
                    oMatrix.FlushToDataSource()
                    oMatrix.AddRow()
                    oMatrix.GetCellSpecific(0, oMatrix.VisualRowCount).Value = oMatrix.VisualRowCount
                    oMatrix.FlushToDataSource()
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


    Sub LoadPreSalesOrderDetails()
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the Presales Order Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            sQuery = " EXEC [dbo].[@AIS_PPC_GetStockPlannedItemDetails] '" & Trim(oDBDSHeader.GetValue("U_ScenarioCode", 0)) & "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
            rsetEmpDets.MoveFirst()
            oDBDSDetail.Clear()
            oMatrix.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                oDBDSDetail.Offset = i
                oApplication.StatusBar.SetText("Please wait ... Line." + i.ToString(), SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, i + 1)

                oDBDSDetail.SetValue("U_BaseEntry", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("DocEntry").Value.ToString())
                oDBDSDetail.SetValue("U_BaseNum", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("DocNum").Value.ToString())
                oDBDSDetail.SetValue("U_BaseObject", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("Object").Value.ToString())
                oDBDSDetail.SetValue("U_BaseLine", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("LineId").Value.ToString())
                oDBDSDetail.SetValue("U_CardCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("CardCode").Value.ToString())
                oDBDSDetail.SetValue("U_CardName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("CardName").Value.ToString())
                oDBDSDetail.SetValue("U_ItemCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ItemCode").Value.ToString())
                oDBDSDetail.SetValue("U_ItemName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ItemName").Value.ToString())

                oDBDSDetail.SetValue("U_PItemCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("PItemCode").Value.ToString())
                oDBDSDetail.SetValue("U_PItemName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("PItemName").Value.ToString())
                oDBDSDetail.SetValue("U_OrderQty", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_Allocated").Value.ToString())
                oDBDSDetail.SetValue("U_WhsCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_WhsCode").Value.ToString())
                oDBDSDetail.SetValue("U_WhsName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_WhsName").Value.ToString())
                oDBDSDetail.SetValue("U_PreFilType", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_PreFilType").Value.ToString())
                oDBDSDetail.SetValue("U_ChangeOverFil", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ChangeOverFil").Value.ToString())
                oDBDSDetail.SetValue("U_PostingDate", oDBDSDetail.Offset, CDate(rsetEmpDets.Fields.Item("U_PostingDate").Value).ToString("yyyyMMdd"))
                oDBDSDetail.SetValue("U_ReqDate", oDBDSDetail.Offset, CDate(rsetEmpDets.Fields.Item("U_ReqDate").Value).ToString("yyyyMMdd"))
                oDBDSDetail.SetValue("U_MacCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_MacCode").Value.ToString())
                oDBDSDetail.SetValue("U_MacName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_MacName").Value.ToString())
                oDBDSDetail.SetValue("U_PBaseEntry", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_PBaseEntry").Value)
                oDBDSDetail.SetValue("U_PBaseNum", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_PBaseNum").Value)
                oDBDSDetail.SetValue("U_PBaseLine", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_PBaseLine").Value)
                oDBDSDetail.SetValue("U_Remarks", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_Remarks").Value.ToString())
                rsetEmpDets.MoveNext()
            Next
            oMatrix.LoadFromDataSource()
            AutoSumEnable()
            If frmRecommendation.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmRecommendation.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Recommendation Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Employees Based On Department Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub



    Sub ItemEvent(FormUID As String, pVal As SAPbouiCOM.ItemEvent, BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID
                                Case "t_SCode"
                                    oDBDSHeader.SetValue("U_ScenarioCode", 0, Trim(oDataTable.GetValue("U_ScenarioCode", 0).ToString()))
                                    oDBDSHeader.SetValue("U_ScenarioName", 0, Trim(oDataTable.GetValue("U_ScenarioName", 0).ToString()))

                                    LoadPreSalesOrderDetails()



                                Case "Matrix"
                                    Select Case pVal.ColUID

                                        Case "WhsCod"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_WhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail.SetValue("U_WhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()



                                    End Select

                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try



                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "mDiesel"
                                Select Case pVal.ColUID
                                    Case "VCode"
                                        If pVal.BeforeAction = False Then
                                            ChooseFromListFilteration(frmRecommendation, "COST_CFL", "PrcCode", "Select  PrcCode    From  OPRC Where DimCode in (1,2)")
                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID

                            Case "mat_StockP"
                                Select Case pVal.ColUID
                                    Case "Litres"
                                        If pVal.BeforeAction = False And pVal.ItemChanged Then
                                            oMatrix.FlushToDataSource()
                                            Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_Litres", pVal.Row - 1)) * CDbl(oDBDSDetail.GetValue("U_Price", pVal.Row - 1))
                                            oDBDSDetail.SetValue("U_Total", pVal.Row - 1, dblValue)
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                    End Try
                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmRecommendation.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmRecommendation.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmRecommendation.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "matrix1"
                                Select Case pVal.ColUID
                                    Case "Selected"
                                        If pVal.BeforeAction = False Then
                                            Try


                                            Catch ex As Exception
                                            Finally
                                            End Try

                                        End If
                                End Select

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "btnNext"
                                If pVal.BeforeAction = False Then

                                    Dim t_SCode As String = frmRecommendation.Items.Item("t_SCode").Specific.value.ToString().Trim()
                                    frmRecommendation.Items.Item("1").Enabled = True


                                    If frmRecommendation.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                        frmRecommendation.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                    End If
                                    frmRecommendation.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    frmRecommendation.Items.Item("2").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    ' frmRecommendation.Close()

                                    oApplication.ActivateMenuItem("ODWP")
                                    Dim frmDayWisePlan As SAPbouiCOM.Form = oApplication.Forms.ActiveForm


                                    Dim sQuery As String = String.Empty
                                    sQuery = " Select DocEntry  from [@AIS_ODWP] Where   U_ScenarioCode='" & t_SCode & "'"
                                    Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                    If rsetEmpDets.RecordCount > 0 Then
                                        frmDayWisePlan.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                        frmDayWisePlan.Items.Item("t_SCode").Specific.value = t_SCode
                                        frmDayWisePlan.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                        frmDayWisePlan.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE

                                    Else

                                        sQuery = " Select Code  from [@AIS_OSCE] Where   U_ScenarioCode='" & t_SCode & "'"
                                        Dim rsetEmpDets_Code As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                        Dim sCode As String = String.Empty
                                        If rsetEmpDets_Code.RecordCount > 0 Then
                                            sCode = Trim(rsetEmpDets_Code.Fields.Item("Code").Value)
                                        End If
                                        frmDayWisePlan.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                                        frmDayWisePlan.Items.Item("t_SCode").Specific.value = sCode
                                        LoadDocumentDate(frmDayWisePlan.Items.Item("t_DocDate").Specific)
                                        Dim oCmbSerial As SAPbouiCOM.ComboBox = frmDayWisePlan.Items.Item("c_series").Specific
                                        oCmbSerial.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)

                                    End If
                                    'frmDayWisePlan.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                End If


                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.ActionSuccess And frmRecommendation.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If
                            Case "Plan"
                                If pVal.BeforeAction = False Then
                                    frmRecommendation.PaneLevel = 1
                                    frmRecommendation.Items.Item("Plan").AffectsFormMode = False
                                    frmRecommendation.Settings.MatrixUID = "mat_StockP"
                                End If
                            Case "Stock"
                                If pVal.BeforeAction = False Then
                                    frmRecommendation.PaneLevel = 2
                                    frmRecommendation.Items.Item("Stock").AffectsFormMode = False
                                    frmRecommendation.Settings.MatrixUID = "mat_Stocka"
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Private Sub InitForm()
        Try
            frmRecommendation.Freeze(True)
            LoadComboBoxSeries(frmRecommendation.Items.Item("c_series").Specific, UDOID)
            LoadDocumentDate(frmRecommendation.Items.Item("t_DocDate").Specific)
            oMatrix.Clear()
            SetNewLine(oMatrix, oDBDSDetail)
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmRecommendation.Freeze(False)
        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub DefineModeForFields()
        Try
            frmRecommendation.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmRecommendation.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmRecommendation.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            '4
            frmRecommendation.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmRecommendation.Items.Item("t_DocDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Try
                        If BusinessObjectInfo.ActionSuccess Then


                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add ,Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        AutoSumEnable()
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
End Class
 