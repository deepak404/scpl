﻿Imports SAPLib
Public Class ResourceMaster
    Dim frmGeneralSeetings As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail As SAPbouiCOM.DBDataSource

    Dim boolFormLoaded As Boolean = False
    Dim strSQL As String = String.Empty
    Dim DeleteRowITEMUID As String

    Sub LoadGeneralSeetingsForm(ByVal FormUID As String)
        Try
            boolFormLoaded = False
            Dim oFolder As SAPbouiCOM.Folder = Nothing
            Dim oItem As SAPbouiCOM.Item
            Dim oItemRef As SAPbouiCOM.Item = Nothing
            Dim iMaxPane As Integer = 0
            frmGeneralSeetings = GFun.oApplication.Forms.Item(FormUID)


            boolFormLoaded = True

            Me.DynamicControl()
            Me.DynamicControlCapacity()
            Me.DynamicControlMech()
            Me.DynamicControlElec()
            Me.DynamicControlOperator()
            Me.DynamicControlMCSpares()
            Me.DynamicControl_MCInformation()
            Me.ItemButton()
            'Me.InitForm()
            'Me.DefineModesForFields()

        Catch ex As Exception
            Msg("Load Form Method Failed:" & ex.Message)
        Finally
            frmGeneralSeetings.Freeze(False)
        End Try
    End Sub
    Function TransactionManagement() As Boolean
        TransactionManagement = True

        If TransactionManagement = True Then
            TransactionManagement = InsertDetails()
        Else
            oApplication.StatusBar.SetText("Product Cycle Data Not Saved", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End If
        If TransactionManagement = True Then
            TransactionManagement = InsertDetails_Mech()
        Else
            oApplication.StatusBar.SetText("Mechanical Data Not Saved", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End If
        If TransactionManagement = True Then
            TransactionManagement = InsertDetails_Elec()
        Else
            oApplication.StatusBar.SetText("Electrical  Data Not Saved", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End If
        If TransactionManagement = True Then
            TransactionManagement = InsertDetails_Operator()
        Else
            oApplication.StatusBar.SetText("Operator  Data Not Saved", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End If
        If TransactionManagement = True Then
            TransactionManagement = InsertDetails_MCSpares()
        Else
            oApplication.StatusBar.SetText("MC Information Spares  Data Not Saved", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End If

        If TransactionManagement = True Then
            TransactionManagement = InsertDetails_MachineInformationDetails()
        Else
            oApplication.StatusBar.SetText("MC Info  Data Not Saved", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End If


        Return TransactionManagement
    End Function

    Function InsertDetails() As Boolean
        Try
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim
            Dim rsetCode As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim strSQL As String = String.Empty
            strSQL = "Delete from [@AIS_RSC8] Where U_BaseNum='" & sResourceCode & "'"
            rsetCode.DoQuery(strSQL)
            strSQL = String.Empty
            If rsetCode.RecordCount > 0 Then
                rsetCode.MoveFirst()
            End If


            Dim SaveDataTableRecords As SAPbouiCOM.DataTable
            SaveDataTableRecords = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms")


            For i As Integer = 0 To SaveDataTableRecords.Rows.Count - 1

                Dim sItemCode As String = SaveDataTableRecords.GetValue("ItemCode", i).ToString
                Dim sItemName As String = SaveDataTableRecords.GetValue("ItemName", i).ToString
                Dim sCycleTime As String = SaveDataTableRecords.GetValue("CycleTime", i).ToString
                If SaveDataTableRecords.GetValue("ItemCode", i).ToString <> String.Empty Then
                    strSQL = strSQL + "Insert into [@AIS_RSC8] (Code,Name,U_Code,U_LineID, U_BaseNum,U_Series, U_ItemCode,U_ItemName,U_CycleTime) Values  ('" & _
                   sResourceCode + i.ToString() & "','" & sResourceCode + i.ToString() & "','" & sResourceCode & "','" & i.ToString() & "','" & sResourceCode & "','" & sSeries & "','" & sItemCode & "','" & sItemName & "','" & sCycleTime & "')"
                End If

            Next
            If strSQL <> String.Empty Then
                rsetCode.DoQuery(strSQL)
                strSQL = String.Empty
            End If



            Return True
        Catch ex As Exception
            StatusBarWarningMsg("Insert Product Cycle Time Details Method Failed : " & ex.Message)
            Return False
        End Try
    End Function
    Function InsertDetails_Mech() As Boolean
        Try
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim
            Dim rsetCode As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim strSQL As String = String.Empty
            strSQL = "Delete from [@AIS_RSC9] Where U_BaseNum='" & sResourceCode & "'"
            rsetCode.DoQuery(strSQL)
            strSQL = String.Empty
            If rsetCode.RecordCount > 0 Then
                rsetCode.MoveFirst()
            End If


            Dim SaveDataTableRecords As SAPbouiCOM.DataTable
            SaveDataTableRecords = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms1")
            For i As Integer = 0 To SaveDataTableRecords.Rows.Count - 1

                Dim sTask As String = SaveDataTableRecords.GetValue("Task", i).ToString
                If sTask <> String.Empty Then
                    Dim sDate As String = String.Empty
                    If SaveDataTableRecords.GetValue("Last Update Date", i) <> Nothing Then
                        sDate = GetDateAsstrign(SaveDataTableRecords.GetValue("Last Update Date", i).ToString)
                    End If
                    Dim U_Remarks As String = SaveDataTableRecords.GetValue("Remarks", i).ToString
                    Dim U_Action As String = SaveDataTableRecords.GetValue("Action", i).ToString
                    Dim Duration As String = SaveDataTableRecords.GetValue("Duration", i).ToString

                    Dim PreparedDate As String = String.Empty
                    If SaveDataTableRecords.GetValue("From Date", i) <> Nothing Then
                        PreparedDate = GetDateAsstrign(SaveDataTableRecords.GetValue("From Date", i).ToString)
                    End If
                    Dim PreparedTDate As String = String.Empty
                    If SaveDataTableRecords.GetValue("To Date", i) <> Nothing Then
                        PreparedTDate = GetDateAsstrign(SaveDataTableRecords.GetValue("To Date", i).ToString)
                    End If

                    Dim sDefaultService As String = SaveDataTableRecords.GetValue("Default Service", i).ToString
                    Dim sFrequency As String = SaveDataTableRecords.GetValue("Frequency", i).ToString
                    Dim sFrequencyDay As String = SaveDataTableRecords.GetValue("FrequencyDay", i).ToString
                    Dim sSFromTime As String = SaveDataTableRecords.GetValue("Total Time", i).ToString
                    Dim sStepCode As String = SaveDataTableRecords.GetValue("Step Code", i).ToString

                    'Dim sSToTime As String = SaveDataTableRecords.GetValue("Schedule To Time", i).ToString
                    If SaveDataTableRecords.GetValue("Task", i).ToString <> String.Empty Then
                        strSQL = strSQL + "Insert into [@AIS_RSC9] (Code,Name,U_Code,U_LineID, U_BaseNum,U_Series,U_StepCode, U_Task,U_LastUpdateDate,U_DefaultService,U_Frequency,U_FrequencyDay,U_Remarks,U_Action,U_Duration,U_PreparedDate,U_PreparedTDate,U_SFromTime) Values  ('" & _
                        sResourceCode + i.ToString() & "','" & sResourceCode + i.ToString() & "','" & sResourceCode & "','" & i.ToString() & "','" & sResourceCode & "','" & sSeries & "','" & sStepCode & "','" & sTask & "','" & (sDate) & "','" & sDefaultService & "','" & sFrequency & "','" & sFrequencyDay & "','" & U_Remarks & "','" & U_Action & "','" & Duration & "','" & (PreparedDate) & "','" & (PreparedTDate) & "','" & sSFromTime & "' )"
                    End If
                End If

            Next
            If strSQL <> String.Empty Then
                rsetCode.DoQuery(strSQL)
                strSQL = String.Empty
            End If

            Return True
        Catch ex As Exception
            StatusBarWarningMsg("InsertDetails_Mech Details Method Failed : " & ex.Message)
            Return False
        End Try
    End Function

    Function InsertDetails_Elec() As Boolean
        Try
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim
            Dim rsetCode As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim strSQL As String = String.Empty
            strSQL = "Delete from [@AIS_RSC11] Where U_BaseNum='" & sResourceCode & "'"
            rsetCode.DoQuery(strSQL)
            strSQL = String.Empty
            If rsetCode.RecordCount > 0 Then
                rsetCode.MoveFirst()
            End If


            Dim SaveDataTableRecords As SAPbouiCOM.DataTable
            SaveDataTableRecords = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms2")


            For i As Integer = 0 To SaveDataTableRecords.Rows.Count - 1

                Dim sTask As String = SaveDataTableRecords.GetValue("Task", i).ToString
                If sTask <> String.Empty Then

                    Dim sDefaultService As String = SaveDataTableRecords.GetValue("Default Service", i).ToString
                    Dim sRemarks As String = SaveDataTableRecords.GetValue("Remarks", i).ToString
                    Dim sAction As String = SaveDataTableRecords.GetValue("Action", i).ToString
                    Dim sDuration As String = SaveDataTableRecords.GetValue("Duration", i).ToString
                    Dim sFrequency As String = SaveDataTableRecords.GetValue("Frequency", i).ToString
                    Dim sFrequencyDay As String = SaveDataTableRecords.GetValue("FrequencyDay", i).ToString

                    Dim sLastUpdateDate As String = String.Empty
                    If SaveDataTableRecords.GetValue("Last Update Date", i) <> Nothing Then
                        sLastUpdateDate = GetDateAsstrign(SaveDataTableRecords.GetValue("Last Update Date", i).ToString)
                    End If

                    Dim sPreparedDate As String = String.Empty
                    If SaveDataTableRecords.GetValue("From Date", i) <> Nothing Then
                        sPreparedDate = GetDateAsstrign(SaveDataTableRecords.GetValue("From Date", i).ToString)
                    End If
                    Dim sPreparedTDate As String = String.Empty
                    If SaveDataTableRecords.GetValue("To Date", i) <> Nothing Then
                        sPreparedTDate = GetDateAsstrign(SaveDataTableRecords.GetValue("To Date", i).ToString)
                    End If
                    Dim sStepCode As String = SaveDataTableRecords.GetValue("Step Code", i).ToString
                    Dim sSFromTime As String = SaveDataTableRecords.GetValue("Total Time", i).ToString
                    'Dim sSToTime As String = SaveDataTableRecords.GetValue("Schedule To Time", i).ToString
                    If SaveDataTableRecords.GetValue("Task", i).ToString <> String.Empty Then
                        strSQL = strSQL + "Insert into [@AIS_RSC11] (Code,Name,U_Code,U_LineID, U_BaseNum,U_Series,U_StepCode, U_Task,U_LastUpdateDate,U_DefaultService,U_Remarks,U_Action,U_Duration,U_Frequency,U_FrequencyDay,U_PreparedDate,U_PreparedTDate,U_SFromTime) Values  ('" & _
                        sResourceCode + i.ToString() & "','" & sResourceCode + i.ToString() & "','" & sResourceCode & "','" & i.ToString() & "','" & sResourceCode & "','" & sSeries & "','" & sStepCode & "','" & sTask & "','" & sLastUpdateDate & "','" & sDefaultService & "','" & sRemarks & "','" & sAction & "','" & sDuration & "','" & sFrequency & "','" & sFrequencyDay & "','" & sPreparedDate & "','" & sPreparedTDate & "','" & sSFromTime & "')"
                    End If
                End If
            Next
            If strSQL <> String.Empty Then
                rsetCode.DoQuery(strSQL)
                strSQL = String.Empty
            End If



            Return True
        Catch ex As Exception
            StatusBarWarningMsg("InsertDetails_Elec Details Method Failed : " & ex.Message)
            Return False
        End Try
    End Function

    Function GetDateAsstrign(FromDate As String) As String
        Dim PreparedDate As String = String.Empty
        Dim strSQL_Date As String = String.Empty
        Dim rsetCode As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

        strSQL_Date = " Select CONVERT(varchar(10),  '" & CDate(FromDate).ToString("yyyyMMdd") & "',103) "
        rsetCode.DoQuery(strSQL_Date)
        If rsetCode.RecordCount > 0 Then
            PreparedDate = rsetCode.Fields.Item(0).Value.ToString()
        End If
        Return PreparedDate
    End Function
    Function InsertDetails_Operator() As Boolean
        Try
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim
            Dim rsetCode As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim strSQL As String = String.Empty
            strSQL = "Delete from [@AIS_RSC12] Where U_BaseNum='" & sResourceCode & "'"
            rsetCode.DoQuery(strSQL)
            strSQL = String.Empty
            If rsetCode.RecordCount > 0 Then
                rsetCode.MoveFirst()
            End If


            Dim SaveDataTableRecords As SAPbouiCOM.DataTable
            SaveDataTableRecords = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms3")


            For i As Integer = 0 To SaveDataTableRecords.Rows.Count - 1

                Dim sTask As String = SaveDataTableRecords.GetValue("Task", i).ToString
                If sTask <> String.Empty Then

                    Dim sDefaultService As String = SaveDataTableRecords.GetValue("Default Service", i).ToString
                    Dim sRemarks As String = SaveDataTableRecords.GetValue("Remarks", i).ToString
                    Dim sAction As String = SaveDataTableRecords.GetValue("Action", i).ToString
                    Dim sDuration As String = SaveDataTableRecords.GetValue("Duration", i).ToString
                    Dim sFrequency As String = SaveDataTableRecords.GetValue("Frequency", i).ToString
                    Dim sFrequencyDay As String = SaveDataTableRecords.GetValue("FrequencyDay", i).ToString
                  
                    Dim sLastUpdateDate As String = String.Empty
                    If SaveDataTableRecords.GetValue("Last Update Date", i) <> Nothing Then
                        sLastUpdateDate = GetDateAsstrign(SaveDataTableRecords.GetValue("Last Update Date", i).ToString)
                    End If

                    Dim sPreparedDate As String = String.Empty
                    If SaveDataTableRecords.GetValue("From Date", i) <> Nothing Then
                        sPreparedDate = GetDateAsstrign(SaveDataTableRecords.GetValue("From Date", i).ToString)
                    End If
                    Dim sPreparedTDate As String = String.Empty
                    If SaveDataTableRecords.GetValue("To Date", i) <> Nothing Then
                        sPreparedTDate = GetDateAsstrign(SaveDataTableRecords.GetValue("To Date", i).ToString)
                    End If


                    Dim sStepCode As String = SaveDataTableRecords.GetValue("Step Code", i).ToString
                    Dim sSFromTime As String = SaveDataTableRecords.GetValue("Total Time", i).ToString
                    If SaveDataTableRecords.GetValue("Task", i).ToString <> String.Empty Then
                        strSQL = strSQL + "Insert into [@AIS_RSC12] (Code,Name,U_Code,U_LineID, U_BaseNum,U_Series,U_StepCode, U_Task,U_LastUpdateDate,U_DefaultService,U_Remarks,U_Action,U_Duration,U_Frequency,U_FrequencyDay,U_PreparedDate,U_PreparedTDate,U_SFromTime,U_SToTime) Values  ('" & _
                        sResourceCode + i.ToString() & "','" & sResourceCode + i.ToString() & "','" & sResourceCode & "','" & i.ToString() & "','" & sResourceCode & "','" & sSeries & "','" & sStepCode & "','" & sTask & "','" & sLastUpdateDate & "','" & sDefaultService & "','" & sRemarks & "','" & sAction & "','" & sDuration & "','" & sFrequency & "','" & sFrequencyDay & "','" & sPreparedDate & "','" & sPreparedTDate & "','" & sSFromTime & "','" & sSFromTime & "')"
                    End If
                End If
            Next
            If strSQL <> String.Empty Then
                rsetCode.DoQuery(strSQL)
                strSQL = String.Empty
            End If



            Return True
        Catch ex As Exception
            StatusBarWarningMsg("InsertDetails_Operator Details Method Failed : " & ex.Message)
            Return False
        End Try
    End Function
    Function InsertDetails_MCSpares() As Boolean
        Try
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim
            Dim rsetCode As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim strSQL As String = String.Empty
            strSQL = "Delete from [@AIS_RSC10] Where U_BaseNum='" & sResourceCode & "'"
            rsetCode.DoQuery(strSQL)
            strSQL = String.Empty
            If rsetCode.RecordCount > 0 Then
                rsetCode.MoveFirst()
            End If


            Dim SaveDataTableRecords As SAPbouiCOM.DataTable
            SaveDataTableRecords = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms4")


            For i As Integer = 0 To SaveDataTableRecords.Rows.Count - 1

                Dim sSerialNo As String = SaveDataTableRecords.GetValue("SerialNo", i).ToString
                If sSerialNo <> String.Empty Then
                    Dim sMainSpareCode As String = SaveDataTableRecords.GetValue("MainSpareCode", i).ToString
                    Dim sMainSpareName As String = SaveDataTableRecords.GetValue("MainSpareName", i).ToString
                    Dim sSubSpareCode As String = SaveDataTableRecords.GetValue("SubSpareCode", i).ToString
                    Dim sSubSpareName As String = SaveDataTableRecords.GetValue("SubSpareName", i).ToString
                    Dim sLastChangeDate As String = String.Empty
                    If SaveDataTableRecords.GetValue("LastChangeDate", i) <> Nothing Then
                        sLastChangeDate = GetDateAsstrign(SaveDataTableRecords.GetValue("LastChangeDate", i).ToString)
                    End If

                    Dim sVerifiedBy As String = SaveDataTableRecords.GetValue("VerifiedBy", i).ToString
                    Dim sAMC As String = SaveDataTableRecords.GetValue("AMC", i).ToString
                    Dim sSpareType As String = SaveDataTableRecords.GetValue("SpareType", i).ToString
                    If SaveDataTableRecords.GetValue("SerialNo", i).ToString <> String.Empty Then
                        strSQL = strSQL + "Insert into [@AIS_RSC10] (Code,Name,U_Code,U_LineID, U_BaseNum,U_Series, U_SerialNo,U_MainSpareCode,U_MainSpareName,U_LastChangeDate,U_VerifiedBy,U_AMC,U_SpareType,U_SubSpareCode,U_SubSpareName) Values  ('" & _
                         sResourceCode + i.ToString() & "','" & sResourceCode + i.ToString() & "','" & sResourceCode & "','" & i.ToString() & "','" & sResourceCode & "','" & sSeries & "','" & sSerialNo & "','" & sMainSpareCode & "','" & sMainSpareName & "','" & sLastChangeDate & "','" & sVerifiedBy & "','" & sAMC & "','" & sSpareType & "','" & sSubSpareCode & "','" & sSubSpareName & "')"
                    End If
                End If
            Next
            If strSQL <> String.Empty Then
                rsetCode.DoQuery(strSQL)
                strSQL = String.Empty
            End If



            Return True
        Catch ex As Exception
            StatusBarWarningMsg("InsertDetails_MCSpares Details Method Failed : " & ex.Message)
            Return False
        End Try
    End Function


    Function InsertDetails_MachineInformationDetails() As Boolean
        Try
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim
            Dim rsetCode As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim strSQL As String = String.Empty
            strSQL = "Delete from [@AIS_RSC13] Where U_BaseNum='" & sResourceCode & "'"
            rsetCode.DoQuery(strSQL)
            strSQL = String.Empty
            If rsetCode.RecordCount > 0 Then
                rsetCode.MoveFirst()
            End If


            Dim SaveDataTableRecords As SAPbouiCOM.DataTable
            SaveDataTableRecords = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms13")


            For i As Integer = 0 To SaveDataTableRecords.Rows.Count - 1

                Dim sSerialNo As String = SaveDataTableRecords.GetValue("VendorCode", i).ToString
                If sSerialNo <> String.Empty Then

                    Dim sManufacturing As String = String.Empty
                    If SaveDataTableRecords.GetValue("Manufacturing", i) <> Nothing Then
                        sManufacturing = GetDateAsstrign(SaveDataTableRecords.GetValue("Manufacturing", i).ToString)
                    End If

                    Dim sInsulationDate As String = String.Empty
                    If SaveDataTableRecords.GetValue("InsulationDate", i) <> Nothing Then
                        sInsulationDate = GetDateAsstrign(SaveDataTableRecords.GetValue("InsulationDate", i).ToString)
                    End If

                    Dim sTechnicalSupp As String = SaveDataTableRecords.GetValue("TechnicalSupp", i).ToString
                    Dim sBudget As String = SaveDataTableRecords.GetValue("Budget", i).ToString
                    strSQL = strSQL + "Insert into [@AIS_RSC13] (Code,Name,U_Code,U_LineID, U_BaseNum,U_Series, U_VendorCode,U_Manufacturing,U_InsulationDate,U_TechnicalSupp,U_Budget) Values  ('" & _
                    sResourceCode + i.ToString() & "','" & sResourceCode + i.ToString() & "','" & sResourceCode & "','" & i.ToString() & "','" & sResourceCode & "','" & sSeries & "','" & sSerialNo & "','" & sManufacturing & "','" & sInsulationDate & "','" & sTechnicalSupp & "','" & sBudget & "')"

                End If
            Next
            If strSQL <> String.Empty Then
                rsetCode.DoQuery(strSQL)
                strSQL = String.Empty
            End If



            Return True
        Catch ex As Exception
            StatusBarWarningMsg("InsertDetails_MachineInformationDetails Details Method Failed : " & ex.Message)
            Return False
        End Try
    End Function


    Sub DynamicControl()
        Try

            Dim oItem As SAPbouiCOM.Item = Nothing
            Dim oButton As SAPbouiCOM.Button = Nothing
            Dim oText As SAPbouiCOM.EditText = Nothing
            Dim oLabel As SAPbouiCOM.StaticText = Nothing
            Dim oEditText As SAPbouiCOM.EditText = Nothing
            Dim oComboBox As SAPbouiCOM.ComboBox = Nothing
            Dim oFolder As SAPbouiCOM.Folder = Nothing
            Dim oItemRef As SAPbouiCOM.Item = Nothing
            Dim strSQL As String

            strSQL = "select ItemCode, ItemName ,0.00 as CycleTime from OITM Where 1=2"
            frmGeneralSeetings.DataSources.DataTables.Add("DT_Terms")
            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms").ExecuteQuery(strSQL)


            oItemRef = frmGeneralSeetings.Items.Item("1470000030")

            oItem = frmGeneralSeetings.Items.Add("Terms", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItem.Top = oItemRef.Top
            oItem.Height = oItemRef.Height
            oItem.Left = oItemRef.Left + oItemRef.Width
            oItem.Width = oItemRef.Width
            oItem.Visible = True
            oFolder = oItem.Specific
            oFolder.Caption = "Product Cycle Time"
            oFolder.GroupWith(oItemRef.UniqueID)
            oFolder.Pane = 300

            Dim oGrid As SAPbouiCOM.Grid
            oItem = frmGeneralSeetings.Items.Add("m_terms", SAPbouiCOM.BoFormItemTypes.it_GRID)
            oItem.Top = frmGeneralSeetings.Items.Item("1470000133").Top
            oItem.Left = frmGeneralSeetings.Items.Item("1470000133").Left
            oItem.Width = frmGeneralSeetings.Items.Item("1470000133").Width
            oItem.Height = frmGeneralSeetings.Items.Item("1470000133").Height
            oItem.FromPane = 300
            oItem.ToPane = 300
            oGrid = oItem.Specific
            'oItem.FromPane = oFolder.Pane
            'oItem.ToPane = oFolder.Pane
            oGrid.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms")
            oGrid.DataTable.Rows.Add()
            'oMatrix_Terms.Columns.Item(0).Visible = False
            'oMatrix_Terms.Columns.Item(1).Visible = False
            '    oMatrix_Terms.Columns.Item(2).Visible = False


            oGrid.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(1).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single

            Dim oCFLsAcceptedWarehouse As SAPbouiCOM.ChooseFromListCollection = frmGeneralSeetings.ChooseFromLists
            Dim oItemCodeColAccepted As SAPbouiCOM.EditTextColumn
            oItemCodeColAccepted = CType(oGrid.Columns.Item(1), SAPbouiCOM.EditTextColumn)
            Dim oCFLCreationParamsAccepted As SAPbouiCOM.ChooseFromListCreationParams = oApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_ChooseFromListCreationParams)
            oCFLCreationParamsAccepted.MultiSelection = False
            oCFLCreationParamsAccepted.ObjectType = "4"
            oCFLCreationParamsAccepted.UniqueID = "CFL_OITM"
            Try
                Dim oCFLAccepted As SAPbouiCOM.ChooseFromList = oCFLsAcceptedWarehouse.Add(oCFLCreationParamsAccepted)
            Catch ex As Exception
            End Try
            oGrid.Columns.Item(0).ChooseFromListUID = "CFL_OITM"
            oItemCodeColAccepted.LinkedObjectType = "-1"
            oGrid.Columns.Item(0).TitleObject.Sortable = True
            oGrid.Columns.Item(1).TitleObject.Sortable = True
            oGrid.Columns.Item(2).TitleObject.Sortable = True


            'C:\Program Files\SAP\SAP Business One SDK\Samples\COM UI\VB.NET\11.SystemFormManipulation



        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control   Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try




    End Sub
    Sub DynamicControlCapacity()
        Try

            Dim oItem As SAPbouiCOM.Item = Nothing
            Dim oButton As SAPbouiCOM.Button = Nothing
            Dim oText As SAPbouiCOM.EditText = Nothing
            Dim oLabel As SAPbouiCOM.StaticText = Nothing
            Dim oEditText As SAPbouiCOM.EditText = Nothing
            Dim oComboBox As SAPbouiCOM.ComboBox = Nothing
            Dim oFolder As SAPbouiCOM.Folder = Nothing
            Dim oItemRef As SAPbouiCOM.Item = Nothing


            frmGeneralSeetings.DataSources.DataTables.Add("DT_Empty")

            oItemRef = frmGeneralSeetings.Items.Item("Terms")

            oItem = frmGeneralSeetings.Items.Add("Empty", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItem.Top = oItemRef.Top
            oItem.Height = oItemRef.Height
            oItem.Left = oItemRef.Left + oItemRef.Width
            oItem.Width = oItemRef.Width
            oItem.Visible = True
            oFolder = oItem.Specific
            oFolder.Caption = "Capacity"
            oFolder.GroupWith(frmGeneralSeetings.Items.Item("Terms").UniqueID)
            'oFolder.GroupWith(oItemRef.UniqueID)

            oFolder.Pane = 400
            Dim oGrid As SAPbouiCOM.Grid
            oItem = frmGeneralSeetings.Items.Add("m_terms1", SAPbouiCOM.BoFormItemTypes.it_GRID)
            oItem.Top = frmGeneralSeetings.Items.Item("1470000133").Top
            oItem.Left = frmGeneralSeetings.Items.Item("1470000133").Left
            oItem.Width = frmGeneralSeetings.Items.Item("1470000133").Width
            oItem.Height = frmGeneralSeetings.Items.Item("1470000133").Height
            oItem.FromPane = 400
            oItem.ToPane = 400
            oGrid = oItem.Specific

        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Capacity  Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub ItemButton()
        Try
            Dim oItem As SAPbouiCOM.Item = Nothing
            Dim oItemRef As SAPbouiCOM.Item = Nothing
            Dim oButton As SAPbouiCOM.Button
            frmGeneralSeetings.Freeze(True)
            oItemRef = frmGeneralSeetings.Items.Item("1470000002")

            oItem = frmGeneralSeetings.Items.Add("GetTask", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = oItemRef.Left + oItemRef.Width + 4
            oItem.Width = 100
            oItem.Height = oItemRef.Height
            oItem.Top = oItemRef.Top

            oButton = oItem.Specific
            oButton.Caption = "Get Task Operator"


            oItem = frmGeneralSeetings.Items.Add("GetMachine", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = oItemRef.Left + oItemRef.Width + 108
            oItem.Width = 100
            oItem.Height = oItemRef.Height
            oItem.Top = oItemRef.Top

            oButton = oItem.Specific
            oButton.Caption = "Get Task Machine"


            oItem = frmGeneralSeetings.Items.Add("GetElect", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = oItemRef.Left + oItemRef.Width + 210
            oItem.Width = 100
            oItem.Height = oItemRef.Height
            oItem.Top = oItemRef.Top

            oButton = oItem.Specific
            oButton.Caption = "Get Task Electrical"




        Catch ex As Exception
            'objAddOn.objApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmGeneralSeetings.Freeze(False)
        End Try
    End Sub
    Sub DynamicControlMech()
        Try

            Dim oItem As SAPbouiCOM.Item = Nothing
            Dim oButton As SAPbouiCOM.Button = Nothing
            Dim oText As SAPbouiCOM.EditText = Nothing
            Dim oLabel As SAPbouiCOM.StaticText = Nothing
            Dim oEditText As SAPbouiCOM.EditText = Nothing
            Dim oComboBox As SAPbouiCOM.ComboBox = Nothing
            Dim oFolder As SAPbouiCOM.Folder = Nothing
            Dim oItemRef As SAPbouiCOM.Item = Nothing
            Dim strSQL As String
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim

            strSQL = "EXEC [dbo].[@AIS_Main_ResourceMaster_LoadCheckListDetails] 'M','" + sResourceCode + "'"

            frmGeneralSeetings.DataSources.DataTables.Add("DT_Terms1")
            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms1").ExecuteQuery(strSQL)


            oItemRef = frmGeneralSeetings.Items.Item("Empty")

            oItem = frmGeneralSeetings.Items.Add("Terms1", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItem.Top = oItemRef.Top
            oItem.Height = oItemRef.Height
            oItem.Left = oItemRef.Left + oItemRef.Width
            oItem.Width = oItemRef.Width
            oItem.Visible = True
            oFolder = oItem.Specific
            oFolder.Caption = "Mech. Check List"
            oFolder.GroupWith(oItemRef.UniqueID)
            oFolder.Pane = 500

            Dim oGrid As SAPbouiCOM.Grid
            oItem = frmGeneralSeetings.Items.Add("m_terms2", SAPbouiCOM.BoFormItemTypes.it_GRID)
            oItem.Top = frmGeneralSeetings.Items.Item("1470000133").Top
            oItem.Left = frmGeneralSeetings.Items.Item("1470000133").Left
            oItem.Width = frmGeneralSeetings.Items.Item("1470000133").Width
            oItem.Height = frmGeneralSeetings.Items.Item("1470000133").Height
            oItem.FromPane = 500
            oItem.ToPane = 500
            oGrid = oItem.Specific
            'oItem.FromPane = oFolder.Pane
            'oItem.ToPane = oFolder.Pane
            oGrid.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms1")
            oGrid.DataTable.Rows.Add()
            'oMatrix_Terms.Columns.Item(0).Visible = False
            'oMatrix_Terms.Columns.Item(1).Visible = False
            '    oMatrix_Terms.Columns.Item(2).Visible = False


            oGrid.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(1).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(2).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(3).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oGrid.Columns.Item(4).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(5).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(6).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(7).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oGrid.Columns.Item(8).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(9).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(10).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single


            Dim ocbc_GetDefaultService As SAPbouiCOM.ComboBoxColumn

            ocbc_GetDefaultService = CType(oGrid.Columns.Item(3), SAPbouiCOM.ComboBoxColumn)
            Dim strQuery As String = ""
            Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strQuery = "[@AIS_Maintenance_GetDefaultService]"
            rsetQuery.DoQuery(strQuery)
            If ocbc_GetDefaultService.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc_GetDefaultService.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc_GetDefaultService.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description



            Dim ocbc As SAPbouiCOM.ComboBoxColumn
            ocbc = CType(oGrid.Columns.Item(7), SAPbouiCOM.ComboBoxColumn)

            strQuery = "[@AIS_Maintenance_GetFrequencyType]"
            rsetQuery.DoQuery(strQuery)
            If ocbc.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description



        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Mech Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try




    End Sub

    Sub DynamicControlMechGetTask()
        Try
            Dim oItem As SAPbouiCOM.Item = Nothing
            Dim oButton As SAPbouiCOM.Button = Nothing
            Dim oText As SAPbouiCOM.EditText = Nothing
            Dim oLabel As SAPbouiCOM.StaticText = Nothing
            Dim oEditText As SAPbouiCOM.EditText = Nothing
            Dim oComboBox As SAPbouiCOM.ComboBox = Nothing
            Dim oFolder As SAPbouiCOM.Folder = Nothing
            Dim oItemRef As SAPbouiCOM.Item = Nothing
            Dim strSQL As String
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim

            strSQL = "EXEC [dbo].[@AIS_Main_ResourceMaster_LoadCheckListDetails] 'M','" + sResourceCode + "'"

            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms1").ExecuteQuery(strSQL)

            Dim oMatrix_Terms As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms2").Specific
            oMatrix_Terms.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms1")


            Dim strQuery As String
            Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            oMatrix_Terms.Columns.Item(7).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox

            Dim ocbc As SAPbouiCOM.ComboBoxColumn
            ocbc = CType(oMatrix_Terms.Columns.Item(7), SAPbouiCOM.ComboBoxColumn)

            strQuery = "[@AIS_Maintenance_GetFrequencyType]"
            rsetQuery.DoQuery(strQuery)
            If ocbc.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description

           


        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Mech Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try




    End Sub
    Sub DynamicControl_MCInformation()
        Try

            Dim oItem As SAPbouiCOM.Item = Nothing
            Dim oButton As SAPbouiCOM.Button = Nothing
            Dim oText As SAPbouiCOM.EditText = Nothing
            Dim oLabel As SAPbouiCOM.StaticText = Nothing
            Dim oEditText As SAPbouiCOM.EditText = Nothing
            Dim oComboBox As SAPbouiCOM.ComboBox = Nothing
            Dim oFolder As SAPbouiCOM.Folder = Nothing
            Dim oItemRef As SAPbouiCOM.Item = Nothing
            Dim strSQL As String

            strSQL = "select  U_VendorCode as VendorCode, U_Manufacturing Manufacturing ,U_InsulationDate InsulationDate,U_TechnicalSupp TechnicalSupp ,U_Budget Budget   from [@AIS_RSC13] where 1=2"

            frmGeneralSeetings.DataSources.DataTables.Add("DT_Terms13")
            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms13").ExecuteQuery(strSQL)


            oItemRef = frmGeneralSeetings.Items.Item("Terms1")

            oItem = frmGeneralSeetings.Items.Add("Terms13", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItem.Top = oItemRef.Top
            oItem.Height = oItemRef.Height
            oItem.Left = oItemRef.Left + oItemRef.Width
            oItem.Width = oItemRef.Width
            oItem.Visible = True
            oFolder = oItem.Specific
            oFolder.Caption = "MC Information"
            oFolder.GroupWith(oItemRef.UniqueID)
            oFolder.Pane = 900

            Dim oGrid As SAPbouiCOM.Grid
            oItem = frmGeneralSeetings.Items.Add("m_terms13", SAPbouiCOM.BoFormItemTypes.it_GRID)
            oItem.Top = frmGeneralSeetings.Items.Item("1470000133").Top
            oItem.Left = frmGeneralSeetings.Items.Item("1470000133").Left
            oItem.Width = frmGeneralSeetings.Items.Item("1470000133").Width
            oItem.Height = frmGeneralSeetings.Items.Item("1470000133").Height
            oItem.FromPane = 900
            oItem.ToPane = 900
            oGrid = oItem.Specific
            'oItem.FromPane = oFolder.Pane
            'oItem.ToPane = oFolder.Pane
            oGrid.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms13")
            oGrid.DataTable.Rows.Add()


            oGrid.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(1).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(2).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(4).Type = SAPbouiCOM.BoGridColumnType.gct_EditText

            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single



        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Mech Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try




    End Sub

    Sub DynamicControlElec()
        Try

            Dim oItem As SAPbouiCOM.Item = Nothing
            Dim oButton As SAPbouiCOM.Button = Nothing
            Dim oText As SAPbouiCOM.EditText = Nothing
            Dim oLabel As SAPbouiCOM.StaticText = Nothing
            Dim oEditText As SAPbouiCOM.EditText = Nothing
            Dim oComboBox As SAPbouiCOM.ComboBox = Nothing
            Dim oFolder As SAPbouiCOM.Folder = Nothing
            Dim oItemRef As SAPbouiCOM.Item = Nothing
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim strSQL As String
            strSQL = "EXEC [dbo].[@AIS_Main_ResourceMaster_LoadCheckListDetails] 'E','" + sResourceCode + "'"
            frmGeneralSeetings.DataSources.DataTables.Add("DT_Terms2")
            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms2").ExecuteQuery(strSQL)


            oItemRef = frmGeneralSeetings.Items.Item("Terms1")

            oItem = frmGeneralSeetings.Items.Add("Terms2", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItem.Top = oItemRef.Top
            oItem.Height = oItemRef.Height
            oItem.Left = oItemRef.Left + oItemRef.Width
            oItem.Width = oItemRef.Width
            oItem.Visible = True
            oFolder = oItem.Specific
            oFolder.Caption = "Elec. Check List"
            oFolder.GroupWith(oItemRef.UniqueID)
            oFolder.Pane = 600

            Dim oGrid As SAPbouiCOM.Grid
            oItem = frmGeneralSeetings.Items.Add("m_terms3", SAPbouiCOM.BoFormItemTypes.it_GRID)
            oItem.Top = frmGeneralSeetings.Items.Item("1470000133").Top
            oItem.Left = frmGeneralSeetings.Items.Item("1470000133").Left
            oItem.Width = frmGeneralSeetings.Items.Item("1470000133").Width
            oItem.Height = frmGeneralSeetings.Items.Item("1470000133").Height
            oItem.FromPane = 600
            oItem.ToPane = 600
            oGrid = oItem.Specific
            'oItem.FromPane = oFolder.Pane
            'oItem.ToPane = oFolder.Pane
            oGrid.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms2")
            oGrid.DataTable.Rows.Add()
            'oMatrix_Terms.Columns.Item(0).Visible = False
            'oMatrix_Terms.Columns.Item(1).Visible = False
            '    oMatrix_Terms.Columns.Item(2).Visible = False


            oGrid.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(1).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(2).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(3).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oGrid.Columns.Item(4).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(5).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(6).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(7).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oGrid.Columns.Item(8).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(9).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(10).Type = SAPbouiCOM.BoGridColumnType.gct_EditText

            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single


            Dim ocbc_GetDefaultService As SAPbouiCOM.ComboBoxColumn
            ocbc_GetDefaultService = CType(oGrid.Columns.Item(3), SAPbouiCOM.ComboBoxColumn)
            Dim strQuery As String = ""
            Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strQuery = "[@AIS_Maintenance_GetDefaultService]"
            rsetQuery.DoQuery(strQuery)
            If ocbc_GetDefaultService.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc_GetDefaultService.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc_GetDefaultService.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description





            Dim ocbc As SAPbouiCOM.ComboBoxColumn
            ocbc = CType(oGrid.Columns.Item(7), SAPbouiCOM.ComboBoxColumn)

            strQuery = "[@AIS_Maintenance_GetFrequencyType]"
            rsetQuery.DoQuery(strQuery)
            If ocbc.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description


            oGrid.Columns.Item(0).TitleObject.Sortable = True
            oGrid.Columns.Item(1).TitleObject.Sortable = True
            oGrid.Columns.Item(2).TitleObject.Sortable = True
            oGrid.Columns.Item(3).TitleObject.Sortable = True
            oGrid.Columns.Item(4).TitleObject.Sortable = True
            oGrid.Columns.Item(5).TitleObject.Sortable = True
            oGrid.Columns.Item(6).TitleObject.Sortable = True
            oGrid.Columns.Item(7).TitleObject.Sortable = True
            oGrid.Columns.Item(8).TitleObject.Sortable = True
            oGrid.Columns.Item(9).TitleObject.Sortable = True


            'C:\Program Files\SAP\SAP Business One SDK\Samples\COM UI\VB.NET\11.SystemFormManipulation



        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Elec  Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub DynamicControlElecGetTask()
        Try

            Dim oItem As SAPbouiCOM.Item = Nothing
            Dim oButton As SAPbouiCOM.Button = Nothing
            Dim oText As SAPbouiCOM.EditText = Nothing
            Dim oLabel As SAPbouiCOM.StaticText = Nothing
            Dim oEditText As SAPbouiCOM.EditText = Nothing
            Dim oComboBox As SAPbouiCOM.ComboBox = Nothing
            Dim oFolder As SAPbouiCOM.Folder = Nothing
            Dim oItemRef As SAPbouiCOM.Item = Nothing
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim strSQL As String
            strSQL = "EXEC [dbo].[@AIS_Main_ResourceMaster_LoadCheckListDetails] 'E','" + sResourceCode + "'"

            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms2").ExecuteQuery(strSQL)

            Dim oMatrix_Terms As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms3").Specific
            oMatrix_Terms.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms2")



            Dim strQuery As String
            Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            oMatrix_Terms.Columns.Item(7).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox

            Dim ocbc As SAPbouiCOM.ComboBoxColumn
            ocbc = CType(oMatrix_Terms.Columns.Item(7), SAPbouiCOM.ComboBoxColumn)

            strQuery = "[@AIS_Maintenance_GetFrequencyType]"
            rsetQuery.DoQuery(strQuery)
            If ocbc.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description


        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Elec  Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub DynamicControlOperator()
        Try

            Dim oItem As SAPbouiCOM.Item = Nothing
            Dim oButton As SAPbouiCOM.Button = Nothing
            Dim oText As SAPbouiCOM.EditText = Nothing
            Dim oLabel As SAPbouiCOM.StaticText = Nothing
            Dim oEditText As SAPbouiCOM.EditText = Nothing
            Dim oComboBox As SAPbouiCOM.ComboBox = Nothing
            Dim oFolder As SAPbouiCOM.Folder = Nothing
            Dim oItemRef As SAPbouiCOM.Item = Nothing
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim strSQL As String

            strSQL = "EXEC [dbo].[@AIS_Main_ResourceMaster_LoadCheckListDetails] 'O','" + sResourceCode + "'"
            frmGeneralSeetings.DataSources.DataTables.Add("DT_Terms3")
            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms3").ExecuteQuery(strSQL)


            oItemRef = frmGeneralSeetings.Items.Item("Terms2")

            oItem = frmGeneralSeetings.Items.Add("Terms3", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItem.Top = oItemRef.Top
            oItem.Height = oItemRef.Height
            oItem.Left = oItemRef.Left + oItemRef.Width
            oItem.Width = oItemRef.Width
            oItem.Visible = True
            oFolder = oItem.Specific
            oFolder.Caption = "Operator Check List"
            oFolder.GroupWith(oItemRef.UniqueID)
            oFolder.Pane = 700

            Dim oGrid As SAPbouiCOM.Grid
            oItem = frmGeneralSeetings.Items.Add("m_terms4", SAPbouiCOM.BoFormItemTypes.it_GRID)
            oItem.Top = frmGeneralSeetings.Items.Item("1470000133").Top
            oItem.Left = frmGeneralSeetings.Items.Item("1470000133").Left
            oItem.Width = frmGeneralSeetings.Items.Item("1470000133").Width
            oItem.Height = frmGeneralSeetings.Items.Item("1470000133").Height
            oItem.FromPane = 700
            oItem.ToPane = 700
            oGrid = oItem.Specific
            'oItem.FromPane = oFolder.Pane
            'oItem.ToPane = oFolder.Pane
            oGrid.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms3")
            oGrid.DataTable.Rows.Add()
            'oMatrix_Terms.Columns.Item(0).Visible = False
            'oMatrix_Terms.Columns.Item(1).Visible = False
            '    oMatrix_Terms.Columns.Item(2).Visible = False


            oGrid.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(1).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(2).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(3).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oGrid.Columns.Item(4).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(5).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(6).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(7).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oGrid.Columns.Item(8).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(9).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(10).Type = SAPbouiCOM.BoGridColumnType.gct_EditText

            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single


            Dim ocbc_GetDefaultService As SAPbouiCOM.ComboBoxColumn
            ocbc_GetDefaultService = CType(oGrid.Columns.Item(3), SAPbouiCOM.ComboBoxColumn)
            Dim strQuery As String = ""
            Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strQuery = "[@AIS_Maintenance_GetDefaultService]"
            rsetQuery.DoQuery(strQuery)
            If ocbc_GetDefaultService.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc_GetDefaultService.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc_GetDefaultService.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description



            Dim ocbc As SAPbouiCOM.ComboBoxColumn
            ocbc = CType(oGrid.Columns.Item(7), SAPbouiCOM.ComboBoxColumn)

            strQuery = "[@AIS_Maintenance_GetFrequencyType]"
            rsetQuery.DoQuery(strQuery)
            If ocbc.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description



            oGrid.Columns.Item(0).TitleObject.Sortable = True
            oGrid.Columns.Item(1).TitleObject.Sortable = True
            oGrid.Columns.Item(2).TitleObject.Sortable = True
            oGrid.Columns.Item(3).TitleObject.Sortable = True
            oGrid.Columns.Item(4).TitleObject.Sortable = True
            oGrid.Columns.Item(5).TitleObject.Sortable = True
            oGrid.Columns.Item(6).TitleObject.Sortable = True
            oGrid.Columns.Item(7).TitleObject.Sortable = True
            oGrid.Columns.Item(8).TitleObject.Sortable = True
            oGrid.Columns.Item(9).TitleObject.Sortable = True

            'C:\Program Files\SAP\SAP Business One SDK\Samples\COM UI\VB.NET\11.SystemFormManipulation



        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Operator  Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try




    End Sub


    Sub DynamicControlOperatorGetTask()
        Try

            Dim oItem As SAPbouiCOM.Item = Nothing
            Dim oButton As SAPbouiCOM.Button = Nothing
            Dim oText As SAPbouiCOM.EditText = Nothing
            Dim oLabel As SAPbouiCOM.StaticText = Nothing
            Dim oEditText As SAPbouiCOM.EditText = Nothing
            Dim oComboBox As SAPbouiCOM.ComboBox = Nothing
            Dim oFolder As SAPbouiCOM.Folder = Nothing
            Dim oItemRef As SAPbouiCOM.Item = Nothing
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim strSQL As String

            strSQL = "EXEC [dbo].[@AIS_Main_ResourceMaster_LoadCheckListDetails] 'O','" + sResourceCode + "'"

            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms3").ExecuteQuery(strSQL)

            Dim oMatrix_Terms As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms4").Specific
            oMatrix_Terms.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms3")





            Dim strQuery As String
            Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            oMatrix_Terms.Columns.Item(7).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox

            Dim ocbc As SAPbouiCOM.ComboBoxColumn
            ocbc = CType(oMatrix_Terms.Columns.Item(7), SAPbouiCOM.ComboBoxColumn)

            strQuery = "[@AIS_Maintenance_GetFrequencyType]"
            rsetQuery.DoQuery(strQuery)
            If ocbc.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description
        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Operator  Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try




    End Sub
    Sub DynamicControlMCSpares()
        Try

            Dim oItem As SAPbouiCOM.Item = Nothing
            Dim oButton As SAPbouiCOM.Button = Nothing
            Dim oText As SAPbouiCOM.EditText = Nothing
            Dim oLabel As SAPbouiCOM.StaticText = Nothing
            Dim oEditText As SAPbouiCOM.EditText = Nothing
            Dim oComboBox As SAPbouiCOM.ComboBox = Nothing
            Dim oFolder As SAPbouiCOM.Folder = Nothing
            Dim oItemRef As SAPbouiCOM.Item = Nothing
            Dim strSQL As String

            strSQL = "Select  U_SerialNo as [SerialNo],U_MainSpareCode as [MainSpareCode],U_MainSpareName as [MainSpareName],U_LastChangeDate as [LastChangeDate],U_VerifiedBy as [VerifiedBy],U_AMC as [AMC],U_SpareType as [SpareType],U_SubSpareCode as [SubSpareCode],U_SubSpareName as [SubSpareName] From [@AIS_RSC10] where 1=2 "
            frmGeneralSeetings.DataSources.DataTables.Add("DT_Terms4")
            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms4").ExecuteQuery(strSQL)


            oItemRef = frmGeneralSeetings.Items.Item("Terms3")

            oItem = frmGeneralSeetings.Items.Add("Terms4", SAPbouiCOM.BoFormItemTypes.it_FOLDER)
            oItem.Top = oItemRef.Top
            oItem.Height = oItemRef.Height
            oItem.Left = oItemRef.Left + oItemRef.Width
            oItem.Width = oItemRef.Width
            oItem.Visible = True
            oFolder = oItem.Specific
            oFolder.Caption = "M/C Spares List"
            oFolder.GroupWith(oItemRef.UniqueID)
            oFolder.Pane = 800

            Dim oGrid As SAPbouiCOM.Grid
            oItem = frmGeneralSeetings.Items.Add("m_terms5", SAPbouiCOM.BoFormItemTypes.it_GRID)
            oItem.Top = frmGeneralSeetings.Items.Item("1470000133").Top
            oItem.Left = frmGeneralSeetings.Items.Item("1470000133").Left
            oItem.Width = frmGeneralSeetings.Items.Item("1470000133").Width
            oItem.Height = frmGeneralSeetings.Items.Item("1470000133").Height
            oItem.FromPane = 800
            oItem.ToPane = 800
            oGrid = oItem.Specific
            'oItem.FromPane = oFolder.Pane
            'oItem.ToPane = oFolder.Pane
            oGrid.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms4")
            oGrid.DataTable.Rows.Add()
            'oMatrix_Terms.Columns.Item(0).Visible = False
            'oMatrix_Terms.Columns.Item(1).Visible = False
            '    oMatrix_Terms.Columns.Item(2).Visible = False


            oGrid.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(1).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(2).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(3).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(4).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(5).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(6).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oGrid.Columns.Item(7).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(8).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single


            Dim ocbc As SAPbouiCOM.ComboBoxColumn
            ocbc = CType(oGrid.Columns.Item(6), SAPbouiCOM.ComboBoxColumn)
            Dim strQuery As String = ""
            Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strQuery = "[@AIS_Maintenance_GetSpareType]"
            rsetQuery.DoQuery(strQuery)
            If ocbc.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description



            oGrid.Columns.Item(0).TitleObject.Sortable = True
            oGrid.Columns.Item(1).TitleObject.Sortable = True
            oGrid.Columns.Item(2).TitleObject.Sortable = True
            oGrid.Columns.Item(3).TitleObject.Sortable = True
            oGrid.Columns.Item(4).TitleObject.Sortable = True
            oGrid.Columns.Item(5).TitleObject.Sortable = True
            oGrid.Columns.Item(6).TitleObject.Sortable = True
            oGrid.Columns.Item(7).TitleObject.Sortable = True
            oGrid.Columns.Item(8).TitleObject.Sortable = True

            'C:\Program Files\SAP\SAP Business One SDK\Samples\COM UI\VB.NET\11.SystemFormManipulation



        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control  MC Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try




    End Sub
    Sub ResizeForm()
        Try
            'Dim oItem As SAPbouiCOM.Item = Nothing
            'Dim oButton As SAPbouiCOM.Button = Nothing
            'Dim oText As SAPbouiCOM.EditText = Nothing
            'Dim oLabel As SAPbouiCOM.StaticText = Nothing
            'Dim oEditText As SAPbouiCOM.EditText = Nothing
            'Dim oComboBox As SAPbouiCOM.ComboBox = Nothing
            'Dim oFolder As SAPbouiCOM.Folder = Nothing
            'Dim oItemRef As SAPbouiCOM.Item = Nothing
            'Dim strSQL As String

            'frmGeneralSeetings = oApplication.Forms.ActiveForm
            'oItemRef = frmGeneralSeetings.Items.Item("7")
            'oItem = frmGeneralSeetings.Items.Item("Terms")
            'oItem.Top = oItemRef.Top
            'oItem.Height = oItemRef.Height
            'oItem.Left = oItemRef.Left + oItemRef.Width
            'oItem.Width = oItemRef.Width
            'oItem.Visible = True
            'oFolder = oItem.Specific
            'oFolder.Caption = "Terms Details"
            'oFolder.GroupWith(oItemRef.UniqueID)
            'oFolder.Pane = 215

            'Dim oMatrix_Terms As SAPbouiCOM.Grid
            'oItem = frmGeneralSeetings.Items.Item("m_terms")
            'oItem.Top = frmGeneralSeetings.Items.Item("92").Top
            'oItem.Left = frmGeneralSeetings.Items.Item("92").Left
            'oItem.Width = frmGeneralSeetings.Items.Item("92").Width
            'oItem.Height = frmGeneralSeetings.Items.Item("92").Height
            'oItem.FromPane = oFolder.Pane
            'oItem.ToPane = oFolder.Pane
            'oMatrix_Terms = oItem.Specific
            'oItem.FromPane = oFolder.Pane
            'oItem.ToPane = oFolder.Pane

            'oMatrix_Terms.Columns.Item(0).Visible = False
            'oMatrix_Terms.Columns.Item(1).Visible = False
            'oMatrix_Terms.Columns.Item(2).Visible = False

            ''--------------------------------------------------------------------------------------------------------------------------------------------------------------
            'oItemRef = frmGeneralSeetings.Items.Item("Terms")
            'oItem = frmGeneralSeetings.Items.Item("Scope")
            'oItem.Top = oItemRef.Top
            'oItem.Height = oItemRef.Height
            'oItem.Left = oItemRef.Left + oItemRef.Width
            'oItem.Width = oItemRef.Width
            'oItem.Visible = True
            'oFolder = oItem.Specific
            'oFolder.Caption = "Scope of Work"
            'oFolder.GroupWith(oItemRef.UniqueID)
            'oFolder.Pane = 216

            'Dim oMatrix_Scope As SAPbouiCOM.Grid
            'oItem = frmGeneralSeetings.Items.Item("m_Scope")
            'oItem.Top = frmGeneralSeetings.Items.Item("92").Top
            'oItem.Left = frmGeneralSeetings.Items.Item("92").Left
            'oItem.Width = frmGeneralSeetings.Items.Item("92").Width
            'oItem.Height = frmGeneralSeetings.Items.Item("92").Height
            'oItem.FromPane = oFolder.Pane
            'oItem.ToPane = oFolder.Pane
            'oMatrix_Scope = oItem.Specific
            'oItem.FromPane = oFolder.Pane
            'oItem.ToPane = oFolder.Pane

            'oMatrix_Scope.Columns.Item(0).Visible = False
            'oMatrix_Scope.Columns.Item(1).Visible = False
            'oMatrix_Scope.Columns.Item(2).Visible = False
            ''--------------------------------------------------------------------------------------------------------------------------------------------------------------------

            'oItemRef = frmGeneralSeetings.Items.Item("Scope")
            'oItem = frmGeneralSeetings.Items.Item("Excus")
            'oItem.Top = oItemRef.Top
            'oItem.Height = oItemRef.Height
            'oItem.Left = oItemRef.Left + oItemRef.Width
            'oItem.Width = oItemRef.Width
            'oItem.Visible = True
            'oFolder = oItem.Specific
            'oFolder.Caption = "Excusions"
            'oFolder.GroupWith(oItemRef.UniqueID)
            'oFolder.Pane = 217

            'Dim oMatrix_Excus As SAPbouiCOM.Grid
            'oItem = frmGeneralSeetings.Items.Item("m_Excus")
            'oItem.Top = frmGeneralSeetings.Items.Item("92").Top
            'oItem.Left = frmGeneralSeetings.Items.Item("92").Left
            'oItem.Width = frmGeneralSeetings.Items.Item("92").Width
            'oItem.Height = frmGeneralSeetings.Items.Item("92").Height
            'oItem.FromPane = oFolder.Pane
            'oItem.ToPane = oFolder.Pane
            'oMatrix_Excus = oItem.Specific
            'oItem.FromPane = oFolder.Pane
            'oItem.ToPane = oFolder.Pane

            'oMatrix_Excus.Columns.Item(0).Visible = False
            'oMatrix_Excus.Columns.Item(1).Visible = False
            'oMatrix_Excus.Columns.Item(2).Visible = False


            'oItem = frmGeneralSeetings.Items.Item("t_PriDet")
            'oItem.Top = frmGeneralSeetings.Items.Item("50").Top
            'oItem.Height = frmGeneralSeetings.Items.Item("50").Height
            'oItem.Left = frmGeneralSeetings.Items.Item("50").Left
            'oItem.Width = frmGeneralSeetings.Items.Item("50").Width
            'oItem.Visible = True
            'oItem.FromPane = 219
            'oItem.ToPane = 219


            'oItem = frmGeneralSeetings.Items.Item("l_PriDet")
            'oItem.Top = frmGeneralSeetings.Items.Item("50").Top
            'oItem.Left = frmGeneralSeetings.Items.Item("162").Left
            'oItem.Width = 100
            'oItem.Visible = True
            'oItem.FromPane = 219
            'oItem.ToPane = 219

            'frmGeneralSeetings.Items.Item("5").Visible = False
            'frmGeneralSeetings.Items.Item("6").Visible = False

            ''--------------------------------------------------------------------------------------------------------------------------------------------------------------------
            'Dim squery As String
            'squery = "select ISNULL(U_Caption,'') AS Caption, ISNULL(U_Order,0) AS [Order],ItmsGrpCod  from OITB Where ISNULL(U_Estimation,'N') ='Y' AND ISNULL(U_Order,'') !='' Order By convert(int,ISNULL(U_Order,0)) "
            'rsDynamicControl = DoQuery(squery)
            'If rsDynamicControl.RecordCount > 0 Then
            '    rsDynamicControl.MoveFirst()
            'End If

            'Dim pNumber As Integer = 0
            'Dim Pre_ItmsGrpCod As String = ""
            'For i As Integer = 1 To rsDynamicControl.RecordCount
            '    oItem = frmGeneralSeetings.Items.Item("DF" + rsDynamicControl.Fields.Item("ItmsGrpCod").Value.ToString())
            '    oItem.Top = oItemRef.Top + 20
            '    oItem.FromPane = 1000
            '    oItem.ToPane = 2000
            '    oItem.Width = frmGeneralSeetings.Items.Item("4").Width
            '    If i = 1 Then
            '        oItem.Left = frmGeneralSeetings.Items.Item("4").Left
            '    Else
            '        oItem.Left = frmGeneralSeetings.Items.Item("4").Left + (frmGeneralSeetings.Items.Item("4").Width * i)
            '    End If
            '    oItem.Visible = True
            '    oFolder = oItem.Specific
            '    oFolder.Caption = rsDynamicControl.Fields.Item("Caption").Value.ToString.Trim
            '    If i > 1 Then oFolder.GroupWith("DF" + Pre_ItmsGrpCod)
            '    Pre_ItmsGrpCod = rsDynamicControl.Fields.Item("ItmsGrpCod").Value.ToString()
            '    oFolder.Pane = rsDynamicControl.Fields.Item("ItmsGrpCod").Value + 1000


            '    Dim oMatrix_dynamic As SAPbouiCOM.Grid
            '    oItem = frmGeneralSeetings.Items.Item("DM" + rsDynamicControl.Fields.Item("ItmsGrpCod").Value.ToString())
            '    oItem.Top = frmGeneralSeetings.Items.Item("92").Top + 15
            '    oItem.Left = frmGeneralSeetings.Items.Item("92").Left
            '    oItem.Width = frmGeneralSeetings.Items.Item("92").Width
            '    oItem.Height = frmGeneralSeetings.Items.Item("172").Height - 15
            '    oItem.FromPane = oFolder.Pane
            '    oItem.ToPane = oFolder.Pane
            '    oMatrix_dynamic = oItem.Specific
            '    oItem.FromPane = oFolder.Pane
            '    oItem.ToPane = oFolder.Pane

            '    oMatrix_dynamic.Columns.Item(0).Visible = False
            '    oMatrix_dynamic.Columns.Item(1).Visible = False
            '    oMatrix_dynamic.Columns.Item(2).Visible = False
            '    oMatrix_dynamic.Columns.Item(3).Visible = False
            '    oMatrix_dynamic.Columns.Item(4).Visible = False

            '    oMatrix_dynamic.Columns.Item(6).Editable = False


            '    rsDynamicControl.MoveNext()

            'Next

        Catch ex As Exception
            ' oApplication.StatusBar.SetText("Resize Form Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            'frmGeneralSeetings.Freeze(False)
        End Try
    End Sub
    Sub DynamicControl_Load()


        Try
            Dim oItem As SAPbouiCOM.Item = Nothing
            If boolFormLoaded = False Then Exit Sub
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim
            Dim strSQL As String

            strSQL = "Select  U_ItemCode as [ItemCode],U_ItemName as [ItemName],U_CycleTime as [CycleTime] From [@AIS_RSC8]  WHERE  U_BaseNum ='" & sResourceCode & "' And U_Series='" + sSeries + "'"
            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms").ExecuteQuery(strSQL)
            Dim oMatrix_Terms As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms").Specific
            oMatrix_Terms.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms")

        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Load  Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub
    Sub DynamicControlCapacity_Load()


        Try
            Dim oItem As SAPbouiCOM.Item = Nothing
            If boolFormLoaded = False Then Exit Sub
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim

            Dim oMatrix_Terms As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms1").Specific
            oMatrix_Terms.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Empty")
        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Capacity Load  Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub

    Sub DynamicControl_MachineInformationDetails_Load()
        Try
            Dim oItem As SAPbouiCOM.Item = Nothing
            If boolFormLoaded = False Then Exit Sub
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim

            strSQL = "select  U_VendorCode as VendorCode, U_Manufacturing Manufacturing ,U_InsulationDate InsulationDate,U_TechnicalSupp TechnicalSupp ,U_Budget Budget   from [@AIS_RSC13]   WHERE  U_BaseNum ='" & sResourceCode & "' And U_Series='" + sSeries + "'"
            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms13").ExecuteQuery(strSQL)


            Dim oMatrix_Terms As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms13").Specific
            oMatrix_Terms.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms13")
        Catch ex As Exception
            oApplication.StatusBar.SetText("InsertDetails_ MachineInformationDetails_Load Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub DynamicControlMech_Load()


        Try
            Dim oItem As SAPbouiCOM.Item = Nothing
            If boolFormLoaded = False Then Exit Sub
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim
            Dim strSQL As String

            strSQL = "select U_Task as Task,U_StepCode as [Step Code], U_LastUpdateDate as [Last Update Date],U_DefaultService as [Default Service],U_Remarks as Remarks,U_Action as Action,U_Duration as Duration,U_Frequency as Frequency,U_FrequencyDay as FrequencyDay,U_PreparedDate as [From Date],U_PreparedTDate as [To Date], U_SFromTime as [Total Time]  from [@AIS_RSC9] WHERE  U_BaseNum ='" & sResourceCode & "' And U_Series='" + sSeries + "'"

            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms1").ExecuteQuery(strSQL)

            Dim oGrid As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms2").Specific
            oGrid.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms1")


            oGrid.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(1).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(2).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(3).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oGrid.Columns.Item(4).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(5).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(6).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(7).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oGrid.Columns.Item(8).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(9).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oGrid.Columns.Item(10).Type = SAPbouiCOM.BoGridColumnType.gct_EditText


            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single


            Dim ocbc_GetDefaultService As SAPbouiCOM.ComboBoxColumn

            ocbc_GetDefaultService = CType(oGrid.Columns.Item(3), SAPbouiCOM.ComboBoxColumn)
            Dim strQuery As String = ""
            Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strQuery = "[@AIS_Maintenance_GetDefaultService]"
            rsetQuery.DoQuery(strQuery)
            If ocbc_GetDefaultService.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc_GetDefaultService.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc_GetDefaultService.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description



            Dim ocbc As SAPbouiCOM.ComboBoxColumn
            ocbc = CType(oGrid.Columns.Item(7), SAPbouiCOM.ComboBoxColumn)

            strQuery = "[@AIS_Maintenance_GetFrequencyType]"
            rsetQuery.DoQuery(strQuery)
            If ocbc.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description


        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Mech Load  Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub DynamicControlElec_Load()


        Try
            Dim oItem As SAPbouiCOM.Item = Nothing
            If boolFormLoaded = False Then Exit Sub
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim
            Dim strSQL As String

            strSQL = "select U_Task as Task,U_StepCode as [Step Code],U_LastUpdateDate as [Last Update Date],U_DefaultService as [Default Service],U_Remarks as Remarks,U_Action as Action,U_Duration as Duration,U_Frequency as Frequency,U_FrequencyDay as FrequencyDay,U_PreparedDate as [From Date],U_PreparedTDate as [To Date], U_SFromTime as [Total Time]  from [@AIS_RSC11] WHERE  U_BaseNum ='" & sResourceCode & "' And U_Series='" + sSeries + "'"
            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms2").ExecuteQuery(strSQL)


            Dim oMatrix_Terms As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms3").Specific
            oMatrix_Terms.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms2")
            oMatrix_Terms.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(1).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(2).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(3).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oMatrix_Terms.Columns.Item(4).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(5).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(6).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(7).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oMatrix_Terms.Columns.Item(8).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(9).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(10).Type = SAPbouiCOM.BoGridColumnType.gct_EditText

            oMatrix_Terms.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single

            Dim ocbc_GetDefaultService As SAPbouiCOM.ComboBoxColumn
            ocbc_GetDefaultService = CType(oMatrix_Terms.Columns.Item(3), SAPbouiCOM.ComboBoxColumn)
            Dim strQuery As String = ""
            Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strQuery = "[@AIS_Maintenance_GetDefaultService]"
            rsetQuery.DoQuery(strQuery)
            If ocbc_GetDefaultService.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc_GetDefaultService.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc_GetDefaultService.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description





            Dim ocbc As SAPbouiCOM.ComboBoxColumn
            ocbc = CType(oMatrix_Terms.Columns.Item(7), SAPbouiCOM.ComboBoxColumn)

            strQuery = "[@AIS_Maintenance_GetFrequencyType]"
            rsetQuery.DoQuery(strQuery)
            If ocbc.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description



        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Elec Load  Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub DynamicControlOperator_Load()


        Try
            Dim oItem As SAPbouiCOM.Item = Nothing
            If boolFormLoaded = False Then Exit Sub
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim
            Dim strSQL As String

            strSQL = "select U_Task as Task,U_StepCode as [Step Code],U_LastUpdateDate as [Last Update Date],U_DefaultService as [Default Service],U_Remarks as Remarks,U_Action as Action,U_Duration as Duration,U_Frequency as Frequency,U_FrequencyDay as FrequencyDay,U_PreparedDate as [From Date],U_PreparedTDate as [To Date], U_SFromTime as [Total Time] from [@AIS_RSC12] WHERE  U_BaseNum ='" & sResourceCode & "' And U_Series='" + sSeries + "'"
            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms3").ExecuteQuery(strSQL)


            Dim oMatrix_Terms As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms4").Specific
            oMatrix_Terms.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms3")
            oMatrix_Terms.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(1).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(2).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(3).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oMatrix_Terms.Columns.Item(4).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(5).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(6).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(7).Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
            oMatrix_Terms.Columns.Item(8).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(9).Type = SAPbouiCOM.BoGridColumnType.gct_EditText
            oMatrix_Terms.Columns.Item(10).Type = SAPbouiCOM.BoGridColumnType.gct_EditText

            oMatrix_Terms.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single

            Dim ocbc_GetDefaultService As SAPbouiCOM.ComboBoxColumn
            ocbc_GetDefaultService = CType(oMatrix_Terms.Columns.Item(3), SAPbouiCOM.ComboBoxColumn)
            Dim strQuery As String = ""
            Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strQuery = "[@AIS_Maintenance_GetDefaultService]"
            rsetQuery.DoQuery(strQuery)
            If ocbc_GetDefaultService.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc_GetDefaultService.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc_GetDefaultService.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description





            Dim ocbc As SAPbouiCOM.ComboBoxColumn
            ocbc = CType(oMatrix_Terms.Columns.Item(7), SAPbouiCOM.ComboBoxColumn)

            strQuery = "[@AIS_Maintenance_GetFrequencyType]"
            rsetQuery.DoQuery(strQuery)
            If ocbc.ValidValues.Count = 0 Then
                rsetQuery.MoveFirst()
                For j As Integer = 0 To rsetQuery.RecordCount - 1
                    ocbc.ValidValues.Add(rsetQuery.Fields.Item(0).Value, rsetQuery.Fields.Item(1).Value)
                    rsetQuery.MoveNext()
                Next
            End If
            ocbc.DisplayType = SAPbouiCOM.BoComboDisplayType.cdt_Description



        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Operator Load  Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub DynamicControlMCSpares_Load()


        Try
            Dim oItem As SAPbouiCOM.Item = Nothing
            If boolFormLoaded = False Then Exit Sub
            Dim sResourceCode As String = frmGeneralSeetings.Items.Item("1470000013").Specific.value.trim
            Dim sSeries As String = frmGeneralSeetings.Items.Item("1470000012").Specific.value.trim
            Dim strSQL As String

            strSQL = "Select  U_SerialNo as SerialNo,U_MainSpareCode as MainSpareCode,U_MainSpareName as MainSpareName,U_LastChangeDate as LastChangeDate,U_VerifiedBy as VerifiedBy,U_AMC as AMC,U_SpareType as SpareType,U_SubSpareCode as SubSpareCode,U_SubSpareName as SubSpareName From [@AIS_RSC10] WHERE  U_BaseNum ='" & sResourceCode & "' And U_Series='" + sSeries + "'"
            frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms4").ExecuteQuery(strSQL)


            Dim oMatrix_Terms As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms5").Specific
            oMatrix_Terms.DataTable = frmGeneralSeetings.DataSources.DataTables.Item("DT_Terms4")




        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Load MC Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


    Sub InitForm()
        Try
            frmGeneralSeetings.Freeze(True)
            DynamicControl_Load()
            DynamicControlCapacity_Load()
            DynamicControlMech_Load()
            DynamicControlElec_Load()
            DynamicControlOperator_Load()
            DynamicControlMCSpares_Load()
            DynamicControl_MachineInformationDetails_Load()
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmGeneralSeetings.Freeze(False)
        End Try
    End Sub

    Sub DefineModesForFields()
        Try

        Catch ex As Exception
            oApplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function ValidateAll() As Boolean
        Try

            'Dim dpotentialamount As Double = frmGeneralSeetings.Items.Item("41").Specific.value
            'If dpotentialamount <= 0 Then
            '    StatusBarWarningMsg("Potential Amount should not be zero")
            '    Return False
            'End If
            ValidateAll = True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.FormTypeEx = SalesOpportunityTypeEx Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
                        Try
                            If pVal.BeforeAction = False Then Me.LoadGeneralSeetingsForm(pVal.FormUID)
                        Catch ex As Exception
                            GFun.oApplication.StatusBar.SetText("Form Load Event Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        Finally
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        Try
                            Dim oDataTable As SAPbouiCOM.DataTable
                            Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                            oCFLE = pVal
                            oDataTable = oCFLE.SelectedObjects
                            If Not oDataTable Is Nothing Then
                                Select Case pVal.ItemUID
                                    Case "m_terms"
                                        Select Case pVal.ColUID
                                            Case "ItemCode"
                                                If pVal.BeforeAction = False Then
                                                    Dim oGrid As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms").Specific
                                                    oGrid.DataTable.Columns.Item("ItemCode").Cells.Item(pVal.Row).Value = Trim(oDataTable.GetValue("ItemCode", 0))
                                                    oGrid.DataTable.Columns.Item("ItemName").Cells.Item(pVal.Row).Value = Trim(oDataTable.GetValue("ItemName", 0))
                                                    oGrid.DataTable.Rows.Add(1)
                                                End If


                                        End Select

                                End Select
                            End If
                        Catch ex As Exception
                            oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        Finally
                        End Try




                    Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                        Try
                            If boolFormLoaded Then

                            End If
                        Catch ex As Exception
                            StatusBarWarningMsg("Validate Event Failed:")
                        Finally
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        Try
                            Select Case pVal.ItemUID


                                Case "m_terms2"
                                    Select Case pVal.ColUID
                                        Case "Task"
                                            If pVal.BeforeAction = False Then
                                                Dim oGrid As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms2").Specific
                                                oGrid.DataTable.Rows.Add(1)
                                            End If

                                    End Select
                                Case "m_terms3"
                                    Select Case pVal.ColUID
                                        Case "Task"
                                            If pVal.BeforeAction = False Then
                                                Dim oGrid As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms3").Specific
                                                oGrid.DataTable.Rows.Add(1)
                                            End If
                                    End Select
                                Case "m_terms4"
                                    Select Case pVal.ColUID
                                        Case "Task"
                                            If pVal.BeforeAction = False Then
                                                Dim oGrid As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms4").Specific
                                                oGrid.DataTable.Rows.Add(1)
                                            End If
                                    End Select
                                Case "m_terms5"
                                    Select Case pVal.ColUID
                                        Case "SerialNo"
                                            If pVal.BeforeAction = False Then
                                                Dim oGrid As SAPbouiCOM.Grid = frmGeneralSeetings.Items.Item("m_terms5").Specific
                                                oGrid.DataTable.Rows.Add(1)
                                            End If
                                    End Select

                            End Select
                        Catch ex As Exception
                            StatusBarWarningMsg("Click Event Failed:")
                        Finally
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        Try
                            Select Case pVal.ItemUID
                                Case "Terms13"
                                    frmGeneralSeetings = oApplication.Forms.Item(FormUID)
                                    If pVal.ActionSuccess = True Then
                                        frmGeneralSeetings.PaneLevel = 900
                                        frmGeneralSeetings.Items.Item(pVal.ItemUID).AffectsFormMode = False
                                    End If
                                Case "Terms"
                                    frmGeneralSeetings = oApplication.Forms.Item(FormUID)
                                    If pVal.ActionSuccess = True Then
                                        frmGeneralSeetings.PaneLevel = 300
                                        frmGeneralSeetings.Items.Item(pVal.ItemUID).AffectsFormMode = False
                                    End If
                                Case "Empty"
                                    frmGeneralSeetings = oApplication.Forms.Item(FormUID)

                                    If pVal.ActionSuccess = True Then

                                        frmGeneralSeetings.PaneLevel = 400S
                                        frmGeneralSeetings.Items.Item(pVal.ItemUID).AffectsFormMode = False
                                    End If
                                Case "Terms1"
                                    frmGeneralSeetings = oApplication.Forms.Item(FormUID)

                                    If pVal.ActionSuccess = True Then
                                        '
                                        frmGeneralSeetings.PaneLevel = 500
                                        frmGeneralSeetings.Items.Item(pVal.ItemUID).AffectsFormMode = False
                                    End If
                                Case "Terms2"
                                    frmGeneralSeetings = oApplication.Forms.Item(FormUID)

                                    If pVal.ActionSuccess = True Then
                                        '
                                        frmGeneralSeetings.PaneLevel = 600
                                        frmGeneralSeetings.Items.Item(pVal.ItemUID).AffectsFormMode = False
                                    End If
                                Case "Terms3"
                                    frmGeneralSeetings = oApplication.Forms.Item(FormUID)

                                    If pVal.ActionSuccess = True Then
                                        frmGeneralSeetings.PaneLevel = 700
                                        frmGeneralSeetings.Items.Item(pVal.ItemUID).AffectsFormMode = False
                                    End If
                                Case "Terms4"
                                    frmGeneralSeetings = oApplication.Forms.Item(FormUID)

                                    If pVal.ActionSuccess = True Then

                                        frmGeneralSeetings.PaneLevel = 800
                                        frmGeneralSeetings.Items.Item(pVal.ItemUID).AffectsFormMode = False
                                    End If
                                Case "1470000001"
                                    If pVal.ActionSuccess And frmGeneralSeetings.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                        Me.InitForm()
                                    End If
                            End Select
                        Catch ex As Exception
                            oApplication.StatusBar.SetText("Item Pressed Event Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        Finally
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        Try
                            Select Case pVal.ItemUID

                                Case "GetTask"
                                    If pVal.BeforeAction = False Then
                                        DynamicControlOperatorGetTask()
                                    End If
                                Case "GetMachine"
                                    If pVal.BeforeAction = False Then
                                        DynamicControlMechGetTask()
                                    End If
                                Case "GetElect"
                                    If pVal.BeforeAction = False Then
                                        DynamicControlElecGetTask()
                                    End If
                                Case "1470000001"
                                    'Add,Update Event
                                    If pVal.BeforeAction = True And (frmGeneralSeetings.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmGeneralSeetings.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                                        If Me.ValidateAll() = False Then
                                            System.Media.SystemSounds.Asterisk.Play()
                                            BubbleEvent = False
                                            Exit Sub
                                        Else
                                            If oCompany.InTransaction = False Then oCompany.StartTransaction()
                                            If Not TransactionManagement() Then
                                                If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                                BubbleEvent = False
                                                Return
                                            Else
                                                If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                            End If

                                        End If
                                    End If
                            End Select
                        Catch ex As Exception
                            oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                            BubbleEvent = False
                        Finally
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_FORM_CLOSE
                        Try
                            If boolFormLoaded And pVal.BeforeAction = False Then
                                boolFormLoaded = False
                            End If
                        Catch ex As Exception
                            GFun.oApplication.StatusBar.SetText("Combo Select Event Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        Finally
                        End Try

                End Select
            End If

        Catch ex As Exception
            GFun.oApplication.StatusBar.SetText("Item Event Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"


                    'Dim SaveDataTableRecords As SAPbouiCOM.DataTable
                    'SaveDataTableRecords = frmGeneralSeetings.DataSources.DataTables.Item("DataTable")
                    'Dim strQuery As String = ""
                    'Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

                    'strQuery = "DELETE FROM [@SMSQ_OGSE]  WHERE CODE = '" & SaveDataTableRecords.GetValue("Location", pVal.row).ToString & "' "
                    'rsetQuery.DoQuery(strQuery)

            End Select
        Catch ex As Exception
            GFun.oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub


    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Try
                        If BusinessObjectInfo.ActionSuccess Then
                            'If Me.Validation() = False Then
                            '    System.Media.SystemSounds.Asterisk.Play()
                            '    BubbleEvent = False
                            '    Exit Sub
                            'End If
                        End If
                        If BusinessObjectInfo.ActionSuccess Then
                            'DynamicControl_Load()
                            'DynamicControlCapacity_Load()
                            'DynamicControlMech_Load()
                            'DynamicControlElec_Load()
                            'DynamicControlOperator_Load()
                            'DynamicControlMCSpares_Load()
                        End If

                    Catch ex As Exception
                        GFun.oApplication.StatusBar.SetText("Form Data Add ,Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If BusinessObjectInfo.ActionSuccess = True Then
                        DynamicControl_Load()
                        DynamicControlCapacity_Load()
                        DynamicControlMech_Load()
                        DynamicControlElec_Load()
                        DynamicControlOperator_Load()
                        DynamicControlMCSpares_Load()
                        DynamicControl_MachineInformationDetails_Load()
                    End If

            End Select

        Catch ex As Exception
            GFun.oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub InsertUpdateDatabaseRecords()
        Try
            Dim files() As String = IO.Directory.GetFiles(Application.StartupPath.ToLower.Replace("\bin\debug", "").Replace("\bin\release", "") + "\File\")
            For Each file As String In files
                Dim text As String = IO.File.ReadAllText(file)
                '     text = text.ToLower.Replace("go", Environment.NewLine)
                Dim rsetCode As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                rsetCode.DoQuery(text)
            Next
            GFun.oApplication.StatusBar.SetText("Files Updated Successsfully", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
        Catch ex As Exception
            GFun.oApplication.StatusBar.SetText("Files Not Updated Error Details : " + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try

    End Sub
    Sub RightClickEvent(ByRef EventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case EventInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK
                    DeleteRowITEMUID = EventInfo.ItemUID
                    If GFun.oApplication.Forms.Item(EventInfo.FormUID).TypeEx = GeneralSeetingsFormTypeEX And GFun.oApplication.Forms.Item(EventInfo.FormUID).Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE _
                      And EventInfo.BeforeAction Then
                        SubMenuAddEx("DataBaseUpdate", "Update Files")

                    End If
            End Select
        Catch ex As Exception
            GFun.oApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


End Class