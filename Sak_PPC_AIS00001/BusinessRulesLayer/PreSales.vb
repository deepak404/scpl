﻿ 
Imports SAPLib

Public Class PreSalesOrder

    Dim frmPreSales As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim boolFormLoaded As Boolean = False
    Dim sFormUID As String
    Dim UDOID As String = "OPRS"
    Sub LoadPreSales()

        Try


            boolFormLoaded = False
            LoadXML(frmPreSales, PreSalesFormID, PreSalesXML)
            frmPreSales = oApplication.Forms.Item(PreSalesFormID)
            oDBDSHeader = frmPreSales.DataSources.DBDataSources.Item("@AIS_OPRS")
            oDBDSDetail = frmPreSales.DataSources.DBDataSources.Item("@AIS_PRS1")
            oMatrix = frmPreSales.Items.Item("matPreSale").Specific


            frmPreSales.Freeze(True)
            ' 

            Me.InitForm()
            Me.DefineModeForFields()


        Catch ex As Exception
            oApplication.StatusBar.SetText("Load Pre Sales Form Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmPreSales.Freeze(False)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmPreSales.Freeze(True)
            LoadComboBoxSeries(frmPreSales.Items.Item("c_series").Specific, UDOID)
            LoadDocumentDate(frmPreSales.Items.Item("t_DocDate").Specific)
            LoadDocumentDate(frmPreSales.Items.Item("t_PostingD").Specific)
            oMatrix.Clear()
            SetNewLine(oMatrix, oDBDSDetail)
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmPreSales.Freeze(False)
        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 


    Sub DefineModeForFields()
        Try
            frmPreSales.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmPreSales.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmPreSales.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmPreSales.Items.Item("t_PostingD").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            '4
            frmPreSales.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmPreSales.Items.Item("t_DocDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmPreSales.Items.Item("t_BaseAmou").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmPreSales.Items.Item("t_TaxAmoun").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmPreSales.Items.Item("t_Total").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmPreSales.Items.Item("t_Name").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            '1
            frmPreSales.Items.Item("t_Customer").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmPreSales.Items.Item("t_PostingD").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)



        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub CreateFormForActivityList()
        Try

            Dim sQuery As String
            Dim CP As SAPbouiCOM.FormCreationParams = oApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)

            Dim oItem As SAPbouiCOM.Item
            Dim oButton As SAPbouiCOM.Button
            Dim oStaticText As SAPbouiCOM.StaticText
            Dim oEditText As SAPbouiCOM.EditText
            Dim oGrid As SAPbouiCOM.Grid

            CP.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Sizable
            CP.UniqueID = "Quotation"
            CP.FormType = "200"

            oForm = oApplication.Forms.AddEx(CP)
            ' Set form width and height 
            oForm.Height = 260
            oForm.Width = 500
            oForm.Title = "Quotation Details"


            '' Add a Grid item to the form 
            oItem = oForm.Items.Add("MyGrid", SAPbouiCOM.BoFormItemTypes.it_GRID)
            ' Set the grid dimentions and position 
            oItem.Left = 6
            oItem.Top = 0
            oItem.Width = 500
            oItem.Height = 200
            ' Set the grid data 
            oGrid = oItem.Specific
            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
            oForm.DataSources.DataTables.Add("MyDataTable")

            'sQuery = "execute [@AIS_PPC_GeQuotationDetails]  '" & frmPreSales.Items.Item("t_Customer").Specific.value & "'"

            sQuery = "execute [@AIS_PPC_DisplayQuotationDetails]  '" & frmPreSales.Items.Item("t_Customer").Specific.value & "'"
            oForm.DataSources.DataTables.Item(0).ExecuteQuery(sQuery)

            oGrid.DataTable = oForm.DataSources.DataTables.Item("MyDataTable")
            oGrid.AutoResizeColumns()
             
            oGrid.Columns.Item(0).Editable = False
            oGrid.Columns.Item(1).Editable = False
            oGrid.Columns.Item(2).Editable = False
          

            ' Add OK Button 
            oItem = oForm.Items.Add("ok", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 6
            oItem.Top = 200
            oItem.Width = 65
            oItem.Height = 20
            oButton = oItem.Specific
            oButton.Caption = "Submit"
            ' Add CANCEL Button 
            oItem = oForm.Items.Add("2", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 90
            oItem.Top = 200
            oItem.Width = 65
            oItem.Height = 20
            oButton = oItem.Specific



        Catch ex As Exception

            oApplication.StatusBar.SetText("Create Form For Quotation Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally

            oForm.Visible = True

        End Try
    End Sub
    Sub LineTotalDetails()
        Dim dBaseAmount As Double = 0
        Dim dTaxRate As Double = 0
        Dim dLineTotal As Double = 0
        For i As Integer = 1 To oMatrix.VisualRowCount
            If oMatrix.Columns.Item("ICode").Cells.Item(i).Specific.value.Trim.Equals("") = False Then
                dBaseAmount = dBaseAmount + CDbl(oMatrix.Columns.Item("bamount").Cells.Item(i).Specific.value.Trim)
                dTaxRate = dTaxRate + CDbl(oMatrix.Columns.Item("taxrate").Cells.Item(i).Specific.value.Trim)
                dLineTotal = dLineTotal + CDbl(oMatrix.Columns.Item("total").Cells.Item(i).Specific.value.Trim)
            End If
        Next
        frmPreSales.Items.Item("t_BaseAmou").Specific.value = dBaseAmount.ToString().Trim
        frmPreSales.Items.Item("t_TaxAmoun").Specific.value = dTaxRate.ToString().Trim
        frmPreSales.Items.Item("t_Total").Specific.value = dLineTotal.ToString().Trim

    End Sub




    Sub LoadPreSalesOrderDetails(DocEntry As String)
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the Presales Order Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Dim sQuery As String
            sQuery = " EXEC [dbo].[@AIS_PPC_GeQuotationDetails] '" + DocEntry + "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
            rsetEmpDets.MoveFirst()
            oMatrix.Clear()
            oDBDSDetail.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                oDBDSDetail.Offset = i

                oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, i + 1)

                oDBDSDetail.SetValue("U_BaseEntry", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("DocEntry").Value)
                oDBDSDetail.SetValue("U_BaseNum", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("DocNum").Value)
                oDBDSDetail.SetValue("U_BaseObject", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("Object").Value)
                oDBDSDetail.SetValue("U_BaseLine", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("LineId").Value)
               
                oDBDSDetail.SetValue("U_ItemCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ItmsGrpCod").Value)
                oDBDSDetail.SetValue("U_ItemName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ItmsGrpNam").Value)
                oDBDSDetail.SetValue("U_Quantity", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_Quantity").Value)
                oDBDSDetail.SetValue("U_UnitPrice", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_UnitPrice").Value)
                oDBDSDetail.SetValue("U_UOM", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_UOM").Value)
                oDBDSDetail.SetValue("U_RequiredDate", oDBDSDetail.Offset, CDate(rsetEmpDets.Fields.Item("U_RequiredDate").Value).ToString("yyyyMMdd"))
                oDBDSDetail.SetValue("U_BaseAmount", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_BaseAmount").Value)
                oDBDSDetail.SetValue("U_TaxCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_TaxCode").Value)
                oDBDSDetail.SetValue("U_TaxRate", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_TaxRate").Value)
                oDBDSDetail.SetValue("U_WhsCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_WhsCode").Value)
                oDBDSDetail.SetValue("U_WhsName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_WhsName").Value)
                oDBDSDetail.SetValue("U_LineTotal", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_LineTotal").Value)

                rsetEmpDets.MoveNext()
            Next
            oMatrix.LoadFromDataSource()

            If frmPreSales.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmPreSales.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Sales Quotation Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
            LineTotalDetails()
        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Sales Quotation Data Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub



    Sub ItemEvent_SubForm(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try

            Select Case pVal.EventType


                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "ok"
                                If pVal.BeforeAction Then
                                    Dim oGrid As SAPbouiCOM.Grid
                                    oGrid = oForm.Items.Item("MyGrid").Specific
                                    For i As Integer = 0 To oGrid.DataTable.Rows.Count - 1
                                        If oGrid.Rows.IsSelected(i) Then
                                            Dim DocEntry = oGrid.DataTable.Columns.Item("DocEntry").Cells.Item(i).Value

                                            LoadPreSalesOrderDetails(DocEntry)
                                        End If
                                    Next

                                    oForm.Close()
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID
                                Case "t_SCode"
                                    oDBDSHeader.SetValue("U_ScenarioCode", 0, Trim(oDataTable.GetValue("U_ScenarioCode", 0).ToString()))
                                    oDBDSHeader.SetValue("U_ScenarioName", 0, Trim(oDataTable.GetValue("U_ScenarioName", 0).ToString()))
                                Case "t_Customer"
                                    oDBDSHeader.SetValue("U_CardCode", 0, Trim(oDataTable.GetValue("CardCode", 0)))
                                    oDBDSHeader.SetValue("U_CardName", 0, Trim(oDataTable.GetValue("CardName", 0)))


                                Case "matPreSale"
                                    Select Case pVal.ColUID
                                        Case "ICode"
                                            Dim sQuery = "Select SalUnitMsr from OITM Where ItemCode='" & Trim(oDataTable.GetValue("ItemCode", 0)) & "'"
                                            Dim ItemUOM As String = String.Empty
                                            Dim rsetItem As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            If rsetItem.RecordCount > 0 Then
                                                rsetItem.MoveFirst()
                                                ItemUOM = Trim(rsetItem.Fields.Item("SalUnitMsr").Value)
                                            End If



                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_ItemCode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail.SetValue("U_ItemName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oDBDSDetail.SetValue("U_UOM", pVal.Row - 1, ItemUOM)
                                            oDBDSDetail.SetValue("U_Quantity", pVal.Row - 1, 1)
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            SetNewLine(oMatrix, oDBDSDetail, oMatrix.VisualRowCount, pVal.ColUID)
                                        Case "TaxCode"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_TaxCode", pVal.Row - 1, Trim(oDataTable.GetValue("Code", 0)))
                                            oDBDSDetail.SetValue("U_TaxRate", pVal.Row - 1, Trim(oDataTable.GetValue("Rate", 0)))
                                            'oMatrix.LoadFromDataSource()
                                            'oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                            'oMatrix.FlushToDataSource()
                                            Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_BaseAmount", pVal.Row - 1)) + CDbl(Trim(oDataTable.GetValue("Rate", 0)))
                                            oDBDSDetail.SetValue("U_LineTotal", pVal.Row - 1, dblValue)
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            LineTotalDetails()

                                        Case "WhsCod"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_WhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail.SetValue("U_WhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                    End Select

                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "matPreSale"
                                Select Case pVal.ColUID
                                    Case "ICode"
                                        If pVal.BeforeAction = False Then
                                            ChooseFromListFilteration(frmPreSales, "ITEM_CFL", "ItemCode", "Select ItemCode ,ItemName  from OITM Where ItemName In (Select U_GN From OITM) order by itemcode")
                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID

                            Case "matPreSale"
                                Select Case pVal.ColUID
                                    Case "V_1", "V_8"
                                        If pVal.BeforeAction = False And pVal.ItemChanged Then
                                            oMatrix.FlushToDataSource()
                                            Dim TaxAmount As Double = CDbl(oDBDSDetail.GetValue("U_TaxRate", pVal.Row - 1))
                                            Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_Quantity", pVal.Row - 1)) * CDbl(oDBDSDetail.GetValue("U_UnitPrice", pVal.Row - 1))
                                            oDBDSDetail.SetValue("U_BaseAmount", pVal.Row - 1, dblValue)
                                            oDBDSDetail.SetValue("U_LineTotal", pVal.Row - 1, dblValue + TaxAmount)
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            LineTotalDetails()
                                        End If
                                        'Case "TaxCode"
                                        '    If pVal.BeforeAction = False And pVal.ItemChanged Then
                                        '        oMatrix.FlushToDataSource()
                                        '        Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_BaseAmount", pVal.Row - 1)) + CDbl(oDBDSDetail.GetValue("U_TaxRate", pVal.Row - 1))
                                        '        oDBDSDetail.SetValue("U_LineTotal", pVal.Row - 1, dblValue)
                                        '        oMatrix.LoadFromDataSource()
                                        '        oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        '        LineTotalDetails()
                                        '    End If
                                End Select
                        End Select
                    Catch ex As Exception
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "btnCopy"
                                If pVal.BeforeAction = False Then
                                    CreateFormForActivityList()
                                End If
                               
                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmPreSales.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmPreSales.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmPreSales.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
               
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.ActionSuccess And frmPreSales.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then

                                    InitForm()

                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
   
    
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Try
                        If BusinessObjectInfo.ActionSuccess Then

                            ' frmPreSales.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE

                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add ,Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        Dim sFormViewModeValidation As Boolean = False
                        





                        Dim oRowCtrl As SAPbouiCOM.CommonSetting

                        oRowCtrl = oMatrix.CommonSetting()

                        Dim sRejected As Boolean = False
                        Dim i As Integer = 0
                        For i = 1 To oMatrix.VisualRowCount
                            Dim ItemCode As String = oMatrix.Columns.Item("ICode").Cells.Item(i).Specific.value
                            Dim c_Status As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("V_11").Cells.Item(i).Specific
                            If (c_Status.Value = "O" Or c_Status.Value = "") And ItemCode <> String.Empty Then
                               
                                If (c_Status.Value = "O" Or c_Status.Value = "") Then
                                    oRowCtrl.SetRowEditable(i, True)
                                    oMatrix.Columns.Item("V_0").Editable = False
                                    oMatrix.Columns.Item("UOM").Editable = False
                                    oMatrix.Columns.Item("bamount").Editable = False
                                    oMatrix.Columns.Item("taxrate").Editable = False
                                    oMatrix.Columns.Item("WhsName").Editable = False
                                    oMatrix.Columns.Item("total").Editable = False
                                    oMatrix.Columns.Item("total").Editable = False
                                End If
                                sFormViewModeValidation = True
                            End If
                            If c_Status.Selected.Value = "C" Then
                                oRowCtrl.SetRowEditable(i, False)
                            End If

                        Next


                        If sFormViewModeValidation = True Then

                            frmPreSales.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                        Else
                            frmPreSales.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                        End If




                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub RightClickEvent(ByRef EventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case EventInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK
                    If oApplication.Forms.Item(EventInfo.FormUID).Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And EventInfo.BeforeAction Then
                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

End Class
