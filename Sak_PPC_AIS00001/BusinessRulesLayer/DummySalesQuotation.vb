﻿Imports SAPLib

Public Class DummySalesQuotation

    Dim frmDummySalesQuotation As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim boolFormLoaded As Boolean = False
    Dim sFormUID As String
    Dim UDOID As String = "ODUQ"
    Sub LoadPreSales()

        Try


            boolFormLoaded = False
            LoadXML(frmDummySalesQuotation, DummySalesQuotationFormID, DummySalesQuotationXML)
            frmDummySalesQuotation = oApplication.Forms.Item(DummySalesQuotationFormID)
            oDBDSHeader = frmDummySalesQuotation.DataSources.DBDataSources.Item("@AIS_ODUQ")
            oDBDSDetail = frmDummySalesQuotation.DataSources.DBDataSources.Item("@AIS_DUQ1")
            oMatrix = frmDummySalesQuotation.Items.Item("matPreSale").Specific


            frmDummySalesQuotation.Freeze(True)
            ' 

            Me.InitForm()
            Me.DefineModeForFields()


        Catch ex As Exception
            oApplication.StatusBar.SetText("Load Pre Sales Form Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmDummySalesQuotation.Freeze(False)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmDummySalesQuotation.Freeze(True)
            LoadComboBoxSeries(frmDummySalesQuotation.Items.Item("c_series").Specific, UDOID)
            LoadDocumentDate(frmDummySalesQuotation.Items.Item("t_DocDate").Specific)
            LoadDocumentDate(frmDummySalesQuotation.Items.Item("t_PostingD").Specific)
            oMatrix.Clear()
            SetNewLine(oMatrix, oDBDSDetail)
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmDummySalesQuotation.Freeze(False)
        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 


    Sub DefineModeForFields()
        Try
            frmDummySalesQuotation.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmDummySalesQuotation.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmDummySalesQuotation.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmDummySalesQuotation.Items.Item("t_PostingD").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            '4
            frmDummySalesQuotation.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmDummySalesQuotation.Items.Item("t_DocDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmDummySalesQuotation.Items.Item("t_BaseAmou").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmDummySalesQuotation.Items.Item("t_TaxAmoun").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmDummySalesQuotation.Items.Item("t_Total").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmDummySalesQuotation.Items.Item("t_Name").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            '1
            frmDummySalesQuotation.Items.Item("t_Customer").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmDummySalesQuotation.Items.Item("t_PostingD").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)



        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function Validation() As Boolean
        Try

            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validation Function Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Function

    Sub LineTotalDetails()
        Dim dBaseAmount As Double = 0
        Dim dTaxRate As Double = 0
        Dim dLineTotal As Double = 0
        For i As Integer = 1 To oMatrix.VisualRowCount
            If oMatrix.Columns.Item("ICode").Cells.Item(i).Specific.value.Trim.Equals("") = False Then
                dBaseAmount = dBaseAmount + CDbl(oMatrix.Columns.Item("bamount").Cells.Item(i).Specific.value.Trim)
                dTaxRate = dTaxRate + CDbl(oMatrix.Columns.Item("taxrate").Cells.Item(i).Specific.value.Trim)
                dLineTotal = dLineTotal + CDbl(oMatrix.Columns.Item("total").Cells.Item(i).Specific.value.Trim)
            End If
        Next
        frmDummySalesQuotation.Items.Item("t_BaseAmou").Specific.value = dBaseAmount.ToString().Trim
        frmDummySalesQuotation.Items.Item("t_TaxAmoun").Specific.value = dTaxRate.ToString().Trim
        frmDummySalesQuotation.Items.Item("t_Total").Specific.value = dLineTotal.ToString().Trim

    End Sub




    Sub LoadPreSalesOrderDetails()
        Try


            Dim sDocEntry As String = oDBDSDetail.GetValue("DocEntry", 0)
            Dim t_SCode As String = frmDummySalesQuotation.Items.Item("t_Customer").Specific.value.ToString().Trim()


            oApplication.ActivateMenuItem("OPRS")
            Dim frmPreSales As SAPbouiCOM.Form = oApplication.Forms.ActiveForm

            Dim oDBDSHeader_PreSales, oDBDSDetail_PreSales As SAPbouiCOM.DBDataSource
            Dim oMatrix_PreSales As SAPbouiCOM.Matrix

            oDBDSHeader_PreSales = frmPreSales.DataSources.DBDataSources.Item("@AIS_OPRS")
            oDBDSDetail_PreSales = frmPreSales.DataSources.DBDataSources.Item("@AIS_PRS1")
            oMatrix_PreSales = frmPreSales.Items.Item("matPreSale").Specific

            frmPreSales.Items.Item("t_Customer").Specific.value = t_SCode


            oApplication.StatusBar.SetText("Please wait ... System is preparing the Presales Order Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Dim sQuery As String
            sQuery = " EXEC [dbo].[@AIS_PPC_GeQuotationDetails] '" + sDocEntry + "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
            rsetEmpDets.MoveFirst()
            oMatrix_PreSales.Clear()
            oDBDSDetail_PreSales.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail_PreSales.InsertRecord(oDBDSDetail_PreSales.Size)
                oDBDSDetail_PreSales.Offset = i

                oDBDSDetail_PreSales.SetValue("LineID", oDBDSDetail_PreSales.Offset, i + 1)

                oDBDSDetail_PreSales.SetValue("U_BaseEntry", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("DocEntry").Value)
                oDBDSDetail_PreSales.SetValue("U_BaseNum", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("DocNum").Value)
                oDBDSDetail_PreSales.SetValue("U_BaseObject", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("Object").Value)
                oDBDSDetail_PreSales.SetValue("U_BaseLine", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("LineId").Value)

                oDBDSDetail_PreSales.SetValue("U_ItemCode", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("U_ItmsGrpCod").Value)
                oDBDSDetail_PreSales.SetValue("U_ItemName", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("U_ItmsGrpNam").Value)
                oDBDSDetail_PreSales.SetValue("U_Quantity", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("U_Quantity").Value)
                oDBDSDetail_PreSales.SetValue("U_UnitPrice", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("U_UnitPrice").Value)
                oDBDSDetail_PreSales.SetValue("U_UOM", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("U_UOM").Value)
                oDBDSDetail_PreSales.SetValue("U_RequiredDate", oDBDSDetail_PreSales.Offset, CDate(rsetEmpDets.Fields.Item("U_RequiredDate").Value).ToString("yyyyMMdd"))
                oDBDSDetail_PreSales.SetValue("U_BaseAmount", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("U_BaseAmount").Value)
                oDBDSDetail_PreSales.SetValue("U_TaxCode", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("U_TaxCode").Value)
                oDBDSDetail_PreSales.SetValue("U_TaxRate", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("U_TaxRate").Value)
                oDBDSDetail_PreSales.SetValue("U_WhsCode", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("U_WhsCode").Value)
                oDBDSDetail_PreSales.SetValue("U_WhsName", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("U_WhsName").Value)
                oDBDSDetail_PreSales.SetValue("U_LineTotal", oDBDSDetail_PreSales.Offset, rsetEmpDets.Fields.Item("U_LineTotal").Value)

                rsetEmpDets.MoveNext()
            Next
            oMatrix_PreSales.LoadFromDataSource()



            Dim dBaseAmount As Double = 0
            Dim dTaxRate As Double = 0
            Dim dLineTotal As Double = 0
            For i As Integer = 1 To oMatrix_PreSales.VisualRowCount
                If oMatrix_PreSales.Columns.Item("ICode").Cells.Item(i).Specific.value.Trim.Equals("") = False Then
                    dBaseAmount = dBaseAmount + CDbl(oMatrix_PreSales.Columns.Item("bamount").Cells.Item(i).Specific.value.Trim)
                    dTaxRate = dTaxRate + CDbl(oMatrix_PreSales.Columns.Item("taxrate").Cells.Item(i).Specific.value.Trim)
                    dLineTotal = dLineTotal + CDbl(oMatrix_PreSales.Columns.Item("total").Cells.Item(i).Specific.value.Trim)
                End If
            Next
            frmPreSales.Items.Item("t_BaseAmou").Specific.value = dBaseAmount.ToString().Trim
            frmPreSales.Items.Item("t_TaxAmoun").Specific.value = dTaxRate.ToString().Trim
            frmPreSales.Items.Item("t_Total").Specific.value = dLineTotal.ToString().Trim



            'If frmDummySalesQuotation.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmDummySalesQuotation.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Sales Quotation Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Sales Quotation Data Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

   


    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID
                              
                                Case "t_Customer"
                                    oDBDSHeader.SetValue("U_CardCode", 0, Trim(oDataTable.GetValue("CardCode", 0)))
                                    oDBDSHeader.SetValue("U_CardName", 0, Trim(oDataTable.GetValue("CardName", 0)))


                                Case "matPreSale"
                                    Select Case pVal.ColUID
                                        Case "ICode"
                                             

                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_ItmsGrpCod", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail.SetValue("U_ItmsGrpNam", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            SetNewLine(oMatrix, oDBDSDetail, oMatrix.VisualRowCount, pVal.ColUID)
                                        Case "TaxCode"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_TaxCode", pVal.Row - 1, Trim(oDataTable.GetValue("Code", 0)))
                                            oDBDSDetail.SetValue("U_TaxRate", pVal.Row - 1, Trim(oDataTable.GetValue("Rate", 0)))
                                            'oMatrix.LoadFromDataSource()
                                            'oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                            'oMatrix.FlushToDataSource()
                                            Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_BaseAmount", pVal.Row - 1)) + CDbl(Trim(oDataTable.GetValue("Rate", 0)))
                                            oDBDSDetail.SetValue("U_LineTotal", pVal.Row - 1, dblValue)
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            LineTotalDetails()

                                        Case "WhsCod"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_WhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail.SetValue("U_WhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                    End Select

                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "matPreSale"
                                Select Case pVal.ColUID
                                    Case "ICode"
                                        If pVal.BeforeAction = False Then
                                            ChooseFromListFilteration(frmDummySalesQuotation, "GROUP_CFL", "ItemCode", "Select ItemCode ,ItemName  from OITM Where ItemName In (Select U_GN From OITM) order by itemcode")
                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID

                            Case "matPreSale"
                                Select Case pVal.ColUID
                                    Case "V_1", "V_8"
                                        If pVal.BeforeAction = False And pVal.ItemChanged Then
                                            oMatrix.FlushToDataSource()
                                            Dim TaxAmount As Double = CDbl(oDBDSDetail.GetValue("U_TaxRate", pVal.Row - 1))
                                            Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_Quantity", pVal.Row - 1)) * CDbl(oDBDSDetail.GetValue("U_UnitPrice", pVal.Row - 1))
                                            oDBDSDetail.SetValue("U_BaseAmount", pVal.Row - 1, dblValue)
                                            oDBDSDetail.SetValue("U_LineTotal", pVal.Row - 1, dblValue + TaxAmount)
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            LineTotalDetails()
                                        End If
                                        'Case "TaxCode"
                                        '    If pVal.BeforeAction = False And pVal.ItemChanged Then
                                        '        oMatrix.FlushToDataSource()
                                        '        Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_BaseAmount", pVal.Row - 1)) + CDbl(oDBDSDetail.GetValue("U_TaxRate", pVal.Row - 1))
                                        '        oDBDSDetail.SetValue("U_LineTotal", pVal.Row - 1, dblValue)
                                        '        oMatrix.LoadFromDataSource()
                                        '        oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        '        LineTotalDetails()
                                        '    End If
                                End Select
                        End Select
                    Catch ex As Exception
                    End Try

                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmDummySalesQuotation.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmDummySalesQuotation.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmDummySalesQuotation.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1000002"
                                If pVal.BeforeAction = False Then
                                    LoadPreSalesOrderDetails()
                                End If


                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.ActionSuccess And frmDummySalesQuotation.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Try
                        If BusinessObjectInfo.ActionSuccess Then


                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add ,Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub RightClickEvent(ByRef EventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case EventInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK
                    If oApplication.Forms.Item(EventInfo.FormUID).Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And EventInfo.BeforeAction Then
                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

End Class
