﻿
Imports SAPLib
Imports System.Globalization

Public Class DayWisePlan
    Dim frmDayWisePlan As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim boolFormLoaded As Boolean = False
    Dim sFormUID As String
    Dim UDOID As String = "ODWP"
    Dim sQuery As String = String.Empty
    Dim iRowIndex As Integer
    Dim sColumnID As String

    Sub LoadDayWisePlan()
        Try


            boolFormLoaded = False
            LoadXML(frmDayWisePlan, DayWisePlanFormID, DayWisePlanXML)
            frmDayWisePlan = oApplication.Forms.Item(DayWisePlanFormID)
            oDBDSHeader = frmDayWisePlan.DataSources.DBDataSources.Item(0)
            oDBDSDetail = frmDayWisePlan.DataSources.DBDataSources.Item(1)
            oMatrix = frmDayWisePlan.Items.Item("m_daywise").Specific


            frmDayWisePlan.Freeze(True)
            ' 
            frmDayWisePlan.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            Me.InitForm()
            Me.DefineModeForFields()


        Catch ex As Exception
            oApplication.StatusBar.SetText("Load Day Wise Plan Form Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmDayWisePlan.Freeze(False)
        End Try
    End Sub
    Sub InitForm()
        Try
            frmDayWisePlan.Freeze(True)
            LoadComboBoxSeries(frmDayWisePlan.Items.Item("c_series").Specific, UDOID)
            LoadDocumentDate(frmDayWisePlan.Items.Item("t_DocDate").Specific)
            oMatrix.Clear()
            SetNewLine(oMatrix, oDBDSDetail)
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmDayWisePlan.Freeze(False)
        End Try
    End Sub

    Sub AutoSumEnable()
        Try
            Try
                oMatrix.Columns.Item("tqty").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix.Columns.Item("PQty").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix.Columns.Item("V_5").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto


            Catch ex As Exception
                oApplication.StatusBar.SetText("AutoSumEnable " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            End Try

        Catch ex As Exception
            oApplication.StatusBar.SetText("AutoSumEnable " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 

    Sub DefineModeForFields()
        Try
            frmDayWisePlan.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmDayWisePlan.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmDayWisePlan.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            '4
            frmDayWisePlan.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmDayWisePlan.Items.Item("t_DocDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()
                Case "1294"
                    'Dim tqty As String = oMatrix.Columns.Item("tqty").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'Dim PQty As String = oMatrix.Columns.Item("PQty").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.FlushToDataSource()
                    'oMatrix.AddRow()
                    'oMatrix.Columns.Item("PQty").Cells.Item(oMatrix.VisualRowCount).Specific.value = (tqty - PQty).ToString()
                    'oMatrix.Columns.Item("bentry").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("bentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("basenum").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("basenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("object").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("object").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("BaseLine").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("BaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("pbentry").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pbentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("pbasenum").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pbasenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("pBaseLine").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pBaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("CCode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("CCode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("V_13").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_13").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("PICode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("PICode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("PIName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("PIName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("ICode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("ICode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("V_11").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_11").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("tqty").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("tqty").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("Mcode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("Mcode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("V_9").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_9").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("V_8").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_8").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("cf").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("cf").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("ReqDate").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("ReqDate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("WhsCod").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("WhsCod").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("WhsName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("WhsName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("V_5").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_5").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("FDate").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("FDate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("TDate").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("TDate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("V_2").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_2").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("V_1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_1").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.GetCellSpecific(0, oMatrix.VisualRowCount).Value = oMatrix.VisualRowCount
                    'oMatrix.FlushToDataSource()

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub ItemEvent(FormUID As String, pVal As SAPbouiCOM.ItemEvent, BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID
                                Case "t_SCode"
                                    If pVal.BeforeAction = False Then
                                        oDBDSHeader.SetValue("U_ScenarioCode", 0, Trim(oDataTable.GetValue("U_ScenarioCode", 0).ToString()))
                                        oDBDSHeader.SetValue("U_ScenarioName", 0, Trim(oDataTable.GetValue("U_ScenarioName", 0).ToString()))
                                        LoadPreSalesOrderDetails()
                                    End If
                                    
                                Case "m_daywise"
                                    Select Case pVal.ColUID
                                        Case "V_14"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_CustomerCode", pVal.Row - 1, Trim(oDataTable.GetValue("CardCode", 0)))
                                            oDBDSDetail.SetValue("U_CustomerName", pVal.Row - 1, Trim(oDataTable.GetValue("CardName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        Case "V_12"

                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_ItemCode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail.SetValue("U_ItemName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                    End Select

                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try



                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "m_daywise"
                                Select Case pVal.ColUID
                                    Case "VCode"
                                        If pVal.BeforeAction = False Then
                                            ChooseFromListFilteration(frmDayWisePlan, "COST_CFL", "PrcCode", "Select  PrcCode    From  OPRC Where DimCode in (1,2)")
                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID

                            Case "m_daywise"
                                Select Case pVal.ColUID
                                    Case "FDate"

                                        If pVal.BeforeAction = False And pVal.ItemChanged Then
                                            Dim sFDate As String = oMatrix.Columns.Item("FDate").Cells.Item(pVal.Row).Specific.value.ToString()
                                            If sFDate = String.Empty Then
                                                Return
                                            End If
                                            Dim sDocDate As String = frmDayWisePlan.Items.Item("t_DocDate").Specific.value.ToString()
                                            Dim sQuery As String = String.Empty
                                            sQuery = "Exec [dbo].[@AIS_DaywisPlan_fromDate] '" + sFDate + "','" + sDocDate + "'"
                                            Dim rsetEmpDets_DateValidation As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            Dim strBoolen As Boolean = False

                                            If rsetEmpDets_DateValidation.RecordCount > 0 Then
                                                strBoolen = Convert.ToBoolean(Trim(rsetEmpDets_DateValidation.Fields.Item("U_CycleTime").Value))
                                            End If
                                            If strBoolen = False Then
                                                oMatrix.Columns.Item("FDate").Cells.Item(pVal.Row).Specific.value = String.Empty
                                                oApplication.StatusBar.SetText("From Date Shoud not be Past Date , Line Id : " + pVal.Row.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                                Return

                                            End If

                                        End If

                                    Case "PQty", "V_2"
                                        If pVal.BeforeAction = False And pVal.ItemChanged Then

                                            Dim sFDate As String = oMatrix.Columns.Item("FDate").Cells.Item(pVal.Row).Specific.value.ToString()
                                            If sFDate = String.Empty Then
                                                Return
                                            End If

                                            Dim sDocDate As String = frmDayWisePlan.Items.Item("t_DocDate").Specific.value.ToString()
                                            Dim sQuery As String = String.Empty
                                            sQuery = "Exec [dbo].[@AIS_DaywisPlan_fromDate] '" + sFDate + "','" + sDocDate + "'"
                                            Dim rsetEmpDets_DateValidation As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            Dim strBoolen As Boolean = False

                                            If rsetEmpDets_DateValidation.RecordCount > 0 Then
                                                strBoolen = Convert.ToBoolean(Trim(rsetEmpDets_DateValidation.Fields.Item("U_CycleTime").Value))
                                            End If
                                            If strBoolen = False Then
                                                oApplication.StatusBar.SetText("From Date Shoud not be Past Date , Line Id : " + pVal.Row.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                                Return

                                            End If
                                            oMatrix.FlushToDataSource()
                                            If Trim(oDBDSDetail.GetValue("U_FromTime", pVal.Row - 1)) = String.Empty Then
                                                Return
                                            End If


                                            sQuery = "EXEC DBO.[@AIS_DaywisPlan_CalculateCycleTime] " + Trim(oDBDSDetail.GetValue("U_PlanningQty", pVal.Row - 1)) + ",'" + Trim(oDBDSDetail.GetValue("U_FromDate", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_PItemCode", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_MCCode", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_FromTime", pVal.Row - 1)) + "'  "



                                            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            If rsetEmpDets.RecordCount > 0 Then
                                                oDBDSDetail.SetValue("U_ReqHours", pVal.Row - 1, Trim(rsetEmpDets.Fields.Item("U_CycleTime").Value))
                                                If Trim(oDBDSDetail.GetValue("U_FromDate", pVal.Row - 1)) <> String.Empty Then
                                                    oDBDSDetail.SetValue("U_ToDate", pVal.Row - 1, CDate(rsetEmpDets.Fields.Item("ToDate").Value).ToString("yyyyMMdd"))
                                                    Dim sTotal As String = Trim(rsetEmpDets.Fields.Item("ToTime").Value)

                                                    oDBDSDetail.SetValue("U_ToTime", pVal.Row - 1, sTotal)
                                                Else
                                                    oDBDSDetail.SetValue("U_ToDate", pVal.Row - 1, String.Empty)
                                                    oDBDSDetail.SetValue("U_ToTime", pVal.Row - 1, String.Empty)
                                                End If

                                                oMatrix.LoadFromDataSource()
                                                oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                            End If

                                            sQuery = "EXEC DBO.[@AIS_PPC_DailyPlan_AllocationTesting] '" + Trim(oDBDSDetail.GetValue("U_FromDate", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_ToDate", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_MCCode", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_ReqHours", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_FromTime", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_ToTime", pVal.Row - 1)) + "'"

                                            Dim rsetEmpDets2 As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            If rsetEmpDets2.RecordCount > 0 Then
                                                If rsetEmpDets2.Fields.Item(0).Value <> "0" Then
                                                    oApplication.StatusBar.SetText(rsetEmpDets2.Fields.Item(0).Value.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                                    oDBDSDetail.SetValue("U_ToDate", pVal.Row - 1, String.Empty)
                                                    oDBDSDetail.SetValue("U_ToTime", pVal.Row - 1, String.Empty)
                                                    oDBDSDetail.SetValue("U_FromTime", pVal.Row - 1, String.Empty)
                                                    oMatrix.LoadFromDataSource()
                                                    oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                                    Return
                                                End If
                                            End If



                                        End If

                                End Select

                        End Select
                    Catch ex As Exception
                    End Try
                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmDayWisePlan.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmDayWisePlan.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmDayWisePlan.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID


                            Case "btnDup"
                                If pVal.BeforeAction = False Then
                                    For i As Integer = 1 To oMatrix.VisualRowCount
                                        If oMatrix.IsRowSelected(i) = True Then
                                            iRowIndex = i
                                            Exit For
                                        End If
                                    Next
                                    Dim tqty As String = oMatrix.Columns.Item("tqty").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    Dim PQty As String = oMatrix.Columns.Item("PQty").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.FlushToDataSource()
                                    oMatrix.AddRow()
                                    oMatrix.Columns.Item("tqty").Cells.Item(oMatrix.VisualRowCount).Specific.value = (tqty - PQty).ToString()
                                    oMatrix.Columns.Item("bentry").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("bentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("basenum").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("basenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("object").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("object").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("BaseLine").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("BaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("pbentry").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pbentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("pbasenum").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pbasenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("pBaseLine").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pBaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("CCode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("CCode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("V_13").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_13").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("PICode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("PICode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("PIName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("PIName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("ICode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("ICode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("V_11").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_11").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("Mcode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("Mcode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("V_9").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_9").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("V_8").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_8").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("cf").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("cf").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("ReqDate").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("ReqDate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("WhsCod").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("WhsCod").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("WhsName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("WhsName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("V_5").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_5").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("FDate").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("FDate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("TDate").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("TDate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("V_2").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_2").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("V_1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_1").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.GetCellSpecific(0, oMatrix.VisualRowCount).Value = oMatrix.VisualRowCount
                                    oMatrix.FlushToDataSource()
                                    oApplication.StatusBar.SetText("Form Update Finished", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                    frmDayWisePlan.Update()

                                    'For i As Integer = 1 To oMatrix.VisualRowCount
                                    '    If oMatrix.IsRowSelected(i) = True Then
                                    '        iRowIndex = i
                                    '        Exit For
                                    '    End If
                                    'Next
                                    'oMatrix.FlushToDataSource()
                                    'oMatrix.AddRow()
                                    'oMatrix.Columns.Item("bentry").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("bentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("basenum").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("basenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("object").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("object").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("BaseLine").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("BaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("pbentry").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pbentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("pbasenum").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pbasenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("pBaseLine").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pBaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("V_0").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_0").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("V_10").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_10").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("date").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("date").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("pdate").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pdate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("ICode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("ICode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("IName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("IName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("MName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("MName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("WhsCod").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("WhsCod").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("WhsName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("WhsName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    'oMatrix.Columns.Item("V_7").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_7").Cells.Item(iRowIndex).Specific.value.ToString().Trim()

                                    'Dim sPft As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("Pft").Cells.Item(iRowIndex).Specific
                                    'Dim sPft2 As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("Pft").Cells.Item(oMatrix.VisualRowCount).Specific
                                    'If Not sPft.Selected Is Nothing Then
                                    '    sPft2.Select(sPft.Selected.Value.ToString(), SAPbouiCOM.BoSearchKey.psk_ByValue)
                                    'End If
                                    'Dim scof As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("cof").Cells.Item(iRowIndex).Specific
                                    'Dim scof2 As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("cof").Cells.Item(oMatrix.VisualRowCount).Specific
                                    'If Not scof.Selected Is Nothing Then
                                    '    scof2.Select(scof.Selected.Value.ToString(), SAPbouiCOM.BoSearchKey.psk_ByValue)
                                    'End If

                                    'Dim sverify As SAPbouiCOM.CheckBox = oMatrix.Columns.Item("verify").Cells.Item(iRowIndex).Specific
                                    'Dim sverify2 As SAPbouiCOM.CheckBox = oMatrix.Columns.Item("verify").Cells.Item(oMatrix.VisualRowCount).Specific
                                    'sverify2.Checked = sverify.Checked
                                    'oMatrix.GetCellSpecific(0, oMatrix.VisualRowCount).Value = oMatrix.VisualRowCount
                                    'oMatrix.FlushToDataSource()

                                    'If frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                    '    frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                    'End If


                                End If
                            Case "b_sales"
                                Try
                                    If pVal.BeforeAction = False Then
                                        If Me.TransactionManagement() = False Then
                                            If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                            BubbleEvent = False


                                        End If

                                        'If Me.TransactionManagementProductionOrder() = False Then
                                        '    If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                        '    BubbleEvent = False
                                        'End If
                                        'If frmDayWisePlan.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                        '    frmDayWisePlan.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                        '    frmDayWisePlan.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                        'Else
                                        '    frmDayWisePlan.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                        'End If



                                    End If


                                Catch ex As Exception
                                    oApplication.StatusBar.SetText("Click(1) Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                Finally
                                End Try

                            Case "b_worder"
                                Try
                                    If pVal.BeforeAction = False Then
                                        If Me.TransactionManagementProductionOrder() = False Then
                                            If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                            BubbleEvent = False
                                        End If
                                    End If
                                    'If frmDayWisePlan.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                    '    frmDayWisePlan.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                    '    frmDayWisePlan.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    'Else
                                    '    frmDayWisePlan.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    'End If

                                Catch ex As Exception
                                    oApplication.StatusBar.SetText("Click(1) Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                Finally
                                End Try

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.ActionSuccess And frmDayWisePlan.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    oApplication.StatusBar.SetText("Form 1 Item Click Finished", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                    InitForm()
                                End If
                            
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Function ValidateAll() As Boolean
        Try

            ValidateAll = True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

    Sub LoadPreSalesOrderDetails()
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the Presales Order Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            sQuery = " EXEC [dbo].[@AIS_PPC_GetRecommendationItemDetails] '" & Trim(oDBDSHeader.GetValue("U_ScenarioCode", 0)) & "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
            rsetEmpDets.MoveFirst()
            oDBDSDetail.Clear()
            oMatrix.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                oDBDSDetail.Offset = i

                oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, i + 1)

                oDBDSDetail.SetValue("U_BaseEntry", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("DocEntry").Value)
                oDBDSDetail.SetValue("U_BaseNum", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("DocNum").Value)
                oDBDSDetail.SetValue("U_BaseObject", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("Object").Value)
                oDBDSDetail.SetValue("U_BaseLine", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("LineId").Value)
                oDBDSDetail.SetValue("U_CardCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("CardCode").Value)
                oDBDSDetail.SetValue("U_CardName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("CardName").Value)
                oDBDSDetail.SetValue("U_ItemCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ItemCode").Value)
                oDBDSDetail.SetValue("U_ItemName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ItemName").Value)

                oDBDSDetail.SetValue("U_PItemCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("PItemCode").Value.ToString())
                oDBDSDetail.SetValue("U_PItemName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("PItemName").Value.ToString())


                oDBDSDetail.SetValue("U_TotalQty", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_OrderQty").Value)
                oDBDSDetail.SetValue("U_WhsCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_WhsCode").Value)
                oDBDSDetail.SetValue("U_WhsName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_WhsName").Value)
                oDBDSDetail.SetValue("U_MCCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_MacCode").Value.ToString())
                oDBDSDetail.SetValue("U_MCName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_MacName").Value.ToString())
                oDBDSDetail.SetValue("U_PostingDate", oDBDSDetail.Offset, CDate(rsetEmpDets.Fields.Item("U_PostingDate").Value).ToString("yyyyMMdd"))
                oDBDSDetail.SetValue("U_ReqDate", oDBDSDetail.Offset, CDate(rsetEmpDets.Fields.Item("U_ReqDate").Value).ToString("yyyyMMdd"))
                oDBDSDetail.SetValue("U_ChangeOverFil", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ChangeOverFil").Value.ToString())
                oDBDSDetail.SetValue("U_PBaseEntry", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_PBaseEntry").Value)
                oDBDSDetail.SetValue("U_PBaseNum", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_PBaseNum").Value)
                oDBDSDetail.SetValue("U_PBaseLine", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_PBaseLine").Value)
                oDBDSDetail.SetValue("U_Remarks", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_Remarks").Value.ToString())

                rsetEmpDets.MoveNext()
            Next
            oMatrix.LoadFromDataSource()
            AutoSumEnable()
            If frmDayWisePlan.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmDayWisePlan.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Load Day Wise Plan Details successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Employees Based  Load Day Wise Plan Details  Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


    Function TransactionManagement() As Boolean
        Try
            Dim boolGoToStockPosting As Boolean = False
            TransactionManagement = True
            oApplication.SetStatusBarMessage("Please Wait,System is Checking Validation and Posting Stock", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
            Dim boolJournalTrue As Boolean = False
            If Me.OpenSalesDelivery() = False Then
                Return False
            End If
            TransactionManagement = True
        Catch ex As Exception
            TransactionManagement = False
            StatusBarErrorMsg("Transaction Management Failed : " & ex.Message)
            TransactionManagement = False
        Finally
        End Try
    End Function


    Function TransactionManagementProductionOrder() As Boolean
        Try
            Dim boolGoToStockPosting As Boolean = False
            TransactionManagementProductionOrder = True
            oApplication.SetStatusBarMessage("Please Wait,System is Checking Validation and Posting Stock", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
            Dim boolJournalTrue As Boolean = False
            If Me.WorkOrder() = False Then
                Return False
            End If
            TransactionManagementProductionOrder = True
        Catch ex As Exception
            TransactionManagementProductionOrder = False
            StatusBarErrorMsg("Transaction Management Failed : " & ex.Message)
            TransactionManagementProductionOrder = False
        Finally
        End Try
    End Function


    Function OpenSalesDelivery() As Boolean
        Try
            Dim ErrCode As Long
            Dim ErrMsg As String = ""


            sQuery = " Select DocEntry  From ORDR Where U_BaseNum ='" + oDBDSHeader.GetValue("DocNum", 0) + "' AND U_BaseObject ='ODWP'AND U_Series ='" + oDBDSHeader.GetValue("Series", 0) + "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)


            If rsetEmpDets.RecordCount > 0 Then
                oApplication.StatusBar.SetText("Already Sales Order Created For This Document", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return True
            End If

            Dim sDataisOk As Boolean = False

            Dim sLoop_BaseEntry As New ArrayList()
            For ik As Integer = 1 To oMatrix.VisualRowCount
                Dim sCardCode As String = oMatrix.Columns.Item("CCode").Cells.Item(ik).Specific.value.ToString().Trim()
                If Not (sLoop_BaseEntry.Contains(sCardCode)) Then
                    sLoop_BaseEntry.Add(sCardCode)
                End If
            Next

            For i As Integer = 0 To sLoop_BaseEntry.Count - 1
                Dim oSalesOrder As SAPbobsCOM.Documents = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders)
                Dim sBoolean As Boolean = False
                For ik As Integer = 1 To oMatrix.VisualRowCount
                    Dim sInnerCardCodea As String = oMatrix.Columns.Item("CCode").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim ReqDate As String = oMatrix.Columns.Item("ReqDate").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim Mcode As String = oMatrix.Columns.Item("Mcode").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim BaseLine As String = oMatrix.Columns.Item("pBaseLine").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim basenum As String = oMatrix.Columns.Item("pbasenum").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim bentry As String = oMatrix.Columns.Item("pbentry").Cells.Item(ik).Specific.value.ToString().Trim()

                    Dim sQuery As String
                    Dim dKiddle As String = String.Empty
                    sQuery = "Select U_MacType  From ORSC Where U_MacType ='K' AND ResCode ='" + Mcode + "'"
                    Dim Ors As SAPbobsCOM.Recordset
                    Ors = DoQuery(sQuery)
                    If Ors.RecordCount > 0 Then
                        dKiddle = "Y"
                    End If

                    If sInnerCardCodea = sLoop_BaseEntry.Item(i).ToString() And dKiddle = String.Empty Then
                        sDataisOk = True
                        If sBoolean = False Then
                            oSalesOrder.CardCode = sInnerCardCodea
                            oSalesOrder.DocDate = Date.Now
                            oSalesOrder.TaxDate = Format_StringToDate(ReqDate)
                            oSalesOrder.DocDueDate = Format_StringToDate(ReqDate)
                            oSalesOrder.Reference2 = oDBDSHeader.GetValue("DocNum", 0)
                            oSalesOrder.Comments = " Day Wise Plan." & oDBDSHeader.GetValue("DocNum", 0)
                            oSalesOrder.UserFields.Fields.Item("U_BaseNum").Value = oDBDSHeader.GetValue("DocNum", 0)
                            oSalesOrder.UserFields.Fields.Item("U_BaseObject").Value = "ODWP"
                            oSalesOrder.UserFields.Fields.Item("U_Series").Value = oDBDSHeader.GetValue("Series", 0)
                            oSalesOrder.UserFields.Fields.Item("U_PBaseline").Value = BaseLine
                            oSalesOrder.UserFields.Fields.Item("U_PBaseNum").Value = basenum
                            oSalesOrder.UserFields.Fields.Item("U_PBEntry").Value = bentry

                            sBoolean = True
                        End If

                        Dim sItemCode As String = oMatrix.Columns.Item("ICode").Cells.Item(ik).Specific.value.Trim
                        Dim PQty As String = oMatrix.Columns.Item("PQty").Cells.Item(ik).Specific.value.Trim

                        Dim WhsCod As String = oMatrix.Columns.Item("WhsCod").Cells.Item(ik).Specific.value.Trim

                        oSalesOrder.Lines.ItemCode = sItemCode
                        oSalesOrder.Lines.Quantity = Trim(PQty)

                        Dim sQueryPresales As String
                        sQueryPresales = "Select  T0.U_TaxCode,T0.U_WhsCode,T0.U_UnitPrice From   [@AIS_PRS1] T0 Where DocEntry ='" + bentry + "' and LineId ='" + BaseLine + "'"
                        Dim OrsPresales As SAPbobsCOM.Recordset
                        OrsPresales = DoQuery(sQueryPresales)
                        If OrsPresales.RecordCount > 0 Then
                            oSalesOrder.Lines.WarehouseCode = OrsPresales.Fields.Item("U_WhsCode").Value
                            oSalesOrder.Lines.TaxCode = OrsPresales.Fields.Item("U_TaxCode").Value
                            oSalesOrder.Lines.Price = OrsPresales.Fields.Item("U_UnitPrice").Value
                        End If

                        oSalesOrder.Lines.Add()
                    End If
                Next
                If sDataisOk = True Then
                    If oSalesOrder.Add() = 0 Then
                        oApplication.StatusBar.SetText("Delivery Document  posted successfully ,Customer Code" + sLoop_BaseEntry.Item(i).ToString(), SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                    Else
                        oApplication.StatusBar.SetText("Unable To Sales Order Document" & oCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    End If
                Else
                    oApplication.StatusBar.SetText("No Data to Create Sales Order", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                End If
                ' 1. Post the GRN documents...

            Next



            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText(" Delivery Document   Posting Method Faild", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        Finally
        End Try
    End Function






    Function WorkOrder() As Boolean
        Try

            Dim strQuery, strQuery1 As String

            sQuery = " Select DocEntry  From [@AS_OWORD] Where Canceled ='N' and U_BaseNum ='" + oDBDSHeader.GetValue("DocNum", 0) + "' AND U_BaseObject ='ODWP'AND U_Series ='" + oDBDSHeader.GetValue("Series", 0) + "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)

            If rsetEmpDets.RecordCount > 0 Then
                oApplication.StatusBar.SetText("Already Work Order Created For This Document", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return True
            End If
            Dim sDataisOk As Boolean = False
            Dim sLoop_SFGCode As New ArrayList()
            Dim sLoop_ICode As New ArrayList()
            Dim sLoop_TDate As New ArrayList()
            Dim sLoop_FDate As New ArrayList()

            Dim sLoop_FTime As New ArrayList()
            Dim sLoop_TTime As New ArrayList()

            For ik As Integer = 1 To oMatrix.VisualRowCount
                Dim SFGCode As String = oMatrix.Columns.Item("CCode").Cells.Item(ik).Specific.value.ToString().Trim()
                Dim ICode As String = oMatrix.Columns.Item("ICode").Cells.Item(ik).Specific.value.ToString().Trim()
                Dim FDate As String = oMatrix.Columns.Item("FDate").Cells.Item(ik).Specific.value.ToString().Trim()
                Dim TDate As String = oMatrix.Columns.Item("TDate").Cells.Item(ik).Specific.value.ToString().Trim()
                Dim FTime As String = oMatrix.Columns.Item("V_2").Cells.Item(ik).Specific.value.ToString().Trim()
                Dim TTime As String = oMatrix.Columns.Item("V_1").Cells.Item(ik).Specific.value.ToString().Trim()
                If Not (sLoop_SFGCode.Contains(SFGCode)) Or Not (sLoop_ICode.Contains(ICode)) Or Not (sLoop_FDate.Contains(FDate)) Or Not (sLoop_TDate.Contains(TDate)) Or Not (sLoop_FTime.Contains(FTime)) Or Not (sLoop_TTime.Contains(TTime)) Then
                    sLoop_SFGCode.Add(SFGCode)
                    sLoop_ICode.Add(ICode)
                    sLoop_FDate.Add(FDate)
                    sLoop_TDate.Add(TDate)
                    sLoop_FTime.Add(FTime)
                    sLoop_TTime.Add(TTime)

                End If
            Next

            For i As Integer = 0 To sLoop_SFGCode.Count - 1
                For ik As Integer = 1 To oMatrix.VisualRowCount

                    oApplication.StatusBar.SetText("Data Loading" + ik.ToString(), SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Dim LineID As String = oMatrix.Columns.Item("V_-1").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim sInnerCardCodea As String = oMatrix.Columns.Item("CCode").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim ReqDate As String = oMatrix.Columns.Item("ReqDate").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim Mcode As String = oMatrix.Columns.Item("Mcode").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim MName As String = oMatrix.Columns.Item("V_9").Cells.Item(ik).Specific.value.ToString().Trim()

                    Dim sItemCode_2 As String = oMatrix.Columns.Item("ICode").Cells.Item(ik).Specific.value.Trim
                    Dim PQty As String = oMatrix.Columns.Item("PQty").Cells.Item(ik).Specific.value.Trim
                    Dim WhsCod As String = oMatrix.Columns.Item("WhsCod").Cells.Item(ik).Specific.value.Trim
                    Dim StartDate As String = (oMatrix.Columns.Item("FDate").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim EndDate As String = (oMatrix.Columns.Item("TDate").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim FTime As String = oMatrix.Columns.Item("V_2").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim TTime As String = oMatrix.Columns.Item("V_1").Cells.Item(ik).Specific.value.ToString().Trim()


                    If sInnerCardCodea = sLoop_SFGCode.Item(i).ToString() And sItemCode_2 = sLoop_ICode.Item(i).ToString() And StartDate = sLoop_FDate.Item(i).ToString() And EndDate = sLoop_TDate.Item(i).ToString() And FTime = sLoop_FTime.Item(i).ToString() And TTime = sLoop_TTime.Item(i).ToString() Then
                        Dim sQuery As String
                        Dim dKiddle As String = String.Empty
                        sQuery = "Select U_MacType  From ORSC Where   ResCode ='" + Mcode + "'"
                        Dim Ors_MC As SAPbobsCOM.Recordset
                        Ors_MC = DoQuery(sQuery)
                        If Ors_MC.RecordCount > 0 Then
                            dKiddle = Ors_MC.Fields.Item("U_MacType").Value
                        End If
                        Dim sDocEntry As String = String.Empty
                        Dim sDocNum As String = String.Empty
                        Dim sItemCode As String = String.Empty
                        Dim sWhsCode As String = String.Empty
                        Dim dQuantity As Double = 0
                        strQuery = "Select Top 1 T0.DocEntry,DocNum ,T1.ItemCode,T1.Quantity,T1.WhsCode From ORDR T0 Inner Join RDR1 T1 On T0.DocEntry =T1.DocEntry Where T0.CardCode='" + sInnerCardCodea + "' AND  T0.U_BaseNum='" + oDBDSHeader.GetValue("DocNum", 0) + "' AND T0.U_BaseObject='ODWP' AND T0.U_Series='" + oDBDSHeader.GetValue("Series", 0) + "' Order by T0.DocEntry Desc"

                        Dim Ors As SAPbobsCOM.Recordset
                        Ors = DoQuery(strQuery)
                        If Ors.RecordCount > 0 Then
                            sDocEntry = Ors.Fields.Item("DocEntry").Value
                            sDocNum = Ors.Fields.Item("DocNum").Value
                            sItemCode = Ors.Fields.Item("ItemCode").Value
                            sWhsCode = Ors.Fields.Item("WhsCode").Value
                            dQuantity = Convert.ToDouble(Ors.Fields.Item("Quantity").Value)
                        Else

                            sItemCode = sItemCode_2


                            sWhsCode = WhsCod
                            'dQuantity = PQty
                            For iqtys As Integer = 1 To oMatrix.VisualRowCount
                                Dim sInnerCardCodea_3 As String = oMatrix.Columns.Item("CCode").Cells.Item(iqtys).Specific.value.ToString().Trim()
                                Dim sItemCode_3 As String = oMatrix.Columns.Item("ICode").Cells.Item(iqtys).Specific.value.Trim
                                Dim PQty_3 As String = oMatrix.Columns.Item("PQty").Cells.Item(iqtys).Specific.value.Trim
                                Dim StartDate_3 As String = (oMatrix.Columns.Item("FDate").Cells.Item(iqtys).Specific.value.ToString().Trim())
                                Dim EndDate_3 As String = (oMatrix.Columns.Item("TDate").Cells.Item(iqtys).Specific.value.ToString().Trim())

                                Dim FTime_3 As String = oMatrix.Columns.Item("V_2").Cells.Item(iqtys).Specific.value.ToString().Trim()
                                Dim TTime_3 As String = oMatrix.Columns.Item("V_1").Cells.Item(iqtys).Specific.value.ToString().Trim()

                                If sInnerCardCodea_3 = sLoop_SFGCode.Item(i).ToString() And sItemCode_3 = sLoop_ICode.Item(i).ToString() And StartDate_3 = sLoop_FDate.Item(i).ToString() And EndDate_3 = sLoop_TDate.Item(i).ToString() And FTime_3 = sLoop_FTime.Item(i).ToString() And TTime_3 = sLoop_TTime.Item(i).ToString() Then
                                    dQuantity = dQuantity + PQty_3
                                End If
                            Next
                        End If


                        Dim sMachine As String = String.Empty
                        sMachine = "Y"
                        Dim sAttdStatus As String = String.Empty

                        Dim oGeneralService As SAPbobsCOM.GeneralService
                        Dim oGeneralData As SAPbobsCOM.GeneralData
                        Dim oChild As SAPbobsCOM.GeneralData
                        Dim oChildren As SAPbobsCOM.GeneralDataCollection
                        Dim sCmp As SAPbobsCOM.CompanyService = oCompany.GetCompanyService
                        oGeneralService = sCmp.GetGeneralService("AS_OWORD")
                        oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                        If dKiddle = "K" Then
                            oGeneralData.SetProperty("U_Type", "k")
                        ElseIf dKiddle = "ZB" Then
                            oGeneralData.SetProperty("U_Type", "DB")
                        ElseIf dKiddle = "ZA" Then
                            oGeneralData.SetProperty("U_Type", "DA")
                        End If
                        oGeneralData.SetProperty("U_DocDate", DateTime.Now)
                        oGeneralData.SetProperty("U_Status", "O")
                        If StartDate = String.Empty Then
                            oApplication.SetStatusBarMessage("Start Date Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                            Return False
                        End If
                        If EndDate = String.Empty Then
                            oApplication.SetStatusBarMessage("End  Date Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                            Return False
                        End If
                        Dim sStartDate2 As DateTime = DateTime.ParseExact(StartDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                        Dim sEndDate2 As DateTime = DateTime.ParseExact(EndDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                        oGeneralData.SetProperty("U_StartDate", sStartDate2)
                        oGeneralData.SetProperty("U_EndDate", sEndDate2)
                        oGeneralData.SetProperty("U_SalesOrderQty", dQuantity)
                        oGeneralData.SetProperty("U_ProducedQty", dQuantity)
                        oGeneralData.SetProperty("U_FGCode", sItemCode)

                        oGeneralData.SetProperty("U_MachineCode", Mcode)
                        oGeneralData.SetProperty("U_MachineName", MName)

                        Dim strQuerys = "Select ItemName  from  OITM Where ItemCode ='" + sItemCode + "'"
                        Dim Ors_Root_ItemName As SAPbobsCOM.Recordset
                        Ors_Root_ItemName = DoQuery(strQuerys)
                        Dim sItemnName As String = String.Empty
                        If Ors_Root_ItemName.RecordCount > 0 Then
                            sItemnName = Ors_Root_ItemName.Fields.Item("ItemName").Value
                        End If
                        oGeneralData.SetProperty("U_FgName", sItemnName)
                        oGeneralData.SetProperty("U_BaseLine", LineID)
                        oGeneralData.SetProperty("U_BaseNum", oDBDSHeader.GetValue("DocNum", 0))
                        oGeneralData.SetProperty("U_BaseObject", "ODWP")
                        oGeneralData.SetProperty("U_Series", oDBDSHeader.GetValue("Series", 0))
                        Dim RouteCode As String = String.Empty
                        Dim RouteName As String = String.Empty
                        Dim bLineDetails As Boolean = False
                        oChildren = oGeneralData.Child("AS_WORD1")
                        strQuery = "Select U_OperCode,U_OperName,U_MCCode,U_MCName,U_PCyc,U_Process, ''U_RuleC  ,'' as U_RuleN  from [@AIS_OIOD]  T0 Inner join  [@AIS_IOD1]   T1 On T0.DocEntry =T1.DocEntry Where T0.U_ItemCode ='" + sItemCode + "'"
                        Dim Ors_Root As SAPbobsCOM.Recordset
                        Ors_Root = DoQuery(strQuery)
                        Dim sOperationCode As String = String.Empty
                        If Ors_Root.RecordCount > 0 Then
                            bLineDetails = True
                            oChild = oChildren.Add
                            For k = 0 To Ors_Root.RecordCount - 1
                                oChildren.Item(k).SetProperty("U_OpCode", Ors_Root.Fields.Item("U_OperCode").Value)
                                If k = 0 Then
                                    sOperationCode = Ors_Root.Fields.Item("U_OperCode").Value.ToString()
                                End If
                                oChildren.Item(k).SetProperty("U_OpName", Ors_Root.Fields.Item("U_OperName").Value)
                                oChildren.Item(k).SetProperty("U_PCyc", Ors_Root.Fields.Item("U_PCyc").Value)

                                Dim sQty As Double = Ors_Root.Fields.Item("U_PCyc").Value * dQuantity
                                oChildren.Item(k).SetProperty("U_PCycle", sQty)

                                oChildren.Item(k).SetProperty("U_Process", Ors_Root.Fields.Item("U_Process").Value)
                                RouteCode = Ors_Root.Fields.Item("U_RuleC").Value
                                RouteName = Ors_Root.Fields.Item("U_RuleN").Value
                                oChild = oChildren.Add
                                Ors_Root.MoveNext()
                            Next
                        Else
                            oApplication.StatusBar.SetText(" No Operation Details, ItemCode" + sItemCode, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                        End If
                        If bLineDetails = False Then
                            oChild = oChildren.Add
                        End If
                        oGeneralData.SetProperty("U_RuleC", RouteCode)
                        oGeneralData.SetProperty("U_RuleN", RouteName)
                        sQuery = "EXEC DBO.[@AIS_PPC_GetMachineCycleTime] '" + sOperationCode + "','" + dQuantity.ToString() + "'"
                        Dim rsetEmpDets_GetRequiredHours As SAPbobsCOM.Recordset = DoQuery(sQuery)
                        If rsetEmpDets_GetRequiredHours.RecordCount > 0 Then
                            Dim sQ As String = Trim(rsetEmpDets_GetRequiredHours.Fields.Item("ReqHrs").Value)
                            oGeneralData.SetProperty("U_RequiredHrs", sQ)
                        Else
                            oGeneralData.SetProperty("U_RequiredHrs", "0")
                        End If


                        Dim sTBool As Boolean = False
                        oChildren = oGeneralData.Child("AS_WORD2")
                        strQuery = "Select Top 1 CardCode,CardName, T0.DocEntry,DocNum ,T1.ItemCode,T1.Quantity,T1.WhsCode From ORDR T0 Inner Join RDR1 T1 On T0.DocEntry =T1.DocEntry  Where T0.CardCode='" + sInnerCardCodea + "'  AND T0.U_BaseNum='" + oDBDSHeader.GetValue("DocNum", 0) + "' AND T0.U_BaseObject='ODWP' AND T0.U_Series='" + oDBDSHeader.GetValue("Series", 0) + "' Order By T0.DocEntry desc"
                        Dim Ors_SalesOrderDetails As SAPbobsCOM.Recordset
                        Ors_SalesOrderDetails = DoQuery(strQuery)
                        If Ors_SalesOrderDetails.RecordCount > 0 Then
                            sTBool = True
                            bLineDetails = True
                            oChild = oChildren.Add
                            For k = 0 To Ors_SalesOrderDetails.RecordCount - 1
                                sDocEntry = Ors.Fields.Item("DocEntry").Value
                                sDocNum = Ors.Fields.Item("DocNum").Value
                                sItemCode = Ors.Fields.Item("ItemCode").Value
                                sWhsCode = Ors.Fields.Item("WhsCode").Value
                                dQuantity = Convert.ToDouble(Ors.Fields.Item("Quantity").Value)
                                oChildren.Item(k).SetProperty("U_BaseEntry", Ors_SalesOrderDetails.Fields.Item("DocEntry").Value.ToString())
                                oChildren.Item(k).SetProperty("U_BaseNum", Ors_SalesOrderDetails.Fields.Item("DocNum").Value.ToString())
                                oChildren.Item(k).SetProperty("U_CardCode", Ors_SalesOrderDetails.Fields.Item("CardCode").Value)
                                oChildren.Item(k).SetProperty("U_CardName", Ors_SalesOrderDetails.Fields.Item("CardName").Value)
                                oChildren.Item(k).SetProperty("U_TotalQty", Ors_SalesOrderDetails.Fields.Item("Quantity").Value.ToString())
                                oChildren.Item(k).SetProperty("U_PendingQty", Ors_SalesOrderDetails.Fields.Item("Quantity").Value.ToString())
                                oChild = oChildren.Add
                                Ors_SalesOrderDetails.MoveNext()
                            Next
                        Else

                            oApplication.StatusBar.SetText(" No Sales Order Details, ItemCode" + sItemCode, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                        End If


                        If sTBool = False Then
                            oChild = oChildren.Add
                        End If
                        oChildren = oGeneralData.Child("AS_WORD2")
                        oChild = oChildren.Add

                        If dKiddle = "K" And bLineDetails = True Then
                            sTBool = True
                            bLineDetails = True
                            oChild = oChildren.Add
                            oChildren.Item(0).SetProperty("U_CardCode", oMatrix.Columns.Item("CCode").Cells.Item(1).Specific.value.ToString.Trim)
                            oChildren.Item(0).SetProperty("U_CardName", oMatrix.Columns.Item("V_13").Cells.Item(1).Specific.value.ToString.Trim)
                            oChild = oChildren.Add

                        End If

                        If bLineDetails = True Then
                            Dim dtDailyAttendanceSheet_Duplicate_Validation As New DataTable
                            If dtDailyAttendanceSheet_Duplicate_Validation.Rows.Count > 0 Then
                                oGeneralData.SetProperty("DocEntry", dtDailyAttendanceSheet_Duplicate_Validation.Rows(0)(0))
                                oGeneralService.Update(oGeneralData)
                            Else
                                oApplication.StatusBar.SetText(" Work Order Created Successfully", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                                oGeneralService.Add(oGeneralData)
                            End If
                        Else
                            oApplication.StatusBar.SetText(" Final Validation" & bLineDetails.ToString(), SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                        End If
                    End If

                Next

            Next

            Return True



        Catch ex As Exception
            oApplication.StatusBar.SetText(" Work Order Document   Posting Method Faild" + ex.Message.ToString(), SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        Finally

        End Try
    End Function





    'Function ProductionOrder() As Boolean
    '    Try
    '        Dim ErrCode As Long
    '        Dim ErrMsg As String = ""
    '        Dim sLoop_BaseEntry As New ArrayList()
    '        For ik As Integer = 1 To oMatrix.VisualRowCount
    '            Dim sCardCode As String = oMatrix.Columns.Item("CCode").Cells.Item(ik).Specific.value.ToString().Trim()
    '            If Not (sLoop_BaseEntry.Contains(sCardCode)) Then
    '                sLoop_BaseEntry.Add(sCardCode)
    '            End If
    '        Next

    '        For i As Integer = 0 To sLoop_BaseEntry.Count - 1

    '            Dim oSalesOrder As SAPbobsCOM.ProductionOrders = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders)

    '            Dim SalesOrderNo As String = String.Empty
    '            Dim SalesOrderEntry As String = String.Empty
    '            sQuery = " Select DocNum ,DocEntry  From ORDR T0  Where  U_BaseNum='" + oDBDSHeader.GetValue("DocNum", 0) + "' AND U_Series='" + oDBDSHeader.GetValue("Series", 0) + "'"

    '            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
    '            If rsetEmpDets.RecordCount > 0 Then
    '                rsetEmpDets.MoveFirst()
    '                SalesOrderNo = rsetEmpDets.Fields.Item("DocNum").Value
    '                SalesOrderEntry = rsetEmpDets.Fields.Item("DocEntry").Value
    '            End If



    '            For ik As Integer = 1 To oMatrix.VisualRowCount
    '                Dim sInnerCardCodea As String = oMatrix.Columns.Item("CCode").Cells.Item(ik).Specific.value.ToString().Trim()
    '                Dim FDate As String = oMatrix.Columns.Item("FDate").Cells.Item(ik).Specific.value.ToString().Trim()
    '                Dim strWhsCod As String = oMatrix.Columns.Item("WhsCod").Cells.Item(ik).Specific.value.ToString().Trim()
    '                Dim PQty As String = oMatrix.Columns.Item("PQty").Cells.Item(ik).Specific.value.ToString().Trim()
    '                Dim ICode As String = oMatrix.Columns.Item("ICode").Cells.Item(ik).Specific.value.ToString().Trim()

    '                If sInnerCardCodea = sLoop_BaseEntry.Item(i).ToString() Then

    '                    oSalesOrder.ProductionOrderType = SAPbobsCOM.BoProductionOrderTypeEnum.bopotStandard
    '                    oSalesOrder.CustomerCode = sInnerCardCodea
    '                    oSalesOrder.ItemNo = ICode
    '                    oSalesOrder.PostingDate = Date.Now
    '                    oSalesOrder.StartDate = Format_StringToDate(FDate)
    '                    oSalesOrder.DueDate = Format_StringToDate(FDate)
    '                    oSalesOrder.ProductionOrderOrigin = SAPbobsCOM.BoProductionOrderOriginEnum.bopooSalesOrder
    '                    oSalesOrder.ProductionOrderOriginEntry = SalesOrderEntry
    '                    oSalesOrder.Warehouse = strWhsCod
    '                    oSalesOrder.PlannedQuantity = PQty
    '                    oSalesOrder.UserFields.Fields.Item("U_BaseNum").Value = oDBDSHeader.GetValue("DocNum", 0)
    '                    oSalesOrder.UserFields.Fields.Item("U_BaseObject").Value = "ODWP"
    '                    oSalesOrder.UserFields.Fields.Item("U_Series").Value = oDBDSHeader.GetValue("Series", 0)




    '                End If

    '                ' 1. Post the GRN documents...
    '                If oSalesOrder.Add() = 0 Then
    '                    oApplication.StatusBar.SetText("Production Order Posted successfully,Product Code " + ICode.ToString(), SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
    '                Else
    '                    oApplication.StatusBar.SetText("Unable To Post Production Order " & oCompany.GetLastErrorDescription)
    '                End If


    '            Next

    '        Next



    '        Return True
    '    Catch ex As Exception
    '        oApplication.StatusBar.SetText(" Delivery Document   Posting Method Faild", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
    '        Return False
    '    Finally
    '    End Try
    'End Function

    Sub RightClickEvent(ByRef EventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case EventInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK
                    If frmDayWisePlan.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And EventInfo.BeforeAction Then
                        Select Case EventInfo.ItemUID
                            Case "m_daywise"
                                iRowIndex = EventInfo.Row
                                sColumnID = EventInfo.ColUID

                        End Select

                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Try
                        If BusinessObjectInfo.ActionSuccess Then


                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add ,Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        AutoSumEnable()
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
End Class
