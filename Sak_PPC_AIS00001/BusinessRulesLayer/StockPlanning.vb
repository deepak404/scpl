﻿
Imports SAPLib
Class StockPlanning
    Dim frmStockPlanning As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail, oDBDSDetail1 As SAPbouiCOM.DBDataSource
    Dim oMatrix, oMatrix1 As SAPbouiCOM.Matrix
    Dim boolFormLoaded As Boolean = False
    Dim sFormUID As String
    Dim UDOID As String = "OSAV"
    Dim sQuery As String
    Dim iRowIndex As Integer
    Dim sColumnID As String
    Sub LoadStockPlanning()
        Try


            boolFormLoaded = False
            LoadXML(frmStockPlanning, StockPlanningFormID, StockPlanningXML)
            frmStockPlanning = oApplication.Forms.Item(StockPlanningFormID)
            oDBDSHeader = frmStockPlanning.DataSources.DBDataSources.Item("@AIS_OSAV")
            oDBDSDetail = frmStockPlanning.DataSources.DBDataSources.Item("@AIS_SAV1")
            oDBDSDetail1 = frmStockPlanning.DataSources.DBDataSources.Item("@AIS_SAV2")
            oMatrix = frmStockPlanning.Items.Item("mat_StockP").Specific
            oMatrix1 = frmStockPlanning.Items.Item("mat_Stocka").Specific
            frmStockPlanning.PaneLevel = 1
            frmStockPlanning.Freeze(True)
            ' 

            Me.InitForm()
            Me.DefineModeForFields()


        Catch ex As Exception
            oApplication.StatusBar.SetText("Load Stock Planning Form Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmStockPlanning.Freeze(False)
        End Try
    End Sub




    Sub AutoSumEnable()
        Try
            Try
                oMatrix.Columns.Item("V_7").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix1.Columns.Item("OrdQty").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix1.Columns.Item("Instock").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix1.Columns.Item("TSock").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix1.Columns.Item("TSock2").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix1.Columns.Item("TSock3").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix1.Columns.Item("TSock4").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix1.Columns.Item("Bonding").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix1.Columns.Item("Total").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix1.Columns.Item("PreA").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix1.Columns.Item("Allocated").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto


            Catch ex As Exception
                oApplication.StatusBar.SetText("AutoSumEnable " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            End Try

        Catch ex As Exception
            oApplication.StatusBar.SetText("AutoSumEnable " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()
                Case "1294"
                    'oMatrix.FlushToDataSource()
                    'oMatrix.AddRow()
                    'oMatrix.Columns.Item("bentry").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("bentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("basenum").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("basenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("object").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("object").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("BaseLine").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("BaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("pbentry").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pbentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("pbasenum").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pbasenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("pBaseLine").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pBaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("V_0").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_0").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("V_10").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_10").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("date").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("date").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("pdate").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pdate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("ICode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("ICode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("IName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("IName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("MName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("MName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("WhsCod").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("WhsCod").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'oMatrix.Columns.Item("WhsName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("WhsName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                    'Dim sPft As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("Pft").Cells.Item(iRowIndex).Specific
                    'Dim sPft2 As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("Pft").Cells.Item(oMatrix.VisualRowCount).Specific
                    'If Not sPft.Selected Is Nothing Then
                    '    sPft2.Select(sPft.Selected.Value.ToString(), SAPbouiCOM.BoSearchKey.psk_ByValue)
                    'End If


                    'Dim scof As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("cof").Cells.Item(iRowIndex).Specific
                    'Dim scof2 As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("cof").Cells.Item(oMatrix.VisualRowCount).Specific
                    'If Not scof.Selected Is Nothing Then
                    '    scof2.Select(scof.Selected.Value.ToString(), SAPbouiCOM.BoSearchKey.psk_ByValue)
                    'End If

                    'Dim sverify As SAPbouiCOM.CheckBox = oMatrix.Columns.Item("verify").Cells.Item(iRowIndex).Specific
                    'Dim sverify2 As SAPbouiCOM.CheckBox = oMatrix.Columns.Item("verify").Cells.Item(oMatrix.VisualRowCount).Specific
                    'sverify2.Checked = sverify.Checked
                    'oMatrix.GetCellSpecific(0, oMatrix.VisualRowCount).Value = oMatrix.VisualRowCount
                    'oMatrix.FlushToDataSource()

                    ''If frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    ''    frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                    ''End If
                    'frmStockPlanning.Items.Item("t_SCode").Click(SAPbouiCOM.BoCellClickType.ct_Regular)


            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


    Sub LoadPreSalesOrderDetails()
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the Presales Order Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            sQuery = " EXEC [dbo].[@AIS_PPC_GetMachineAavailabilityItemDetails] '" & Trim(oDBDSHeader.GetValue("U_ScenarioCode", 0)) & "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
            rsetEmpDets.MoveFirst()
            oDBDSDetail.Clear()
            oMatrix.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                oDBDSDetail.Offset = i

                oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, i + 1)

                oDBDSDetail.SetValue("U_BaseEntry", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("DocEntry").Value)
                oDBDSDetail.SetValue("U_BaseNum", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("DocNum").Value)
                oDBDSDetail.SetValue("U_BaseObject", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("Object").Value)
                oDBDSDetail.SetValue("U_BaseLine", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("LineId").Value)
                oDBDSDetail.SetValue("U_CardCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("CardCode").Value)
                oDBDSDetail.SetValue("U_CardName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("CardName").Value)
                'oDBDSDetail.SetValue("U_ItemGrpCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("ItmsGrpCod").Value)
                'oDBDSDetail.SetValue("U_ItemGrpName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("ItmsGrpNam").Value)
                oDBDSDetail.SetValue("U_ItemCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ItemCode").Value)
                oDBDSDetail.SetValue("U_ItemName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ItemName").Value)
                oDBDSDetail.SetValue("U_OrderQty", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_TotalPlanQty").Value)
                oDBDSDetail.SetValue("U_WhsCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_WhsCode").Value)
                oDBDSDetail.SetValue("U_WhsName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_WhsName").Value)
                oDBDSDetail.SetValue("U_MacCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_MacCode").Value.ToString())
                oDBDSDetail.SetValue("U_MacName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_MacName").Value.ToString())
                oDBDSDetail.SetValue("U_PreFilType", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ChangeOverFil").Value.ToString())
                oDBDSDetail.SetValue("U_PostingDate", oDBDSDetail.Offset, CDate(rsetEmpDets.Fields.Item("U_PostingDate").Value).ToString("yyyyMMdd"))
                oDBDSDetail.SetValue("U_ReqDate", oDBDSDetail.Offset, CDate(rsetEmpDets.Fields.Item("U_ReqDate").Value).ToString("yyyyMMdd"))


                oDBDSDetail.SetValue("U_PBaseEntry", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_BaseEntry").Value)
                oDBDSDetail.SetValue("U_PBaseNum", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_BaseNum").Value)
                oDBDSDetail.SetValue("U_PBaseLine", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_BaseLine").Value)
                oDBDSDetail.SetValue("U_Remarks", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_Remarks").Value.ToString())
                rsetEmpDets.MoveNext()
            Next
            oMatrix.LoadFromDataSource()
            AutoSumEnable()
            If frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Monthly attendance sheet calculated successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Employees Based On Department Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub LoadStockItemDetails(boole As Boolean)
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the Presales Order Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)


            If boole = True Then
                oDBDSDetail1.Clear()
                oMatrix1.Clear()
                Dim sRMICode As String = "Select ItemCode From OITM Where ItemCode In ('"

                For i As Integer = 1 To oMatrix.VisualRowCount

                    Dim oCmbSerial As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("cof").Cells.Item(i).Specific
                    Dim strRequireDate As String = oMatrix.Columns.Item("date").Cells.Item(i).Specific.value
                    Dim ICode As String = oMatrix.Columns.Item("ICode").Cells.Item(i).Specific.value

                    Dim CardCode As String = oMatrix.Columns.Item("V_0").Cells.Item(i).Specific.value
                    Dim CardName As String = oMatrix.Columns.Item("V_10").Cells.Item(i).Specific.value
                    Dim OrderedQty As String = oMatrix.Columns.Item("V_7").Cells.Item(i).Specific.value

                    Dim PreSalesItemCode As String = oMatrix.Columns.Item("ICode").Cells.Item(i).Specific.value
                    Dim PreSalesItemName As String = oMatrix.Columns.Item("IName").Cells.Item(i).Specific.value


                    If i = 1 Then
                        sRMICode = sRMICode + ICode + "'"
                    Else
                        sRMICode = sRMICode + ",'" + ICode + "'"
                    End If
                    Dim strSerialCode As String

                    If Not oCmbSerial.Selected Is Nothing Then
                        strSerialCode = oCmbSerial.Selected.Value
                    Else
                        strSerialCode = String.Empty
                    End If

                    If strSerialCode <> String.Empty Then
                        sQuery = "[@AIS_GetRawMaterialItemDetais] '" + strSerialCode + "','" + strRequireDate + "'"
                        Dim rsetEmpDets_GetRawMaterial As SAPbobsCOM.Recordset = DoQuery(sQuery)

                        If rsetEmpDets_GetRawMaterial.RecordCount > 0 Then
                            oDBDSDetail1.InsertRecord(oDBDSDetail1.Size)
                            oDBDSDetail1.Offset = i - 1
                            oDBDSDetail1.SetValue("LineID", oDBDSDetail1.Offset, i + 1)
                            oDBDSDetail1.SetValue("U_CustCode", oDBDSDetail1.Offset, CardCode)
                            oDBDSDetail1.SetValue("U_CustName", oDBDSDetail1.Offset, CardName)
                            oDBDSDetail1.SetValue("U_OrderQty", oDBDSDetail1.Offset, OrderedQty)
                            oDBDSDetail1.SetValue("U_FGItemCode", oDBDSDetail1.Offset, PreSalesItemCode)
                            oDBDSDetail1.SetValue("U_FGItemName", oDBDSDetail1.Offset, PreSalesItemName)
                            oDBDSDetail1.SetValue("U_ItemCode", oDBDSDetail1.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("ItemCode").Value)
                            oDBDSDetail1.SetValue("U_ItemName", oDBDSDetail1.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("ItemName").Value)
                            If rsetEmpDets_GetRawMaterial.Fields.Item("Type").Value.ToString().Trim() = "1" Then
                                oDBDSDetail1.SetValue("U_Instock", oDBDSDetail1.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("InStock").Value)
                            Else
                                oDBDSDetail1.SetValue("U_BondingStock", oDBDSDetail1.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("InStock").Value)
                            End If

                            oDBDSDetail1.SetValue("U_TransitStock", oDBDSDetail1.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("TStock").Value)
                            oDBDSDetail1.SetValue("U_TransitStock2", oDBDSDetail1.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("TStock2").Value)
                            oDBDSDetail1.SetValue("U_TransitStock3", oDBDSDetail1.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("TStock3").Value)
                            oDBDSDetail1.SetValue("U_TransitStock4", oDBDSDetail1.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("TStock4").Value)
                            oDBDSDetail1.SetValue("U_AlloctedQty", oDBDSDetail1.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("PreAllocated").Value)
                            oDBDSDetail1.SetValue("U_Total", oDBDSDetail1.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("Total").Value)
                            oDBDSDetail1.SetValue("U_ChangeOverFil", oDBDSDetail1.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("U_FT").Value)
                            rsetEmpDets_GetRawMaterial.MoveNext()
                        End If




                    End If



                Next
                oMatrix1.LoadFromDataSource()
                sRMICode = sRMICode + ")"
                ChooseFromListFilteration(frmStockPlanning, "ITEM_FGCFL", "ItemCode", sRMICode)

            End If


            If frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Data Loaded  Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub ItemEvent(FormUID As String, pVal As SAPbouiCOM.ItemEvent, BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID
                                Case "t_SCode"
                                    oDBDSHeader.SetValue("U_ScenarioCode", 0, Trim(oDataTable.GetValue("U_ScenarioCode", 0).ToString()))
                                    oDBDSHeader.SetValue("U_ScenarioName", 0, Trim(oDataTable.GetValue("U_ScenarioName", 0).ToString()))

                                    LoadPreSalesOrderDetails()

                                Case "mat_Stocka"
                                    Select Case pVal.ColUID
                                        Case "Pcode"
                                            oMatrix1.FlushToDataSource()
                                            oDBDSDetail1.SetValue("U_FGItemCode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail1.SetValue("U_FGItemName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oMatrix1.LoadFromDataSource()
                                            oMatrix1.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()


                                        Case "V_0"
                                            oMatrix1.FlushToDataSource()
                                            oDBDSDetail1.SetValue("U_ItmCode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail1.SetValue("U_ItmName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oMatrix1.LoadFromDataSource()
                                            oMatrix1.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                    End Select

                                Case "mat_StockP"
                                    Select Case pVal.ColUID
                                        Case "V_0"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_CusCode", pVal.Row - 1, Trim(oDataTable.GetValue("CardCode", 0)))
                                            oDBDSDetail.SetValue("U_CusName", pVal.Row - 1, Trim(oDataTable.GetValue("CardName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        Case "V_9"

                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_ItemGrpCode", pVal.Row - 1, Trim(oDataTable.GetValue("ItmsGrpCod", 0)))
                                            oDBDSDetail.SetValue("U_ItemGrpName", pVal.Row - 1, Trim(oDataTable.GetValue("ItmsGrpNam", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                    End Select

                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try



                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "mDiesel"
                                Select Case pVal.ColUID
                                    Case "VCode"
                                        If pVal.BeforeAction = False Then
                                            ChooseFromListFilteration(frmStockPlanning, "COST_CFL", "PrcCode", "Select  PrcCode    From  OPRC Where DimCode in (1,2)")
                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID

                            Case "mat_StockP"
                                Select Case pVal.ColUID
                                    Case "Litres"
                                        If pVal.BeforeAction = False And pVal.ItemChanged Then
                                            oMatrix.FlushToDataSource()
                                            Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_Litres", pVal.Row - 1)) * CDbl(oDBDSDetail.GetValue("U_Price", pVal.Row - 1))
                                            oDBDSDetail.SetValue("U_Total", pVal.Row - 1, dblValue)
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                    End Try
                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmStockPlanning.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmStockPlanning.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "matrix1"
                                Select Case pVal.ColUID
                                    Case "Selected"
                                        If pVal.BeforeAction = False Then
                                            Try


                                            Catch ex As Exception
                                            Finally
                                            End Try

                                        End If
                                End Select

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID

                            Case "btnRef"
                                LoadStockItemDetails(True)

                            Case "btnDup"

                                If pVal.BeforeAction = False Then
                                    For i As Integer = 1 To oMatrix.VisualRowCount
                                        If oMatrix.IsRowSelected(i) = True Then
                                            iRowIndex = i
                                            Exit For
                                        End If
                                    Next
                                    oMatrix.FlushToDataSource()
                                    oMatrix.AddRow()
                                    oMatrix.Columns.Item("V_7").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_7").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("bentry").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("bentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("basenum").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("basenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("object").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("object").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("BaseLine").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("BaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("pbentry").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pbentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("pbasenum").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pbasenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("pBaseLine").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pBaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("V_0").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_0").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("V_10").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_10").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("date").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("date").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("pdate").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pdate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("ICode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("ICode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("IName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("IName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()

                                    oMatrix.Columns.Item("MCode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("MCode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()

                                    oMatrix.Columns.Item("MName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("MName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("WhsCod").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("WhsCod").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrix.Columns.Item("WhsName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("WhsName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()


                                    Dim sPft As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("Pft").Cells.Item(iRowIndex).Specific
                                    Dim sPft2 As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("Pft").Cells.Item(oMatrix.VisualRowCount).Specific
                                    If Not sPft.Selected Is Nothing Then
                                        sPft2.Select(sPft.Selected.Value.ToString(), SAPbouiCOM.BoSearchKey.psk_ByValue)
                                    End If
                                    Dim scof As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("cof").Cells.Item(iRowIndex).Specific
                                    Dim scof2 As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("cof").Cells.Item(oMatrix.VisualRowCount).Specific
                                    If Not scof.Selected Is Nothing Then
                                        scof2.Select(scof.Selected.Value.ToString(), SAPbouiCOM.BoSearchKey.psk_ByValue)
                                    End If

                                    Dim sverify As SAPbouiCOM.CheckBox = oMatrix.Columns.Item("verify").Cells.Item(iRowIndex).Specific
                                    Dim sverify2 As SAPbouiCOM.CheckBox = oMatrix.Columns.Item("verify").Cells.Item(oMatrix.VisualRowCount).Specific
                                    sverify2.Checked = sverify.Checked
                                    oMatrix.GetCellSpecific(0, oMatrix.VisualRowCount).Value = oMatrix.VisualRowCount

                                    oMatrix.Columns.Item("V_7").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_7").Cells.Item(iRowIndex).Specific.value.ToString().Trim()



                                    oMatrix.FlushToDataSource()
                                    frmStockPlanning.Items.Item("Stock").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    frmStockPlanning.Items.Item("Plan").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    frmStockPlanning.Update()
                                End If


                                'If frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                '    frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                'End If
                                'Case "btnDup"
                                '    If pVal.BeforeAction = False Then
                                '        For i As Integer = 1 To oMatrix1.VisualRowCount
                                '            If oMatrix.IsRowSelected(i) = True Then
                                '                iRowIndex = i
                                '                Exit For
                                '            End If
                                '        Next
                                '        Dim tqty As String = oMatrix.Columns.Item("tqty").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        Dim PQty As String = oMatrix.Columns.Item("PQty").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.FlushToDataSource()
                                '        oMatrix.AddRow()
                                '        oMatrix.Columns.Item("PQty").Cells.Item(oMatrix.VisualRowCount).Specific.value = (tqty - PQty).ToString()
                                '        oMatrix.Columns.Item("bentry").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("bentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("basenum").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("basenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("object").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("object").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("BaseLine").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("BaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("pbentry").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pbentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("pbasenum").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pbasenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("pBaseLine").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("pBaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("CCode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("CCode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("V_13").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_13").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("PICode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("PICode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("PIName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("PIName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("ICode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("ICode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("V_11").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_11").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("tqty").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("tqty").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("Mcode").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("Mcode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("V_9").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_9").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("V_8").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_8").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("cf").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("cf").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("ReqDate").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("ReqDate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("WhsCod").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("WhsCod").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("WhsName").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("WhsName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("V_5").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_5").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("FDate").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("FDate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("TDate").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("TDate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("V_2").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_2").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.Columns.Item("V_1").Cells.Item(oMatrix.VisualRowCount).Specific.value = oMatrix.Columns.Item("V_1").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                '        oMatrix.GetCellSpecific(0, oMatrix.VisualRowCount).Value = oMatrix.VisualRowCount
                                '        oMatrix.FlushToDataSource()


                                '    End If

                            Case "btnNext"
                                If pVal.BeforeAction = False Then
                                    Dim t_SCode As String = frmStockPlanning.Items.Item("t_SCode").Specific.value.ToString().Trim()
                                    Dim t_SName As String = frmStockPlanning.Items.Item("t_SName").Specific.value.ToString().Trim()








                                    If t_SCode = String.Empty Then
                                        oApplication.StatusBar.SetText("Scenario Code Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If
                                    If t_SName = String.Empty Then
                                        oApplication.StatusBar.SetText("Scenario Name Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If
                                    If oMatrix.VisualRowCount = 0 Then
                                        oApplication.StatusBar.SetText("Details Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If
                                    If oMatrix.VisualRowCount = 1 Then
                                        Dim sCustomerCode As String = oMatrix.Columns.Item("ICode").Cells.Item(1).Specific.value.Trim
                                        If sCustomerCode = String.Empty Then
                                            oApplication.StatusBar.SetText("Details Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                            Return
                                        End If

                                    End If

                                    Dim intverify As Integer = 0
                                    For i As Integer = 1 To oMatrix.VisualRowCount
                                        Dim sMCode As String = oMatrix.Columns.Item("V_0").Cells.Item(i).Specific.value.Trim
                                        Dim verify As SAPbouiCOM.CheckBox = oMatrix.Columns.Item("verify").Cells.Item(i).Specific
                                        If verify.Checked = True Then
                                            intverify = intverify + 1
                                        End If
                                        If sMCode = String.Empty Then
                                            oApplication.StatusBar.SetText("Customer Code Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                            Return
                                        End If
                                    Next

                                    If intverify = 0 Then
                                        oApplication.StatusBar.SetText("Atleast One Records Need to be Select to move Next Process", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If


                                    Dim AssingedQty As Integer = 0
                                    For i As Integer = 1 To oMatrix1.VisualRowCount
                                        Dim sAllocated As Double = Convert.ToDouble(oMatrix1.Columns.Item("Allocated").Cells.Item(i).Specific.value.Trim)
                                        AssingedQty = AssingedQty + 1
                                    Next
                                    If AssingedQty = 0 Then
                                        oApplication.StatusBar.SetText("Allocated Qty Should not be Zero", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If


                                    If frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                        frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                    End If
                                    frmStockPlanning.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    frmStockPlanning.Items.Item("2").Click(SAPbouiCOM.BoCellClickType.ct_Regular)



                                    oApplication.ActivateMenuItem("OREC")
                                    Dim frmRecommendation As SAPbouiCOM.Form = oApplication.Forms.ActiveForm


                                    Dim sQuery As String = String.Empty
                                    sQuery = " Select DocEntry  from [@AIS_OREC] Where   U_ScenarioCode='" & t_SCode & "'"
                                    Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                    If rsetEmpDets.RecordCount > 0 Then
                                        frmRecommendation.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                        frmRecommendation.Items.Item("t_SCode").Specific.value = t_SCode
                                        frmRecommendation.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                        frmRecommendation.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE

                                    Else

                                        sQuery = " Select Code  from [@AIS_OSCE] Where   U_ScenarioCode='" & t_SCode & "'"
                                        Dim rsetEmpDets_Code As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                        Dim sCode As String = String.Empty
                                        If rsetEmpDets_Code.RecordCount > 0 Then
                                            sCode = Trim(rsetEmpDets_Code.Fields.Item("Code").Value)
                                        End If
                                        frmRecommendation.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                                        frmRecommendation.Items.Item("t_SCode").Specific.value = sCode

                                        LoadDocumentDate(frmRecommendation.Items.Item("t_DocDate").Specific)
                                        Dim oCmbSerial As SAPbouiCOM.ComboBox = frmRecommendation.Items.Item("c_series").Specific
                                        oCmbSerial.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)

                                    End If
                                End If

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.ActionSuccess And frmStockPlanning.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If
                            Case "Plan"
                                If pVal.BeforeAction = False Then
                                    frmStockPlanning.PaneLevel = 1
                                    frmStockPlanning.Items.Item("Plan").AffectsFormMode = False
                                    frmStockPlanning.Settings.MatrixUID = "mat_StockP"
                                End If
                            Case "Stock"
                                If pVal.BeforeAction = False Then
                                    frmStockPlanning.PaneLevel = 2
                                    frmStockPlanning.Items.Item("Stock").AffectsFormMode = False
                                    frmStockPlanning.Settings.MatrixUID = "mat_Stocka"
                                    If oMatrix1.VisualRowCount = 0 Then
                                        LoadStockItemDetails(True)
                                    Else
                                        LoadStockItemDetails(False)
                                    End If



                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Private Sub InitForm()
        Try
            frmStockPlanning.Freeze(True)
            LoadComboBoxSeries(frmStockPlanning.Items.Item("c_series").Specific, UDOID)
            LoadDocumentDate(frmStockPlanning.Items.Item("t_DocDate").Specific)
            oMatrix.Clear()
            SetNewLine(oMatrix, oDBDSDetail)
            SetNewLine(oMatrix1, oDBDSDetail1)
            'For i As Integer = 1 To oMatrix.VisualRowCount
            '    Dim ICode As String = oMatrix.Columns.Item("ICode").Cells.Item(i).Specific.value


            '    setComboBoxValue(oMatrix.Columns.Item("cof").Cells.Item(i).Specific, "Select distinct U_FT ,U_FT    from OITM  wHERE ItemCode='" & ICode & "' and ISNULL(U_FT ,'')!='' UNION ALL SELECT '-1','None'")
            'Next
            setComboBoxValue(oMatrix.Columns.Item("cof").Cells.Item(1).Specific, "Select distinct U_FT ,U_FT    from OITM  wHERE   ISNULL(U_FT ,'')!='' UNION ALL SELECT '-1','None'")

            setComboBoxValue(oMatrix.Columns.Item("Pft").Cells.Item(1).Specific, "Select distinct U_FT ,U_FT    from OITM  wHERE ISNULL(U_FT ,'')!='' UNION ALL SELECT '-1','None'")

        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmStockPlanning.Freeze(False)
        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub DefineModeForFields()
        Try
            frmStockPlanning.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmStockPlanning.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmStockPlanning.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            '4
            frmStockPlanning.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmStockPlanning.Items.Item("t_DocDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub RightClickEvent(ByRef EventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case EventInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK
                    If frmStockPlanning.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And EventInfo.BeforeAction Then
                        Select Case EventInfo.ItemUID
                            Case "mat_StockP"
                                iRowIndex = EventInfo.Row
                                sColumnID = EventInfo.ColUID

                        End Select

                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Try
                        If BusinessObjectInfo.ActionSuccess Then


                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add ,Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        AutoSumEnable()

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
End Class
