﻿Imports SAPLib
Public Class LoginPage
    Dim frmLoginPage As SAPbouiCOM.Form
    Dim oDBDSMaster As SAPbouiCOM.DBDataSource
    Dim UDOID As String = "OSCE"
    Dim bSystemForms As Boolean = False

    Sub LoadXMLForm()
        Try
            LoadXML(frmLoginPage, LoginPageFormID, LoginPageXML)
            frmLoginPage = oApplication.Forms.Item(LoginPageFormID)
            oDBDSMaster = frmLoginPage.DataSources.DBDataSources.Item("@AIS_OSCE")

            Me.DefineModesForFields()

            Me.InitForm()

        Catch ex As Exception

        End Try
    End Sub

    Sub InitForm()
        Try
            frmLoginPage.Freeze(True)
            If frmLoginPage.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                frmLoginPage.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            End If
            If frmLoginPage.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then oDBDSMaster.SetValue("Code", 0, GetCodeGeneration("[@AIS_OSCE]"))

            frmLoginPage.Freeze(False)
            frmLoginPage.ActiveItem = "t_SCode"
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            frmLoginPage.Freeze(False)
        Finally
        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    Sub DefineModesForFields()
        Try
            'frmLoginPage.Items.Item("t_SCode").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            'frmLoginPage.Items.Item("t_SCode").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            'frmLoginPage.Items.Item("t_SCode").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'frmLoginPage.Items.Item("t_SCode").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
        Catch ex As Exception
            oApplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function ValidateAll() As Boolean
        Try
            If frmLoginPage.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then If (Not isDuplicate(frmLoginPage.Items.Item("t_SName").Specific, "[@AIS_OSCE]", "U_ScenarioCode", "Scenario Code")) Then Return False

            ValidateAll = True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try
    End Function

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)


        


        Try
            Select Case pVal.EventType
                


                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Select Case pVal.ItemUID
                        Case "t_name"
                            If pVal.BeforeAction = True Then

                            End If
                    End Select
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "btnNext"
                                If pVal.BeforeAction = False Then

                                    Dim t_SCode As String = frmLoginPage.Items.Item("t_SCode").Specific.value.ToString().Trim()
                                    Dim t_SName As String = frmLoginPage.Items.Item("t_SName").Specific.value.ToString().Trim()

                                    If t_SCode = String.Empty Then
                                        oApplication.StatusBar.SetText("Scenario Code Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If

                                    If t_SName = String.Empty Then
                                        oApplication.StatusBar.SetText("Scenario Name Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If
                                    Dim sQuery As String = String.Empty
                                    sQuery = " Select DocEntry  from [@AIS_OSCE] Where   U_ScenarioCode='" & t_SCode & "'"
                                    Dim rsetEmpDets2 As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                    If rsetEmpDets2.RecordCount > 0 Then
                                        oApplication.StatusBar.SetText("Scenario Code Should not be Duplicate", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If

                                    frmLoginPage.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    frmLoginPage.Items.Item("2").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    'frmLoginPage.Close()

                                    oApplication.ActivateMenuItem("OMAV")
                                    Dim frmMachineAvailability As SAPbouiCOM.Form = oApplication.Forms.ActiveForm
                                    bSystemForms = True
                                    sQuery = " Select DocEntry  from [@AIS_OMAV] Where   U_ScenarioCode='" & t_SCode & "'"
                                    Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                    If rsetEmpDets.RecordCount > 0 Then
                                        frmMachineAvailability.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                        frmMachineAvailability.Items.Item("t_SCode").Specific.value = t_SCode
                                        frmMachineAvailability.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                        frmMachineAvailability.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                    Else
                                        sQuery = " Select Code  from [@AIS_OSCE] Where   U_ScenarioCode='" & t_SCode & "'"
                                        Dim rsetEmpDets_Code As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                        Dim sCode As String = String.Empty
                                        If rsetEmpDets_Code.RecordCount > 0 Then
                                            sCode = Trim(rsetEmpDets_Code.Fields.Item("Code").Value)
                                        End If



                                        frmMachineAvailability.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE

                                        frmMachineAvailability.Items.Item("t_SCode").Specific.value = sCode
                                        LoadDocumentDate(frmMachineAvailability.Items.Item("t_DocDate").Specific)
                                        Dim oCmbSerial As SAPbouiCOM.ComboBox = frmMachineAvailability.Items.Item("c_series").Specific
                                        oCmbSerial.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)


                                    End If
                                    'frmMachineAvailability.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)



                                End If
                            Case "1"
                                If pVal.BeforeAction = True And (frmLoginPage.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmLoginPage.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                                    If Me.ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Exit Sub
                                        End
                                    End If
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.BeforeAction = False And pVal.ActionSuccess And frmLoginPage.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    Me.InitForm()
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1281"
                Case "1282"
                    Me.InitForm()
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Try
                        If frmLoginPage.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            'oDBDSMaster.SetValue("Code", 0, GetCodeGeneration("[@SMHR_OSFT]"))
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add ,Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

End Class

