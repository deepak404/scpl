﻿
Imports SAPLib
Imports System.Globalization

Public Class ProductionPlan

    Dim frmProductionPlan, frmSubPlanningDetailSubMatrix As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail, oDBDSSubPlanDetails, oDBDSMainPlanDetails As SAPbouiCOM.DBDataSource
    Dim oMatrix, oMatSubPlanDetails, oMatMainplanDetails As SAPbouiCOM.Matrix
    Dim boolFormLoaded As Boolean = False
    Dim sFormUID As String
    Dim SubRowID_PlanningDetails As Integer
    Dim SubRowID_DayID As String
    Dim UDOID As String = "OPPL"
    Dim SubItemName As String
    Dim SubItemCode As String
    Dim SubItemQty As Double
    Dim SubFromwarehouseCode As String
    Dim SubFromwarehouseName As String
    Dim cntlMat As String
    Dim row2 As Integer

    Sub LoadProductionPlan()

        Try


            boolFormLoaded = False
            LoadXML(frmProductionPlan, ProductionPlanFormID, ProductionPlanXML)
            frmProductionPlan = oApplication.Forms.Item(ProductionPlanFormID)
            oDBDSHeader = frmProductionPlan.DataSources.DBDataSources.Item("@AIS_OPPL")
            oDBDSDetail = frmProductionPlan.DataSources.DBDataSources.Item("@AIS_PPL1")
            oDBDSMainPlanDetails = frmProductionPlan.DataSources.DBDataSources.Item("@AIS_PPL2")
            oMatrix = frmProductionPlan.Items.Item("Matrix").Specific
            oMatMainplanDetails = frmProductionPlan.Items.Item("SubMatrix").Specific

            frmProductionPlan.Freeze(True)
            ' 

            Me.InitForm()
            Me.DefineModeForFields()


        Catch ex As Exception
            oApplication.StatusBar.SetText("Load Production Plan Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmProductionPlan.Freeze(False)
        End Try
    End Sub


    Sub InitForm()
        Try
            frmProductionPlan.Freeze(True)
            LoadComboBoxSeries(frmProductionPlan.Items.Item("c_series").Specific, UDOID)
            setComboBoxValue(frmProductionPlan.Items.Item("c_Type").Specific, "select * from  [@AIS_WrkTyp] Where Code Not in ('D','K')")
            setComboBoxValue(frmProductionPlan.Items.Item("c_WKOT").Specific, "select * from  [@AIS_WrkTyp]")
            LoadDocumentDate(frmProductionPlan.Items.Item("t_DocDate").Specific)

            oMatrix.Clear()

            SetNewLine(oMatrix, oDBDSDetail)

            setComboBoxValue(oMatrix.Columns.Item("NOE").Cells.Item(1).Specific, "select distinct isnull(U_NE,'') as NE ,isnull(U_NE,'') as NE   from  OITM  Where isnull(U_NE,'')!=''")
            setComboBoxValue(oMatrix.Columns.Item("Dimnsn").Cells.Item(1).Specific, " select distinct isnull(U_DM ,'') as DM ,isnull(U_DM,'') as DM   from  OITM  Where isnull(U_DM,'')!=''")
            setComboBoxValue(oMatrix.Columns.Item("Spool").Cells.Item(1).Specific, " select distinct isnull(U_ST ,'') as ST ,isnull(U_ST,'') as ST   from  OITM  Where isnull(U_ST,'')!=''")
            oMatrix.Columns.Item("Batch").Cells.Item(1).Specific.value = "Click Link"

        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmProductionPlan.Freeze(False)
        End Try
    End Sub


    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Sub DefineModeForFields()
        Try
            frmProductionPlan.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmProductionPlan.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            frmProductionPlan.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmProductionPlan.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)




            frmProductionPlan.Items.Item("c_Type").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmProductionPlan.Items.Item("c_WKOT").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmProductionPlan.Items.Item("t_RCode").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function Validation() As Boolean
        Try

            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validation Function Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Function
    Sub CreateFormForActivityList()
        Try

            Dim sQuery As String
            Dim CP As SAPbouiCOM.FormCreationParams = oApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)

            Dim oItem As SAPbouiCOM.Item
            Dim oButton As SAPbouiCOM.Button
            Dim oStaticText As SAPbouiCOM.StaticText
            Dim oEditText As SAPbouiCOM.EditText
            Dim oGrid As SAPbouiCOM.Grid

            CP.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Sizable
            CP.UniqueID = "WorkOrder"
            CP.FormType = "200"

            oForm = oApplication.Forms.AddEx(CP)
            ' Set form width and height 
            oForm.Height = 260
            oForm.Width = 500
            oForm.Title = "WorkOrder Details"


            '' Add a Grid item to the form 
            oItem = oForm.Items.Add("MyGrid", SAPbouiCOM.BoFormItemTypes.it_GRID)
            ' Set the grid dimentions and position 
            oItem.Left = 6
            oItem.Top = 0
            oItem.Width = 500
            oItem.Height = oForm.Height - 60

            ' Set the grid data 
            oGrid = oItem.Specific
            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
            oForm.DataSources.DataTables.Add("MyDataTable")

            Dim oCmbSerial As SAPbouiCOM.ComboBox = frmProductionPlan.Items.Item("c_Type").Specific
            Dim strSerialCode As String = oCmbSerial.Selected.Value

            Dim c_WKOT As SAPbouiCOM.ComboBox = frmProductionPlan.Items.Item("c_WKOT").Specific

            sQuery = "execute [@AIS_GetWorkOrderDetailsforProductionPlan] '" + strSerialCode + "','" + c_WKOT.Selected.Value + "'"
            oForm.DataSources.DataTables.Item(0).ExecuteQuery(sQuery)

            oGrid.DataTable = oForm.DataSources.DataTables.Item("MyDataTable")
            oGrid.AutoResizeColumns()

            oGrid.Columns.Item(0).Editable = False
            oGrid.Columns.Item(1).Editable = False
            oGrid.Columns.Item(2).Editable = False
            oGrid.Columns.Item(3).Editable = False
            oGrid.Columns.Item(4).Editable = False

            ' Add OK Button 
            oItem = oForm.Items.Add("ok", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 6
            oItem.Top = 200
            oItem.Width = 65
            oItem.Height = 20
            oButton = oItem.Specific
            oButton.Caption = "Submit"
            ' Add CANCEL Button 
            oItem = oForm.Items.Add("2", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 90
            oItem.Top = 200
            oItem.Width = 65
            oItem.Height = 20
            oButton = oItem.Specific



        Catch ex As Exception

            oApplication.StatusBar.SetText("Create Form For Quotation Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally

            oForm.Visible = True

        End Try
    End Sub
  
    'Function Validation_SubGrid() As Boolean
    '    Try
    '        Return True
    '    Catch ex As Exception
    '        oApplication.StatusBar.SetText("SubGrid Validation Failed" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
    '    Finally
    '    End Try
    'End Function
    Sub LoadSubMatrixDetails(ByVal Row As Integer, ColumID As String)

        Try

            If FormExist(BatchDetailsFormID) = False Then
                AddXML(BatchDetailsDetailXML)
                oApplication.Forms.Item(BatchDetailsFormID).Select()
            End If
            frmSubPlanningDetailSubMatrix = oApplication.Forms.Item(BatchDetailsFormID)
            oDBDSSubPlanDetails = frmSubPlanningDetailSubMatrix.DataSources.DBDataSources.Item("@AIS_PPL2")
            oMatSubPlanDetails = frmSubPlanningDetailSubMatrix.Items.Item("SubMatrix").Specific
            oMatrix.FlushToDataSource()
            'Load Sub Grid

            Dim x2 As Integer = oDBDSMainPlanDetails.Size

            frmSubPlanningDetailSubMatrix.Freeze(True)


            LoadSubGrid(oMatSubPlanDetails, oDBDSSubPlanDetails, oDBDSMainPlanDetails, Row, SubItemCode, SubItemName, SubItemQty, SubFromwarehouseCode, SubFromwarehousename)

            Dim boolDataAvl As Boolean = False
            oMatSubPlanDetails.FlushToDataSource()
            For i As Integer = 0 To oMatSubPlanDetails.VisualRowCount - 1
                Dim x As String = Trim(oDBDSSubPlanDetails.GetValue("U_Batch", i))
                If Trim(oDBDSSubPlanDetails.GetValue("U_Batch", i)).Equals("") = False Then
                    boolDataAvl = True
                    Exit For
                End If
            Next
            '  Load the details...
            ' Me.LoadBatchDetails()
            If boolDataAvl = False Then
                oMatSubPlanDetails.Clear()
                oDBDSSubPlanDetails.Clear()
                SetNewLineSubGrid(SubRowID_PlanningDetails, oMatSubPlanDetails, SubItemCode, SubItemName, SubItemQty, SubFromwarehouseCode, SubFromwarehouseName, oDBDSSubPlanDetails, Row)
                oMatSubPlanDetails.LoadFromDataSource()
            End If

            boolModelForm = True
            boolModelFormID = BatchDetailsFormID
        Catch ex As Exception
            oApplication.StatusBar.SetText("Failed to Load  Sub Matrix" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmSubPlanningDetailSubMatrix.Freeze(False)

        End Try
    End Sub



    Sub SetNewLineSubGrid(ByVal UniqID As Integer, ByVal oMatSubGrid As SAPbouiCOM.Matrix, ItemCode As String, ItemName As String, ItemQty As Double, FromWarehouseCode As String, FromWarehoueName As String, ByVal oDBDSSubGrid As SAPbouiCOM.DBDataSource, Optional ByVal RowID As Integer = 1, Optional ByVal ColumnUID As String = "", Optional ByVal DefaulFields As String(,) = Nothing)
        Try
            If ColumnUID.Equals("") = False Then
                If oMatSubGrid.Columns.Item(ColumnUID).Cells.Item(RowID).Specific.Value.Equals("") = False And RowID = oMatSubGrid.VisualRowCount Then
                    oMatSubGrid.FlushToDataSource()
                    oMatSubGrid.AddRow()
                    oDBDSSubGrid.InsertRecord(oDBDSSubGrid.Size)
                    oDBDSSubGrid.Offset = oMatSubGrid.VisualRowCount - 1
                    oDBDSSubGrid.SetValue("LineID", oDBDSSubGrid.Offset, oMatSubGrid.VisualRowCount)
                    oDBDSSubGrid.SetValue("U_UniqueID", oDBDSSubGrid.Offset, UniqID)
                    oDBDSSubGrid.SetValue("U_ICode", oDBDSSubGrid.Offset, ItemCode)
                    oDBDSSubGrid.SetValue("U_IName", oDBDSSubGrid.Offset, ItemName)
                    oDBDSSubGrid.SetValue("U_ItemQty", oDBDSSubGrid.Offset, ItemQty)
                    oDBDSSubGrid.SetValue("U_WhsCode", oDBDSSubGrid.Offset, FromWarehouseCode)
                    oDBDSSubGrid.SetValue("U_WhsName", oDBDSSubGrid.Offset, FromWarehoueName)
                    If Not DefaulFields Is Nothing Then
                        For f As Int16 = 0 To DefaulFields.GetLength(0) - 1
                            oDBDSSubGrid.SetValue(DefaulFields(f, 0), oDBDSSubGrid.Offset, DefaulFields(f, 1))
                        Next
                    End If
                    oMatSubGrid.SetLineData(oMatSubGrid.VisualRowCount)
                    oMatSubGrid.FlushToDataSource()
                    oMatSubGrid.LoadFromDataSource()
                End If
            Else
                oMatSubGrid.FlushToDataSource()
                oMatSubGrid.AddRow()
                oDBDSSubGrid.InsertRecord(oDBDSSubGrid.Size)
                oDBDSSubGrid.Offset = oMatSubGrid.VisualRowCount - 1
                oDBDSSubGrid.SetValue("LineID", oDBDSSubGrid.Offset, oMatSubGrid.VisualRowCount)
                oDBDSSubGrid.SetValue("U_UniqueID", oDBDSSubGrid.Offset, UniqID)
                oDBDSSubGrid.SetValue("U_ICode", oDBDSSubGrid.Offset, ItemCode)
                oDBDSSubGrid.SetValue("U_IName", oDBDSSubGrid.Offset, ItemName)
                oDBDSSubGrid.SetValue("U_ItemQty", oDBDSSubGrid.Offset, ItemQty)
                oDBDSSubGrid.SetValue("U_WhsCode", oDBDSSubGrid.Offset, FromWarehouseCode)
                oDBDSSubGrid.SetValue("U_WhsName", oDBDSSubGrid.Offset, FromWarehoueName)

                If Not DefaulFields Is Nothing Then
                    For f As Int16 = 0 To DefaulFields.GetLength(0) - 1
                        oDBDSSubGrid.SetValue(DefaulFields(f, 0), oDBDSSubGrid.Offset, DefaulFields(f, 1))
                    Next
                End If
                oMatSubGrid.SetLineData(oMatSubGrid.VisualRowCount)
                oMatSubGrid.FlushToDataSource()
                oMatSubGrid.LoadFromDataSource()
            End If
        Catch ex As Exception
            oApplication.StatusBar.SetText("SetNewLine Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Function LoadSubGrid(ByVal oMatSubGrid As SAPbouiCOM.Matrix, ByVal oDBDSSubGrid As SAPbouiCOM.DBDataSource, ByVal oDBDSMainSubGrid As SAPbouiCOM.DBDataSource, ByVal UniqID As Integer, ItemCode As String, ItemName As String, ItemQty As Double, FromWarehouse As String, FromwarehouseName As String, Optional ByVal DefaulFields As String(,) = Nothing) As Boolean
        Try
            oMatSubGrid.Clear()
            oDBDSSubGrid.Clear()

            Dim strGetColUID As String = "Select AliasID From CUFD Where  AliasID <> 'UniqID' AND TableID ='" & oDBDSSubGrid.TableName & "' "
            Dim rsetGetColUID As SAPbobsCOM.Recordset = Me.DoQuery(strGetColUID)
            Dim strColUID As String = ""

            Dim boolUniqID As Boolean = False
            For x As Integer = 0 To oDBDSMainSubGrid.Size - 1
                If Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", x)).Equals(UniqID.ToString) Then
                    boolUniqID = True
                    Exit For
                End If
            Next
            If boolUniqID = False Then
                ' Add the Rows in Sub Grid ...
                SetNewLineSubGrid(UniqID, oMatSubGrid, ItemCode, ItemName, ItemQty, FromWarehouse, FromWarehouseName, oDBDSSubGrid, , , DefaulFields)


            ElseIf oDBDSMainSubGrid.Size >= 1 And boolUniqID = True Then
                For i As Integer = 0 To oDBDSMainSubGrid.Size - 1
                    oDBDSMainSubGrid.Offset = i
                    If Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset)).Equals("") = False And oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset).Trim.Equals("") = False Then
                        If CInt(oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset)) = UniqID Then
                            oDBDSSubGrid.InsertRecord(oDBDSSubGrid.Size)
                            oDBDSSubGrid.Offset = oDBDSSubGrid.Size - 1
                            oDBDSSubGrid.SetValue("LineID", oDBDSSubGrid.Offset, oDBDSSubGrid.Offset + 1)
                            oDBDSSubGrid.SetValue("U_UniqueID", oDBDSSubGrid.Offset, UniqID)
                            If Not DefaulFields Is Nothing Then
                                For f As Int16 = 0 To DefaulFields.GetLength(0) - 1
                                    oDBDSSubGrid.SetValue(DefaulFields(f, 0), oDBDSSubGrid.Offset, DefaulFields(f, 1))
                                Next
                            End If
                            rsetGetColUID.MoveFirst()
                            For j As Integer = 0 To rsetGetColUID.RecordCount - 1
                                strColUID = "U_" & rsetGetColUID.Fields.Item(0).Value
                                oDBDSSubGrid.SetValue(strColUID, oDBDSSubGrid.Offset, oDBDSMainSubGrid.GetValue(strColUID, oDBDSMainSubGrid.Offset).Trim)
                                rsetGetColUID.MoveNext()
                            Next
                        End If



                    End If

                Next
                oMatSubGrid.LoadFromDataSource()
                oMatSubGrid.FlushToDataSource()
                ' Add the Rows in Sub Grid ...
                'Me.SetNewLineSubGrid(UniqID, oMatSubGrid, oDBDSSubGrid, , , DefaulFields)
            End If
            Return True
        Catch ex As Exception
            Msg("Global Fun. : Load Sub Grid " & ex.Message)
            Return False
        Finally
        End Try
    End Function

    Function DoQuery(ByVal strSql As String) As SAPbobsCOM.Recordset
        Try
            Dim rsetCode As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            rsetCode.DoQuery(strSql)
            Return rsetCode
        Catch ex As Exception
            oApplication.StatusBar.SetText("Execute Query Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return Nothing
        Finally
        End Try
    End Function

    Function SaveSubGrid(ByVal oMatMainGrid As SAPbouiCOM.Matrix, ByVal oDBDSMainSubGrid As SAPbouiCOM.DBDataSource, ByVal oMatSubGrid As SAPbouiCOM.Matrix, ByVal oDBDsSubGrid As SAPbouiCOM.DBDataSource, ByVal RowID As Integer, SubRowID_DayID As String) As Boolean
        Try
            Dim strGetColUID As String = "Select AliasID From CUFD Where  AliasID <> 'UniqID' AND TableID ='" & oDBDsSubGrid.TableName & "' "
            Dim rsetGetColUID As SAPbobsCOM.Recordset = Me.DoQuery(strGetColUID)
            Dim strColUID As String = ""

            Dim intInitSize As Integer = oDBDSMainSubGrid.Size
            Dim intCurrSize As Integer = oDBDSMainSubGrid.Size
            Dim oEmpty As Boolean = True


            For i As Integer = 0 To intInitSize - 1
                oDBDSMainSubGrid.Offset = i - (intInitSize - intCurrSize)
                Dim aa = oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset).Trim
                If oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset).Trim <> "" Then
                    If CInt(oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset)) = RowID Then
                        oDBDSMainSubGrid.RemoveRecord(oDBDSMainSubGrid.Offset)
                    End If
                ElseIf oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset).Trim.Equals("") Then
                    oDBDSMainSubGrid.RemoveRecord(oDBDSMainSubGrid.Offset)
                End If
                intCurrSize = oDBDSMainSubGrid.Size
            Next

            If oDBDSMainSubGrid.Size = 0 Then
                oDBDSMainSubGrid.InsertRecord(oDBDSMainSubGrid.Size)
                oDBDSMainSubGrid.Offset = 0
            End If

            If oDBDSMainSubGrid.Size = 1 And Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset)).Equals("") Then
                oEmpty = True
            Else
                oEmpty = False
            End If
            oMatSubGrid.FlushToDataSource()
            oMatSubGrid.LoadFromDataSource()
            For i As Integer = 0 To oMatSubGrid.VisualRowCount - 1
                oDBDsSubGrid.Offset = i
                If oEmpty = True Then
                    oDBDSMainSubGrid.Offset = oDBDSMainSubGrid.Size - 1
                    oDBDSMainSubGrid.SetValue("U_UniqueID", oDBDSMainSubGrid.Offset, RowID)
                    oDBDSMainSubGrid.SetValue("LineID", oDBDSMainSubGrid.Offset, oDBDSMainSubGrid.Size)
                    rsetGetColUID.MoveFirst()
                    For j As Integer = 0 To rsetGetColUID.RecordCount - 1
                        strColUID = "U_" & rsetGetColUID.Fields.Item(0).Value
                        oDBDSMainSubGrid.SetValue(strColUID, oDBDSMainSubGrid.Offset, oDBDsSubGrid.GetValue(strColUID, i).Trim)
                        rsetGetColUID.MoveNext()
                    Next
                    If i <> (oMatSubGrid.VisualRowCount - 1) Then
                        oDBDSMainSubGrid.InsertRecord(oDBDSMainSubGrid.Size)
                    End If
                ElseIf oEmpty = False Then

                    oDBDSMainSubGrid.InsertRecord(oDBDSMainSubGrid.Size)
                    oDBDSMainSubGrid.Offset = oDBDSMainSubGrid.Size - 1
                    oDBDSMainSubGrid.SetValue("U_UniqueID", oDBDSMainSubGrid.Offset, RowID)
                    oDBDSMainSubGrid.SetValue("LineID", oDBDSMainSubGrid.Offset, oDBDSMainSubGrid.Size)
                    rsetGetColUID.MoveFirst()
                    For j As Integer = 0 To rsetGetColUID.RecordCount - 1
                        strColUID = "U_" & rsetGetColUID.Fields.Item(0).Value
                        oDBDSMainSubGrid.SetValue(strColUID, oDBDSMainSubGrid.Offset, oDBDsSubGrid.GetValue(strColUID, i).Trim)
                        rsetGetColUID.MoveNext()
                    Next
                End If
            Next
            oMatMainGrid.LoadFromDataSource()

            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Save Sub Grid Method Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        Finally
        End Try
    End Function



    Function Validation_SubGrid() As Boolean
        Try
            'oMatSubPlanSetails.FlushToDataSource()
            'For i As Integer = 1 To oMatSubPlanSetails.VisualRowCount - 1
            '    Dim Type As String = oMatSubPlanSetails.Columns.Item("type").Cells.Item(i).Specific.Value
            '    If Type.Equals("LV") Then
            '        If oMatSubPlanSetails.Columns.Item("lvecod").Cells.Item(i).Specific.Value() = "" Then
            '            oGFun.Msg("Plan Details Line :" & i & " : Leave Code Should Not Left Empty !!!")
            '        End If
            '    End If
            'Next

            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText("SubGrid Validation Failed" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Function
    Sub DeleteRowSubGrid(ByVal oMatMainSubGrid As SAPbouiCOM.Matrix, ByVal oDBDSMainSubGrid As SAPbouiCOM.DBDataSource, ByVal SubRowID As Integer)
        Try
            oMatMainSubGrid.FlushToDataSource()
            For i As Integer = 0 To oDBDSMainSubGrid.Size - 1
                If i <= oDBDSMainSubGrid.Size - 1 Then
                    oDBDSMainSubGrid.Offset = i
                    oDBDSMainSubGrid.SetValue("LineID", oDBDSMainSubGrid.Offset, i + 1)

                    If Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", i)).Equals("") = False Then
                        Dim aa = Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", i))
                        If CDbl(oDBDSMainSubGrid.GetValue("U_UniqueID", i)) = SubRowID Then
                            If Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", i)).Equals(SubRowID.ToString().Trim) Then
                                oDBDSMainSubGrid.RemoveRecord(i)
                                i -= 1
                            Else
                                oDBDSMainSubGrid.SetValue("U_UniqueID", oDBDSMainSubGrid.Offset, CDbl(oDBDSMainSubGrid.GetValue("U_UniqueID", i)) - 1)
                            End If
                        End If
                    Else
                        oDBDSMainSubGrid.RemoveRecord(i)
                        i -= 1
                    End If
                End If
            Next
            oMatMainSubGrid.LoadFromDataSource()
        Catch ex As Exception
            oApplication.StatusBar.SetText("Delete Row SubGrid Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub ItemEvent_SubGrid(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "Add"
                                If pVal.BeforeAction Then
                                    If Me.Validation_SubGrid = False Then BubbleEvent = False
                                ElseIf pVal.BeforeAction = False Then
                                    If frmProductionPlan.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                        DeleteRowSubGrid(oMatMainplanDetails, oDBDSMainPlanDetails, SubRowID_PlanningDetails)
                                    End If
                                    If SaveSubGrid(oMatMainplanDetails, oDBDSMainPlanDetails, oMatSubPlanDetails, oDBDSSubPlanDetails, SubRowID_PlanningDetails, SubRowID_DayID) Then
                                        Dim strICod As String = ""
                                        Dim A As Integer
                                        Dim Qty As Integer
                                        For i As Integer = 1 To oMatMainplanDetails.RowCount
                                            Qty = Qty + oMatMainplanDetails.Columns.Item("Qty").Cells.Item(i).Specific.Value
                                        Next
                                        oMatrix.LoadFromDataSource()
                                        oMatrix.Columns.Item("Batch").Cells.Item(SubRowID_PlanningDetails).Specific.value = Qty
                                        A = oMatrix.Columns.Item("Batch").Cells.Item(SubRowID_PlanningDetails).Specific.value
                                        'For i As Integer = 1 To oMatMainplanDetails.RowCount
                                        '    Dim s As Integer = oMatMainplanDetails.Columns.Item("UID").Cells.Item(i).Specific.Value
                                        '    If SubRowID_PlanningDetails = oMatMainplanDetails.Columns.Item("UID").Cells.Item(i).Specific.Value Then
                                        '        If i <> oMatMainplanDetails.RowCount Then
                                        '        End If
                                        '    End If
                                        'Next
                                        'oDBDSDetail.SetValue("U_D1", SubRowID_DayID - 1, strFTim.TrimEnd(","))
                                        'oDBDSDetail.SetValue("U_D1", SubRowID_PlanningDetails - 1, strFTim.TrimEnd(","))
                                        ' oMatrix.LoadFromDataSource()
                                        frmSubPlanningDetailSubMatrix.Close()
                                        If frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                    End If
                                End If
                        End Select
                    Catch ex As Exception
                        Msg("Click Event Failed : " & ex.Message)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing And pVal.BeforeAction = False And frmSubPlanningDetailSubMatrix.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            Select Case pVal.ItemUID
                                Case "submatrix"
                                    Select Case pVal.ColUID

                                    End Select
                                    Select Case pVal.ColUID

                                    End Select
                            End Select
                        End If
                    Catch ex As Exception
                        Msg("Sub-grid Cost Center : Item event failed : " & ex.Message)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "SubMatrix"
                                Select Case pVal.ColUID

                                End Select
                        End Select
                    Catch ex As Exception
                        Msg("matrix link pressed Event Failed : " & ex.Message)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "SubMatrix"
                                Select Case pVal.ColUID
                                    Case "Batch"
                                        SetNewLineSubGrid(SubRowID_PlanningDetails, oMatSubPlanDetails, SubItemCode, SubItemName, SubItemQty, SubFromwarehouseCode, SubFromwarehouseName, oDBDSSubPlanDetails, pVal.Row, pVal.ColUID)
                                End Select
                        End Select
                    Catch ex As Exception
                        Msg("Lost Focus Event  : " & ex.Message)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("SubGrid Item Event Failed" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub
    'Sub LoadBatchDetails()
    '    Try
    '        Dim sQuery As String
    '        sQuery = " EXEC [dbo].[@AIS_GetBatchDetailsforProductionPlan]  '" + DocEntry + "'"
    '        sQuery = "  Select U_ICode,U_Batch,U_Qty from @AIS_PPL2 where U_FGcode ='" + oDBDSDetail.GetValue("DocNum", 0) + "' and U_BaseEntry='" + oDBDSHeader.GetValue("DocEntry", 0) + "'"
    '        sQuery = "  Select Itemcode,Quantity from IGE1 where U_FGcode ='" + oDBDSDetail.GetValue("DocNum", 0) + "' and U_BaseEntry='" + oDBDSHeader.GetValue("DocEntry", 0) + "'"
    '        Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
    '        rsetEmpDets.MoveFirst()
    '        oMatrix.Clear()
    '        oDBDSDetail.Clear()
    '    Catch ex As Exception
    '        oApplication.StatusBar.SetText("Load Batch Details Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
    '    End Try
    'End Sub
    Sub LoadProductionPlanOrderDetails(DocEntry As String)
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the ProductionPlan Order Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Dim sQuery As String
            sQuery = " EXEC [dbo].[@AIS_GetWorkOrderDetailsforProductionPlanItemDetails]  '" + DocEntry + "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
            rsetEmpDets.MoveFirst()
            oMatrix.Clear()
            oDBDSDetail.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                oDBDSDetail.Offset = i
                oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, i + 1)
                oDBDSDetail.SetValue("U_BaseEntry", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("DocEntry").Value)
                oDBDSDetail.SetValue("U_BaseNum", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("DocNum").Value)
                oDBDSDetail.SetValue("U_PlanDate", oDBDSDetail.Offset, CDate(rsetEmpDets.Fields.Item("PlanDate").Value).ToString("yyyyMMdd"))
                oDBDSDetail.SetValue("U_CustCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("CardCode").Value)
                oDBDSDetail.SetValue("U_CustName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("CardName").Value)
                oDBDSDetail.SetValue("U_ReqQty", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("Qty").Value)
                oDBDSDetail.SetValue("U_SFGCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_SFGCode").Value)
                oDBDSDetail.SetValue("U_SFGName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_SFGName").Value)
                oDBDSDetail.SetValue("U_BalReqQty", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("Qty").Value)
                oDBDSDetail.SetValue("U_TotalQty", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("Qty").Value)
                oDBDSDetail.SetValue("U_Batch", oDBDSDetail.Offset, "Click Link")

                rsetEmpDets.MoveNext()
            Next
            oMatrix.LoadFromDataSource()

            If frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Sales Quotation Data Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub



    Sub ItemEvent_SubForm(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try

            Select Case pVal.EventType


                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "ok"
                                If pVal.BeforeAction Then
                                    Dim oGrid As SAPbouiCOM.Grid
                                    oGrid = oForm.Items.Item("MyGrid").Specific

                                    Dim sDocEntry As String = String.Empty
                                    For i As Integer = 0 To oGrid.DataTable.Rows.Count - 1
                                        If oGrid.Rows.IsSelected(i) Then
                                            If sDocEntry = String.Empty Then
                                                sDocEntry = oGrid.DataTable.Columns.Item("DocEntry").Cells.Item(i).Value.ToString()
                                            Else
                                                sDocEntry = sDocEntry + "," + oGrid.DataTable.Columns.Item("DocEntry").Cells.Item(i).Value.ToString()
                                            End If



                                        End If
                                    Next
                                    LoadProductionPlanOrderDetails(sDocEntry)
                                    oForm.Close()

                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID
                                Case "t_RCode"
                                    oDBDSHeader.SetValue("U_RouteCode", 0, Trim(oDataTable.GetValue("U_RuleC", 0)))
                                    oDBDSHeader.SetValue("U_RouteName", 0, Trim(oDataTable.GetValue("U_RuleN", 0)))


                                Case "Matrix"
                                    Select Case pVal.ColUID
                                        Case "SFGCode"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_SFGCode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail.SetValue("U_SFGName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        Case "CCode"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_CustCode", pVal.Row - 1, Trim(oDataTable.GetValue("CardCode", 0)))
                                            oDBDSDetail.SetValue("U_CustName", pVal.Row - 1, Trim(oDataTable.GetValue("CardName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()


                                        Case "MCode"
                                            oMatrix.FlushToDataSource()
                                            Dim sResCode As String = Trim(oDataTable.GetValue("ResCode", 0))
                                            oDBDSDetail.SetValue("U_MCCode", pVal.Row - 1, sResCode)
                                            oDBDSDetail.SetValue("U_MCName", pVal.Row - 1, Trim(oDataTable.GetValue("ResName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        Case "FGcode"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_FGcode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail.SetValue("U_FGName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        Case "WhsCod"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_WhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail.SetValue("U_WhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))

                                            Dim sItemCode As String = String.Empty
                                            Dim oCmbSerial As SAPbouiCOM.ComboBox = frmProductionPlan.Items.Item("c_Type").Specific
                                            Dim strSerialCode As String = oCmbSerial.Selected.Value
                                            If strSerialCode = "G" Or strSerialCode = "S" Or strSerialCode = "T" Or strSerialCode = "F" Then
                                                sItemCode = oDBDSDetail.GetValue("U_SFGCode", pVal.Row - 1)
                                            ElseIf strSerialCode = "FW" Then
                                                sItemCode = oDBDSDetail.GetValue("U_FGcode", pVal.Row - 1)
                                            End If

                                            Dim sQuery As String = String.Empty
                                            sQuery = "   Select OnHand From OITW Where Whscode = '" & Trim(oDataTable.GetValue("WhsCode", 0)) & "'  and Itemcode='" + Trim(sItemCode) + "'"
                                            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            If rsetEmpDets.RecordCount > 0 Then
                                                oDBDSDetail.SetValue("U_Instock", pVal.Row - 1, Trim(rsetEmpDets.Fields.Item("OnHand").Value))
                                            End If
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                        Case "TWhsCod"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_ToWhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail.SetValue("U_ToWhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                    End Select

                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "Machine"
                                Select Case pVal.ColUID
                                    Case "MCode"
                                        If pVal.BeforeAction = False Then
                                            ChooseFromListFilteration(frmProductionPlan, "CFL_RES", "ResCode", "Select  ResCode  From ORSC Where ResType ='M'")
                                        End If
                                End Select
                            Case "matPreSale"
                                Select Case pVal.ColUID
                                    Case "ICode"
                                        If pVal.BeforeAction = False Then
                                            ChooseFromListFilteration(frmProductionPlan, "ITEM_CFL", "ItemCode", "Select ItemCode ,ItemName  from OITM Where ItemName In (Select U_GN From OITM) order by itemcode")
                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID

                            Case "Matrix"
                                Select Case pVal.ColUID



                                    Case "RQty", "AQty", "BRQty"
                                        If pVal.BeforeAction = False And pVal.ItemChanged Then
                                            oMatrix.FlushToDataSource()
                                            Dim ReqQty As Double = CDbl(oDBDSDetail.GetValue("U_ReqQty", pVal.Row - 1))
                                            Dim AQty As Double = CDbl(oDBDSDetail.GetValue("U_AllctdQty", pVal.Row - 1))
                                            Dim AddReqQty As Double = CDbl(oDBDSDetail.GetValue("U_AddReqQty", pVal.Row - 1))
                                            oDBDSDetail.SetValue("U_BalReqQty", pVal.Row - 1, (ReqQty - AQty).ToString())
                                            oDBDSDetail.SetValue("U_TotalQty", pVal.Row - 1, (AddReqQty + (ReqQty - AQty)).ToString())
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                        End If
                                    Case "ARQty"
                                        If pVal.BeforeAction = False And pVal.ItemChanged Then
                                            oMatrix.FlushToDataSource()
                                            Dim ReqQty As Double = CDbl(oDBDSDetail.GetValue("U_BalReqQty", pVal.Row - 1))
                                            Dim AQty As Double = CDbl(oDBDSDetail.GetValue("U_AddReqQty", pVal.Row - 1))
                                            oDBDSDetail.SetValue("U_TotalQty", pVal.Row - 1, (ReqQty + AQty).ToString())
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                        End If


                                    Case "PDF", "PTF"

                                        If pVal.BeforeAction = False And pVal.ItemChanged Then
                                            Dim sFDate As String = oMatrix.Columns.Item("PDF").Cells.Item(pVal.Row).Specific.value.ToString()
                                            If sFDate = String.Empty Then
                                                Return
                                            End If

                                            Dim sDocDate As String = frmProductionPlan.Items.Item("t_DocDate").Specific.value.ToString()
                                            Dim sQuery As String = String.Empty
                                            sQuery = "Exec [dbo].[@AIS_DaywisPlan_fromDate] '" + sFDate + "','" + sDocDate + "'"
                                            Dim rsetEmpDets_DateValidation As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            Dim strBoolen As Boolean = False

                                            If rsetEmpDets_DateValidation.RecordCount > 0 Then
                                                strBoolen = Convert.ToBoolean(Trim(rsetEmpDets_DateValidation.Fields.Item("U_CycleTime").Value))
                                            End If
                                            If strBoolen = False Then
                                                oApplication.StatusBar.SetText("Plan From Date Shoud not be Past Date , Line Id : " + pVal.Row.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                                Return
                                            End If
                                            oMatrix.FlushToDataSource()
                                            If Trim(oDBDSDetail.GetValue("U_PTimeFrm", pVal.Row - 1)) = String.Empty Then
                                                Return
                                            End If



                                            sQuery = "EXEC DBO.[@AIS_ProductionPlan_CalculateCycleTime] " + Trim(oDBDSDetail.GetValue("U_TotalQty", pVal.Row - 1)) + ",'" + Trim(oDBDSDetail.GetValue("U_PDateFrm", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_SFGCode", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_PTimeFrm", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_MCCode", pVal.Row - 1)) + "'  "

                                            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)


                                            If rsetEmpDets.RecordCount > 0 Then
                                                oDBDSDetail.SetValue("U_TotalMin", pVal.Row - 1, Trim(rsetEmpDets.Fields.Item("U_CycleTime").Value))
                                                If Trim(oDBDSDetail.GetValue("U_PTimeFrm", pVal.Row - 1)) <> String.Empty Then
                                                    Dim sTotal As String = Trim(rsetEmpDets.Fields.Item("ToTime").Value)
                                                    oDBDSDetail.SetValue("U_PTimeTo", pVal.Row - 1, sTotal)
                                                    oDBDSDetail.SetValue("U_PDateTo", pVal.Row - 1, CDate(rsetEmpDets.Fields.Item("ToDate").Value).ToString("yyyyMMdd"))
                                                    Dim PDateto As String = Trim(oDBDSDetail.GetValue("U_PDateTo", pVal.Row - 1))
                                                    Dim sYear As Integer = Convert.ToInt32(PDateto.Substring(0, 4))
                                                    If sYear < 2017 Then
                                                        oDBDSDetail.SetValue("U_PDateTo", pVal.Row - 1, String.Empty)
                                                    End If
                                                    
                                                    'If PDateto < CDate("30/12") Then
                                                    '    oDBDSDetail.SetValue("U_PDateTo", pVal.Row - 1, 0)
                                                    'Else
                                                    '    Dim sTotal As String = Trim(rsetEmpDets.Fields.Item("ToTime").Value)
                                                    '    oDBDSDetail.SetValue("U_PTimeTo", pVal.Row - 1, sTotal)
                                                    '    oDBDSDetail.SetValue("U_PDateTo", pVal.Row - 1, CDate(rsetEmpDets.Fields.Item("ToDate").Value).ToString("yyyyMMdd"))
                                                    'End If
                                                Else
                                                    oDBDSDetail.SetValue("U_PDateTo", pVal.Row - 1, String.Empty)
                                                    oDBDSDetail.SetValue("U_PTimeTo", pVal.Row - 1, String.Empty)
                                                End If
                                                    oMatrix.LoadFromDataSource()
                                                    oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                                End If

                                            'sQuery = "EXEC DBO.[@AIS_PPC_DailyPlan_AllocationTesting] '" + Trim(oDBDSDetail.GetValue("U_PDateFrm", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_PDateTo", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_TotalMin", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_PTimeFrm", pVal.Row - 1)) + "','" + Trim(oDBDSDetail.GetValue("U_PTimeTo", pVal.Row - 1)) + "'"

                                            'Dim rsetEmpDets2 As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            'If rsetEmpDets2.RecordCount > 0 Then
                                            '    If rsetEmpDets2.Fields.Item(0).Value <> "0" Then
                                            '        oApplication.StatusBar.SetText(rsetEmpDets2.Fields.Item(0).Value.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                            '        oDBDSDetail.SetValue("U_PDateTo", pVal.Row - 1, String.Empty)
                                            '        oDBDSDetail.SetValue("U_PTimeTo", pVal.Row - 1, String.Empty)
                                            '        oDBDSDetail.SetValue("U_PTimeFrm", pVal.Row - 1, String.Empty)
                                            '        oMatrix.LoadFromDataSource()
                                            '        oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                            '        Return
                                            '    End If
                                            'End If
                                        End If

                                End Select
                        End Select
                    Catch ex As Exception
                    End Try
                    Try
                        Select Case pVal.ItemUID
                            Case "Matrix"
                                Select Case pVal.ColUID
                                    Case "WKO"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix, oDBDSDetail, pVal.Row, pVal.ColUID)
                                        End If

                                End Select

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "btnCopy"
                                If pVal.BeforeAction = False Then
                                    CreateFormForActivityList()
                                End If
                            Case "bt_CSO"
                                Try
                                    If pVal.BeforeAction = False Then
                                        If Me.TransactionManagement() = False Then
                                            If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                            BubbleEvent = False
                                        Else
                                            If frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                                                frmProductionPlan.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                            End If
                                            If frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                                frmProductionPlan.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                            End If
                                        End If
                                    End If


                                Catch ex As Exception
                                    oApplication.StatusBar.SetText("Click(1) Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                Finally
                                End Try
                            Case "bt_CWO"
                                Try
                                    If pVal.BeforeAction = False Then
                                        If Me.TransactionManagementProductionOrder() = False Then
                                            If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                            BubbleEvent = False
                                        Else
                                            'If frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                                            '    frmProductionPlan.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                            'End If
                                            'If frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                            '    frmProductionPlan.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                            'End If
                                            'oApplication.ActivateMenuItem("1291")

                                        End If
                                    End If
                                Catch ex As Exception
                                    oApplication.StatusBar.SetText("Click(1) Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                Finally
                                End Try
                            Case "bt_TI"
                                Try
                                    If pVal.BeforeAction = False Then
                                        If Me.InventoryTransfer() = False Then
                                            If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                            BubbleEvent = False
                                        Else
                                            If frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                                                frmProductionPlan.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                            End If
                                            If frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                                frmProductionPlan.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                            End If
                                        End If
                                    End If
                                Catch ex As Exception
                                    oApplication.StatusBar.SetText("Click(1) Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                Finally
                                End Try
                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmProductionPlan.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmProductionPlan.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                            Case "Matrix"
                                Select Case pVal.ColUID
                                    Case "NOE", "Dimnsn", "Spool"
                                        If pVal.BeforeAction = False Then

                                            oMatrix.FlushToDataSource()
                                            Dim sQuery As String = String.Empty
                                            Dim U_NoOfEnd As String = (oDBDSDetail.GetValue("U_NoOfEnd", pVal.Row - 1))
                                            Dim U_Dimension As String = (oDBDSDetail.GetValue("U_Dimension", pVal.Row - 1))
                                            Dim U_SpoolType As String = (oDBDSDetail.GetValue("U_SpoolType", pVal.Row - 1))

                                            sQuery = "   Select ItemCode,ItemName From OITM Where U_NE = '" & Trim(U_NoOfEnd) & "'  and U_DM='" + Trim(U_Dimension) + "'" + "and U_ST='" + Trim(U_SpoolType) + "'"
                                            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            If rsetEmpDets.RecordCount > 0 Then

                                                oDBDSDetail.SetValue("U_FGcode", pVal.Row - 1, Trim(rsetEmpDets.Fields.Item("ItemCode").Value))
                                                oDBDSDetail.SetValue("U_FGName", pVal.Row - 1, Trim(rsetEmpDets.Fields.Item("ItemName").Value))
                                                oMatrix.LoadFromDataSource()
                                                oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            End If
                                            ChooseFromListFilteration(frmProductionPlan, "FG_CFL", "ItemCode", sQuery)

                                        End If
                                End Select


                            Case "c_Type"
                                If pVal.BeforeAction = False Then
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmProductionPlan.Items.Item("c_Type").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value

                                    oMatrix.Columns.Item("SFGCode").Visible = True
                                    oMatrix.Columns.Item("SFGName").Visible = True


                                    If strSerialCode = "G" Or strSerialCode = "S" Or strSerialCode = "T" Or strSerialCode = "F" Then
                                        oMatrix.Columns.Item("NOE").Visible = False
                                        oMatrix.Columns.Item("Dimnsn").Visible = False
                                        oMatrix.Columns.Item("Spool").Visible = False
                                        oMatrix.Columns.Item("FGcode").Visible = False
                                        oMatrix.Columns.Item("FGname").Visible = False
                                        frmProductionPlan.Items.Item("bt_CSO").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                                        frmProductionPlan.Items.Item("t_RCode").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                                        frmProductionPlan.Items.Item("t_RName").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                                    ElseIf strSerialCode = "FW" Then
                                        oMatrix.Columns.Item("SFGCode").Visible = False
                                        oMatrix.Columns.Item("SFGName").Visible = False
                                        frmProductionPlan.Items.Item("bt_CSO").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                                        frmProductionPlan.Items.Item("t_RCode").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                                        frmProductionPlan.Items.Item("t_RName").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                                        'oMatrix.Columns.Item("MCode").Visible = True
                                        'oMatrix.Columns.Item("MName").Visible = True
                                        oMatrix.Columns.Item("NOE").Visible = True
                                        oMatrix.Columns.Item("Dimnsn").Visible = True
                                        oMatrix.Columns.Item("Spool").Visible = True
                                        oMatrix.Columns.Item("FGcode").Visible = True
                                        oMatrix.Columns.Item("FGname").Visible = True
                                        oMatrix.Columns.Item("CCode").Editable = True
                                    ElseIf strSerialCode = "SO" Then
                                        frmProductionPlan.Items.Item("t_RCode").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                                        frmProductionPlan.Items.Item("t_RName").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                                    Else
                                        frmProductionPlan.Items.Item("bt_CSO").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                                        frmProductionPlan.Items.Item("t_RCode").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                                        frmProductionPlan.Items.Item("t_RName").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                                        'oMatrix.Columns.Item("MCode").Visible = True
                                        'oMatrix.Columns.Item("MName").Visible = True
                                        oMatrix.Columns.Item("NOE").Visible = True
                                        oMatrix.Columns.Item("Dimnsn").Visible = True
                                        oMatrix.Columns.Item("Spool").Visible = True
                                        oMatrix.Columns.Item("FGcode").Visible = True
                                        oMatrix.Columns.Item("FGname").Visible = True
                                    End If
                                End If


                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "Matrix"
                                Select Case pVal.ColUID
                                    Case "basenum"
                                        If pVal.BeforeAction = False Then
                                            oApplication.ActivateMenuItem("MNU_WORDER")
                                            Dim oForm2 As SAPbouiCOM.Form
                                            oForm2 = oApplication.Forms.ActiveForm
                                            oForm2.Select()
                                            oForm2.Items.Item("t_DocNum").Specific.Value = Trim(oMatrix.Columns.Item("basenum").Cells.Item(pVal.Row).Specific.value.ToString().Trim())
                                            oForm2.Items.Item("1").Click()

                                        End If
                                    Case "Batch"
                                        If pVal.BeforeAction = False Then

                                            Dim sItemCode As String = String.Empty
                                            Dim oCmbSerial As SAPbouiCOM.ComboBox = frmProductionPlan.Items.Item("c_Type").Specific
                                            Dim strSerialCode As String = oCmbSerial.Selected.Value
                                            If strSerialCode = "G" Or strSerialCode = "S" Or strSerialCode = "T" Or strSerialCode = "F" Then
                                                SubItemCode = oMatrix.Columns.Item("SFGCode").Cells.Item(pVal.Row).Specific.value.Trim
                                                SubItemName = oMatrix.Columns.Item("SFGName").Cells.Item(pVal.Row).Specific.value.Trim
                                            ElseIf strSerialCode = "FW" Then
                                                SubItemCode = oMatrix.Columns.Item("FGcode").Cells.Item(pVal.Row).Specific.value.Trim
                                                SubItemName = oMatrix.Columns.Item("FGname").Cells.Item(pVal.Row).Specific.value.Trim
                                            End If
                                            SubRowID_PlanningDetails = pVal.Row
                                            If oMatrix.Columns.Item("AQty").Cells.Item(pVal.Row).Specific.value.Trim <> String.Empty Then
                                                SubItemQty = Convert.ToDouble(oMatrix.Columns.Item("AQty").Cells.Item(pVal.Row).Specific.value.Trim)
                                            Else
                                                SubItemQty = 0
                                            End If

                                            SubFromwarehouseCode = oMatrix.Columns.Item("WhsCod").Cells.Item(pVal.Row).Specific.value.Trim
                                            SubFromwarehouseName = oMatrix.Columns.Item("WhsName").Cells.Item(pVal.Row).Specific.value.Trim

                                            Me.LoadSubMatrixDetails(SubRowID_PlanningDetails, pVal.ColUID)
                                        End If
                                End Select
                            Case "1"
                                If pVal.ActionSuccess And frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
                Case (SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
                    Try
                        Select Case pVal.ItemUID

                            Case "Matrix"
                                Select Case pVal.ColUID
                                    Case "Batch"

                                End Select
                        End Select

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "Selects"
                                If pVal.BeforeAction = False Then



                                End If
                            Case "1"
                                If pVal.ActionSuccess And frmProductionPlan.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()
                Case "1293"
                    DeleteRow(oMatrix, oDBDSDetail)





            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function TransactionManagement() As Boolean
        Try
            Dim boolGoToStockPosting As Boolean = False
            TransactionManagement = True
            oApplication.SetStatusBarMessage("Please Wait,System is Checking Validation and Posting Stock", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
            Dim boolJournalTrue As Boolean = False
            If Me.OpenSalesOrders() = False Then
                Return False
            End If
            If Me.WorkOrderForUpdate() = False Then
                Return False
            End If

            TransactionManagement = True
        Catch ex As Exception
            TransactionManagement = False
            StatusBarErrorMsg("Transaction Management Failed : " & ex.Message)
            TransactionManagement = False
        Finally
        End Try
    End Function
    Function OpenSalesOrders() As Boolean
        Try
            Dim ErrCode As Long
            Dim ErrMsg As String = ""

            'Dim strQuery As String
            'strQuery = " Select DocEntry from ORDR Where  U_Series='" + oDBDSHeader.GetValue("Series", 0) + "' AND U_BaseNum ='" + oDBDSHeader.GetValue("DocNum", 0) + "' AND U_BaseObject = 'OPPL'"
            'Dim Ors_RecordsCount As SAPbobsCOM.Recordset
            'Ors_RecordsCount = DoQuery(strQuery)
            'If Ors_RecordsCount.RecordCount > 0 Then
            '    oApplication.SetStatusBarMessage("Already Sales Order Posted for this Document", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
            '    Return False
            'End If


            Dim sDataisOk As Boolean = False

            Dim sLoop_BaseEntry As New ArrayList()
            For ik As Integer = 1 To oMatrix.VisualRowCount
                Dim sCardCode As String = oMatrix.Columns.Item("bentry").Cells.Item(ik).Specific.value.ToString().Trim()
                If Not (sLoop_BaseEntry.Contains(sCardCode)) Then
                    sLoop_BaseEntry.Add(sCardCode)
                End If
            Next

            For i As Integer = 0 To sLoop_BaseEntry.Count - 1
                Dim oSalesOrder As SAPbobsCOM.Documents = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders)
                Dim sBoolean As Boolean = False
                For ik As Integer = 1 To oMatrix.VisualRowCount
                    Dim sInnerCardCodea As String = oMatrix.Columns.Item("bentry").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim ReqDate As String = oMatrix.Columns.Item("PlnDat").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim Mcode As String = oMatrix.Columns.Item("MCode").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim CCode As String = oMatrix.Columns.Item("CCode").Cells.Item(ik).Specific.value.ToString().Trim()
                    If sInnerCardCodea = sLoop_BaseEntry.Item(i).ToString() Then
                        sDataisOk = True
                        If sBoolean = False Then
                            oSalesOrder.CardCode = CCode
                            oSalesOrder.DocDate = Date.Now
                            oSalesOrder.TaxDate = Format_StringToDate(ReqDate)
                            oSalesOrder.DocDueDate = Format_StringToDate(ReqDate)
                            oSalesOrder.Reference2 = oDBDSHeader.GetValue("DocNum", 0)
                            oSalesOrder.Comments = " Day Wise Plan." & oDBDSHeader.GetValue("DocNum", 0)
                            oSalesOrder.UserFields.Fields.Item("U_BaseNum").Value = oDBDSHeader.GetValue("DocNum", 0)
                            oSalesOrder.UserFields.Fields.Item("U_BaseObject").Value = "OPPL"
                            oSalesOrder.UserFields.Fields.Item("U_Series").Value = oDBDSHeader.GetValue("Series", 0)
                            oSalesOrder.UserFields.Fields.Item("U_WDDocEntry").Value = sInnerCardCodea

                            sBoolean = True
                        End If
                        Dim sItemCode As String = oMatrix.Columns.Item("FGcode").Cells.Item(ik).Specific.value.Trim
                        Dim PQty As String = oMatrix.Columns.Item("TotalQty").Cells.Item(ik).Specific.value.Trim
                        Dim WhsCod As String = oMatrix.Columns.Item("WhsCod").Cells.Item(ik).Specific.value.Trim
                        oSalesOrder.Lines.ItemCode = sItemCode
                        oSalesOrder.Lines.Quantity = Trim(PQty)
                        oSalesOrder.Lines.Price = Trim(1)
                        oSalesOrder.Lines.WarehouseCode = Trim(WhsCod)
                        If WhsCod = String.Empty Then
                            oApplication.SetStatusBarMessage("Warehouse Should not be Empty", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                            Return False
                        End If


                        oSalesOrder.Lines.Add()
                    End If
                Next
                If sDataisOk = True Then
                    If oSalesOrder.Add() = 0 Then
                        oApplication.StatusBar.SetText("Delivery Document  posted successfully ,Customer Code" + sLoop_BaseEntry.Item(i).ToString(), SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                    Else
                        oApplication.StatusBar.SetText("Unable To Sales Order Document" & oCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    End If
                Else
                    oApplication.StatusBar.SetText("No Data to Create Sales Order", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                End If
                ' 1. Post the GRN documents...

            Next



            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText(" Delivery Document   Posting Method Faild", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        Finally
        End Try
    End Function
    Function TransactionManagementProductionOrder() As Boolean
        Try
            Dim boolGoToStockPosting As Boolean = False
            TransactionManagementProductionOrder = True
            oApplication.SetStatusBarMessage("Please Wait,System is Checking Validation and Posting Stock", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
            Dim boolJournalTrue As Boolean = False
            If Me.WorkOrder() = False Then
                Return False
            End If
            TransactionManagementProductionOrder = True
        Catch ex As Exception
            TransactionManagementProductionOrder = False
            StatusBarErrorMsg("Transaction Management Failed : " & ex.Message)
            TransactionManagementProductionOrder = False
        Finally
        End Try
    End Function
    Function WorkOrder() As Boolean
        Try
            Dim strQuery As String
            strQuery = " Select DocEntry from [@AS_OWORD] Where Canceled ='N' and  U_Series='" + oDBDSHeader.GetValue("Series", 0) + "' AND U_BaseNum ='" + oDBDSHeader.GetValue("DocNum", 0) + "' AND U_BaseObject = 'OPPL'"
            Dim Ors_RecordsCount As SAPbobsCOM.Recordset
            Ors_RecordsCount = DoQuery(strQuery)
            If Ors_RecordsCount.RecordCount > 0 Then
                oApplication.SetStatusBarMessage("Already Work Order Posted for this Document", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                Return False
            End If
            Dim oCmbSerial As SAPbouiCOM.ComboBox = frmProductionPlan.Items.Item("c_Type").Specific
            Dim strSerialCode As String = oCmbSerial.Selected.Value
            Dim oc_Type As SAPbouiCOM.ComboBox = frmProductionPlan.Items.Item("c_Type").Specific
            Dim Type As String = oc_Type.Selected.Value
            If strSerialCode = "G" Or strSerialCode = "S" Or strSerialCode = "T" Or strSerialCode = "F" Then

                Dim sLoop_SFGCode As New ArrayList()
                Dim sLoop_PDF As New ArrayList()
                Dim sLoop_PDT As New ArrayList()
                Dim sLoop_PTF As New ArrayList()
                Dim sLoop_PTT As New ArrayList()
                Dim sLoop_Machine As New ArrayList()

                For ik As Integer = 1 To oMatrix.VisualRowCount
                    Dim SFGCode As String = oMatrix.Columns.Item("SFGCode").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim PDF As String = oMatrix.Columns.Item("PDF").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim PDT As String = oMatrix.Columns.Item("PDT").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim PTF As String = oMatrix.Columns.Item("PTF").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim PTT As String = oMatrix.Columns.Item("PTT").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim MCode As String = oMatrix.Columns.Item("MCode").Cells.Item(ik).Specific.value.ToString().Trim()

                    If Not (sLoop_Machine.Contains(MCode)) Or Not (sLoop_SFGCode.Contains(SFGCode)) Or Not (sLoop_PDF.Contains(PDF)) Or Not (sLoop_PDT.Contains(PDT)) Or Not (sLoop_PTF.Contains(PTF) Or Not (sLoop_PTT.Contains(PTT))) Then
                        sLoop_SFGCode.Add(SFGCode)
                        sLoop_PDF.Add(PDF)
                        sLoop_PDT.Add(PDT)
                        sLoop_PTF.Add(PTF)
                        sLoop_PTT.Add(PTT)
                        sLoop_Machine.Add(MCode)

                    End If
                Next


                For i As Integer = 0 To sLoop_SFGCode.Count - 1
                    Dim oGeneralService As SAPbobsCOM.GeneralService
                    Dim oGeneralData As SAPbobsCOM.GeneralData
                    Dim oChild As SAPbobsCOM.GeneralData
                    Dim oChildren As SAPbobsCOM.GeneralDataCollection
                    Dim sCmp As SAPbobsCOM.CompanyService = oCompany.GetCompanyService
                    oGeneralService = sCmp.GetGeneralService("AS_OWORD")
                    oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                    Dim sEntryPosted As Boolean = False
                    For ik As Integer = 1 To oMatrix.VisualRowCount
                        Dim sDocEntry As String = oMatrix.Columns.Item("bentry").Cells.Item(ik).Specific.value.ToString().Trim()
                        Dim TotalQty As Double = Convert.ToDouble(oMatrix.Columns.Item("TotalQty").Cells.Item(ik).Specific.value.ToString().Trim())
                        Dim StartDate As String = (oMatrix.Columns.Item("PDF").Cells.Item(ik).Specific.value.ToString().Trim())
                        Dim EndDate As String = (oMatrix.Columns.Item("PDT").Cells.Item(ik).Specific.value.ToString().Trim())
                        Dim MCode As String = (oMatrix.Columns.Item("MCode").Cells.Item(ik).Specific.value.ToString().Trim())
                        Dim MName As String = (oMatrix.Columns.Item("MName").Cells.Item(ik).Specific.value.ToString().Trim())
                        Dim LineID As String = (oMatrix.Columns.Item("LID").Cells.Item(ik).Specific.value.ToString().Trim())
                        Dim sItemCode_2 As String = oMatrix.Columns.Item("SFGCode").Cells.Item(ik).Specific.value.Trim
                        Dim iPDF As String = oMatrix.Columns.Item("PDF").Cells.Item(ik).Specific.value.ToString().Trim()
                        Dim iPDT As String = oMatrix.Columns.Item("PDT").Cells.Item(ik).Specific.value.ToString().Trim()
                        Dim iPTF As String = oMatrix.Columns.Item("PTF").Cells.Item(ik).Specific.value.ToString().Trim()
                        Dim iPTT As String = oMatrix.Columns.Item("PTT").Cells.Item(ik).Specific.value.ToString().Trim()


                        If TotalQty > 0 And MCode = sLoop_Machine.Item(i).ToString() And sItemCode_2 = sLoop_SFGCode.Item(i).ToString() And iPDF = sLoop_PDF.Item(i).ToString() And iPDT = sLoop_PDT.Item(i).ToString() And iPTT = sLoop_PTT.Item(i).ToString() And iPTF = sLoop_PTF.Item(i).ToString() And iPTT = sLoop_PTT.Item(i).ToString() And sEntryPosted = False Then
                            oGeneralData.SetProperty("U_Type", Type)
                            oGeneralData.SetProperty("U_DocDate", DateTime.Now)
                            oGeneralData.SetProperty("U_Status", "O")

                            Dim sLineQty As Double = 0

                            For iQty As Integer = 1 To oMatrix.VisualRowCount
                                Dim TotalQty_3 As Double = Convert.ToDouble(oMatrix.Columns.Item("TotalQty").Cells.Item(iQty).Specific.value.ToString().Trim())
                                Dim StartDate_3 As String = (oMatrix.Columns.Item("PDF").Cells.Item(iQty).Specific.value.ToString().Trim())
                                Dim EndDate_3 As String = (oMatrix.Columns.Item("PDT").Cells.Item(iQty).Specific.value.ToString().Trim())
                                Dim MCode_3 As String = (oMatrix.Columns.Item("MCode").Cells.Item(iQty).Specific.value.ToString().Trim())
                                Dim sItemCode_3 As String = oMatrix.Columns.Item("SFGCode").Cells.Item(iQty).Specific.value.Trim

                                Dim iPDF_3 As String = oMatrix.Columns.Item("PDF").Cells.Item(iQty).Specific.value.ToString().Trim()
                                Dim iPDT_3 As String = oMatrix.Columns.Item("PDT").Cells.Item(iQty).Specific.value.ToString().Trim()

                                Dim iPTF_3 As String = oMatrix.Columns.Item("PTF").Cells.Item(ik).Specific.value.ToString().Trim()
                                Dim iPTT_3 As String = oMatrix.Columns.Item("PTT").Cells.Item(ik).Specific.value.ToString().Trim()

                                If TotalQty_3 > 0 And MCode_3 = sLoop_Machine.Item(i).ToString() And sItemCode_3 = sLoop_SFGCode.Item(i).ToString() And iPDF_3 = sLoop_PDF.Item(i).ToString() And iPDT_3 = sLoop_PDT.Item(i).ToString() And iPTT_3 = sLoop_PTT.Item(i).ToString() And iPTF_3 = sLoop_PTF.Item(i).ToString() And sEntryPosted = False Then
                                    sLineQty = sLineQty + Convert.ToDouble(oMatrix.Columns.Item("TotalQty").Cells.Item(iQty).Specific.value.ToString().Trim())
                                End If

                            Next
                            oGeneralData.SetProperty("U_SalesOrderQty", sLineQty)
                            oGeneralData.SetProperty("U_ProducedQty", sLineQty)

                            Dim strQuerys = "Select ItemName  from  OITM Where ItemCode ='" + sItemCode_2 + "'"
                            Dim Ors_Root_ItemName As SAPbobsCOM.Recordset
                            Ors_Root_ItemName = DoQuery(strQuerys)
                            Dim sItemnName As String = String.Empty
                            If Ors_Root_ItemName.RecordCount > 0 Then
                                sItemnName = Ors_Root_ItemName.Fields.Item("ItemName").Value
                            End If
                            oGeneralData.SetProperty("U_FGCode", sItemCode_2)
                            oGeneralData.SetProperty("U_FgName", sItemnName)
                            oGeneralData.SetProperty("U_Type", strSerialCode.ToString().Trim())

                            oGeneralData.SetProperty("U_MachineCode", MCode)
                            oGeneralData.SetProperty("U_MachineName", MName)





                            If StartDate = String.Empty Then
                                oApplication.SetStatusBarMessage("Start Date Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                                Return False
                            End If
                            If EndDate = String.Empty Then
                                oApplication.SetStatusBarMessage("End  Date Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                                Return False
                            End If

                            Dim sStartDate2 As DateTime = DateTime.ParseExact(StartDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                            Dim sEndDate2 As DateTime = DateTime.ParseExact(EndDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)

                            oGeneralData.SetProperty("U_StartDate", sStartDate2)
                            oGeneralData.SetProperty("U_EndDate", sEndDate2)
                            oGeneralData.SetProperty("U_BaseLine", LineID)
                            oGeneralData.SetProperty("U_BaseNum", oDBDSHeader.GetValue("DocNum", 0))
                            oGeneralData.SetProperty("U_BaseEntry", oDBDSHeader.GetValue("DocEntry", 0))
                            oGeneralData.SetProperty("U_BaseObject", "OPPL")
                            oGeneralData.SetProperty("U_Series", oDBDSHeader.GetValue("Series", 0))



                            Dim RouteCode As String = String.Empty
                            Dim RouteName As String = String.Empty

                            Dim bLineDetails As Boolean = False

                            oChildren = oGeneralData.Child("AS_WORD1")
                            Dim sOperationCode As String = String.Empty
                            strQuery = "Select U_OperCode,U_OperName,U_MCCode,U_MCName,U_PCyc,U_Process, ''U_RuleC  ,'' as U_RuleN  from [@AIS_OIOD]  T0 Inner join  [@AIS_IOD1]   T1 On T0.DocEntry =T1.DocEntry Where Isnull(U_OperCode,'')!='' AND  T0.U_ItemCode ='" + sItemCode_2 + "' and T1.U_MCCode ='" + MCode + "'"
                            Dim Ors_Root As SAPbobsCOM.Recordset
                            Ors_Root = DoQuery(strQuery)
                            If Ors_Root.RecordCount > 0 Then
                                bLineDetails = True
                                oChild = oChildren.Add
                                For k = 0 To Ors_Root.RecordCount - 1
                                    If k = 0 Then
                                        sOperationCode = Ors_Root.Fields.Item("U_OperCode").Value.ToString()
                                    End If
                                    oChildren.Item(k).SetProperty("U_OpCode", Ors_Root.Fields.Item("U_OperCode").Value)
                                    oChildren.Item(k).SetProperty("U_OpName", Ors_Root.Fields.Item("U_OperName").Value)
                                    oChildren.Item(k).SetProperty("U_PCyc", Ors_Root.Fields.Item("U_PCyc").Value)
                                    Dim sQty As Double = Ors_Root.Fields.Item("U_PCyc").Value * sLineQty
                                    oChildren.Item(k).SetProperty("U_PCycle", sQty)

                                    oChildren.Item(k).SetProperty("U_Process", Ors_Root.Fields.Item("U_Process").Value)
                                    RouteCode = Ors_Root.Fields.Item("U_RuleC").Value
                                    RouteName = Ors_Root.Fields.Item("U_RuleN").Value

                                    oChild = oChildren.Add
                                    Ors_Root.MoveNext()

                                Next
                            End If
                            If bLineDetails = False Then
                                oChild = oChildren.Add
                            End If

                            oGeneralData.SetProperty("U_RuleC", RouteCode)
                            oGeneralData.SetProperty("U_RuleN", RouteName)


                            Dim sQuery2 = "EXEC DBO.[@AIS_PPC_GetMachineCycleTime] '" + sOperationCode + "','" + sLineQty.ToString() + "'"
                            Dim rsetEmpDets_GetRequiredHours As SAPbobsCOM.Recordset = DoQuery(sQuery2)
                            If rsetEmpDets_GetRequiredHours.RecordCount > 0 Then
                                Dim sQ As String = Trim(rsetEmpDets_GetRequiredHours.Fields.Item("ReqHrs").Value)
                                oGeneralData.SetProperty("U_RequiredHrs", sQ)
                            Else
                                oGeneralData.SetProperty("U_RequiredHrs", "0")
                            End If




                            oChildren = oGeneralData.Child("AS_WORD2")
                            oChild = oChildren.Add
                            'strQuery = "Select Top 1 CardCode,CardName, T0.DocEntry,DocNum ,T1.ItemCode,T1.Quantity,T1.WhsCode From ORDR T0 Inner Join RDR1 T1 On T0.DocEntry =T1.DocEntry Where   T0.DocEntry='" + sDocEntry + "'"
                            'Dim Ors_SalesOrderDetails As SAPbobsCOM.Recordset
                            'Ors_SalesOrderDetails = DoQuery(strQuery)
                            'If Ors_SalesOrderDetails.RecordCount > 0 Then
                            '    bLineDetails = True
                            '    oChild = oChildren.Add
                            '    For k = 0 To Ors_SalesOrderDetails.RecordCount - 1



                            '        oChildren.Item(k).SetProperty("U_BaseEntry", Ors_SalesOrderDetails.Fields.Item("DocEntry").Value.ToString())
                            '        oChildren.Item(k).SetProperty("U_BaseNum", Ors_SalesOrderDetails.Fields.Item("DocNum").Value.ToString())
                            '        oChildren.Item(k).SetProperty("U_CardCode", Ors_SalesOrderDetails.Fields.Item("CardCode").Value)
                            '        oChildren.Item(k).SetProperty("U_CardName", Ors_SalesOrderDetails.Fields.Item("CardName").Value)
                            '        oChildren.Item(k).SetProperty("U_TotalQty", Ors_SalesOrderDetails.Fields.Item("Quantity").Value.ToString())
                            '        oChildren.Item(k).SetProperty("U_PendingQty", Ors_SalesOrderDetails.Fields.Item("Quantity").Value.ToString())

                            '        oChild = oChildren.Add
                            '        Ors_SalesOrderDetails.MoveNext()
                            '    Next
                            'End If

                            oChildren = oGeneralData.Child("AS_WORD3")
                            oChild = oChildren.Add


                            'AS_WORD3

                            Try
                                oGeneralService.Add(oGeneralData)

                                Dim WorkOrderNo As String
                                Dim WorkOrderDocEntry As String
                                Dim sQuery As String = String.Empty
                                sQuery = "  Select DocNum,DocEntry from [@AS_OWORD] Where Canceled ='N' AND  U_BaseNum='" + oDBDSHeader.GetValue("DocNum", 0) + "' and U_BaseEntry='" + oDBDSHeader.GetValue("DocEntry", 0) + "'"
                                Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                If rsetEmpDets.RecordCount > 0 Then
                                    rsetEmpDets.MoveFirst()
                                    WorkOrderNo = rsetEmpDets.Fields.Item("DocNum").Value
                                    WorkOrderDocEntry = rsetEmpDets.Fields.Item("DocEntry").Value
                                End If
                                oMatrix.Columns.Item("WDocNum").Cells.Item(ik).Specific.value = WorkOrderNo
                                oMatrix.Columns.Item("WDocEntry").Cells.Item(ik).Specific.value = WorkOrderDocEntry

                                oApplication.SetStatusBarMessage("Data Posted Successfully", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                            Catch ex As Exception
                                oApplication.SetStatusBarMessage("Posting Error", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                            End Try


                            sEntryPosted = True

                        End If

                    Next

                Next

            ElseIf strSerialCode = "SO" Then


                For ik As Integer = 1 To oMatrix.VisualRowCount
                    Dim sDocEntry As String = oMatrix.Columns.Item("bentry").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim TotalQty As Double = Convert.ToDouble(oMatrix.Columns.Item("TotalQty").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim StartDate As String = (oMatrix.Columns.Item("PDF").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim EndDate As String = (oMatrix.Columns.Item("PDT").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim sDocNum As String = String.Empty
                    Dim sItemCode As String = String.Empty
                    Dim sItemName As String = String.Empty
                    Dim sWhsCode As String = String.Empty
                    Dim LineID As String = (oMatrix.Columns.Item("LID").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim MCode As String = (oMatrix.Columns.Item("MCode").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim MName As String = (oMatrix.Columns.Item("MName").Cells.Item(ik).Specific.value.ToString().Trim())

                    Dim dQuantity As Double = 0
                    strQuery = "Select Top 1 T0.DocEntry,DocNum ,T1.ItemCode,T1.Quantity,T1.WhsCode,T2.ItemName From ORDR T0 Inner Join RDR1 T1 On T0.DocEntry =T1.DocEntry Inner Join OITM T2 on T1.ItemCode =T2.ItemCode    Where T0.DocEntry in (Select distinct T1.U_BaseEntry  from [@AS_OWORD] T0 Inner Join  [@AS_WORD2] T1 On T0.DocEntry =T1.DocEntry  Where T0.Canceled ='N' and   isnull(T1.U_BaseEntry,'')!=''  AND T1.DocEntry = '" + sDocEntry + "')"
                    Dim Ors As SAPbobsCOM.Recordset
                    Ors = DoQuery(strQuery)
                    If Ors.RecordCount > 0 Then
                        sDocEntry = Ors.Fields.Item("DocEntry").Value.ToString()
                        sDocNum = Ors.Fields.Item("DocNum").Value.ToString()
                        sItemCode = Ors.Fields.Item("ItemCode").Value.ToString()
                        sItemName = Ors.Fields.Item("ItemName").Value.ToString()

                        sWhsCode = Ors.Fields.Item("WhsCode").Value.ToString()
                        dQuantity = TotalQty
                    End If

                    Dim sMachine As String = String.Empty
                    sMachine = "Y"
                    Dim sAttdStatus As String = String.Empty

                    Dim oGeneralService As SAPbobsCOM.GeneralService
                    Dim oGeneralData As SAPbobsCOM.GeneralData
                    Dim oChild As SAPbobsCOM.GeneralData
                    Dim oChildren As SAPbobsCOM.GeneralDataCollection
                    Dim sCmp As SAPbobsCOM.CompanyService = oCompany.GetCompanyService


                    oGeneralService = sCmp.GetGeneralService("AS_OWORD")
                    oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                    oGeneralData.SetProperty("U_Type", Type)
                    oGeneralData.SetProperty("U_DocDate", DateTime.Now)
                    oGeneralData.SetProperty("U_Status", "O")

                    oGeneralData.SetProperty("U_SalesOrderQty", dQuantity)
                    oGeneralData.SetProperty("U_ProducedQty", dQuantity)
                    oGeneralData.SetProperty("U_FGCode", sItemCode)
                    oGeneralData.SetProperty("U_FgName", sItemName)
                    oGeneralData.SetProperty("U_Type", strSerialCode.ToString().Trim())

                    oGeneralData.SetProperty("U_MachineCode", MCode)
                    oGeneralData.SetProperty("U_MachineName", MName)


                    If StartDate = String.Empty Then
                        oApplication.SetStatusBarMessage("Start Date Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                        Return False
                    End If
                    If EndDate = String.Empty Then
                        oApplication.SetStatusBarMessage("End  Date Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                        Return False
                    End If

                    Dim sStartDate2 As DateTime = DateTime.ParseExact(StartDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                    Dim sEndDate2 As DateTime = DateTime.ParseExact(EndDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)

                    oGeneralData.SetProperty("U_StartDate", sStartDate2)
                    oGeneralData.SetProperty("U_EndDate", sEndDate2)



                    oGeneralData.SetProperty("U_BaseLine", LineID)
                    oGeneralData.SetProperty("U_BaseNum", oDBDSHeader.GetValue("DocNum", 0))
                    oGeneralData.SetProperty("U_BaseEntry", oDBDSHeader.GetValue("DocEntry", 0))
                    oGeneralData.SetProperty("U_BaseObject", "OPPL")
                    oGeneralData.SetProperty("U_Series", oDBDSHeader.GetValue("Series", 0))
                    Dim RouteCode As String = String.Empty
                    Dim RouteName As String = String.Empty

                    Dim bLineDetails As Boolean = False

                    oChildren = oGeneralData.Child("AS_WORD1")

                    strQuery = "Select  U_RuleC,U_RuleN, U_OpCode,U_OpName,U_PCyc,U_Process ,U_BalTime  From [@AS_ROOT] T0 Inner Join [@AS_ROOT1] T1 On T0.DocEntry=T1.DocEntry Where T0.U_RuleC ='" + Trim(oDBDSHeader.GetValue("U_RouteCode", 0)) + "'"
                    Dim Ors_Root As SAPbobsCOM.Recordset
                    Dim sOperationCode As String = String.Empty
                    Ors_Root = DoQuery(strQuery)
                    If Ors_Root.RecordCount > 0 Then
                        bLineDetails = True
                        oChild = oChildren.Add
                        For k = 0 To Ors_Root.RecordCount - 1
                            If k = 0 Then
                                sOperationCode = Ors_Root.Fields.Item("U_OperCode").Value
                            End If
                            oChildren.Item(k).SetProperty("U_OpCode", Ors_Root.Fields.Item("U_OperCode").Value)
                            oChildren.Item(k).SetProperty("U_OpName", Ors_Root.Fields.Item("U_OperName").Value)
                            oChildren.Item(k).SetProperty("U_PCyc", Ors_Root.Fields.Item("U_PCyc").Value)
                            Dim sQty As Double = Ors_Root.Fields.Item("U_PCyc").Value * dQuantity
                            oChildren.Item(k).SetProperty("U_PCycle", sQty)
                            oChildren.Item(k).SetProperty("U_Process", Ors_Root.Fields.Item("U_Process").Value)
                            RouteCode = Ors_Root.Fields.Item("U_RuleC").Value
                            RouteName = Ors_Root.Fields.Item("U_RuleN").Value

                            oChild = oChildren.Add
                            Ors_Root.MoveNext()

                        Next
                    End If

                    Dim sQuery2 = "EXEC DBO.[@AIS_PPC_GetMachineCycleTime] '" + sOperationCode + "','" + dQuantity.ToString() + "'"
                    Dim rsetEmpDets_GetRequiredHours As SAPbobsCOM.Recordset = DoQuery(sQuery2)
                    If rsetEmpDets_GetRequiredHours.RecordCount > 0 Then
                        Dim sQ As String = Trim(rsetEmpDets_GetRequiredHours.Fields.Item("ReqHrs").Value)
                        oGeneralData.SetProperty("U_RequiredHrs", sQ)
                    Else
                        oGeneralData.SetProperty("U_RequiredHrs", "0")
                    End If



                    If bLineDetails = False Then
                        oChild = oChildren.Add
                    End If


                    oGeneralData.SetProperty("U_RuleC", RouteCode)
                    oGeneralData.SetProperty("U_RuleN", RouteName)


                    oChildren = oGeneralData.Child("AS_WORD2")

                    strQuery = "Select Top 1 CardCode,CardName, T0.DocEntry,DocNum ,T1.ItemCode,T1.Quantity,T1.WhsCode From ORDR T0 Inner Join RDR1 T1 On T0.DocEntry =T1.DocEntry Where   T0.DocEntry='" + sDocEntry + "'"
                    Dim Ors_SalesOrderDetails As SAPbobsCOM.Recordset
                    Ors_SalesOrderDetails = DoQuery(strQuery)
                    If Ors_SalesOrderDetails.RecordCount > 0 Then
                        bLineDetails = True
                        oChild = oChildren.Add
                        For k = 0 To Ors_SalesOrderDetails.RecordCount - 1

                            sDocEntry = Ors.Fields.Item("DocEntry").Value
                            sDocNum = Ors.Fields.Item("DocNum").Value
                            sItemCode = Ors.Fields.Item("ItemCode").Value
                            sWhsCode = Ors.Fields.Item("WhsCode").Value
                            dQuantity = Convert.ToDouble(Ors.Fields.Item("Quantity").Value)
                            oChildren.Item(k).SetProperty("U_BaseEntry", Ors_SalesOrderDetails.Fields.Item("DocEntry").Value.ToString())
                            oChildren.Item(k).SetProperty("U_BaseNum", Ors_SalesOrderDetails.Fields.Item("DocNum").Value.ToString())
                            oChildren.Item(k).SetProperty("U_CardCode", Ors_SalesOrderDetails.Fields.Item("CardCode").Value)
                            oChildren.Item(k).SetProperty("U_CardName", Ors_SalesOrderDetails.Fields.Item("CardName").Value)
                            oChildren.Item(k).SetProperty("U_TotalQty", Ors_SalesOrderDetails.Fields.Item("Quantity").Value.ToString())
                            oChildren.Item(k).SetProperty("U_PendingQty", Ors_SalesOrderDetails.Fields.Item("Quantity").Value.ToString())

                            oChild = oChildren.Add
                            Ors_SalesOrderDetails.MoveNext()
                        Next
                    End If
                    If bLineDetails = False Then
                        oChild = oChildren.Add
                    End If
                    oChildren = oGeneralData.Child("AS_WORD3")
                    oChild = oChildren.Add

                    If bLineDetails = True Then
                        Try
                            oGeneralService.Add(oGeneralData)

                            Dim WorkOrderNo As String
                            Dim WorkOrderDocEntry As String
                            Dim sQuery As String = String.Empty
                            sQuery = "  Select DocNum,DocEntry from [@AS_OWORD] Where Canceled ='N' AND  U_BaseNum='" + oDBDSHeader.GetValue("DocNum", 0) + "' and U_BaseEntry='" + oDBDSHeader.GetValue("DocEntry", 0) + "'"
                            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                            If rsetEmpDets.RecordCount > 0 Then
                                rsetEmpDets.MoveFirst()
                                WorkOrderNo = rsetEmpDets.Fields.Item("DocNum").Value
                                WorkOrderDocEntry = rsetEmpDets.Fields.Item("DocEntry").Value
                            End If
                            oMatrix.Columns.Item("WDocNum").Cells.Item(ik).Specific.value = WorkOrderNo
                            oMatrix.Columns.Item("WDocEntry").Cells.Item(ik).Specific.value = WorkOrderDocEntry

                            oApplication.SetStatusBarMessage("Data Posted Successfully", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                        Catch ex As Exception
                            oApplication.SetStatusBarMessage("Posting Error", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                        End Try
                    End If
                Next
            ElseIf strSerialCode = "FW" Then
                For ik As Integer = 1 To oMatrix.VisualRowCount
                    Dim sDocEntry As String = oMatrix.Columns.Item("bentry").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim TotalQty As Double = Convert.ToDouble(oMatrix.Columns.Item("TotalQty").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim StartDate As String = (oMatrix.Columns.Item("PDF").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim EndDate As String = (oMatrix.Columns.Item("PDT").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim MCode As String = (oMatrix.Columns.Item("MCode").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim MName As String = (oMatrix.Columns.Item("MName").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim LineID As String = (oMatrix.Columns.Item("LID").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim sItemName As String = String.Empty
                    Dim sDocNum As String = String.Empty
                    Dim sItemCode As String = String.Empty
                    Dim sWhsCode As String = String.Empty
                    Dim dQuantity As Double = 0
                    strQuery = "Select Top 1 T0.DocEntry,DocNum ,T1.ItemCode,T1.Quantity,T1.WhsCode,T2.ItemName From ORDR T0 Inner Join RDR1 T1 On T0.DocEntry =T1.DocEntry Inner Join OITM T2 on T1.ItemCode =T2.ItemCode Where T0.DocEntry in (Select distinct T1.U_BaseEntry  from [@AS_OWORD] T0 Inner Join  [@AS_WORD2] T1 On T0.DocEntry =T1.DocEntry  Where T0.Canceled ='N' and   isnull(T1.U_BaseEntry,'')!=''  AND T1.DocEntry = '" + sDocEntry + "')"
                    Dim Ors As SAPbobsCOM.Recordset
                    Ors = DoQuery(strQuery)
                    If Ors.RecordCount > 0 Then
                        sDocEntry = Ors.Fields.Item("DocEntry").Value.ToString()
                        sDocNum = Ors.Fields.Item("DocNum").Value.ToString()
                        sItemCode = Ors.Fields.Item("ItemCode").Value.ToString()
                        sItemName = Ors.Fields.Item("ItemName").Value.ToString()

                        sWhsCode = Ors.Fields.Item("WhsCode").Value.ToString()
                        dQuantity = TotalQty
                    End If

                    Dim sMachine As String = String.Empty
                    sMachine = "Y"
                    Dim sAttdStatus As String = String.Empty

                    Dim oGeneralService As SAPbobsCOM.GeneralService
                    Dim oGeneralData As SAPbobsCOM.GeneralData
                    Dim oChild As SAPbobsCOM.GeneralData
                    Dim oChildren As SAPbobsCOM.GeneralDataCollection
                    Dim sCmp As SAPbobsCOM.CompanyService = oCompany.GetCompanyService


                    oGeneralService = sCmp.GetGeneralService("AS_OWORD")
                    oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                    oGeneralData.SetProperty("U_Type", Type)
                    oGeneralData.SetProperty("U_DocDate", DateTime.Now)
                    oGeneralData.SetProperty("U_Status", "O")
                    oGeneralData.SetProperty("U_SalesOrderQty", dQuantity)
                    oGeneralData.SetProperty("U_ProducedQty", dQuantity)
                    oGeneralData.SetProperty("U_MachineCode", MCode)
                    oGeneralData.SetProperty("U_MachineName", MName)

                    oGeneralData.SetProperty("U_FGCode", sItemCode)
                    oGeneralData.SetProperty("U_FgName", sItemName)
                    oGeneralData.SetProperty("U_Type", strSerialCode.ToString().Trim())

                    If StartDate = String.Empty Then
                        oApplication.SetStatusBarMessage("Start Date Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                        Return False
                    End If
                    If EndDate = String.Empty Then
                        oApplication.SetStatusBarMessage("End  Date Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                        Return False
                    End If

                    Dim sStartDate2 As DateTime = DateTime.ParseExact(StartDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)
                    Dim sEndDate2 As DateTime = DateTime.ParseExact(EndDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None)

                    oGeneralData.SetProperty("U_StartDate", sStartDate2)
                    oGeneralData.SetProperty("U_EndDate", sEndDate2)
                    oGeneralData.SetProperty("U_BaseLine", LineID)

                    oGeneralData.SetProperty("U_BaseNum", oDBDSHeader.GetValue("DocNum", 0))
                    oGeneralData.SetProperty("U_BaseEntry", oDBDSHeader.GetValue("DocEntry", 0))
                    oGeneralData.SetProperty("U_BaseObject", "OPPL")
                    oGeneralData.SetProperty("U_Series", oDBDSHeader.GetValue("Series", 0))
                    Dim RouteCode As String = String.Empty
                    Dim RouteName As String = String.Empty

                    Dim bLineDetails As Boolean = False

                    oChildren = oGeneralData.Child("AS_WORD1")

                    Dim sOperationCode As String = String.Empty

                    strQuery = "Select U_OperCode,U_OperName,U_MCCode,U_MCName,U_PCyc,U_Process, ''U_RuleC  ,'' as U_RuleN  from [@AIS_OIOD]  T0 Inner join  [@AIS_IOD1]   T1 On T0.DocEntry =T1.DocEntry Where Isnull(U_OperCode,'')!='' AND  T0.U_ItemCode ='" + sItemCode + "' and T1.U_MCCode ='" + MCode + "'"
                    Dim Ors_Root As SAPbobsCOM.Recordset
                    Ors_Root = DoQuery(strQuery)
                    If Ors_Root.RecordCount > 0 Then
                        bLineDetails = True
                        oChild = oChildren.Add
                        For k = 0 To Ors_Root.RecordCount - 1
                            If k = 0 Then
                                sOperationCode = Ors_Root.Fields.Item("U_OperCode").Value
                            End If
                            oChildren.Item(k).SetProperty("U_OpCode", Ors_Root.Fields.Item("U_OperCode").Value)
                            oChildren.Item(k).SetProperty("U_OpName", Ors_Root.Fields.Item("U_OperName").Value)
                            oChildren.Item(k).SetProperty("U_PCyc", Ors_Root.Fields.Item("U_PCyc").Value)
                            Dim sQty As Double = Ors_Root.Fields.Item("U_PCyc").Value * dQuantity
                            oChildren.Item(k).SetProperty("U_PCycle", sQty)
                            oChildren.Item(k).SetProperty("U_Process", Ors_Root.Fields.Item("U_Process").Value)
                            RouteCode = Ors_Root.Fields.Item("U_RuleC").Value
                            RouteName = Ors_Root.Fields.Item("U_RuleN").Value
                            oChild = oChildren.Add
                            Ors_Root.MoveNext()
                        Next
                    End If

                    Dim sQuery2 = "EXEC DBO.[@AIS_PPC_GetMachineCycleTime] '" + sOperationCode + "','" + dQuantity.ToString() + "'"
                    Dim rsetEmpDets_GetRequiredHours As SAPbobsCOM.Recordset = DoQuery(sQuery2)
                    If rsetEmpDets_GetRequiredHours.RecordCount > 0 Then
                        Dim sQ As String = Trim(rsetEmpDets_GetRequiredHours.Fields.Item("ReqHrs").Value)
                        oGeneralData.SetProperty("U_RequiredHrs", sQ)
                    Else
                        oGeneralData.SetProperty("U_RequiredHrs", "0")
                    End If




                    'strQuery = "Select  U_RuleC,U_RuleN, U_OpCode,U_OpName,U_PCyc,U_Process ,U_BalTime  From [@AS_ROOT] T0 Inner Join [@AS_ROOT1] T1 On T0.DocEntry=T1.DocEntry Where T0.U_RuleC ='" + Trim(oDBDSHeader.GetValue("U_RouteCode", 0)) + "'"
                    'Dim Ors_Root As SAPbobsCOM.Recordset
                    'Ors_Root = DoQuery(strQuery)
                    'If Ors_Root.RecordCount > 0 Then
                    '    bLineDetails = True
                    '    oChild = oChildren.Add
                    '    For k = 0 To Ors_Root.RecordCount - 1
                    '        oChildren.Item(k).SetProperty("U_OpCode", Ors_Root.Fields.Item("U_OperCode").Value)
                    '        oChildren.Item(k).SetProperty("U_OpName", Ors_Root.Fields.Item("U_OperName").Value)
                    '        oChildren.Item(k).SetProperty("U_PCyc", Ors_Root.Fields.Item("U_PCyc").Value)
                    '        oChildren.Item(k).SetProperty("U_Process", Ors_Root.Fields.Item("U_Process").Value)
                    '        RouteCode = Ors_Root.Fields.Item("U_RuleC").Value
                    '        RouteName = Ors_Root.Fields.Item("U_RuleN").Value

                    '        oChild = oChildren.Add
                    '        Ors_Root.MoveNext()

                    '    Next
                    'End If
                    If bLineDetails = False Then
                        oChild = oChildren.Add
                    End If




                    oGeneralData.SetProperty("U_RuleC", RouteCode)
                    oGeneralData.SetProperty("U_RuleN", RouteName)


                    oChildren = oGeneralData.Child("AS_WORD2")

                    strQuery = "Select Top 1 CardCode,CardName, T0.DocEntry,DocNum ,T1.ItemCode,T1.Quantity,T1.WhsCode From ORDR T0 Inner Join RDR1 T1 On T0.DocEntry =T1.DocEntry Where   T0.DocEntry='" + sDocEntry + "'"
                    Dim Ors_SalesOrderDetails As SAPbobsCOM.Recordset
                    Ors_SalesOrderDetails = DoQuery(strQuery)
                    If Ors_SalesOrderDetails.RecordCount > 0 Then
                        bLineDetails = True
                        oChild = oChildren.Add
                        For k = 0 To Ors_SalesOrderDetails.RecordCount - 1

                            sDocEntry = Ors.Fields.Item("DocEntry").Value
                            sDocNum = Ors.Fields.Item("DocNum").Value
                            sItemCode = Ors.Fields.Item("ItemCode").Value
                            sWhsCode = Ors.Fields.Item("WhsCode").Value
                            dQuantity = Convert.ToDouble(Ors.Fields.Item("Quantity").Value)

                            oChildren.Item(k).SetProperty("U_BaseEntry", Ors_SalesOrderDetails.Fields.Item("DocEntry").Value.ToString())
                            oChildren.Item(k).SetProperty("U_BaseNum", Ors_SalesOrderDetails.Fields.Item("DocNum").Value.ToString())
                            oChildren.Item(k).SetProperty("U_CardCode", Ors_SalesOrderDetails.Fields.Item("CardCode").Value)
                            oChildren.Item(k).SetProperty("U_CardName", Ors_SalesOrderDetails.Fields.Item("CardName").Value)
                            oChildren.Item(k).SetProperty("U_TotalQty", Ors_SalesOrderDetails.Fields.Item("Quantity").Value.ToString())
                            oChildren.Item(k).SetProperty("U_PendingQty", Ors_SalesOrderDetails.Fields.Item("Quantity").Value.ToString())

                            oChild = oChildren.Add
                            Ors_SalesOrderDetails.MoveNext()
                        Next
                    End If

                    If bLineDetails = False Then
                        oChild = oChildren.Add
                    End If
                    oChildren = oGeneralData.Child("AS_WORD3")
                    oChild = oChildren.Add

                    If bLineDetails = True Then
                        Try
                            oGeneralService.Add(oGeneralData)

                            Dim WorkOrderNo As String
                            Dim WorkOrderDocEntry As String
                            Dim sQuery As String = String.Empty
                            sQuery = "  Select DocNum,DocEntry from [@AS_OWORD] Where Canceled ='N' AND  U_BaseNum='" + oDBDSHeader.GetValue("DocNum", 0) + "' and U_BaseEntry='" + oDBDSHeader.GetValue("DocEntry", 0) + "'"
                            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                            If rsetEmpDets.RecordCount > 0 Then
                                rsetEmpDets.MoveFirst()
                                WorkOrderNo = rsetEmpDets.Fields.Item("DocNum").Value
                                WorkOrderDocEntry = rsetEmpDets.Fields.Item("DocEntry").Value
                            End If
                            oMatrix.Columns.Item("WDocNum").Cells.Item(ik).Specific.value = WorkOrderNo
                            oMatrix.Columns.Item("WDocEntry").Cells.Item(ik).Specific.value = WorkOrderDocEntry

                            oApplication.SetStatusBarMessage("Data Posted Successfully", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                        Catch ex As Exception
                            oApplication.SetStatusBarMessage("Posting Error", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                        End Try
                    End If
                Next
            End If


            Return True



        Catch ex As Exception
            oApplication.StatusBar.SetText(" Work Order Document   Posting Method Faild", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        Finally

        End Try
    End Function




    Function WorkOrderForUpdate() As Boolean
        Try
            Dim strQuery As String

            Dim oCmbSerial As SAPbouiCOM.ComboBox = frmProductionPlan.Items.Item("c_Type").Specific
            Dim strSerialCode As String = oCmbSerial.Selected.Value
            Dim oc_Type As SAPbouiCOM.ComboBox = frmProductionPlan.Items.Item("c_Type").Specific
            Dim Type As String = oc_Type.Selected.Value
            If strSerialCode = "G" Or strSerialCode = "S" Or strSerialCode = "T" Or strSerialCode = "F" Then
            ElseIf strSerialCode = "SO" Then
            ElseIf strSerialCode = "FW" Then
                For ik As Integer = 1 To oMatrix.VisualRowCount
                    Dim sDocEntry As String = oMatrix.Columns.Item("bentry").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim TotalQty As Double = Convert.ToDouble(oMatrix.Columns.Item("TotalQty").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim StartDate As String = (oMatrix.Columns.Item("PDF").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim EndDate As String = (oMatrix.Columns.Item("PDT").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim basenum As String = (oMatrix.Columns.Item("basenum").Cells.Item(ik).Specific.value.ToString().Trim())
                    Dim sDocNum As String = String.Empty
                    Dim sItemCode As String = String.Empty
                    Dim sWhsCode As String = String.Empty
                    Dim dQuantity As Double = 0


                    Dim sMachine As String = String.Empty
                    sMachine = "Y"
                    Dim sAttdStatus As String = String.Empty

                    'Dim oGeneralService As SAPbobsCOM.GeneralService
                    'Dim oGeneralData As SAPbobsCOM.GeneralData
                    Dim oChild As SAPbobsCOM.GeneralData
                    Dim oChildren As SAPbobsCOM.GeneralDataCollection
                    'Dim sCmp As SAPbobsCOM.CompanyService = oCompany.GetCompanyService
                    Dim bLineDetails As Boolean = False

                    Dim oGeneralService As SAPbobsCOM.GeneralService
                    Dim oGeneralData As SAPbobsCOM.GeneralData
                    Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                    Dim sCmp As SAPbobsCOM.CompanyService
                    sCmp = oCompany.GetCompanyService
                    oGeneralService = sCmp.GetGeneralService("AS_OWORD")

                    'Get UDO record
                    oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                    oGeneralParams.SetProperty("DocEntry", sDocEntry)
                    oGeneralData = oGeneralService.GetByParams(oGeneralParams)




                    'oGeneralService = sCmp.GetGeneralService("AS_OWORD")

                    'oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                    'oGeneralData.SetProperty("DocNum", basenum.ToString().Trim())
                    oChildren = oGeneralData.Child("AS_WORD2")


                    strQuery = "Select Top 1 T0.DocEntry,DocNum ,T1.ItemCode,T1.Quantity,T1.WhsCode,  T0.CardCode,  T0.CardName From ORDR T0 Inner Join RDR1 T1 On T0.DocEntry =T1.DocEntry Where  U_WDDocEntry ='" + sDocEntry + "' AND T0.U_Series='" + oDBDSHeader.GetValue("Series", 0) + "' AND  T0.U_BaseObject='OPPL' AND  T0.U_baseNum = '" + oDBDSHeader.GetValue("DocNum", 0) + "' "
                    Dim Ors_SalesOrderDetails As SAPbobsCOM.Recordset
                    Ors_SalesOrderDetails = DoQuery(strQuery)
                    If Ors_SalesOrderDetails.RecordCount > 0 Then
                        bLineDetails = True
                        oChild = oChildren.Add
                        For k = 0 To Ors_SalesOrderDetails.RecordCount - 1
                            oChildren.Item(k).SetProperty("U_BaseEntry", Ors_SalesOrderDetails.Fields.Item("DocEntry").Value.ToString())
                            oChildren.Item(k).SetProperty("U_BaseNum", Ors_SalesOrderDetails.Fields.Item("DocNum").Value.ToString())
                            oChildren.Item(k).SetProperty("U_CardCode", Ors_SalesOrderDetails.Fields.Item("CardCode").Value)
                            oChildren.Item(k).SetProperty("U_CardName", Ors_SalesOrderDetails.Fields.Item("CardName").Value)
                            oChildren.Item(k).SetProperty("U_TotalQty", Ors_SalesOrderDetails.Fields.Item("Quantity").Value.ToString())
                            oChildren.Item(k).SetProperty("U_PendingQty", Ors_SalesOrderDetails.Fields.Item("Quantity").Value.ToString())
                            oChild = oChildren.Add
                            Ors_SalesOrderDetails.MoveNext()
                        Next
                    End If

                    If bLineDetails = False Then
                        oChild = oChildren.Add
                    End If




                    If bLineDetails = True Then
                        Try

                            oGeneralService.Update(oGeneralData)
                            oApplication.SetStatusBarMessage("Data Updated Successfully", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                        Catch ex As Exception
                            oApplication.SetStatusBarMessage("Posting Error", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                        End Try
                    End If
                Next
            End If


            Return True



        Catch ex As Exception
            oApplication.StatusBar.SetText(" Work Order Document   Posting Method Faild", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        Finally

        End Try
    End Function
    Function TransactionManagementInventoryTransfer() As Boolean
        Try
            Dim boolGoToStockPosting As Boolean = False
            TransactionManagementInventoryTransfer = True
            oApplication.SetStatusBarMessage("Please Wait,System is Checking Validation and Posting Stock", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
            Dim boolJournalTrue As Boolean = False
            If Me.InventoryTransfer() = False Then
                Return False
            End If
            TransactionManagementInventoryTransfer = True
        Catch ex As Exception
            TransactionManagementInventoryTransfer = False
            StatusBarErrorMsg("Transaction Management Failed : " & ex.Message)
            TransactionManagementInventoryTransfer = False
        Finally
        End Try
    End Function


    Function InventoryTransfer() As Boolean
        Try

            Try
                Dim ErrCode As Long
                Dim ErrMsg As String = ""
                Dim strQuery As String
                strQuery = " Select DocEntry from OWTR Where  U_Series='" + oDBDSHeader.GetValue("Series", 0) + "' AND U_BaseNum ='" + oDBDSHeader.GetValue("DocNum", 0) + "' AND U_BaseObject = 'OPPL'"
                Dim Ors_RecordsCount As SAPbobsCOM.Recordset
                Ors_RecordsCount = DoQuery(strQuery)
                If Ors_RecordsCount.RecordCount > 0 Then
                    oApplication.SetStatusBarMessage("Already Inventry Posted for this Document", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                    Return False
                End If
                Dim sDataisOk As Boolean = False
                Dim sLoop_BaseEntry As New ArrayList()
                For ik As Integer = 1 To oMatrix.VisualRowCount
                    Dim sCardCode As String = oMatrix.Columns.Item("bentry").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim oCmbSerial As SAPbouiCOM.CheckBox = oMatrix.Columns.Item("Selects").Cells.Item(ik).Specific
                    If oCmbSerial.Checked = True Then
                        If Not (sLoop_BaseEntry.Contains(sCardCode)) Then
                            sLoop_BaseEntry.Add(sCardCode)
                        End If
                    End If

                Next
                For i As Integer = 0 To sLoop_BaseEntry.Count - 1
                    Dim oSalesOrder As SAPbobsCOM.IStockTransfer = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oStockTransfer)
                    Dim sBoolean As Boolean = False
                    For ik As Integer = 1 To oMatrix.VisualRowCount
                        Dim sInnerCardCodea As String = oMatrix.Columns.Item("bentry").Cells.Item(ik).Specific.value.ToString().Trim()
                        Dim ReqDate As String = oMatrix.Columns.Item("PlnDat").Cells.Item(ik).Specific.value.ToString().Trim()
                        Dim Mcode As String = oMatrix.Columns.Item("MCode").Cells.Item(ik).Specific.value.ToString().Trim()
                        Dim oCmbSerial As SAPbouiCOM.CheckBox = oMatrix.Columns.Item("Selects").Cells.Item(ik).Specific
                        If sInnerCardCodea = sLoop_BaseEntry.Item(i).ToString() And oCmbSerial.Checked = True Then
                            sDataisOk = True
                            If sBoolean = False Then
                                oSalesOrder.DocDate = Date.Now
                                oSalesOrder.TaxDate = Format_StringToDate(ReqDate)
                                oSalesOrder.Reference2 = oDBDSHeader.GetValue("DocNum", 0)
                                oSalesOrder.Comments = " Production Plan ." & oDBDSHeader.GetValue("DocNum", 0)
                                oSalesOrder.UserFields.Fields.Item("U_BaseNum").Value = oDBDSHeader.GetValue("DocNum", 0)
                                oSalesOrder.UserFields.Fields.Item("U_BaseObject").Value = "OPPL"
                                oSalesOrder.UserFields.Fields.Item("U_Series").Value = oDBDSHeader.GetValue("Series", 0)
                                sBoolean = True
                            End If
                            Dim sItemCode As String = oMatrix.Columns.Item("SFGCode").Cells.Item(ik).Specific.value.Trim
                            Dim PQty As String = oMatrix.Columns.Item("AQty").Cells.Item(ik).Specific.value.Trim
                            Dim TWhsCod As String = oMatrix.Columns.Item("TWhsCod").Cells.Item(ik).Specific.value.Trim
                            Dim WhsCod As String = oMatrix.Columns.Item("WhsCod").Cells.Item(ik).Specific.value.Trim
                            oSalesOrder.Lines.ItemCode = sItemCode
                            oSalesOrder.Lines.FromWarehouseCode = Trim(WhsCod)
                            oSalesOrder.Lines.Quantity = Trim(PQty)
                            oSalesOrder.Lines.WarehouseCode = Trim(TWhsCod)
                            If TWhsCod = String.Empty Then
                                oApplication.StatusBar.SetText("To Warehouse Should not be empty" & oCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                Return False
                            End If

                            If WhsCod = String.Empty Then
                                oApplication.StatusBar.SetText("From Warehouse Should not be empty" & oCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                Return False
                            End If

                            For iBatchNumber As Integer = 1 To oMatMainplanDetails.VisualRowCount
                                If oMatMainplanDetails.Columns.Item("UID").Cells.Item(iBatchNumber).Specific.string = ik Then
                                    oSalesOrder.Lines.BatchNumbers.BatchNumber = oMatMainplanDetails.Columns.Item("Batch").Cells.Item(iBatchNumber).Specific.string
                                    oSalesOrder.Lines.BatchNumbers.Quantity = oMatMainplanDetails.Columns.Item("Qty").Cells.Item(iBatchNumber).Specific.string
                                    oSalesOrder.Lines.BatchNumbers.Add()
                                End If
                            Next
                            oSalesOrder.Lines.Add()
                        End If
                    Next
                    If sDataisOk = True Then
                        If oSalesOrder.Add() = 0 Then

                            Dim WorkOrderNo As String
                            Dim WorkOrderDocEntry As String
                            Dim sQuery As String = String.Empty
                            sQuery = "  Select DocNum,DocEntry from OWTR Where DocEntry='" + oCompany.GetNewObjectKey + "'"
                            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                            If rsetEmpDets.RecordCount > 0 Then
                                rsetEmpDets.MoveFirst()
                                WorkOrderNo = rsetEmpDets.Fields.Item("DocNum").Value
                                WorkOrderDocEntry = rsetEmpDets.Fields.Item("DocEntry").Value
                            End If
                            oMatrix.Columns.Item("IDocNum").Cells.Item(i + 1).Specific.value = WorkOrderNo
                            oMatrix.Columns.Item("IDocEntry").Cells.Item(i + 1).Specific.value = WorkOrderDocEntry
                            oApplication.StatusBar.SetText("Inventory Transfer  posted successfully ,Customer Code" + sLoop_BaseEntry.Item(i).ToString(), SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                        Else
                            oApplication.StatusBar.SetText("Unable To Inventory Transfer  Document" & oCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                            Return False

                        End If
                    Else
                        oApplication.StatusBar.SetText("No Data to Create Inventory Transfer", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        Return False
                    End If
                Next

                Return True
            Catch ex As Exception
                oApplication.StatusBar.SetText(" Inventory Transfer    Posting Method Faild", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return False
            Finally
            End Try


            Return True



        Catch ex As Exception
            oApplication.StatusBar.SetText(" Inventory Transfer  Document   Posting Method Faild", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        Finally

        End Try
    End Function
    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Try
                        If BusinessObjectInfo.ActionSuccess Then


                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add ,Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub RightClickEvent(ByRef EventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case EventInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK
                    'If oApplication.Forms.Item(EventInfo.FormUID).Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And EventInfo.BeforeAction Then
                    cntlMat = EventInfo.ItemUID
                    If EventInfo.ItemUID = "Matrix" And _
                  oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And _
                  EventInfo.Row >= 1 Then
                        row2 = EventInfo.Row
                    End If
                    ' End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

End Class
