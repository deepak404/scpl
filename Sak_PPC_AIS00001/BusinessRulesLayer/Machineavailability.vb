﻿
Imports SAPLib
Class Machineavailability
    Dim frmMachineavailability As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim oMatrixMachine As SAPbouiCOM.Matrix
    Dim boolFormLoaded As Boolean = False
    Dim sFormUID As String
    Dim UDOID As String = "OMAV"
    Dim sQuery As String
    Dim iRowIndex As Integer
    Dim sColumnID As String

    Sub LoadMachineavailability()
        Try


            boolFormLoaded = False
            LoadXML(frmMachineavailability, MachineavailabilityFormID, MachineavailabilityXML)
            frmMachineavailability = oApplication.Forms.Item(MachineavailabilityFormID)
            oDBDSHeader = frmMachineavailability.DataSources.DBDataSources.Item("@AIS_OMAV")
            oDBDSDetail = frmMachineavailability.DataSources.DBDataSources.Item("@AIS_MAV1")

            oMatrixMachine = frmMachineavailability.Items.Item("Machine").Specific


            frmMachineavailability.PaneLevel = 1

            Me.InitForm()
            Me.DefineModeForFields()


        Catch ex As Exception
            oApplication.StatusBar.SetText("Load Machine availability Form Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally

        End Try
    End Sub

    Private Sub InitForm()
        Try

            LoadComboBoxSeries(frmMachineavailability.Items.Item("c_series").Specific, UDOID)
            LoadDocumentDate(frmMachineavailability.Items.Item("t_DocDate").Specific)
            oMatrixMachine.Clear()
            SetNewLine(oMatrixMachine, oDBDSDetail)
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally

        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Private Sub DefineModeForFields()
        Try
            frmMachineavailability.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmMachineavailability.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmMachineavailability.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            '4
            frmMachineavailability.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmMachineavailability.Items.Item("t_DocDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'frmMachineavailability.Items.Item("3").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'frmMachineavailability.Items.Item("4").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'frmMachineavailability.Items.Item("5").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'frmMachineavailability.Items.Item("6").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'frmMachineavailability.Items.Item("7").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'frmMachineavailability.Items.Item("8").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'frmMachineavailability.Items.Item("9").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'frmMachineavailability.Items.Item("10").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'frmMachineavailability.Items.Item("11").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub



    Sub AutoSumEnable()
        Try
            Try
                oMatrixMachine.Columns.Item("CQty").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrixMachine.Columns.Item("V_6").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrixMachine.Columns.Item("CTime").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrixMachine.Columns.Item("ReqHrs").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrixMachine.Columns.Item("Qty").ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
            Catch ex As Exception
                oApplication.StatusBar.SetText("AutoSumEnable " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            End Try

        Catch ex As Exception
            oApplication.StatusBar.SetText("AutoSumEnable " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub



    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()
                    'Case "1294"
                    '    SetNewLine(oMatrixMachine, oDBDSDetail)
                    'oSalesEstimation.EstimateNo = oDBDSHeader.GetValue("OpprId", 0).ToString
                    'oSalesEstimation.LoadForm()

                Case "1294"
                    oMatrixMachine.FlushToDataSource()
                    oMatrixMachine.AddRow()
                    oMatrixMachine.GetCellSpecific(0, oMatrixMachine.VisualRowCount).Value = oMatrixMachine.VisualRowCount
                    oMatrixMachine.FlushToDataSource()
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


    Sub LoadPreSalesOrderDetails()
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the Presales Order Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            sQuery = " EXEC [dbo].[@AIS_PPC_GetPreSalesOrderItemDetails] "
            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
            rsetEmpDets.MoveFirst()
            oDBDSDetail.Clear()
            oMatrixMachine.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                oDBDSDetail.Offset = i

                oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, i + 1)

                oDBDSDetail.SetValue("U_BaseEntry", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("DocEntry").Value)
                oDBDSDetail.SetValue("U_BaseNum", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("DocNum").Value)
                oDBDSDetail.SetValue("U_BaseObject", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("Object").Value)
                oDBDSDetail.SetValue("U_BaseLine", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("LineId").Value)
                oDBDSDetail.SetValue("U_CardCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("CardCode").Value)
                oDBDSDetail.SetValue("U_CardName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("CardName").Value)
                oDBDSDetail.SetValue("U_ItemCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ItemCode").Value)
                oDBDSDetail.SetValue("U_ItemName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_ItemName").Value)
                oDBDSDetail.SetValue("U_Quantity", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_Quantity").Value)
                oDBDSDetail.SetValue("U_TotalPlanQty", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_Quantity").Value)
                oDBDSDetail.SetValue("U_UOM", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_UOM").Value)
                oDBDSDetail.SetValue("U_Remarks", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_Remarks").Value.ToString())
                oDBDSDetail.SetValue("U_WhsCode", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_WhsCode").Value)
                oDBDSDetail.SetValue("U_WhsName", oDBDSDetail.Offset, rsetEmpDets.Fields.Item("U_WhsName").Value)
                oDBDSDetail.SetValue("U_PostingDate", oDBDSDetail.Offset, CDate(rsetEmpDets.Fields.Item("U_PostingDate").Value).ToString("yyyyMMdd"))
                oDBDSDetail.SetValue("U_ReqDate", oDBDSDetail.Offset, CDate(rsetEmpDets.Fields.Item("U_RequiredDate").Value).ToString("yyyyMMdd"))
                rsetEmpDets.MoveNext()
            Next
            oMatrixMachine.LoadFromDataSource()
            AutoSumEnable()


            If frmMachineavailability.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmMachineavailability.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Machine Availability calculated successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Employees Based On Department Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub CalculateCycleTime(RowId As String, ColumnID As String, sKiddie As String, MacineCode As String)
        oMatrixMachine.FlushToDataSource()

        oMatrixMachine.LoadFromDataSource()
        'oMatrixMachine.Columns.Item(ColumnID).Cells.Item(RowId - 1).Click()


        Dim dAllocated3 As Double = 0
        Dim dAllocated2 As Double = 0
        Dim dAllocated1 As Double = 0

        For i As Integer = 1 To oMatrixMachine.VisualRowCount
            Dim sMacCode As String = oDBDSDetail.GetValue("U_MacCode", i - 1).Trim()
            Dim U_ReqHrs As Double = oDBDSDetail.GetValue("U_ReqHrs", i - 1).Trim()
            Dim sMacType As String = String.Empty
            Dim sQuery As String = String.Empty
            sQuery = " Select U_MacType  from ORSC Where   ResCode='" & sMacCode & "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
            If rsetEmpDets.RecordCount > 0 Then
                sMacType = Trim(rsetEmpDets.Fields.Item("U_MacType").Value)
            End If
            If sMacType = "K" Then
                dAllocated3 = dAllocated3 + U_ReqHrs
            End If
            If sMacType = "ZB" Then
                dAllocated2 = dAllocated2 + U_ReqHrs
            End If
            If sMacType = "ZA" Then
                dAllocated1 = dAllocated1 + U_ReqHrs
            End If






        Next
        oDBDSHeader.SetValue("U_Allocated1", 0, dAllocated1.ToString())
        oDBDSHeader.SetValue("U_Allocated2", 0, dAllocated2.ToString())
        oDBDSHeader.SetValue("U_Allocated3", 0, dAllocated3.ToString())


        If MacineCode <> String.Empty Then
            sQuery = " EXEC DBO.[@AIS_MachineAvailability_GetMachineAvilability]  '" & MacineCode & "' "

            Dim rsetEmpDets_GetMachineAvilability As SAPbobsCOM.Recordset = DoQuery(sQuery)
            If rsetEmpDets_GetMachineAvilability.RecordCount > 0 Then
                Dim sReqHrs As Double = Convert.ToDouble(Trim(rsetEmpDets_GetMachineAvilability.Fields.Item("ReqHrs").Value))
                If sKiddie = "ZA" Then
                    oDBDSHeader.SetValue("U_Available1", 0, sReqHrs.ToString())
                    If sReqHrs > 0 Then
                        oDBDSHeader.SetValue("U_Balance1", 0, (sReqHrs - dAllocated1).ToString())
                    End If
                End If
                If sKiddie = "ZB" Then
                    oDBDSHeader.SetValue("U_Available2", 0, sReqHrs.ToString())
                    If sReqHrs > 0 Then
                        oDBDSHeader.SetValue("U_Balance2", 0, (sReqHrs - dAllocated2).ToString())
                    End If

                End If
                If sKiddie = "K" Then
                    oDBDSHeader.SetValue("U_Available3", 0, sReqHrs.ToString())
                    If sReqHrs > 0 Then
                        oDBDSHeader.SetValue("U_Balance3", 0, (sReqHrs - dAllocated3).ToString())
                    End If

                End If

            End If
        Else
            Dim U_Available1 As Double = oDBDSHeader.GetValue("U_Available1", 0)
            Dim U_Available2 As Double = oDBDSHeader.GetValue("U_Available2", 0)
            Dim U_Available3 As Double = oDBDSHeader.GetValue("U_Available3", 0)

            oDBDSHeader.SetValue("U_Balance1", 0, (U_Available1 - dAllocated1).ToString())
            oDBDSHeader.SetValue("U_Balance2", 0, (U_Available2 - dAllocated2).ToString())
            oDBDSHeader.SetValue("U_Balance3", 0, (U_Available3 - dAllocated3).ToString())
        End If




    End Sub

    Sub ItemEvent(FormUID As String, pVal As SAPbouiCOM.ItemEvent, BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID
                                Case "t_SCode"
                                    oDBDSHeader.SetValue("U_ScenarioCode", 0, Trim(oDataTable.GetValue("U_ScenarioCode", 0).ToString()))
                                    oDBDSHeader.SetValue("U_ScenarioName", 0, Trim(oDataTable.GetValue("U_ScenarioName", 0).ToString()))
                                    LoadPreSalesOrderDetails()
                                Case "Machine"
                                    Select Case pVal.ColUID
                                        Case "CQty"
                                            oMatrixMachine.FlushToDataSource()
                                            Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_Quantity", pVal.Row - 1)) + CDbl(Trim(oDataTable.GetValue("U_CushionQty", 0)))
                                            oDBDSDetail.SetValue("U_TotalPlanQty", pVal.Row - 1, dblValue)
                                            oMatrixMachine.LoadFromDataSource()
                                            oMatrixMachine.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        Case "MCode"
                                            oMatrixMachine.FlushToDataSource()
                                            Dim sResCode As String = Trim(oDataTable.GetValue("ResCode", 0))

                                            oDBDSDetail.SetValue("U_MacCode", pVal.Row - 1, sResCode)
                                            oDBDSDetail.SetValue("U_MacName", pVal.Row - 1, Trim(oDataTable.GetValue("ResName", 0)))
                                             
                                            Dim sQuery As String = String.Empty
                                            sQuery = "EXEC DBO.[@AIS_PPC_GetMachineCycleTimeDetails]'" + Trim(oDBDSDetail.GetValue("U_ItemCode", pVal.Row - 1)) + "','" + Trim(oDataTable.GetValue("ResCode", 0)) + "','" + Trim(oDBDSDetail.GetValue("U_TotalPlanQty", pVal.Row - 1)) + "'"
                                            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            If rsetEmpDets.RecordCount > 0 Then
                                                oDBDSDetail.SetValue("U_CycleTime", pVal.Row - 1, Trim(rsetEmpDets.Fields.Item("U_CycleTime").Value))
                                                oDBDSDetail.SetValue("U_ReqHrs", pVal.Row - 1, Trim(rsetEmpDets.Fields.Item("ReqHrs").Value))
                                            Else
                                                oDBDSDetail.SetValue("U_CycleTime", pVal.Row - 1, "0")
                                                oDBDSDetail.SetValue("U_ReqHrs", pVal.Row - 1, "0")

                                            End If


                                            Dim sDocDate As String = frmMachineavailability.Items.Item("t_DocDate").Specific.value

                                            sQuery = " EXEC DBO.[@AIS_MachineAvailability_GetMachineCapacity] '" & sDocDate & "' "
                                            Dim rsetEmpDets_MachingCapacity As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            If rsetEmpDets_MachingCapacity.RecordCount > 0 Then
                                                Dim sCycyleTime As String = Trim(rsetEmpDets_MachingCapacity.Fields.Item("NoOfDays").Value)
                                                oDBDSDetail.SetValue("U_MacCapaity", pVal.Row - 1, sCycyleTime)
                                            End If


                                            oMatrixMachine.LoadFromDataSource()
                                            oMatrixMachine.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            Dim sKiddie As String = Trim(oDataTable.GetValue("U_MacType", 0)).ToString()
                                            If sKiddie = "K" Then
                                                oMatrixMachine.Columns.Item("NoOfEnd").Editable = True
                                                oMatrixMachine.Columns.Item("Dimention").Editable = True
                                                oMatrixMachine.Columns.Item("SpType").Editable = True
                                            Else
                                                oMatrixMachine.Columns.Item("NoOfEnd").Editable = False
                                                oMatrixMachine.Columns.Item("Dimention").Editable = False
                                                oMatrixMachine.Columns.Item("SpType").Editable = False
                                            End If
                                            CalculateCycleTime(pVal.Row - 1, pVal.ColUID, sKiddie, sResCode)

                                            AutoSumEnable()



                                        Case "V_14"
                                            oMatrixMachine.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_CustCode", pVal.Row - 1, Trim(oDataTable.GetValue("CardCode", 0)))
                                            oDBDSDetail.SetValue("U_CustName", pVal.Row - 1, Trim(oDataTable.GetValue("CardName", 0)))
                                            oMatrixMachine.LoadFromDataSource()
                                            oMatrixMachine.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        Case "WhsCod"
                                            oMatrixMachine.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_WhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail.SetValue("U_WhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrixMachine.LoadFromDataSource()
                                            oMatrixMachine.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                        Case "V_12"

                                            oMatrixMachine.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_ItmGrpCode", pVal.Row - 1, Trim(oDataTable.GetValue("ItmsGrpCod", 0)))
                                            oDBDSDetail.SetValue("U_ItmGrpName", pVal.Row - 1, Trim(oDataTable.GetValue("ItmsGrpNam", 0)))
                                            oMatrixMachine.LoadFromDataSource()
                                            oMatrixMachine.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                    End Select

                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try



                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "Machine"
                                Select Case pVal.ColUID
                                    Case "MCode"
                                        If pVal.BeforeAction = False Then
                                            Dim MCode As String = oMatrixMachine.Columns.Item("MCode").Cells.Item(pVal.Row).Specific.value.Trim
                                            Dim MName As String = oMatrixMachine.Columns.Item("MName").Cells.Item(pVal.Row).Specific.value.Trim
                                            If MCode = String.Empty And MName <> String.Empty Then
                                                oDBDSDetail.SetValue("U_MacName", pVal.Row - 1, String.Empty)
                                                oDBDSDetail.SetValue("U_CycleTime", pVal.Row - 1, "0")
                                                oDBDSDetail.SetValue("U_ReqHrs", pVal.Row - 1, "0")
                                                oDBDSDetail.SetValue("U_MacCapaity", pVal.Row - 1, "0")
                                                oMatrixMachine.Columns.Item("NoOfEnd").Editable = False
                                                oMatrixMachine.Columns.Item("Dimention").Editable = False
                                                oMatrixMachine.Columns.Item("SpType").Editable = False
                                                oDBDSDetail.SetValue("U_MacCode", pVal.Row - 1, String.Empty)

                                                oMatrixMachine.LoadFromDataSource()
                                                CalculateCycleTime(pVal.Row - 1, pVal.ColUID, "N", "")
                                            End If
                                            AutoSumEnable()
 
                                        End If


                                End Select
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "Machine"
                                Select Case pVal.ColUID
                                    Case "MCode"
                                        If pVal.BeforeAction = False Then
                                            ChooseFromListFilteration(frmMachineavailability, "CFL_RES", "ResCode", "Select  ResCode  From ORSC Where ResType ='M'")
                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID

                            Case "Machine"
                                Select Case pVal.ColUID
                                    Case "CTime"
                                        If pVal.BeforeAction = False And pVal.ItemChanged Then

                                            'CalculateCycleTime(pVal.Row - 1, pVal.ColUID)
                                        End If

                                    Case "CQty"
                                        If pVal.BeforeAction = False And pVal.ItemChanged Then

                                            oMatrixMachine.FlushToDataSource()
                                            Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_Quantity", pVal.Row - 1)) + CDbl(oDBDSDetail.GetValue("U_CushionQty", pVal.Row - 1))
                                            oDBDSDetail.SetValue("U_TotalPlanQty", pVal.Row - 1, dblValue)

                                            oMatrixMachine.LoadFromDataSource()
                                            oMatrixMachine.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            AutoSumEnable()
                                            oMatrixMachine.Columns.Item("MCCode").Cells.Item(pVal.Row).Click()

                                            oApplication.SendKeys("{TAB}")
                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                    End Try
                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmMachineavailability.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmMachineavailability.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmMachineavailability.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "matrix1"
                                Select Case pVal.ColUID
                                    Case "Selected"
                                        If pVal.BeforeAction = False Then
                                            Try


                                            Catch ex As Exception
                                            Finally
                                            End Try

                                        End If
                                End Select

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID

                            Case "btnDup"

                                If pVal.BeforeAction = False Then
                                    For i As Integer = 1 To oMatrixMachine.VisualRowCount
                                        If oMatrixMachine.IsRowSelected(i) = True Then
                                            iRowIndex = i
                                            Exit For
                                        End If
                                    Next
                                    oMatrixMachine.FlushToDataSource()
                                    oMatrixMachine.AddRow()
                                    oMatrixMachine.Columns.Item("bentry").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("bentry").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("basenum").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("basenum").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("object").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("object").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("BaseLine").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("BaseLine").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("date").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("date").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("pdate").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("pdate").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("ICode").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("ICode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("IName").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("IName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("MName").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("MName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("WhsCod").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("WhsCod").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("WhsName").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("WhsName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()

                                    oMatrixMachine.Columns.Item("V_14").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("V_14").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("V_13").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("V_13").Cells.Item(iRowIndex).Specific.value.ToString().Trim()

                                    Dim sQty As String = oMatrixMachine.Columns.Item("Qty").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("Qty").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = sQty
                                    Dim sQty1 As String = oMatrixMachine.Columns.Item("Qty").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value

                                    oMatrixMachine.Columns.Item("V_8").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("V_8").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("CQty").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("CQty").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("V_6").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("V_6").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("MCode").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("MCode").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("MName").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("MName").Cells.Item(iRowIndex).Specific.value.ToString().Trim()

                                    oMatrixMachine.Columns.Item("V_4").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("V_4").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("CTime").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("CTime").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("ReqHrs").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("ReqHrs").Cells.Item(iRowIndex).Specific.value.ToString().Trim()

                                    oMatrixMachine.Columns.Item("NoOfEnd").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("NoOfEnd").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("Dimention").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("Dimention").Cells.Item(iRowIndex).Specific.value.ToString().Trim()
                                    oMatrixMachine.Columns.Item("SpType").Cells.Item(oMatrixMachine.VisualRowCount).Specific.value = oMatrixMachine.Columns.Item("SpType").Cells.Item(iRowIndex).Specific.value.ToString().Trim()

                                    oMatrixMachine.GetCellSpecific(0, oMatrixMachine.VisualRowCount).Value = oMatrixMachine.VisualRowCount
                                    oMatrixMachine.FlushToDataSource()
                                    frmMachineavailability.Update()

                                     
                                End If



                            Case "btnNext"
                                If pVal.BeforeAction = False Then
                                    Dim t_SCode As String = frmMachineavailability.Items.Item("t_SCode").Specific.value.ToString().Trim()
                                    Dim t_SName As String = frmMachineavailability.Items.Item("t_SName").Specific.value.ToString().Trim()
                                    If t_SCode = String.Empty Then
                                        oApplication.StatusBar.SetText("Scenario Code Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If

                                    If t_SName = String.Empty Then
                                        oApplication.StatusBar.SetText("Scenario Name Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If


                                    If oMatrixMachine.VisualRowCount = 0 Then
                                        oApplication.StatusBar.SetText("Details Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If
                                    If oMatrixMachine.VisualRowCount = 1 Then
                                        Dim sCustomerCode As String = oMatrixMachine.Columns.Item("ICode").Cells.Item(1).Specific.value.Trim
                                        If sCustomerCode = String.Empty Then
                                            oApplication.StatusBar.SetText("Details Should not be empty", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                            Return
                                        End If

                                    End If

                                    Dim intverify As Integer = 0
                                    For i As Integer = 1 To oMatrixMachine.VisualRowCount
                                        Dim sMCode As String = oMatrixMachine.Columns.Item("MCode").Cells.Item(i).Specific.value.Trim
                                        Dim verify As SAPbouiCOM.CheckBox = oMatrixMachine.Columns.Item("verify").Cells.Item(i).Specific
                                        If verify.Checked = True Then
                                            intverify = intverify + 1
                                            If sMCode = String.Empty Then
                                                oApplication.StatusBar.SetText("Machine Code Should not be Empty , Line Id " + i.ToString(), SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                                Return
                                            End If
                                        End If

                                    Next

                                    If intverify = 0 Then
                                        oApplication.StatusBar.SetText("Atleast One Records Need to be Select to move Next Process", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If
                                    Dim t_Avai1 As Double = Convert.ToDouble(frmMachineavailability.Items.Item("t_Avai1").Specific.value.ToString().Trim())
                                    Dim t_Avai2 As Double = Convert.ToDouble(frmMachineavailability.Items.Item("t_Avai2").Specific.value.ToString().Trim())
                                    Dim t_Avai3 As Double = Convert.ToDouble(frmMachineavailability.Items.Item("t_Avai3").Specific.value.ToString().Trim())

                                    If t_Avai1 < 0 Then
                                        oApplication.StatusBar.SetText("Zell-A  Should not be Less then Zero", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If

                                    If t_Avai2 < 0 Then
                                        oApplication.StatusBar.SetText("Zell-B  Should not be Less then Zero", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If


                                    If t_Avai3 < 0 Then
                                        oApplication.StatusBar.SetText("Keddi  Should not be Less then Zero", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If


                                    Dim t_Aolcate1 As Double = Convert.ToDouble(frmMachineavailability.Items.Item("t_Aolcate1").Specific.value.ToString().Trim())
                                    Dim t_Aolcate2 As Double = Convert.ToDouble(frmMachineavailability.Items.Item("t_Aolcate2").Specific.value.ToString().Trim())
                                    Dim t_Aolcate3 As Double = Convert.ToDouble(frmMachineavailability.Items.Item("t_Aolcate3").Specific.value.ToString().Trim())

                                    If t_Aolcate1 < 0 Then
                                        oApplication.StatusBar.SetText("Allocated Should not be Less then Zero", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If

                                    If t_Aolcate3 < 0 Then
                                        oApplication.StatusBar.SetText("Allocated  Should not be Less then Zero", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If


                                    If t_Avai3 < 0 Then
                                        oApplication.StatusBar.SetText("Allocated  Should not be Less then Zero", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If




                                    Dim t_Balance1 As Double = Convert.ToDouble(frmMachineavailability.Items.Item("t_Balance1").Specific.value.ToString().Trim())
                                    Dim t_Balance2 As Double = Convert.ToDouble(frmMachineavailability.Items.Item("t_Balance2").Specific.value.ToString().Trim())
                                    Dim t_Balance3 As Double = Convert.ToDouble(frmMachineavailability.Items.Item("t_Balance3").Specific.value.ToString().Trim())

                                    If t_Balance1 < 0 Then
                                        oApplication.StatusBar.SetText("Balance for Zell-A Should not be Less then Zero", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If

                                    If t_Balance2 < 0 Then
                                        oApplication.StatusBar.SetText("Balance for Zell-B Should not be Less then Zero", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If


                                    If t_Balance3 < 0 Then
                                        oApplication.StatusBar.SetText("Balance for Keddi  Should not be Less then Zero", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                        Return
                                    End If



                                    If frmMachineavailability.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                        frmMachineavailability.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                    End If
                                    frmMachineavailability.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    frmMachineavailability.Items.Item("2").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    'frmMachineavailability.Close()




                                    oApplication.ActivateMenuItem("OSAV")
                                    Dim frmRecommendation As SAPbouiCOM.Form = oApplication.Forms.ActiveForm


                                    Dim sQuery As String = String.Empty
                                    sQuery = " Select DocEntry  from [@AIS_OSAV] Where   U_ScenarioCode='" & t_SCode & "'"
                                    Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                    If rsetEmpDets.RecordCount > 0 Then
                                        frmRecommendation.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                        frmRecommendation.Items.Item("t_SCode").Specific.value = t_SCode
                                        frmRecommendation.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                        frmRecommendation.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE

                                    Else

                                        sQuery = " Select Code  from [@AIS_OSCE] Where   U_ScenarioCode='" & t_SCode & "'"
                                        Dim rsetEmpDets_Code As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                        Dim sCode As String = String.Empty
                                        If rsetEmpDets_Code.RecordCount > 0 Then
                                            sCode = Trim(rsetEmpDets_Code.Fields.Item("Code").Value)
                                        End If
                                        frmRecommendation.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE

                                        frmRecommendation.Items.Item("t_SCode").Specific.value = sCode
                                        LoadDocumentDate(frmRecommendation.Items.Item("t_DocDate").Specific)
                                        Dim oCmbSerial As SAPbouiCOM.ComboBox = frmRecommendation.Items.Item("c_series").Specific
                                        oCmbSerial.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)


                                    End If
                                    '  frmRecommendation.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)






                                End If

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.ActionSuccess And frmMachineavailability.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If
                            Case "17"
                                If pVal.BeforeAction = False Then
                                    frmMachineavailability.PaneLevel = 1
                                    frmMachineavailability.Items.Item("17").AffectsFormMode = False
                                    frmMachineavailability.Settings.MatrixUID = "Machine"
                                End If
                            Case "18"
                                If pVal.BeforeAction = False Then
                                    frmMachineavailability.PaneLevel = 2
                                    frmMachineavailability.Items.Item("18").AffectsFormMode = False
                                    frmMachineavailability.Settings.MatrixUID = "Total"
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Try
                        If BusinessObjectInfo.ActionSuccess Then


                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add ,Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        AutoSumEnable()
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


    Sub RightClickEvent(ByRef EventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case EventInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK
                    If frmMachineavailability.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And EventInfo.BeforeAction Then
                        Select Case EventInfo.ItemUID
                            Case "Machine"
                                iRowIndex = EventInfo.Row
                                sColumnID = EventInfo.ColUID

                        End Select

                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub



End Class
