﻿
Imports SAPLib
Imports System.Text.RegularExpressions

Public Class ResourceCapacity

    Dim frmResourceCapacity, frmSubPlanningDetailsSubMatrix As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail, oDBDSSubPlanDetails, oDBDSMainPlanDetails As SAPbouiCOM.DBDataSource
    Dim oMatrix, oMatSubPlanDetails, oMatMainplanDetails As SAPbouiCOM.Matrix
    Dim boolFormLoaded As Boolean = False
    Dim sFormUID As String
    Dim UDOID As String = "ORCP"
    Dim SubRowID_PlanningDetails As Integer
    Dim SubRowID_DayID As String
    Dim sQuery As String
    Sub LoadPreSales()

        Try


            boolFormLoaded = False
            LoadXML(frmResourceCapacity, ResourceCapacityFormID, ResourceCapacityXML)
            frmResourceCapacity = oApplication.Forms.Item(ResourceCapacityFormID)
            oDBDSHeader = frmResourceCapacity.DataSources.DBDataSources.Item("@AIS_ORCP")
            oDBDSDetail = frmResourceCapacity.DataSources.DBDataSources.Item("@AIS_RCP1")
            oDBDSMainPlanDetails = frmResourceCapacity.DataSources.DBDataSources.Item("@AIS_RCP2")
            oMatrix = frmResourceCapacity.Items.Item("Matrix").Specific
            oMatMainplanDetails = frmResourceCapacity.Items.Item("submatrix").Specific


            frmResourceCapacity.Freeze(True)
            ' 

            Me.InitForm()
            Me.DefineModeForFields()


        Catch ex As Exception
            oApplication.StatusBar.SetText("Load Pre Sales Form Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmResourceCapacity.Freeze(False)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmResourceCapacity.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            frmResourceCapacity.Freeze(True)
            LoadComboBoxSeries(frmResourceCapacity.Items.Item("c_series").Specific, UDOID)
            LoadDocumentDate(frmResourceCapacity.Items.Item("t_DocDate").Specific)

            oMatrix.Clear()
            SetNewLine(oMatrix, oDBDSDetail)
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmResourceCapacity.Freeze(False)
        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 


    Sub DefineModeForFields()
        Try
            frmResourceCapacity.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmResourceCapacity.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmResourceCapacity.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            '4
            frmResourceCapacity.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmResourceCapacity.Items.Item("t_DocDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub AutoSumEnable()
        Try

            oMatrix = frmResourceCapacity.Items.Item("Matrix").Specific
            Try
                For i As Integer = 1 To 31
                        oMatrix.Columns.Item("d" + i.ToString()).ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                Next
            Catch ex As Exception
                oApplication.StatusBar.SetText("AutoSumEnable " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            End Try


            Try
                For i As Integer = 1 To 31
                        oMatrix.Columns.Item("B" + i.ToString()).ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                Next
            Catch ex As Exception
                oApplication.StatusBar.SetText("AutoSumEnable " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            End Try

            Try
                For i As Integer = 1 To 31

                    oMatrix.Columns.Item("A" + i.ToString()).ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                Next
            Catch ex As Exception
                oApplication.StatusBar.SetText("AutoSumEnable " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            End Try

            
        Catch ex As Exception
            oApplication.StatusBar.SetText("AutoSumEnable " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub

    Function Validation() As Boolean
        Try

            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validation Function Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Function

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID
                                Case "t_Customer"
                                    oDBDSHeader.SetValue("U_Customer", 0, Trim(oDataTable.GetValue("CardCode", 0)))
                                    oDBDSHeader.SetValue("U_Name", 0, Trim(oDataTable.GetValue("CardName", 0)))


                                Case "Matrix"
                                    Select Case pVal.ColUID
                                        Case "RCode"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_ResourceNo", pVal.Row - 1, Trim(oDataTable.GetValue("ResCode", 0)))
                                            oDBDSDetail.SetValue("U_RDescription", pVal.Row - 1, Trim(oDataTable.GetValue("ResName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()


                                        Case "V_-1"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_ItemCode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail.SetValue("U_ItemName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        Case "V_3"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_TaxCode", pVal.Row - 1, Trim(oDataTable.GetValue("Code", 0)))
                                            oDBDSDetail.SetValue("U_TaxAmount", pVal.Row - 1, Trim(oDataTable.GetValue("Rate", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        Case "WhsCode"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_WhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail.SetValue("U_WhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            SetNewLine(oMatrix, oDBDSDetail, oMatrix.VisualRowCount, pVal.ColUID)

                                    End Select

                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID

                            Case "matPreSale"
                                Select Case pVal.ColUID
                                    Case "V_1", "V_8"
                                        If pVal.BeforeAction = False And pVal.ItemChanged Then
                                            oMatrix.FlushToDataSource()
                                            Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_Quantity", pVal.Row - 1)) * CDbl(oDBDSDetail.GetValue("U_UnitPrice", pVal.Row - 1))
                                            oDBDSDetail.SetValue("U_BaseAmount", pVal.Row - 1, dblValue)
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                        End If
                                End Select
                                Select Case pVal.ColUID
                                    Case "V_6"
                                        If pVal.BeforeAction = False And pVal.ItemChanged Then
                                            oMatrix.FlushToDataSource()
                                            Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_BaseAmount", pVal.Row - 1)) + CDbl(oDBDSDetail.GetValue("U_TaxAmount", pVal.Row - 1))
                                            oDBDSDetail.SetValue("U_TotalPrice", pVal.Row - 1, dblValue)
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                    End Try

                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmResourceCapacity.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmResourceCapacity.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmResourceCapacity.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case (SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK)
                    Try
                        Select Case pVal.ItemUID

                            Case "Matrix"
                                Select Case pVal.ColUID
                                    Case "d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "d10", "d11", "d12", "d13", "d14", "d15", "d16", "d17", "d18", "d19", "d20", "d21", "d22", "d23", "d24", "d25", "d26", "d27", "d28", "d29", "d30"
                                        If pVal.BeforeAction = False Then
                                            SubRowID_PlanningDetails = pVal.Row
                                            SubRowID_DayID = pVal.ColUID
                                            Me.LoadSubMatrixDetails(SubRowID_PlanningDetails, pVal.ColUID)


                                        End If

                                End Select
                        End Select

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID

                            Case "1"
                                If pVal.BeforeAction = True And (frmResourceCapacity.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmResourceCapacity.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                                    If Me.ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Exit Sub
                                    End If
                                End If

                            Case "3"
                                If pVal.BeforeAction = False Then
                                    SetVisibleAndInvisibleColumns()
                                End If

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.ActionSuccess And frmResourceCapacity.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub SetVisibleAndInvisibleColumns()
        Try

            Dim sQuery As String = String.Empty

            frmResourceCapacity.Freeze(True)

            Dim sColumnName As String = ""
            Dim iLastRowCount As Integer = 0
            Dim iLastRowCountCopy As Integer = 0
            Dim iLastRowCountCopy2 As Integer = 0
            Dim oColumn As SAPbouiCOM.Column

            For j As Integer = 1 To 31
                oColumn = oMatrix.Columns.Item("d" & j)
                oColumn.Visible = False
                oColumn = oMatrix.Columns.Item("A" & j)
                oColumn.Visible = False
                oColumn = oMatrix.Columns.Item("B" & j)
                oColumn.Visible = False

            Next

            Dim sFromDate As String = oDBDSHeader.GetValue("U_CPeriodFrom", 0)
            Dim sToDate As String = oDBDSHeader.GetValue("U_CPeriodTo", 0)
            Dim sResourceType As String = oDBDSHeader.GetValue("U_ResourceType", 0)
            sQuery = "Select ResCode ,ResName  From ORSC  Where U_RCapacity='Y' AND  ResType ='" + sResourceType + "'"

            Dim rsetVisible_Resource As SAPbobsCOM.Recordset = DoQuery(sQuery)

            rsetVisible_Resource.MoveFirst()
            oDBDSDetail.Clear()
            oMatrix.Clear()

            For i As Integer = 0 To rsetVisible_Resource.RecordCount - 1
                oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                oDBDSDetail.Offset = i
                oDBDSDetail.SetValue("LineID", oDBDSDetail.Offset, i + 1)
                oDBDSDetail.SetValue("U_ResourceNo", oDBDSDetail.Offset, rsetVisible_Resource.Fields.Item("ResCode").Value)
                oDBDSDetail.SetValue("U_RDescription", oDBDSDetail.Offset, rsetVisible_Resource.Fields.Item("ResName").Value)



                sQuery = "SELECT day(DATEADD(DAY,number ,'" + sFromDate + "' )  ) FROM master..spt_values WHERE type = 'P' AND DATEADD(DAY,number ,'" + sFromDate + "') <= '" + sToDate + "'"
                Dim rsetVisible2 As SAPbobsCOM.Recordset = DoQuery(sQuery)
                For index As Integer = 0 To rsetVisible2.RecordCount - 1
                    sColumnName = "A" + Trim(rsetVisible2.Fields.Item(0).Value)
                    oDBDSDetail.SetValue("U_" + sColumnName, oDBDSDetail.Offset, 1440)
                    sColumnName = "B" + Trim(rsetVisible2.Fields.Item(0).Value)
                    oDBDSDetail.SetValue("U_" + sColumnName, oDBDSDetail.Offset, 1440)
                    rsetVisible2.MoveNext()

                Next
                

                rsetVisible_Resource.MoveNext()
            Next

            oMatrix.LoadFromDataSource()
            sQuery = "SELECT day(DATEADD(DAY,number ,'" + sFromDate + "' )  ) FROM master..spt_values WHERE type = 'P' AND DATEADD(DAY,number ,'" + sFromDate + "') <= '" + sToDate + "'"
            Dim rsetVisible As SAPbobsCOM.Recordset = DoQuery(sQuery)
            For i As Integer = 0 To rsetVisible.RecordCount - 1
                sColumnName = "d" + Trim(rsetVisible.Fields.Item(0).Value)
                oMatrix.Columns.Item(sColumnName).ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix.Columns.Item(sColumnName).Visible = True

                sColumnName = "A" + Trim(rsetVisible.Fields.Item(0).Value)
                oMatrix.Columns.Item(sColumnName).ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix.Columns.Item(sColumnName).Visible = True

                sColumnName = "B" + Trim(rsetVisible.Fields.Item(0).Value)
                oMatrix.Columns.Item(sColumnName).ColumnSetting.SumType = SAPbouiCOM.BoColumnSumType.bst_Auto
                oMatrix.Columns.Item(sColumnName).Visible = True


                rsetVisible.MoveNext()
            Next
        Catch ex As Exception
            oApplication.StatusBar.SetText("Set Visible and In-Visible Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmResourceCapacity.Freeze(False)
        End Try
    End Sub
    Public Function FormExist(ByVal FormID As String) As Boolean
        FormExist = False
        For Each uid As SAPbouiCOM.Form In oApplication.Forms
            If uid.UniqueID = FormID Then
                FormExist = True
                Exit For
            End If
        Next
        If FormExist Then
            oApplication.Forms.Item(FormID).Visible = True
            oApplication.Forms.Item(FormID).Select()
        End If
    End Function
#Region "       ... Sub Matrix Plan Details ...        "
    Sub LoadSubMatrixDetails(ByVal Row As Integer, ColumID As String)

        Try

            If FormExist(PlanningDetailsFormID) = False Then
                AddXML(PlanningDetailsXML)
                oApplication.Forms.Item(PlanningDetailsFormID).Select()
            End If
            frmSubPlanningDetailsSubMatrix = oApplication.Forms.Item(PlanningDetailsFormID)
            oDBDSSubPlanDetails = frmSubPlanningDetailsSubMatrix.DataSources.DBDataSources.Item("@AIS_RCP2")
            oMatSubPlanDetails = frmSubPlanningDetailsSubMatrix.Items.Item("submatrix").Specific
            oMatrix.FlushToDataSource()
            'Load Sub Grid

            Dim x2 As Integer = oDBDSMainPlanDetails.Size

            frmSubPlanningDetailsSubMatrix.Freeze(True)

            LoadSubGrid(oMatSubPlanDetails, oDBDSSubPlanDetails, oDBDSMainPlanDetails, Row, ColumID)

            Dim boolDataAvl As Boolean = False
            oMatSubPlanDetails.FlushToDataSource()
            For i As Integer = 0 To oMatSubPlanDetails.VisualRowCount - 1
                Dim x As String = Trim(oDBDSSubPlanDetails.GetValue("U_FrmTime", i))
                If Trim(oDBDSSubPlanDetails.GetValue("U_FrmTime", i)).Equals("") = False Then
                    boolDataAvl = True
                    Exit For
                End If
            Next
            '  Load the details...

            If boolDataAvl = False Then
                oMatSubPlanDetails.Clear()
                oDBDSSubPlanDetails.Clear()
                SetNewLineSubGrid(SubRowID_PlanningDetails, oMatSubPlanDetails, oDBDSSubPlanDetails, Row)
                oMatSubPlanDetails.LoadFromDataSource()
            End If

            boolModelForm = True
            boolModelFormID = PlanningDetailsFormID
        Catch ex As Exception
            oApplication.StatusBar.SetText("Failed to Load  Sub Matrix" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmSubPlanningDetailsSubMatrix.Freeze(False)

        End Try
    End Sub
    Sub SetNewLineSubGrid(ByVal UniqID As Integer, ByVal oMatSubGrid As SAPbouiCOM.Matrix, ByVal oDBDSSubGrid As SAPbouiCOM.DBDataSource, Optional ByVal RowID As Integer = 1, Optional ByVal ColumnUID As String = "", Optional ByVal DefaulFields As String(,) = Nothing)
        Try
            If ColumnUID.Equals("") = False Then
                If oMatSubGrid.Columns.Item(ColumnUID).Cells.Item(RowID).Specific.Value.Equals("") = False And RowID = oMatSubGrid.VisualRowCount Then
                    oMatSubGrid.FlushToDataSource()
                    oMatSubGrid.AddRow()
                    oDBDSSubGrid.InsertRecord(oDBDSSubGrid.Size)
                    oDBDSSubGrid.Offset = oMatSubGrid.VisualRowCount - 1
                    oDBDSSubGrid.SetValue("LineID", oDBDSSubGrid.Offset, oMatSubGrid.VisualRowCount)
                    oDBDSSubGrid.SetValue("U_UniqueID", oDBDSSubGrid.Offset, UniqID)
                    If Not DefaulFields Is Nothing Then
                        For f As Int16 = 0 To DefaulFields.GetLength(0) - 1
                            oDBDSSubGrid.SetValue(DefaulFields(f, 0), oDBDSSubGrid.Offset, DefaulFields(f, 1))
                        Next
                    End If
                    oMatSubGrid.SetLineData(oMatSubGrid.VisualRowCount)
                    oMatSubGrid.FlushToDataSource()
                    oMatSubGrid.LoadFromDataSource()
                End If
            Else
                oMatSubGrid.FlushToDataSource()
                oMatSubGrid.AddRow()
                oDBDSSubGrid.InsertRecord(oDBDSSubGrid.Size)
                oDBDSSubGrid.Offset = oMatSubGrid.VisualRowCount - 1
                oDBDSSubGrid.SetValue("LineID", oDBDSSubGrid.Offset, oMatSubGrid.VisualRowCount)
                oDBDSSubGrid.SetValue("U_UniqueID", oDBDSSubGrid.Offset, UniqID)
                If Not DefaulFields Is Nothing Then
                    For f As Int16 = 0 To DefaulFields.GetLength(0) - 1
                        oDBDSSubGrid.SetValue(DefaulFields(f, 0), oDBDSSubGrid.Offset, DefaulFields(f, 1))
                    Next
                End If
                oMatSubGrid.SetLineData(oMatSubGrid.VisualRowCount)
                oMatSubGrid.FlushToDataSource()
                oMatSubGrid.LoadFromDataSource()
            End If
        Catch ex As Exception
            oApplication.StatusBar.SetText("SetNewLine Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


    Function LoadSubGrid(ByVal oMatSubGrid As SAPbouiCOM.Matrix, ByVal oDBDSSubGrid As SAPbouiCOM.DBDataSource, ByVal oDBDSMainSubGrid As SAPbouiCOM.DBDataSource, ByVal UniqID As Integer, DayID As String, Optional ByVal DefaulFields As String(,) = Nothing) As Boolean
        Try
            oMatSubGrid.Clear()
            oDBDSSubGrid.Clear()

            Dim strGetColUID As String = "Select AliasID From CUFD Where  AliasID <> 'UniqID' AND TableID ='" & oDBDSSubGrid.TableName & "' "
            Dim rsetGetColUID As SAPbobsCOM.Recordset = Me.DoQuery(strGetColUID)
            Dim strColUID As String = ""

            Dim boolUniqID As Boolean = False
            For x As Integer = 0 To oDBDSMainSubGrid.Size - 1
                If Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", x)).Equals(UniqID.ToString) Then
                    boolUniqID = True
                    Exit For
                End If
            Next
            If boolUniqID = False Then
                ' Add the Rows in Sub Grid ...
                SetNewLineSubGrid(UniqID, oMatSubGrid, oDBDSSubGrid, , , DefaulFields)


            ElseIf oDBDSMainSubGrid.Size >= 1 And boolUniqID = True Then
                For i As Integer = 0 To oDBDSMainSubGrid.Size - 1
                    oDBDSMainSubGrid.Offset = i
                    If Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset)).Equals("") = False And oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset).Trim.Equals("") = False Then
                        If CInt(oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset)) = UniqID And DayID = Trim(oDBDSMainSubGrid.GetValue("U_DayID", oDBDSMainSubGrid.Offset)) Then
                            oDBDSSubGrid.InsertRecord(oDBDSSubGrid.Size)
                            oDBDSSubGrid.Offset = oDBDSSubGrid.Size - 1
                            oDBDSSubGrid.SetValue("LineID", oDBDSSubGrid.Offset, oDBDSSubGrid.Offset + 1)
                            oDBDSSubGrid.SetValue("U_UniqueID", oDBDSSubGrid.Offset, UniqID)
                            If Not DefaulFields Is Nothing Then
                                For f As Int16 = 0 To DefaulFields.GetLength(0) - 1
                                    oDBDSSubGrid.SetValue(DefaulFields(f, 0), oDBDSSubGrid.Offset, DefaulFields(f, 1))
                                Next
                            End If
                            rsetGetColUID.MoveFirst()
                            For j As Integer = 0 To rsetGetColUID.RecordCount - 1
                                strColUID = "U_" & rsetGetColUID.Fields.Item(0).Value
                                oDBDSSubGrid.SetValue(strColUID, oDBDSSubGrid.Offset, oDBDSMainSubGrid.GetValue(strColUID, oDBDSMainSubGrid.Offset).Trim)
                                rsetGetColUID.MoveNext()
                            Next
                        End If



                    End If

                Next
                oMatSubGrid.LoadFromDataSource()
                oMatSubGrid.FlushToDataSource()
                ' Add the Rows in Sub Grid ...
                'Me.SetNewLineSubGrid(UniqID, oMatSubGrid, oDBDSSubGrid, , , DefaulFields)
            End If
            Return True
        Catch ex As Exception
            Msg("Global Fun. : Load Sub Grid " & ex.Message)
            Return False
        Finally
        End Try
    End Function

    Function DoQuery(ByVal strSql As String) As SAPbobsCOM.Recordset
        Try
            Dim rsetCode As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            rsetCode.DoQuery(strSql)
            Return rsetCode
        Catch ex As Exception
            oApplication.StatusBar.SetText("Execute Query Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return Nothing
        Finally
        End Try
    End Function

    Function SaveSubGrid(ByVal oMatMainGrid As SAPbouiCOM.Matrix, ByVal oDBDSMainSubGrid As SAPbouiCOM.DBDataSource, ByVal oMatSubGrid As SAPbouiCOM.Matrix, ByVal oDBDsSubGrid As SAPbouiCOM.DBDataSource, ByVal RowID As Integer, SubRowID_DayID As String) As Boolean
        Try
            Dim strGetColUID As String = "Select AliasID From CUFD Where  AliasID <> 'UniqID' AND TableID ='" & oDBDsSubGrid.TableName & "' "
            Dim rsetGetColUID As SAPbobsCOM.Recordset = Me.DoQuery(strGetColUID)
            Dim strColUID As String = ""

            Dim intInitSize As Integer = oDBDSMainSubGrid.Size
            Dim intCurrSize As Integer = oDBDSMainSubGrid.Size
            Dim oEmpty As Boolean = True


            For i As Integer = 0 To intInitSize - 1
                oDBDSMainSubGrid.Offset = i - (intInitSize - intCurrSize)
                Dim aa = oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset).Trim
                If oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset).Trim <> "" Then
                    If CInt(oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset)) = RowID Then
                        oDBDSMainSubGrid.RemoveRecord(oDBDSMainSubGrid.Offset)
                    End If
                ElseIf oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset).Trim.Equals("") Then
                    oDBDSMainSubGrid.RemoveRecord(oDBDSMainSubGrid.Offset)
                End If
                intCurrSize = oDBDSMainSubGrid.Size
            Next

            If oDBDSMainSubGrid.Size = 0 Then
                oDBDSMainSubGrid.InsertRecord(oDBDSMainSubGrid.Size)
                oDBDSMainSubGrid.Offset = 0
            End If

            If oDBDSMainSubGrid.Size = 1 And Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", oDBDSMainSubGrid.Offset)).Equals("") Then
                oEmpty = True
            Else
                oEmpty = False
            End If
            oMatSubGrid.FlushToDataSource()
            oMatSubGrid.LoadFromDataSource()
            For i As Integer = 0 To oMatSubGrid.VisualRowCount - 1
                oDBDsSubGrid.Offset = i
                If oEmpty = True Then
                    oDBDSMainSubGrid.Offset = oDBDSMainSubGrid.Size - 1
                    oDBDSMainSubGrid.SetValue("U_UniqueID", oDBDSMainSubGrid.Offset, RowID)
                    oDBDSMainSubGrid.SetValue("U_DayID", oDBDSMainSubGrid.Offset, SubRowID_DayID)
                    oDBDSMainSubGrid.SetValue("LineID", oDBDSMainSubGrid.Offset, oDBDSMainSubGrid.Size)
                    rsetGetColUID.MoveFirst()
                    For j As Integer = 0 To rsetGetColUID.RecordCount - 1
                        strColUID = "U_" & rsetGetColUID.Fields.Item(0).Value
                        oDBDSMainSubGrid.SetValue(strColUID, oDBDSMainSubGrid.Offset, oDBDsSubGrid.GetValue(strColUID, i).Trim)
                        rsetGetColUID.MoveNext()
                    Next
                    If i <> (oMatSubGrid.VisualRowCount - 1) Then
                        oDBDSMainSubGrid.InsertRecord(oDBDSMainSubGrid.Size)
                    End If
                ElseIf oEmpty = False Then

                    oDBDSMainSubGrid.InsertRecord(oDBDSMainSubGrid.Size)
                    oDBDSMainSubGrid.Offset = oDBDSMainSubGrid.Size - 1
                    oDBDSMainSubGrid.SetValue("U_UniqueID", oDBDSMainSubGrid.Offset, RowID)
                    oDBDSMainSubGrid.SetValue("U_DayID", oDBDSMainSubGrid.Offset, SubRowID_DayID)
                    oDBDSMainSubGrid.SetValue("LineID", oDBDSMainSubGrid.Offset, oDBDSMainSubGrid.Size)
                    rsetGetColUID.MoveFirst()
                    For j As Integer = 0 To rsetGetColUID.RecordCount - 1
                        strColUID = "U_" & rsetGetColUID.Fields.Item(0).Value
                        oDBDSMainSubGrid.SetValue(strColUID, oDBDSMainSubGrid.Offset, oDBDsSubGrid.GetValue(strColUID, i).Trim)
                        rsetGetColUID.MoveNext()
                    Next
                End If
            Next
            oMatMainGrid.LoadFromDataSource()

            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Save Sub Grid Method Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        Finally
        End Try
    End Function



    Function Validation_SubGrid() As Boolean
        Try
            'oMatSubPlanSetails.FlushToDataSource()
            'For i As Integer = 1 To oMatSubPlanSetails.VisualRowCount - 1
            '    Dim Type As String = oMatSubPlanSetails.Columns.Item("type").Cells.Item(i).Specific.Value
            '    If Type.Equals("LV") Then
            '        If oMatSubPlanSetails.Columns.Item("lvecod").Cells.Item(i).Specific.Value() = "" Then
            '            oGFun.Msg("Plan Details Line :" & i & " : Leave Code Should Not Left Empty !!!")
            '        End If
            '    End If
            'Next

            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText("SubGrid Validation Failed" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Function
    Sub DeleteRowSubGrid(ByVal oMatMainSubGrid As SAPbouiCOM.Matrix, ByVal oDBDSMainSubGrid As SAPbouiCOM.DBDataSource, ByVal SubRowID As Integer)
        Try
            oMatMainSubGrid.FlushToDataSource()
            For i As Integer = 0 To oDBDSMainSubGrid.Size - 1
                If i <= oDBDSMainSubGrid.Size - 1 Then
                    oDBDSMainSubGrid.Offset = i
                    oDBDSMainSubGrid.SetValue("LineID", oDBDSMainSubGrid.Offset, i + 1)

                    If Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", i)).Equals("") = False Then
                        Dim aa = Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", i))
                        If CDbl(oDBDSMainSubGrid.GetValue("U_UniqueID", i)) = SubRowID Then
                            If Trim(oDBDSMainSubGrid.GetValue("U_UniqueID", i)).Equals(SubRowID.ToString().Trim) Then
                                oDBDSMainSubGrid.RemoveRecord(i)
                                i -= 1
                            Else
                                oDBDSMainSubGrid.SetValue("U_UniqueID", oDBDSMainSubGrid.Offset, CDbl(oDBDSMainSubGrid.GetValue("U_UniqueID", i)) - 1)
                            End If
                        End If
                    Else
                        oDBDSMainSubGrid.RemoveRecord(i)
                        i -= 1
                    End If
                End If
            Next
            oMatMainSubGrid.LoadFromDataSource()
        Catch ex As Exception
            oApplication.StatusBar.SetText("Delete Row SubGrid Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub ItemEvent_SubGrid(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "add"
                                If pVal.BeforeAction Then
                                    If Me.Validation_SubGrid = False Then BubbleEvent = False
                                ElseIf pVal.BeforeAction = False Then
                                    If frmResourceCapacity.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                        DeleteRowSubGrid(oMatMainplanDetails, oDBDSMainPlanDetails, SubRowID_PlanningDetails)
                                    End If
                                    If SaveSubGrid(oMatMainplanDetails, oDBDSMainPlanDetails, oMatSubPlanDetails, oDBDSSubPlanDetails, SubRowID_PlanningDetails, SubRowID_DayID) Then
                                        Dim strFTim As String = ""
                                        For i As Integer = 1 To oMatMainplanDetails.RowCount
                                            Dim s As Integer = oMatMainplanDetails.Columns.Item("UID").Cells.Item(i).Specific.Value
                                            If SubRowID_PlanningDetails = oMatMainplanDetails.Columns.Item("UID").Cells.Item(i).Specific.Value Then
                                                If i <> oMatMainplanDetails.RowCount Then
                                                    strFTim = strFTim + oMatMainplanDetails.Columns.Item("FTim").Cells.Item(i).Specific.Value + ","
                                                End If
                                            End If
                                        Next

                                        'oDBDSDetail.SetValue("U_D1", SubRowID_DayID - 1, strFTim.TrimEnd(","))
                                        'oDBDSDetail.SetValue("U_D1", SubRowID_PlanningDetails - 1, strFTim.TrimEnd(","))
                                        oMatrix.LoadFromDataSource()
                                        frmSubPlanningDetailsSubMatrix.Close()
                                        If frmResourceCapacity.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmResourceCapacity.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                                    End If
                                End If
                        End Select
                    Catch ex As Exception
                        Msg("Click Event Failed : " & ex.Message)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing And pVal.BeforeAction = False And frmSubPlanningDetailsSubMatrix.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            Select Case pVal.ItemUID
                                Case "submatrix"
                                    Select Case pVal.ColUID

                                    End Select
                                    Select Case pVal.ColUID

                                    End Select
                            End Select
                        End If
                    Catch ex As Exception
                        Msg("Sub-grid Cost Center : Item event failed : " & ex.Message)
                    Finally
                    End Try
                    'Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    '    Try
                    '        Select Case pVal.ItemUID
                    '            Case "submatrix"
                    '                Select Case pVal.ColUID
                    '                    Case "Dayoff"


                    '                End Select
                    '        End Select
                    '    Catch ex As Exception
                    '        Msg("Item Pressed  Event Failed : " & ex.Message)
                    '    Finally
                    '    End Try
                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "submatrix"
                                Select Case pVal.ColUID

                                End Select
                        End Select
                    Catch ex As Exception
                        Msg("matrix link pressed Event Failed : " & ex.Message)
                    Finally
                    End Try
                    'Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    '    Try
                    '        Select Case pVal.ItemUID
                    '            Case "submatrix"
                    '                Select Case pVal.ColUID
                    '                    Case "type"
                    '                End Select
                    '        End Select
                    '    Catch ex As Exception
                    '        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    '    Finally
                    '    End Try
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "submatrix"
                                Select Case pVal.ColUID
                                    Case "FTim"
                                        SetNewLineSubGrid(SubRowID_PlanningDetails, oMatSubPlanDetails, oDBDSSubPlanDetails, pVal.Row, pVal.ColUID)
                                End Select
                        End Select
                    Catch ex As Exception
                        Msg("Lost Focus Event  : " & ex.Message)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("SubGrid Item Event Failed" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub

#End Region
    Private Shared Function Num(ByVal value As String) As Integer
        Dim returnVal As String = String.Empty
        Dim collection As MatchCollection = Regex.Matches(value, "\d+")
        For Each m As Match In collection
            returnVal += m.ToString()
        Next
        Return Convert.ToInt32(returnVal)
    End Function
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Try
                        If BusinessObjectInfo.ActionSuccess Then


                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add ,Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        AutoSumEnable()
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Function ValidateAll() As Boolean
        Try
            'Hearder Level Validation


            ValidateAll = True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

    Sub RightClickEvent(ByRef EventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case EventInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK
                    If oApplication.Forms.Item(EventInfo.FormUID).Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And EventInfo.BeforeAction Then
                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

End Class

