

Alter Procedure [dbo].[@AIS_PPC_GetStockPlannedItemDetails](@ScenarioCode Varchar(50))
AS
Begin 
Select T1.U_MacCode,U_FT,T1.DocEntry, T0.DocNum,T0.Object ,T1.LineId ,T1.U_CardCode as CardCode,  T4.ItemCode as U_ItemCode,T4.FrgnName as  U_ItemName  ,
T2.U_Allocated  as U_Allocated ,T1.U_ChangeOverFil,T1.U_PreFilType
,T1.U_WhsCode,T1.U_WhsName,T3.CardName ,T1.U_MacCode,T1.U_MacName,U_ReqDate  
,T1.U_ItemCode as PItemCode,T1.U_ItemName as PItemName,
U_PostingDate,
U_PBaseEntry,U_PBaseNum,U_PBaseLine,T1.U_Remarks   From [@AIS_OSAV] T0 Inner Join [@AIS_SAV1] T1 on T0.DocEntry =T1.DocEntry 
Inner Join [@AIS_SAV2] T2 on T2.DocEntry =T0.DocEntry   and T2.U_FGItemCode = T1.U_ItemCode and T1.U_ChangeOverFil =T2.U_ChangeOverFil
and T1.U_CardCode =T2.U_CustCode  
Inner Join OCRD T3 on T3.CardCode =T1.U_CardCode 
Inner Join OITM T4 On T4.U_GN  =T1.U_ItemName   And U_FT =T1.U_ChangeOverFil  AND T4.U_MC =T1.U_MacCode
Where T0.U_ScenarioCode =@ScenarioCode 

END 

 