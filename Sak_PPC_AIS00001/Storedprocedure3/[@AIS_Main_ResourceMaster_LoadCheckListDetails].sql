Alter procedure [dbo].[@AIS_Main_ResourceMaster_LoadCheckListDetails] (@Type Varchar(max),@MCCode Varchar(50))
as
begin

select  U_Task as Task,'' [Last Update Date],'' [Default Service],'' as Remarks,
'' as Action,'' as Duration,'' as Frequency,'' as [Prepared Date],
'' as [Schedule From Time],'' as [Schedule To Time]  
   from [@AIS_OCLM] T0 Inner Join [@AIS_CLM1] T1 On T0.DocEntry =T1.DocEntry Where T0.U_Type  =@Type
    AND T0.U_MCCode = @MCCode AND Isnull(U_Task,'')!='' AND T1.U_Active='Y'

end 