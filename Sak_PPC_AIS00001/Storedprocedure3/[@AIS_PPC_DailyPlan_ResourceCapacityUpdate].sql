 

--EXEC [@AIS_PPC_DailyPlan_ResourceCapacityUpdate] '1'

Alter Procedure [@AIS_PPC_DailyPlan_ResourceCapacityUpdate](@DocEntry Int)
As
Begin
 
DECLARE @U_MCCode VARCHAR(50)  
DECLARE @U_ReqHours VARCHAR(256)  
DECLARE @U_FromDate VARCHAR(256) 
DECLARE @U_ToDate VARCHAR(20)  


DECLARE db_cursor CURSOR FOR  Select T1.U_MCCode ,T1.U_ReqHours ,T1.U_FromDate ,T1.U_ToDate  From [@AIS_ODWP] T0 Inner Join [@AIS_DWP1] T1 On T0.DocEntry =T1.DocEntry  Where  T0.DocEntry =@DocEntry
 

OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @U_MCCode,   @U_ReqHours,@U_FromDate,@U_ToDate  

WHILE @@FETCH_STATUS = 0   
BEGIN   




DECLARE @sqlCommand varchar(1000)
Declare @thedate varchar(max)


DECLARE db_cursor_inner CURSOR FOR    SELECT thedate FROM dbo.ExplodeDates(@U_FromDate  ,@U_ToDate)  
OPEN db_cursor_inner   
FETCH NEXT FROM db_cursor_inner INTO @thedate  
 
 WHILE @@FETCH_STATUS = 0   
BEGIN   

Set @sqlCommand ='Update T1 Set   U_D'+Convert(varchar(max),day(@thedate))+' ='+'1'+'    from [@AIS_ORCP] T0 Inner Join [@AIS_RCP1] T1 On T0.DocEntry =T1.DocEntry Where T1.U_ResourceNo = '''+@U_MCCode +'''' 
select @sqlCommand
EXEC (@sqlCommand) 
 FETCH NEXT FROM db_cursor_inner INTO @thedate  
END   

CLOSE db_cursor_inner   
DEALLOCATE db_cursor_inner



      
	  
	   FETCH NEXT FROM db_cursor INTO @U_MCCode,   @U_ReqHours,@U_FromDate,@U_ToDate     
END   

CLOSE db_cursor   
DEALLOCATE db_cursor





END 