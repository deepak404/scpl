
Alter Procedure [@AIS_ResourceMaster_UpdateFrequencyDate] (@DocEntry Int)
As
Begin
Declare @MCCode Varchar(50)

Select @MCCode=T1.U_MCCode from [@AIS_RSC11] T0
inner join [@AIS_OPMA] T1 on T1.U_MCCode=T0.U_BaseNum Where T1.DocEntry=@DocEntry

UPDATE  [@AIS_RSC9] SET U_PreparedDate=DateAdd(Day,U_FrequencyDay,U_PreparedDate) where U_BaseNum=@MCCOde and U_PreparedDate!='' And 
U_Frequency='D'
UPDATE  [@AIS_RSC9] SET U_PreparedTDate=DateAdd(Day,U_FrequencyDay,U_PreparedTDate) where U_BaseNum=@MCCOde and U_PreparedTDate!=''And 
U_Frequency='D'
UPDATE  [@AIS_RSC12] SET U_PreparedDate=DateAdd(Day,U_FrequencyDay,U_PreparedDate) where U_BaseNum=@MCCOde And U_PreparedDate!=''And 
U_Frequency='D'
UPDATE  [@AIS_RSC12] SET U_PreparedTDate=DateAdd(Day,U_FrequencyDay,U_PreparedTDate) where U_BaseNum=@MCCOde And U_PreparedTDate!=''And 
U_Frequency='D'
UPDATE  [@AIS_RSC11] SET U_PreparedDate=DateAdd(Day,U_FrequencyDay,U_PreparedDate) where U_BaseNum=@MCCOde And U_PreparedDate!=''And 
U_Frequency='D'
UPDATE  [@AIS_RSC11] SET U_PreparedTDate=DateAdd(Day,U_FrequencyDay,U_PreparedTDate) where U_BaseNum=@MCCOde And U_PreparedTDate!=''And 
U_Frequency='D'


UPDATE  [@AIS_RSC9] SET U_PreparedDate=DateAdd(MONTH,U_FrequencyDay,U_PreparedDate) where U_BaseNum=@MCCOde and U_PreparedDate!='' And 
U_Frequency='M'
UPDATE  [@AIS_RSC9] SET U_PreparedTDate=DateAdd(MONTH,U_FrequencyDay,U_PreparedTDate) where U_BaseNum=@MCCOde and U_PreparedTDate!=''And 
U_Frequency='M'
UPDATE  [@AIS_RSC12] SET U_PreparedDate=DateAdd(MONTH,U_FrequencyDay,U_PreparedDate) where U_BaseNum=@MCCOde And U_PreparedDate!='' And 
U_Frequency='M'
UPDATE  [@AIS_RSC12] SET U_PreparedTDate=DateAdd(MONTH,U_FrequencyDay,U_PreparedTDate) where U_BaseNum=@MCCOde And U_PreparedTDate!='' And 
U_Frequency='M'
UPDATE  [@AIS_RSC11] SET U_PreparedDate=DateAdd(MONTH,U_FrequencyDay,U_PreparedDate) where U_BaseNum=@MCCOde And U_PreparedDate!='' And 
U_Frequency='M'

UPDATE  [@AIS_RSC11] SET U_PreparedTDate=DateAdd(MONTH,U_FrequencyDay,U_PreparedTDate) where U_BaseNum=@MCCOde And U_PreparedTDate!=''And 
U_Frequency='M'

UPDATE  [@AIS_RSC9] SET U_PreparedDate=DateAdd(YEAR,U_FrequencyDay,U_PreparedDate) where U_BaseNum=@MCCOde and U_PreparedDate!='' And U_Frequency='Y'
UPDATE  [@AIS_RSC9] SET U_PreparedTDate=DateAdd(YEAR,U_FrequencyDay,U_PreparedTDate) where U_BaseNum=@MCCOde and U_PreparedTDate!='' And U_Frequency='Y'
UPDATE  [@AIS_RSC12] SET U_PreparedDate=DateAdd(YEAR,U_FrequencyDay,U_PreparedDate) where U_BaseNum=@MCCOde And U_PreparedDate!='' And U_Frequency='Y'
UPDATE  [@AIS_RSC12] SET U_PreparedTDate=DateAdd(YEAR,U_FrequencyDay,U_PreparedTDate) where U_BaseNum=@MCCOde And U_PreparedTDate!=''And U_Frequency='Y'
UPDATE  [@AIS_RSC11] SET U_PreparedDate=DateAdd(YEAR,U_FrequencyDay,U_PreparedDate) where U_BaseNum=@MCCOde And U_PreparedDate!='' And U_Frequency='Y'
UPDATE  [@AIS_RSC11] SET U_PreparedTDate=DateAdd(YEAR,U_FrequencyDay,U_PreparedTDate) where U_BaseNum=@MCCOde And U_PreparedTDate!='' And U_Frequency='Y'
end
