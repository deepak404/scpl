Create Procedure [dbo].[@AIS_ResourceMaster_UpdateRSCAction] (@DocEntry Int)
As
Begin
Declare @MCCode Varchar(50)


UPDATE T0  SET T0.U_Action=T2.U_Action ,T0.U_Remarks=T2.U_Remarks ,T0.U_Duration =t2.U_Duration  From [@AIS_RSC9] T0
inner join [@AIS_OPMA] T1 on T1.U_MCCode =T0.U_BaseNum
inner join [@AIS_PMA1] t2 on T2.DocEntry=T1.DocEntry  Where T1.DocEntry=@DocEntry
 

UPDATE T0  SET T0.U_Action=T2.U_Action ,T0.U_Remarks=T2.U_Remarks ,T0.U_Duration =t2.U_Duration  From [@AIS_RSC11] T0
inner join [@AIS_OPMA] T1 on T1.U_MCCode =T0.U_BaseNum
inner join [@AIS_PMA1] t2 on T2.DocEntry=T1.DocEntry  Where T1.DocEntry=@DocEntry

UPDATE T0  SET T0.U_Action=T2.U_Action ,T0.U_Remarks=T2.U_Remarks ,T0.U_Duration =t2.U_Duration  From [@AIS_RSC12] T0
inner join [@AIS_OPMA] T1 on T1.U_MCCode =T0.U_BaseNum
inner join [@AIS_PMA1] t2 on T2.DocEntry=T1.DocEntry  Where T1.DocEntry=@DocEntry

End


