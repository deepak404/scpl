﻿Public Class APInvoice

    Dim frmAPInvoice As SAPbouiCOM.Form
    Dim oDBDSHeader As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim frmid As String = ""
    Sub LoadForm()
        Try
            frmAPInvoice = oApplication.Forms.GetFormByTypeAndCount("141", 1)
            FrmId = frmAPInvoice.UniqueID

            oDBDSHeader = frmAPInvoice.DataSources.DBDataSources.Item(0)
            oDBDSDetail = frmAPInvoice.DataSources.DBDataSources.Item(1)
            oMatrix = frmAPInvoice.Items.Item("38").Specific
            Me.DefineModesForFields()
            Me.InitForm()
        Catch ex As Exception
            oGFun.Msg("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmAPInvoice.Freeze(True)
           
        Catch ex As Exception
            oGFun.Msg("InitForm Method Failed:" & ex.Message)
        Finally
            frmAPInvoice.Freeze(False)
        End Try
    End Sub

    Sub DefineModesForFields()
        Try
        Catch ex As Exception
            oApplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function ValidateAll() As Boolean
        Try
            ValidateAll = True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
                    Try
                        If pVal.BeforeAction = False Then
                            Me.LoadForm()
                        End If
                    Catch ex As Exception
                        oGFun.Msg("Form Load Event Failed:" & ex.Message)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        'Assign Selected Rows
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        '        'Filter before open the CFL
                        If pVal.BeforeAction Then
                            Select Case oCFLE.ChooseFromListUID
                                Case "12"

                                    Dim strSql = " select DocNum from ( select DocNum , PDN1.ItemCode , SUM(isnull(PDN1.Quantity,0)) - " & _
                                              " isnull((Select SUM(isnull(OINS.U_InsQty,0))  from [@MIPL_OINS]  OINS where " & _
                                              " OPDN.DocNum =OINS.U_GRNNo and PDN1.ItemCode =OINS.U_ItemCode ),0) Qty from " & _
                                              " OPDN,PDN1 where OPDN.DocStatus ='O' and OPDN.Doctype ='I' and OPDN.DocEntry =PDN1.DocEntry and " & _
                                              " PDN1.ItemCode in (Select Itemcode from OITM where isnull(U_InsReq,'N') = 'Y' and PDN1.U_WhsCode Is not Null) " & _
                                              " Group By OPDN.DocNum , PDN1.ItemCode ) AA where  Qty > 0  "

                                    strSql = strSql + " Union All select isnull(U_GRNNo,'') from [@MIPL_OINS] where isnull(U_RejQty,0) >0 and ISNULL(U_Return,'') = '' "

                                    strSql = " Select Distinct DocNum from OPDN where DocNum Not In ( " & strSql & ")"

                                    oGFun.ChooseFromListFilteration(frmAPInvoice, "12", "DocNum", strSql)

                            End Select
                        End If

                    Catch ex As Exception
                        oGFun.Msg("Choose from List Event Failed:" & ex.Message)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    'Select Case pVal.ItemUID
                    '    Case "10000330"
                    '        If pVal.BeforeAction = False Then
                    '            Dim ocmb As SAPbouiCOM.ButtonCombo = frmAPInvoice.Items.Item("10000330").Specific

                    '            Dim ss = ocmb.Selected.Value

                    '            If ocmb.Selected.Description = "20" And frmAPInvoice.Items.Item("3").Specific.Selected.Value.ToString.Trim = "I" Then
                    '                Dim DocNum As String = frmAPInvoice.Items.Item("8").Specific.Value
                    '                Dim strSql = " select Count(*) from ( select OPDN.DocNum , PDN1.ItemCode , SUM(isnull(PDN1.Quantity,0)) - " & _
                    '                       " isnull((Select SUM(isnull(OINS.U_InsQty,0))  from [@MIPL_OINS]  OINS where " & _
                    '                       " OPDN.DocNum =OINS.U_GRNNo and PDN1.ItemCode =OINS.U_ItemCode ),0) Qty from " & _
                    '                       " OPDN,PDN1 where OPDN.DocStatus ='O' and OPDN.DocEntry =PDN1.DocEntry and " & _
                    '                       " PDN1.ItemCode in (Select Itemcode from OITM where isnull(U_InsReq,'N') = 'Y' and PDN1.U_WhsCode Is not Null) " & _
                    '                       " Group By OPDN.DocNum , PDN1.ItemCode ) AA where  Qty > 0 AND DocNum = '" & DocNum & "' "
                    '                Dim count = oGFun.getSingleValue(strSql)
                    '                count = IIf(count.Trim = "", 0, count)
                    '                If CInt(count) > 0 Then
                    '                    oGFun.Msg("Inspection Not Yet Completed..")
                    '                    oApplication.Forms.ActiveForm.Close()
                    '                End If

                    '                strSql = "select DocNum from [@MIPL_OINS] where isnull(U_RejQty,0) >0 and ISNULL(U_Return,'') = '' and isnull(U_GRNNo,'') = '" & DocNum & "'"
                    '                Dim InsNo = oGFun.getSingleValue(strSql)

                    '                If InsNo.Trim <> "" Then
                    '                    oGFun.Msg("Inspection DocNum : " & InsNo & ", Return GRN Not Yet Completed..")
                    '                    oApplication.Forms.ActiveForm.Close()
                    '                Else
                    '                    oAPInvoice.LoadForm()
                    '                End If
                    '            End If
                    '        End If
                    'End Select
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
                    Try
                        If BusinessObjectInfo.BeforeAction Then
                            If Me.ValidateAll() = False Then
                                System.Media.SystemSounds.Asterisk.Play()
                                BubbleEvent = False
                                Exit Sub
                            Else
                            End If
                        End If
                        If BusinessObjectInfo.BeforeAction = False And BusinessObjectInfo.ActionSuccess Then
                        End If
                    Catch ex As Exception
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD

                    If BusinessObjectInfo.ActionSuccess Then
                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

End Class
