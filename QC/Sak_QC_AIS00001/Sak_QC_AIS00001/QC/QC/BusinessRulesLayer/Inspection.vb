﻿Public Class Inspection

    Public frmInspection As SAPbouiCOM.Form
    Dim ocombobox As SAPbouiCOM.ComboBox
    Dim oDBDSHeader As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail1 As SAPbouiCOM.DBDataSource
    Dim ODBDSDetail2 As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail3 As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail4 As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail5 As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail6 As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail7 As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail8 As SAPbouiCOM.DBDataSource

    Dim oMatrix As SAPbouiCOM.Matrix
    Dim oMatrix2 As SAPbouiCOM.Matrix
    Dim oMatrix3 As SAPbouiCOM.Matrix
    Dim oMatrix4 As SAPbouiCOM.Matrix
    Dim oMatrix5 As SAPbouiCOM.Matrix
    Dim oMatrix6 As SAPbouiCOM.Matrix
    Dim oMatrix7 As SAPbouiCOM.Matrix
    Dim oMatrix8 As SAPbouiCOM.Matrix
    Dim UDOID As String = "INS"

    Dim StrSql, StrSeries As String
    Dim oSt_txt, oValues As SAPbouiCOM.RowHeaders
    Dim ofolder As SAPbouiCOM.Folder

    Sub LoadForm()
        Try
            oGFun.LoadXML(frmInspection, InspectionFormID, InspectionXML)
            frmInspection = oApplication.Forms.Item(InspectionFormID)
            oDBDSHeader = frmInspection.DataSources.DBDataSources.Item("@MIPL_OINS")
            oDBDSDetail1 = frmInspection.DataSources.DBDataSources.Item("@MIPL_INS1")
            ODBDSDetail2 = frmInspection.DataSources.DBDataSources.Item("@MIPL_INS2")
            oDBDSDetail3 = frmInspection.DataSources.DBDataSources.Item("@MIPL_INS3")
            oDBDSDetail4 = frmInspection.DataSources.DBDataSources.Item("@MIPL_INS4")
            oDBDSDetail5 = frmInspection.DataSources.DBDataSources.Item("@MIPL_INS5")
            oDBDSDetail6 = frmInspection.DataSources.DBDataSources.Item("@MIPL_INS6")
            oDBDSDetail7 = frmInspection.DataSources.DBDataSources.Item("@MIPL_INS7")
            oDBDSDetail8 = frmInspection.DataSources.DBDataSources.Item("@MIPL_INS8")

            oMatrix = frmInspection.Items.Item("55").Specific
            oMatrix2 = frmInspection.Items.Item("56").Specific
            oMatrix3 = frmInspection.Items.Item("Matrix3").Specific
            oMatrix4 = frmInspection.Items.Item("Matrix6").Specific
            oMatrix5 = frmInspection.Items.Item("Batch").Specific
            oMatrix6 = frmInspection.Items.Item("MC").Specific
            oMatrix7 = frmInspection.Items.Item("FG").Specific
            oMatrix8 = frmInspection.Items.Item("DNo").Specific
            Me.InitForm()
            Me.DefineModesForFields()

            frmInspection.Items.Item("18").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
        Catch ex As Exception
            oGFun.Msg("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmInspection.Freeze(True)
            oGFun.LoadComboBoxSeries(frmInspection.Items.Item("c_series").Specific, UDOID)
            oGFun.setDocNum(frmInspection)
            oGFun.setComboBoxValue(frmInspection.Items.Item("95").Specific, "select Code,Location from OLCT")
            oGFun.setComboBoxValue(frmInspection.Items.Item("130").Specific, "select code ,Name from [@AIS_WrkTyp]")
            If frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                oGFun.LoadDocumentDate(frmInspection.Items.Item("116").Specific)
            End If

            TypeWiseControlAlloc()

            oMatrix.Clear()
            oMatrix2.Clear()
            oMatrix3.Clear()
            oMatrix4.Clear()
            oMatrix5.Clear()
            oMatrix6.Clear()
            oMatrix7.Clear()
            oMatrix8.Clear()



            oGFun.SetNewLine(oMatrix, oDBDSDetail1)
            oGFun.SetNewLine(oMatrix2, ODBDSDetail2)
            oGFun.SetNewLine(oMatrix3, oDBDSDetail3)
            oGFun.SetNewLine(oMatrix4, oDBDSDetail4)
            oGFun.SetNewLine(oMatrix5, oDBDSDetail5)
            oGFun.SetNewLine(oMatrix6, oDBDSDetail6)
            oGFun.SetNewLine(oMatrix7, oDBDSDetail7)
            oGFun.SetNewLine(oMatrix8, oDBDSDetail8)
            Dim oCombo1 As SAPbouiCOM.ComboBox
            oCombo1 = frmInspection.Items.Item("130").Specific
            oGFun.setComboBoxValue(oCombo1, "select * from [@AIS_WrkTyp]")

        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmInspection.Freeze(False)

        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Sub DefineModesForFields()
        Try
            frmInspection.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmInspection.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 3, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            frmInspection.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmInspection.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 3, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmInspection.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            frmInspection.Items.Item("b_Print").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmInspection.Items.Item("b_Print").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 3, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmInspection.Items.Item("b_Print").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmInspection.Items.Item("b_Print").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            frmInspection.Items.Item("148").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            Dim oCmbSerial As SAPbouiCOM.ComboBox = frmInspection.Items.Item("c_Dev").Specific
            Dim strSerialCode As String = oCmbSerial.Value.ToString.Trim
            If strSerialCode.ToUpper.Trim = "N" Or strSerialCode.ToUpper.Trim = "" Then
                frmInspection.Items.Item("btnDev").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            Else
                frmInspection.Items.Item("btnDev").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            End If


        Catch ex As Exception
            oApplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function Valid(dFromValue As String, dToValue As String, dObservationValue As String, sType As String) As Boolean
        Dim sBoole As Boolean = False
        Dim sQuery = " EXEC [dbo].[@AIS_QC_InspectionQC] '" & dFromValue & "','" & dToValue & "','" & dObservationValue & "','" & sType & "'"
        Dim rsetEmpDets As SAPbobsCOM.Recordset = oGFun.DoQuery(sQuery)
        rsetEmpDets.MoveFirst()
        For icount = 1 To rsetEmpDets.RecordCount
            If rsetEmpDets.Fields.Item("U_FgCode").Value > 1 Then
                sBoole = True
            Else
                sBoole = False
                Exit For
            End If
        Next

        Return sBoole
    End Function

    Function ValidateAll() As Boolean
        Try

            Dim dFromValue As Double = 0
            Dim dToValue As Double = 0

            Dim dCorrelation As Double = 0
            Dim sFromValue As String = String.Empty
            Dim sToValue As String = String.Empty
            Dim dObservationValue As Double = 0
            Dim dBatchNo As String = String.Empty

            Dim sObservationText As String = String.Empty
            Dim sBoole As Boolean = False

            Dim U_Type As String = oDBDSHeader.GetValue("U_Type", 0).ToString.Trim
            Dim U_Deviation As String = oDBDSHeader.GetValue("U_Deviation", 0).ToString.Trim

            Dim sCount As Integer
            If frmInspection.Items.Item("95").Specific.value.ToString.Trim.Equals("") = True Then
                oGFun.Msg("Location Should Not Be Left Empty")
                Return False
            End If
            If frmInspection.Items.Item("48").Specific.value.ToString.Trim.Equals("36") = True Then
                oGFun.Msg("Prepared by Code Should Not Be Left Empty")
                Return False
            End If
            If U_Deviation = "N" Then
                If U_Type = "IN" Then

                    oMatrix7 = frmInspection.Items.Item("FG").Specific
                    For i As Integer = 1 To oMatrix7.VisualRowCount
                        If i = 1 Then
                            sBoole = True
                        End If
                        Dim oBatchSerialType As SAPbouiCOM.ComboBox = oMatrix7.Columns.Item("Stype").Cells.Item(i).Specific
                        Dim NoOfObser As Double = Convert.ToDouble(oMatrix7.Columns.Item("NoOfObser").Cells.Item(i).Specific.value)
                        Dim ocmbSelected As SAPbouiCOM.CheckBox = oMatrix7.Columns.Item("Selected").Cells.Item(i).Specific

                        If ocmbSelected.Checked = True Then



                            If oBatchSerialType.Selected.Value = "V" Then

                                If oMatrix7.Columns.Item("CR").Cells.Item(i).Specific.string <> String.Empty Then
                                    dCorrelation = Convert.ToDouble(oMatrix7.Columns.Item("CR").Cells.Item(i).Specific.string)
                                Else
                                    dCorrelation = 0
                                End If


                                If oMatrix7.Columns.Item("8").Cells.Item(i).Specific.string <> String.Empty Then
                                    dFromValue = Convert.ToDouble(oMatrix7.Columns.Item("8").Cells.Item(i).Specific.string)
                                Else
                                    dFromValue = 0
                                End If

                                If oMatrix7.Columns.Item("9").Cells.Item(i).Specific.string <> String.Empty Then
                                    dToValue = Convert.ToDouble(oMatrix7.Columns.Item("9").Cells.Item(i).Specific.string)
                                Else
                                    dToValue = 0
                                End If



                                If oMatrix7.Columns.Item("ob1").Cells.Item(i).Specific.string <> String.Empty Then
                                    dObservationValue = oMatrix7.Columns.Item("ob1").Cells.Item(i).Specific.string
                                Else
                                    dObservationValue = -1
                                End If


                                If oMatrix7.Columns.Item("b1").Cells.Item(i).Specific.string <> String.Empty Then
                                    dBatchNo = oMatrix7.Columns.Item("b1").Cells.Item(i).Specific.string
                                Else
                                    dBatchNo = String.Empty
                                End If

                                dObservationValue = dObservationValue + dCorrelation
                                If sBoole = True Then
                                    If NoOfObser > 0 Then
                                        NoOfObser = NoOfObser - 1
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 1 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If

                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 1  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If

                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If






                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob2").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix7.Columns.Item("ob2").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If


                                    If oMatrix7.Columns.Item("b2").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b2").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If

                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 2 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 2  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If




                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob3").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix7.Columns.Item("ob3").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix7.Columns.Item("b3").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b3").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then

                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 3 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 3  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If

                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob4").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix7.Columns.Item("ob4").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If

                                    If oMatrix7.Columns.Item("b4").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b4").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If


                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 4 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 4  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If



                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob5").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix7.Columns.Item("ob5").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix7.Columns.Item("b5").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b5").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 5 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 5  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If



                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob6").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix7.Columns.Item("ob6").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix7.Columns.Item("b6").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b6").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation

                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 6 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 6  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If

                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob7").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix7.Columns.Item("ob7").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix7.Columns.Item("b7").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b7").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 7 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 7  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If

                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob8").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix7.Columns.Item("ob8").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix7.Columns.Item("b8").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b8").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 8 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 8  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If
                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob9").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix7.Columns.Item("ob9").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix7.Columns.Item("b9").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b9").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 9 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 9  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If
                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob10").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix7.Columns.Item("ob10").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix7.Columns.Item("b10").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b10").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 10 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 10  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If


                            Else


                                If oMatrix7.Columns.Item("8").Cells.Item(i).Specific.string <> String.Empty Then
                                    sFromValue = (oMatrix7.Columns.Item("8").Cells.Item(i).Specific.string)
                                Else
                                    sFromValue = ""
                                End If

                                If oMatrix7.Columns.Item("9").Cells.Item(i).Specific.string <> String.Empty Then
                                    sToValue = (oMatrix7.Columns.Item("9").Cells.Item(i).Specific.string)
                                Else
                                    sToValue = ""
                                End If



                                If oMatrix7.Columns.Item("ob1").Cells.Item(i).Specific.string <> String.Empty Then
                                    sObservationText = oMatrix7.Columns.Item("ob1").Cells.Item(i).Specific.string
                                Else
                                    sObservationText = ""
                                End If

                                If oMatrix7.Columns.Item("b1").Cells.Item(i).Specific.string <> String.Empty Then
                                    dBatchNo = oMatrix7.Columns.Item("b1").Cells.Item(i).Specific.string
                                Else
                                    dBatchNo = String.Empty
                                End If


                                If sBoole = True Then
                                    If NoOfObser > 0 Then
                                        NoOfObser = NoOfObser - 1
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 1 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 1  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If






                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob2").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix7.Columns.Item("ob2").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If

                                    If oMatrix7.Columns.Item("b2").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b2").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If

                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 2 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 2  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If




                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob3").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix7.Columns.Item("ob3").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If

                                    If oMatrix7.Columns.Item("b3").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b3").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If



                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then

                                            oGFun.Msg("Observation 3 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 3  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If

                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob4").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix7.Columns.Item("ob4").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If


                                    If oMatrix7.Columns.Item("b4").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b4").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 4 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 4  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If



                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob5").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix7.Columns.Item("ob5").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If

                                    If oMatrix7.Columns.Item("b5").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b5").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 5 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 5  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If



                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob6").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix7.Columns.Item("ob6").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If

                                    If oMatrix7.Columns.Item("b6").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b6").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 6 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 6  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If

                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob7").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix7.Columns.Item("ob7").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If

                                    If oMatrix7.Columns.Item("b7").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b7").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 7 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 7  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If

                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob8").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix7.Columns.Item("ob8").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = 0
                                    End If

                                    If oMatrix7.Columns.Item("b8").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b8").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 8 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 8  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If
                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob9").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix7.Columns.Item("ob9").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If
                                    If oMatrix7.Columns.Item("b9").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b9").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 9 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 9  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If
                                If sBoole = True Then
                                    If oMatrix7.Columns.Item("ob10").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix7.Columns.Item("ob10").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = 0
                                    End If
                                    If oMatrix7.Columns.Item("b10").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix7.Columns.Item("b10").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 10 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 10  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If


                            End If

                        End If
                    Next
                    '  


                Else

                    oMatrix = frmInspection.Items.Item("55").Specific

                    For i As Integer = 1 To oMatrix.VisualRowCount
                        If i = 1 Then
                            sBoole = True
                        End If


                        Dim oBatchSerialType As SAPbouiCOM.ComboBox = oMatrix.Columns.Item("Stype").Cells.Item(i).Specific
                        Dim NoOfObser As Double = Convert.ToDouble(oMatrix.Columns.Item("NoOfObser").Cells.Item(i).Specific.value)
                        Dim ocmbSelected As SAPbouiCOM.CheckBox = oMatrix.Columns.Item("Selected").Cells.Item(i).Specific

                        If ocmbSelected.Checked = True Then


                            If oBatchSerialType.Selected.Value = "V" Then


                                If oMatrix7.Columns.Item("CR").Cells.Item(i).Specific.string <> String.Empty Then
                                    dCorrelation = Convert.ToDouble(oMatrix7.Columns.Item("CR").Cells.Item(i).Specific.string)
                                Else
                                    dCorrelation = 0
                                End If

                                dObservationValue = dObservationValue + dCorrelation

                                If oMatrix.Columns.Item("llim").Cells.Item(i).Specific.string <> String.Empty Then
                                    dFromValue = Convert.ToDouble(oMatrix.Columns.Item("llim").Cells.Item(i).Specific.string)
                                Else
                                    dFromValue = 0
                                End If

                                If oMatrix.Columns.Item("ulim").Cells.Item(i).Specific.string <> String.Empty Then
                                    dToValue = Convert.ToDouble(oMatrix.Columns.Item("ulim").Cells.Item(i).Specific.string)
                                Else
                                    dToValue = 0
                                End If

                                If oMatrix.Columns.Item("observ1").Cells.Item(i).Specific.string <> String.Empty Then
                                    dObservationValue = oMatrix.Columns.Item("observ1").Cells.Item(i).Specific.string
                                Else
                                    dObservationValue = -1
                                End If
                                If oMatrix.Columns.Item("13").Cells.Item(i).Specific.string <> String.Empty Then
                                    dBatchNo = oMatrix.Columns.Item("13").Cells.Item(i).Specific.string
                                Else
                                    dBatchNo = String.Empty
                                End If
                                dObservationValue = dObservationValue + dCorrelation

                                If sBoole = True Then
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 1 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 1  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If

                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If


                                If sBoole = True Then
                                    If oMatrix.Columns.Item("observ2").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix.Columns.Item("observ2").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If

                                    If oMatrix.Columns.Item("Batch2").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("Batch2").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If

                                    dObservationValue = dObservationValue + dCorrelation

                                    If NoOfObser > 0 Then
                                        If dObservationValue <= 0 Then
                                            oGFun.Msg("Observation 2 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 2  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If




                                If sBoole = True Then
                                    If oMatrix.Columns.Item("observ3").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix.Columns.Item("observ3").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = 0
                                    End If

                                    If oMatrix.Columns.Item("17").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("17").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 3 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 3  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If

                                If sBoole = True Then
                                    If oMatrix.Columns.Item("observ4").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix.Columns.Item("observ4").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix.Columns.Item("19").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("19").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 4 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 4  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If



                                If sBoole = True Then
                                    If oMatrix.Columns.Item("observ5").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix.Columns.Item("observ5").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If

                                    If oMatrix.Columns.Item("21").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("21").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 5 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 5  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If



                                If sBoole = True Then
                                    If oMatrix.Columns.Item("ob6").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix.Columns.Item("ob6").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix.Columns.Item("b6").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("b6").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 6 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 6  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If


                                End If

                                If sBoole = True Then
                                    If oMatrix.Columns.Item("ob7").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix.Columns.Item("ob7").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix.Columns.Item("b7").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("b7").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 7 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 7  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If

                                If sBoole = True Then
                                    If oMatrix.Columns.Item("ob8").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix.Columns.Item("ob8").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix.Columns.Item("b8").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("b8").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 8 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 8  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If
                                If sBoole = True Then
                                    If oMatrix.Columns.Item("ob9").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix.Columns.Item("ob9").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix.Columns.Item("b9").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("b9").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 9 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 9  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If
                                If sBoole = True Then
                                    If oMatrix.Columns.Item("ob10").Cells.Item(i).Specific.string <> String.Empty Then
                                        dObservationValue = oMatrix.Columns.Item("ob10").Cells.Item(i).Specific.string
                                    Else
                                        dObservationValue = -1
                                    End If
                                    If oMatrix.Columns.Item("b10").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("b10").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    dObservationValue = dObservationValue + dCorrelation
                                    If NoOfObser > 0 Then
                                        If dObservationValue < 0 Then
                                            oGFun.Msg("Observation 10 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 10  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(dFromValue, dToValue, dObservationValue, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If
                            Else
                                If oMatrix.Columns.Item("llim").Cells.Item(i).Specific.string <> String.Empty Then
                                    sFromValue = (oMatrix.Columns.Item("llim").Cells.Item(i).Specific.string)
                                Else
                                    sFromValue = ""
                                End If

                                If oMatrix.Columns.Item("ulim").Cells.Item(i).Specific.string <> String.Empty Then
                                    sToValue = (oMatrix.Columns.Item("ulim").Cells.Item(i).Specific.string)
                                Else
                                    sToValue = ""
                                End If



                                If oMatrix.Columns.Item("observ1").Cells.Item(i).Specific.string <> String.Empty Then
                                    sObservationText = oMatrix.Columns.Item("observ1").Cells.Item(i).Specific.string
                                Else
                                    sObservationText = ""
                                End If
                                If oMatrix.Columns.Item("13").Cells.Item(i).Specific.string <> String.Empty Then
                                    dBatchNo = oMatrix.Columns.Item("13").Cells.Item(i).Specific.string
                                Else
                                    dBatchNo = String.Empty
                                End If
                                If sBoole = True Then
                                    If NoOfObser > 0 Then
                                        NoOfObser = NoOfObser - 1
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 1 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 1  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If






                                If sBoole = True Then
                                    If oMatrix.Columns.Item("observ2").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix.Columns.Item("observ2").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If
                                    If oMatrix.Columns.Item("Batch2").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("Batch2").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 2 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 2  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If




                                If sBoole = True Then
                                    If oMatrix.Columns.Item("observ3").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix.Columns.Item("observ3").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If
                                    If oMatrix.Columns.Item("17").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("17").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then

                                            oGFun.Msg("Observation 3 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 3  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If

                                If sBoole = True Then
                                    If oMatrix.Columns.Item("observ4").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix.Columns.Item("observ4").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If
                                    If oMatrix.Columns.Item("19").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("19").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 4 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 4  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If



                                If sBoole = True Then
                                    If oMatrix.Columns.Item("observ5").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix.Columns.Item("observ5").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If
                                    If oMatrix.Columns.Item("21").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("21").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 5 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 5  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If



                                If sBoole = True Then
                                    If oMatrix.Columns.Item("ob6").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix.Columns.Item("ob6").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If
                                    If oMatrix.Columns.Item("b6").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("b6").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 6 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 6  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If

                                End If

                                If sBoole = True Then
                                    If oMatrix.Columns.Item("ob7").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix.Columns.Item("ob7").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If
                                    If oMatrix.Columns.Item("b7").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("b7").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 7 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 7  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If

                                If sBoole = True Then
                                    If oMatrix.Columns.Item("ob8").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix.Columns.Item("ob8").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = 0
                                    End If
                                    If oMatrix.Columns.Item("b8").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("b8").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 8 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 8  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If
                                If sBoole = True Then
                                    If oMatrix.Columns.Item("ob9").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix.Columns.Item("ob9").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = ""
                                    End If
                                    If oMatrix.Columns.Item("b9").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("b9").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 9 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 9  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If
                                If sBoole = True Then
                                    If oMatrix.Columns.Item("ob10").Cells.Item(i).Specific.string <> String.Empty Then
                                        sObservationText = oMatrix.Columns.Item("ob10").Cells.Item(i).Specific.string
                                    Else
                                        sObservationText = 0
                                    End If
                                    If oMatrix.Columns.Item("b10").Cells.Item(i).Specific.string <> String.Empty Then
                                        dBatchNo = oMatrix.Columns.Item("b10").Cells.Item(i).Specific.string
                                    Else
                                        dBatchNo = String.Empty
                                    End If
                                    If NoOfObser > 0 Then
                                        If sObservationText = "" Then
                                            oGFun.Msg("Observation 10 Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        If dBatchNo = String.Empty Then
                                            oGFun.Msg("Batch 10  Details Should not be empty, Line Id:" + i.ToString())
                                            Return False
                                        End If
                                        NoOfObser = NoOfObser - 1
                                        sBoole = Valid(sFromValue, sToValue, sObservationText, oBatchSerialType.Selected.Value.ToString())
                                    End If
                                End If


                            End If
                        End If


                    Next




                End If
                Dim cmbc_QCStatus As SAPbouiCOM.ComboBox = frmInspection.Items.Item("c_QCStatus").Specific
                If sBoole = False Then
                    Dim TotalRejectedQty As Double = 0
                    oApplication.StatusBar.SetText("Verify:" & sBoole, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    For i As Integer = 1 To oMatrix5.VisualRowCount
                        oMatrix5.Columns.Item("RejQty").Cells.Item(i).Specific.string = oMatrix5.Columns.Item("2").Cells.Item(i).Specific.string
                        oMatrix5.Columns.Item("AccQty").Cells.Item(i).Specific.string = 0
                        TotalRejectedQty = TotalRejectedQty + Convert.ToDouble(oMatrix5.Columns.Item("2").Cells.Item(i).Specific.string)
                    Next
                    frmInspection.Items.Item("34").Specific.value = TotalRejectedQty.ToString()
                    cmbc_QCStatus.Select("R", SAPbouiCOM.BoSearchKey.psk_ByValue)

                End If
                If sBoole = True Then
                    Dim TotalAcceptedQty As Double = 0
                    oApplication.StatusBar.SetText("Verify:" & sBoole, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    For i As Integer = 1 To oMatrix5.VisualRowCount
                        oMatrix5.Columns.Item("AccQty").Cells.Item(i).Specific.string = oMatrix5.Columns.Item("2").Cells.Item(i).Specific.string
                        oMatrix5.Columns.Item("RejQty").Cells.Item(i).Specific.string = 0
                        TotalAcceptedQty = TotalAcceptedQty + Convert.ToDouble(oMatrix5.Columns.Item("2").Cells.Item(i).Specific.string)
                    Next
                    frmInspection.Items.Item("32").Specific.value = TotalAcceptedQty.ToString()
                    cmbc_QCStatus.Select("A", SAPbouiCOM.BoSearchKey.psk_ByValue)
                End If
            End If

            'rengan




            ValidateAll = True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate All Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

    Sub LoadPreSalesOrderDetails()
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the   Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            Dim sQuery = " EXEC [dbo].[@AIS_ProductionReceipt_GetDoffNoDetails] '" & Trim(oDBDSHeader.GetValue("U_WBaseEntry", 0)) & "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = oGFun.DoQuery(sQuery)
            rsetEmpDets.MoveFirst()
            oDBDSDetail8.Clear()
            oMatrix8.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail8.InsertRecord(oDBDSDetail8.Size)
                oDBDSDetail8.Offset = i

                oDBDSDetail8.SetValue("LineID", oDBDSDetail8.Offset, i + 1)
                oDBDSDetail8.SetValue("U_DNO", oDBDSDetail8.Offset, rsetEmpDets.Fields.Item("U_DoffNo").Value)
                oDBDSDetail8.SetValue("U_MCCode", oDBDSDetail8.Offset, rsetEmpDets.Fields.Item("U_MCCode").Value)
                oDBDSDetail8.SetValue("U_MCNAme", oDBDSDetail8.Offset, rsetEmpDets.Fields.Item("U_MCName").Value)
                rsetEmpDets.MoveNext()
            Next
            oMatrix8.LoadFromDataSource()

            If frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Data Based On Department Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub LoadParameterDetails()
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the   Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            Dim sQuery = " EXEC [dbo].[@AIS_ProductionReceipt_MCParameter] '" & Trim(oDBDSHeader.GetValue("U_RecEntry", 0)) & "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = oGFun.DoQuery(sQuery)
            rsetEmpDets.MoveFirst()
            oDBDSDetail6.Clear()
            oMatrix6.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail6.InsertRecord(oDBDSDetail6.Size)
                oDBDSDetail6.Offset = i

                oDBDSDetail6.SetValue("LineID", oDBDSDetail6.Offset, i + 1)
                oDBDSDetail6.SetValue("U_Parameter", oDBDSDetail6.Offset, rsetEmpDets.Fields.Item("U_PName").Value)
                oDBDSDetail6.SetValue("U_Result", oDBDSDetail6.Offset, rsetEmpDets.Fields.Item("U_Result").Value)

                rsetEmpDets.MoveNext()
            Next
            oMatrix6.LoadFromDataSource()

            If frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Data Based On Department Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub LoadBatchDetails()
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the   Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            Dim sQuery = " EXEC [dbo].[@AIS_ProductionReceipt_GetBatchDetails] '" & Trim(oDBDSHeader.GetValue("U_RecEntry", 0)) & "','" & Trim(oDBDSHeader.GetValue("U_ICode", 0)) & "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = oGFun.DoQuery(sQuery)
            rsetEmpDets.MoveFirst()
            oDBDSDetail5.Clear()
            oMatrix5.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail5.InsertRecord(oDBDSDetail5.Size)
                oDBDSDetail5.Offset = i

                oDBDSDetail5.SetValue("LineID", oDBDSDetail5.Offset, i + 1)
                oDBDSDetail5.SetValue("U_BatNo", oDBDSDetail5.Offset, rsetEmpDets.Fields.Item("BatchNum").Value)
                oDBDSDetail5.SetValue("U_AQty", oDBDSDetail5.Offset, rsetEmpDets.Fields.Item("Quantity").Value)
                oDBDSDetail5.SetValue("U_UOM", oDBDSDetail5.Offset, rsetEmpDets.Fields.Item("UOM").Value)


                rsetEmpDets.MoveNext()
            Next
            oMatrix5.LoadFromDataSource()

            If frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Data Based On Department Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub GRNBatchDetails()
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the   Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)



            Dim oBatchSerialType As SAPbouiCOM.ComboBox = frmInspection.Items.Item("c_BSType").Specific
            Dim Type As String = oBatchSerialType.Selected.Value
            Dim FormType As String = frmInspection.Items.Item("97").Specific.value.ToString.Trim
            Dim GrnEntry As String = frmInspection.Items.Item("t_GRNEntry").Specific.value.ToString.Trim
            Dim ItemCode As String = frmInspection.Items.Item("118").Specific.value.ToString.Trim
            Dim GRNWhsCode As String = oGFun.getSingleValue("select d.whscode from OPDN m left outer join PDN1 d on m.docentry=d.docentry where m.docnum='" & frmInspection.Items.Item("t_GRNNo").Specific.value.ToString.Trim & "'  and d.itemcode='" & frmInspection.Items.Item("118").Specific.value.ToString.Trim & "'")
            StrSql = " EXEC [dbo].[@AIS_Inspection_GetBatchNumberDetailsForGRN] '" & Trim(ItemCode) & "','" & Trim(GRNWhsCode) & "','" & Trim(GrnEntry) & "','" & Trim(Type) & "'"

            Dim rsetEmpDets As SAPbobsCOM.Recordset = oGFun.DoQuery(StrSql)
            rsetEmpDets.MoveFirst()
            oDBDSDetail5.Clear()
            oMatrix5.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail5.InsertRecord(oDBDSDetail5.Size)
                oDBDSDetail5.Offset = i

                oDBDSDetail5.SetValue("LineID", oDBDSDetail5.Offset, i + 1)
                oDBDSDetail5.SetValue("U_BatNo", oDBDSDetail5.Offset, rsetEmpDets.Fields.Item("BatchNum").Value)
                oDBDSDetail5.SetValue("U_AQty", oDBDSDetail5.Offset, rsetEmpDets.Fields.Item("qty").Value)
                'oDBDSDetail5.SetValue("U_UOM", oDBDSDetail5.Offset, rsetEmpDets.Fields.Item("UOM").Value)


                rsetEmpDets.MoveNext()
            Next
            oMatrix5.LoadFromDataSource()

            If frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Data Based On Department Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub Oper_List_SubItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "b_ok" And pVal.BeforeAction = True Then
                        Dim row As Integer = 0
                        Dim frmOperationsList As SAPbouiCOM.Form = oApplication.Forms.Item(OperationsListFormID)
                        Dim oGrid As SAPbouiCOM.Grid = frmOperationsList.Items.Item("Grid").Specific
                        oMatrix5.Clear()
                        Dim oBatchSerialType As SAPbouiCOM.ComboBox = frmInspection.Items.Item("c_BSType").Specific
                        frmInspection.Freeze(True)
                        Dim RowCount As Integer = 0
                        For i As Integer = 0 To oGrid.Rows.Count - 1
                            If oGrid.Rows.IsSelected(i) Then
                                oMatrix5.AddRow()
                                If (oBatchSerialType.Selected.Value = "B") Then
                                    RowCount = RowCount + 1
                                    oMatrix5.Columns.Item("uom").Cells.Item(RowCount).Specific.value = oGrid.DataTable.GetValue(0, i)
                                    oMatrix5.Columns.Item("#").Cells.Item(RowCount).Specific.value = RowCount
                                    oMatrix5.Columns.Item("2").Cells.Item(RowCount).Specific.value = oGrid.DataTable.GetValue(1, i)
                                    oMatrix5.Columns.Item("3").Cells.Item(RowCount).Specific.value = oGrid.DataTable.GetValue(1, i)
                                    Dim ManSerNum = oGFun.getSingleValue("Select  InvntryUom  from oitm  Where  " & _
                                             "ItemCode = '" & oDBDSHeader.GetValue("U_ItemCode", 0).ToString.Trim & "'")
                                    oMatrix5.Columns.Item("uomna").Cells.Item(RowCount).Specific.value = ManSerNum

                                ElseIf (oBatchSerialType.Selected.Value = "S") Then
                                    RowCount = RowCount + 1
                                    oMatrix5.Columns.Item("uom").Cells.Item(RowCount).Specific.value = oGrid.DataTable.GetValue(0, i)
                                    oMatrix5.Columns.Item("#").Cells.Item(RowCount).Specific.value = RowCount
                                    oMatrix5.Columns.Item("2").Cells.Item(RowCount).Specific.value = 1
                                    oMatrix5.Columns.Item("3").Cells.Item(RowCount).Specific.value = 1

                                    Dim cmbvalue As SAPbouiCOM.ComboBox
                                    cmbvalue = oMatrix5.Columns.Item("SerNoRt").Cells.Item(RowCount).Specific
                                    cmbvalue.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)

                                    cmbvalue = oMatrix5.Columns.Item("SerNoHRt").Cells.Item(RowCount).Specific
                                    cmbvalue.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)


                                End If

                                row = row + 1
                            End If
                        Next
                        frmOperationsList.Close()
                    End If

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("SubItemEvent method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmInspection.Freeze(False)
        End Try
    End Sub
    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try

                        'Assign Selected Rows
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        '        'Filter before open the CFL
                        If pVal.BeforeAction Then
                            Select Case oCFLE.ChooseFromListUID
                                Case "cfl_WNO"
                                    Dim strSql = "[@AIS_QC_WorkOrderInspectionFilter]"
                                    oGFun.ChooseFromListFilteration(frmInspection, "cfl_WNO", "DocNum", strSql)
                                Case "CFL_Mach"
                                    Dim strSql = "[@AIS_QC_ResourceMasterInspectionType]"
                                    oGFun.ChooseFromListFilteration(frmInspection, "CFL_Mach", "ResCode", strSql)

                                Case "CFL_AS_OIGN"

                               
                                    Dim strSqlWO = " [@AIS_QC_WorkOrderProductionReceiptInspectionFilter] '" & oDBDSHeader.GetValue("U_WBaseNum", 0).ToString().Trim() & "' "
                                        oGFun.ChooseFromListFilteration(frmInspection, "CFL_AS_OIGN", "DocNum", strSqlWO)


                                Case "cfl_PRO"

                                    Dim strSql = " select U_ProNo , U_IndNo , OWIP.DocNum from [@MIPL_OWIP] OWIP,[@MIPL_WIP1] WIP1,OWOR,WOR1 where " & _
                                                  " OWIP.DocEntry = WIP1.DocEntry and OWOR.DocEntry =WOR1.DocEntry and OWOR.DocNum = OWIP.U_ProNo and WOR1.ItemCode = WIP1.U_OpCode  and ISNULL(wip1.U_FinQty,0) > " & _
                                                  " ISNULL(wip1.U_AccQty_,0)+ISNULL(wip1.U_RejQty_,0)+ISNULL(wip1.U_RewQty_,0) " & _
                                                  " and isnull(WOR1.U_Inspec,'N') = 'Y' "

                                    oGFun.ChooseFromListFilteration(frmInspection, "cfl_PRO", "DocNum", strSql)

                                Case "cfl_IND"

                                    Dim strSql = " select U_IndNo , U_ProNo , OWIP.DocNum from [@MIPL_OWIP] OWIP,[@MIPL_WIP1] WIP1,OWOR,WOR1 where " & _
                                                  " OWIP.DocEntry = WIP1.DocEntry and OWOR.DocEntry =WOR1.DocEntry and OWOR.DocNum = OWIP.U_ProNo and ISNULL(wip1.U_FinQty,0) > " & _
                                                  " ISNULL(wip1.U_AccQty_,0)+ISNULL(wip1.U_RejQty_,0)+ISNULL(wip1.U_RewQty_,0) " & _
                                                  " and isnull(WOR1.U_Inspec,'N') = 'Y' and U_ProNo = '" & oDBDSHeader.GetValue("U_ProNo", 0) & "' "

                                    oGFun.ChooseFromListFilteration(frmInspection, "cfl_IND", "DocNum", strSql)

                                Case "cfl_GRN"
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmInspection.Items.Item("97").Specific
                                    Dim strSerialCode As String = oCmbSerial.Value.ToString.Trim
                                    If strSerialCode.ToUpper.Trim = "GRN" Then
                                        StrSql = " select distinct DocNum from ( select OPDN.DocNum , PDN1.ItemCode , SUM(isnull(PDN1.Quantity,0)) - " & _
                                                    " isnull((Select SUM(isnull(OINS.U_InsQty,0))  from [@MIPL_OINS]  OINS where " & _
                                                    " OPDN.DocNum =OINS.U_GRNNo and PDN1.ItemCode =OINS.U_ICode ),0) Qty from " & _
                                                    " OPDN,PDN1 where OPDN.DocStatus ='O'and isnull(PDN1.U_ItemCode,'') ='' and OPDN.DocEntry =PDN1.DocEntry and " & _
                                                    " PDN1.ItemCode in (Select Itemcode from OITM where isnull(U_InsReq,'N') = 'Y' and PDN1.U_WhsCode Is not Null) " & _
                                                    " Group By OPDN.DocNum , PDN1.ItemCode ) AA where  Qty > 0 "

                                        oGFun.ChooseFromListFilteration(frmInspection, "cfl_GRN", "DocNum", StrSql)
                                    End If

                                Case "CFL_4A"
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmInspection.Items.Item("97").Specific
                                    Dim strSerialCode As String = oCmbSerial.Value.ToString.Trim

                                    If strSerialCode.ToUpper.Trim = "IN" Then

                                        StrSql = " select distinct ItemCode from ( select OIGN.DocEntry , IGN1.ItemCode , SUM(isnull(IGN1.Quantity,0)) - " & _
                                                    " isnull((Select SUM(isnull(OINS.U_InsQty,0))  from [@MIPL_OINS]  OINS where " & _
                                                    " OIGN.DocEntry =OINS.U_RecEntry and IGN1.ItemCode =OINS.U_ICode ),0) Qty from " & _
                                                    " OIGN,IGN1 where  OIGN.DocEntry =IGN1.DocEntry and " & _
                                                    " IGN1.ItemCode in (Select Itemcode from OITM where isnull(U_InsReq,'N') = 'Y' and isnull(IGN1.WhsCode,'') <>'')  " & _
                                                      "  And OIGN.U_BaseNum = '" & oDBDSHeader.GetValue("U_Recno", 0).ToString.Trim & "'" & _
                                                    " Group By OIGN.DocEntry ,OIGN.DocEntry, IGN1.ItemCode ) AA where  Qty > 0 "



                                    ElseIf strSerialCode.ToUpper.Trim = "GRN" Then

                                        StrSql = " Select ItemCode from ( select PDN1.ItemCode , OPDN.DocNum , SUM(isnull(PDN1.Quantity,0)) - " & _
                                                " isnull((Select SUM(isnull(OINS.U_InsQty,0)) from [@MIPL_OINS]  OINS where " & _
                                                " OPDN.DocNum =OINS.U_GRNNo and PDN1.ItemCode =OINS.U_ICode ),0) Qty from " & _
                                                " OPDN,PDN1 where OPDN.DocEntry =PDN1.DocEntry and " & _
                                                " PDN1.ItemCode in (Select Itemcode from OITM where isnull(U_InsReq,'N') = 'Y') And " & _
                                                " OPDN.DocNum = '" & oDBDSHeader.GetValue("U_GRNNo", 0) & "' " & _
                                                " Group By OPDN.DocNum , PDN1.ItemCode ) AA Where Qty > 0 "
                                    End If

                                    oGFun.ChooseFromListFilteration(frmInspection, "CFL_4A", "ItemCode", StrSql)

                            End Select
                        End If


                        If pVal.BeforeAction = False And Not oDataTable Is Nothing Then
                            Select Case oCFLE.ChooseFromListUID
                                Case "CFL_PARA"
                                    oMatrix7.FlushToDataSource()
                                    oDBDSDetail7.SetValue("U_PCode", pVal.Row - 1, Trim(oDataTable.GetValue("U_Code", 0)))
                                    oDBDSDetail7.SetValue("U_Des", pVal.Row - 1, Trim(oDataTable.GetValue("U_Name", 0)))
                                    oMatrix7.LoadFromDataSource()
                                    oMatrix7.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                Case "cfl_WNO"
                                    oDBDSHeader.SetValue("U_WBaseNum", 0, oDataTable.GetValue("DocNum", 0))
                                    oDBDSHeader.SetValue("U_WBaseEntry", 0, oDataTable.GetValue("DocEntry", 0))
                                    oDBDSHeader.SetValue("U_WOType", 0, oDataTable.GetValue("U_Type", 0).ToString())


                                    Dim sQuery = " EXEC [dbo].[@AIS_Quality_GetCustomerDetails] '" & oDataTable.GetValue("DocEntry", 0) & "'"
                                    Dim rsetEmpDets As SAPbobsCOM.Recordset = oGFun.DoQuery(sQuery)
                                    Dim WorkOrderEntry As String = String.Empty
                                    For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                                        oDBDSHeader.SetValue("U_CustCode", 0, rsetEmpDets.Fields.Item("CardCode").Value)
                                        oDBDSHeader.SetValue("U_CustName", 0, rsetEmpDets.Fields.Item("CardName").Value)
                                    Next


                                    LoadPreSalesOrderDetails()



                                Case "CFL_Bpartner"
                                    oDBDSHeader.SetValue("U_CustCode", 0, oDataTable.GetValue("CardCode", 0))
                                    oDBDSHeader.SetValue("U_CustName", 0, oDataTable.GetValue("CardName", 0))

                                Case "CFL_Machine"
                                    oMatrix.FlushToDataSource()
                                    oDBDSDetail1.SetValue("U_MCCode", pVal.Row - 1, Trim(oDataTable.GetValue("ResCode", 0)))
                                    oDBDSDetail1.SetValue("U_MCName", pVal.Row - 1, Trim(oDataTable.GetValue("ResName", 0)))
                                    oMatrix.LoadFromDataSource()
                                    oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                Case "CFL_AS_OIGN"
                                    oDBDSHeader.SetValue("U_Recno", 0, oDataTable.GetValue("DocNum", 0))
                                    oDBDSHeader.SetValue("U_RecEntry", 0, oDataTable.GetValue("DocEntry", 0))
                                    Dim sQuery = " EXEC [dbo].[@AIS_ProductionReceipt_GetNoOfSpindle] '" & Trim(oDataTable.GetValue("DocEntry", 0)) & "'"
                                    Dim rsetEmpDets As SAPbobsCOM.Recordset = oGFun.DoQuery(sQuery)
                                    Dim WorkOrderEntry As String = String.Empty
                                    For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                                        oDBDSHeader.SetValue("U_SNo", 0, rsetEmpDets.Fields.Item("U_NoOfSpindle").Value)
                                        WorkOrderEntry = rsetEmpDets.Fields.Item("WorkOrderEntry").Value
                                    Next
                                    LoadParameterDetails()
                                Case "CFL_4A"
                                    oDBDSHeader.SetValue("U_ICode", 0, oDataTable.GetValue("ItemCode", 0))
                                    oDBDSHeader.SetValue("U_IDes", 0, oDataTable.GetValue("FrgnName", 0))
                                    oDBDSHeader.SetValue("U_ItmsGrpCod", 0, oDataTable.GetValue("ItmsGrpCod", 0))
                                    Dim ItmsGrpNam = oGFun.getSingleValue("select ItmsGrpNam  from OITB   Where ItmsGrpCod = " & _
                                     " '" & oDataTable.GetValue("ItmsGrpCod", 0).ToString().Trim() & "'")
                                    oDBDSHeader.SetValue("U_ItmsGrpNam", 0, ItmsGrpNam)
                                    Dim ManBtchNum = oGFun.getSingleValue("select ManBtchNum      from OITM where " & _
                                                "ItemCode = '" & oDataTable.GetValue("ItemCode", 0).ToString().Trim() & "'")
                                    Dim ManSerNum = oGFun.getSingleValue("select  ManSerNum    from OITM where " & _
                                              "ItemCode = '" & oDataTable.GetValue("ItemCode", 0).ToString().Trim() & "'")
                                    Dim Qty
                                    Dim oBatchSerialType As SAPbouiCOM.ComboBox = frmInspection.Items.Item("c_BSType").Specific
                                    If ManBtchNum = "Y" Then
                                        oDBDSHeader.SetValue("U_MItemBy", 0, "B")
                                    ElseIf ManSerNum = "Y" Then
                                        oDBDSHeader.SetValue("U_MItemBy", 0, "S")
                                    Else
                                        oDBDSHeader.SetValue("U_MItemBy", 0, "N")
                                    End If
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmInspection.Items.Item("97").Specific
                                    Dim strSerialCode As String = oCmbSerial.Value.ToString.Trim
                                    If strSerialCode.ToUpper.Trim = "IN" Then
                                        frmInspection.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                                        CheckEnforceSamplingDetailsInspection(oDataTable.GetValue("ItemCode", 0))
                                        LoadBatchDetails()
                                        StrSql = "[@AIS_QC_Inspection_GetBalanceQty] '" + oDBDSHeader.GetValue("U_Recno", 0) + "','" + oDBDSHeader.GetValue("U_ICode", 0) + "'"
                                        Qty = oGFun.getSingleValue(StrSql)
                                        oDBDSHeader.SetValue("U_PendQty", pVal.Row, Qty)
                                        oDBDSHeader.SetValue("U_InsQty", pVal.Row, Qty)

                                    ElseIf strSerialCode.ToUpper.Trim = "GRN" Then
                                        StrSql = " select SUM(isnull(PDN1.Quantity,0)) - " & _
                                                " isnull((Select SUM(isnull(OINS.U_InsQty,0)) from [@MIPL_OINS]  OINS where " & _
                                                " OPDN.DocNum =OINS.U_GRNNo and PDN1.ItemCode =OINS.U_ICode ),0) Qty from " & _
                                                " OPDN,PDN1 where OPDN.DocEntry =PDN1.DocEntry and " & _
                                                " PDN1.ItemCode in (Select Itemcode from OITM where isnull(U_InsReq,'N') = 'Y') And " & _
                                                " OPDN.DocNum = '" & oDBDSHeader.GetValue("U_GRNNo", 0) & "' And PDN1.ItemCode = '" & _
                                                oDBDSHeader.GetValue("U_ICode", 0) & "' " & _
                                                " Group By OPDN.DocNum , PDN1.ItemCode "
                                        Qty = oGFun.getSingleValue(StrSql)
                                        Qty = IIf(Qty.Trim = "", 0, Qty)
                                        oDBDSHeader.SetValue("U_PendQty", pVal.Row, Qty)
                                        oDBDSHeader.SetValue("U_InsQty", pVal.Row, Qty)

                                        GetUom(oDBDSHeader.GetValue("U_ICode", 0).ToString.Trim)
                                        CheckEnforceSamplingDetailsRawMaterial(oDataTable.GetValue("ItemCode", 0))
                                        SelectSeries()
                                        Dim itemcode = oDBDSHeader.GetValue("U_ICode", 0)
                                        If itemcode <> "" Then GetFields(itemcode)
                                        oGFun.SetComboBoxValueRefresh(frmInspection.Items.Item("69").Specific, "Select Code,U_Name from [@MIPL_QC_DEFECT] where U_Material ='0'")
                                        GRNBatchDetails()
                                    End If




                                Case "cfl_GRN"
                                    oDBDSHeader.SetValue("U_GRNNo", 0, oDataTable.GetValue("DocNum", 0))
                                    oDBDSHeader.SetValue("U_GRNEntry", 0, oDataTable.GetValue("DocEntry", 0))
                                    oDBDSHeader.SetValue("U_CustCode", 0, oDataTable.GetValue("CardCode", 0))
                                    oDBDSHeader.SetValue("U_CustName", 0, oDataTable.GetValue("CardName", 0))

                                Case "cfl_empche"
                                    oDBDSHeader.SetValue("U_Chkcode", 0, oDataTable.GetValue("empID", 0))
                                    oDBDSHeader.SetValue("U_ChkName", 0, oDataTable.GetValue("lastName", 0) + oDataTable.GetValue("firstName", 0))
                                Case "cfl_ccod"
                                    oDBDSHeader.SetValue("U_CByCod", 0, oDataTable.GetValue("empID", 0))
                                    oDBDSHeader.SetValue("U_Cby", 0, oDataTable.GetValue("lastName", 0) + oDataTable.GetValue("firstName", 0))
                                Case "cfl_empins"
                                    oDBDSHeader.SetValue("U_Inscode", 0, oDataTable.GetValue("empID", 0))
                                    oDBDSHeader.SetValue("U_InsName", 0, oDataTable.GetValue("lastName", 0) + oDataTable.GetValue("firstName", 0))
                                Case "cfl_IND"
                                    oDBDSHeader.SetValue("U_IndNo", 0, oDataTable.GetValue("DocNum", 0))
                                Case "cfl_WIP"
                                    oDBDSHeader.SetValue("U_WIPNo", 0, oDataTable.GetValue("DocNum", 0))
                                Case "cfl_rspm"
                                    oDBDSHeader.SetValue("U_RsMgCode", 0, oDataTable.GetValue("empID", 0))
                                    oDBDSHeader.SetValue("U_RsMgName", 0, oDataTable.GetValue("lastName", 0) + oDataTable.GetValue("firstName", 0))
                                Case "CFL_Mach"
                                    oMatrix7.FlushToDataSource()
                                    oDBDSDetail7.SetValue("U_MCCode", pVal.Row - 1, Trim(oDataTable.GetValue("ResCode", 0)))
                                    oDBDSDetail7.SetValue("U_MCName", pVal.Row - 1, Trim(oDataTable.GetValue("ResName", 0)))
                                    oMatrix7.LoadFromDataSource()
                                    oMatrix7.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                            End Select
                        End If

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try

                        If pVal.ItemUID = "55" And pVal.ColUID = "2" And (frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            oMatrix = frmInspection.Items.Item("55").Specific
                            If oMatrix.Columns.Item("2").Cells.Item(oMatrix.VisualRowCount).Specific.value <> "" Then
                                oMatrix.AddRow()
                                oMatrix.Columns.Item("#").Cells.Item(oMatrix.VisualRowCount).Specific.string = oMatrix.VisualRowCount
                                oMatrix.ClearRowData(oMatrix.VisualRowCount)
                            End If
                        End If

                        If pVal.ItemUID = "56" And pVal.ColUID = "2" And (frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            oMatrix2 = frmInspection.Items.Item("56").Specific
                            If oMatrix2.Columns.Item("2").Cells.Item(oMatrix2.VisualRowCount).Specific.value <> "" Then
                                oMatrix2.AddRow()
                                oMatrix2.Columns.Item("#").Cells.Item(oMatrix2.VisualRowCount).Specific.string = oMatrix2.VisualRowCount
                                oMatrix2.ClearRowData(oMatrix2.VisualRowCount)
                            End If
                        End If

                        If pVal.ItemUID = "FG" And pVal.ColUID = "sno" And (frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            oMatrix7 = frmInspection.Items.Item("FG").Specific
                            If oMatrix7.Columns.Item("sno").Cells.Item(oMatrix7.VisualRowCount).Specific.value <> "" Then
                                oMatrix7.AddRow()
                                oMatrix7.Columns.Item("#").Cells.Item(oMatrix7.VisualRowCount).Specific.string = oMatrix7.VisualRowCount
                                oMatrix7.ClearRowData(oMatrix7.VisualRowCount)
                            End If
                        End If

                        If pVal.ItemUID = "MC" And pVal.ColUID = "par" And (frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            oMatrix6 = frmInspection.Items.Item("MC").Specific
                            If oMatrix6.Columns.Item("par").Cells.Item(oMatrix6.VisualRowCount).Specific.value <> "" Then
                                oMatrix6.AddRow()
                                oMatrix6.Columns.Item("#").Cells.Item(oMatrix6.VisualRowCount).Specific.string = oMatrix6.VisualRowCount
                                oMatrix6.ClearRowData(oMatrix6.VisualRowCount)
                            End If
                        End If

                        If pVal.ItemUID = "DNo" And pVal.ColUID = "mCod" And (frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            oMatrix8 = frmInspection.Items.Item("DNo").Specific
                            If oMatrix8.Columns.Item("mCod").Cells.Item(oMatrix8.VisualRowCount).Specific.value <> "" Then
                                oMatrix8.AddRow()
                                oMatrix8.Columns.Item("#").Cells.Item(oMatrix8.VisualRowCount).Specific.string = oMatrix8.VisualRowCount
                                oMatrix8.ClearRowData(oMatrix8.VisualRowCount)
                            End If
                        End If

                        If pVal.ItemUID = "Batch" And pVal.ColUID = "uom" And (frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            oMatrix4 = frmInspection.Items.Item("Batch").Specific
                            If oMatrix4.Columns.Item("uom").Cells.Item(oMatrix4.VisualRowCount).Specific.value <> "" Then
                                oMatrix4.AddRow()
                                oMatrix4.Columns.Item("#").Cells.Item(oMatrix4.VisualRowCount).Specific.string = oMatrix4.VisualRowCount
                                oMatrix4.ClearRowData(oMatrix4.VisualRowCount)
                            End If
                        End If
                        If pVal.ItemUID = "Matrix6" And pVal.ColUID = "St_txt" And (frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            oMatrix4 = frmInspection.Items.Item("Matrix6").Specific
                            If oMatrix4.Columns.Item("St_txt").Cells.Item(oMatrix4.VisualRowCount).Specific.value <> "" Then
                                oMatrix4.AddRow()
                                oMatrix4.Columns.Item("#").Cells.Item(oMatrix4.VisualRowCount).Specific.string = oMatrix4.VisualRowCount
                                oMatrix4.ClearRowData(oMatrix4.VisualRowCount)
                            End If
                        End If

                        If pVal.ItemUID = "Matrix3" And pVal.ColUID = "LinkPath" And (frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            oMatrix3 = frmInspection.Items.Item("Matrix3").Specific
                            If oMatrix3.Columns.Item("LinkPath").Cells.Item(oMatrix3.VisualRowCount).Specific.value <> "" Then
                                oMatrix3.AddRow()
                                oMatrix3.Columns.Item("#").Cells.Item(oMatrix3.VisualRowCount).Specific.string = oMatrix3.VisualRowCount
                                oMatrix3.ClearRowData(oMatrix3.VisualRowCount)
                            End If
                        End If

                        Select Case pVal.ItemUID
                            Case "30"
                                If pVal.BeforeAction Then
                                    Dim PenQty = oDBDSHeader.GetValue("U_PendQty", 0)
                                    Dim InsQty = oDBDSHeader.GetValue("U_InsQty", 0)

                                    PenQty = IIf(PenQty.Trim = "", 0, PenQty)
                                    InsQty = IIf(InsQty.Trim = "", 0, InsQty)

                                    If CDbl(PenQty) < CDbl(InsQty) Then
                                        oGFun.Msg("Inspection Qty. Should be Less than Pending Qty. ")
                                        BubbleEvent = False
                                        Return
                                    End If
                                End If

                        End Select

                        Select Case pVal.ColUID


                        End Select

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Validate Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    End Try

                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            'Get the Serial Number Based On Series...
                            Case "c_series"
                                If frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmInspection.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Value.ToString.Trim
                                    Dim strDocNum As Long = frmInspection.BusinessObject.GetNextSerialNumber(strSerialCode, "INS")
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                            Case "c_Dev"
                                Dim oCmbSerial As SAPbouiCOM.ComboBox = frmInspection.Items.Item("c_Dev").Specific
                                Dim strSerialCode As String = oCmbSerial.Value.ToString.Trim
                                If strSerialCode.ToUpper.Trim = "N" Or strSerialCode.ToUpper.Trim = "" Then
                                    frmInspection.Items.Item("btnDev").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                                Else
                                    frmInspection.Items.Item("btnDev").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Visible, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                                End If


                            Case "97"
                                If pVal.BeforeAction = False Then
                                    TypeWiseControlAlloc()
                                    frmInspection.Items.Item("135").Specific.value = String.Empty
                                    frmInspection.Items.Item("134").Specific.value = String.Empty
                                    frmInspection.Items.Item("t_GRNNo").Specific.value = String.Empty
                                    frmInspection.Items.Item("t_GRNEntry").Specific.value = String.Empty
                                    frmInspection.Items.Item("118").Specific.value = String.Empty
                                    frmInspection.Items.Item("1000015").Specific.value = String.Empty
                                    frmInspection.Items.Item("t_CCode").Specific.value = String.Empty
                                    frmInspection.Items.Item("t_CName").Specific.value = String.Empty

                                End If
                        End Select

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try

                        Select Case pVal.ItemUID

                            Case "1000016"
                                Try
                                    'Link button to load Parameter Master for Raw Material
                                    If pVal.BeforeAction Then

                                    Else



                                        'oIssueBiMatrix = oIssueForm.Items.Item("52").Specific
                                        'Dim issuline As Integer = oIssueBiMatrix.VisualRowCount
                                        'Dim GrpLot As String = objAddOn.getSingleValue("select U_Lot from [@AS_OWORD] where DocEntry = '" & WoDocEntry & "' ")
                                        'j = issuline


                                        oApplication.Menus.Item("AS_OIGN").Activate()
                                        Dim frm As SAPbouiCOM.Form
                                        frm = oApplication.Forms.GetForm("AS_OIGN", -1)
                                        frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                        'frm.Items.Item("t_DocNum").Visible = False
                                        frm.Items.Item("DocEntry").Visible = True
                                        frm.Items.Item("DocEntry").Enabled = True
                                        frm.Items.Item("DocEntry").Specific.Value = frmInspection.Items.Item("RE").Specific.value
                                        frm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                    End If

                                Catch ex As Exception
                                    oApplication.StatusBar.SetText("et_ITEM_PRESSED Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

                                End Try
                            Case "155"
                                Try

                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmInspection.Items.Item("97").Specific
                                    Dim strSerialCode As String = oCmbSerial.Value.ToString.Trim

                                    If strSerialCode.ToUpper.Trim = "IN" Then
                                        'Link button to load Parameter Master for Raw Material
                                        If pVal.BeforeAction Then

                                        Else
                                            oApplication.Menus.Item("3078").Activate()
                                            Dim frm As SAPbouiCOM.Form
                                            frm = oApplication.Forms.ActiveForm
                                            frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                            'frm.Items.Item("t_DocNum").Visible = False
                                            frm.Items.Item("7").Specific.Value = frmInspection.Items.Item("t_GRNNo").Specific.value
                                            frm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                        End If

                                    Else

                                        'Link button to load Parameter Master for Raw Material
                                        If pVal.BeforeAction Then

                                        Else
                                            oApplication.Menus.Item("2306").Activate()
                                            Dim frm As SAPbouiCOM.Form
                                            frm = oApplication.Forms.ActiveForm
                                            frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                            'frm.Items.Item("t_DocNum").Visible = False
                                            frm.Items.Item("8").Specific.Value = frmInspection.Items.Item("t_GRNNo").Specific.value
                                            frm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                        End If



                                    End If


                                Catch ex As Exception
                                    oApplication.StatusBar.SetText("et_ITEM_PRESSED Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

                                End Try


                            Case "156"
                                Try

                                    'Link button to load Parameter Master for Raw Material
                                    If pVal.BeforeAction Then

                                    Else


                                        oApplication.Menus.Item("AS_OWORD").Activate()
                                        Dim frm As SAPbouiCOM.Form
                                        frm = oApplication.Forms.GetForm("AS_OWORD", -1)
                                        frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                        'frm.Items.Item("t_DocNum").Visible = False
                                        frm.Items.Item("DocEntry").Visible = True
                                        frm.Items.Item("DocEntry").Enabled = True
                                        frm.Items.Item("DocEntry").Specific.Value = frmInspection.Items.Item("WBE").Specific.value
                                        frm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)



                                    End If



                                    'If pVal.BeforeAction = False Then
                                    '    Dim oForm As SAPbouiCOM.Form
                                    '    Dim Bool As Boolean = False

                                    '    For frm As Integer = 0 To oApplication.Forms.Count - 1
                                    '        If oApplication.Forms.Item(frm).UniqueID = "AS_OWORD" Then
                                    '            oForm = oApplication.Forms.Item("AS_OWORD")
                                    '            oForm.Close()
                                    '            Exit For
                                    '        End If
                                    '    Next
                                    '    If Bool = False Then
                                    '        oApplication.ActivateMenuItem("AS_OWORD")
                                    '        oForm = oApplication.Forms.Item("AS_OWORD")
                                    '        oForm.Select()
                                    '        oForm.Freeze(True)
                                    '        oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                    '        oForm.Items.Item("DocEntry").Enabled = True
                                    '        oForm.Items.Item("DocEntry").Specific.Value = Trim(frmInspection.Items.Item("WBE").Specific.value)
                                    '        oForm.Items.Item("1").Click()
                                    '        oForm.Freeze(False)
                                    '    End If


                                    'End If


                                    ''Link button to load Parameter Master for Raw Material
                                    'oApplication.Menus.Item("MNU_WORDER").Activate()
                                    'Dim frm As SAPbouiCOM.Form
                                    'frm = oApplication.Forms.ActiveForm
                                    ''frm.Items.Item("t_DocNum").Visible = False
                                    'frm.Items.Item("DocEntry").Enabled = True
                                    'frm.Items.Item("DocEntry").Specific.Value = frmInspection.Items.Item("WBE").Specific.value
                                    'frm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)


                                Catch ex As Exception
                                    oApplication.StatusBar.SetText("et_ITEM_PRESSED Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

                                End Try



                            Case "1"
                                If pVal.BeforeAction = False Then
                                    ' Me.InitForm()
                                    'Dim cmbType As SAPbouiCOM.ComboBox = oMatrix7.Columns.Item("6").Specific
                                    'Dim hoursType As String = cmbType.Selected.Value
                                    For i As Integer = 1 To oMatrix7.VisualRowCount
                                        Dim hoursType As String = oMatrix7.Columns.Item("6").Cells.Item(i).Specific.ToString()
                                    Next

                                End If
                            Case "btnDev"

                                If pVal.BeforeAction = False Then

                                    Dim TotalAcceptedQty As Double = 0
                                    For i As Integer = 1 To oMatrix5.VisualRowCount
                                        oMatrix5.Columns.Item("AccQty").Cells.Item(i).Specific.string = oMatrix5.Columns.Item("2").Cells.Item(i).Specific.string
                                        oMatrix5.Columns.Item("RejQty").Cells.Item(i).Specific.string = 0
                                        TotalAcceptedQty = TotalAcceptedQty + Convert.ToDouble(oMatrix5.Columns.Item("2").Cells.Item(i).Specific.string)
                                    Next
                                    frmInspection.Items.Item("32").Specific.value = TotalAcceptedQty.ToString()



                                    If oCompany.InTransaction = False Then oCompany.StartTransaction()
                                    Dim BoolPosting As Boolean = False
                                    BoolPosting = Me.StockPosting_LathingProcess()
                                    If BoolPosting = False Then
                                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                        BubbleEvent = False
                                    Else
                                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                    End If

                                End If



                            Case "154"
                                If (pVal.BeforeAction = False And frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) Then

                                    Dim oBatchSerialType As SAPbouiCOM.ComboBox = frmInspection.Items.Item("c_BSType").Specific

                                    Dim Type As String = oBatchSerialType.Selected.Value
                                    Dim FormType As String = frmInspection.Items.Item("97").Specific.value.ToString.Trim
                                    Dim GrnEntry As String = frmInspection.Items.Item("t_GRNEntry").Specific.value.ToString.Trim
                                    Dim ItemCode As String = frmInspection.Items.Item("118").Specific.value.ToString.Trim

                                    If (frmInspection.Items.Item("97").Specific.value.ToString.Trim = "GRN" Or frmInspection.Items.Item("97").Specific.value.ToString.Trim = "SC") Then

                                        If (frmInspection.Items.Item("97").Specific.value.ToString.Trim = "GRN") Then


                                            Dim GRNWhsCode As String = oGFun.getSingleValue("select d.whscode from OPDN m left outer join PDN1 d on m.docentry=d.docentry where m.docnum='" & frmInspection.Items.Item("t_GRNNo").Specific.value.ToString.Trim & "'  and d.itemcode='" & frmInspection.Items.Item("118").Specific.value.ToString.Trim & "'")
                                            Dim strsql As String = ""
                                            If (Type = "B") Then
                                                strsql = " EXEC [dbo].[@AIS_Inspection_GetBatchNumberDetailsForGRN] '" & Trim(ItemCode) & "','" & Trim(GRNWhsCode) & "','" & Trim(GrnEntry) & "','" & Trim(Type) & "'"
                                            ElseIf (Type = "S") Then
                                                strsql = " select a.[IntrSerial],a.ItemCode ,'1' qty   from (  " & _
                                                         " select T0.IntrSerial , t0.ItemCode " & _
                                                         " from OSRI  T0 INNER JOIN SRI1 T1 ON T0.ItemCode = T1.ItemCode  and T0.SysSerial = T1.SysSerial  " & _
                                                         " where T1.BaseType='20' and  T1.baseentry='" & frmInspection.Items.Item("t_GRNEntry").Specific.value.ToString.Trim & "'  and T1.WhsCode ='" & GRNWhsCode & "'  and T0.ItemCode ='" & frmInspection.Items.Item("118").Specific.value.ToString.Trim & "'  " & _
                                                         " and t0.Status='0' and T0.IntrSerial not in (select U_serialNo from [@MIPL_OINS]  m left outer join [@MIPL_INS5]  d on m.DocEntry =d.DocEntry where   m.U_ItemCode ='" & frmInspection.Items.Item("118").Specific.value.ToString.Trim & "'  " & _
                                                         " and d.U_SerialNo =T0.IntrSerial   and U_GRNEntry ='" & frmInspection.Items.Item("t_GRNEntry").Specific.value.ToString.Trim & "' and U_Type ='" & Type & "'  and m.Canceled <>'Y') " & _
                                                         " ) a "
                                            End If

                                            oOperationsList.LoadForm(strsql, FormType, Type)


                                        End If




                                    ElseIf (frmInspection.Items.Item("97").Specific.value.ToString.Trim = "IN") Then
                                        Dim oType As SAPbouiCOM.ComboBox = frmInspection.Items.Item("97").Specific

                                        Dim strsql As String = ""
                                        If (oBatchSerialType.Selected.Value = "B") Then
                                            strsql = " EXEC [dbo].[@AIS_ProductionReceipt_GetBatchDetails] '" & Trim(oDBDSHeader.GetValue("U_WBaseEntry", 0)) & "','" & Trim(oDBDSHeader.GetValue("U_ICode", 0)) & "'"
                                        ElseIf (oBatchSerialType.Selected.Value = "S") Then

                                        End If
                                        oOperationsList.LoadForm(strsql, oType.Selected.Value, oBatchSerialType.Selected.Value)

                                    End If

                                End If

                            Case "18"
                                If pVal.ActionSuccess Then frmInspection.PaneLevel = 1
                                'calInspectionQty()
                            Case "98"
                                If pVal.ActionSuccess Then
                                    frmInspection.PaneLevel = 2
                                    frmInspection.Settings.MatrixUID = "Matrix6"
                                    frmInspection.Settings.Enabled = True
                                End If

                            Case "98"
                                If pVal.ActionSuccess Then
                                    frmInspection.PaneLevel = 2
                                    frmInspection.Settings.MatrixUID = "Matrix6"
                                    frmInspection.Settings.Enabled = True
                                End If

                            Case "19"
                                If pVal.ActionSuccess Then frmInspection.PaneLevel = 3
                                frmInspection.Settings.MatrixUID = "55"


                            Case "20"
                                If pVal.ActionSuccess Then frmInspection.PaneLevel = 4
                                frmInspection.Settings.MatrixUID = "Matrix6"
                                frmInspection.Settings.Enabled = True

                            Case "67"
                                If pVal.ActionSuccess Then frmInspection.PaneLevel = 5
                            Case "15"
                                If pVal.ActionSuccess Then frmInspection.PaneLevel = 6
                                frmInspection.Settings.MatrixUID = "Matrix3"
                                frmInspection.Settings.Enabled = True


                            Case "120"
                                If pVal.ActionSuccess Then frmInspection.PaneLevel = 7
                                frmInspection.Settings.MatrixUID = "Matrix3"
                                frmInspection.Settings.Enabled = True

                            Case "146"
                                If pVal.ActionSuccess Then frmInspection.PaneLevel = 8
                                frmInspection.Settings.MatrixUID = "FG"
                                frmInspection.Settings.Enabled = True


                            Case "147"
                                If pVal.ActionSuccess Then frmInspection.PaneLevel = 9
                                frmInspection.Settings.MatrixUID = "MC"
                                frmInspection.Settings.Enabled = True


                            Case "148"
                                If pVal.ActionSuccess Then frmInspection.PaneLevel = 10
                                frmInspection.Settings.MatrixUID = "Batch"
                                frmInspection.Settings.Enabled = True

                            Case "149"
                                If pVal.ActionSuccess Then frmInspection.PaneLevel = 11
                                frmInspection.Settings.MatrixUID = "DNo"
                                frmInspection.Settings.Enabled = True




                            Case "b_Return"
                                Dim t_Return As String = frmInspection.Items.Item("t_Return").Specific.Value.ToString.Trim
                                Dim t_InvtfNo As String = frmInspection.Items.Item("t_InvtfNo").Specific.Value.ToString.Trim

                                If pVal.BeforeAction = False And t_Return.Trim = "" Then

                                    If oCompany.InTransaction = False Then oCompany.StartTransaction()
                                    Dim BoolPosting As Boolean = False
                                    BoolPosting = Me.StockPosting_LathingProcess()
                                    If BoolPosting = False Then
                                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                        BubbleEvent = False
                                    Else
                                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                    End If



                                End If

                            Case "lk_GRNNo"
                                'Link button on GRNNO
                                If pVal.BeforeAction = True Then
                                    oApplication.Menus.Item("2306").Activate()
                                Else
                                    Dim frm As SAPbouiCOM.Form
                                    frm = oApplication.Forms.ActiveForm
                                    frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                    frm.Items.Item("8").Specific.Value = oDBDSHeader.GetValue("U_GRNNo", 0)
                                    frm.Items.Item("1").Click()
                                End If

                            Case "lk_Return"
                                'Link Button on Return no
                                If pVal.BeforeAction Then
                                    oApplication.Menus.Item("2307").Activate()
                                Else
                                    Dim frm As SAPbouiCOM.Form
                                    frm = oApplication.Forms.ActiveForm
                                    frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                    frm.Items.Item("8").Specific.Value = oDBDSHeader.GetValue("U_Return", 0)
                                    frm.Items.Item("1").Click()
                                End If

                            Case "lk_ProNo"
                                'Link button on Production no
                                If pVal.BeforeAction Then
                                    oApplication.Menus.Item("4369").Activate()
                                Else
                                    Dim frm As SAPbouiCOM.Form
                                    frm = oApplication.Forms.ActiveForm
                                    frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                    frm.Items.Item("18").Specific.Value = oDBDSHeader.GetValue("U_ProNo", 0)
                                    frm.Items.Item("1").Click()
                                End If

                            Case "lk_IndNo"
                                'Link button on Indent no
                                If pVal.BeforeAction Then
                                    oApplication.Menus.Item("IND").Activate()
                                Else
                                    Dim frm As SAPbouiCOM.Form
                                    frm = oApplication.Forms.ActiveForm
                                    frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                    frm.Items.Item("11").Specific.Value = oDBDSHeader.GetValue("U_IndNo", 0)
                                    frm.Items.Item("1").Click()
                                End If

                            Case "lk_WIPNo"
                                'Link button on WIP No
                                If pVal.BeforeAction Then
                                    oApplication.Menus.Item("WIP").Activate()
                                Else
                                    Dim frm As SAPbouiCOM.Form
                                    frm = oApplication.Forms.ActiveForm
                                    frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                    frm.Items.Item("t_DocNum").Specific.Value = oDBDSHeader.GetValue("U_WIPNo", 0)
                                    frm.Items.Item("1").Click()
                                End If

                                'Changed on 13/06/2013
                            Case "b_Print"
                                If pVal.BeforeAction = False Then
                                    'Show EOU Report
                                    Dim PrintType = oGFun.getSingleValue("Select isnull(U_PType,'') from OADM")
                                    If PrintType = "EOU" Then
                                        showprint_EOUreport()
                                    Else
                                        showprint_DTAreport()
                                    End If
                                End If

                                'Case "b_Print1"
                                '    If pVal.BeforeAction = False Then
                                '        'Show EOU Report
                                '        showprint_DTAreport()
                                '    End If
                        End Select
                    Catch ex As Exception
                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                'Add,Update Event
                                If pVal.BeforeAction = True And frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    If Me.ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Exit Sub
                                    Else
                                        If oCompany.InTransaction = False Then oCompany.StartTransaction()
                                        ' Dim BoolPosting As Boolean = False
                                        Dim BoolPosting As Boolean = Me.StockPosting_LathingProcess()
                                        If BoolPosting = False Then
                                            If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                            BubbleEvent = False
                                        Else
                                            If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                        End If
                                    End If
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        BubbleEvent = False
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS

                    Select Case pVal.ItemUID

                        Case "24"
                            Try
                                Dim ItemCode = oDBDSHeader.GetValue("U_ICode", 0)
                                If ItemCode = "" Then
                                    frmInspection.Items.Item("19").Visible = False
                                    frmInspection.Items.Item("20").Visible = False
                                    frmInspection.Items.Item("88").Visible = False
                                    frmInspection.Items.Item("89").Visible = False
                                Else
                                    '05/06/2013
                                    SelectSeries()
                                End If
                            Catch ex As Exception
                                oApplication.StatusBar.SetText("Lost Focus - (Itemcode) Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            End Try

                        Case "30"
                            Try

                                Dim InsQty = oDBDSHeader.GetValue("U_InsQty", 0).ToString.Trim
                                Dim ItemCode = oDBDSHeader.GetValue("U_ICode", 0).ToString.Trim
                                Dim Sampling = oGFun.getSingleValue("select top 1 isnull(OQCCP.U_Enforce,'N') from [@MIPL_OQCCP] OQCCP where " & _
                                                        "OQCCP.U_OprCode = '" & ItemCode & "'")
                                Sampling = IIf(Sampling.ToString.Trim = "", "N", Sampling)

                                If InsQty = "" Then InsQty = 0


                            Catch ex As Exception
                                oApplication.StatusBar.SetText("Lost Focus (Inspection Qty) Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            End Try



                    End Select

                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "Matrix3"

                                'Attachment 05.06.2013
                                If pVal.BeforeAction And oMatrix3.VisualRowCount = pVal.Row Then
                                    oGFun.SetNewLine(oMatrix3, oDBDSDetail3)
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Double Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    End Try



                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
                    Try
                        Select Case pVal.ItemUID And pVal.ColUID
                            'Changed by Sankaralakshmi
                            Case "55" And "2"
                                'Link button to load Parameter Master for Raw Material
                                If pVal.BeforeAction Then
                                    oApplication.Menus.Item("QPM").Activate()
                                Else
                                    Dim frm As SAPbouiCOM.Form
                                    frm = oApplication.Forms.ActiveForm
                                    frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                    frm.Items.Item("t_Code1").Specific.Value = oMatrix.GetCellSpecific("2", pVal.Row).Value
                                    frm.Items.Item("1").Click()
                                End If

                            Case "56" And "2"
                                'Link button to load Parameter master for BoughtOut Material
                                If pVal.BeforeAction Then
                                    oApplication.Menus.Item("QPM").Activate()
                                Else
                                    Dim frm As SAPbouiCOM.Form
                                    frm = oApplication.Forms.ActiveForm
                                    frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                    frm.Items.Item("t_Code1").Specific.Value = oMatrix2.GetCellSpecific("2", pVal.Row).Value
                                    frm.Items.Item("1").Click()
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Link Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    'Goods Return for GRN
    'Function GoodsReturn_GRN() As Boolean
    '    'If oCompany.InTransaction = False Then oCompany.StartTransaction()
    '    Try

    '        Dim oPurchaseReturns As SAPbobsCOM.Documents = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseReturns)

    '        Dim ItemCode = oDBDSHeader.GetValue("U_ICode", 0).Trim
    '        Dim FrmWorkCenter = oGFun.getSingleValue("Select Top 1 WhsCode from OPDN, PDN1 where OPDN.DocEntry = PDN1.DocEntry and OPDN.DocNum = '" & _
    '                                               oDBDSHeader.GetValue("U_GRNNo", 0).Trim & "' and PDN1.ItemCode = '" & ItemCode & "' ")
    '        Dim BaseLine = oGFun.getSingleValue("Select Top 1 LineNum from OPDN, PDN1 where OPDN.DocEntry = PDN1.DocEntry and OPDN.DocNum = '" & _
    '                                               oDBDSHeader.GetValue("U_GRNNo", 0).Trim & "' and PDN1.ItemCode = '" & ItemCode & "' ")
    '        Dim CardCode = oGFun.getSingleValue("Select CardCode from OPDN where OPDN.DocNum = '" & _
    '                                              oDBDSHeader.GetValue("U_GRNNo", 0).Trim & "'")

    '        Dim RejQty = oDBDSHeader.GetValue("U_RejQty", 0)
    '        RejQty = IIf(RejQty.Trim = "", 0, RejQty)

    '        Dim RewQty = oDBDSHeader.GetValue("U_RewQty", 0)
    '        RewQty = IIf(RewQty.Trim = "", 0, RewQty)

    '        Dim Qty As Double
    '        Qty = CDbl(RejQty)

    '        Dim DocNum = oDBDSHeader.GetValue("DocNum", 0).Trim
    '        Dim DocEntry = oDBDSHeader.GetValue("DocEntry", 0).Trim
    '        Dim BaseEntry = oGFun.getSingleValue("Select DocEntry from OPDN where DocNum = '" & oDBDSHeader.GetValue("U_GRNNo", 0).Trim & "'")

    '        If Qty > 0 Then

    '            oPurchaseReturns.CardCode = CardCode
    '            oPurchaseReturns.Comments = "Stock Posted From QC DocNum -> " & DocNum
    '            oPurchaseReturns.Reference2 = DocNum
    '            oPurchaseReturns.DocDate = Date.Now

    '            oPurchaseReturns.Lines.BaseEntry = BaseEntry
    '            oPurchaseReturns.Lines.BaseLine = BaseLine
    '            oPurchaseReturns.Lines.BaseType = SAPbobsCOM.BoObjectTypes.oPurchaseDeliveryNotes
    '            oPurchaseReturns.Reference2 = DocNum
    '            oPurchaseReturns.DocDate = Date.Now
    '            oPurchaseReturns.Lines.ItemCode = ItemCode

    '            oPurchaseReturns.Lines.Quantity = Qty
    '            oPurchaseReturns.Lines.WarehouseCode = FrmWorkCenter

    '            'Batch Number Allocation
    '            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '            Dim ManBtchNum = oGFun.getSingleValue("Select ManBtchNum  from OITM Where ItemCode='" & ItemCode & "'")
    '            If ManBtchNum.Trim <> "N" Then

    '                Dim rs As SAPbobsCOM.Recordset = oGFun.DoQuery("select * from OBTQ where ItemCode ='" & _
    '                                                          ItemCode & _
    '                                                          "' and WhsCode ='" & FrmWorkCenter & _
    '                                                          "' And Quantity > 0 order by SysNumber")

    '                Dim count As Double = CDbl(Qty)

    '                For k As Integer = 0 To rs.RecordCount - 1
    '                    If CDbl(rs.Fields.Item("Quantity").Value) >= count Then
    '                        Dim ss = rs.Fields.Item("SysNumber").Value
    '                        Dim BatchNumber = _
    '                    oGFun.getSingleValue("select DistNumber from OBTN where ItemCode ='" & _
    '                                   ItemCode & _
    '                                   "' and SysNumber ='" & rs.Fields.Item("SysNumber").Value & "'")

    '                        oPurchaseReturns.Lines.BatchNumbers.BatchNumber = BatchNumber
    '                        oPurchaseReturns.Lines.BatchNumbers.Quantity = count
    '                        oPurchaseReturns.Lines.BatchNumbers.Add()
    '                        Exit For
    '                    Else
    '                        Dim BatchNumber = _
    '                    oGFun.getSingleValue("select DistNumber from OBTN where ItemCode ='" & _
    '                                   ItemCode & _
    '                                   "' and SysNumber ='" & rs.Fields.Item("SysNumber").Value & "'")

    '                        oPurchaseReturns.Lines.BatchNumbers.BatchNumber = BatchNumber
    '                        oPurchaseReturns.Lines.BatchNumbers.Quantity = rs.Fields.Item("Quantity").Value
    '                        oPurchaseReturns.Lines.BatchNumbers.Add()
    '                        count = count - CDbl(rs.Fields.Item("Quantity").Value)

    '                    End If
    '                    rs.MoveNext()
    '                Next
    '            End If

    '            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '            oPurchaseReturns.Lines.Add()
    '        End If

    '        Dim ErrCode = oPurchaseReturns.Add

    '        If ErrCode <> 0 Then
    '            If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
    '            oGFun.Msg(" GRN Return Posting Error : " & oCompany.GetLastErrorDescription)
    '            Return False
    '        Else
    '            Dim ReturnNo = oGFun.getSingleValue(" Select DocNum from ORPD where DocEntry = (Select Max(DocEntry) from ORPD)")
    '            Dim RetEntry = oGFun.getSingleValue("Select Max(DocEntry) from ORPD")

    '            oGFun.DoExQuery("Update [@MIPL_OINS] set U_Return = " & ReturnNo & ",U_RetEntry = '" & RetEntry & "' where DocEntry = '" & DocEntry & "' ")
    '            frmInspection.Items.Item("b_Return"). _
    '              SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, _
    '              frmInspection.Mode, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
    '            frmInspection.Items.Item("t_Return").Specific.Value = ReturnNo
    '            frmInspection.Items.Item("t_RetEntry").Specific.Value = RetEntry

    '            frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        oApplication.StatusBar.SetText("Goods Return for GRN Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
    '        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
    '    Finally
    '    End Try
    '    'If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
    'End Function

    'Stock Posting GRN
    'Function StockPosting_GRN() As Boolean
    '    Try
    '        Dim oStockTransfer As SAPbobsCOM.StockTransfer = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oStockTransfer)
    '        Dim ItemCode = oDBDSHeader.GetValue("U_ICode", 0).Trim
    '        Dim FrmWorkCenter = oGFun.getSingleValue("Select Top 1 WhsCode from OPDN, PDN1 where OPDN.DocEntry = PDN1.DocEntry and OPDN.DocNum = '" & _
    '                                               oDBDSHeader.GetValue("U_GRNNo", 0).Trim & "' and PDN1.ItemCode = '" & ItemCode & "' ")
    '        Dim ToWorkCenter = oGFun.getSingleValue("Select Top 1 U_WhsCode from OPDN, PDN1 where OPDN.DocEntry = PDN1.DocEntry and OPDN.DocNum = '" & _
    '                                               oDBDSHeader.GetValue("U_GRNNo", 0).Trim & "' and PDN1.ItemCode = '" & ItemCode & "' ")



    '        Dim RejectedWarehouse As String = oGFun.getSingleValue(" SELECT top 1 case when (select Excisable from OITM where Itemcode='" & ItemCode & "' )='N' then U_RejWhCode else U_RejWhCode  end  FROM [dbo].[@MIPL_QCGT1]   where   isnull(U_Active,'N') = 'Y'")
    '        Dim ReworkWarehouse As String = oGFun.getSingleValue(" SELECT top 1  case when (select Excisable from OITM where Itemcode='" & ItemCode & "' )='N' then U_RewWhCode else U_RewWhCode end   FROM [dbo].[@MIPL_QCGT1]  where   isnull(U_Active,'N') = 'Y'")





    '        Dim DocNum = oDBDSHeader.GetValue("DocNum", 0).ToString.Trim
    '        Dim DocEntry = oDBDSHeader.GetValue("DocEntry", 0).ToString.Trim
    '        Dim AccQty = oDBDSHeader.GetValue("U_AccQty", 0).ToString.Trim
    '        AccQty = IIf(AccQty.Trim = "", 0, AccQty)

    '        Dim PartAcc = oDBDSHeader.GetValue("U_PartAcc", 0).ToString.Trim
    '        PartAcc = IIf(PartAcc.Trim = "", 0, PartAcc)

    '        Dim Qty As Double
    '        Qty = CDbl(AccQty) + CDbl(PartAcc)

    '        Dim RejQty = oDBDSHeader.GetValue("U_RejQty", 0).ToString.Trim
    '        Dim RewQty = oDBDSHeader.GetValue("U_RewQty", 0).ToString.Trim

    '        oStockTransfer.Comments = "Stock Posted From QC DocNum -> " & DocNum
    '        oStockTransfer.Reference2 = DocNum
    '        oStockTransfer.DocDate = Date.Now
    '        oStockTransfer.FromWarehouse = FrmWorkCenter
    '        oStockTransfer.UserFields.Fields.Item("U_BaseNum").Value = oDBDSHeader.GetValue("DocNum", 0)
    '        oStockTransfer.UserFields.Fields.Item("U_BaseObject").Value = "INS"
    '        oStockTransfer.UserFields.Fields.Item("U_Series").Value = oDBDSHeader.GetValue("Series", 0)


    '        If CDbl(Qty) > 0 Then
    '            oStockTransfer.Lines.ItemCode = ItemCode
    '            oStockTransfer.Lines.Quantity = Qty
    '            oStockTransfer.Lines.WarehouseCode = ToWorkCenter

    '            'Batch Number Allocation for Acc Qty
    '            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '            Dim ManBtchNumAcc = oGFun.getSingleValue("Select ManBtchNum  from OITM Where ItemCode='" & ItemCode & "'")

    '            If ManBtchNumAcc.Trim <> "N" Then

    '                Dim rs As SAPbobsCOM.Recordset = oGFun.DoQuery("select * from OBTQ where ItemCode ='" & _
    '                                                          ItemCode & _
    '                                                          "' and WhsCode ='" & FrmWorkCenter & _
    '                                                          "' And Quantity > 0 order by SysNumber")

    '                Dim count As Double = CDbl(Qty)

    '                For k As Integer = 0 To rs.RecordCount - 1
    '                    If CDbl(rs.Fields.Item("Quantity").Value) >= count Then
    '                        Dim ss = rs.Fields.Item("SysNumber").Value

    '                        oStockTransfer.Lines.BatchNumbers.BatchNumber = _
    '                   oGFun.getSingleValue("select DistNumber from OBTN where ItemCode ='" & _
    '                                   ItemCode & _
    '                                   "' and SysNumber ='" & rs.Fields.Item("SysNumber").Value & "'")
    '                        oStockTransfer.Lines.BatchNumbers.Quantity = count
    '                        oStockTransfer.Lines.BatchNumbers.Add()
    '                        Exit For
    '                    Else

    '                        oStockTransfer.Lines.BatchNumbers.BatchNumber = _
    '                   oGFun.getSingleValue("select DistNumber from OBTN where ItemCode ='" & _
    '                                   ItemCode & _
    '                                   "' and SysNumber ='" & rs.Fields.Item("SysNumber").Value & "'")

    '                        oStockTransfer.Lines.BatchNumbers.Quantity = rs.Fields.Item("Quantity").Value
    '                        oStockTransfer.Lines.BatchNumbers.Add()
    '                        count = count - CDbl(rs.Fields.Item("Quantity").Value)

    '                    End If
    '                    rs.MoveNext()
    '                Next
    '            End If

    '            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '            oStockTransfer.Lines.Add()

    '        End If

    '        'For RewQty
    '        If RewQty > 0 Then
    '            oStockTransfer.Lines.ItemCode = ItemCode
    '            oStockTransfer.Lines.Quantity = RewQty
    '            oStockTransfer.Lines.WarehouseCode = ReworkWarehouse

    '            'Batch Number Allocation
    '            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '            Dim ManBtchNumRew = oGFun.getSingleValue("Select ManBtchNum  from OITM Where ItemCode='" & ItemCode & "'")

    '            If ManBtchNumRew.Trim <> "N" Then

    '                Dim rs As SAPbobsCOM.Recordset = oGFun.DoQuery("select * from OBTQ where ItemCode ='" & _
    '                                                          ItemCode & _
    '                                                          "' and WhsCode ='" & FrmWorkCenter & _
    '                                                          "' And Quantity > 0 order by SysNumber")

    '                Dim count As Double = CDbl(RewQty)

    '                For k As Integer = 0 To rs.RecordCount - 1
    '                    If CDbl(rs.Fields.Item("Quantity").Value) >= count Then
    '                        Dim ss = rs.Fields.Item("SysNumber").Value

    '                        oStockTransfer.Lines.BatchNumbers.BatchNumber = _
    '                   oGFun.getSingleValue("select DistNumber from OBTN where ItemCode ='" & _
    '                                   ItemCode & _
    '                                   "' and SysNumber ='" & rs.Fields.Item("SysNumber").Value & "'")
    '                        oStockTransfer.Lines.BatchNumbers.Quantity = count
    '                        oStockTransfer.Lines.BatchNumbers.Add()
    '                        Exit For
    '                    Else

    '                        oStockTransfer.Lines.BatchNumbers.BatchNumber = _
    '                   oGFun.getSingleValue("select DistNumber from OBTN where ItemCode ='" & _
    '                                   ItemCode & _
    '                                   "' and SysNumber ='" & rs.Fields.Item("SysNumber").Value & "'")

    '                        oStockTransfer.Lines.BatchNumbers.Quantity = rs.Fields.Item("Quantity").Value
    '                        oStockTransfer.Lines.BatchNumbers.Add()
    '                        count = count - CDbl(rs.Fields.Item("Quantity").Value)

    '                    End If
    '                    rs.MoveNext()
    '                Next
    '            End If

    '            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    '            oStockTransfer.Lines.Add()
    '        End If
    '        'For RewQty
    '        If RejQty > 0 Then
    '            oStockTransfer.Lines.ItemCode = ItemCode
    '            oStockTransfer.Lines.Quantity = RejQty
    '            oStockTransfer.Lines.WarehouseCode = RejectedWarehouse

    '            'Batch Number Allocation
    '            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '            Dim ManBtchNumRew = oGFun.getSingleValue("Select ManBtchNum  from OITM Where ItemCode='" & ItemCode & "'")

    '            If ManBtchNumRew.Trim <> "N" Then

    '                Dim rs As SAPbobsCOM.Recordset = oGFun.DoQuery("select * from OBTQ where ItemCode ='" & _
    '                                                          ItemCode & _
    '                                                          "' and WhsCode ='" & FrmWorkCenter & _
    '                                                          "' And Quantity > 0 order by SysNumber")

    '                Dim count As Double = CDbl(RejQty)

    '                For k As Integer = 0 To rs.RecordCount - 1
    '                    If CDbl(rs.Fields.Item("Quantity").Value) >= count Then
    '                        Dim ss = rs.Fields.Item("SysNumber").Value

    '                        oStockTransfer.Lines.BatchNumbers.BatchNumber = _
    '                   oGFun.getSingleValue("select DistNumber from OBTN where ItemCode ='" & _
    '                                   ItemCode & _
    '                                   "' and SysNumber ='" & rs.Fields.Item("SysNumber").Value & "'")
    '                        oStockTransfer.Lines.BatchNumbers.Quantity = count
    '                        oStockTransfer.Lines.BatchNumbers.Add()
    '                        Exit For
    '                    Else

    '                        oStockTransfer.Lines.BatchNumbers.BatchNumber = _
    '                   oGFun.getSingleValue("select DistNumber from OBTN where ItemCode ='" & _
    '                                   ItemCode & _
    '                                   "' and SysNumber ='" & rs.Fields.Item("SysNumber").Value & "'")

    '                        oStockTransfer.Lines.BatchNumbers.Quantity = rs.Fields.Item("Quantity").Value
    '                        oStockTransfer.Lines.BatchNumbers.Add()
    '                        count = count - CDbl(rs.Fields.Item("Quantity").Value)

    '                    End If
    '                    rs.MoveNext()
    '                Next
    '            End If

    '            '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    '            oStockTransfer.Lines.Add()
    '        End If


    '        Dim ErrCode = oStockTransfer.Add()

    '        If ErrCode <> 0 Then
    '            oGFun.Msg(" QC Posting Error : " & oCompany.GetLastErrorDescription)
    '            Return False
    '        End If

    '        '' ''Updation of InventoryTransfer No.
    '        'Dim InvtfNo = oGFun.getSingleValue(" Select DocNum from OWTR where DocEntry = (Select Max(DocEntry) from OWTR)")
    '        'InvtfNo = IIf(InvtfNo.ToString.Trim = "", 0, InvtfNo)
    '        'Dim InvEntry = oGFun.getSingleValue(" Select Max(DocEntry) from OWTR")
    '        'InvEntry = IIf(InvEntry.ToString.Trim = "", 0, InvEntry)

    '        'Dim rsQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
    '        'rsQuery.DoQuery("Update [@MIPL_OINS] set U_InvtfNo = " & InvtfNo & " ,U_InvEntry = '" & InvEntry & "' where DocEntry = '" & DocEntry & "' ")

    '        'frmInspection.Items.Item("t_InvtfNo").Specific.Value = InvtfNo
    '        'frmInspection.Items.Item("t_InvEntry").Specific.Value = InvEntry

    '        Return True
    '        'oApplication.MessageBox("Finished")
    '    Catch ex As Exception
    '        oGFun.Msg(" GRN Stock Posting Methord Faild " & ex.Message)
    '        Return False
    '    End Try
    'End Function

    Sub TypeWiseControlAlloc()
        Try
            Dim oCmbSerial As SAPbouiCOM.ComboBox = frmInspection.Items.Item("97").Specific
            Dim strSerialCode As String = oCmbSerial.Value.ToString.Trim

            If strSerialCode.ToUpper.Trim = "IN" Then

                frmInspection.Items.Item("t_GRNNo").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                frmInspection.Items.Item("135").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                frmInspection.Items.Item("134").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                frmInspection.Items.Item("130").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                frmInspection.Items.Item("18").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                ' frmInspection.Items.Item("98").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                frmInspection.Items.Item("55").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                frmInspection.Items.Item("149").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                frmInspection.Items.Item("146").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                frmInspection.Items.Item("147").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                'frmInspection.Items.Item("148").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)


            Else
                frmInspection.Items.Item("t_GRNNo").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                frmInspection.Items.Item("135").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                frmInspection.Items.Item("134").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                frmInspection.Items.Item("130").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                frmInspection.Items.Item("18").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                ' frmInspection.Items.Item("98").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                frmInspection.Items.Item("55").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                frmInspection.Items.Item("149").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                frmInspection.Items.Item("146").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                frmInspection.Items.Item("147").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                'frmInspection.Items.Item("148").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            End If






        Catch ex As Exception
            oApplication.StatusBar.SetText("TypeWiseControlAlloc Methord Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub

    Sub CheckEnforceSamplingDetailsInspection(ItemCode As String)
        Try
            Dim rs As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim StrSql As String = ""


            Dim sCustCode As String = frmInspection.Items.Item("t_CCode").Specific.value
            If ItemCode <> "" Then
                frmInspection.Items.Item("t_SamplQty").Enabled = True
                Dim U_Recno As String = frmInspection.Items.Item("134").Specific.value
                StrSql = "EXEC DBO.[@AIS_QC_GetFGInspectionDetails]  '" & ItemCode & "','" + U_Recno + "','" + sCustCode + "'"
                rs.DoQuery(StrSql)
                If rs.RecordCount > 0 Then
                    oDBDSDetail7.Clear()
                End If
                For i As Integer = 1 To rs.RecordCount
                    oDBDSDetail7.InsertRecord(oDBDSDetail7.Size)
                    oDBDSDetail7.Offset = oDBDSDetail7.Size - 1
                    oDBDSDetail7.SetValue("LineId", oDBDSDetail7.Size - 1, i)
                    oDBDSDetail7.SetValue("U_SNo", oDBDSDetail7.Size - 1, rs.Fields.Item("U_No").Value)
                    oDBDSDetail7.SetValue("U_PCode", oDBDSDetail7.Size - 1, rs.Fields.Item("U_PCode").Value)
                    oDBDSDetail7.SetValue("U_Des", oDBDSDetail7.Size - 1, rs.Fields.Item("U_PName").Value)
                    oDBDSDetail7.SetValue("U_PType", oDBDSDetail7.Size - 1, rs.Fields.Item("U_Type").Value)
                    oDBDSDetail7.SetValue("U_Unit", oDBDSDetail7.Size - 1, rs.Fields.Item("U_Unit").Value)
                    oDBDSDetail7.SetValue("U_lLimit", oDBDSDetail7.Size - 1, rs.Fields.Item("U_Lowlimit").Value)
                    oDBDSDetail7.SetValue("U_SubType", oDBDSDetail7.Size - 1, rs.Fields.Item("SubType").Value)
                    oDBDSDetail7.SetValue("U_ulimit", oDBDSDetail7.Size - 1, rs.Fields.Item("U_Uplimit").Value)
                    oDBDSDetail7.SetValue("U_MhdChek", oDBDSDetail7.Size - 1, rs.Fields.Item("U_ChkMethd").Value)
                    oDBDSDetail7.SetValue("U_NoOfObser", oDBDSDetail7.Size - 1, rs.Fields.Item("U_NoOfObser").Value)
                    oDBDSDetail7.SetValue("U_Correlation", oDBDSDetail7.Size - 1, rs.Fields.Item("U_Correlation").Value)
                    oDBDSDetail7.SetValue("U_Reference", oDBDSDetail7.Size - 1, rs.Fields.Item("U_RefLoad").Value)


                    rs.MoveNext()
                Next
                oMatrix7.LoadFromDataSourceEx()
                frmInspection.Items.Item("88").Visible = False
                frmInspection.Items.Item("89").Visible = False


                StrSql = "Exec [dbo].[@AIS_QC_Inspection_GetControlSamplePlanDetails]'" + ItemCode + "','" + sCustCode + "' "
                rs.DoQuery(StrSql)
                If rs.RecordCount > 0 Then
                    rs.MoveFirst()

                End If
                For i As Integer = 1 To rs.RecordCount
                    Dim ssampleQTy As String = rs.Fields.Item("U_SampSize").Value.ToString()
                    oDBDSHeader.SetValue("U_SamplQty", 0, ssampleQTy)

                Next



            Else
                frmInspection.Items.Item("19").Visible = False
                frmInspection.Items.Item("88").FromPane = "1"
                frmInspection.Items.Item("89").ToPane = "1"
                frmInspection.Items.Item("88").Visible = True
                frmInspection.Items.Item("89").Visible = True
                frmInspection.Items.Item("89").Specific.Value = "As per ISO 2500 Part - I"
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText("CheckEnforceSamplingDetails Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try

    End Sub
    Sub CheckEnforceSamplingDetailsRawMaterial(ItemCode As String)
        Try
            Dim rs As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)


            Dim StrSql As String = ""
            Dim Enforce = oGFun.getSingleValue("select top 1 isnull(OQCCP.U_Enforce,'N') from [@MIPL_OQCCP] OQCCP where " & _
                                                "OQCCP.U_OprCode = '" & ItemCode & "'")

            Dim sCustCode As String = frmInspection.Items.Item("t_CCode").Specific.value

            Enforce = IIf(Enforce.ToString.Trim = "", "N", Enforce)
            frmInspection.Items.Item("19").Visible = True
            If ItemCode <> "" Then
                'For Raw Material
                frmInspection.Items.Item("t_SamplQty").Enabled = True
                StrSql = "Exec [dbo].[@AIS_QC_Inspection_GetControlPlanDetails]'" + ItemCode + "','" + sCustCode + "' "
                rs.DoQuery(StrSql)
                oDBDSDetail1.Clear()
                For i As Integer = 1 To rs.RecordCount
                    oDBDSDetail1.InsertRecord(oDBDSDetail1.Size)
                    oDBDSDetail1.Offset = oDBDSDetail1.Size - 1
                    oDBDSDetail1.SetValue("LineId", oDBDSDetail1.Size - 1, i)
                    oDBDSDetail1.SetValue("U_PCode", oDBDSDetail1.Size - 1, rs.Fields.Item("U_PCode").Value)
                    oDBDSDetail1.SetValue("U_PName", oDBDSDetail1.Size - 1, rs.Fields.Item("U_PName").Value)
                    oDBDSDetail1.SetValue("U_Type", oDBDSDetail1.Size - 1, rs.Fields.Item("U_Type").Value)
                    oDBDSDetail1.SetValue("U_Unit", oDBDSDetail1.Size - 1, rs.Fields.Item("U_Unit").Value)
                    oDBDSDetail1.SetValue("U_Lowlimit", oDBDSDetail1.Size - 1, rs.Fields.Item("U_Lowlimit").Value)
                    oDBDSDetail1.SetValue("U_Uplimit", oDBDSDetail1.Size - 1, rs.Fields.Item("U_Uplimit").Value)
                    oDBDSDetail1.SetValue("U_ChkMethd", oDBDSDetail1.Size - 1, rs.Fields.Item("U_ChkMethd").Value)
                    oDBDSDetail1.SetValue("U_Reference", oDBDSDetail1.Size - 1, rs.Fields.Item("U_RefLoad").Value)
                    oDBDSDetail1.SetValue("U_Correlation", oDBDSDetail1.Size - 1, rs.Fields.Item("U_Correlation").Value)
                    oDBDSDetail1.SetValue("U_Uplimit", oDBDSDetail1.Size - 1, rs.Fields.Item("U_Uplimit").Value)
                    oDBDSDetail1.SetValue("U_ChkMethd", oDBDSDetail1.Size - 1, rs.Fields.Item("U_ChkMethd").Value)
                    oDBDSDetail1.SetValue("U_NoOfObser", oDBDSDetail1.Size - 1, rs.Fields.Item("U_NoOfObser").Value)
                    oDBDSDetail1.SetValue("U_SubType", oDBDSDetail1.Size - 1, rs.Fields.Item("SubType").Value)
                    rs.MoveNext()
                Next i

                oMatrix.LoadFromDataSourceEx()


                StrSql = "Exec [dbo].[@AIS_QC_Inspection_GetControlSamplePlanDetails]'" + ItemCode + "','" + sCustCode + "' "
                rs.DoQuery(StrSql)
                If rs.RecordCount > 0 Then
                    rs.MoveFirst()

                End If
                For i As Integer = 1 To rs.RecordCount
                    Dim ssampleQTy As String = rs.Fields.Item("U_SampSize").Value.ToString()
                    oDBDSHeader.SetValue("U_SamplQty", 0, ssampleQTy)

                Next
                frmInspection.Items.Item("88").Visible = False
                frmInspection.Items.Item("89").Visible = False

            Else
                'For Raw Material
                frmInspection.Items.Item("19").Visible = False

                'Visible Sample Reference
                'Sample Ref
                frmInspection.Items.Item("88").FromPane = "1"
                frmInspection.Items.Item("89").ToPane = "1"
                frmInspection.Items.Item("88").Visible = True
                frmInspection.Items.Item("89").Visible = True

                frmInspection.Items.Item("89").Specific.Value = "As per ISO 2500 Part - I"
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText("CheckEnforceSamplingDetails Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try

    End Sub
    Sub GenerateSampleSizeColumns(ByVal itemcode As String, ByVal insQty As Double)
        Try


            Dim StrSql As String = ""

            oMatrix2.Clear()
            ODBDSDetail2.Clear()

            Dim SamplSize = oGFun.getSingleValue("Select isnull(QCCP2.U_SampSize,0) from [@MIPL_OQCCP] OQCCP,[@MIPL_QCCP2] QCCP2 " & _
                                    " where OQCCP.Code= QCCP2.code and OQCCP.U_OprCode='" & itemcode & "' and OQCCP.U_Enforce ='Y' " & _
                                      "and '" & insQty & "' between (QCCP2.U_LtFrmSiz) and (QCCP2.U_LtToSize) ")

            If SamplSize = "" Then SamplSize = 0

            'Assigned Sampling qty
            frmInspection.Items.Item("t_SamplQty").Specific.value = SamplSize

            If CDbl(SamplSize) <= 32 Then

                'Remove Columns
                Dim n As Integer = 1
10:

                For i As Integer = 0 To oMatrix2.Columns.Count - 1
                    Dim aa = oMatrix2.Columns.Item(i).UniqueID
                    If oMatrix2.Columns.Item(i).UniqueID = "O_" & n Then
                        oMatrix2.Columns.Remove(i)
                        n = n + 1
                        GoTo 10
                    End If
                Next

                'Add Columns
                For i As Integer = 1 To CDbl(SamplSize)
                    Try
                        oMatrix2.Columns.Add("O_" & i, SAPbouiCOM.BoFormItemTypes.it_EDIT)
                    Catch ex As Exception
                    End Try
                    oMatrix2.Columns.Item("O_" & i).TitleObject.Caption = "Observation " & i
                    oMatrix2.Columns.Item("O_" & i).Width = 75
                    oMatrix2.Columns.Item("O_" & i).DataBind.SetBound(True, "@MIPL_INS2", "U_O" & i)
                Next i

            Else
                oGFun.Msg("Columns cannot create : Sampling Qty is Out of Boundaries")
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText("GenerateSampleColumns Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try

    End Sub

    'Sub LoadParameterDetails(ByVal itemcode As String)
    '    Try
    '        Dim rs As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

    '        'Load Parameter Details
    '        StrSql = "select QCCP1.U_PCode,QCCP1.U_PName,QCCP1.U_Type,QCCP1.U_Unit,QCCP1.U_Lowlimit,QCCP1.U_Uplimit,QCCP1.U_ChkMethd " & _
    '                "from [@MIPL_OQCCP] OQCCP,[@MIPL_QCCP1] QCCP1 where OQCCP.Code=QCCP1.Code and " & _
    '                "OQCCP.U_OprCode ='" & itemcode & "' and isnull(QCCP1.U_Active,'N') ='Y' "

    '        rs.DoQuery(StrSql)

    '        ODBDSDetail2.Clear()

    '        For j As Integer = 1 To rs.RecordCount

    '            'Fill the BoughtOutMaterial from Control Panel

    '            ODBDSDetail2.InsertRecord(ODBDSDetail2.Size)
    '            ODBDSDetail2.Offset = ODBDSDetail2.Size - 1
    '            ODBDSDetail2.SetValue("LineId", ODBDSDetail2.Size - 1, j)
    '            ODBDSDetail2.SetValue("U_PCode", ODBDSDetail2.Size - 1, rs.Fields.Item("U_PCode").Value)
    '            ODBDSDetail2.SetValue("U_PName", ODBDSDetail2.Size - 1, rs.Fields.Item("U_PName").Value)
    '            ODBDSDetail2.SetValue("U_Type", ODBDSDetail2.Size - 1, rs.Fields.Item("U_Type").Value)
    '            ODBDSDetail2.SetValue("U_Unit", ODBDSDetail2.Size - 1, rs.Fields.Item("U_Unit").Value)
    '            ODBDSDetail2.SetValue("U_Lowlimit", ODBDSDetail2.Size - 1, rs.Fields.Item("U_Lowlimit").Value)
    '            ODBDSDetail2.SetValue("U_Uplimit", ODBDSDetail2.Size - 1, rs.Fields.Item("U_Uplimit").Value)
    '            ODBDSDetail2.SetValue("U_ChkMethd", ODBDSDetail2.Size - 1, rs.Fields.Item("U_ChkMethd").Value)
    '            rs.MoveNext()

    '        Next j

    '        oMatrix2.LoadFromDataSourceEx()

    '    Catch ex As Exception
    '        oApplication.StatusBar.SetText("LoadParameterDetails Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
    '    End Try

    'End Sub

    Sub LoadFolderDetails()
        Try
            Dim ItemCode = oDBDSHeader.GetValue("U_ICode", 0).ToString.Trim
            Dim Enforce = oGFun.getSingleValue("select top 1 isnull(OQCCP.U_Enforce,'N') from [@MIPL_OQCCP] OQCCP where " & _
                                     "OQCCP.U_OprCode = '" & ItemCode & "'")

            Enforce = IIf(Enforce.ToString.Trim = "", "N", Enforce)

            frmInspection.Items.Item("98").Click()
            'frmInspection.PaneLevel = "2"

            If Enforce = "Y" And ItemCode <> "" And oDBDSHeader.GetValue("U_SamRef", 0).ToString.Trim <> "" Then

                'Visible for Bought Material
                frmInspection.Items.Item("19").Visible = False
                frmInspection.Items.Item("20").Visible = True
                'Sample Ref
                frmInspection.Items.Item("88").Visible = True
                frmInspection.Items.Item("89").Visible = True

            Else
                'Visible for Raw Material
                frmInspection.Items.Item("20").Visible = False
                frmInspection.Items.Item("19").Visible = True
                'Sample Ref
                frmInspection.Items.Item("88").Visible = False
                frmInspection.Items.Item("89").Visible = False


            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText("SetVisibleFolder Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub

    Sub GetUom(ByVal itemcode As String)
        Try
            '25/04/2013
            Dim Uom = oGFun.getSingleValue("select isnull(BuyUnitMsr,'') from OITM where ItemCode = '" & itemcode & "'  ")

            frmInspection.Items.Item("t_UOM").Specific.Value = Uom
            frmInspection.Items.Item("t_SamUom").Specific.Value = Uom
            frmInspection.Items.Item("t_InsUom").Specific.Value = Uom
            frmInspection.Items.Item("t_AccUom").Specific.Value = Uom
            frmInspection.Items.Item("t_PAccUom").Specific.Value = Uom
            frmInspection.Items.Item("t_RejUom").Specific.Value = Uom
            frmInspection.Items.Item("t_RewUom").Specific.Value = Uom

        Catch ex As Exception
            oApplication.StatusBar.SetText("GetUom Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub

    Sub GetFields(ByVal itemcode As String)
        Try
            ''28/05/2013

            'Dim rs As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            'Dim StrSql = "select isnull(U_SaltRept,'N') [SaltRept],isnull(U_PlatRept,'N') [PlatRept],isnull(U_MSIMDS,'N') [MSIMDS],isnull(U_IMDS,'N') [IMDS],isnull(U_InsRept,'N') [InsRept], " & _
            '                               " isnull(U_RmtcRec,'N') [Rmtc],isnull(U_TstCtRec,'N') [TstCtRec] from [@MIPL_OQCCP] " & _
            '                                " where U_OprCode ='" & itemcode & "'"
            'rs.DoQuery(StrSql)
            ''Visible Combobox 
            ''changed on 28/05/2013
            'oMatrix4.Clear()
            'oDBDSDetail4.Clear()

            ''oMatrix6.Columns.Item("c_Value").Editable = True
            'If rs.RecordCount > 0 Then

            '    If rs.Fields.Item("SaltRept").Value = "Y" Then
            '        oMatrix4.AddRow(1)
            '        oMatrix4.GetCellSpecific("St_txt", oMatrix4.VisualRowCount).Value = "Salt Spray Report"
            '    End If

            '    If rs.Fields.Item("PlatRept").Value = "Y" Then
            '        oMatrix4.AddRow(1)
            '        oMatrix4.GetCellSpecific("St_txt", oMatrix4.VisualRowCount).Value = "Plating Report"
            '    End If

            '    If rs.Fields.Item("MSIMDS").Value = "Y" Then
            '        oMatrix4.AddRow(1)
            '        oMatrix4.GetCellSpecific("St_txt", oMatrix4.VisualRowCount).Value = "MSDS"
            '    End If

            '    If rs.Fields.Item("IMDS").Value = "Y" Then
            '        oMatrix4.AddRow(1)
            '        oMatrix4.GetCellSpecific("St_txt", oMatrix4.VisualRowCount).Value = "IMDS"
            '    End If

            '    If rs.Fields.Item("InsRept").Value = "Y" Then
            '        oMatrix4.AddRow(1)
            '        oMatrix4.GetCellSpecific("St_txt", oMatrix4.VisualRowCount).Value = "Inspection Report"
            '    End If

            '    If rs.Fields.Item("TstCtRec").Value = "Y" Then
            '        oMatrix4.AddRow(1)
            '        oMatrix4.GetCellSpecific("St_txt", oMatrix4.VisualRowCount).Value = "Test Certificate Received"
            '    End If

            '    If rs.Fields.Item("Rmtc").Value = "Y" Then
            '        oMatrix4.AddRow(1)
            '        oMatrix4.GetCellSpecific("St_txt", oMatrix4.VisualRowCount).Value = "RMTC Received"
            '    End If
            'End If
        Catch ex As Exception
            oApplication.StatusBar.SetText("GetFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub

    Sub SelectSeries()
        Try

            ''StrSeries = "select max(Series) from NNM1 where ObjectCode ='INS' and seriesName = case when isnull((Select OITB.ItmsGrpNam from OITM,OITB Where OITM.ItmsGrpCod = OITB.ItmsGrpCod And ItemCode ='" & oDBDSHeader.GetValue("U_ItemCode", 0) & "' and OITB.ItmsGrpNam = 'RAW MATERIAL' ),'') = '' then 'BO' Else 'RM' end "

            'StrSeries = " select case when isnull((Select OITB.ItmsGrpNam from OITM,OITB where OITM.ItmsGrpCod = OITB.ItmsGrpCod " & _
            '           " And ItemCode = '" & oDBDSHeader.GetValue("U_ItemCode", 0) & "' and OITB.ItmsGrpNam = 'RAW MATERIAL'),'') ='' then 'BO' else 'RM' end"

            'Dim Series = oGFun.getSingleValue(StrSeries)

            'Dim c_series As SAPbouiCOM.ComboBox = frmInspection.Items.Item("c_series").Specific
            'c_series.Select(Series, SAPbouiCOM.BoSearchKey.psk_ByDescription)

        Catch ex As Exception
            'oApplication.StatusBar.SetText("Series Method Failed" & ex.Message)
        End Try

    End Sub

#Region "Inspection EOU Report"

    Sub showprint_EOUreport()
        Try

            Dim oPrintPreviewEOU As New PrintPreviewEOU
            oPrintPreviewEOU.oReport.Load(Application.StartupPath & "\NCMR_REPORT_EOU.rpt")
            Dim CryViewer As New CrystalDecisions.Windows.Forms.CrystalReportViewer
            Dim oLI As New CrystalDecisions.Shared.TableLogOnInfo
            oLI.ConnectionInfo.ServerName = oCompany.Server
            oLI.ConnectionInfo.UserID = oCompany.DbUserName
            oLI.ConnectionInfo.Password = "Admin@123"
            oLI.ConnectionInfo.DatabaseName = oCompany.CompanyDB
            oPrintPreviewEOU.oReport.Database.Tables.Item(0).ApplyLogOnInfo(oLI)
            oPrintPreviewEOU.oReport.SetParameterValue(0, oDBDSHeader.GetValue("DocEntry", 0))
            CryViewer.ReportSource = oPrintPreviewEOU.oReport
            CryViewer.Dock = System.Windows.Forms.DockStyle.Fill
            oPrintPreviewEOU.CrystalReportViewer1.ReportSource = oPrintPreviewEOU.oReport

            System.Windows.Forms.Application.Run(oPrintPreviewEOU)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Show Print File Fun Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

#End Region

#Region "Inspection DTA Report"

    Sub showprint_DTAreport()
        Try

            Dim oPrintPreviewDTA As New PrintPreviewDTA
            oPrintPreviewDTA.oReport.Load(Application.StartupPath & "\NCMR_REPORT_DTA.rpt")
            Dim CryViewer As New CrystalDecisions.Windows.Forms.CrystalReportViewer
            Dim oLI As New CrystalDecisions.Shared.TableLogOnInfo
            oLI.ConnectionInfo.ServerName = oCompany.Server
            oLI.ConnectionInfo.UserID = oCompany.DbUserName
            oLI.ConnectionInfo.Password = "Admin@123"
            oLI.ConnectionInfo.DatabaseName = oCompany.CompanyDB
            oPrintPreviewDTA.oReport.Database.Tables.Item(0).ApplyLogOnInfo(oLI)
            oPrintPreviewDTA.oReport.SetParameterValue(0, oDBDSHeader.GetValue("DocEntry", 0))
            CryViewer.ReportSource = oPrintPreviewDTA.oReport
            CryViewer.Dock = System.Windows.Forms.DockStyle.Fill
            oPrintPreviewDTA.CrystalReportViewer1.ReportSource = oPrintPreviewDTA.oReport

            System.Windows.Forms.Application.Run(oPrintPreviewDTA)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Show Print File Fun Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
#End Region

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try

            Select Case pVal.MenuUID
                Case "1282"
                    If pVal.BeforeAction = False Then Me.InitForm()
                Case "1293"
                    If pVal.BeforeAction = False Then
                        oGFun.DeleteRow(oMatrix, oDBDSDetail1)
                        oGFun.DeleteRow(oMatrix2, ODBDSDetail2)
                    End If

                    'Load Dynamic Columns in Navigation

                    'For First record

                    'Case "1290"
                    '    If pVal.BeforeAction Then

                    '        DocEntry = oGFun.getSingleValue("Select ISNULL(Min(ISNULL(DocEntry,0)),0) From [@MIPL_OINS]")
                    '        Dim itemCode = oGFun.getSingleValue("Select ISNULL(U_ItemCode,'') From [@MIPL_OINS] where DocEntry = '" & DocEntry & "'")
                    '        Dim InsQty = oGFun.getSingleValue("Select ISNULL(U_InsQty,0) From [@MIPL_OINS] where DocEntry = '" & DocEntry & "'")
                    '        IIf(InsQty.ToString.Trim = "", 0, InsQty)
                    '        GenerateSampleSizeColumns(itemCode, InsQty)
                    '    End If

                    '    'For last record
                    'Case "1291"
                    '    If pVal.BeforeAction Then

                    '        DocEntry = oGFun.getSingleValue("Select ISNULL(Max(ISNULL(DocEntry,0)),0) From [@MIPL_OINS]")
                    '        Dim itemCode = oGFun.getSingleValue("Select ISNULL(U_ItemCode,'') From [@MIPL_OINS] where DocEntry = '" & DocEntry & "'")
                    '        Dim InsQty = oGFun.getSingleValue("Select ISNULL(U_InsQty,0) From [@MIPL_OINS] where DocEntry = '" & DocEntry & "'")
                    '        IIf(InsQty.ToString.Trim = "", 0, InsQty)
                    '        GenerateSampleSizeColumns(itemCode, InsQty)
                    '    End If

                    '    'For Next record
                    'Case "1288"
                    '    If pVal.BeforeAction Then

                    '        DocEntry = oDBDSHeader.GetValue("DocEntry", 0).ToString.Trim
                    '        If DocEntry = "" Then DocEntry = oGFun.getSingleValue("Select ISNULL(Max(ISNULL(DocEntry,0)),0) From [@MIPL_OINS]")
                    '        DocEntry = oGFun.getSingleValue("Select top 1 DocEntry From [@MIPL_OINS] Where DocEntry > " & DocEntry & " Order By DocEntry asc")
                    '        If DocEntry = "" Then DocEntry = oGFun.getSingleValue("Select ISNULL(Min(ISNULL(DocEntry,0)),0) From [@MIPL_OINS]")
                    '        Dim itemCode = oGFun.getSingleValue("Select ISNULL(U_ItemCode,'') From [@MIPL_OINS] where DocEntry = '" & DocEntry & "'")
                    '        Dim InsQty = oGFun.getSingleValue("Select ISNULL(U_InsQty,0) From [@MIPL_OINS] where DocEntry = '" & DocEntry & "'")
                    '        IIf(InsQty.ToString.Trim = "", 0, InsQty)
                    '        GenerateSampleSizeColumns(itemCode, InsQty)
                    '    End If

                    '    'For Previous record
                    'Case "1289"
                    '    If pVal.BeforeAction Then
                    '        DocEntry = oDBDSHeader.GetValue("DocEntry", 0).ToString.Trim
                    '        If DocEntry = "" Then DocEntry = oGFun.getSingleValue("Select ISNULL(Max(ISNULL(DocEntry,0)),0) From [@MIPL_OINS]")
                    '        DocEntry = oGFun.getSingleValue("Select top 1 DocEntry From [@MIPL_OINS] Where DocEntry < " & DocEntry & " Order By DocEntry desc ")
                    '        If DocEntry = "" Then DocEntry = oGFun.getSingleValue("Select ISNULL(Max(ISNULL(DocEntry,0)),0) Code From [@MIPL_OINS]")
                    '        Dim itemCode = oGFun.getSingleValue("Select ISNULL(U_ItemCode,'') From [@MIPL_OINS] where DocEntry = '" & DocEntry & "'")
                    '        Dim InsQty = oGFun.getSingleValue("Select ISNULL(U_InsQty,0) From [@MIPL_OINS] where DocEntry = '" & DocEntry & "'")
                    '        IIf(InsQty.ToString.Trim = "", 0, InsQty)
                    '        GenerateSampleSizeColumns(itemCode, InsQty)
                    '    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Try
                        If BusinessObjectInfo.BeforeAction Then
                            If Me.ValidateAll() = False And frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                System.Media.SystemSounds.Asterisk.Play()
                                BubbleEvent = False
                                Exit Sub
                            Else
                                'oGFun.DeleteEmptyRowInFormDataEvent(oMatrix, "2", oDBDSDetail1)
                                oGFun.DeleteEmptyRowInFormDataEvent(oMatrix2, "2", ODBDSDetail2)
                                oGFun.DeleteEmptyRowInFormDataEvent(oMatrix3, "LinkPath", oDBDSDetail3)


                                If frmInspection.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    Try
                                        If oCompany.InTransaction = False Then oCompany.StartTransaction()
                                        Dim BoolPosting As Boolean = False
                                        BoolPosting = Me.StockPosting_LathingProcess()
                                        If BoolPosting = False Then
                                            If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                            BubbleEvent = False
                                        Else
                                            If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit)
                                        End If
                                    Catch ex As Exception
                                        BubbleEvent = False
                                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                        oApplication.StatusBar.SetText("Transaction is Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                        Return
                                    End Try
                                End If
                            End If
                            
                        End If

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("FormDataEvent Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If BusinessObjectInfo.ActionSuccess = True Then
                        TypeWiseControlAlloc()
                        LoadFolderDetails()

                        frmInspection.Items.Item("55").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                        frmInspection.Items.Item("FG").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Function StockPosting_LathingProcess() As Boolean

        Try

            Dim oStockTransfer As SAPbobsCOM.StockTransfer = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oStockTransfer)
            Dim Flag As Boolean = False

            Dim DocNum = oDBDSHeader.GetValue("DocNum", 0).ToString.Trim
            Dim DocEntry = oDBDSHeader.GetValue("DocEntry", 0).ToString.Trim
            Dim RecEntry = oDBDSHeader.GetValue("U_RecEntry", 0).ToString.Trim
            Dim Location = oDBDSHeader.GetValue("U_LocCode", 0).ToString.Trim
            Dim WorkOrderBaseNum = oDBDSHeader.GetValue("U_WBaseNum", 0).ToString.Trim

            Dim ItemCode As String = frmInspection.Items.Item("118").Specific.value
            Dim U_Type As String = oDBDSHeader.GetValue("U_Type", 0).ToString.Trim
            Dim U_WOType As String = oDBDSHeader.GetValue("U_WOType", 0).ToString.Trim
            Dim U_GRNEntry As String = oDBDSHeader.GetValue("U_GRNEntry", 0).ToString.Trim


            For iPostingCount As Integer = 1 To oMatrix5.VisualRowCount
                Dim FromWarehouse As String = String.Empty
                Dim ItemGroup As String = String.Empty

                Dim AccQty As Double = CDbl(oMatrix5.Columns.Item("AccQty").Cells.Item(iPostingCount).Specific.value.ToString.Trim)
                Dim RejQty As Double = CDbl(oMatrix5.Columns.Item("RejQty").Cells.Item(iPostingCount).Specific.value.ToString.Trim)
                Dim RewQty As Double = CDbl(oMatrix5.Columns.Item("RewQty").Cells.Item(iPostingCount).Specific.value.ToString.Trim)
                Dim BatchNo As String = oMatrix5.Columns.Item("uom").Cells.Item(iPostingCount).Specific.value.ToString.Trim
                Dim ManBtchNumYes = oGFun.getSingleValue("Select ManBtchNum  from OITM Where ItemCode='" & ItemCode & "'")
                Dim SerialNoYes = oGFun.getSingleValue("Select ManSerNum  from OITM Where ItemCode='" & ItemCode & "'")
                ItemGroup = oGFun.getSingleValue(" Select Top 1 ItmsGrpCod   FRom oitm Where ItemCode in ('" & ItemCode & "')")
                Dim strQuer As String = String.Empty
                If U_Type = "IN" Then
                    strQuer = " select Top 1   WhsCode  from OIGN m left outer join IGN1 d on m.DocEntry =d.DocEntry  where  m.U_WBaseNum ='" & WorkOrderBaseNum & "' AND m.U_BaseNum ='" & RecEntry & "' Order by m.DocEntry Desc"
                Else
                    strQuer = " select distinct   WhsCode  from OPDN m left outer join PDN1 d on m.DocEntry =d.DocEntry  where m.DocEntry ='" & U_GRNEntry & "' AND d.ItemCode='" + ItemCode + "'"
                End If

                FromWarehouse = oGFun.getSingleValue(strQuer)
                If FromWarehouse = String.Empty Then
                    oGFun.Msg(" From Warehouse Is empty: " & strQuer)
                End If
                Dim AcceptedWarehouse As String = oGFun.getSingleValue(" SELECT top 1 case when (select Excisable from OITM where Itemcode='" & ItemCode & "' )='N' then U_AccWhCode else U_AccWhCode  end   FROM [dbo].[@MIPL_QCGT1]  where U_LocCode ='" & Location & "' And isnull(U_Active,'N') = 'Y' and U_Type='" + U_Type + "' and Isnull(U_OPType,'')='" + U_WOType + "'")
                Dim RejectedWareHouse As String = oGFun.getSingleValue(" SELECT top 1 case when (select Excisable from OITM where Itemcode='" & ItemCode & "' )='N' then U_RejWhCode else U_RejWhCode  end  FROM [dbo].[@MIPL_QCGT1]  where U_LocCode ='" & Location & "' And isnull(U_Active,'N') = 'Y'  and U_Type='" + U_Type + "' and Isnull(U_OPType,'')='" + U_WOType + "'")
                Dim ReworkWareHouse As String = oGFun.getSingleValue(" SELECT top 1  case when (select Excisable from OITM where Itemcode='" & ItemCode & "' )='N' then U_RewWhCode else U_RewWhCode end   FROM [dbo].[@MIPL_QCGT1]  where U_LocCode ='" & Location & "' And isnull(U_Active,'N') = 'Y' and U_Type='" + U_Type + "' and Isnull(U_OPType,'')='" + U_WOType + "'")


                oGFun.Msg("FromWarehouse" + FromWarehouse.ToString())
                oGFun.Msg("Rejected" + RejectedWareHouse.ToString())
                oGFun.Msg("Rework" + ReworkWareHouse.ToString())


                If AcceptedWarehouse = String.Empty Then
                    oGFun.Msg(" Accepted Warehouse Is empty")
                End If
                If RejectedWareHouse = String.Empty Then
                    oGFun.Msg(" Rejected Warehouse Is empty")
                End If
                If ReworkWareHouse = String.Empty Then
                    oGFun.Msg(" Rework Warehouse Is empty")
                End If

                oStockTransfer.Comments = "Inspection - Stock Posted From QC DocNum -> " & DocNum
                oStockTransfer.Reference2 = DocNum
                oStockTransfer.DocDate = Date.Now
                oStockTransfer.FromWarehouse = FromWarehouse
                oStockTransfer.ToWarehouse = AcceptedWarehouse
                oStockTransfer.UserFields.Fields.Item("U_BaseNum").Value = oDBDSHeader.GetValue("DocNum", 0)
                oStockTransfer.UserFields.Fields.Item("U_BaseObject").Value = "INS"
                oStockTransfer.UserFields.Fields.Item("U_Series").Value = oDBDSHeader.GetValue("Series", 0)

                If (AccQty > 0) Then
                    Flag = True
                    oStockTransfer.Lines.Quantity = AccQty
                    oStockTransfer.Lines.ItemCode = ItemCode
                    oStockTransfer.Lines.FromWarehouseCode = FromWarehouse
                    oStockTransfer.Lines.WarehouseCode = AcceptedWarehouse
                    If ManBtchNumYes = "Y" Then
                        oStockTransfer.Lines.BatchNumbers.BatchNumber = BatchNo
                        oStockTransfer.Lines.BatchNumbers.Quantity = AccQty
                        oStockTransfer.Lines.BatchNumbers.Add()
                    End If

                    oStockTransfer.Lines.Add()
                End If




                If (RejQty > 0) Then
                    Flag = True
                    oStockTransfer.Lines.Quantity = RejQty
                    oStockTransfer.Lines.ItemCode = ItemCode
                    oStockTransfer.Lines.FromWarehouseCode = FromWarehouse
                    oStockTransfer.Lines.WarehouseCode = RejectedWareHouse
                    If ManBtchNumYes = "Y" Then
                        oStockTransfer.Lines.BatchNumbers.BatchNumber = BatchNo
                        oStockTransfer.Lines.BatchNumbers.Quantity = RejQty
                        oStockTransfer.Lines.BatchNumbers.Add()
                    End If
                    'If SerialNoYes = "Y" Then
                    '    oStockTransfer.Lines.SerialNumbers.InternalSerialNumber = SerialNo
                    '    oStockTransfer.Lines.SerialNumbers.Quantity = 1
                    '    oStockTransfer.Lines.SerialNumbers.Add()
                    'End If
                    oStockTransfer.Lines.Add()
                End If
                If (RewQty > 0) Then
                    Flag = True
                    oStockTransfer.Lines.Quantity = RewQty
                    oStockTransfer.Lines.ItemCode = ItemCode
                    oStockTransfer.Lines.FromWarehouseCode = FromWarehouse
                    oStockTransfer.Lines.WarehouseCode = ReworkWareHouse
                    If ManBtchNumYes = "Y" Then
                        oStockTransfer.Lines.BatchNumbers.BatchNumber = BatchNo
                        oStockTransfer.Lines.BatchNumbers.Quantity = RewQty
                        oStockTransfer.Lines.BatchNumbers.Add()
                    End If
                    ''If SerialNoYes = "Y" Then
                    ''    oStockTransfer.Lines.SerialNumbers.InternalSerialNumber = SerialNo
                    ''    oStockTransfer.Lines.SerialNumbers.Quantity = 1
                    ''    oStockTransfer.Lines.SerialNumbers.Add()
                    ''End If
                    oStockTransfer.Lines.Add()
                End If
            Next
            If (Flag = True) Then
                Dim ErrCode = oStockTransfer.Add()
                If ErrCode <> 0 And Flag Then
                    oGFun.Msg(" StockPosting_LathingProcess  Posting Error : " & oCompany.GetLastErrorDescription)
                    Return False
                Else
                    Return True
                End If
            Else
                Return True
            End If
        Catch ex As Exception
            oGFun.Msg(" Inhouse Stock Posting Method Faild " & ex.Message)
            Return False
        End Try

    End Function

End Class
