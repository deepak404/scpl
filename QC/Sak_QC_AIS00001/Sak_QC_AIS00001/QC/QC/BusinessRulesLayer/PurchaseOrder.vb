﻿Public Class PurchaseOrder

    Dim frmPurchaseOrder As SAPbouiCOM.Form
    Dim oDBDSHeader As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim FrmID As String = ""

    Sub LoadForm()
        Try
            frmPurchaseOrder = oApplication.Forms.GetFormByTypeAndCount("142", 1)
            FrmId = frmPurchaseOrder.UniqueID

            oDBDSHeader = frmPurchaseOrder.DataSources.DBDataSources.Item(0)
            oDBDSDetail = frmPurchaseOrder.DataSources.DBDataSources.Item(1)
            oMatrix = frmPurchaseOrder.Items.Item("38").Specific
            Me.InitForm()
            Me.DefineModesForFields()
        Catch ex As Exception
            oGFun.Msg("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmPurchaseOrder.Freeze(True)
            oMatrix.Columns.Item("24").Width = 0
            Try
                oGFun.setEditTextColumnCFL(frmPurchaseOrder, oMatrix, "U_WhsCode", "cfl_Whs", "64", "WhsCode")
                oGFun.setEditTextColumnCFL(frmPurchaseOrder, oMatrix, "U_ItemCode", "cfl_4A", "4", "ItemCode")
            Catch ex As Exception
            End Try

        Catch ex As Exception
            oGFun.Msg("InitForm Method Failed:" & ex.Message)
        Finally
            frmPurchaseOrder.Freeze(False)
        End Try
    End Sub

    Sub DefineModesForFields()
        Try
        Catch ex As Exception
            oApplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function ValidateAll() As Boolean
        Try
            If frmPurchaseOrder.Items.Item("3").Specific.Selected.Value.ToString.Trim = "S" Then
                Return True
            End If

            For i As Integer = 1 To oMatrix.VisualRowCount - 1
                If oMatrix.GetCellSpecific("U_WhsCode", i).Value.ToString.Trim = "" Then
                    oGFun.Msg("Line No : " & i.ToString & " Warehouse Code Should be Enter..")
                    Return False
                End If
            Next
            ValidateAll = True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
                    Try
                        If pVal.BeforeAction = False Then
                            Me.LoadForm()
                        End If
                    Catch ex As Exception
                        oGFun.Msg("Form Load Event Failed:" & ex.Message)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        'Assign Selected Rows
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        '        'Filter before open the CFL
                        If pVal.BeforeAction Then
                            Select Case oCFLE.ChooseFromListUID
                                'Case "cfl_Whs"
                                'Dim ItemCode As String = oMatrix.GetCellSpecific("1", pVal.Row).Value
                                'Dim Excisable As String = oGFun.getSingleValue("select Excisable from OITM where ItemCode = '" & ItemCode & "' ")
                                'oGFun.ChooseFromListFilteration(frmPurchaseOrder, "cfl_Whs", "Excisable", "Select '" & Excisable & "' ")

                            End Select
                        End If
                        If pVal.BeforeAction = False And Not oDataTable Is Nothing Then
                            '            'Assign Value to selected control
                            Select Case oCFLE.ChooseFromListUID
                                Case "cfl_4A"
                                    Dim oEditText As SAPbouiCOM.EditText
                                    oEditText = oMatrix.GetCellSpecific("1", pVal.Row)
                                    ''oMatrix.Columns.Item("1").Cells.Item(pVal.Row).Click()
                                    ''Try
                                    ''    oEditText.Value = LV_SubCantractItem
                                    ''    'oApplication.SendKeys(LV_SubCantractItem)
                                    ''Catch ex As Exception
                                    ''End Try
                                    ''oMatrix.Columns.Item("U_ItemCode").Cells.Item(pVal.Row).Click()
                                    '    oEditText = oMatrix.GetCellSpecific("U_ItemCode", pVal.Row)
                                    Try
                                        oEditText.Value = oDataTable.GetValue("ItemCode", 0)
                                    Catch ex As Exception
                                    End Try

                                Case "cfl_Whs"
                                    Dim oEditText As SAPbouiCOM.EditText

                                    Dim WhsCode As String = oDataTable.GetValue("WhsCode", 0)
                                    Dim ItemCode As String = oMatrix.GetCellSpecific("1", pVal.Row).Value
                                    Dim InsReq As String = oGFun.getSingleValue("select isnull(U_InsReq,'N') from OITM where ItemCode = '" & ItemCode & "' ")
                                    ' Dim Excisable As String = oGFun.getSingleValue("select Excisable from OITM where ItemCode = '" & ItemCode & "' ")
                                    Dim Excisable As String = oGFun.getSingleValue("select Excisable from OWHS where WhsCode = '" & WhsCode & "' ")
                                    Dim Location As String = oGFun.getSingleValue("select Location from OWHS where WhsCode = '" & WhsCode & "' ")
                                    Dim QcWhCod As String = oGFun.getSingleValue("select " & IIf(Excisable.Trim = "Y", " U_QcEWhCod ", " U_QcWhCod ") & " from [@MIPL_QCGT1] where U_LocCode = '" & Location & "' ")

                                    'If QcWhCod.Trim = "" Then
                                    '    oGFun.Msg("QC. Setting Not Define for this Location..")
                                    '    Return
                                    'End If

                                    oEditText = oMatrix.GetCellSpecific("24", pVal.Row)

                                    Try
                                        If InsReq = "N" Then
                                            oEditText.Value = oDataTable.GetValue("WhsCode", 0)
                                        Else
                                            oEditText.Value = QcWhCod
                                        End If
                                    Catch ex As Exception
                                    End Try

                                    Try
                                        oEditText = oMatrix.GetCellSpecific("U_WhsCode", pVal.Row)
                                        oEditText.Value = oDataTable.GetValue("WhsCode", 0)
                                    Catch ex As Exception
                                    End Try

                            End Select
                        End If
                    Catch ex As Exception
                        oGFun.Msg("Choose from List Event Failed:" & ex.Message)
                    End Try

                    'Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    '    Try

                    '        If pVal.ColUID = "U_ItemCode" And pVal.BeforeAction = False Then
                    '            Dim U_Itemcode = oMatrix.GetCellSpecific("U_ItemCode", pVal.Row).Value
                    '            Dim oEditText As SAPbouiCOM.EditText
                    '            oEditText = oMatrix.GetCellSpecific("1", pVal.Row)
                    '            oMatrix.Columns.Item("1").Cells.Item(pVal.Row).Click()
                    '            Try
                    '                oEditText.Value = LV_SubCantractItem
                    '                'oApplication.SendKeys(LV_SubCantractItem)
                    '            Catch ex As Exception
                    '            End Try

                    '            oEditText = oMatrix.GetCellSpecific("U_ItemCode", pVal.Row)
                    '            Try
                    '                oEditText.Value = U_Itemcode
                    '            Catch ex As Exception
                    '            End Try

                    '        End If
                    '    Catch ex As Exception
                    '        oGFun.Msg("Lost Focus Event Failed:" & ex.Message)
                    '    End Try

                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    Try
                        Select Case pVal.ItemUID
                            Case "3"
                            Case "10000329"
                                If pVal.BeforeAction = False Then
                                    Dim ocmb As SAPbouiCOM.ComboBox = frmPurchaseOrder.Items.Item("10000329").Specific
                                    If ocmb.Selected.Description = "20" Then _
                                    oGoodsReceiptPO.LoadForm()
                                End If
                        End Select
                    Catch ex As Exception
                        oGFun.Msg("Click Event Failed:" & ex.Message)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                'Add,Update Event
                                If pVal.BeforeAction = True And _
                                    (frmPurchaseOrder.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or _
                                     frmPurchaseOrder.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then

                                    If Me.ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Return
                                    End If

                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
                    Try
                        If BusinessObjectInfo.BeforeAction Then
                            If Me.ValidateAll() = False Then
                                System.Media.SystemSounds.Asterisk.Play()
                                BubbleEvent = False
                                Exit Sub
                            Else
                            End If
                        End If
                        If BusinessObjectInfo.BeforeAction = False And BusinessObjectInfo.ActionSuccess Then
                        End If
                    Catch ex As Exception
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD

                    If BusinessObjectInfo.ActionSuccess Then
                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

End Class
