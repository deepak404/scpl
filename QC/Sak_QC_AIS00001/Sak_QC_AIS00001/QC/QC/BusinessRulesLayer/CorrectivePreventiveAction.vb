﻿Public Class CorrectivePreventiveAction
    Public frmCorrectivePreventiveAction As SAPbouiCOM.Form
    Dim ocombobox As SAPbouiCOM.ComboBox
    Dim oDBDSHeader As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim UDOID As String = "QCPA"

    Sub LoadForm()
        Try
            oGFun.LoadXML(frmCorrectivePreventiveAction, CorrectivePreventiveActionFormID, CorrectivePreventiveActionXML)
            frmCorrectivePreventiveAction = oApplication.Forms.Item(CorrectivePreventiveActionFormID)
            'Assign Data Source
            oDBDSHeader = frmCorrectivePreventiveAction.DataSources.DBDataSources.Item(0)

            'Assign Matrix

            Me.DefineModesForFields()
            Me.InitForm()
            frmCorrectivePreventiveAction.Items.Item("67").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
        Catch ex As Exception
            oGFun.Msg("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmCorrectivePreventiveAction.Freeze(True)

            oGFun.LoadComboBoxSeries(frmCorrectivePreventiveAction.Items.Item("c_Series").Specific, UDOID)
            ' oGFun.setDocNum(frmCorrectivePreventiveAction.Items.Item("t_DocNum").Specific)
            oGFun.LoadDocumentDate(frmCorrectivePreventiveAction.Items.Item("t_DueDate").Specific)

            oGFun.setComboBoxValue(frmCorrectivePreventiveAction.Items.Item("4").Specific, "Select Code, Name from OUDP")

            oGFun.setComboBoxValue(frmCorrectivePreventiveAction.Items.Item("40").Specific, "Select Code, Name from OUDP")

            ' oDBDSDetail.Clear()


        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmCorrectivePreventiveAction.Freeze(False)

        End Try
    End Sub

    Sub DefineModesForFields()
        Try
        Catch ex As Exception
            oApplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function ValidateAll() As Boolean
        Try

            If frmCorrectivePreventiveAction.Items.Item("4").Specific.value.ToString.Trim.Equals("") = True Then
                oGFun.Msg("Department Should Not Be Left Empty")
                'frmCorrectivePreventiveAction.ActiveItem = "4"
                Return False
            End If

            If frmCorrectivePreventiveAction.Items.Item("6").Specific.value.ToString.Trim.Equals("") = True Then
                oGFun.Msg("Inspection Should Not Be Left Empty")
                'frmCorrectivePreventiveAction.ActiveItem = "6"
                Return False
            End If

            If frmCorrectivePreventiveAction.Items.Item("17").Specific.value.ToString.Trim.Equals("") = True Then
                oGFun.Msg("Originator Code Should Not Be Left Empty")
                'frmCorrectivePreventiveAction.ActiveItem = "17"
                Return False
            End If

            If frmCorrectivePreventiveAction.Items.Item("25").Specific.value.ToString.Trim.Equals("") = True Then
                oGFun.Msg("Prepared by Code Should Not Be Left Empty")
                'frmCorrectivePreventiveAction.ActiveItem = "25"
                Return False
            End If

            If frmCorrectivePreventiveAction.Items.Item("60").Specific.value.ToString.Trim.Equals("") = True Then
                oGFun.Msg("Responsible Manager Code Should Not Be Left Empty")
                'frmCorrectivePreventiveAction.ActiveItem = "60"
                Return False
            End If

            ValidateAll = True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        'Assign Selected Rows
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        '        'Filter before open the CFL
                        'If pVal.BeforeAction Then
                        '    Select Case oCFLE.ChooseFromListUID
                        '        Case "cfl_itmcod"
                        '            oGFun.ChooseFromListFilteration(frmCorrectivePreventiveAction, "cfl_pcod", "U_Code", "Select U_Code from [@MIPL_OQCPM]")
                        '    End Select
                        'End If

                        If pVal.BeforeAction = False And Not oDataTable Is Nothing Then
                            '            'Assign Value to selected control
                            Select Case oCFLE.ChooseFromListUID
                                '                'Assign Custemer
                                Case "cfl_ins"

                                    oDBDSHeader.SetValue("U_InspNo", pVal.Row, oDataTable.GetValue("DocNum", 0))
                                    oDBDSHeader.SetValue("U_ItemCode", pVal.Row, oDataTable.GetValue("U_ItemCode", 0))

                                    '  If oCFLE.ChooseFromListUID = "cfl_pcod" Then oGFun.SetNewLine(oMatrix1, ODBDSDetail1, pVal.Row, "2")

                                Case "cfl_org"

                                    oDBDSHeader.SetValue("U_OrgnCode", pVal.Row, oDataTable.GetValue("empID", 0))
                                    oDBDSHeader.SetValue("U_OrgnName", pVal.Row, oDataTable.GetValue("lastName", 0) + ", " + oDataTable.GetValue("firstName", 0))


                                Case "cfl_pre"
                                    oDBDSHeader.SetValue("U_PreCode", pVal.Row, oDataTable.GetValue("empID", 0))
                                    oDBDSHeader.SetValue("U_PreName", pVal.Row, oDataTable.GetValue("lastName", 0) + ", " + oDataTable.GetValue("firstName", 0))


                                Case "cfl_rspm"
                                    oDBDSHeader.SetValue("U_RsMgCode", pVal.Row, oDataTable.GetValue("empID", 0))
                                    oDBDSHeader.SetValue("U_RsMgName", pVal.Row, oDataTable.GetValue("lastName", 0) + ", " + oDataTable.GetValue("firstName", 0))

                            End Select
                        End If

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Select Case pVal.ItemUID
                        Case "63"
                            'Changed by Sankaralakshmi
                            Try
                                If pVal.BeforeAction = True Then
                                    oApplication.Menus.Item("QCIS").Activate()
                                Else
                                    'Dim InspectionNo As String = frmCorrectivePreventiveAction.Items.Item("6").Specific.Value
                                    Dim frm As SAPbouiCOM.Form
                                    frm = oApplication.Forms.ActiveForm()
                                    frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                    frm.Items.Item("t_DocNum").Specific.Value = oDBDSHeader.GetValue("DocNum", 0)
                                    frm.Items.Item("1").Click()
                                End If
                            Catch ex As Exception
                                oApplication.StatusBar.SetText(ex.Message)
                            End Try

                        Case "14"
                            If pVal.BeforeAction = False Then
                                frmCorrectivePreventiveAction.PaneLevel = 2
                            End If
                        Case "15"
                            If pVal.BeforeAction = False Then
                                frmCorrectivePreventiveAction.PaneLevel = 3
                            End If
                    End Select



                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    frmCorrectivePreventiveAction.Freeze(True)
                    Try
                        If pVal.ItemUID = "14" Then
                            frmCorrectivePreventiveAction.PaneLevel = 2
                        ElseIf pVal.ItemUID = "15" Then
                            frmCorrectivePreventiveAction.PaneLevel = 3
                        End If
                    Catch ex As Exception
                    Finally
                        frmCorrectivePreventiveAction.Freeze(False)
                    End Try

                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            'Get the Serial Number Based On Series...
                            Case "c_Series"
                                If frmCorrectivePreventiveAction.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    oGFun.setDocNum(frmCorrectivePreventiveAction)
                                End If
                        End Select

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                'Refresh the Form After Add Event
                                'If pVal.BeforeAction = False And frmCorrectivePreventiveAction.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                '    Me.InitForm()
                                'End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                'Add,Update Event
                                If pVal.BeforeAction = True And (frmCorrectivePreventiveAction.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmCorrectivePreventiveAction.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then

                                    If Me.ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Exit Sub
                                    End If
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        BubbleEvent = False
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()
                Case "1293"
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Dim rsetUpadteIndent As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Try
                        If BusinessObjectInfo.BeforeAction Then
                            If Me.ValidateAll() = False Then
                                System.Media.SystemSounds.Asterisk.Play()
                                BubbleEvent = False
                                Exit Sub
                            Else

                                If frmCorrectivePreventiveAction.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    oGFun.setDocNum(frmCorrectivePreventiveAction)
                                End If
                            End If
                        End If
                        If BusinessObjectInfo.ActionSuccess = True Then

                        End If
                    Catch ex As Exception
                        BubbleEvent = False
                    Finally
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If BusinessObjectInfo.ActionSuccess = True Then

                    End If
                    'We can Update the Document still Status Is OPen Other Wish View Mode.
                    If oDBDSHeader.GetValue("Status", 0).Trim = "O" Then
                        frmCorrectivePreventiveAction.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        '  oGFun.SetNewLine(oMatrix, oDBDSDetail, oMatrix.VisualRowCount)
                        '   oGFun.SetNewLine(oMatrix 1, oDBDSDetail, oMatrix.VisualRowCount)
                    Else
                        frmCorrectivePreventiveAction.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


End Class
