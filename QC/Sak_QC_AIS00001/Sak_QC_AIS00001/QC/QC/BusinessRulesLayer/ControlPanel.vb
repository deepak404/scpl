﻿Public Class ControlPanel
    Public frmControlPanel As SAPbouiCOM.Form
    Dim ocombobox As SAPbouiCOM.ComboBox
    Dim oDBDSHeader As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail, oDBDSDetail2, oDBDSDetail3 As SAPbouiCOM.DBDataSource
    Dim oMatrix1, oMatrix2, oMatrix3 As SAPbouiCOM.Matrix
    Dim UDOID As String = "QCCP"
    Dim Str As String


    Sub LoadForm()
        Try
            oGFun.LoadXML(frmControlPanel, ControlPanelFormID, ControlPanelXML)
            frmControlPanel = oApplication.Forms.Item(ControlPanelFormID)
            oDBDSHeader = frmControlPanel.DataSources.DBDataSources.Item("@MIPL_OQCCP")
            oDBDSDetail = frmControlPanel.DataSources.DBDataSources.Item("@MIPL_QCCP1")
            oDBDSDetail2 = frmControlPanel.DataSources.DBDataSources.Item("@MIPL_QCCP2")
            oDBDSDetail3 = frmControlPanel.DataSources.DBDataSources.Item("@MIPL_QCCP3")
            frmControlPanel.Items.Item("43").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            oMatrix1 = frmControlPanel.Items.Item("45").Specific
            oMatrix2 = frmControlPanel.Items.Item("Sample").Specific
            oMatrix3 = frmControlPanel.Items.Item("Attach").Specific

            DefineModesForFields()
        Catch ex As Exception
            oGFun.Msg("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmControlPanel.Freeze(True)

            If frmControlPanel.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                oDBDSHeader.SetValue("Code", 0, oGFun.GetCodeGeneration("[@MIPL_OQCCP]"))
                oGFun.LoadDocumentDate(frmControlPanel.Items.Item("28").Specific)
            End If
            oGFun.setComboBoxValue(frmControlPanel.Items.Item("c_LCode").Specific, "Select Code,Location from OLCT")
            'oGFun.setComboBoxValue(frmControlPanel.Items.Item("c_BPartner").Specific, "Select CardCode,CardName from OCRD")

            oMatrix1.Clear()
            oDBDSDetail.Clear()
            oMatrix2.Clear()
            oDBDSDetail2.Clear()
            oMatrix3.Clear()
            oDBDSDetail3.Clear()
            oGFun.SetNewLine(oMatrix1, oDBDSDetail)
            oGFun.SetNewLine(oMatrix2, oDBDSDetail2)
            oGFun.SetNewLine(oMatrix3, oDBDSDetail3)
            CheckEnforceSampling()

            ' Enable ArrowKey changed on 15/04/2013
            oMatrix1.CommonSetting.EnableArrowKey = True
            oMatrix2.CommonSetting.EnableArrowKey = True
            oMatrix3.CommonSetting.EnableArrowKey = True

            oGFun.setComboBoxValue(frmControlPanel.Items.Item("c_TstCtRec").Specific, "EXEC  [dbo].[@MIPL_QC_GetTstCtRec] ")
            Dim oCmbo As SAPbouiCOM.ComboBox = frmControlPanel.Items.Item("c_TstCtRec").Specific
            oCmbo.Select("N", SAPbouiCOM.BoSearchKey.psk_ByValue)

        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmControlPanel.Freeze(False)
        End Try



    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Sub DefineModesForFields()
        Try
            frmControlPanel.Items.Item("28").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmControlPanel.Items.Item("28").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmControlPanel.Items.Item("26").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
        Catch ex As Exception
            oApplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function ValidateAll() As Boolean
        Try



            ValidateAll = True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)

        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    
                    If pVal.ItemUID = "Attach" And pVal.ColUID = "2s" And (frmControlPanel.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or frmControlPanel.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                       

                        oGFun.SetNewLine(oMatrix3, oDBDSDetail3, oMatrix3.VisualRowCount, pVal.ColUID)


                    End If
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        'Assign Selected Rows
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        '        'Filter before open the CFL
                        If pVal.BeforeAction Then
                            Select Case oCFLE.ChooseFromListUID
                                Case "cfl_itmcod"
                                    oGFun.ChooseFromListFilteration(frmControlPanel, "cfl_itmcod", "ItemCode", "select ItemCode from OITM,OITT where OITM.ItemCode =OITT.Code and ISNULL(OITM.validFor,'N')='Y'")
                                Case "cfl_opcod"
                                    'Changed by sankaralakshmi
                                    Dim FGCode = oDBDSHeader.GetValue("U_OprCode", 0).ToString.Trim
                                    If FGCode <> "" Then
                                        Str = "select OITM.ItemCode ,OITM.ItemName from OITM,ITT1 where OITM.ItemCode=ITT1.Code and ITT1.Father ='" & FGCode & "' and ISNULL(OITM.validFor,'N')='Y' and OITM.U_InsReq = 'Y' "
                                    Else
                                        Str = "Select ItemCode from OITM where ISNULL(validFor,'N')='Y' and U_InsReq = 'Y'"
                                    End If
                                    oGFun.ChooseFromListFilteration(frmControlPanel, "cfl_opcod", "ItemCode", Str)
                            End Select
                        End If

                        If pVal.BeforeAction = False And Not oDataTable Is Nothing Then
                            '            'Assign Value to selected control
                            Select Case oCFLE.ChooseFromListUID
                                '                'Assign Customer

                                Case "cfl_Bpartner"
                                    oDBDSHeader.SetValue("U_CustCode", 0, oDataTable.GetValue("CardCode", 0))
                                    oDBDSHeader.SetValue("U_CustName", 0, oDataTable.GetValue("CardName", 0))

                                
                                Case "cfl_itmcod"
                                    oDBDSHeader.SetValue("U_ItmCode", 0, oDataTable.GetValue("ItemCode", 0))
                                    oDBDSHeader.SetValue("U_ItmName", 0, oDataTable.GetValue("FrgnName", 0))
                                    'Changed by Sankaralakshmi
                                Case "cfl_opcod"
                                    'Assign ItemMaster to Operation Code'
                                    oDBDSHeader.SetValue("U_OprCode", 0, oDataTable.GetValue("ItemCode", 0))
                                    oDBDSHeader.SetValue("U_OprName", 0, oDataTable.GetValue("FrgnName", 0))
                                Case "cfl_precod"
                                    'Assign Employee Master to Preparedbycode
                                    oDBDSDetail.SetValue("U_PreCode", pVal.Row - 1, oDataTable.GetValue("empID", 0))
                                    oDBDSDetail.SetValue("U_PreName", pVal.Row - 1, oDataTable.GetValue("lastName", 0) + ", " + oDataTable.GetValue("firstName", 0))
                                    oMatrix1.LoadFromDataSourceEx()
                                Case "cfl_appcod"
                                    'Assign Employee Master to Approvedbycode
                                    oDBDSHeader.SetValue("U_Appcod", 0, oDataTable.GetValue("empID", 0))
                                    oDBDSHeader.SetValue("U_AppName", 0, oDataTable.GetValue("lastName", 0) + ", " + oDataTable.GetValue("firstName", 0))
                                Case "cfl_pmrcod"

                                    oMatrix1.FlushToDataSource()
                                    oDBDSDetail.SetValue("U_PCode", pVal.Row - 1, Trim(oDataTable.GetValue("U_Code", 0)))
                                    oDBDSDetail.SetValue("U_PName", pVal.Row - 1, Trim(oDataTable.GetValue("U_Name", 0)))
                                    oDBDSDetail.SetValue("U_Type", pVal.Row - 1, oDataTable.GetValue("U_Type", 0))
                                    oMatrix1.LoadFromDataSource()
                                    oMatrix1.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                    oGFun.SetNewLine(oMatrix1, oDBDSDetail, oMatrix1.VisualRowCount, pVal.ColUID)





                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try



                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "Attach"
                                Select Case pVal.ColUID
                                    Case "2s"
                                        oGFun.SetNewLine(oMatrix3, oDBDSDetail3, oMatrix3.VisualRowCount, pVal.ColUID)
                                End Select

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.BeforeAction = True And frmControlPanel.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    If Me.ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Exit Sub
                                    End If
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "45"
                                'Changed by sankaralakshmi
                                Select Case pVal.ColUID

                                    Case "2"
                                        'Link button in parameter code
                                        If pVal.BeforeAction = False Then
                                            oParameterMaster.LoadForm()
                                            Dim frm As SAPbouiCOM.Form
                                            frm = oApplication.Forms.ActiveForm
                                            frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                            frm.Items.Item("t_Code1").Specific.Value = oMatrix1.GetCellSpecific("2", pVal.Row).Value
                                            frm.Items.Item("1").Click()
                                        End If

                                    Case "6"
                                        'Link button in Inst code
                                        If pVal.BeforeAction And pVal.ColUID = "6" Then
                                            oApplication.Menus.Item("TLS").Activate()
                                        Else
                                            Dim frm As SAPbouiCOM.Form
                                            frm = oApplication.Forms.ActiveForm
                                            frm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                            frm.Items.Item("5").Specific.Value = oMatrix1.GetCellSpecific("6", pVal.Row).Value
                                            frm.Items.Item("1").Click()
                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Link Pressed Event Failed:" & ex.Message)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "Enforce"
                                Try
                                    If pVal.BeforeAction = False And frmControlPanel.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or
                                        frmControlPanel.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Or
                                        frmControlPanel.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                                        CheckEnforceSampling()
                                    End If
                                Catch ex As Exception
                                    oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message)
                                End Try
                       
                            Case "1"
                                If pVal.ActionSuccess And frmControlPanel.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If
                            Case "43"
                                If pVal.BeforeAction = False Then
                                    frmControlPanel.PaneLevel = 1
                                    'frmControlPanel.Items.Item("17").AffectsFormMode = False
                                    'frmControlPanel.Settings.MatrixUID = "Machine"
                                End If
                            Case "44"
                                If pVal.BeforeAction = False Then
                                    frmControlPanel.PaneLevel = 2
                                    'frmControlPanel.Items.Item("17").AffectsFormMode = False
                                    'frmControlPanel.Settings.MatrixUID = "Machine"
                                End If
                            Case "f_attach"
                                If pVal.BeforeAction = False Then
                                    frmControlPanel.PaneLevel = 3
                                    'frmControlPanel.Items.Item("18").AffectsFormMode = False
                                    'frmControlPanel.Settings.MatrixUID = "Total"
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Select Case pVal.ItemUID
                        Case "Sample"
                            Select Case pVal.ColUID
                                Case "LtFrmSiz"
                                    oGFun.SetNewLine(oMatrix2, oDBDSDetail2, oMatrix2.VisualRowCount, pVal.ColUID)

                            End Select
                    End Select

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item  Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub


    Sub CheckEnforceSampling()

        Try
            Dim Enforce As SAPbouiCOM.CheckBox = frmControlPanel.Items.Item("Enforce").Specific
            If Enforce.Checked = True Then
                frmControlPanel.Items.Item("44").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                frmControlPanel.Items.Item("44").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            Else
                frmControlPanel.Items.Item("44").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText("CheckEnforceSampling Method Failed :" & ex.Message)
        End Try
    End Sub

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()
                Case "1293"
                    oGFun.DeleteRow(oMatrix1, oDBDSDetail)
                    'Changed on 01.04.13
                    'Case "1292"
                    '    'oGFun.SetNewLine(oMatrix1, oDBDSDetail)
                    '    oMatrix1.AddRow()

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub



    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Dim rsetUpadteIndent As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Try
                        If BusinessObjectInfo.BeforeAction Then
                            If Me.ValidateAll() = False And frmControlPanel.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                System.Media.SystemSounds.Asterisk.Play()
                                BubbleEvent = False
                                Exit Sub
                            Else



                            End If
                        End If

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("FormDataEvent Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If BusinessObjectInfo.ActionSuccess = True Then
                        CheckEnforceSampling()
                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


End Class
