﻿
Imports System.Reflection

Public Class OperationsList

    Private oDBDSHeader As SAPbouiCOM.DBDataSource
    Private frmOperationsList As SAPbouiCOM.Form
    Private oMatrix As SAPbouiCOM.Matrix
    Private oMatrix4 As SAPbouiCOM.Matrix
    Private oGrid As SAPbouiCOM.Grid

    '~~~~~~~~~~~~~~Log_Inf~~~~~~~~~~~~~~~~~~
    Dim str_log_Error As Boolean
    Dim str_log_Info As Boolean
    Dim str_log_Severe As Boolean
    Dim int_Run_No As Integer
    Dim str_Session As Integer = 0
    Dim edit_Track_Msg As String
    Dim str_Log_Info_In_Tran As String = ""
    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    Public Sub LoadForm(ByVal query As String, ByVal Formtype As String, ByVal Type As String)
        Try

            oGFun.LoadXML(frmOperationsList, OperationsListFormID, OperationsListXML)
            frmOperationsList = oApplication.Forms.Item(OperationsListFormID)
            'oDBDSHeader = frmOperationsList.DataSources.DBDataSources.Item(0)
            'oMatrix = frmOperationsList.Items.Item("Matrix").Specific
            frmOperationsList.Items.Item("Grid").Visible = True
            oGrid = frmOperationsList.Items.Item("Grid").Specific
            frmOperationsList.DataSources.DataTables.Add("DataTable")



            InitForm(query, Formtype, Type)

        Catch ex As Exception
            oGFun.Msg("LoadForm :" + ex.Message, "E")
            frmOperationsList.Freeze(False)
        End Try
    End Sub


    Public Sub InitForm(ByVal query As String, ByVal formtype As String, ByVal type As String)
        Try
            frmOperationsList = oApplication.Forms.ActiveForm
            frmOperationsList.Freeze(True)



            Dim dt As SAPbouiCOM.DataTable


            oGrid.DataTable = frmOperationsList.DataSources.DataTables.Item("DataTable")
            oGrid.DataTable.Clear()

            'Dim strQ As String = " select * from (select m.DocNum PurchaseOrder ,d.U_ItemCode ItemCode,d.U_ItemName ItemName ,d.U_PlanQty PlannedQty ,d.U_IssQty IssueQty ,(d.U_PlanQty -d.U_IssQty ) PendingQty,d.U_UOM  UOM ,d.U_LabItem,m.U_DraftKey,d.U_BaseQty BaseQty from " & _
            '                     " OPOR m  left outer join  [@SHJ_JKP_ISS1] d on m.DocNum =d.U_PONO   " & _
            '                     " where  d.U_DraftKey =m.U_DraftKey and DocNum in(" & Pro_No & ") )aa where aa.PendingQty > 0  "



            frmOperationsList.DataSources.DataTables.Item("DataTable").Clear()
            frmOperationsList.DataSources.DataTables.Item("DataTable").ExecuteQuery(query)
            oGrid.DataTable = frmOperationsList.DataSources.DataTables.Item("DataTable")

        Catch ex As Exception

            oGFun.Msg("InitForm :" + ex.Message, "E")
        Finally
            frmOperationsList.Freeze(False)
        End Try
    End Sub

    Public Sub DefineModesForFields()
        Try
        Catch ex As Exception
            oGFun.Msg("DefineModesforFields :" + ex.Message, "E")
        End Try
    End Sub

    Public Function ValidateAll() As Boolean
        Try
            Return True
        Catch ex As Exception
            oGFun.Msg("ValidateAll :" + ex.Message, "E")
            Return False
        End Try
    End Function

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType


            End Select
        Catch ex As Exception
            oGFun.Msg("Item Event Faild:" & ex.Message, "E")
        Finally
        End Try
    End Sub

    Public Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    'InitForm()
                    Exit Select
                Case "_Log_Info"
                   
            End Select
        Catch ex As Exception
            oGFun.Msg("MenuEvent :" + ex.Message, "E")
        End Try
    End Sub

   

End Class
