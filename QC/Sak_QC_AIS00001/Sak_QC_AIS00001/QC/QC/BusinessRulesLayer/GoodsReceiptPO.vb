﻿
Public Class GoodsReceiptPO

    Public frmGoodsReceiptPO As SAPbouiCOM.Form
    Public frmSubContract As SAPbouiCOM.Form
    Dim oDBDSHeader As SAPbouiCOM.DBDataSource
    Public oDBDSDetail, oDBDSDetail_SUG As SAPbouiCOM.DBDataSource
    Public DataTable_SUG As New DataTable
    Dim oMatrix, oMatrix_SUG As SAPbouiCOM.Matrix
    Dim PLineId_SUG As Integer
    Dim ParentItemCode As String = ""
    Public strSQL As String
    Dim FrmID As String = ""
    Sub LoadForm()
        Try
            frmGoodsReceiptPO = oApplication.Forms.GetFormByTypeAndCount("143", 1)
            FrmId = frmGoodsReceiptPO.UniqueID

            oDBDSHeader = frmGoodsReceiptPO.DataSources.DBDataSources.Item(0)
            oDBDSDetail = frmGoodsReceiptPO.DataSources.DBDataSources.Item(1)
            oMatrix = frmGoodsReceiptPO.Items.Item("38").Specific
            Me.InitForm()
            Me.DefineModesForFields()
        Catch ex As Exception
            oGFun.Msg("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub

    Sub InitForm()
        Try

            frmGoodsReceiptPO.Freeze(True)

            'Add DataTable

            DataTable_SUG.Columns.Clear()

            DataTable_SUG.Columns.Add("PLineId")
            DataTable_SUG.Columns.Add("BaseNum")
            DataTable_SUG.Columns.Add("PRONo")
            DataTable_SUG.Columns.Add("BaseEntry")
            DataTable_SUG.Columns.Add("BLineId")
            DataTable_SUG.Columns.Add("WONo")
            DataTable_SUG.Columns.Add("OpCode")
            DataTable_SUG.Columns.Add("OpName")
            DataTable_SUG.Columns.Add("OpQty")
            DataTable_SUG.Columns.Add("IssueQty")
            DataTable_SUG.Columns.Add("ReqQty")
            DataTable_SUG.Columns.Add("BalQty")
            DataTable_SUG.Columns.Add("RecQty")

            oMatrix.Columns.Item("24").Width = 0
            Try
                oGFun.setEditTextColumnCFL(frmGoodsReceiptPO, oMatrix, "U_WhsCode", "cfl_Whs", "64", "WhsCode")
            Catch ex As Exception

            End Try

        Catch ex As Exception
            oGFun.Msg("InitForm Method Failed:" & ex.Message)
        Finally
            frmGoodsReceiptPO.Freeze(False)
        End Try
    End Sub

    Sub DefineModesForFields()
        Try

        Catch ex As Exception
            oApplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function ValidateAll() As Boolean

        Try
            If frmGoodsReceiptPO.Items.Item("3").Specific.Selected.Value.ToString.Trim = "S" Then
                Return True
            End If

            For i As Integer = 1 To oMatrix.VisualRowCount - 1
                If oMatrix.GetCellSpecific("U_WhsCode", i).Value.ToString.Trim = "" Then
                    oGFun.Msg("Line No : " & i.ToString & " Warehouse Code Should be Enter..")
                    Return False
                End If
            Next

            ValidateAll = True

        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
                    Try
                        If pVal.BeforeAction = False Then
                            Me.LoadForm()
                        End If
                    Catch ex As Exception
                        oGFun.Msg("Form Load Event Failed:" & ex.Message)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        'Assign Selected Rows
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        '        'Filter before open the CFL
                        If pVal.BeforeAction Then
                            Select Case oCFLE.ChooseFromListUID
                                'Case "cfl_Whs"
                                'Dim ItemCode As String = oMatrix.GetCellSpecific("1", pVal.Row).Value
                                'Dim Excisable As String = oGFun.getSingleValue("select Excisable from OITM where ItemCode = '" & ItemCode & "' ")
                                'oGFun.ChooseFromListFilteration(frmGoodsReceiptPO, "cfl_Whs", "Excisable", "Select '" & Excisable & "' ")

                            End Select
                        End If
                        If pVal.BeforeAction = False And Not oDataTable Is Nothing Then
                            '            'Assign Value to selected control
                            Select Case oCFLE.ChooseFromListUID
                                Case "cfl_Whs"
                                    Dim oEditText As SAPbouiCOM.EditText

                                    Dim WhsCode As String = oDataTable.GetValue("WhsCode", 0)
                                    Dim ItemCode As String = oMatrix.GetCellSpecific("1", pVal.Row).Value
                                    'Dim DocEntry As String = 
                                    Dim InsReq As String = oGFun.getSingleValue("select isnull(U_InsReq,'N') from OITM where ItemCode = '" & ItemCode & "' ")
                                    ' Dim InsReq As String = oGFun.getSingleValue("select isnull(U_Inspec,'N') from WOR1 where ItemCode='" & ItemCode & "' ")
                                    'Dim Excisable As String = oGFun.getSingleValue("select Excisable from OITM where ItemCode = '" & ItemCode & "' ")
                                    Dim Excisable As String = oGFun.getSingleValue("select Excisable from OWHS where WhsCode = '" & WhsCode & "' ")
                                    Dim Location As String = oGFun.getSingleValue("select Location from OWHS where WhsCode = '" & WhsCode & "' ")
                                    Dim QcWhCod As String = oGFun.getSingleValue("select " & IIf(Excisable.Trim = "Y", " U_QcEWhCod ", " U_QcWhCod ") & " from [@MIPL_QCGT1] where U_LocCode = '" & Location & "' and U_Type='GRN' ")

                                    'If QcWhCod.Trim = "" Then
                                    '    oGFun.Msg("QC. Setting Not Define for this Location..")
                                    '    Return
                                    'End If

                                    oEditText = oMatrix.GetCellSpecific("24", pVal.Row)

                                    Try
                                        If InsReq = "N" Then
                                            oEditText.Value = oDataTable.GetValue("WhsCode", 0)
                                        Else
                                            oEditText.Value = QcWhCod
                                        End If
                                    Catch ex As Exception
                                    End Try

                                    Try
                                        oEditText = oMatrix.GetCellSpecific("U_WhsCode", pVal.Row)
                                        oEditText.Value = oDataTable.GetValue("WhsCode", 0)
                                    Catch ex As Exception
                                    End Try

                            End Select
                        End If
                    Catch ex As Exception
                        oGFun.Msg("Choose form List Event Failed:" & ex.Message)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    Try
                        Select Case pVal.ItemUID


                            Case "10000329"
                                If pVal.BeforeAction = False Then
                                    Dim ocmb As SAPbouiCOM.ComboBox = frmGoodsReceiptPO.Items.Item("10000329").Specific
                                    If ocmb.Selected.Description = "18" And frmGoodsReceiptPO.Items.Item("3").Specific.Selected.Value.ToString.Trim = "I" Then
                                        Dim DocNum As String = frmGoodsReceiptPO.Items.Item("8").Specific.Value
                                        Dim strSql = " select Count(*) from ( select OPDN.DocNum , PDN1.ItemCode , SUM(isnull(PDN1.Quantity,0)) - " & _
                                               " isnull((Select SUM(isnull(OINS.U_InsQty,0))  from [@MIPL_OINS]  OINS where " & _
                                               " OPDN.DocNum =OINS.U_GRNNo and PDN1.ItemCode =OINS.U_ItemCode ),0) Qty from " & _
                                               " OPDN,PDN1 where OPDN.DocStatus ='O' and OPDN.DocEntry =PDN1.DocEntry and " & _
                                               " PDN1.ItemCode in (Select Itemcode from OITM where isnull(U_InsReq,'N') = 'Y' and PDN1.U_WhsCode Is not Null) " & _
                                               " Group By OPDN.DocNum , PDN1.ItemCode ) AA where  Qty > 0 AND DocNum = '" & DocNum & "' "
                                        Dim count = oGFun.getSingleValue(strSql)
                                        count = IIf(count.Trim = "", 0, count)
                                        If CInt(count) > 0 Then
                                            oGFun.Msg("Inspection Not Yet Completed..")
                                            oApplication.Forms.ActiveForm.Close()
                                        Else
                                            strSql = "select DocNum from [@MIPL_OINS] where isnull(U_RejQty,0) >0 and ISNULL(U_Return,'') = '' and isnull(U_GRNNo,'') = '" & DocNum & "'"
                                            Dim InsNo = oGFun.getSingleValue(strSql)

                                            If InsNo.Trim <> "" Then
                                                oGFun.Msg("Inspection DocNum : " & InsNo & ", Return GRN Not Yet Completed..")
                                                oApplication.Forms.ActiveForm.Close()
                                            Else
                                                oAPInvoice.LoadForm()
                                            End If
                                        End If
                                    End If

                                End If
                        End Select
                    Catch ex As Exception
                        oGFun.Msg("Click Event Failed:" & ex.Message)
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                'Add,Update Event
                                If pVal.BeforeAction = True And _
                                    (frmGoodsReceiptPO.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or _
                                     frmGoodsReceiptPO.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then

                                    If Me.ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Return

                                    End If
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
                    Try
                        If BusinessObjectInfo.BeforeAction Then
                            If Me.ValidateAll() = False Then
                                System.Media.SystemSounds.Asterisk.Play()
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                    Finally

                    End Try

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If BusinessObjectInfo.ActionSuccess = True Then
                        Try
                            'LoadGRNDetails()
                        Catch ex As Exception
                            oApplication.StatusBar.SetText("Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        End Try
                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

End Class
