﻿Public Class GeneralSetting
    Public frmGeneralSetting As SAPbouiCOM.Form
    Dim oDBDSHeader As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim UDOID As String = "QCGT"

    Sub LoadForm()
        Try
            oGFun.LoadXML(frmGeneralSetting, GeneralSettingFormID, GeneralSettingXML)
            frmGeneralSetting = oApplication.Forms.Item(GeneralSettingFormID)
            'Assign Data Source
            oDBDSHeader = frmGeneralSetting.DataSources.DBDataSources.Item(0)
            oDBDSDetail = frmGeneralSetting.DataSources.DBDataSources.Item(1)
            'Assign Matrix
            oMatrix = frmGeneralSetting.Items.Item("Matrix").Specific
            Me.DefineModesForFields()
            Me.InitForm()
        Catch ex As Exception
            oGFun.Msg("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmGeneralSetting.Freeze(True)
            Dim code = oGFun.getSingleValue("Select top 1 Code from [@MIPL_OQCGT] ")
            code = IIf(code.Trim = "", 0, code)

            If CDbl(code) <> 0 Then

                oMatrix.Clear()
                oDBDSDetail.Clear()
                oGFun.SetNewLine(oMatrix, oDBDSDetail)


                oGFun.setComboBoxValue(oMatrix.GetCellSpecific("t_Type", 1), "Select 'GRN','GRN' Union All Select 'IN','Inhouse'")
                oGFun.setComboBoxValue(oMatrix.GetCellSpecific("t_OPType", 1), "Select Code, Name from [@AIS_WrkTyp] ")

                oGFun.setComboBoxValue(oMatrix.GetCellSpecific("LocCode", 1), "Select Code, Location from OLCT ")

                frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                frmGeneralSetting.Items.Item("t_Code").Specific.Value = code
                frmGeneralSetting.Items.Item("1").Click()

            Else
                frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                oMatrix.Clear()
                oDBDSDetail.Clear()
                oGFun.SetNewLine(oMatrix, oDBDSDetail)
                oGFun.setComboBoxValue(oMatrix.GetCellSpecific("LocCode", 1), "Select Code, Location from OLCT ")

            End If

            ' Enable ArrowKey changed on 15/04/2013
            oMatrix.CommonSetting.EnableArrowKey = True

        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmGeneralSetting.Freeze(False)

        End Try
    End Sub

    Sub DefineModesForFields()
        Try
        Catch ex As Exception
            oApplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function ValidateAll() As Boolean
        Try

            Dim strLocList = ""

            'Datail level Validation
            For i As Integer = 1 To oMatrix.VisualRowCount - 1

                If oMatrix.Columns.Item("LocCode").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                    oGFun.Msg("Line No." & i & " Location Should Not Be Left Empty")
                    Return False
                End If

                Dim strLoc = oMatrix.Columns.Item("LocCode").Cells.Item(i).Specific.value.ToString.Trim
                If strLocList.Contains("/" + strLoc + "/") = True Then
                    oGFun.Msg("Line No." & i & " Location Should Not be Duplicate")
                    Return False
                End If
                strLocList = strLocList + "/" + strLoc + "/"

                If oMatrix.Columns.Item("AccWhCode").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                    oGFun.Msg("Line No." & i & " Acc. Whscode Should Not Be Left Empty")
                    Return False
                End If

                If oMatrix.Columns.Item("RejWhCode").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                    oGFun.Msg("Line No." & i & " Rej. Whscode Should Not Be Left Empty")
                    Return False
                End If

                If oMatrix.Columns.Item("RewWhCode").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                    oGFun.Msg("Line No." & i & " ReW. Whscode Should Not Be Left Empty")
                    Return False
                End If

                If oMatrix.Columns.Item("StrWhCode").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                    oGFun.Msg("Line No." & i & " Store Whscode Should Not Be Left Empty")
                    Return False
                End If

                If oMatrix.Columns.Item("ScrWhCode").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                    oGFun.Msg("Line No." & i & " Scrap Whscode Should Not Be Left Empty")
                    Return False
                End If

                'If oMatrix.Columns.Item("QcWhCod").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                '    oGFun.Msg("Line No." & i & " Store Whscode Should Not Be Left Empty")
                '    Return False
                'End If

                'If oMatrix.Columns.Item("QcEWhCod").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                '    oGFun.Msg("Line No." & i & " Scrap Whscode Should Not Be Left Empty")
                '    Return False
                'End If
            Next

            If oMatrix.VisualRowCount = 1 Then
                oGFun.Msg("Details Should Not Be Left Empty")
                Return False
            End If

            ValidateAll = True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        'Assign Selected Rows
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent = pVal
                        oDataTable = oCFLE.SelectedObjects
                        Dim rset As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        'Filter before open the CFL
                        If pVal.BeforeAction And pVal.Row <> 0 Then
                            Dim LocCode = oMatrix.GetCellSpecific("LocCode", pVal.Row).Value
                            Select Case oCFLE.ChooseFromListUID
                                'Changed by sankaralakshmi
                                Case "cfl_Acc"
                                    oGFun.ChooseFromListFilteration(frmGeneralSetting, "cfl_Acc", "WhsCode", "select WhsCode from OWHS where Location='" & LocCode & "'")
                                Case "cfl_Rej"
                                    oGFun.ChooseFromListFilteration(frmGeneralSetting, "cfl_Rej", "WhsCode", "select WhsCode from OWHS where Location='" & LocCode & "'")
                                Case "cfl_Rew"
                                    oGFun.ChooseFromListFilteration(frmGeneralSetting, "cfl_Rew", "WhsCode", "select WhsCode from OWHS where Location='" & LocCode & "'")
                                Case "cfl_Scr"
                                    oGFun.ChooseFromListFilteration(frmGeneralSetting, "cfl_Scr", "WhsCode", "select WhsCode from OWHS where Location='" & LocCode & "'")
                                Case "cfl_Str"
                                    oGFun.ChooseFromListFilteration(frmGeneralSetting, "cfl_Str", "WhsCode", "select WhsCode from OWHS where Location='" & LocCode & "'")
                                Case "cfl_Qc"
                                    oGFun.ChooseFromListFilteration(frmGeneralSetting, "cfl_Qc", "WhsCode", "Select WhsCode from OWHS Where Excisable = 'N' And Location = '" & LocCode & "' ")
                                Case "cfl_QcE"
                                    oGFun.ChooseFromListFilteration(frmGeneralSetting, "cfl_QcE", "WhsCode", "Select WhsCode from OWHS Where Excisable = 'Y' And Location = '" & LocCode & "' ")
                                Case "cfl_Sub"
                                    oGFun.ChooseFromListFilteration(frmGeneralSetting, "cfl_Sub", "WhsCode", "select WhsCode from OWHS where Location='" & LocCode & "'")

                            End Select
                        End If
                        If pVal.BeforeAction = False And Not oDataTable Is Nothing Then
                            'Assign Value to selected control
                            Select Case oCFLE.ChooseFromListUID

                                ' change work 

                                Case "cfl_Qc"

                                    oMatrix.FlushToDataSource()
                                    oDBDSDetail.SetValue("U_QcWhCod", pVal.Row - 1, oDataTable.GetValue("WhsCode", 0))
                                    oDBDSDetail.SetValue("U_QcWhNam", pVal.Row - 1, oDataTable.GetValue("WhsName", 0))
                                    oMatrix.LoadFromDataSourceEx()

                                Case "cfl_QcE"

                                    oMatrix.FlushToDataSource()
                                    oDBDSDetail.SetValue("U_QcEWhCod", pVal.Row - 1, oDataTable.GetValue("WhsCode", 0))
                                    oDBDSDetail.SetValue("U_QcEWhNam", pVal.Row - 1, oDataTable.GetValue("WhsName", 0))
                                    oMatrix.LoadFromDataSourceEx()

                                Case "cfl_Sub"

                                    oMatrix.FlushToDataSource()
                                    oDBDSDetail.SetValue("U_SubWhCod", pVal.Row - 1, oDataTable.GetValue("WhsCode", 0))
                                    oDBDSDetail.SetValue("U_SubWhNam", pVal.Row - 1, oDataTable.GetValue("WhsName", 0))
                                    oMatrix.LoadFromDataSourceEx()

                                    'Assign Customer
                                Case "cfl_Acc", "cfl_Rej", "cfl_Rew", "cfl_Scr", "cfl_Str"
                                    oMatrix.FlushToDataSource()
                                    Dim n As Integer = pVal.Row - 1
                                    'For Multi Row Select
                                    oMatrix.FlushToDataSource()
                                    oDBDSDetail.SetValue(oMatrix.Columns.Item(pVal.ColUID).DataBind.Alias, pVal.Row - 1, oDataTable.GetValue("WhsCode", 0))
                                    oDBDSDetail.SetValue(oMatrix.Columns.Item(pVal.ColUID).DataBind.Alias.ToString.Replace("Code", "Nam"), pVal.Row - 1, oDataTable.GetValue("WhsName", 0))
                                    oMatrix.LoadFromDataSourceEx()
                                    If oCFLE.ChooseFromListUID = "cfl_Acc" Then oGFun.SetNewLine(oMatrix, oDBDSDetail, pVal.Row, "AccWhCode")
                            End Select
                        End If

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "AccWhCode"
                                If pVal.BeforeAction = False Then
                                    For i As Integer = 1 To oMatrix.VisualRowCount - 1
                                        If oMatrix.Columns.Item("AccWhCode").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                                            oMatrix.Columns.Item("AccWhName").Cells.Item(i).Specific.value.ToString.Trim.Equals("")
                                        End If
                                    Next
                                End If
                        End Select

                    Catch ex As Exception

                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                'Add,Update Event
                                If pVal.BeforeAction = True And frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    If Me.ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Exit Sub
                                    End If
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        BubbleEvent = False
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Select Case pVal.ItemUID
                        Case "1"
                            If pVal.BeforeAction = False And frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                Me.InitForm()
                            End If
                    End Select

                    'changed by sankaralakshmi

                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    Try
                        Select Case pVal.ItemUID
                            Case "Matrix"
                                If pVal.BeforeAction = False And pVal.ColUID = "LocCode" And frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                                    oMatrix.Columns.Item("AccWhCode").Cells.Item(pVal.Row).Specific.Value = ""
                                    oMatrix.Columns.Item("AccWhNam").Cells.Item(pVal.Row).Specific.Value = ""
                                    oMatrix.Columns.Item("RejWhCode").Cells.Item(pVal.Row).Specific.Value = ""
                                    oMatrix.Columns.Item("RejWhNam").Cells.Item(pVal.Row).Specific.Value = ""
                                    oMatrix.Columns.Item("RewWhCode").Cells.Item(pVal.Row).Specific.Value = ""
                                    oMatrix.Columns.Item("V_RewWhNam").Cells.Item(pVal.Row).Specific.Value = ""
                                    oMatrix.Columns.Item("StrWhCode").Cells.Item(pVal.Row).Specific.Value = ""
                                    oMatrix.Columns.Item("StrWhNam").Cells.Item(pVal.Row).Specific.Value = ""
                                    oMatrix.Columns.Item("ScrWhCode").Cells.Item(pVal.Row).Specific.Value = ""
                                    oMatrix.Columns.Item("ScrWhNam").Cells.Item(pVal.Row).Specific.Value = ""
                                End If
                                'End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message)
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    ' Me.InitForm()
                Case "1293"
                    oGFun.DeleteRow(oMatrix, oDBDSDetail)
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE

                    Dim rsetUpadteIndent As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Try
                        If BusinessObjectInfo.BeforeAction Then
                            If Me.ValidateAll() = False And frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                System.Media.SystemSounds.Asterisk.Play()
                                BubbleEvent = False
                                Exit Sub
                            Else
                                'Delete Last Empty Row
                                oGFun.DeleteEmptyRowInFormDataEvent(oMatrix, "AccWhCode", oDBDSDetail)
                                If frmGeneralSetting.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then

                                    oDBDSHeader.SetValue("Code", 0, oGFun.GetCodeGeneration("[@MIPL_OQCGT]"))
                                End If
                            End If
                        End If
                        If BusinessObjectInfo.ActionSuccess = True Then
                            ' InitForm()

                        End If
                    Catch ex As Exception
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If BusinessObjectInfo.ActionSuccess = True Then
                        oGFun.SetNewLine(oMatrix, oDBDSDetail)
                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

End Class
