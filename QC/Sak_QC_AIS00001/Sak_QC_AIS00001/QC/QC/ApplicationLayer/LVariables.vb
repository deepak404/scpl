﻿Imports SDKLib881

Module LVariables

#Region " ... General Purpose ..."

    Public v_RetVal, v_ErrCode As Long
    Public v_ErrMsg As String = ""
    Public addonName As String = "QC"
    Public oCompany As SAPbobsCOM.Company
    'Attachment Option
    Public ShowFolderBrowserThread As Threading.Thread
    Public BankFileName As String
    Public boolModelForm As Boolean = False
    Public boolModelFormID As String = ""
    Public oGFun As New GFun(addonName)

    Public HWKEY() As String = New String() {"L0287935425", "Z0464796071", "C0529684196", "E2081971285", "G1592142987", "J0125909377"}


#End Region

#Region " ... Common For Module ..."

    'AddOn Selection

    Public LV_WIP As String = ""
    Public LV_QC As String = ""
    Public LV_CPL As String = ""
    Public LV_WOC As String = ""
    Public LV_PCK As String = ""
    Public LV_BLD As String = ""
    Public LV_PR As String = ""

    ' Item Group List
    Public FinishedGoods As String = ""
    Public RawMaterial As String = ""
    Public Operations As String = ""
    Public Machinery As String = ""
    Public Tools As String = ""
    Public Scrap As String = ""
    Public consumables As String = ""
    Public LV_SubCantractItem As String = "JS1500008"

    ' Find the whs details of current Location
    Public LV_StrLocCode As String = ""
    Public LV_StrLocName As String = ""
    Public LV_AccWhsName As String = ""
    Public LV_AccWhsCode As String = ""
    Public LV_RejWhsName As String = ""
    Public LV_RejWhsCode As String = ""
    Public LV_RewWhsName As String = ""
    Public LV_RewWhsCode As String = ""
    Public LV_ScrWhsName As String = ""
    Public LV_ScrWhsCode As String = ""
    Public LV_StrWhsName As String = ""
    Public LV_StrWhsCode As String = ""
    Public LV_SubWhsCode As String = ""
    Public LV_SubWhsName As String = ""

    'Capacity Planning

    Public LV_Height As Integer = 0
    Public LV_Width As Integer = 0
    Public LV_Top As Integer = 0
    Public LV_Left As Integer = 0
    Public LV_State As SAPbouiCOM.BoFormStateEnum = SAPbouiCOM.BoFormStateEnum.fs_Restore

    'Shift Time
    Public LV_ShiftTime = 8

    'No object Form Id
    Public LV_PmrForm As String = ""

#End Region

#Region " ... Common For Forms ..."

#Region "Qc "

    'General Setting
    Public GeneralSettingFormID As String = "QCGT"
    Public GeneralSettingXML As String = "GeneralSetting.xml"
    Public oGeneralSetting As New GeneralSetting

    'Parameter Master
    'Public ParameterMasterFormID As String = "QCPM"
    'Public ParameterMasterXML As String = "ParameterMaster.xml"
    'Public oParameterMaster As New ParameterMaster

    'Parameter Master
    Public ParameterMasterFormID As String = "QPM"
    Public ParameterMasterXML As String = "ParameterDocument.xml"
    Public oParameterMaster As New ParameterMaster

    'Control Panel
    Public ControlPanelFormID As String = "QCCP"
    Public ControlPanelXML As String = "ControlPanel.xml"
    Public oControlPanel As New ControlPanel

    'Inspection
    Public InspectionFormID As String = "INS"
    Public InspectionXML As String = "Inspection.xml"
    Public oInspection As New Inspection

    'Corrective/Preventive Action
    Public CorrectivePreventiveActionFormID As String = "QCPA"
    Public CorrectivePreventiveActionXML As String = "CorrectivePreventiveAction.xml"
    Public oCorrectivePreventiveAction As New CorrectivePreventiveAction



    'Operations Issue List
    Public OperationsListFormID = "OPER", OperationsListXML As String = "OperationsList.xml"
    Public oOperationsList As New OperationsList



    'Item Master
    Public ItemMastertypeex As String = "150"
    Public ItemMastermenuid As String = "3073"
    'Public oItemMaster As New ItemMaster

    'Purchase Order
    Public PurchaseOrderMenuID As String = "2305"
    Public PurchaseOrderFormType As String = "142"
    Public oPurchaseOrder As New PurchaseOrder

    ' Goods Receipt PO
    Public GoodsReceiptPOMenuID As String = "2306"
    Public GoodsReceiptPOFormType As String = "143"
    Public oGoodsReceiptPO As New GoodsReceiptPO

    ' AP Invoice
    Public APInvoiceMenuID As String = "2308"
    Public APInvoiceFormType As String = "141"
    Public oAPInvoice As New APInvoice

    'UserDefaultGroup
    Public UserDefaultGroupMenuID As String = "8462"
    Public UserDefaultGroupFormType As String = "153"
    Public oUserDefaultGroup As New UserDefaultGroup

#End Region

#End Region

End Module