
If @object_type='67' and @transaction_type in (N'A') 
Begin

IF Exists(select * from OWTR T0 Inner Join WTR1 T1 on T0.DocEntry=T1.DocEntry where T0.DocEntry=@list_of_cols_val_tab_del and T1.fromWhsCod='WAH0020'and UserSign!=1)
BEGIN
SELECT @Error = -5001
SELECT @error_message ='Stock Transfer Should not be allowed' 
END
End

If @object_type='60' and @transaction_type in (N'A') 
Begin
IF Exists(select * from OIGE T0 Inner Join IGE1 T1 on T0.DocEntry=T1.DocEntry where T0.DocEntry=@list_of_cols_val_tab_del and T1.WhsCode='WAH0020' and UserSign!=1)
BEGIN
SELECT @Error = -5001
SELECT @error_message ='Goods Issue Should not be allowed' 
END
End