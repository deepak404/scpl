 
Create Procedure [dbo].[@AIS_QC_WorkOrderProductionReceiptInspectionFilterLineID](@WorkOrderDocNum Int) 

As

Begin



 select T1.U_BaseNum   from [@AS_OWORD] T0 Inner Join [@AS_WORD4]  T1 On T0.DocEntry =T1.DocEntry 

 Inner Join [@AS_OIGN] T2 on T2.DocNum = T1.U_BaseNum 

 Inner Join [@AS_IGN5] T3 on T3.DocEntry = T2.DocEntry   

 Inner Join OITM T4 On T4.ItemCode =T3.U_itemcode    

 Where Isnull(T1.U_BaseNum,'')!='' and  Convert(varchar(max), T2.DocNum )  +'-'+ T3.U_itemcode    
  Not in (Select Isnull(U_Recno,'')+'-'+ isnull(U_icode,'')    from [@MIPL_OINS] Where U_Type ='IN') 

 ANd T4.U_InsReq ='Y' ANd U_WONo =@WorkOrderDocNum 

 and T1.LineId =1

 union All  



 select T1.U_BaseNum   from [@AS_OWORD] T0 Inner Join [@AS_WORD4]  T1 On T0.DocEntry =T1.DocEntry 

  Inner Join [@AS_OIGN] T2 on T2.DocNum = T1.U_BaseNum 

   Inner Join [@AS_IGN5] T3 on T3.DocEntry = T2.DocEntry   

    Inner Join OITM T4 On T4.ItemCode =T3.U_itemcode    

	 Inner Join [@AS_IGN4] T5 on T5.DocEntry = T2.DocEntry   

	  Where Isnull(T1.U_BaseNum,'')!='' and  Convert(varchar(max), T2.DocNum ) +'-'+ T3.U_itemcode     
	  Not in (Select   Isnull(U_Recno,'')+'-'+ isnull(U_icode,'')    from [@MIPL_OINS] Where U_Type ='IN') 

	    ANd T4.U_InsReq ='Y' ANd U_WONo =@WorkOrderDocNum  ANd T5.U_Type !='N'   
end 


