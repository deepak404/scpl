

CREATE Procedure [@AIS_Production_ProductionReceipt_GetFGItemDetails](@OperationCode varchar(max),@OperationType Varchar(max) ) 

AS

Begin

 

select U_FgCode,isnull(U_Desc,'')'Desc',Case when T1.U_InsReq ='Y' then 

(Select Isnull(U_QcWhCod,'') From  [@MIPL_QCGT1] Where U_OPType =@OperationType and U_Type ='IN'  )  

 else   (Select U_WhsCode From  [@AIS_WRKTYP] Where code=@OperationCode )  end U_Whs,

 Case when T1.U_InsReq ='Y' then 

(Select Isnull(U_QcWhNam,'') From  [@MIPL_QCGT1] Where U_OPType =@OperationType and U_Type ='IN'  )  

 else   (Select U_WhsName From  [@AIS_WRKTYP] Where code=@OperationCode )  end      U_WName,U_Type  

from [@AS_PRN4] T0 Inner Join OITM T1 On  T0.U_FgCode=T1.ItemCode 

  WHERE Code = @OperationCode  and ISNULL (U_FgCode,'')!='' 





  END 


