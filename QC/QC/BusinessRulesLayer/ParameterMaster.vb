﻿Public Class ParameterMaster
    Public frmParameterMaster As SAPbouiCOM.Form
    Dim t_Code1 As SAPbouiCOM.EditText
    Dim oDBDSHeader As SAPbouiCOM.DBDataSource
    Dim oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim UDOID As String = "QPM"

    Sub LoadForm()
        Try
            oGFun.LoadXML(frmParameterMaster, ParameterMasterFormID, ParameterMasterXML)
            frmParameterMaster = oApplication.Forms.Item(ParameterMasterFormID)
            oDBDSHeader = frmParameterMaster.DataSources.DBDataSources.Item(0)
            Me.InitForm()
            Me.DefineModesForFields()
        Catch ex As Exception
            oGFun.Msg("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmParameterMaster.Freeze(True)

            oGFun.LoadComboBoxSeries(frmParameterMaster.Items.Item("c_series").Specific, "QPM")
            'Me.LoadDocNum()
            'Dim c_Type As SAPbouiCOM.ComboBox = frmParameterMaster.Items.Item("c_Type").Specific
            'oGFun.setComboBoxValue(c_Type, "select Code,Name from [@MIPL_PMRTYPE] order by code")
            Try
                ' c_Type.ValidValues.Add("-1", "Define New")
            Catch ex As Exception
            End Try

        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            'oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Finally
            frmParameterMaster.Freeze(False)

        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Sub DefineModesForFields()
        Try
            frmParameterMaster.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmParameterMaster.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 3, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmParameterMaster.Items.Item("t_Code1").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmParameterMaster.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmParameterMaster.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 3, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            frmParameterMaster.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
        Catch ex As Exception
            oApplication.StatusBar.SetText("DefineModesForFields Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function ValidateAll() As Boolean
        Try
            'If Not oGFun.isDuplicate(frmParameterMaster.Items.Item("t_Code1").Specific, "[@MIPL_OQCPM]", "U_Code", "Code Should not be duplicate") Then
            '    Return False
            'End If
            ValidateAll = True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                'Add,Update Event
                                If pVal.BeforeAction = True Then
                                    'If Me.ValidateAll() = False Then
                                    '    System.Media.SystemSounds.Asterisk.Play()
                                    '    BubbleEvent = False
                                    '    Exit Sub
                                    'End If

                                    If frmParameterMaster.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then

                                        If Not oGFun.isDuplicate(frmParameterMaster.Items.Item("t_Code1").Specific, _
                                                                 "[@MIPL_OQPM]", "U_Code", "Parameter Code Should not be duplicate") Then

                                            BubbleEvent = False
                                            Exit Sub

                                        End If
                                    End If
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    Try
                    Select Case pVal.ItemUID
                            Case "c_Type1"
                                If pVal.BeforeAction = False Then
                                    Dim c_Type As SAPbouiCOM.ComboBox = frmParameterMaster.Items.Item("c_Type").Specific
                                    Dim TblName As String
                                    If c_Type.Selected.Description = "Define New" And
                                  frmParameterMaster.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                        'Load No object PM types Form
                                        For i = 0 To oApplication.Menus.Item("51200").SubMenus.Count - 1
                                            TblName = oApplication.Menus.Item("51200").SubMenus.Item(i).String
                                            TblName = TblName.Remove(TblName.IndexOf("-"))
                                            If Trim(TblName) = "MIPL_PMRTYPE" Then
                                                oApplication.Menus.Item("51200").SubMenus.Item(i).Activate()
                                                LV_PmrForm = oApplication.Forms.ActiveForm.TypeEx
                                                Exit For
                                            End If
                                        Next
                                    End If
                                End If

                            Case "c_series"
                                Try
                                    If frmParameterMaster.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                        oGFun.setDocNum(frmParameterMaster)
                                        Me.LoadDocNum()
                                    End If
                                Catch ex As Exception
                                    oApplication.StatusBar.SetText("Load Series Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                End Try
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED

                    Select Case pVal.ItemUID
                        Case "1"
                            Try
                                If pVal.BeforeAction = False And frmParameterMaster.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    Me.InitForm()
                                End If
                            Catch ex As Exception
                                oApplication.StatusBar.SetText("Item Pressed Event Failed :" & ex.Message)
                            End Try

                    End Select

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()
                Case "1293"
                    oGFun.DeleteRow(oMatrix, oDBDSDetail)
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub LoadDocNum()
        Try
            'Changed on 28/05/2013
            Dim Series As SAPbouiCOM.ComboBox = frmParameterMaster.Items.Item("c_series").Specific
            Dim BeginStr = oGFun.getSingleValue("Select isnull(BeginStr,'') from NNM1 where Series = '" & Series.Selected.Value & "'")
            Dim DocNum = oDBDSHeader.GetValue("DocNum", 0)
            Dim PCode = BeginStr + DocNum
            frmParameterMaster.Items.Item("t_Code1").Specific.value = PCode

        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
                    Dim rsetUpadteIndent As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Try
                        If BusinessObjectInfo.BeforeAction Then
                            If Me.ValidateAll() = False Then
                                System.Media.SystemSounds.Asterisk.Play()
                                BubbleEvent = False
                                Exit Sub
                            Else
                                If frmParameterMaster.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    oGFun.setDocNum(frmParameterMaster)
                                End If
                            End If
                        End If
                        If BusinessObjectInfo.ActionSuccess = True Then
                        End If
                    Catch ex As Exception
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If BusinessObjectInfo.ActionSuccess = True Then
                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

End Class
