﻿Public Class UserDefaultGroup
    Public frmUserDefaultGroup As SAPbouiCOM.Form
    Sub LoadForm()
        Try
            frmUserDefaultGroup = oApplication.Forms.ActiveForm
            Me.InitForm()
        Catch ex As Exception
            oApplication.StatusBar.SetText("Load Form Method Failed:" & ex.Message)
        End Try
    End Sub
    Sub InitForm()
        Try
            frmUserDefaultGroup.Freeze(True)
            Dim oItem As SAPbouiCOM.Item
            Dim oLabel As SAPbouiCOM.StaticText
            Dim oCombo As SAPbouiCOM.ComboBox

            'Add combobox for Location
            oItem = frmUserDefaultGroup.Items.Add("c_Location", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
            oItem.Left = frmUserDefaultGroup.Items.Item("57").Left
            oItem.Width = frmUserDefaultGroup.Items.Item("57").Width
            oItem.Top = frmUserDefaultGroup.Items.Item("57").Top + 20
            oItem.Height = frmUserDefaultGroup.Items.Item("57").Height
            oItem.FromPane = 1
            oItem.ToPane = 1
            oCombo = oItem.Specific
            oCombo.DataBind.SetBound(True, "OUDG", "U_Location")
            frmUserDefaultGroup.Items.Item("c_Location").DisplayDesc = True

            'Add Static text Location
            oItem = frmUserDefaultGroup.Items.Add("l_Location", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = frmUserDefaultGroup.Items.Item("58").Left
            oItem.Width = frmUserDefaultGroup.Items.Item("58").Width
            oItem.Top = frmUserDefaultGroup.Items.Item("c_Location").Top
            oItem.Height = frmUserDefaultGroup.Items.Item("58").Height
            oItem.LinkTo = "c_Location"
            oItem.FromPane = 1
            oItem.ToPane = 1
            oLabel = oItem.Specific
            oLabel.Caption = "Location"

            'Load Location values to combobox 
            oGFun.SetComboBoxValueRefresh(frmUserDefaultGroup.Items.Item("c_Location").Specific, "select code,location from OLCT order by code asc")
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message)
        Finally
            frmUserDefaultGroup.Freeze(False)
        End Try
    End Sub
End Class
