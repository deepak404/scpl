﻿Public Class TableCreation
    Dim ValidValueYesORNo = New String(,) {{"N", "No"}, {"Y", "Yes"}}
    Dim Type = New String(,) {{"Pd", "Product"}, {"Ps", "Process"}}
    Dim NewType = New String(,) {{"IN", "In-house"}, {"GRN", "GRN"}}
    Dim Status1 = New String(,) {{"O", "Open"}, {"C", "Close"}}
    'Dim Item = New String(,) {{"B", "Batch"}, {"S", "Serial"}}
    Dim Priority = New String(,) {{"L", "Low"}, {"M", "Medium"}, {"H", "High"}}
    Dim RefDoc = New String(,) {{"1", "Drawings"}, {"2", "Specification sheet"}, {"3", "Catalog"}, {"4", "Standards"}, {"5", "Others"}}
    Dim QCValidValues = New String(,) {{"-", ""}, {"Y", "Yes"}, {"N", "No"}, {"NA", "Not Applicable"}}
    Dim Material = New String(,) {{"0", "Raw Material"}, {"1", "BoughtoutMaterial"}}
    Sub New()
        Try
            Inspection()

            oGFun.CreateUserFields("OUDG", "Location", "Location", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("POR1", "WhsCode", "Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("POR1", "ItemCode", "ItemCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFieldsComboBox("OITM", "InsReq", "Inspection is Required", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", ValidValueYesORNo)
            oGFun.CreateUserFields("PDN1", "WhsCode", "Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateTable("MIPL_PmrType", "Parameter Types", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            oGFun.CreateTable("MIPL_QC_DEFECT", "Defect Code", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            oGFun.CreateUserFields("@MIPL_QC_DEFECT", "Name", "DefectCodes", SAPbouiCOM.BoFieldsType.ft_AlphaNumeric, 500)
            oGFun.CreateUserFieldsComboBox("@MIPL_QC_DEFECT", "Material", "DefectCodes Material", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Material)
            GeneralSetting()
            ParameterDocument()
            ControlPanel()
            GnWhsDetails()

        Catch ex As Exception
            oApplication.StatusBar.SetText("Table Creation New Method Failed :" & ex.Message)
        End Try
    End Sub

#Region "QC "

#Region "General Setting"

    Sub GeneralSetting()
        Try
            Me.GeneralSettingHeader()
            Me.GeneralSettingDetail()

            If Not oGFun.UDOExists("QCGT") Then
                Dim findAliasNDescription = New String(,) {{"Code", "Code"}}
                oGFun.RegisterUDO("QCGT", "QC general Setting", SAPbobsCOM.BoUDOObjType.boud_MasterData, findAliasNDescription, "MIPL_OQCGT", "MIPL_QCGT1")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub GeneralSettingHeader()
        Try
            oGFun.CreateTable("MIPL_OQCGT", "QC general Setting Header", SAPbobsCOM.BoUTBTableType.bott_MasterData)

            oGFun.CreateUserFields("@MIPL_OQCGT", "TUnit", "Time Unit", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub GeneralSettingDetail()
        Try
            oGFun.CreateTable("MIPL_QCGT1", "QC general Setting Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)

            oGFun.CreateUserFields("@MIPL_QCGT1", "Active", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            oGFun.CreateUserFields("@MIPL_QCGT1", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_QCGT1", "OPType", "Operation Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_QCGT1", "LocCode", "Loc. Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_QCGT1", "LocName", "Loc. Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCGT1", "AccWhCode", "QC Acc. WhsCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_QCGT1", "AccWhNam", "QC Acc. WhsName", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCGT1", "RejWhCode", "QC Rej. WhsCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_QCGT1", "RejWhNam", "QC Rej. WhsName", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCGT1", "RewWhCode", "QC Rew. WhsCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_QCGT1", "RewWhNam", "QC Rew. WhsName", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCGT1", "ScrWhCode", "QC Scr. WhsCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_QCGT1", "ScrWhNam", "QC Scr. WhsName", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCGT1", "StrWhCode", "General Store WhsCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_QCGT1", "StrWhNam", "General Store WhsName", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCGT1", "QcWhCod", "Quality WhsCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_QCGT1", "QcWhNam", "Quality WhsName", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCGT1", "QcEWhCod", "Quality WhsCode Excisable", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_QCGT1", "QcEWhNam", "Quality WhsName Excisable", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCGT1", "SubWhCod", "Subcontracting WhsCode ", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_QCGT1", "SubWhNam", "Subcontracting WhsName ", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region

    '#Region "   ParameterMaster     "

    '    Sub ParameterMaster()
    '        Try
    '            Me.ParameterMasterHeader()


    '            If Not oGFun.UDOExists("QCPM") Then
    '                Dim findAliasNDescription = New String(,) {{"Code", "Code"}}
    '                oGFun.RegisterUDO("QCPM", "Parameter Master", SAPbobsCOM.BoUDOObjType.boud_MasterData, findAliasNDescription, "MIPL_OQCPM")
    '                findAliasNDescription = Nothing
    '            End If

    '        Catch ex As Exception
    '            oApplication.StatusBar.SetText(ex.Message)
    '        End Try
    '    End Sub

    '    Sub ParameterMasterHeader()
    '        Try
    '            oGFun.CreateTable("MIPL_OQCPM", "Parameter Master", SAPbobsCOM.BoUTBTableType.bott_MasterData)

    '            oGFun.CreateUserFields("@MIPL_OQCPM", "Code", "Parameter Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
    '            oGFun.CreateUserFieldsComboBox("@MIPL_OQCPM", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", Type)
    '            oGFun.CreateUserFields("@MIPL_OQCPM", "VlidFrom", "ValidFrom Date", SAPbobsCOM.BoFieldTypes.db_Date)
    '            oGFun.CreateUserFields("@MIPL_OQCPM", "VlidTo", "ValidTo Date", SAPbobsCOM.BoFieldTypes.db_Date)
    '            oGFun.CreateUserFields("@MIPL_OQCPM", "Specific", "Specification", SAPbobsCOM.BoFieldTypes.db_Memo, 254)
    '        Catch ex As Exception
    '            oApplication.StatusBar.SetText(ex.Message)
    '        End Try
    '    End Sub

    '    'Changed on 28/05/2013 for Parameter Master - Document type

    Sub ParameterDocument()
        Try
            Me.ParameterDocumentHeader()


            If Not oGFun.UDOExists("QPM") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "DocNum"}}
                oGFun.RegisterUDO("QPM", "Parameter Document", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "MIPL_OQPM")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub ParameterDocumentHeader()
        Try
            oGFun.CreateTable("MIPL_OQPM", "Parameter Document", SAPbobsCOM.BoUTBTableType.bott_Document)

            oGFun.CreateUserFields("@MIPL_OQPM", "Code", "Parameter Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OQPM", "Name", "Parameter Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFieldsComboBox("@MIPL_OQPM", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", Type)
            oGFun.CreateUserFields("@MIPL_OQPM", "VlidFrom", "ValidFrom Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OQPM", "VlidTo", "ValidTo Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OQPM", "Specific", "Specification", SAPbobsCOM.BoFieldTypes.db_Memo, 254)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    '#End Region

#Region "       Control Panel     "

    Sub ControlPanel()
        Try
            Me.ControlPanelHeader()
            Me.ControlPanelDetail1()
            Me.ControlPanelDetail2()
            Me.ControlPanelDetail3()

            If Not oGFun.UDOExists("QCCP") Then
                Dim findAliasNDescription = New String(,) {{"Code", "Code"}}
                oGFun.RegisterUDO("QCCP", "Control Panel", SAPbobsCOM.BoUDOObjType.boud_MasterData, findAliasNDescription, "MIPL_OQCCP", "MIPL_QCCP1", "MIPL_QCCP2", "MIPL_QCCP3", , , , , , , , , SAPbobsCOM.BoYesNoEnum.tYES)
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub ControlPanelHeader()
        Try
            oGFun.CreateTable("MIPL_OQCCP", "Control panel Header", SAPbobsCOM.BoUTBTableType.bott_MasterData)

            oGFun.CreateUserFields("@MIPL_OQCCP", "LocCode", "Loc. Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OQCCP", "OprCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OQCCP", "OprName", "Item Description", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_OQCCP", "DocCode", "DocCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OQCCP", "DocDate", "DocDate", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OQCCP", "SLife", "Shelf Life", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_OQCCP", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            oGFun.CreateUserFieldsComboBox("@MIPL_OQCCP", "CApproval", "Customer Approv", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", ValidValueYesORNo)
            oGFun.CreateUserFieldsComboBox("@MIPL_OQCCP", "Enforce", "Enforce Sampling", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", ValidValueYesORNo)
            oGFun.CreateUserFieldsComboBox("@MIPL_OQCCP", "RefDoc", "Reference Document", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", RefDoc)
            oGFun.CreateUserFields("@MIPL_OQCCP", "RefDocNo", "Reference DocNo.", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFieldsComboBox("@MIPL_OQCCP", "TstCtRec", "QC TestReceive", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", QCValidValues, "N")
            oGFun.CreateUserFieldsComboBox("@MIPL_OQCCP", "InsRept", "Inspection Report", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", QCValidValues)
            oGFun.CreateUserFieldsComboBox("@MIPL_OQCCP", "MSIMDS", "QC MSDSIMDS", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", QCValidValues)
            oGFun.CreateUserFieldsComboBox("@MIPL_OQCCP", "IMDS", "QC IMDS", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", QCValidValues)
            oGFun.CreateUserFields("@MIPL_OQCCP", "CustCode", "Customer Code ", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OQCCP", "CustName", "Customer Name ", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    'Folder
    Sub ControlPanelDetail1()
        Try
           
            oGFun.CreateTable("MIPL_QCCP1", "Parameter Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
            oGFun.CreateUserFields("@MIPL_QCCP1", "PCode", "Parameter Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCCP1", "PName", "Description", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFieldsComboBox("@MIPL_QCCP1", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", Type)
            oGFun.CreateUserFields("@MIPL_QCCP1", "Unit", "Unit", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCCP1", "InsCode", "Instrument Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCCP1", "ChkMethd", "ChkMethod", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCCP1", "RefLoad", "Reference Load", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCCP1", "Lowlimit", "CPL Lowlimit", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_QCCP1", "Uplimit", "CPL Uplimit", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_QCCP1", "Correlation", "Correlation", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCCP1", "RevNo", "Revision No.", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            oGFun.CreateUserFields("@MIPL_QCCP1", "RevDate", "Revision Date.", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_QCCP1", "PreCode", "Prepared Code.", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCCP1", "PreName", "Prepared Name.", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_QCCP1", "NoOfObser", "No Of Obser.", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)

            oGFun.CreateUserFieldsComboBox("@MIPL_QCCP1", "Active", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", ValidValueYesORNo, "N")
            Dim sSubtype = New String(,) {{"V", "Value"}, {"C", "Color"}}
            oGFun.CreateUserFieldsComboBox("@MIPL_QCCP1", "SubType", "SubType", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", sSubtype, "V")

            oGFun.CreateUserFields("@MIPL_QCCP1", "Remarks", "Remarks Parameter", SAPbobsCOM.BoFieldTypes.db_Memo)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    'Folder2
    Sub ControlPanelDetail2()
        Try
            oGFun.CreateTable("MIPL_QCCP2", "Sampling", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
            oGFun.CreateUserFields("@MIPL_QCCP2", "LtFrmSiz", "Lot From Size", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCCP2", "LtToSize", "Lot To Size", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCCP2", "SampSize", "Sample Size", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCCP2", "Accpt", "Acceptancy", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_QCCP2", "Remarks", "Remarks Sampling", SAPbobsCOM.BoFieldTypes.db_Memo)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub ControlPanelDetail3()
        Try
            oGFun.CreateTable("MIPL_QCCP3", "Attachments", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
            oGFun.CreateUserFields("@MIPL_QCCP3", "Files", "Files", SAPbobsCOM.BoFieldTypes.db_Memo, 0, SAPbobsCOM.BoFldSubTypes.st_Link)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region

#Region "   Inspection     "

    Sub Inspection()
        Try
            Me.InspectionHeader()
            Me.InspectionDetail()
            Inspection2()
            Me.InspectionDetail3()
            Me.InspectionDetail4()
            Me.InspectionDetail5()
            Me.InspectionDetail6()
            Me.InspectionDetail7()
            Me.InspectionDetail8()
            If Not oGFun.UDOExists("INS") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "DocNum"}}
                oGFun.RegisterUDO("INS", "Inspection", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "MIPL_OINS", "MIPL_INS1", "MIPL_INS2", "MIPL_INS3", "MIPL_INS4", "MIPL_INS5", "MIPL_INS6", "MIPL_INS7", "MIPL_INS8")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub InspectionHeader()
        Try
            oGFun.CreateTable("MIPL_OINS", "Inspection Header", SAPbobsCOM.BoUTBTableType.bott_Document)

            oGFun.CreateUserFields("@MIPL_OINS", "LocCode", "Loc. Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "", NewType, "IN")
            oGFun.CreateUserFields("@MIPL_OINS", "WEntry", "Work Order Entry", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "WOType", "Work Order Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "WONo", "Work Order No", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "SDate", "Start Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OINS", "STime", "Start Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            oGFun.CreateUserFields("@MIPL_OINS", "CByCod", "Checked By", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_OINS", "Cby", "Checked By", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_OINS", "ETime", "End Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            oGFun.CreateUserFields("@MIPL_OINS", "EDate", "End Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OINS", "Date", "Doc Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OINS", "ICode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "IDes", "Item Description", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_OINS", "SNo", "Spindle No.", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)           
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "OpType", "Type of Operation", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "", NewType, "IN")
            Dim Item = New String(,) {{"B", "Batch"}, {"S", "Serial"}}
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "MItemBy", "Manage Item By", SAPbobsCOM.BoFieldTypes.db_Alpha, 6, SAPbobsCOM.BoFldSubTypes.st_None, "", Item, "B")
            oGFun.CreateUserFields("@MIPL_OINS", "Recno", "Receipt No.", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            oGFun.CreateUserFields("@MIPL_OINS", "RecEntry", "Receipt Entry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)

            oGFun.CreateUserFields("@MIPL_OINS", "IBaseNum", "Inventory Base Num", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            oGFun.CreateUserFields("@MIPL_OINS", "IBaseEntry", "Inventory Base Entry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)

            oGFun.CreateUserFields("@MIPL_OINS", "WBaseNum", "Production Order", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "WBaseEntry", "Production Order", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "GRNNo", "GRN No.", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "GRNEntry", "GRN Entry.", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

            oGFun.CreateUserFields("@MIPL_OINS", "PQty", "Pending Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFields("@MIPL_OINS", "CustCode", "Customer Code ", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "CustName", "Customer Name ", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

            oGFun.CreateUserFields("@MIPL_OINS", "ItmsGrpCod", "ItmsGrpCod", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OINS", "ItmsGrpNam", "ItmsGrpNam", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)


            oGFun.CreateUserFields("@MIPL_OINS", "PendQty", "Pending Qty.", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFields("@MIPL_OINS", "InsQty", "Inspection Qty.", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFields("@MIPL_OINS", "SamplQty", "Sampling Qty.", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFields("@MIPL_OINS", "AccQty", "Accepted Qty.", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFields("@MIPL_OINS", "PartAcc", "Partially Accepted Qty.", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFields("@MIPL_OINS", "RejQty", "Rejected Qty.", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFields("@MIPL_OINS", "RewQty", "Rework Qty.", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "IsRepRcv", "Inspection Report Received", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", ValidValueYesORNo)
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "TstCrReq", "Test Certification Required", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", ValidValueYesORNo)
            oGFun.CreateUserFields("@MIPL_OINS", "Chkcode", "Checked by Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OINS", "ChkName", "Checked by Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "Inscode", "Inspected by Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OINS", "InsName", "Inspected by Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "Remarks1", "Remarks1", SAPbobsCOM.BoFieldTypes.db_Memo, 500)
            oGFun.CreateUserFields("@MIPL_OINS", "Remarks2", "Remarks2", SAPbobsCOM.BoFieldTypes.db_Memo, 500)

            oGFun.CreateUserFields("@MIPL_OINS", "CardCode", "GRN CardCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OINS", "CardName", "GRN CardName", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "InvNo", "GRN InvNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OINS", "InvDate", "GRN InvDate", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OINS", "SamRef", "Sample Reference", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "Rmtc", "Ins Rmtc", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", QCValidValues)
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "SaltRept", "InspectSaltReport", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", QCValidValues)
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "PlatRept", "InspectPlatingReport", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", QCValidValues)
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "MSIMDS", "InspectMSDSIMDS", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", QCValidValues)
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "IMDS", "InspectIMDS", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", QCValidValues)
            oGFun.CreateUserFields("@MIPL_OINS", "BatchNo", "GRN BatchNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OINS", "UOM", "GRN UOM", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

            'Non Confirmation Status
            oGFun.CreateUserFields("@MIPL_OINS", "Dept", "Department", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "InspNo", "Inspection No.", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            'oGFun.CreateUserFields("@MIPL_OINS", "ItemCode", "ItemCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            'oGFun.CreateUserFields("@MIPL_OINS", "series", "No", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "DueDate", "Document Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OINS", "NcsCtgry", "Category", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "MIPL_QC_DEFECT")
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "NcsPrity", "Priority", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Priority)
            oGFun.CreateUserFields("@MIPL_OINS", "InsObser", "Inspection Observation", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OINS", "Remarks3", "Remarks3", SAPbobsCOM.BoFieldTypes.db_Memo, 500)

            'CP Action
            oGFun.CreateUserFields("@MIPL_OINS", "OrgnCode", "OriginatorCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OINS", "OrgnName", "OriginatorName", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "ActionTo", "Action To", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "ReportNo", "Report No", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OINS", "PreCode", "Prepared By Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OINS", "PreName", "Prepared By Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "CpaCtgry", "Category", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "CpaPrity", "CpaPrity", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Priority)
            oGFun.CreateUserFields("@MIPL_OINS", "SubReq", "Subject of Action Request", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OINS", "ConDet", "Concern Details", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OINS", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OINS", "CntctPsn", "Contract Person", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "RespDept", "Responsible Department", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "AssgnCmp", "Assign To Company", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "ActvDate", "Activating Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OINS", "RsRqDate", "Response Required Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OINS", "Remarks4", "Remarks4", SAPbobsCOM.BoFieldTypes.db_Memo, 500)
            oGFun.CreateUserFields("@MIPL_OINS", "NCRefNo", "NC RefNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            'Result
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Status1)

            Dim QCStatus = New String(,) {{"A", "Accepted"}, {"R", "Rejected"}, {"N", "None"}}
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "QCStatus", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", QCStatus, "N")
            oGFun.CreateUserFieldsComboBox("@MIPL_OINS", "Deviation", "Deviation", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", ValidValueYesORNo, "N")
            oGFun.CreateUserFields("@MIPL_OINS", "RsMgCode", "Responsible Manager Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OINS", "RsMgName", "Responsible Manager Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OINS", "RespDate", "Response Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OINS", "ClseDate", "Closing Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OINS", "RATDate", "Corrective action take date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OINS", "RootCaus", "Root Cause", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OINS", "CorAct", "Interim corrective action", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OINS", "CrctPrvt", "Corrective Action", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OINS", "ActRevew", "Action Review", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OINS", "Remarks5", "Remarks5", SAPbobsCOM.BoFieldTypes.db_Memo, 500)
            oGFun.CreateUserFields("@MIPL_OINS", "Remarks6", "Remarks6", SAPbobsCOM.BoFieldTypes.db_Memo, 500)
            oGFun.CreateUserFields("@MIPL_OINS", "Remarks7", "Remarks7", SAPbobsCOM.BoFieldTypes.db_Memo, 500)

            'Doff
            oGFun.CreateUserFields("@MIPL_OINS", "Remarks8", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo, 500)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub InspectionDetail()
        Try
            oGFun.CreateTable("MIPL_INS1", "RawMat.Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)

            oGFun.CreateUserFields("@MIPL_INS1", "Selected", "Selected", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            oGFun.CreateUserFields("@MIPL_INS1", "PCode", "Parameter Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "PName", "Description", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFieldsComboBox("@MIPL_INS1", "Type", "RM Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", Type)


            oGFun.CreateUserFields("@MIPL_INS1", "Unit", "RM Unit", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "MCCode", "MC Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "MCName", "MC Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "NoOfObser", "No Of Obser.", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            Dim sSubtype = New String(,) {{"V", "Value"}, {"C", "Color"}}
            oGFun.CreateUserFieldsComboBox("@MIPL_INS1", "SubType", "SubType", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", sSubtype, "V")


            oGFun.CreateUserFields("@MIPL_INS1", "ChkMethd", "RM ChkMethod", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_INS1", "Lowlimit", "RM Lowlimit", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "Uplimit", "RM Uplimit", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "Correlation", "Correlation", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)



            oGFun.CreateUserFields("@MIPL_INS1", "Reference", "Reference Load", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

            oGFun.CreateUserFields("@MIPL_INS1", "Specific", "Specification", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

            oGFun.CreateUserFields("@MIPL_INS1", "observ1", "Observation1", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "observ2", "Observation2", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "observ3", "Observation3", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "observ4", "Observation4", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "observ5", "Observation5", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "Batch1", "Batch1", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "Batch2", "Batch2", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "Batch3", "Batch3", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "Batch4", "Batch4", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "Batch5", "Batch5", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "OB6", "Observation6", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "B6", "Batch6", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "OB7", "Observation7", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "B7", "Batch7", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "OB8", "Observation8", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "B8", "Batch8", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "OB9", "Observation9", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "B9", "Batch9", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "OB10", "Observation10", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "B10", "Batch10", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "observ11", "Observation11", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "observ12", "Observation12", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "observ13", "Observation13", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "observ14", "Observation14", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "observ15", "Observation15", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "Batch11", "Batch11", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "Batch12", "Batch12", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "Batch13", "Batch13", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "Batch14", "Batch14", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "Batch15", "Batch15", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "OB16", "Observation16", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "B16", "Batch16", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "OB17", "Observation17", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "B17", "Batch17", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "OB18", "Observation18", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "B18", "Batch18", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "OB19", "Observation19", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "B19", "Batch19", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "OB20", "Observation20", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS1", "B20", "Batch20", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
      

            oGFun.CreateUserFields("@MIPL_INS1", "Identi", "Identification", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS1", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub Inspection2()
        oGFun.CreateTable("MIPL_INS2", "BoughtInspection Details", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)

        oGFun.CreateUserFields("@MIPL_INS2", "PCode", "Parameter Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        oGFun.CreateUserFields("@MIPL_INS2", "PName", "Description", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
        oGFun.CreateUserFieldsComboBox("@MIPL_INS2", "Type", "BghtMaterial Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", Type)
        oGFun.CreateUserFields("@MIPL_INS2", "Unit", "BghtMaterial Unit", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        oGFun.CreateUserFields("@MIPL_INS2", "ChkMethd", "BghtMaterial ChkMethod", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)

        oGFun.CreateUserFields("@MIPL_INS2", "Lowlimit", "BghtMaterial Lowlimit", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        oGFun.CreateUserFields("@MIPL_INS2", "Uplimit", "BghtMaterial Uplimit", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)


        oGFun.CreateUserFields("@MIPL_INS2", "Specific", "Specification", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        oGFun.CreateUserFields("@MIPL_INS2", "Remarks", "Bought Remarks", SAPbobsCOM.BoFieldTypes.db_Memo, 254)
    End Sub

    Sub InspectionDetail3()
        Try
            oGFun.CreateTable("MIPL_INS3", "Inspection Detail1", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            oGFun.CreateUserFields("@MIPL_INS3", "lkpath", "Linked Path", SAPbobsCOM.BoFieldTypes.db_Memo, , SAPbobsCOM.BoFldSubTypes.st_Link)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub

    Sub InspectionDetail4()
        Try
            oGFun.CreateTable("MIPL_INS4", "Inspection Detail2", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            oGFun.CreateUserFields("@MIPL_INS4", "St_txt", "Selection Fields", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFieldsComboBox("@MIPL_INS4", "Values", " Values", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, , ValidValueYesORNo)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
    Sub InspectionDetail5()
        Try
            oGFun.CreateTable("MIPL_INS5", "Inspection Detail3", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            oGFun.CreateUserFields("@MIPL_INS5", "BatNo", "Batch No.", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS5", "UOM", "UOM Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS5", "AQty", "Availabilty Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFields("@MIPL_INS5", "IQty", "Inspected Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFields("@MIPL_INS5", "ACQty", "Accepted Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFields("@MIPL_INS5", "ACwhs", "Accepted whs", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS5", "RQty", "Rejected Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFields("@MIPL_INS5", "rejwhs", "Rejected whs", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS5", "ReworkQty", "Rework Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            oGFun.CreateUserFields("@MIPL_INS5", "Reason", "Reason", SAPbobsCOM.BoFieldTypes.db_Memo)
            oGFun.CreateUserFields("@MIPL_INS5", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)


        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub

    Sub InspectionDetail6()
        Try
            oGFun.CreateTable("MIPL_INS6", "Inspection Detail4", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            oGFun.CreateUserFields("@MIPL_INS6", "Parameter", "Parameter", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS6", "Result", "Result", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

            Dim Waiting = New String(,) {{"W", "Waiting"}, {"A", "Approved"}, {"R", "Rejected"}}
            oGFun.CreateUserFieldsComboBox("@MIPL_INS6", "Approval", "Approval", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, , Waiting, "W")

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub

    Sub InspectionDetail7()
        Try
            oGFun.CreateTable("MIPL_INS7", "Inspection Detail5", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)

            oGFun.CreateUserFields("@MIPL_INS7", "SNo", "Spindle No.", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            oGFun.CreateUserFields("@MIPL_INS7", "MCCode", "Machine Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "MCName", "Machine Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "PCode", "Parameter Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "Des", "Description", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "lLimit", "Lower limit", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            Dim sParameterType = New String(,) {{"1", "1"}, {"24", "24"}}
            oGFun.CreateUserFieldsComboBox("@MIPL_INS7", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", sParameterType)
            oGFun.CreateUserFieldsComboBox("@MIPL_INS7", "PType", "Parameter Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", Type)
            oGFun.CreateUserFields("@MIPL_INS7", "Reference", "Reference Load", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "Correlation", "Correlation", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)


            oGFun.CreateUserFields("@MIPL_INS7", "Selected", "Selected", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            oGFun.CreateUserFields("@MIPL_INS7", "Unit", "Unit", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "ulimit", "Upper Limit", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "UpperLimit", "UpperLimit", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)



            oGFun.CreateUserFields("@MIPL_INS7", "MhdChek", "Method of Checking", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB1", "Observation1", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B1", "Batch1", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB2", "Observation2", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B2", "Batch2", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB3", "Observation3", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B3", "Batch3", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB4", "Observation4", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B4", "Batch4", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB5", "Observation5", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B5", "Batch5", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB6", "Observation6", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B6", "Batch6", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB7", "Observation7", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B7", "Batch7", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB8", "Observation8", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B8", "Batch8", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB9", "Observation9", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B9", "Batch9", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB10", "Observation10", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B10", "Batch10", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "observ11", "Observation11", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "observ12", "Observation12", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "observ13", "Observation13", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "observ14", "Observation14", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "observ15", "Observation15", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

            oGFun.CreateUserFields("@MIPL_INS7", "Batch11", "Batch11", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "Batch12", "Batch12", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "Batch13", "Batch13", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "Batch14", "Batch14", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "Batch15", "Batch15", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB16", "Observation16", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B16", "Batch16", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB17", "Observation17", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B17", "Batch17", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB18", "Observation18", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B18", "Batch18", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB19", "Observation19", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B19", "Batch19", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS7", "OB20", "Observation20", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS7", "B20", "Batch20", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)

            Dim sSubtype = New String(,) {{"V", "Value"}, {"C", "Color"}}
            oGFun.CreateUserFieldsComboBox("@MIPL_INS7", "SubType", "SubType", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", sSubtype, "V")
            oGFun.CreateUserFields("@MIPL_INS7", "NoOfObser", "No Of Obser.", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)

        Catch ex As Exception

            oApplication.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub

    Sub InspectionDetail8()
        Try
            oGFun.CreateTable("MIPL_INS8", "Inspection Detail6", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            oGFun.CreateUserFields("@MIPL_INS8", "MCName", "Machine Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            oGFun.CreateUserFields("@MIPL_INS8", "MCCode", "Machine Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_INS8", "DNO", "Doff No.", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)

            Dim Waiting = New String(,) {{"W", "Waiting"}, {"A", "Approved"}, {"R", "Rejected"}}
            oGFun.CreateUserFieldsComboBox("@MIPL_INS8", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Waiting)
            oGFun.CreateUserFieldsComboBox("@MIPL_INS8", "IField", "Inspection", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Waiting)
            oGFun.CreateUserFields("@MIPL_INS8", "Reason", "Reason", SAPbobsCOM.BoFieldTypes.db_Memo)


        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub

#End Region

#Region "   Corrective/Preventive Action     "

    Sub CorrectivePreventiveAction()
        Try

            oGFun.CreateTable("MIPL_OQCPA", "CorrectivePreventiveAction", SAPbobsCOM.BoUTBTableType.bott_Document)

            'Non Conformation Status
            oGFun.CreateUserFields("@MIPL_OQCPA", "Dept", "Department", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OQCPA", "InspNo", "Inspection No.", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OQCPA", "ItemCode", "ItemCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OQCPA", "series", "No", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OQCPA", "DueDate", "Document Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OQCPA", "NcsCtgry", "Category", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFieldsComboBox("@MIPL_OQCPA", "NcsPrity", "Priority", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Priority, "L")
            oGFun.CreateUserFields("@MIPL_OQCPA", "InsObser", "Inspection Observation", SAPbobsCOM.BoFieldTypes.db_Memo, 250)

            'CP Action
            oGFun.CreateUserFields("@MIPL_OQCPA", "OrgnCode", "OriginatorCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OQCPA", "OrgnName", "OriginatorName", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OQCPA", "ActionTo", "Action To", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OQCPA", "ReportNo", "Report No", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OQCPA", "PreCode", "Prepared By Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OQCPA", "PreName", "Prepared By Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OQCPA", "CpaCtgry", "Category", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFieldsComboBox("@MIPL_OQCPA", "CpaPrity", "Priority", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Priority, "L")
            oGFun.CreateUserFields("@MIPL_OQCPA", "SubReq", "Subject of Action Request", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OQCPA", "ConDet", "Concern Details", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OQCPA", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OQCPA", "CntctPsn", "Contract Person", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OQCPA", "RespDept", "Responsible Department", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OQCPA", "AssgnCmp", "Assign To Company", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OQCPA", "ActvDate", "Activating Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OQCPA", "RsRqDate", "Response Required Date", SAPbobsCOM.BoFieldTypes.db_Date)


            'Result
            oGFun.CreateUserFieldsComboBox("@MIPL_OQCPA", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Status1, "O")
            oGFun.CreateUserFields("@MIPL_OQCPA", "RsMgCode", "Responsible Manager Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            oGFun.CreateUserFields("@MIPL_OQCPA", "RsMgName", "Responsible Manager Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            oGFun.CreateUserFields("@MIPL_OQCPA", "RespDate", "Response Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OQCPA", "ClseDate", "Closing Date", SAPbobsCOM.BoFieldTypes.db_Date)
            oGFun.CreateUserFields("@MIPL_OQCPA", "RootCaus", "Root Cause", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OQCPA", "CrctPrvt", "Corrective Action", SAPbobsCOM.BoFieldTypes.db_Memo, 250)
            oGFun.CreateUserFields("@MIPL_OQCPA", "ActRevew", "Action Review", SAPbobsCOM.BoFieldTypes.db_Memo, 250)


            If Not oGFun.UDOExists("QCPA") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "DocNum"}}
                oGFun.RegisterUDO("QCPA", "CorrectivePreventive Action", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "MIPL_OQCPA")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

#End Region

#End Region

#Region "       Item Group Generation     "

    Sub ItemGroupGeneration()
        Try
            Dim OITB As SAPbobsCOM.ItemGroups = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItemGroups)

            If CInt(oGFun.getSingleValue("select count(*) from OITB where ItmsGrpNam ='Finished Goods' ")) = 0 Then
                OITB.GroupName = "Finished Goods"
                OITB.Add()
            End If
            If CInt(oGFun.getSingleValue("select count(*) from OITB where ItmsGrpNam ='Raw Material' ")) = 0 Then
                OITB.GroupName = "Raw Material"
                OITB.Add()
            End If
            If CInt(oGFun.getSingleValue("select count(*) from OITB where ItmsGrpNam ='Operations' ")) = 0 Then
                OITB.GroupName = "Operations"
                OITB.Add()
            End If
            If CInt(oGFun.getSingleValue("select count(*) from OITB where ItmsGrpNam ='Machinery' ")) = 0 Then
                OITB.GroupName = "Machinery"
                OITB.Add()
            End If
            If CInt(oGFun.getSingleValue("select count(*) from OITB where ItmsGrpNam ='Tools' ")) = 0 Then
                OITB.GroupName = "Tools"
                OITB.Add()
            End If
            If CInt(oGFun.getSingleValue("select count(*) from OITB where ItmsGrpNam ='Scrap' ")) = 0 Then
                OITB.GroupName = "Scrap"
                OITB.Add()
            End If
            If CInt(oGFun.getSingleValue("select count(*) from OITB where ItmsGrpNam ='Consumables' ")) = 0 Then
                OITB.GroupName = "Consumables"
                OITB.Add()
            End If

            FinishedGoods = oGFun.getSingleValue("select ItmsGrpCod from OITB where ItmsGrpNam ='Finished Goods' ")
            RawMaterial = oGFun.getSingleValue("select ItmsGrpCod from OITB where ItmsGrpNam ='Raw Material' ")
            Operations = oGFun.getSingleValue("select ItmsGrpCod from OITB where ItmsGrpNam ='Operations' ")
            Machinery = oGFun.getSingleValue("select ItmsGrpCod from OITB where ItmsGrpNam ='Machinery' ")
            Tools = oGFun.getSingleValue("select ItmsGrpCod from OITB where ItmsGrpNam ='Tools' ")
            Scrap = oGFun.getSingleValue("select ItmsGrpCod from OITB where ItmsGrpNam ='Scrap' ")
            consumables = oGFun.getSingleValue("select ItmsGrpCod from OITB where ItmsGrpNam ='Consumables' ")

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

#End Region

#Region "       Find the whs details of current Location     "

    Sub GnWhsDetails()
        Try

            Dim rs_Whsdtails As SAPbobsCOM.Recordset

            LV_StrLocCode = oGFun.getSingleValue(" Select U_Location from OUDG where Code=(select DfltsGroup from OUSR where USER_CODE ='" & oCompany.UserName & "')")
            LV_StrLocName = oGFun.getSingleValue(" Select OLCT.Location from OUDG,OLCT where OUDG.U_Location = OLCT.Code and OUDG.Code=(select DfltsGroup from OUSR where USER_CODE ='" & oCompany.UserName & "')")
            rs_Whsdtails = oGFun.DoQuery(" Select Top 1 * from [@MIPL_QCGT1] where U_LocCode ='" & LV_StrLocCode & "' And isnull(U_Active,'N') = 'Y'")

            If rs_Whsdtails.RecordCount <> 0 Then

                LV_AccWhsName = rs_Whsdtails.Fields.Item("U_AccWhNam").Value.ToString.Trim
                LV_AccWhsCode = rs_Whsdtails.Fields.Item("U_AccWhCode").Value.ToString.Trim
                LV_RejWhsName = rs_Whsdtails.Fields.Item("U_RejWhNam").Value.ToString.Trim
                LV_RejWhsCode = rs_Whsdtails.Fields.Item("U_RejWhCode").Value.ToString.Trim
                LV_RewWhsName = rs_Whsdtails.Fields.Item("U_RewWhNam").Value.ToString.Trim
                LV_RewWhsCode = rs_Whsdtails.Fields.Item("U_RewWhCode").Value.ToString.Trim
                LV_ScrWhsName = rs_Whsdtails.Fields.Item("U_ScrWhNam").Value.ToString.Trim
                LV_ScrWhsCode = rs_Whsdtails.Fields.Item("U_ScrWhCode").Value.ToString.Trim
                LV_StrWhsName = rs_Whsdtails.Fields.Item("U_StrWhNam").Value.ToString.Trim
                LV_StrWhsCode = rs_Whsdtails.Fields.Item("U_StrWhCode").Value.ToString.Trim
                LV_SubWhsCode = rs_Whsdtails.Fields.Item("U_SubWhCod").Value.ToString.Trim
                LV_SubWhsName = rs_Whsdtails.Fields.Item("U_SubWhNam").Value.ToString.Trim

            End If


        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

#End Region

End Class
