﻿Imports SDKLib881
''' <summary>
''' SAP has set of different events For access the controls.
''' In this module particularly using to control events.
''' 1) Menu Event using for while the User choose the menus to select the patricular form 
''' 2) Item Event using for to pass the event Function while user doing process
''' 3) Form Data Event Using to Insert,Update,Delete data on Date Base 
''' 4) Status Bar Event will be call when display message to user, message may be will come 
'''    Warring or Error
''' </summary>
''' <remarks></remarks>
''' 

Module EventHandler

    Public WithEvents oApplication As SAPbouiCOM.Application
    Public oForm As SAPbouiCOM.Form

#Region " ... 1) Menu Event ..."

    Private Sub oApplication_MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) Handles oApplication.MenuEvent
        Try
            'Changed by sankaralakshmi
            
               
            QC_MenuEvent(pVal, BubbleEvent)
           
            If pVal.MenuUID = "526" Then
                Try
                    ' oApplication.RemoveWindowsMessage(SAPbouiCOM.BoWindowsMessageType.bo_WM_TIMER, True)
                    oCompany.Disconnect()
                    oApplication.StatusBar.SetText(addonName & " AddOn is Disconnected . . .", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                    End
                Catch ex As Exception
                    oApplication.StatusBar.SetText("Exit Menu Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                Finally
                End Try
            End If
        Catch ex As Exception
            oApplication.StatusBar.SetText("Application Menu Event Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Private Sub QC_MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)

        If pVal.BeforeAction Then
            Select Case pVal.MenuUID
                'For QC
                Case InspectionFormID
                    If Not isValidLocation() Then BubbleEvent = False
            End Select
            oForm = oApplication.Forms.ActiveForm
            Select Case oForm.UniqueID
                Case InspectionFormID
                    oInspection.MenuEvent(pVal, BubbleEvent)
            End Select
        End If

        If pVal.BeforeAction = False Then

            Select Case pVal.MenuUID

                'Qc AddOn FormId's
                Case GeneralSettingFormID
                    oGeneralSetting.LoadForm()
                    'Case ItemMastermenuid
                    '   oItemMaster.Loadform()
                Case ParameterMasterFormID
                    oParameterMaster.LoadForm()
                Case ControlPanelFormID
                    oControlPanel.LoadForm()
                Case InspectionFormID
                    oInspection.LoadForm()
                    'Case CorrectivePreventiveActionFormID
                    '    oCorrectivePreventiveAction.LoadForm()
                    'Case PurchaseOrderMenuID
                    '    ' Dim aa As String = oApplication.Forms.ActiveForm.GetAsXML
                    '    oPurchaseOrder.LoadForm()
                    'Case GoodsReceiptPOMenuID
                    '    oGoodsReceiptPO.LoadForm()
                Case UserDefaultGroupMenuID
                    oUserDefaultGroup.LoadForm()
                    'Case APInvoiceMenuID
                    '    oAPInvoice.LoadForm()
                    'Case "2561"
                    '    Dim SS = oApplication.Forms.ActiveForm.GetAsXML
            End Select

            oForm = oApplication.Forms.ActiveForm

            Select Case pVal.MenuUID

                'Changed by bowya                                                            {{{{{{{{{{{{{{{{{{}}}}}}}}}}}
                Case "1282", "1281", "1292", "1293", "1287", "519", "1284", "1286", "5890", "1290", "1289", "1288", "1291"


                    Select Case oForm.UniqueID
                        'Qc AddOn
                        Case GeneralSettingFormID
                            oGeneralSetting.MenuEvent(pVal, BubbleEvent)
                        Case ParameterMasterFormID
                            oParameterMaster.MenuEvent(pVal, BubbleEvent)
                        Case ControlPanelFormID
                            oControlPanel.MenuEvent(pVal, BubbleEvent)
                        Case InspectionFormID
                            oInspection.MenuEvent(pVal, BubbleEvent)
                        Case CorrectivePreventiveActionFormID
                            oCorrectivePreventiveAction.MenuEvent(pVal, BubbleEvent)
                    End Select

                    Select Case oForm.TypeEx
                        'Qc 
                        Case PurchaseOrderFormType
                            oPurchaseOrder.MenuEvent(pVal, BubbleEvent)
                        Case GoodsReceiptPOFormType
                            oGoodsReceiptPO.MenuEvent(pVal, BubbleEvent)
                        Case APInvoiceFormType
                            oAPInvoice.MenuEvent(pVal, BubbleEvent)
                    End Select

            End Select
        End If
    End Sub

#End Region

#Region " ... 2) Item Event ..."

    Private Sub oApplication_ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) Handles oApplication.ItemEvent
        Try
            'Dim ss = oApplication.Forms.ActiveForm.GetAsXML
            'If pVal.BeforeAction = False Then
            'End If
         
            QC_ItemEvent(FormUID, pVal, BubbleEvent)
            
        Catch ex As Exception
            oApplication.StatusBar.SetText("Application ItemEvent Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Private Sub QC_ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try

            'Dim ss = oApplication.Forms.ActiveForm.GetAsXML

            Select Case pVal.FormType
                Case GoodsReceiptPOFormType
                    oGoodsReceiptPO.ItemEvent(FormUID, pVal, BubbleEvent)
                Case PurchaseOrderFormType
                    oPurchaseOrder.ItemEvent(FormUID, pVal, BubbleEvent)
                Case APInvoiceFormType
                    oAPInvoice.ItemEvent(FormUID, pVal, BubbleEvent)
            End Select

            Select Case pVal.FormUID

                'Qc AddOn
                Case GeneralSettingFormID
                    oGeneralSetting.ItemEvent(FormUID, pVal, BubbleEvent)
                Case ParameterMasterFormID
                    oParameterMaster.ItemEvent(FormUID, pVal, BubbleEvent)
                Case ControlPanelFormID
                    oControlPanel.ItemEvent(FormUID, pVal, BubbleEvent)
                Case InspectionFormID
                    oInspection.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "OPER"
                    oInspection.Oper_List_SubItemEvent(FormUID, pVal, BubbleEvent)
                    'Case CorrectivePreventiveActionFormID
                    '    oCorrectivePreventiveAction.ItemEvent(FormUID, pVal, BubbleEvent)
            End Select

            'Select pVal.FormTypeEx
            '    Case LV_PmrForm
            '        'Update Combobox value
            '        If pVal.BeforeAction = False And pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE Then
            '            Dim c_Type As SAPbouiCOM.ComboBox = oParameterMaster.frmParameterMaster.Items.Item("c_Type").Specific
            '            oGFun.SetComboBoxValueRefresh(c_Type, "select top 1 code,Name  from [@MIPL_PMRTYPE]")
            '            'c_Type.ValidValues.Add("-1", "Define New")
            '            'c_Type.Select(1, SAPbouiCOM.BoSearchKey.psk_Index)
            '        End If
            'End Select

        Catch ex As Exception
            oApplication.StatusBar.SetText("QC ItemEvent Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub

#End Region

#Region " ... 3) FormDataEvent ..."

    Private Sub oApplication_FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean) Handles oApplication.FormDataEvent
        Try
            QC_FormDataEvent(BusinessObjectInfo, BubbleEvent)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Application FormDataEvent Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub

    Private Sub QC_FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            'Qc AddOn
            Select Case BusinessObjectInfo.FormTypeEx
                Case GoodsReceiptPOFormType
                    oGoodsReceiptPO.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case PurchaseOrderFormType
                    oPurchaseOrder.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case APInvoiceFormType
                    oAPInvoice.FormDataEvent(BusinessObjectInfo, BubbleEvent)
            End Select

            Select Case BusinessObjectInfo.FormUID
                'Qc AddOn
                Case GeneralSettingFormID
                    oGeneralSetting.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case ParameterMasterFormID
                    oParameterMaster.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case ControlPanelFormID
                    oControlPanel.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case InspectionFormID
                    oInspection.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    'Case CorrectivePreventiveActionFormID
                    '    oCorrectivePreventiveAction.FormDataEvent(BusinessObjectInfo, BubbleEvent)
            End Select

        Catch ex As Exception
            oApplication.StatusBar.SetText("QC FormDataEvent Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub
#End Region

#Region " ... 4) Status Bar Event ..."
    Public Sub oApplication_StatusBarEvent(ByVal Text As String, ByVal MessageType As SAPbouiCOM.BoStatusBarMessageType) Handles oApplication.StatusBarEvent
        Try
            If MessageType = SAPbouiCOM.BoStatusBarMessageType.smt_Warning Or MessageType = SAPbouiCOM.BoStatusBarMessageType.smt_Error Then
                System.Media.SystemSounds.Asterisk.Play()
            End If
        Catch ex As Exception
            oApplication.StatusBar.SetText(addonName & " StatusBarEvent Event Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
#End Region

#Region " ... 5) Set Event Filter ..."
    Public Sub SetEventFilter()
        Try
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        Finally
        End Try
    End Sub
#End Region

#Region " ... 6) Right Click Event ..."

    Private Sub oApplication_RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean) Handles oApplication.RightClickEvent
        Try

        Catch ex As Exception
            oApplication.StatusBar.SetText(addonName & " : Right Click Event Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

#End Region

#Region " ... 7) Application Event ..."
    Private Sub oApplication_AppEvent(ByVal EventType As SAPbouiCOM.BoAppEventTypes) Handles oApplication.AppEvent
        Try
            Select Case EventType
                Case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged,
                        SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged,
                        SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition, SAPbouiCOM.BoAppEventTypes.aet_ShutDown

                    'Exit the Application
                    System.Windows.Forms.Application.Exit()

            End Select

        Catch ex As Exception
            oApplication.StatusBar.SetText("Application Event Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub
#End Region

#Region " Valid Location "

    Public Function isValidLocation() As Boolean

        'Changed by Sankaralakshmi

        Try
            'Dim count As Integer
            'count = oGFun.getSingleValue("select count(*) from [@MIPL_QCGT1] where U_LocCode='" & LV_StrLocCode & "' and ISNULL(U_Active,'N')='Y'")
            'If count = 0 Then
            '    oApplication.MessageBox("General Setting not defined for this location : " & LV_StrLocName)
            '    Return False
            'Else
            Return True
            'End If
        Catch ex As Exception
            oApplication.StatusBar.SetText("isValidLocation Function Failed:" & ex.Message)
            Return False
        End Try
    End Function

#End Region

End Module
