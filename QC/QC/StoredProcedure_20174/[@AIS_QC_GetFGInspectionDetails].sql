Alter Procedure [@AIS_QC_GetFGInspectionDetails](@ItemCode Varchar(max),@DocEntry int ) 
As
Begin 
Declare @WorkOrderEntry int 
Select @WorkOrderEntry=DocEntry from [@AS_OWORD] Where DocNum in (Select U_WONo  from [@AS_OIGN] Where DocNum =@DocEntry )

select Isnull(QCCP1.U_PCode,'') as U_PCode ,Isnull(QCCP1.U_PName,'') as U_PName,Isnull(QCCP1.U_Type,'') as U_Type,

Isnull(QCCP1.U_Unit,'') as U_Unit,Isnull(QCCP1.U_Lowlimit,'')as  U_Lowlimit,Isnull(QCCP1.U_Uplimit,'') as U_Uplimit,isnull(QCCP1.U_ChkMethd ,'') as U_ChkMethd,isnull(U_No,'') U_No ,
Isnull(QCCP1.U_SubType ,'') as SubType

from [@MIPL_OQCCP] OQCCP,[@MIPL_QCCP1] QCCP1, [@AS_IGN7] T1  where OQCCP.Code=QCCP1.Code and OQCCP.U_OprCode =@ItemCode   and isnull(QCCP1.U_Active,'N') ='Y' 

And T1.DocEntry =@DocEntry

 



END 