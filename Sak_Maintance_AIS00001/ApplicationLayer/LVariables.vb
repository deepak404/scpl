﻿Imports SAPLib

Module LVariables

#Region " ... General Purpose ..."

    Public v_RetVal, v_ErrCode As Long
    Public v_ErrMsg As String = ""
    Public addonName As String = "MaintenanceCustomization"
    Public oCompany As SAPbobsCOM.Company
    Public ShowFolderBrowserThread As Threading.Thread
    Public BankFileName As String
    Public boolModelForm As Boolean = False
    Public boolModelFormID As String = ""
    Public ExpiryDate As String = "2017-10-01 00:00:00.000"
    Public HWKEY() As String = New String() {"L0287935425", "J0125909377", "Z0464796071", "J0125909377"}

#End Region

#Region " ... Common For Module ..."

     

#End Region

#Region " ... Common For Forms ..."
 

#Region "Maintenance Customization"

    ''   Tenant Slot ...
    'Public SalesOpportunityMenuId As String = "4372"
    'Public SalesOpportunityTypeEx As String = "90000"
    'Public oSalesOpportunity As New ResourceMaster

    Public PMChangeRequestFormID As String = "OPMCR"
    Public PMChangeRequestXML As String = "PMChangeRequest.xml"
    Public oPMChangeRequest As New PMChangeRequest


    Public PMChangeRequestApprovalFormID As String = "OPMCRA"
    Public PMChangeRequestApprovalXML As String = "PMChangeRequestApproval.xml"
    Public oPMChangeRequestApproval As New PMChangeRequestApproval

    'Public SalesOpportunityMenuId As String = "4372"
    'Public SalesOpportunityTypeEx As String = "90000"
    'Public oSalesOpportunity As New ResourceMaster

    Public CAPAReportFormID As String = "CAPA"
    Public CAPAReportXML As String = "CAPAReport.xml"
    Public oCAPAReport As New CAPAReport
    Public ParentSetupFormID As String = "OPRP"
    Public oParentSetup As New ParentSetup

    Public BreakdownRequestFormID As String = "OBDR"
    Public BreakdownRequestXML As String = "BreakdownRequest.xml"
    Public oBreakdownRequest As New BreakdownRequest

    Public PMActivityFormID As String = "OPMA"
    Public PMActivityXML As String = "PMActivity.xml"
    Public oPMActivity As New PMActivity

    Public BreakdownActivityFormID As String = "OBDA"
    Public BreakdownActivityXML As String = "BreakdownActivity.xml"
    Public oBreakdownActivity As New BreakdownActivity


    'Public ParentSetupFormID As String = "AIS_PSP"
    'Public ParentSetupXML As String = "ParentSetup.xml"
    'Public oParentSetup As New ParentSetup


    Public CheckListMasterFormID As String = "OCLM"
    Public CheckListMasterXML As String = "CheckListMaster.xml"
    Public oCheckListMaster As New CheckListMaster
#End Region

#End Region

End Module