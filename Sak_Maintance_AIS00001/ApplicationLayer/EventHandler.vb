﻿Imports SAPLib

Module EventHandler

    Public WithEvents oApplication As SAPbouiCOM.Application
    Public oForm As SAPbouiCOM.Form

#Region " ... 1) Menu Event ..."

    Private Sub oApplication_MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) Handles oApplication.MenuEvent
        Try
            oForm = oApplication.Forms.ActiveForm

            If pVal.BeforeAction = False Then

                Select Case pVal.MenuUID
                    Case PMChangeRequestFormID
                        oPMChangeRequest.PMChangeRequest()
                    Case PMChangeRequestApprovalFormID
                        oPMChangeRequestApproval.PMChangeRequest()
                    Case BreakdownRequestFormID
                        oBreakdownRequest.BreakdownRequest()
                    Case PMActivityFormID
                        oPMActivity.PMActivity()
                    Case BreakdownActivityFormID
                        oBreakdownActivity.BreakdownActivity()
                    Case CAPAReportFormID
                        oCAPAReport.CAPAReport()
                    Case CheckListMasterFormID
                        oCheckListMaster.CheckListMaster()
                    Case ParentSetupFormID
                        If FormExist(ParentSetupFormID) Then
                            oApplication.Forms.Item(ParentSetupFormID).Visible = True
                            oApplication.Forms.Item(ParentSetupFormID).Select()
                        Else
                            oParentSetup.LoadPurchaseRequestPosting()
                        End If

                End Select

                Select Case oForm.TypeEx

                    ' Case ParentsetupFormID
                    '   oParentSetup.SBO_Application_ProgressBarEvent(pVal, BubbleEvent)
                End Select

            Else

                Select Case pVal.MenuUID
                    Case "1282", "1281", "1292", "1293", "1287", "519", "1284", "1286", "5890", "1290", "1289", "1288", "1291"
                        Select Case oForm.UniqueID
                            Case PMChangeRequestFormID
                                oPMChangeRequest.MenuEvent(pVal, BubbleEvent)
                            Case PMChangeRequestApprovalFormID
                                oPMChangeRequestApproval.MenuEvent(pVal, BubbleEvent)

                            Case BreakdownRequestFormID
                                oBreakdownRequest.MenuEvent(pVal, BubbleEvent)
                            Case BreakdownActivityFormID
                                oBreakdownActivity.MenuEvent(pVal, BubbleEvent)
                            Case PMActivityFormID
                                oPMActivity.MenuEvent(pVal, BubbleEvent)
                            Case CAPAReportFormID
                                oCAPAReport.MenuEvent(pVal, BubbleEvent)
                            Case CheckListMasterFormID
                                oCheckListMaster.MenuEvent(pVal, BubbleEvent)
                            Case ParentSetupFormID
                                If FormExist(ParentSetupFormID) Then
                                    oApplication.Forms.Item(ParentSetupFormID).Visible = True
                                    oApplication.Forms.Item(ParentSetupFormID).Select()
                                Else
                                    oParentSetup.LoadPurchaseRequestPosting()
                                End If
                        End Select
                End Select

                Select Case pVal.MenuUID
                    Case "1282", "1281", "1292", "1293", "1287", "519", "1284", "1286", "5890", "1290", "1289", "1288", "1291"
                        Select Case oForm.UniqueID
                            Case PMChangeRequestFormID
                                oPMChangeRequest.MenuEvent(pVal, BubbleEvent)
                            Case PMChangeRequestApprovalFormID
                                oPMChangeRequestApproval.MenuEvent(pVal, BubbleEvent)

                            Case BreakdownRequestFormID
                                oBreakdownRequest.MenuEvent(pVal, BubbleEvent)
                            Case BreakdownActivityFormID
                                oBreakdownActivity.MenuEvent(pVal, BubbleEvent)
                            Case PMActivityFormID
                                oPMActivity.MenuEvent(pVal, BubbleEvent)
                            Case CAPAReportFormID
                                oCAPAReport.MenuEvent(pVal, BubbleEvent)
                            Case CheckListMasterFormID
                                oCheckListMaster.MenuEvent(pVal, BubbleEvent)
                            Case ParentSetupFormID
                                If FormExist(ParentSetupFormID) Then
                                    oApplication.Forms.Item(ParentSetupFormID).Visible = True
                                    oApplication.Forms.Item(ParentSetupFormID).Select()
                                Else
                                    oParentSetup.LoadPurchaseRequestPosting()
                                End If
                        End Select
                End Select

            End If

            If pVal.MenuUID = "526" Then
                oCompany.Disconnect()
                oApplication.StatusBar.SetText(addonName & " AddOn is Disconnected . . .", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                End
            End If
        Catch ex As Exception
            oApplication.StatusBar.SetText("Application Menu Event Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub





#End Region

#Region " ... 2) Item Event ..."

    Private Sub oApplication_ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) Handles oApplication.ItemEvent
        Try
            Select Case pVal.FormUID
                Case BreakdownActivityFormID
                    oBreakdownActivity.ItemEvent(FormUID, pVal, BubbleEvent)
                Case BreakdownRequestFormID
                    oBreakdownRequest.ItemEvent(FormUID, pVal, BubbleEvent)
                Case PMActivityFormID
                    oPMActivity.ItemEvent(FormUID, pVal, BubbleEvent)
                Case PMChangeRequestFormID
                    oPMChangeRequest.ItemEvent(FormUID, pVal, BubbleEvent)
                Case PMChangeRequestApprovalFormID
                    oPMChangeRequestApproval.ItemEvent(FormUID, pVal, BubbleEvent)

                Case CAPAReportFormID
                    oCAPAReport.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "Quotation"
                    oBreakdownActivity.ItemEvent_SubGrid(FormUID, pVal, BubbleEvent)
                Case "PMChangeRequest"
                    oPMChangeRequest.ItemEvent_SubGrid(FormUID, pVal, BubbleEvent)
                Case "OPRP"
                    oParentSetup.ItemEvent_SubForm(FormUID, pVal, BubbleEvent)
                Case CheckListMasterFormID
                    oCheckListMaster.ItemEvent(FormUID, pVal, BubbleEvent)
            End Select

            Select Case pVal.FormTypeEx
             
            End Select

        Catch ex As Exception
            oApplication.StatusBar.SetText("Application ItemEvent Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

#End Region

#Region " ... 3) FormDataEvent ..."

    Private Sub oApplication_FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean) Handles oApplication.FormDataEvent
        Try
            Select Case BusinessObjectInfo.FormTypeEx

            End Select

            Select Case BusinessObjectInfo.FormUID
                Case BreakdownActivityFormID
                    oBreakdownActivity.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case BreakdownRequestFormID
                    oBreakdownRequest.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case PMActivityFormID
                    oPMActivity.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case PMChangeRequestFormID
                    oPMChangeRequest.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                Case PMChangeRequestApprovalFormID
                    oPMChangeRequestApproval.FormDataEvent(BusinessObjectInfo, BubbleEvent)

                Case CAPAReportFormID
                    oCAPAReport.FormDataEvent(BusinessObjectInfo, BubbleEvent)
            End Select

        Catch ex As Exception
            oApplication.StatusBar.SetText("Application FormDataEvent Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub

#End Region

#Region " ... 4) Status Bar Event ..."
    Public Sub oApplication_StatusBarEvent(ByVal Text As String, ByVal MessageType As SAPbouiCOM.BoStatusBarMessageType) Handles oApplication.StatusBarEvent
        Try
            If MessageType = SAPbouiCOM.BoStatusBarMessageType.smt_Warning Or MessageType = SAPbouiCOM.BoStatusBarMessageType.smt_Error Then
                System.Media.SystemSounds.Asterisk.Play()
            End If
        Catch ex As Exception
            oApplication.StatusBar.SetText(addonName & " StatusBarEvent Event Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
#End Region

#Region " ... 5) Set Event Filter ..."
    Public Sub SetEventFilter()
        Try
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        Finally
        End Try
    End Sub
#End Region

#Region " ... 6) Right Click Event ..."

    Private Sub oApplication_RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean) Handles oApplication.RightClickEvent
        Try



        Catch ex As Exception
            oApplication.StatusBar.SetText(addonName & " : Right Click Event Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

#End Region

#Region " ... 7) Application Event ..."
    Private Sub oApplication_AppEvent(ByVal EventType As SAPbouiCOM.BoAppEventTypes) Handles oApplication.AppEvent
        Try
            Select Case EventType
                Case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged, SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged, SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition, SAPbouiCOM.BoAppEventTypes.aet_ShutDown
                    System.Windows.Forms.Application.Exit()
            End Select

        Catch ex As Exception
            oApplication.StatusBar.SetText("Application Event Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub
#End Region



End Module
