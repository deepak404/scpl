﻿Imports SAPLib
Public Class TableCreation
    Dim sTableName As String = String.Empty
    Dim ValidValueYesORNo = New String(,) {{"N", "No"}, {"Y", "Yes"}}
    Dim Frequency = New String(,) {{"D", "Daily"}, {"M", "Monthly"}, {"W", "Weekly"}, {"Y", "Yearly"}}
    Sub New()
        PMChangeRequest()
        BreakdownRequest()
        PMActivity()
        BreakdownActivity()
        ProblemMasterDetails()
        ResourceMasterTableCreation()
        ParentSetup()
        CAPAReport()
        CheckListMaster()

        Dim Department_Type = New String(,) {{"PM", "PM Activity"}, {"N", "None"}}
        CreateUserFieldsComboBox("OUDP", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, SAPbobsCOM.BoFldSubTypes.st_None, "", Department_Type, "N")

        Dim cmb_RawMaterial = New String(,) {{"S", "Spare"}, {"R", "Raw Materail"}, {"F", "FG"}, {"N", "None"}, {"SG", "SFG"}}
        CreateUserFieldsComboBox("OITB", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 2, , , cmb_RawMaterial, "N")

        CreateUserFields("OWTQ", "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields("OWTQ", "BaseEntry", "BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields("OWTQ", "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        CreateUserFields("OWTQ", "BaseObject", "BaseObject", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
    End Sub


    Sub PMActivity()
        Try
            Me.PMActivityHeader()
            Me.PMActivityDetail1()
            Me.PMActivityDetail2()
            Me.PMActivityDetail3()
            Me.PMActivityDetail4()
            Me.PMActivityDetail5()
            Me.PMActivityDetail6()
            If Not UDOExists("OPMA") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("OPMA", "PM Activity", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OPMA", "AIS_PMA1", "AIS_PMA2", "AIS_PMA3", "AIS_PMA4", "AIS_PMA5", "AIS_PMA6")
                findAliasNDescription = Nothing
            End If
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub PMActivityHeader()
        Try


            CreateTable("AIS_OPMA", "PM Activity", SAPbobsCOM.BoUTBTableType.bott_Document)

            CreateUserFields("@AIS_OPMA", "PM", "PM", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OPMA", "EmpName", "Employee Name or Operator Name ", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OPMA", "MCCode", "MC Code ", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OPMA", "MCName", "MC Name ", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OPMA", "Branch", " Branch", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OPMA", "ToDept", "To Department ", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OPMA", "Shift", "Shift ", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OPMA", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields("@AIS_OPMA", "AEng", "Assign Engineer", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OPMA", "Budget", "Budget", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_OPMA", "DocDate", "DocDate ", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OPMA", "FDate", "From Date ", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OPMA", "TDate", "To Date ", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OPMA", "CapaNo", "CapaNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_OPMA", "CapaEntry", "CapaEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            Dim DeptType = New String(,) {{"M", "Mechanical"}, {"E", "Electrical"}, {"O", "Operator"}, {"N", "None"}}
            CreateUserFieldsComboBox("@AIS_OPMA", "DeptType", "DeptType", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", DeptType, "N")
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub PMActivityDetail1()
        Try
            CreateTable("AIS_PMA1", "PM Check List", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFieldsComboBox("AIS_PMA1", "Frequency", "Frequency", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Frequency, "D")
            CreateUserFields("@AIS_PMA1", "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PMA1", "BaseEntry", "BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PMA1", "Task", "Task", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields("@AIS_PMA1", "StepCode", "StepCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields("@AIS_PMA1", "LUdate", "Last Update Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_PMA1", "ProbCode", "Problem Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PMA1", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields("@AIS_PMA1", "Action", "Action", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PMA1", "SDFrom", "Scheduled Date From", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_PMA1", "SDTo", "Scheduled Date To", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_PMA1", "STFrom", "Scheduled Time From", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_PMA1", "STTo", "Scheduled Time To", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_PMA1", "AStart", " Actual Start", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_PMA1", "AFinish", " Actual Finish", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_PMA1", "Duration", "Duration", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            Dim Request = New String(,) {{"I", "Inventory Transfer Request"}, {"P", "Purchase Request"}, {"N", "None"}}
            CreateUserFieldsComboBox("AIS_PMA1", "Request", "Request", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Request, "N")
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub PMActivityDetail2()
        Try
            CreateTable("AIS_PMA2", "PM Spare Parts", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
          

            CreateUserFields("@AIS_PMA2", "SCode", "Spare Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PMA2", "SName", "Spare Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PMA2", "Qty", "Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_PMA2", "LCDate", "Last Change Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_PMA2", "VBCode", "Verified By Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PMA2", "VBName", "Verified By Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PMA2", "FromWhs", "From Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PMA2", "ToWhs", "To Warehouse", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            Dim Request = New String(,) {{"I", "Inventory Transfer Request"}, {"P", "Purchase Request"}, {"N", "None"}}
            CreateUserFieldsComboBox("AIS_PMA2", "Request", "Request", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Request, "N")
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub PMActivityDetail3()
        Try
            CreateTable("AIS_PMA3", "PM Issue Spares", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
          
            CreateUserFields("@AIS_PMA3", "ICode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PMA3", "IName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PMA3", "Qty", "Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_PMA3", "WhsCode", "WareHouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PMA3", "WhsName", "WareHouse Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub PMActivityDetail4()
        Try
            CreateTable("AIS_PMA4", "PM Reciept Spares", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_PMA4", "ICode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PMA4", "IName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_PMA4", "Qty", "Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_PMA4", "WhsCode", "WareHouse Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PMA4", "WhsName", "WareHouse Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub PMActivityDetail5()
        Try
            CreateTable("AIS_PMA5", "PM MC Information", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_PMA5", "Vendor", "Vendor", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields("@AIS_PMA5", "Mft", "Manufacturing", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PMA5", "InsDate", "InsulationDate", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_PMA5", "TSupport", "Technical Supporter", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PMA5", "Budget", "Budget", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub PMActivityDetail6()
        Try
            CreateTable("AIS_PMA6", "PM Attachment", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_PMA6", "Files", "Files", SAPbobsCOM.BoFieldTypes.db_Memo, , SAPbobsCOM.BoFldSubTypes.st_Link)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#Region "  PM Change Request       "

    Sub PMChangeRequest()
        Try
            Me.PMChangeRequestHeader()
            Me.PMChangeRequestDetail()

            If Not UDOExists("OPMCR") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("OPMCR", "PM Change Request", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OPMCR", "AIS_PMCR1")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub PMChangeRequestHeader()
        Try
            CreateTable("AIS_OPMCR", "PM Change Request Header", SAPbobsCOM.BoUTBTableType.bott_Document)
            CreateUserFields("@AIS_OPMCR", "ToDept", "To Department ", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OPMCR", "ReqBy", "Requested by ", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OPMCR", "MCCode", "M/C Code ", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields("@AIS_OPMCR", "MCName", "M/C Name ", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OPMCR", "ReqName", "Req Name ", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OPMCR", "FromDate", " From Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OPMCR", "ToDate", " To Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OPMCR", "AName", "Approver Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OPMCR", "DocDate ", "Document Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OPMCR", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)


            CreateUserFields("@AIS_OPMCR", "FDept", "From Department", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            Dim Dept = New String(,) {{"M", "Mechanical"}, {"E", "Electrical"}, {"O", "Operator"}, {"N", "None"}}
            CreateUserFieldsComboBox("@AIS_OPMCR", "DepType", "Department Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Dept, "N")
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub PMChangeRequestDetail()
        Try
            CreateTable("AIS_PMCR1", "PM Change Request Detail", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_PMCR1", "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PMCR1", "BaseEntry", "BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PMCR1", "BaseLine", "BaseLine", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_PMCR1", "Task", "Task name", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields("@AIS_PMCR1", "PMDate", "PM Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_PMCR1", "AvailDate", "Available Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_PMCR1", "AvailTime", "Available Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_PMCR1", "Reason", "Reasons", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_PMCR1", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFieldsComboBox("AIS_PMCR1", "Frequency", "Frequency", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Frequency, "D")

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub BreakdownActivity()

        Try
            Me.BreakdownActivityHeader()
            Me.BreakDownActivityDetail1()
            Me.BreakDownActivityDetail2()
            Me.BreakDownActivityDetail3()
            Me.BreakDownActivityDetail4()
            Me.BreakDownActivityDetail5()
            Me.BreakDownActivityDetail6()

            If Not UDOExists("OBDA") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("OBDA", "BreakdownActivity", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OBDA", "AIS_BDA1", "AIS_BDA2", "AIS_BDA3", "AIS_BDA4", "AIS_BDA5", "AIS_BDA6")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub BreakdownActivityHeader()
        Try
            CreateTable("AIS_OBDA", "BreakDown Activity Header", SAPbobsCOM.BoUTBTableType.bott_Document)
            CreateUserFields("@AIS_OBDA", "Dept", "Department", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OBDA", "RTime", "Reported Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_OBDA", "MCCode", "MC Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OBDA", "MCName", "MC Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OBDA", "Branch", "Branch", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OBDA", "Shift", "Shift", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OBDA", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields("@AIS_OBDA", "DocDate", "Doc Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OBDA", "AEng", "Assign Engineer", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OBDA", "ProbCode", "Problem Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OBDA", "IssueDB", "Issue Done By", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OBDA", "Budget", "Budget", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_OBDA", "FrmDept", "From Department", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OBDA", "CapaNo", "CapaNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_OBDA", "CapaEntry", "CapaEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub



    Sub BreakDownActivityDetail1()
        Try
            CreateTable("AIS_BDA1", "BreakDown Issue", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_BDA1", "IssueBD", "Issue Of BreakDown", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA1", "IDDate", "Identify Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_BDA1", "IDTime", "Identify Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_BDA1", "SDate", "Solved Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_BDA1", "STime", "Solved Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_BDA1", "Duration", "Duration", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA1", "BReqNo", "Breakdown Request No.", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA1", "Solution", "Solution", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA1", "Action", " Action", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_BDA1", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields("@AIS_BDA1", "AckBy", "Acknowledged By", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA1", "PLTime", "Production Loss Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_BDA1", "PLPrice", "Production Loss Price", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA1", "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_BDA1", "BaseEntry", "BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_BDA1", "BaseLine", "BaseLine", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub BreakDownActivityDetail2()
        Try
            CreateTable("AIS_BDA2", "BDA Spare Parts", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_BDA2", "SCode", "Spare Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA2", "SName", "Spare Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_BDA2", "Qty", "Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_BDA2", "LCDate", "Last Change Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_BDA2", "VBCode", "Verified By Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_BDA2", "FWhsCode", "From Whs Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA2", "FWhsName", "From Whs Namee", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_BDA2", "TWhsCode", "To Whs Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA2", "TWhsName", "To Whs Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_BDA2", "VBName", "Verified By Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_BDA2", "UOMCode", "UOM Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            Dim Request = New String(,) {{"I", "Inventory Transfer Request"}, {"P", "Purchase Request"}, {"N", "None"}}
            CreateUserFieldsComboBox("AIS_BDA2", "Request", "Request", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Request, "N")
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub BreakDownActivityDetail3()
        Try
            CreateTable("AIS_BDA3", "BDA Issue Spares", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_BDA3", "ICode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA3", "IName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_BDA3", "Qty", "Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_BDA3", "WhsCode", "whs Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA3", "WhsName", "Whs Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)


        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub BreakDownActivityDetail4()
        Try
            CreateTable("AIS_BDA4", "BDA Reciept Spares", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_BDA4", "ICode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA4", "IName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_BDA4", "Qty", "Qty", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_BDA4", "WhsCode", "whs Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_BDA4", "WhsName", "Whs Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub BreakDownActivityDetail5()
        Try
            CreateTable("AIS_BDA5", "BDA MC Information", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_BDA5", "Vendor", "Vendor", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields("@AIS_BDA5", "Mft", "Manufacturing", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields("@AIS_BDA5", "InsDate", "InsulationDate", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_BDA5", "TSupport", "Technical Supporter", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_BDA5", "Budget", "Budget", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub BreakDownActivityDetail6()
        Try
            CreateTable("AIS_BDA6", "BDA Attachment", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_BDA6", "Files", "Files", SAPbobsCOM.BoFieldTypes.db_Memo, , SAPbobsCOM.BoFldSubTypes.st_Link)


        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try

    End Sub
#End Region


#Region "Breakdown Request"

    Sub BreakdownRequest()

        Try
            Me.BreakdownRequestHeader()
            Me.BreakdownRequestDetail()
            Me.BreakdownRequestDetail2()

            If Not UDOExists("OBDR") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("OBDR", "BreakdownRequest", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OBDR", "AIS_BDR1", "AIS_BDR2")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub BreakdownRequestHeader()
        Try
            CreateTable("AIS_OBDR", "Breakdown Request Header", SAPbobsCOM.BoUTBTableType.bott_Document)
            CreateUserFields("@AIS_OBDR", "Dept", "Department", SAPbobsCOM.BoFieldTypes.db_Alpha, 80)
            CreateUserFields("@AIS_OBDR", "ReqBy", "Requested By", SAPbobsCOM.BoFieldTypes.db_Alpha, 80)
            CreateUserFields("@AIS_OBDR", "MCcode", "M/C Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 80)
            CreateUserFields("@AIS_OBDR", "MCName", "M/C Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 80)
            CreateUserFields("@AIS_OBDR", "ToDept", "To Department", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields("@AIS_OBDR", "Branch", "Branch", SAPbobsCOM.BoFieldTypes.db_Alpha, 80)
            CreateUserFields("@AIS_OBDR", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OBDR", "Shift", "Shift", SAPbobsCOM.BoFieldTypes.db_Alpha, 80)
            CreateUserFields("@AIS_OBDR", "DocDate ", "Document Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OBDR", "AEng", "Assign Engineer", SAPbobsCOM.BoFieldTypes.db_Alpha, 80)
            CreateUserFields("@AIS_OBDR", "ProbCode", "Problem Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 80)
            CreateUserFields("@AIS_OBDR", "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Alpha, 80)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub BreakdownRequestDetail()
        Try
            CreateTable("AIS_BDR1", "Breakdown Issue1 Detail", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_BDR1", "BDIssue", "Breakdown Issue", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_BDR1", "IDDate", "Identify Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_BDR1", "IDTime", "Identify Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_BDR1", "RPerson", "Responsible Person", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_BDR1", "RPCode", "Resp. Person Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub BreakdownRequestDetail2()
        Try
            CreateTable("AIS_BDR2", "Breakdown Request Attachment", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_BDR2", "Files", "Files", SAPbobsCOM.BoFieldTypes.db_Memo, , SAPbobsCOM.BoFldSubTypes.st_Link)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region
    Sub ProblemMasterDetails()
        Try
            CreateTable("AIS_PMD", "Problem Master Details", SAPbobsCOM.BoUTBTableType.bott_NoObjectAutoIncrement)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

#Region "ResourceMasterData"
    Sub ResourceMasterTableCreation()
        ResourceMasterData()
        ResourceMasterData1()
        ResourceMasterData2()
        ResourceMasterData3()
        ResourceMasterData4()
        ResourceMasterData5()
    End Sub
    Sub ResourceMasterData()
        Try
            CreateTable("AIS_RSC8", "Sales Items Details", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_RSC8"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "Line ID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "ItemCode", "Item Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "ItemName", "Item Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "CycleTime", "Cycle Time", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)


        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub ResourceMasterData1()
        Try
            CreateTable("AIS_RSC9", "Mech. Check List Details", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_RSC9"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "LineID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFieldsComboBox(sTableName, "Frequency", "Frequency", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Frequency, "D")
            CreateUserFields(sTableName, "FrequencyDay", "Frequency Day", SAPbobsCOM.BoFieldTypes.db_Numeric)
            CreateUserFields(sTableName, "Task", "Task", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "LastUpdateDate", "Last Update Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "DefaultService", "Default Service", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Action", "Action", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Duration", "Duration", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields(sTableName, "PreparedDate", "Prepared Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "SFromTime", "Schedule From Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields(sTableName, "SToTime", "Schedule To Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub ResourceMasterData2()
        Try
            CreateTable("AIS_RSC10", "M/C Spares List Details", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_RSC10"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "LineID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "SerialNo", "Serial No", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields(sTableName, "MainSpareCode", "Main Spare Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields(sTableName, "MainSpareName", "Main Spare Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields(sTableName, "LastChangeDate", "Last Change Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "VerifiedBy", "Verified By", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields(sTableName, "AMC", "AMC", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields(sTableName, "SpareType", "Spare Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
           


        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub ResourceMasterData3()
        Try
            CreateTable("AIS_RSC11", "Elec. Check List Details", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_RSC11"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "LineID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFieldsComboBox(sTableName, "Frequency", "Frequency", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Frequency, "D")
            CreateUserFields(sTableName, "FrequencyDay", "Frequency Day", SAPbobsCOM.BoFieldTypes.db_Numeric)
            CreateUserFields(sTableName, "Task", "Task", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "LastUpdateDate", "Last Update Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "DefaultService", "Default Service", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Action", "Action", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Duration", "Duration", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields(sTableName, "PreparedDate", "Prepared Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "SFromTime", "Schedule From Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields(sTableName, "SToTime", "Schedule To Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub ResourceMasterData4()
        Try
            CreateTable("AIS_RSC12", "Operator Check List Details", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_RSC12"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "LineID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFieldsComboBox(sTableName, "Frequency", "Frequency", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Frequency, "D")
            CreateUserFields(sTableName, "FrequencyDay", "Frequency Day", SAPbobsCOM.BoFieldTypes.db_Numeric)
            CreateUserFields(sTableName, "Task", "Task", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "LastUpdateDate", "Last Update Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "DefaultService", "Default Service", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Remarks", "Remarks", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Action", "Action", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Duration", "Duration", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields(sTableName, "PreparedDate", "Prepared Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "SFromTime", "Schedule From Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields(sTableName, "SToTime", "Schedule To Time", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub ResourceMasterData5()
        Try
            CreateTable("AIS_RSC13", "Resourec M/C Info. Det.", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_RSC13"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "LineID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields(sTableName, "VendorCode", "VendorCode", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields(sTableName, "Manufacturing", "Manufacturing", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "InsulationDate", "Insulation Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields(sTableName, "TechnicalSupp", "Technical Supporter", SAPbobsCOM.BoFieldTypes.db_Memo)
            CreateUserFields(sTableName, "Budget", "Budget", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Sum)
           

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

#End Region



    Sub ParentSetup()
        Try
            CreateTable("AIS_PSP", "Parent Setup", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            sTableName = "@AIS_PSP"
            CreateUserFields(sTableName, "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Series", "Series", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "LineID", "LineID", SAPbobsCOM.BoFieldTypes.db_Alpha, 20)
            CreateUserFields(sTableName, "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields(sTableName, "Name", "Name", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields(sTableName, "PMachine", "PMachine", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields(sTableName, "LOrder", "LOrder", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields(sTableName, "Parent", "Parent", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields(sTableName, "SActive", "Set Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub



#Region "CAPA Report"

    Sub CAPAReport()

        Try
            Me.CAPAReportHeader()
            Me.CAPAReportDetail()
            Me.CAPAReportDetail2()
            Me.CAPAReportDetail3()


            If Not UDOExists("CAPA") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("CAPA", "CAPA Report", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OCAPA", "AIS_CAPA1", "AIS_CAPA2", "AIS_CAPA3")
                findAliasNDescription = Nothing
            End If

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub CAPAReportHeader()
        Try
            CreateTable("AIS_OCAPA", "CAPA Report Header ", SAPbobsCOM.BoUTBTableType.bott_Document)

            CreateUserFields("@AIS_OCAPA", "DOR", "Date of Raised", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OCAPA", "Shift", "Shift", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "NCrefNo", "NC Ref No", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "DocDate", "Doc Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OCAPA", "TRaised", "Time Raised", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_Time)
            CreateUserFields("@AIS_OCAPA", "Product", "Product", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "ItemName", "ItemName", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

            CreateUserFields("@AIS_OCAPA", "LotNo", "Lot or Doff No", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "QHeld", "Quantity Held", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Location", "Location", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Machine", "Machine", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "MacName", "Machine", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

            CreateUserFields("@AIS_OCAPA", "BaseNum", "BaseNum", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)
            CreateUserFields("@AIS_OCAPA", "BaseEntry", "BaseEntry", SAPbobsCOM.BoFieldTypes.db_Alpha, 11)


            CreateUserFields("@AIS_OCAPA", "Duration", "Duration", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Raisedby", "Raised By", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "QHeld", "Quantity Held", SAPbobsCOM.BoFieldTypes.db_Float, 0, SAPbobsCOM.BoFldSubTypes.st_Quantity)
            CreateUserFields("@AIS_OCAPA", "TMI", "Team MemberInvolve", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "SignedBy", " Signed By", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Name1", "Name1", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "NOP", "Nature of Problem", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
            CreateUserFields("@AIS_OCAPA", "TempAction", "Temporary Action", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)

            CreateUserFields("@AIS_OCAPA", "DOC", "Date Of Closure ", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OCAPA", "POwner", "Process Owner", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "ReviewBy", "Reviewed By ", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "ApprovBy", " Approved By ", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "PSR", "Process Specification Revised ", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "WIP", "Work Instruction Revised", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "ControlPlan", "Control Plan Revised ", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "FOccurence", "First Occurence", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "PCR", "Process Checklist  ", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "PreventiveMS", "Preventive Schedule", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "PreventiveMC", "Preventive Checklist", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "CSUpdated", "Critical Sparelist Updated", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "MWIUpdate", "Maintenance  Updated", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "ProcessPD", "Process Parameter", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "ProductPD", "Product Parameter", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "Breakdown", "Breakdown", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "FOccurence", "First Occurence", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "Repeated", " Repeated", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "POccurence", "Previous Filament", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "CAUpdated", "CAction Updated", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_OCAPA", "Name2", "Name2", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "Name3", "Name3", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "Name4", "Name4", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "Name5", "Name5", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "Name6", "Name6", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCAPA", "Why1", "Why1", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why2", "Why2", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why3", "Why3", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why4", "Why4", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why11", "Why11", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why12", "Why12", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why13", "Why13", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why14", "Why14", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why24", "Why24", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why23", "Why23", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why22", "Why22", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why21", "Why21", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why34", "Why34", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why33", "Why33", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why32", "Why32", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Why31", "Why31", SAPbobsCOM.BoFieldTypes.db_Alpha, 50)
            CreateUserFields("@AIS_OCAPA", "Closedt", "Closing Date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_OCAPA", "HDeploy", "Horizontal Deployment", SAPbobsCOM.BoFieldTypes.db_Alpha, 250)
            Dim Status = New String(,) {{"O", "Open"}, {"C", "Close"}}
            CreateUserFieldsComboBox("@AIS_OCAPA", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Status, "O")
            Dim Type = New String(,) {{"I", "Internal"}, {"C", "Customer"}}
            CreateUserFieldsComboBox("@AIS_OCAPA", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "", Type, "I")

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub




    Sub CAPAReportDetail()
        Try
            CreateTable("AIS_CAPA1", "CAPAReport Detail1", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_CAPA1", "RClause", "Relevent Causes", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_CAPA1", "Stand", "Standard/Specify", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_CAPA1", "Actual", "Actual/Observe", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            Dim Status = New String(,) {{"N", "No"}, {"Y", "Yes"}}
            CreateUserFields("@AIS_CAPA1", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub CAPAReportDetail2()
        Try
            CreateTable("AIS_CAPA2", "CAPAReport Detail2", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_CAPA2", "CAction", "Corrective Action", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_CAPA2", "RCIdenty", "Root Cause Identy", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_CAPA2", "TDate", "Target date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_CAPA2", "Resp", "Resp.", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            Dim Status1 = New String(,) {{"N", "No"}, {"Y", "Yes"}}
            CreateUserFields("@AIS_CAPA2", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub CAPAReportDetail3()
        Try
            CreateTable("AIS_CAPA3", "CAPAReport Detail3", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_CAPA3", "PAction", "Preventive Action", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_CAPA3", "RCIdenty", "Root Cause Identy", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_CAPA3", "TDate", "Target date", SAPbobsCOM.BoFieldTypes.db_Date)
            CreateUserFields("@AIS_CAPA3", "Resp", "Resp.", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            Dim Status2 = New String(,) {{"N", "No"}, {"Y", "Yes"}}
            CreateUserFields("@AIS_CAPA3", "Status", "Status", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

#End Region


    Sub CheckListMaster()
        Try
            Me.CheckListMasterHeader()
            Me.CheckListMasterDetail()
            If Not UDOExists("OCLM") Then
                Dim findAliasNDescription = New String(,) {{"DocNum", "Document Number"}}
                RegisterUDO("OCLM", "Check List Master", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "AIS_OCLM", "AIS_CLM1")
                findAliasNDescription = Nothing
            End If
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub CheckListMasterHeader()
        Try


            CreateTable("AIS_OCLM", "Check List Master Header", SAPbobsCOM.BoUTBTableType.bott_Document)
            CreateUserFields("@AIS_OCLM", "Type", "Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            CreateUserFields("@AIS_OCLM", "MCCode", "Machine Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
            ' CreateUserFields("@AIS_OCLM", "Code", "Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 100)
        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub CheckListMasterDetail()
        Try
            CreateTable("AIS_CLM1", "Check List Master Detail", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
            CreateUserFields("@AIS_CLM1", "Task", "Task", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)
            CreateUserFields("@AIS_CLM1", "Active", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 1)
            CreateUserFields("@AIS_CLM1", "StepCode", "Step Code", SAPbobsCOM.BoFieldTypes.db_Alpha, 200)

        Catch ex As Exception
            oApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

End Class

