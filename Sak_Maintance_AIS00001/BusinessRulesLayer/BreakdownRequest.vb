﻿Imports System.Xml
Imports System.Collections.Specialized
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data
Imports SAPLib
Class BreakdownRequest

    Dim frmBreakdownRequest As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail, oDBDSDetail2 As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim oMatrix2 As SAPbouiCOM.Matrix
    Dim boolFormLoaded As Boolean = False

    Dim UDOID As String = "OBDR"
    Sub BreakdownRequest()
        Try
            LoadXML(frmBreakdownRequest, BreakdownRequestFormID, BreakdownRequestXML)
            frmBreakdownRequest = oApplication.Forms.Item(BreakdownRequestFormID)
            oDBDSHeader = frmBreakdownRequest.DataSources.DBDataSources.Item("@AIS_OBDR")
            oDBDSDetail = frmBreakdownRequest.DataSources.DBDataSources.Item("@AIS_BDR1")
            oDBDSDetail2 = frmBreakdownRequest.DataSources.DBDataSources.Item("@AIS_BDR2")
            oMatrix = frmBreakdownRequest.Items.Item("Matrix").Specific
            oMatrix2 = frmBreakdownRequest.Items.Item("Matrix1").Specific

            frmBreakdownRequest.Freeze(True)
            Me.InitForm()
            Me.DefineModeForFields()
            frmBreakdownRequest.PaneLevel = 1
            frmBreakdownRequest.Items.Item("f_Breakd").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

            boolFormLoaded = True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Load  Breakdown request Form Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmBreakdownRequest.Freeze(False)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmBreakdownRequest.Freeze(True)
            LoadComboBoxSeries(frmBreakdownRequest.Items.Item("c_series").Specific, UDOID)
            setComboBoxValue(frmBreakdownRequest.Items.Item("c_Dept").Specific, "select code,name from OUDP")
            setComboBoxValue(frmBreakdownRequest.Items.Item("c_ToDept").Specific, "select code,name from OUDP")
            setComboBoxValue(frmBreakdownRequest.Items.Item("c_ProbCode").Specific, "select Code,Name from [@AIS_PMD]")
            setComboBoxValue(frmBreakdownRequest.Items.Item("t_Branch").Specific, "select  Code,Name from OUBR")
            setComboBoxValue(frmBreakdownRequest.Items.Item("c_Shift").Specific, "EXEC [dbo].[@AIS_Maintenance_GetShiftMaster] ")
            setComboBoxValue(frmBreakdownRequest.Items.Item("c_Status").Specific, "EXEC [dbo].[@AIS_Maintenance_GetStatusMaster] ")
            Dim oCmbo As SAPbouiCOM.ComboBox = frmBreakdownRequest.Items.Item("c_Status").Specific
            oCmbo.Select("O", SAPbouiCOM.BoSearchKey.psk_ByValue)

            LoadDocumentDate(frmBreakdownRequest.Items.Item("t_DocDate").Specific)

            oMatrix.Clear()
            SetNewLine(oMatrix, oDBDSDetail)
            SetNewLine(oMatrix2, oDBDSDetail2)
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmBreakdownRequest.Freeze(False)
        End Try
    End Sub
  
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub


    Sub DefineModeForFields()
        Try
            frmBreakdownRequest.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmBreakdownRequest.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmBreakdownRequest.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub ItemEvent(FormUID As String, pVal As SAPbouiCOM.ItemEvent, BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects

                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID

                                Case "Matrix"
                                    Select Case pVal.ColUID
                                        Case "RPCode"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_RPCode", pVal.Row - 1, Trim(oDataTable.GetValue("empID", 0)))
                                            oDBDSDetail.SetValue("U_RPerson", pVal.Row - 1, Trim(oDataTable.GetValue("lastName", 0)) + Trim(oDataTable.GetValue("firstName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                    End Select
                                Case "c_Dept"
                                    oDBDSHeader.SetValue("U_Dept", 0, Trim(oDataTable.GetValue("dept", 0)))
                                    Dim strDept As String = frmBreakdownRequest.Items.Item("c_Dept").Specific.value.ToString.Trim
                                    Dim sQuery As String = String.Empty
                                    sQuery = "EXEC [dbo].[@AIS_Maintain_BRequest_LoadEmployeeDetails] '" & strDept & "'"
                                    Dim rsetItem As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                    If rsetItem.RecordCount > 0 Then
                                        rsetItem.MoveFirst()
                                    End If
                                    oDBDSHeader.InsertRecord(oDBDSHeader.Size)
                                    oDBDSHeader.SetValue("firstName", 0, Trim(rsetItem.Fields.Item("U_AEng").Value))
                                    rsetItem.MoveNext()
                                    oDBDSHeader.LoadFromDataSource()
                                Case "t_AEng"
                                    oDBDSHeader.SetValue("U_AEng", 0, Trim(oDataTable.GetValue("firstName", 0)))

                                Case "t_ReqBy"
                                    oDBDSHeader.SetValue("U_ReqBy", 0, Trim(oDataTable.GetValue("firstName", 0)) + Trim(oDataTable.GetValue("lastName", 0)))
                                Case "t_MCCode"
                                    oDBDSHeader.SetValue("U_MCCode", 0, Trim(oDataTable.GetValue("ResCode", 0)))
                                    oDBDSHeader.SetValue("U_MCName", 0, Trim(oDataTable.GetValue("ResName", 0)))
                            End Select

                        End If

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    Try
                        Select Case pVal.ItemUID
                            Case "t_AEng"
                                If pVal.BeforeAction = False And boolFormLoaded = True Then
                                    Dim sDepartment As SAPbouiCOM.ComboBox = frmBreakdownRequest.Items.Item("c_Dept").Specific
                                    ChooseFromListFilteration(frmBreakdownRequest, "Asgn_CFL", "empID", "Select empID From  OHEM where dept='" + sDepartment.Selected.Value.ToString() + "'")
                                End If
                            Case "t_ReqBy"
                                If pVal.BeforeAction = False And boolFormLoaded = True Then
                                    Dim sDepartment As SAPbouiCOM.ComboBox = frmBreakdownRequest.Items.Item("c_Dept").Specific
                                    ChooseFromListFilteration(frmBreakdownRequest, "ReqBy_CFL", "empID", "Select empID From  OHEM where dept='" + sDepartment.Selected.Value.ToString() + "'")
                                End If
                            Case "t_MCCode"
                                If pVal.BeforeAction = False And boolFormLoaded = True Then
                                    ChooseFromListFilteration(frmBreakdownRequest, "MC_CFL", "ResCode", "select ResCode From ORSC where ResType='M'")
                                End If
                        End Select

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Lost Focus Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmBreakdownRequest.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmBreakdownRequest.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmBreakdownRequest.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                            
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "matrix1"
                                Select Case pVal.ColUID
                                    Case "Selected"
                                        If pVal.BeforeAction = False Then
                                            Try


                                            Catch ex As Exception
                                            Finally
                                            End Try

                                        End If
                                End Select

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID
                            Case "Matrix"
                                Select Case pVal.ColUID
                                    Case "BDIssue"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix, oDBDSDetail, pVal.Row, pVal.ColUID)
                                        End If
                                End Select
                            Case "Matrix1"
                                Select Case pVal.ColUID
                                    Case "Files"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix2, oDBDSDetail2, pVal.Row, pVal.ColUID)

                                        End If
                                End Select
                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
              

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.ActionSuccess And frmBreakdownRequest.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If
                            Case "f_Breakd"
                                If pVal.BeforeAction = False Then
                                    frmBreakdownRequest.PaneLevel = 1
                                    'frmBreakdownRequest.Items.Item("17").AffectsFormMode = False
                                    'frmBreakdownRequest.Settings.MatrixUID = "Machine"
                                End If
                            Case "f_Attach"
                                If pVal.BeforeAction = False Then
                                    frmBreakdownRequest.PaneLevel = 2
                                    'frmBreakdownRequest.Items.Item("18").AffectsFormMode = False
                                    'frmBreakdownRequest.Settings.MatrixUID = "Total"
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try


                    'Try
                    '    Select Case pVal.ItemUID

                    '        Case "f_Breakd"
                    '            If pVal.ActionSuccess Then
                    '                frmBreakdownRequest.PaneLevel = 1
                    '            End If
                    '        Case "f_Attach"
                    '            If pVal.ActionSuccess Then
                    '                frmBreakdownRequest.PaneLevel = 2
                    '            End If
                    '    End Select
                    'Catch ex As Exception
                    '    oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    '    BubbleEvent = False
                    'Finally
                    'End Try


            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        Dim oCmbo As SAPbouiCOM.ComboBox = frmBreakdownRequest.Items.Item("c_Status").Specific
                        If oCmbo.Value = "C" Then
                            frmBreakdownRequest.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                        Else
                            frmBreakdownRequest.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Dim rsetUpadteIndent As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Try
                        If BusinessObjectInfo.BeforeAction Then
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add/Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

End Class
