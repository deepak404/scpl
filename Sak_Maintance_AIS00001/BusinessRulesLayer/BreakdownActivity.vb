﻿Imports SAPLib

Class BreakdownActivity

    Dim frmBreakdownActivity As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail, oDBDSDetail2, oDBDSDetail3, oDBDSDetail4, oDBDSDetail5, oDBDSDetail6 As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim oMatrix2 As SAPbouiCOM.Matrix
    Dim oMatrix3 As SAPbouiCOM.Matrix
    Dim oMatrix4 As SAPbouiCOM.Matrix
    Dim oMatrix5 As SAPbouiCOM.Matrix
    Dim oMatrix6 As SAPbouiCOM.Matrix
    Dim boolFormLoaded As Boolean = False
    Dim sFormUID As String
    Dim UDOID As String = "OBDA"


    Sub BreakdownActivity()
        Try
            boolFormLoaded = False
            LoadXML(frmBreakdownActivity, BreakdownActivityFormID, BreakdownActivityXML)
            frmBreakdownActivity = oApplication.Forms.Item(BreakdownActivityFormID)
            oDBDSHeader = frmBreakdownActivity.DataSources.DBDataSources.Item("@AIS_OBDA")
            oDBDSDetail = frmBreakdownActivity.DataSources.DBDataSources.Item("@AIS_BDA1")
            oDBDSDetail2 = frmBreakdownActivity.DataSources.DBDataSources.Item("@AIS_BDA2")
            oDBDSDetail3 = frmBreakdownActivity.DataSources.DBDataSources.Item("@AIS_BDA3")
            oDBDSDetail4 = frmBreakdownActivity.DataSources.DBDataSources.Item("@AIS_BDA4")
            oDBDSDetail5 = frmBreakdownActivity.DataSources.DBDataSources.Item("@AIS_BDA5")
            oDBDSDetail6 = frmBreakdownActivity.DataSources.DBDataSources.Item("@AIS_BDA6")
            oMatrix = frmBreakdownActivity.Items.Item("Matrix").Specific
            oMatrix2 = frmBreakdownActivity.Items.Item("Matrix1").Specific
            oMatrix3 = frmBreakdownActivity.Items.Item("Matrix2").Specific
            oMatrix4 = frmBreakdownActivity.Items.Item("Matrix3").Specific
            oMatrix5 = frmBreakdownActivity.Items.Item("Matrix4").Specific
            oMatrix6 = frmBreakdownActivity.Items.Item("Matrix5").Specific
            frmBreakdownActivity.Items.Item("f_break").Click()
            frmBreakdownActivity.Freeze(True)
            Me.InitForm()
            Me.DefineModeForFields()

        Catch ex As Exception
            oApplication.StatusBar.SetText("Load  Breakdown Activity Form Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmBreakdownActivity.Freeze(False)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmBreakdownActivity.Freeze(True)
            LoadComboBoxSeries(frmBreakdownActivity.Items.Item("c_series").Specific, UDOID)
            setComboBoxValue(frmBreakdownActivity.Items.Item("t_dept").Specific, "select code,name from OUDP")
            setComboBoxValue(frmBreakdownActivity.Items.Item("t_FDept").Specific, "select code,name from OUDP")
            setComboBoxValue(frmBreakdownActivity.Items.Item("t_branch").Specific, "select  Code,Name from OUBR")
            setComboBoxValue(frmBreakdownActivity.Items.Item("C_PCode").Specific, "select Code,Name from [@AIS_PMD]")
            setComboBoxValue(frmBreakdownActivity.Items.Item("c_Status").Specific, "EXEC [dbo].[@AIS_Maintenance_GetStatusMaster] ")
            Dim oCmbo As SAPbouiCOM.ComboBox = frmBreakdownActivity.Items.Item("c_Status").Specific
            oCmbo.Select("O", SAPbouiCOM.BoSearchKey.psk_ByValue)
            LoadDocumentDate(frmBreakdownActivity.Items.Item("t_DocDate").Specific)
            setComboBoxValue(frmBreakdownActivity.Items.Item("t_shift").Specific, "EXEC [dbo].[@AIS_Maintenance_GetShiftMaster] ")
            oMatrix.Clear()
            SetNewLine(oMatrix, oDBDSDetail)
            SetNewLine(oMatrix2, oDBDSDetail2)
            SetNewLine(oMatrix3, oDBDSDetail3)
            SetNewLine(oMatrix4, oDBDSDetail4)
            SetNewLine(oMatrix5, oDBDSDetail5)
            SetNewLine(oMatrix6, oDBDSDetail6)
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmBreakdownActivity.Freeze(False)
        End Try
    End Sub




    Sub LoadStockItemDetails()
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the Presales Order Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)


            oMatrix3.Clear()
            oDBDSDetail3.Clear()

            For i As Integer = 1 To oMatrix2.VisualRowCount

                Dim oCmbSerial As SAPbouiCOM.ComboBox = oMatrix2.Columns.Item("SICode").Cells.Item(i).Specific
                Dim strSerialCode As String
                If Not oCmbSerial.Selected Is Nothing Then
                    strSerialCode = oCmbSerial.Selected.Value
                Else
                    strSerialCode = String.Empty
                End If

                Dim MachineCode As String = frmBreakdownActivity.Items.Item("t_mc").Specific.value
                Dim sQuery As String = String.Empty
                Dim icount As Integer = 1
                If strSerialCode <> String.Empty Then
                    sQuery = "Select distinct U_SubSpareCode ,U_SubSpareName   from [@AIS_RSC10] Where U_BaseNum ='" + MachineCode + "' and U_MainSpareCode =   '" + strSerialCode + "'"
                    Dim rsetEmpDets_GetRawMaterial As SAPbobsCOM.Recordset = DoQuery(sQuery)
                    If rsetEmpDets_GetRawMaterial.RecordCount > 0 Then
                        For iReCount As Integer = 1 To rsetEmpDets_GetRawMaterial.RecordCount
                            oDBDSDetail3.InsertRecord(oDBDSDetail3.Size)
                            oDBDSDetail3.Offset = iReCount - 1
                            oDBDSDetail3.SetValue("LineID", oDBDSDetail3.Offset, iReCount)
                            oDBDSDetail3.SetValue("U_ICode", oDBDSDetail3.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("U_SubSpareCode").Value)
                            oDBDSDetail3.SetValue("U_IName", oDBDSDetail3.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("U_SubSpareName").Value)
                            rsetEmpDets_GetRawMaterial.MoveNext()
                        Next
                        
                    End If
                End If



            Next
            oMatrix3.LoadFromDataSource()

            If frmBreakdownActivity.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmBreakdownActivity.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Data Loaded  Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub LoadStockItemDetailsReceipt()
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the Presales Order Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            oMatrix4.Clear()
            oDBDSDetail4.Clear()
            For i As Integer = 1 To oMatrix2.VisualRowCount

                Dim oCmbSerial As SAPbouiCOM.ComboBox = oMatrix2.Columns.Item("SICode").Cells.Item(i).Specific
                Dim strSerialCode As String
                If Not oCmbSerial.Selected Is Nothing Then
                    strSerialCode = oCmbSerial.Selected.Value
                Else
                    strSerialCode = String.Empty
                End If

                Dim MachineCode As String = frmBreakdownActivity.Items.Item("t_mc").Specific.value
                Dim sQuery As String = String.Empty
                If strSerialCode <> String.Empty Then
                    sQuery = "Select distinct U_SubSpareCode ,U_SubSpareName   from [@AIS_RSC10] Where U_BaseNum ='" + MachineCode + "' and U_MainSpareCode =   '" + strSerialCode + "'"
                    Dim rsetEmpDets_GetRawMaterial As SAPbobsCOM.Recordset = DoQuery(sQuery)
                    If rsetEmpDets_GetRawMaterial.RecordCount > 0 Then
                        For iReCount As Integer = 1 To rsetEmpDets_GetRawMaterial.RecordCount
                            oDBDSDetail4.InsertRecord(oDBDSDetail4.Size)
                            oDBDSDetail4.Offset = iReCount - 1
                            oDBDSDetail4.SetValue("LineID", oDBDSDetail4.Offset, iReCount)
                            oDBDSDetail4.SetValue("U_ICode", oDBDSDetail4.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("U_SubSpareCode").Value)
                            oDBDSDetail4.SetValue("U_IName", oDBDSDetail4.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("U_SubSpareName").Value)
                            rsetEmpDets_GetRawMaterial.MoveNext()
                        Next
                      
                    End If
                End If



            Next
            oMatrix4.LoadFromDataSource()

            If frmBreakdownActivity.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmBreakdownActivity.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Data Loaded  Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub



    Sub CreateFormForActivityList()
        Try

            Dim sQuery As String
            Dim CP As SAPbouiCOM.FormCreationParams = oApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)
            Dim oItem As SAPbouiCOM.Item
            Dim oButton As SAPbouiCOM.Button
            ' Dim oStaticText As SAPbouiCOM.StaticText
            'Dim oEditText As SAPbouiCOM.EditText
            Dim oGrid As SAPbouiCOM.Grid
            CP.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Sizable
            CP.UniqueID = "Quotation"
            CP.FormType = "200"

            oForm = oApplication.Forms.AddEx(CP)
            ' Set form width and height 
            oForm.Height = 260
            oForm.Width = 500
            oForm.Title = "Break Down Request Details"


            '' Add a Grid item to the form 
            oItem = oForm.Items.Add("MyGrid", SAPbouiCOM.BoFormItemTypes.it_GRID)
            ' Set the grid dimentions and position 
            oItem.Left = 6
            oItem.Top = 0
            oItem.Width = 500
            oItem.Height = 200
            ' Set the grid data 
            oGrid = oItem.Specific
            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
            oForm.DataSources.DataTables.Add("MyDataTable")

            'sQuery = "execute [@AIS_PPC_GeQuotationDetails]  '" & frmPreSales.Items.Item("t_Customer").Specific.value & "'"

            sQuery = "execute [@AIS_Maintenance_BActivity_GetBreakDownHeader] '" & frmBreakdownActivity.Items.Item("t_mc").Specific.value & "'"
            oForm.DataSources.DataTables.Item(0).ExecuteQuery(sQuery)

            oGrid.DataTable = oForm.DataSources.DataTables.Item("MyDataTable")
            oGrid.AutoResizeColumns()


            oGrid.Columns.Item(1).Editable = False
            oGrid.Columns.Item(2).Editable = False

            oGrid.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox
            ' Add OK Button 
            oItem = oForm.Items.Add("Bt_Copy", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 6
            oItem.Top = 200
            oItem.Width = 65
            oItem.Height = 20
            oButton = oItem.Specific
            oButton.Caption = "Submit"
            ' Add CANCEL Button 
            oItem = oForm.Items.Add("2", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 90
            oItem.Top = 200
            oItem.Width = 65
            oItem.Height = 20
            oButton = oItem.Specific



        Catch ex As Exception

            oApplication.StatusBar.SetText("Create Form For Quotation Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally

            oForm.Visible = True

        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Sub DefineModeForFields()
        Try
            frmBreakdownActivity.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmBreakdownActivity.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmBreakdownActivity.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub LoadProductionPlanOrderDetails(DocEntry As String)
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the Machine Information  Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Dim sQuery As String
            sQuery = " EXEC [dbo].[@AIS_Maintance_GetMachineInformation]'" + DocEntry + "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
            rsetEmpDets.MoveFirst()
            oMatrix5.Clear()
            oDBDSDetail5.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail5.InsertRecord(oDBDSDetail5.Size)
                oDBDSDetail5.Offset = i
                oDBDSDetail5.SetValue("LineID", oDBDSDetail5.Offset, i + 1)
                oDBDSDetail5.SetValue("U_InsDate", oDBDSDetail5.Offset, CDate(rsetEmpDets.Fields.Item("U_InsulationDate").Value).ToString("yyyyMMdd"))
                oDBDSDetail5.SetValue("U_Mft", oDBDSDetail5.Offset, CDate(rsetEmpDets.Fields.Item("U_Manufacturing").Value).ToString("yyyyMMdd"))
                oDBDSDetail5.SetValue("U_Vendor", oDBDSDetail5.Offset, rsetEmpDets.Fields.Item("U_VendorCode").Value)
                oDBDSDetail5.SetValue("U_TSupport", oDBDSDetail5.Offset, rsetEmpDets.Fields.Item("U_TechnicalSupp").Value)
                oDBDSDetail5.SetValue("U_Budget", oDBDSDetail5.Offset, rsetEmpDets.Fields.Item("U_Budget").Value)


                rsetEmpDets.MoveNext()
            Next
            oMatrix5.LoadFromDataSource()

            oApplication.StatusBar.SetText(" Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Machine Information Data Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub ItemEvent(FormUID As String, pVal As SAPbouiCOM.ItemEvent, BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID
                                Case "t_cano"
                                    oDBDSHeader.SetValue("U_CapaNo", 0, Trim(oDataTable.GetValue("DocNum", 0)))
                                    oDBDSHeader.SetValue("U_CapaEntry", 0, Trim(oDataTable.GetValue("DocEntry", 0)))

                                Case "t_AEng"
                                    oDBDSHeader.SetValue("U_AEng", 0, Trim(oDataTable.GetValue("firstName", 0)))
                                Case "t_issue"
                                    oDBDSHeader.SetValue("U_IssueDB", 0, Trim(oDataTable.GetValue("firstName", 0)) + Trim(oDataTable.GetValue("lastName", 0)))
                                Case "t_mc"
                                    oDBDSHeader.SetValue("U_MCCode", 0, Trim(oDataTable.GetValue("ResCode", 0)))
                                    oDBDSHeader.SetValue("U_MCName", 0, Trim(oDataTable.GetValue("ResName", 0)))

                                    LoadProductionPlanOrderDetails(Trim(oDataTable.GetValue("ResCode", 0)))
                                    setComboBoxValue(oMatrix2.Columns.Item("SICode").Cells.Item(1).Specific, "Select distinct U_MainSpareCode ,U_MainSpareName  from [@AIS_RSC10] Where U_BaseNum ='" + Trim(oDataTable.GetValue("ResCode", 0)) + "' ")



                                Case "Matrix"
                                    Select Case pVal.ColUID
                                        Case "Ack"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_AckBy", pVal.Row - 1, Trim(oDataTable.GetValue("lastName", 0)) + Trim(oDataTable.GetValue("firstName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                    End Select
                                Case "Matrix1"
                                    Select Case pVal.ColUID
                                        Case "SICode"
                                            oMatrix2.FlushToDataSource()
                                            oDBDSDetail2.SetValue("U_SCode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail2.SetValue("U_SName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))

                                            Dim sQuery As String = String.Empty
                                            sQuery = "Select InvntryUom  from OITM  Where ItemCode ='" & Trim(oDataTable.GetValue("ItemCode", 0)) & "'"
                                            Dim rsetItem As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            If rsetItem.RecordCount > 0 Then
                                                rsetItem.MoveFirst()
                                            End If
                                            For iRowID As Integer = 0 To rsetItem.RecordCount - 1
                                                oDBDSDetail2.SetValue("U_UOMCode", iRowID, Trim(rsetItem.Fields.Item("InvntryUom").Value))
                                            Next

                                            oMatrix2.LoadFromDataSource()
                                            oMatrix2.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            SetNewLine(oMatrix2, oDBDSDetail2, pVal.Row, pVal.ColUID)
                                        Case "VBC"
                                            oMatrix2.FlushToDataSource()
                                            oDBDSDetail2.SetValue("U_VBCode", pVal.Row - 1, Trim(oDataTable.GetValue("empID", 0)))
                                            oDBDSDetail2.SetValue("U_VBName", pVal.Row - 1, Trim(oDataTable.GetValue("lastName", 0)) + Trim(oDataTable.GetValue("firstName", 0)))
                                            oMatrix2.LoadFromDataSource()
                                            oMatrix2.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()


                                        Case "FWhsCode"
                                            oMatrix2.FlushToDataSource()
                                            oDBDSDetail2.SetValue("U_FWhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail2.SetValue("U_FWhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrix2.LoadFromDataSource()
                                            oMatrix2.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                        Case "TWhsCode"
                                            oMatrix2.FlushToDataSource()
                                            oDBDSDetail2.SetValue("U_TWhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail2.SetValue("U_TWhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrix2.LoadFromDataSource()
                                            oMatrix2.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()



                                    End Select
                                Case "Matrix2"
                                    Select Case pVal.ColUID
                                        Case "ICode"

                                            oMatrix3.FlushToDataSource()
                                            oDBDSDetail3.SetValue("U_ICode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail3.SetValue("U_IName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oMatrix3.LoadFromDataSource()
                                            oMatrix3.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            SetNewLine(oMatrix3, oDBDSDetail3, pVal.Row, pVal.ColUID)
                                        Case "Whsc"

                                            oMatrix3.FlushToDataSource()
                                            oDBDSDetail3.SetValue("U_WhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail3.SetValue("U_WhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrix3.LoadFromDataSource()
                                            oMatrix3.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                    End Select
                                Case "Matrix3"
                                    Select Case pVal.ColUID
                                        Case "ICode"

                                            oMatrix4.FlushToDataSource()
                                            oDBDSDetail4.SetValue("U_ICode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail4.SetValue("U_IName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oMatrix4.LoadFromDataSource()
                                            oMatrix4.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            SetNewLine(oMatrix4, oDBDSDetail4, pVal.Row, pVal.ColUID)
                                        Case "Whsc"

                                            oMatrix4.FlushToDataSource()
                                            oDBDSDetail4.SetValue("U_WhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail4.SetValue("U_WhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrix4.LoadFromDataSource()
                                            oMatrix4.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                    End Select
                                Case "Matrix4"
                                    Select Case pVal.ColUID
                                        Case "TSupport"
                                            oMatrix5.FlushToDataSource()
                                            oDBDSDetail5.SetValue("U_TSupport", pVal.Row - 1, Trim(oDataTable.GetValue("lastName", 0)) + Trim(oDataTable.GetValue("firstName", 0)))
                                            oMatrix5.LoadFromDataSource()
                                            oMatrix5.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                    End Select
                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try


                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "Matrix1"
                                If pVal.BeforeAction = False Then
                                    SetNewLine(oMatrix2, oDBDSDetail2, pVal.Row, pVal.ColUID)
                                End If
                                  
                            Case "c_series"
                                If frmBreakdownActivity.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmBreakdownActivity.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmBreakdownActivity.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "matrix1"
                                Select Case pVal.ColUID
                                    Case "Selected"
                                        If pVal.BeforeAction = False Then
                                            Try


                                            Catch ex As Exception
                                            Finally
                                            End Try

                                        End If
                                End Select

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID
                            'Case "Matrix"
                            '    Select Case pVal.ColUID
                            '        Case "IssueBD"
                            '            If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                            '                SetNewLine(oMatrix, oDBDSDetail, pVal.Row, pVal.ColUID)
                            '            End If
                            '    End Select
                            'Case "Matrix1"
                            '    Select Case pVal.ColUID
                            '        Case "PlC"
                            '            If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                            '                SetNewLine(oMatrix2, oDBDSDetail2, pVal.Row, pVal.ColUID)
                            '            End If
                            '    End Select
                            'Case "Matrix2"
                            '    Select Case pVal.ColUID
                            '        Case "ICode"
                            '            If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                            '                SetNewLine(oMatrix3, oDBDSDetail3, pVal.Row, pVal.ColUID)
                            '            End If
                            '    End Select

                            'Case "Matrix3"
                            '    Select Case pVal.ColUID
                            '        Case "ICode"
                            '            If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                            '                SetNewLine(oMatrix4, oDBDSDetail4, pVal.Row, pVal.ColUID)
                            '            End If
                            '    End Select
                            ' 
                            Case "Matrix4"
                                Select Case pVal.ColUID
                                    Case "Vendor"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix5, oDBDSDetail5, pVal.Row, pVal.ColUID)
                                        End If
                                End Select

                            Case "Matrix5"
                                Select Case pVal.ColUID
                                    Case "Files"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix6, oDBDSDetail6, pVal.Row, pVal.ColUID)
                                        End If
                                End Select
                        End Select

        Catch ex As Exception
            StatusBarWarningMsg("Click Event Failed:")
        Finally
        End Try

                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Select Case pVal.ItemUID
                       
                    End Select


                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "53"
                                LoadStockItemDetails()
                            Case "54"
                                LoadStockItemDetailsReceipt()

                            Case "Bt_Copy"
                                If pVal.BeforeAction = False Then
                                    CreateFormForActivityList()
                                End If
                            Case "b_PR"
                                If pVal.BeforeAction = False Then
                                    If Me.TransactionManagementProductionOrder() = False Then
                                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                        BubbleEvent = False
                                    End If
                                End If
                            Case "b_IT"
                                If pVal.BeforeAction = False Then
                                    If Me.TransactionManagement() = False Then
                                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                        BubbleEvent = False
                                    End If
                                End If


                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.ActionSuccess And frmBreakdownActivity.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If
                            Case "f_break"
                                If pVal.BeforeAction = False Then
                                    frmBreakdownActivity.PaneLevel = 1
                                End If
                            Case "f_parts"
                                If pVal.BeforeAction = False Then
                                    frmBreakdownActivity.PaneLevel = 2
                                End If
                            Case "f_issue"
                                If pVal.BeforeAction = False Then
                                    frmBreakdownActivity.PaneLevel = 3
                              

                                End If
                            Case "f_receipt"
                                If pVal.BeforeAction = False Then
                                    frmBreakdownActivity.PaneLevel = 4
                                   

                                End If
                            Case "f_mcinfo"
                                If pVal.BeforeAction = False Then
                                    frmBreakdownActivity.PaneLevel = 5
                                End If
                            Case "f_attach"
                                If pVal.BeforeAction = False Then
                                    frmBreakdownActivity.PaneLevel = 6
                                End If
                            Case "f_capa"
                                If pVal.BeforeAction = False Then
                                    frmBreakdownActivity.PaneLevel = 7
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function TransactionManagement() As Boolean
        Try
            Dim boolGoToStockPosting As Boolean = False
            TransactionManagement = True
            oApplication.SetStatusBarMessage("Please Wait,System is Checking Validation and Posting Stock", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
            Dim boolJournalTrue As Boolean = False
            If Me.InventroyTransferRequest() = False Then
                Return False
            End If
            TransactionManagement = True
        Catch ex As Exception
            TransactionManagement = False
            StatusBarErrorMsg("Transaction Management Failed : " & ex.Message)
            TransactionManagement = False
        Finally
        End Try
    End Function
    Function TransactionManagementProductionOrder() As Boolean
        Try
            Dim boolGoToStockPosting As Boolean = False
            TransactionManagementProductionOrder = True
            oApplication.SetStatusBarMessage("Please Wait,System is Checking Validation and Posting Stock", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
            Dim boolJournalTrue As Boolean = False
            If Me.PurchaseRequest() = False Then
                Return False
            End If
            TransactionManagementProductionOrder = True
        Catch ex As Exception
            TransactionManagementProductionOrder = False
            StatusBarErrorMsg("Transaction Management Failed : " & ex.Message)
            TransactionManagementProductionOrder = False
        Finally
        End Try
    End Function
    Function InventroyTransferRequest() As Boolean
        Try
            Dim sQuery As String = String.Empty
            sQuery = "Select DocEntry  from OWTR  Where U_BaseNum ='" + Trim(oDBDSHeader.GetValue("DocNum", 0)) + "' AND U_BaseObject ='OBDA' AND U_Series='" + Trim(oDBDSHeader.GetValue("Series", 0)) + "'  "
            Dim rsetItem As SAPbobsCOM.Recordset = DoQuery(sQuery)
            If rsetItem.RecordCount > 0 Then
                oApplication.StatusBar.SetText(" Already Inventory Transfer Request Created for this Document", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return False
            End If


            oMatrix2 = frmBreakdownActivity.Items.Item("Matrix1").Specific
            Dim oSalesOrder As SAPbobsCOM.StockTransfer = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryTransferRequest)
            Dim sBoolean As Boolean = False
            For ik As Integer = 1 To oMatrix2.VisualRowCount
                Dim ITR As String = oMatrix2.Columns.Item("ITR").Cells.Item(ik).Specific.value.ToString().Trim()
                If ITR = "I" Then
                    oSalesOrder.DocDate = Date.Now
                    oSalesOrder.DueDate = Format_StringToDate(frmBreakdownActivity.Items.Item("t_DocDate").Specific.value)
                    oSalesOrder.TaxDate = Format_StringToDate(frmBreakdownActivity.Items.Item("t_DocDate").Specific.value)
                    oSalesOrder.UserFields.Fields.Item("U_BaseNum").Value = Trim(oDBDSHeader.GetValue("DocNum", 0))
                    oSalesOrder.UserFields.Fields.Item("U_BaseObject").Value = "OBDA"
                    oSalesOrder.UserFields.Fields.Item("U_Series").Value = Trim(oDBDSHeader.GetValue("Series", 0))
                    oSalesOrder.UserFields.Fields.Item("U_BaseEntry").Value = Trim(oDBDSHeader.GetValue("DocEntry", 0))
                    Dim sItemCode As String = oMatrix2.Columns.Item("SICode").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim Qty As Double = Convert.ToDouble(oMatrix2.Columns.Item("Qty").Cells.Item(ik).Specific.value.Trim)
                    Dim FWhsCode As String = oMatrix2.Columns.Item("FWhsCode").Cells.Item(ik).Specific.value.Trim
                    Dim TWhsCode As String = oMatrix2.Columns.Item("TWhsCode").Cells.Item(ik).Specific.value.Trim
                    oSalesOrder.Lines.ItemCode = sItemCode
                    oSalesOrder.Lines.Quantity = Trim(Qty)
                    oSalesOrder.Lines.FromWarehouseCode = Trim(FWhsCode)
                    oSalesOrder.Lines.WarehouseCode = Trim(TWhsCode)
                    oSalesOrder.Lines.Add()
                    ' 1. Post the GRN documents...
                    If oSalesOrder.Add() = 0 Then
                        oApplication.StatusBar.SetText("Inventory Transfer Request Posted successfully ,Item Code" + sItemCode.ToString(), SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                    Else
                        oApplication.StatusBar.SetText("Unable To Inventory Transfer Request" & oCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    End If
                End If
            Next

            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText(" Transfer Request Document   Posting Method Faild", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        Finally
        End Try
    End Function

    Function PurchaseRequest() As Boolean
        Try
            Dim sQuery As String = String.Empty
            sQuery = "Select DocEntry  from OPRQ  Where U_BaseNum ='" + Trim(oDBDSHeader.GetValue("DocNum", 0)) + "' AND U_BaseObject ='OBDA' AND U_Series='" + Trim(oDBDSHeader.GetValue("Series", 0)) + "'  "
            Dim rsetItem As SAPbobsCOM.Recordset = DoQuery(sQuery)
            If rsetItem.RecordCount > 0 Then
                oApplication.StatusBar.SetText(" Already Purchase Order Created for this Document", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return False
            End If

            oMatrix2 = frmBreakdownActivity.Items.Item("Matrix1").Specific
            Dim oSalesOrder As SAPbobsCOM.Documents = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseRequest)
            Dim sBoolean As Boolean = False
            For ik As Integer = 1 To oMatrix2.VisualRowCount
                Dim ITR As String = oMatrix2.Columns.Item("ITR").Cells.Item(ik).Specific.value.ToString().Trim()
                Dim sItemCode As String = oMatrix2.Columns.Item("SICode").Cells.Item(ik).Specific.value.ToString().Trim()
                If ITR = "P" And sItemCode <> String.Empty Then
                    oSalesOrder.DocDate = Date.Now
                    oSalesOrder.TaxDate = Format_StringToDate(frmBreakdownActivity.Items.Item("t_DocDate").Specific.value)
                    oSalesOrder.DocDueDate = Format_StringToDate(frmBreakdownActivity.Items.Item("t_DocDate").Specific.value)
                    oSalesOrder.RequriedDate = Format_StringToDate(frmBreakdownActivity.Items.Item("t_DocDate").Specific.value)
                    oSalesOrder.UserFields.Fields.Item("U_BaseNum").Value = Trim(oDBDSHeader.GetValue("DocNum", 0))
                    oSalesOrder.UserFields.Fields.Item("U_BaseObject").Value = "OBDA"
                    oSalesOrder.UserFields.Fields.Item("U_Series").Value = Trim(oDBDSHeader.GetValue("Series", 0))
                    oSalesOrder.UserFields.Fields.Item("U_BaseEntry").Value = Trim(oDBDSHeader.GetValue("DocEntry", 0))
                    'oSalesOrder.Requester = oCompany.UserSignature

                    Dim Qty As Double = Convert.ToDouble(oMatrix2.Columns.Item("Qty").Cells.Item(ik).Specific.value.Trim)
                    Dim FWhsCode As String = oMatrix2.Columns.Item("FWhsCode").Cells.Item(ik).Specific.value.Trim
                    Dim TWhsCode As String = oMatrix2.Columns.Item("TWhsCode").Cells.Item(ik).Specific.value.Trim
                    oSalesOrder.Lines.ItemCode = sItemCode
                    oSalesOrder.Lines.Quantity = Trim(Qty)
                    oSalesOrder.Lines.WarehouseCode = Trim(TWhsCode)
                    oSalesOrder.Lines.Add()
                    ' 1. Post the GRN documents...
                    If oSalesOrder.Add() = 0 Then
                        oApplication.StatusBar.SetText("Purchase Request Posted successfully ,Item Code" + sItemCode.ToString(), SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                    Else
                        oApplication.StatusBar.SetText("Unable To Purchase Transfer Request" & oCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    End If
                End If
            Next



            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText(" Purchase Document   Posting Method Faild", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        Finally
        End Try
    End Function
    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        Dim oCmbo As SAPbouiCOM.ComboBox = frmBreakdownActivity.Items.Item("c_Status").Specific
                        If oCmbo.Value = "C" Then
                            frmBreakdownActivity.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                        Else
                            frmBreakdownActivity.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Dim rsetUpadteIndent As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Try
                        If BusinessObjectInfo.BeforeAction Then


                        End If

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add/Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try


            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()
                
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub ItemEvent_SubGrid(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "Bt_Copy"
                                oMatrix.Clear()
                                oDBDSDetail.Clear()

                                Dim oGrid As SAPbouiCOM.Grid = oForm.Items.Item("MyGrid").Specific

                                For i As Integer = 0 To oGrid.Rows.Count - 1
                                    Dim sDocEntry As String = oGrid.DataTable.Columns.Item("DocEntry").Cells.Item(i).Value.ToString().Trim()
                                    If oGrid.DataTable.Columns.Item("Selected").Cells.Item(i).Value.ToString().Trim.ToUpper() = "Y" Then
                                        Dim sQuery As String = String.Empty
                                        sQuery = "EXEC [dbo].[@AIS_Maintenance_BActivity_GetBreakDownDetails] '" & sDocEntry & "'"
                                        Dim rsetItem As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                        If rsetItem.RecordCount > 0 Then
                                            rsetItem.MoveFirst()
                                        End If
                                        For iRowID As Integer = 0 To rsetItem.RecordCount - 1
                                            oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                                            oDBDSDetail.SetValue("LineId", iRowID, iRowID + 1)
                                            oDBDSDetail.SetValue("U_IssueBD", iRowID, Trim(rsetItem.Fields.Item("U_BDIssue").Value))
                                            oDBDSDetail.SetValue("U_IDDate", iRowID, CDate(rsetItem.Fields.Item("U_IDDate").Value).ToString("yyyyMMdd"))
                                            oDBDSDetail.SetValue("U_IDTime", iRowID, Trim(rsetItem.Fields.Item("U_IDTime").Value))
                                            oDBDSDetail.SetValue("U_BaseNum", iRowID, Trim(rsetItem.Fields.Item("DocNum").Value))
                                            oDBDSDetail.SetValue("U_BaseEntry", iRowID, Trim(rsetItem.Fields.Item("DocEntry").Value))
                                            oDBDSDetail.SetValue("U_BaseLine", iRowID, Trim(rsetItem.Fields.Item("LineId").Value))
                                            rsetItem.MoveNext()
                                        Next
                                        oMatrix.LoadFromDataSource()
                                    End If
                                Next
                                oForm.Close()
                             
                        End Select
                    Catch ex As Exception
                        Msg("Click Event Failed : " & ex.Message)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("SubGrid Item Event Failed" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub
End Class
