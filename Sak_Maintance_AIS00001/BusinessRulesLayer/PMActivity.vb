﻿Imports System.Xml
Imports System.Collections.Specialized
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data
Imports SAPLib
Class PMActivity

    Dim frmPMActivity As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail, oDBDSDetail2, oDBDSDetail3, oDBDSDetail4, oDBDSDetail5, oDBDSDetail6 As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim boolFormLoaded As Boolean = False
    Dim sFormUID As String
    Dim UDOID As String = "OPMA"
    Dim oMatrix2 As SAPbouiCOM.Matrix
    Dim oMatrix3 As SAPbouiCOM.Matrix
    Dim oMatrix4 As SAPbouiCOM.Matrix
    Dim oMatrix5 As SAPbouiCOM.Matrix
    Dim oMatrix6 As SAPbouiCOM.Matrix



    Sub PMActivity()
        Try


            boolFormLoaded = False
            LoadXML(frmPMActivity, PMActivityFormID, PMActivityXML)
            frmPMActivity = oApplication.Forms.Item(PMActivityFormID)
            oDBDSHeader = frmPMActivity.DataSources.DBDataSources.Item("@AIS_OPMA")
            oDBDSDetail = frmPMActivity.DataSources.DBDataSources.Item("@AIS_PMA1")
            oDBDSDetail2 = frmPMActivity.DataSources.DBDataSources.Item("@AIS_PMA2")
            oDBDSDetail3 = frmPMActivity.DataSources.DBDataSources.Item("@AIS_PMA3")
            oDBDSDetail4 = frmPMActivity.DataSources.DBDataSources.Item("@AIS_PMA4")
            oDBDSDetail5 = frmPMActivity.DataSources.DBDataSources.Item("@AIS_PMA5")
            oDBDSDetail6 = frmPMActivity.DataSources.DBDataSources.Item("@AIS_PMA6")
            oMatrix = frmPMActivity.Items.Item("Matrix").Specific
            oMatrix2 = frmPMActivity.Items.Item("Matrix2").Specific
            oMatrix3 = frmPMActivity.Items.Item("Matrix3").Specific
            oMatrix4 = frmPMActivity.Items.Item("Matrix4").Specific
            oMatrix5 = frmPMActivity.Items.Item("Matrix5").Specific
            oMatrix6 = frmPMActivity.Items.Item("Matrix6").Specific
            frmPMActivity.Items.Item("f_check").Click()
            frmPMActivity.Freeze(True)
            Me.InitForm()
            Me.DefineModeForFields()


        Catch ex As Exception
            oApplication.StatusBar.SetText("Load  PM Activity Form Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmPMActivity.Freeze(False)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmPMActivity.Freeze(True)
            LoadComboBoxSeries(frmPMActivity.Items.Item("c_series").Specific, UDOID)
            setComboBoxValue(frmPMActivity.Items.Item("c_PM").Specific, "select Code,Name from OUDP Where U_Type='PM'")
            setComboBoxValue(frmPMActivity.Items.Item("c_todep").Specific, "select Code,Name from OUDP")
            setComboBoxValue(frmPMActivity.Items.Item("c_branch").Specific, "select  Code,Name from OUBR")
            setComboBoxValue(frmPMActivity.Items.Item("c_shift").Specific, "EXEC [dbo].[@AIS_Maintenance_GetShiftMaster] ")
            setComboBoxValue(frmPMActivity.Items.Item("c_Status").Specific, "EXEC [dbo].[@AIS_Maintenance_GetStatusMaster] ")
            Dim oCmbo As SAPbouiCOM.ComboBox = frmPMActivity.Items.Item("c_Status").Specific
            oCmbo.Select("O", SAPbouiCOM.BoSearchKey.psk_ByValue)
            LoadDocumentDate(frmPMActivity.Items.Item("t_DocDate").Specific)
           
            oMatrix.Clear()
            SetNewLine(oMatrix, oDBDSDetail)
            setComboBoxValue(oMatrix.Columns.Item("ProbCode").Cells.Item(1).Specific, "select Code,Name from [@AIS_PMD] ")
            '   LoadDocumentDate(oMatrix.Columns.Item("LUDate").Cells.Item(1).Specific)
            SetNewLine(oMatrix2, oDBDSDetail2)
            SetNewLine(oMatrix3, oDBDSDetail3)
            SetNewLine(oMatrix4, oDBDSDetail4)
            SetNewLine(oMatrix5, oDBDSDetail5)
            SetNewLine(oMatrix6, oDBDSDetail6)
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmPMActivity.Freeze(False)
        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 


    Sub DefineModeForFields()
        Try
            frmPMActivity.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmPMActivity.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmPMActivity.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub LoadProductionPlanOrderDetails(DocEntry As String)
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the Machine Information  Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Dim sQuery As String
            sQuery = " EXEC [dbo].[@AIS_Maintance_GetMachineInformation]'" + DocEntry + "'"
            Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
            rsetEmpDets.MoveFirst()
            oMatrix5.Clear()
            oDBDSDetail5.Clear()
            For i As Integer = 0 To rsetEmpDets.RecordCount - 1
                oDBDSDetail5.InsertRecord(oDBDSDetail5.Size)
                oDBDSDetail5.Offset = i
                oDBDSDetail5.SetValue("LineID", oDBDSDetail5.Offset, i + 1)
                oDBDSDetail5.SetValue("U_InsDate", oDBDSDetail5.Offset, CDate(rsetEmpDets.Fields.Item("U_InsulationDate").Value).ToString("yyyyMMdd"))
                oDBDSDetail5.SetValue("U_Mft", oDBDSDetail5.Offset, CDate(rsetEmpDets.Fields.Item("U_Manufacturing").Value).ToString("yyyyMMdd"))
                oDBDSDetail5.SetValue("U_Vendor", oDBDSDetail5.Offset, rsetEmpDets.Fields.Item("U_VendorCode").Value)
                oDBDSDetail5.SetValue("U_TSupport", oDBDSDetail5.Offset, rsetEmpDets.Fields.Item("U_TechnicalSupp").Value)
                oDBDSDetail5.SetValue("U_Budget", oDBDSDetail5.Offset, rsetEmpDets.Fields.Item("U_Budget").Value)


                rsetEmpDets.MoveNext()
            Next
            oMatrix5.LoadFromDataSource()

            oApplication.StatusBar.SetText(" Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Fill Machine Information Data Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function isValidFrAndToTime(ByVal FrTime As String, ByVal ToTime As String) As Boolean
        Try
            Dim rset As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim strQuery = "Select case when convert(TimeStamp,'" & FrTime & "') <= convert(TimeStamp,'" & ToTime & "') Then 'True' else 'False' End"
            rset.DoQuery(strQuery)
            Return Convert.ToBoolean(rset.Fields.Item(0).Value)
        Catch ex As Exception
            oApplication.StatusBar.SetText(" IS valid From Date and To Date Function Failed : " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        End Try
    End Function
    Sub ItemEvent(FormUID As String, pVal As SAPbouiCOM.ItemEvent, BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID
                                Case "t_cano"
                                    oDBDSHeader.SetValue("U_CapaNo", 0, Trim(oDataTable.GetValue("DocNum", 0)))
                                    oDBDSHeader.SetValue("U_CapaEntry", 0, Trim(oDataTable.GetValue("DocEntry", 0)))
                                Case "t_mccode"
                                    oDBDSHeader.SetValue("U_MCCode", 0, Trim(oDataTable.GetValue("ResCode", 0)))
                                    oDBDSHeader.SetValue("U_MCName", 0, Trim(oDataTable.GetValue("ResName", 0)))

                                    setComboBoxValue(oMatrix2.Columns.Item("Scode").Cells.Item(1).Specific, "Select distinct U_MainSpareCode ,U_MainSpareName  from [@AIS_RSC10] Where U_BaseNum ='" + Trim(oDataTable.GetValue("ResCode", 0)) + "' ")

                                   


                                Case "t_emp"
                                    oDBDSHeader.SetValue("U_EmpName", 0, Trim(oDataTable.GetValue("lastName", 0)) + Trim(oDataTable.GetValue("firstName", 0)))
                                Case "t_AEng"
                                    oDBDSHeader.SetValue("U_AEng", 0, Trim(oDataTable.GetValue("lastName", 0)) + Trim(oDataTable.GetValue("firstName", 0)))
                                Case "Matrix2"
                                    Select Case pVal.ColUID
                                        Case "Scode"
                                            oMatrix2.FlushToDataSource()
                                            oDBDSDetail2.SetValue("U_SCode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail2.SetValue("U_SName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oMatrix2.LoadFromDataSource()
                                            oMatrix2.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            SetNewLine(oMatrix2, oDBDSDetail2, pVal.Row, pVal.ColUID)
                                        Case "VBC"
                                            oMatrix2.FlushToDataSource()
                                            oDBDSDetail2.SetValue("U_VBCode", pVal.Row - 1, Trim(oDataTable.GetValue("empID", 0)))
                                            oDBDSDetail2.SetValue("U_VBName", 0, Trim(oDataTable.GetValue("lastName", 0)) + Trim(oDataTable.GetValue("firstName", 0)))
                                            oMatrix2.LoadFromDataSource()
                                            oMatrix2.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        Case "FromWhs"
                                            oMatrix2.FlushToDataSource()
                                            oDBDSDetail2.SetValue("U_Fromwhs", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oMatrix2.LoadFromDataSource()
                                            oMatrix2.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        Case "ToWhs"
                                            oMatrix2.FlushToDataSource()
                                            oDBDSDetail2.SetValue("U_ToWhs", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oMatrix2.LoadFromDataSource()
                                            oMatrix2.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                    End Select
                                Case "Matrix4"
                                    Select Case pVal.ColUID
                                        Case "Itcode"
                                            oMatrix4.FlushToDataSource()
                                            oDBDSDetail4.SetValue("U_ICode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail4.SetValue("U_IName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oMatrix4.LoadFromDataSource()
                                            oMatrix4.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            SetNewLine(oMatrix4, oDBDSDetail4, pVal.Row, pVal.ColUID)
                                        Case "WhsCode"
                                            oMatrix4.FlushToDataSource()
                                            oDBDSDetail4.SetValue("U_WhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail4.SetValue("U_WhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrix4.LoadFromDataSource()
                                            oMatrix4.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                    End Select
                                Case "Matrix3"
                                    Select Case pVal.ColUID
                                        Case "Icode1"
                                            oMatrix3.FlushToDataSource()
                                            oDBDSDetail3.SetValue("U_ICode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail3.SetValue("U_IName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oMatrix3.LoadFromDataSource()
                                            oMatrix3.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            SetNewLine(oMatrix3, oDBDSDetail3, pVal.Row, pVal.ColUID)
                                        Case "WHCode"
                                            oMatrix3.FlushToDataSource()
                                            oDBDSDetail3.SetValue("U_WhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail3.SetValue("U_WhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrix3.LoadFromDataSource()
                                            oMatrix3.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                    End Select
                                Case "Matrix5"
                                    Select Case pVal.ColUID
                                        Case "Tsup"
                                            oMatrix5.FlushToDataSource()
                                            oDBDSDetail5.SetValue("U_TSupport", pVal.Row - 1, Trim(oDataTable.GetValue("lastName", 0)) + Trim(oDataTable.GetValue("firstName", 0)))
                                            oMatrix5.LoadFromDataSource()
                                            oMatrix5.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                    End Select
                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID

                            Case "Matrix2"

                                If pVal.BeforeAction = False Then
                                    SetNewLine(oMatrix2, oDBDSDetail2, pVal.Row, pVal.ColUID)
                                End If




                            Case "c_series"
                                If frmPMActivity.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmPMActivity.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmPMActivity.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If

                            Case "c_DType"
                                If pVal.BeforeAction = False Then
                                    oMatrix.Clear()
                                    oDBDSDetail.Clear()
                                    Dim strMCCode As String = frmPMActivity.Items.Item("t_mccode").Specific.value.ToString.Trim
                                    Dim strFromdate As String = frmPMActivity.Items.Item("51").Specific.value.ToString.Trim
                                    Dim strTodate As String = frmPMActivity.Items.Item("52").Specific.value.ToString.Trim
                                    Dim oc_DType As SAPbouiCOM.ComboBox = frmPMActivity.Items.Item("c_DType").Specific
                                    Dim strSerialCode As String = oc_DType.Selected.Value
                                    Dim sQuery As String = String.Empty
                                    sQuery = "EXEC [dbo].[@AIS_Main_PMActivity_LoadResourceDetails] '" & strMCCode & "','" + strSerialCode + "','" + strFromdate + "','" + strTodate + "'"
                                    Dim rsetItem As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                    If rsetItem.RecordCount > 0 Then
                                        rsetItem.MoveFirst()
                                    End If
                                    For iRowID As Integer = 0 To rsetItem.RecordCount - 1
                                        oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                                        oDBDSDetail.SetValue("LineId", iRowID, iRowID + 1)
                                        oDBDSDetail.SetValue("U_BaseEntry", iRowID, Trim(rsetItem.Fields.Item("Code").Value))
                                        oDBDSDetail.SetValue("U_BaseNum", iRowID, Trim(rsetItem.Fields.Item("U_Code").Value))
                                        oDBDSDetail.SetValue("U_Frequency", iRowID, Trim(rsetItem.Fields.Item("U_Frequency").Value))
                                        oDBDSDetail.SetValue("U_Task", iRowID, Trim(rsetItem.Fields.Item("U_Task").Value))
                                        oDBDSDetail.SetValue("U_StepCode", iRowID, Trim(rsetItem.Fields.Item("U_StepCode").Value))
                                        oDBDSDetail.SetValue("U_LUDate", iRowID, CDate(rsetItem.Fields.Item("U_LastUpdateDate").Value).ToString("yyyyMMdd"))
                                        oDBDSDetail.SetValue("U_SDFrom", iRowID, CDate(rsetItem.Fields.Item("U_PreparedDate").Value).ToString("yyyyMMdd"))
                                        oDBDSDetail.SetValue("U_STFrom", iRowID, Trim(rsetItem.Fields.Item("U_SFromTime").Value))
                                        oDBDSDetail.SetValue("U_STTo", iRowID, Trim(rsetItem.Fields.Item("U_SToTime").Value))
                                        oDBDSDetail.SetValue("U_SDTo", iRowID, CDate(rsetItem.Fields.Item("U_LastUpdateDate").Value).ToString("yyyyMMdd"))

                                        rsetItem.MoveNext()
                                    Next
                                    oMatrix.LoadFromDataSource()
                                    LoadProductionPlanOrderDetails(strMCCode)

                                End If



                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "matrix1"
                                Select Case pVal.ColUID
                                    Case "Selected"
                                        If pVal.BeforeAction = False Then
                                            Try


                                            Catch ex As Exception
                                            Finally
                                            End Try

                                        End If
                                End Select

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID
                            Case "Matrix"
                                Select Case pVal.ColUID
                                    Case "Freq"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix, oDBDSDetail, pVal.Row, pVal.ColUID)

                                        End If

                                        'Case "Start", "Finish", "Durtn"
                                        '    If pVal.BeforeAction = False And pVal.ItemChanged Then
                                        '        oMatrix.FlushToDataSource()
                                        '        Dim ITime As Double = CDbl(oDBDSDetail.GetValue("U_AStart", pVal.Row - 1))
                                        '        Dim STime As Double = CDbl(oDBDSDetail.GetValue("U_Afinish", pVal.Row - 1))
                                        '        oDBDSDetail.SetValue("U_Duration", pVal.Row - 1, STime - ITime)
                                        '        oMatrix.LoadFromDataSource()
                                        '        oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                        '    End If
                                End Select
                                End Select

                        
                        Select Case pVal.ItemUID
                            Case "Matrix2"
                                Select Case pVal.ColUID
                                    Case "Scode"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix2, oDBDSDetail2, pVal.Row, pVal.ColUID)
                                        End If

                                End Select

                        End Select
                        Select Case pVal.ItemUID
                            Case "Matrix3"
                                Select Case pVal.ColUID
                                    Case "Icode1"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix3, oDBDSDetail3, pVal.Row, pVal.ColUID)
                                        End If

                                End Select

                        End Select
                        Select Case pVal.ItemUID
                            Case "Matrix4"
                                Select Case pVal.ColUID
                                    Case "Itcode"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix4, oDBDSDetail4, pVal.Row, pVal.ColUID)
                                        End If

                                End Select

                        End Select

                        Select Case pVal.ItemUID
                            Case "Matrix5"
                                Select Case pVal.ColUID
                                    Case "Vendor"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix5, oDBDSDetail5, pVal.Row, pVal.ColUID)
                                        End If

                                End Select

                        End Select

                        Select Case pVal.ItemUID
                            Case "Matrix6"
                                Select Case pVal.ColUID
                                    Case "V_0"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix6, oDBDSDetail6, pVal.Row, pVal.ColUID)
                                        End If

                                End Select

                        End Select

                          
                    Catch ex As Exception
                        StatusBarWarningMsg("Validate Event Failed:")
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "53"
                                LoadStockItemDetails()
                            Case "54"
                                LoadStockItemDetailsReceipt()

                            Case "b_PR"
                                If pVal.BeforeAction = False Then
                                    If Me.TransactionManagementProductionOrder() = False Then
                                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                        BubbleEvent = False
                                    End If
                                End If
                            Case "b_IT"
                                If pVal.BeforeAction = False Then
                                    If Me.TransactionManagement() = False Then
                                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                                        BubbleEvent = False
                                    End If
                                End If
                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS

                    Select Case pVal.ItemUID
                        Case "Matrix"

                            If (pVal.ColUID = "Start" Or pVal.ColUID = "Finish") Then
                                oMatrix = frmPMActivity.Items.Item("Matrix").Specific
                                Dim sFromDate As SAPbouiCOM.EditText = oMatrix.Columns.Item("Start").Cells.Item(pVal.Row).Specific
                                Dim sToDate As SAPbouiCOM.EditText = oMatrix.Columns.Item("Finish").Cells.Item(pVal.Row).Specific

                                If sFromDate.Value = String.Empty Or sToDate.Value = String.Empty Then
                                    Return
                                End If

                                If Not isValidFrAndToTime(sFromDate.Value, sToDate.Value) Then
                                    oApplication.StatusBar.SetText("Solved Time Should Not Be Less Than Identify Time", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                End If

                                Dim sShiftTotalHrs As String = String.Empty
                                Dim sQuery As String = "select dbo.[@AIS_GetTimeValue] ('" & sFromDate.Value & "','" & sToDate.Value & "')"


                                Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                rsetQuery.DoQuery(sQuery)

                                If rsetQuery.RecordCount > 0 Then
                                    rsetQuery.MoveFirst()
                                    sShiftTotalHrs = Trim(rsetQuery.Fields.Item(0).Value)
                                Else
                                    sShiftTotalHrs = 0
                                End If

                                'oMatrix.Columns.Item("Duration").Editable = True
                                oMatrix.Columns.Item("Durtn").Cells.Item(pVal.Row).Click(SAPbouiCOM.BoCellClickType.ct_Regular, SAPbouiCOM.BoModifiersEnum.mt_CTRL)
                                Try
                                    oMatrix.Columns.Item("Durtn").Cells.Item(pVal.Row).Specific.value = sShiftTotalHrs.ToString()
                                Catch ex As Exception

                                End Try
                                'oMatrix.Columns.Item("Duration").Editable = False

                            End If

                    End Select
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "Matrix6"

                            Case "1"
                                If pVal.ActionSuccess And frmPMActivity.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If
                            Case "f_check"
                                If pVal.BeforeAction = False Then
                                    frmPMActivity.PaneLevel = 1
                                    Dim fSettings As SAPbouiCOM.FormSettings
                                    fSettings = frmPMActivity.Settings
                                    fSettings.MatrixUID = "Matrix"
                                End If
                            Case "f_parts"
                                If pVal.BeforeAction = False Then
                                    frmPMActivity.PaneLevel = 2
                                    Dim fSettings As SAPbouiCOM.FormSettings
                                    fSettings = frmPMActivity.Settings
                                    fSettings.MatrixUID = "Matrix2"


                                End If
                            Case "f_issue"
                                If pVal.BeforeAction = False Then
                                    frmPMActivity.PaneLevel = 3
                                    Dim fSettings As SAPbouiCOM.FormSettings
                                    fSettings = frmPMActivity.Settings
                                    fSettings.MatrixUID = "Matrix3"
                                    
                                End If
                            Case "f_receipt"
                                If pVal.BeforeAction = False Then
                                    frmPMActivity.PaneLevel = 4
                                    Dim fSettings As SAPbouiCOM.FormSettings
                                    fSettings = frmPMActivity.Settings
                                    fSettings.MatrixUID = "Matrix4"
                                  

                                End If
                            Case "f_mcinfo"
                                If pVal.BeforeAction = False Then
                                    frmPMActivity.PaneLevel = 5
                                    Dim fSettings As SAPbouiCOM.FormSettings
                                    fSettings = frmPMActivity.Settings
                                    fSettings.MatrixUID = "Matrix5"
                                End If
                            Case "f_attach"
                                If pVal.BeforeAction = False Then
                                    frmPMActivity.PaneLevel = 6
                                    Dim fSettings As SAPbouiCOM.FormSettings
                                    fSettings = frmPMActivity.Settings
                                    fSettings.MatrixUID = "Matrix6"
                                End If
                          
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try


            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub LoadStockItemDetails()
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the Presales Order Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            oMatrix3.Clear()
            oDBDSDetail3.Clear()

            For i As Integer = 1 To oMatrix2.VisualRowCount

                Dim oCmbSerial As SAPbouiCOM.ComboBox = oMatrix2.Columns.Item("Scode").Cells.Item(i).Specific
                Dim strSerialCode As String
                If Not oCmbSerial.Selected Is Nothing Then
                    strSerialCode = oCmbSerial.Selected.Value
                Else
                    strSerialCode = String.Empty
                End If

                Dim MachineCode As String = frmPMActivity.Items.Item("t_mccode").Specific.value
                Dim sQuery As String = String.Empty
                Dim icount As Integer = 1
                If strSerialCode <> String.Empty Then
                    sQuery = "Select distinct U_SubSpareCode ,U_SubSpareName   from [@AIS_RSC10] Where U_BaseNum ='" + MachineCode + "' and U_MainSpareCode =   '" + strSerialCode + "'"
                    Dim rsetEmpDets_GetRawMaterial As SAPbobsCOM.Recordset = DoQuery(sQuery)
                    If rsetEmpDets_GetRawMaterial.RecordCount > 0 Then

                        For iReCount As Integer = 1 To rsetEmpDets_GetRawMaterial.RecordCount
                            oDBDSDetail3.InsertRecord(oDBDSDetail3.Size)
                            oDBDSDetail3.Offset = iReCount - 1
                            oDBDSDetail3.SetValue("LineID", oDBDSDetail3.Offset, iReCount)
                            oDBDSDetail3.SetValue("U_ICode", oDBDSDetail3.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("U_SubSpareCode").Value)
                            oDBDSDetail3.SetValue("U_IName", oDBDSDetail3.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("U_SubSpareName").Value)
                            rsetEmpDets_GetRawMaterial.MoveNext()
                        Next

                      
                    End If
                End If



            Next
            oMatrix3.LoadFromDataSource()

            If frmPMActivity.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmPMActivity.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Data Loaded  Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub LoadStockItemDetailsReceipt()
        Try
            oApplication.StatusBar.SetText("Please wait ... System is preparing the Presales Order Details...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            oMatrix4.Clear()
            oDBDSDetail4.Clear()
            For i As Integer = 1 To oMatrix2.VisualRowCount

                Dim oCmbSerial As SAPbouiCOM.ComboBox = oMatrix2.Columns.Item("Scode").Cells.Item(i).Specific
                Dim strSerialCode As String
                If Not oCmbSerial.Selected Is Nothing Then
                    strSerialCode = oCmbSerial.Selected.Value
                Else
                    strSerialCode = String.Empty
                End If

                Dim MachineCode As String = frmPMActivity.Items.Item("t_mccode").Specific.value
                Dim sQuery As String = String.Empty
                If strSerialCode <> String.Empty Then
                    sQuery = "Select distinct U_SubSpareCode ,U_SubSpareName   from [@AIS_RSC10] Where U_BaseNum ='" + MachineCode + "' and U_MainSpareCode =   '" + strSerialCode + "'"
                    Dim rsetEmpDets_GetRawMaterial As SAPbobsCOM.Recordset = DoQuery(sQuery)
                    If rsetEmpDets_GetRawMaterial.RecordCount > 0 Then


                        For iReCount As Integer = 1 To rsetEmpDets_GetRawMaterial.RecordCount
                            oDBDSDetail4.InsertRecord(oDBDSDetail4.Size)
                            oDBDSDetail4.Offset = iReCount - 1
                            oDBDSDetail4.SetValue("LineID", oDBDSDetail4.Offset, iReCount)
                            oDBDSDetail4.SetValue("U_ICode", oDBDSDetail4.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("U_SubSpareCode").Value)
                            oDBDSDetail4.SetValue("U_IName", oDBDSDetail4.Offset, rsetEmpDets_GetRawMaterial.Fields.Item("U_SubSpareName").Value)
                            rsetEmpDets_GetRawMaterial.MoveNext()

                        Next

                       
                    End If
                End If



            Next
            oMatrix4.LoadFromDataSource()

            If frmPMActivity.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then frmPMActivity.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            oApplication.StatusBar.SetText(" Data Loaded Successfully...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Data Loaded  Function Failed :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function TransactionManagement() As Boolean
        Try
            Dim boolGoToStockPosting As Boolean = False
            TransactionManagement = True
            oApplication.SetStatusBarMessage("Please Wait,System is Checking Validation and Posting Stock", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
            Dim boolJournalTrue As Boolean = False
            If Me.InventroyTransferRequest() = False Then
                Return False
            End If
            TransactionManagement = True
        Catch ex As Exception
            TransactionManagement = False
            StatusBarErrorMsg("Transaction Management Failed : " & ex.Message)
            TransactionManagement = False
        Finally
        End Try
    End Function
    Function TransactionManagementProductionOrder() As Boolean
        Try
            Dim boolGoToStockPosting As Boolean = False
            TransactionManagementProductionOrder = True
            oApplication.SetStatusBarMessage("Please Wait,System is Checking Validation and Posting Stock", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
            Dim boolJournalTrue As Boolean = False
            If Me.PurchaseRequest() = False Then
                Return False
            End If
            TransactionManagementProductionOrder = True
        Catch ex As Exception
            TransactionManagementProductionOrder = False
            StatusBarErrorMsg("Transaction Management Failed : " & ex.Message)
            TransactionManagementProductionOrder = False
        Finally
        End Try
    End Function
    Function InventroyTransferRequest() As Boolean
        Try

            oMatrix = frmPMActivity.Items.Item("Matrix2").Specific
            Dim oSalesOrder As SAPbobsCOM.StockTransfer = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryTransferRequest)
            Dim sBoolean As Boolean = False
            For ik As Integer = 1 To oMatrix.VisualRowCount
                Dim ITR As String = oMatrix.Columns.Item("ITR").Cells.Item(ik).Specific.value.ToString().Trim()
                If ITR = "I" Then
                    oSalesOrder.DocDate = Date.Now
                    oSalesOrder.DueDate = Format_StringToDate(frmPMActivity.Items.Item("t_DocDate").Specific.value)
                    oSalesOrder.TaxDate = Format_StringToDate(frmPMActivity.Items.Item("t_DocDate").Specific.value)
                    oSalesOrder.UserFields.Fields.Item("U_BaseNum").Value = oDBDSHeader.GetValue("DocNum", 0)
                    oSalesOrder.UserFields.Fields.Item("U_BaseObject").Value = "OBDA"
                    oSalesOrder.UserFields.Fields.Item("U_Series").Value = oDBDSHeader.GetValue("Series", 0)
                    oSalesOrder.UserFields.Fields.Item("U_BaseEntry").Value = oDBDSHeader.GetValue("DocEntry", 0)
                    Dim sItemCode As String = oMatrix.Columns.Item("Scode").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim Qty As Double = Convert.ToDouble(oMatrix.Columns.Item("Qty").Cells.Item(ik).Specific.value.Trim)
                    Dim FWhsCode As String = oMatrix.Columns.Item("FromWhs").Cells.Item(ik).Specific.value.Trim
                    Dim TWhsCode As String = oMatrix.Columns.Item("ToWhs").Cells.Item(ik).Specific.value.Trim
                    oSalesOrder.Lines.ItemCode = sItemCode
                    oSalesOrder.Lines.Quantity = Trim(Qty)
                    oSalesOrder.Lines.FromWarehouseCode = Trim(FWhsCode)
                    oSalesOrder.Lines.WarehouseCode = Trim(TWhsCode)
                    oSalesOrder.Lines.Add()
                    ' 1. Post the GRN documents...
                    If oSalesOrder.Add() = 0 Then
                        oApplication.StatusBar.SetText("Inventory Transfer Request Posted successfully ,Item Code" + sItemCode.ToString(), SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                    Else
                        oApplication.StatusBar.SetText("Unable To Inventory Transfer Request" & oCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    End If
                End If
            Next

            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText(" Transfer Request Document   Posting Method Faild", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        Finally
        End Try
    End Function

    Function PurchaseRequest() As Boolean
        Try
            oMatrix = frmPMActivity.Items.Item("Matrix2").Specific
            Dim oSalesOrder As SAPbobsCOM.Documents = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseRequest)
            Dim sBoolean As Boolean = False
            For ik As Integer = 1 To oMatrix.VisualRowCount
                Dim ITR As String = oMatrix.Columns.Item("ITR").Cells.Item(ik).Specific.value.ToString().Trim()
                If ITR = "P" Then
                    oSalesOrder.DocDate = Date.Now
                    oSalesOrder.TaxDate = Format_StringToDate(frmPMActivity.Items.Item("t_DocDate").Specific.value)
                    oSalesOrder.DocDueDate = Format_StringToDate(frmPMActivity.Items.Item("t_DocDate").Specific.value)
                    oSalesOrder.RequriedDate = Format_StringToDate(frmPMActivity.Items.Item("t_DocDate").Specific.value)
                    oSalesOrder.UserFields.Fields.Item("U_BaseNum").Value = oDBDSHeader.GetValue("DocNum", 0)
                    oSalesOrder.UserFields.Fields.Item("U_BaseObject").Value = "OBDA"
                    oSalesOrder.UserFields.Fields.Item("U_Series").Value = oDBDSHeader.GetValue("Series", 0)
                    oSalesOrder.UserFields.Fields.Item("U_BaseEntry").Value = oDBDSHeader.GetValue("DocEntry", 0)
                    'oSalesOrder.Requester = oCompany.UserSignature
                    Dim sItemCode As String = oMatrix.Columns.Item("Scode").Cells.Item(ik).Specific.value.ToString().Trim()
                    Dim Qty As Double = Convert.ToDouble(oMatrix.Columns.Item("Qty").Cells.Item(ik).Specific.value.Trim)
                    Dim FWhsCode As String = oMatrix.Columns.Item("FromWhs").Cells.Item(ik).Specific.value.Trim
                    Dim TWhsCode As String = oMatrix.Columns.Item("ToWhs").Cells.Item(ik).Specific.value.Trim
                    oSalesOrder.Lines.ItemCode = sItemCode
                    oSalesOrder.Lines.Quantity = Trim(Qty)
                    oSalesOrder.Lines.WarehouseCode = Trim(TWhsCode)
                    oSalesOrder.Lines.Add()
                    ' 1. Post the GRN documents...
                    If oSalesOrder.Add() = 0 Then
                        oApplication.StatusBar.SetText("Inventory Purchase Request Posted successfully ,Item Code" + sItemCode.ToString(), SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                    Else
                        oApplication.StatusBar.SetText("Unable To Purchase Transfer Request" & oCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    End If
                End If
            Next



            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText(" Purchase Document   Posting Method Faild", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        Finally
        End Try
    End Function
  

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        Dim oCmbo As SAPbouiCOM.ComboBox = frmPMActivity.Items.Item("c_Status").Specific
                        If oCmbo.Value = "C" Then
                            frmPMActivity.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                        Else
                            frmPMActivity.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try


                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Dim rsetUpadteIndent As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Try
                        If BusinessObjectInfo.BeforeAction Then


                        End If

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add/Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try


            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
End Class
