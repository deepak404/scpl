﻿Imports SAPLib
Imports System.Globalization
Imports System.Text.RegularExpressions
Imports SAPbobsCOM
Imports System.Data
Imports System.Threading
Public Class ParentSetup
    Dim boolPurchaseRequestPosting As Boolean = False
    Private oProgBar As SAPbouiCOM.ProgressBar
    Sub LoadPurchaseRequestPosting()
        Try

            CreateMySimpleFormForPurchaseRequestDetails()
            Try
                oForm.EnableMenu("1290", True)
                oForm.EnableMenu("1291", True)
                oForm.EnableMenu("1281", True)
                oForm.EnableMenu("1282", True)
                oForm.EnableMenu("1283", True)
                oForm.EnableMenu("1288", True)
                oForm.EnableMenu("1289", True)
            Catch ex As Exception
                StatusBarWarningMsg("Cannot Enable The Disabled System Menu ")
            Finally
            End Try
            DynamicControl_Load()


        Catch ex As Exception
            StatusBarWarningMsg("Load Parent Setup Failed")
        Finally
        End Try
    End Sub
    Function TransactionManagement() As Boolean

        TransactionManagement = InsertDetails()
        Return TransactionManagement
    End Function


    Function InsertDetails() As Boolean
        Try
            Dim sLOrder As String = oForm.Items.Item("C_LOrder").Specific.value.trim
            Dim sName As String = oForm.Items.Item("t_Name").Specific.value.trim
            Dim sPMachine As String = oForm.Items.Item("t_PMachine").Specific.value.trim
            'Dim sLevel As String = oForm.Items.Item("t_Level").Specific.value.trim
            'Dim sSLevel As String = oForm.Items.Item("t_SubLevel").Specific.value.trim
            Dim sParent As String = oForm.Items.Item("C_Parent").Specific.value.trim
            Dim rsetCode As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim strSQL As String = String.Empty
            strSQL = "select Isnull(Max( Convert(int,  code) ),0) +1 as Code from [@AIS_PSP]  "
            rsetCode.DoQuery(strSQL)
            strSQL = String.Empty
            Dim MaxNo As String = String.Empty
            If rsetCode.RecordCount > 0 Then
                rsetCode.MoveFirst()
                MaxNo = rsetCode.Fields.Item("Code").Value.ToString()
            End If
            Dim SaveDataRecords As SAPbouiCOM.Item
            SaveDataRecords = oForm.Items.Item("t_Name")
            SaveDataRecords = oForm.Items.Item("t_PMachine")
            SaveDataRecords = oForm.Items.Item("C_LOrder")
 
            SaveDataRecords = oForm.Items.Item("C_Parent")
            strSQL = strSQL + "Insert into [@AIS_PSP] (Code,Name,U_Name,U_PMachine,U_LOrder, U_Parent) Values ('" & MaxNo & "','" & MaxNo & "','" & sName & "','" & sPMachine & "','" & sLOrder & "  ','" & sParent & "')"

            If strSQL <> String.Empty Then
                rsetCode.DoQuery(strSQL)

                strSQL = String.Empty
            End If



            Return True
        Catch ex As Exception
            StatusBarWarningMsg("Insert Details Method Failed : " & ex.Message)
            Return False
        End Try
    End Function
    Sub CreateMySimpleFormForPurchaseRequestDetails()
        Try
            Dim CP As SAPbouiCOM.FormCreationParams = oApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)
            Dim oItem As SAPbouiCOM.Item
            Dim oLabel As SAPbouiCOM.StaticText
            Dim oButton As SAPbouiCOM.Button
            Dim oEditText As SAPbouiCOM.EditText
            Dim ogrid As SAPbouiCOM.Grid
            Dim oCheckBox As SAPbouiCOM.CheckBox
            Dim oCombobox As SAPbouiCOM.ComboBox
            CP.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Sizable
            CP.UniqueID = "OPRP"
            CP.FormType = "200"

            If FormExist("OPRP") Then
                oApplication.Forms.Item("OPRP").Select()
                oApplication.Forms.Item("OPRP").Close()
                oForm = oApplication.Forms.AddEx(CP)

            Else
                oForm = oApplication.Forms.AddEx(CP)
            End If


            ' Set form width and height 
            oForm.Height = 350
            oForm.Width = 600
            oForm.Title = "Parent Setup"



            ' Add a Grid item to the form 
            oItem = oForm.Items.Add("MDetails", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = 5
            oItem.Height = 14
            oItem.Top = 5
            oItem.Width = 100
            oLabel = oItem.Specific
            oLabel.Caption = "Machine Details"

            oItem = oForm.Items.Add("Name", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = 5
            oItem.Height = 14
            oItem.Top = 30
            oItem.Width = 45
            oLabel = oItem.Specific
            oLabel.Caption = "Name"

            oItem = oForm.Items.Add("PMachine", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = 5
            oItem.Height = 14
            oItem.Top = 45
            oItem.Width = 100
            oLabel = oItem.Specific
            oLabel.Caption = "Parent Machine"

            'oItem = oForm.Items.Add("Level", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            'oItem.Left = 5
            'oItem.Height = 14
            'oItem.Top = 94
            'oItem.Width = 100
            'oLabel = oItem.Specific
            'oLabel.Caption = "Level"


            'oItem = oForm.Items.Add("SubLevel", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            'oItem.Left = 5
            'oItem.Height = 14
            'oItem.Top = 108
            'oItem.Width = 100
            'oLabel = oItem.Specific
            'oLabel.Caption = "SubLevel"

            oItem = oForm.Items.Add("Parent", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = 5
            oItem.Height = 14
            oItem.Top = 122
            oItem.Width = 100
            oLabel = oItem.Specific
            oLabel.Caption = "Parent"


            oItem = oForm.Items.Add("LOrder", SAPbouiCOM.BoFormItemTypes.it_STATIC)
            oItem.Left = 5
            oItem.Height = 14
            oItem.Top = 61
            oItem.Width = 100
            oLabel = oItem.Specific
            oLabel.Caption = "Location Order"


            oItem = oForm.Items.Add("t_Name", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = 112
            oItem.Top = 31
            oItem.Width = 100
            oItem.Height = 14
            oEditText = oItem.Specific

            oItem = oForm.Items.Add("t_PMachine", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = 112
            oItem.Top = 46
            oItem.Width = 100
            oItem.Height = 14
            oEditText = oItem.Specific

            'oItem = oForm.Items.Add("t_Level", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            'oItem.Left = 112
            'oItem.Top = 92
            'oItem.Width = 100
            'oItem.Height = 14
            'oItem.Enabled = True
            'oEditText = oItem.Specific



            'oItem = oForm.Items.Add("t_SubLevel", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            'oItem.Left = 112
            'oItem.Top = 108
            'oItem.Width = 100
            'oItem.Height = 14
            'oItem.Enabled = True
            'oEditText = oItem.Specific

            oItem = oForm.Items.Add("C_Parent", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
            oItem.Left = 112
            oItem.Top = 123
            oItem.Width = 100
            oItem.Height = 14
            oItem.DisplayDesc = True
            oCombobox = oItem.Specific
            setComboBoxValue(oForm.Items.Item("C_Parent").Specific, "Select Code,U_Name from [@AIS_PSP] Union All Select '0','-' ")


            oForm.DataSources.UserDataSources.Add("SActive", SAPbouiCOM.BoDataType.dt_SHORT_TEXT, 1)
            oItem = oForm.Items.Add("SActive", SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
            oItem.Left = 5
            oItem.Top = 77
            oItem.Width = 100
            oItem.Height = 18
            oCheckBox = oItem.Specific
            oCheckBox.Checked = True
            oCheckBox.Caption = "Active"
            oCheckBox.DataBind.SetBound(True, "", "SActive")
            oForm.DataSources.UserDataSources.Item("SActive").Value = "Y"
            oCheckBox.ValOn = "Y"
            oCheckBox.ValOff = "N"


            oItem = oForm.Items.Add("C_LOrder", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
            oItem.Left = 112
            oItem.Top = 61
            oItem.Width = 100
            oItem.Height = 14
            oItem.DisplayDesc = True
            oCombobox = oItem.Specific
            setComboBoxValue(oForm.Items.Item("C_LOrder").Specific, "EXEC [dbo].[@AIS_Maintenance_GetLocationOrder] ")

            ' Set the grid data 
            oItem = oForm.Items.Add("Grid", SAPbouiCOM.BoFormItemTypes.it_GRID)
            oItem.Left = 226
            oItem.Height = 260
            oItem.Top = 30
            oItem.Width = 339
            oItem.Enabled = False
            ogrid = oItem.Specific


            oForm.DataSources.DataTables.Add("MyDataTable")

            oItem = oForm.Items.Add("Submit", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 5
            oItem.Top = 290
            oItem.Visible = True
            oButton = oItem.Specific
            oButton.Caption = "Add"

            oItem = oForm.Items.Add("2", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 75
            oItem.Top = 290
            oItem.Width = 65
            oItem.Height = 20
            oItem.Visible = True
            oButton = oItem.Specific
            oButton.Caption = "Cancel"


         

            oItem = oForm.Items.Add("B_Remove", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 220
            oItem.Top = 290
            oItem.Width = 70
            oItem.Height = 20
            oItem.Enabled = True
            oButton = oItem.Specific
            oButton.Caption = "Remove"


            oItem = oForm.Items.Add("t_Code", SAPbouiCOM.BoFormItemTypes.it_EDIT)
            oItem.Left = 112
            oItem.Top = 108
            oItem.Width = 0
            oItem.Height = 0
            oItem.Enabled = True
            oEditText = oItem.Specific


        Catch ex As Exception
            oApplication.StatusBar.SetText("Get Transfer ItemsMethod Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            oForm.Visible = True
        End Try
    End Sub


    Sub DynamicControl_Load()


        Try
            Dim oItem As SAPbouiCOM.Item = Nothing
            If boolPurchaseRequestPosting = False Then
                'Dim sName As String = oForm.Items.Item("t_Name").Specific.value.trim
                'Dim sParentMachine As String = oForm.Items.Item("t_PMachine").Specific.value.trim
                'Dim sLOrder As String = oForm.Items.Item("C_LOrder").Specific.value.trim
                'Dim sParent As String = oForm.Items.Item("C_Parent").Specific.value.trim

                Dim strSQL As String

                strSQL = "EXEC DBO.[@AIS_Maintance_LoadTreeValues]"
                oForm.DataSources.DataTables.Item("MyDataTable").ExecuteQuery(strSQL)
                Dim oMatrix_Terms As SAPbouiCOM.Grid = oForm.Items.Item("Grid").Specific
                oMatrix_Terms.DataTable = oForm.DataSources.DataTables.Item("MyDataTable")
            End If
        Catch ex As Exception
            oApplication.StatusBar.SetText("Dynamic Control Load  Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub




    Sub InitForm()
        Try
            oForm.Freeze(True)

        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            oForm.Freeze(False)
        End Try
    End Sub



    Function ValidateAll() As Boolean
        Try

            ValidateAll = True

        Catch ex As Exception
            oApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try

    End Function
    Sub ItemEvent_SubForm(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    Select Case pVal.ItemUID
                        Case "Grid"
                            Select Case pVal.ColUID
                                Case pVal.ColUID
                                    If pVal.BeforeAction = False Then
                                        Dim ogrid As SAPbouiCOM.Grid = oForm.Items.Item("Grid").Specific
                                        oForm.Items.Item("t_Code").Specific.value = ogrid.DataTable.Columns.Item("Code").Cells.Item(pVal.Row).Value
                                        oForm.Items.Item("t_Name").Specific.value = ogrid.DataTable.Columns.Item("Name").Cells.Item(pVal.Row).Value.ToString().Replace("-", "").ToString().Replace(">", "")
                                        oForm.Items.Item("t_PMachine").Specific.value = ogrid.DataTable.Columns.Item("MachineName").Cells.Item(pVal.Row).Value
                                        Dim oCmbo As SAPbouiCOM.ComboBox = oForm.Items.Item("C_Parent").Specific
                                        oCmbo.Select(ogrid.DataTable.Columns.Item("Parent").Cells.Item(pVal.Row).Value, SAPbouiCOM.BoSearchKey.psk_ByValue)
                                        Dim C_LOrder As SAPbouiCOM.ComboBox = oForm.Items.Item("C_LOrder").Specific

                                        If ogrid.DataTable.Columns.Item("Parent").Cells.Item(pVal.Row).Value = "0" Then
                                            C_LOrder.Select("P", SAPbouiCOM.BoSearchKey.psk_ByValue)
                                        Else
                                            C_LOrder.Select("C", SAPbouiCOM.BoSearchKey.psk_ByValue)
                                        End If
                                    End If



                            End Select

                        Case "t_Path"
                            If pVal.BeforeAction = False Then
                                ' cherche_fichier()
                                If BankFileName.Equals("") = False Then
                                    oForm = oApplication.Forms.ActiveForm
                                    oForm.Items.Item("t_Path").Specific.value = BankFileName
                                End If
                            End If

                    End Select

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "B_Remove"
                                If pVal.BeforeAction = False Then

                                    Try
                                        Dim SaveDataTableRecords As SAPbouiCOM.DataTable
                                        SaveDataTableRecords = oForm.DataSources.DataTables.Item("MyDataTable")
                                        Dim strQuery As String = ""
                                        Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                        strQuery = "DELETE FROM [@AIS_PSP] WHERE CODE = '" & oForm.Items.Item("t_Code").Specific.value & "' "
                                        rsetQuery.DoQuery(strQuery)
                                        DynamicControl_Load()
                                        oApplication.StatusBar.SetText("Data Removed Successfully", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                        oForm.Items.Item("t_Name").Specific.value = String.Empty
                                        oForm.Items.Item("t_PMachine").Specific.value = String.Empty

                                        SetComboBoxValueRefresh(oForm.Items.Item("C_Parent").Specific, "Select Code,U_Name from [@AIS_PSP] Union All Select '0','-' ")

                                    Catch ex As Exception
                                        oApplication.StatusBar.SetText("REMOVE Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

                                    End Try



                                End If



                            Case "Submit"
                                If pVal.BeforeAction = False Then
                                    DynamicControl_Load()
                                    oForm.Items.Item("t_Name").Specific.value = String.Empty
                                    oForm.Items.Item("t_PMachine").Specific.value = String.Empty
                                    Dim oCmbo As SAPbouiCOM.ComboBox = oForm.Items.Item("C_Parent").Specific
                                    oCmbo.Select("0", SAPbouiCOM.BoSearchKey.psk_ByValue)
                                    SetComboBoxValueRefresh(oForm.Items.Item("C_Parent").Specific, "Select Code,U_Name from [@AIS_PSP] Union All Select '0','-' ")



                                End If
                                'Add,Update Event
                                If pVal.BeforeAction = True And (oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then

                                    If Me.ValidateAll() = False Then
                                        System.Media.SystemSounds.Asterisk.Play()
                                        BubbleEvent = False
                                        Exit Sub



                                    Else

                                        If Not TransactionManagement() Then

                                            BubbleEvent = False
                                            Return
                                        Else
                                            

                                        End If

                                    End If
                                End If


                        End Select

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        If oCompany.InTransaction Then oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_RollBack)
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID


                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                    Try
                        If boolPurchaseRequestPosting Then

                        End If
                    Catch ex As Exception
                        StatusBarWarningMsg("Validate Event Failed:")
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("SubGrid Item Event Failed" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub


    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1283"
                    'Dim SaveDataTableRecords As SAPbouiCOM.DataTable
                    'SaveDataTableRecords = oForm.DataSources.DataTables.Item("MyDataTable")
                    'Dim strQuery As String = ""
                    'Dim rsetQuery As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    'strQuery = "DELETE FROM [@AIS_PSP] WHERE CODE = '" & SaveDataTableRecords.GetValue("Code", pVal.Row).ToString & "' "
                    'rsetQuery.DoQuery(strQuery)


            End Select
        Catch ex As Exception
            GFun.oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try

    End Sub
End Class
