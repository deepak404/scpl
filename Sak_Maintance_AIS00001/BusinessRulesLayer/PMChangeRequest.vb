﻿Imports System.Xml
Imports System.Collections.Specialized
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data
Imports SAPLib
Class PMChangeRequest

    Dim frmPMChangeRequest As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim boolFormLoaded As Boolean = False
    Dim sFormUID As String
    Dim UDOID As String = "OPMCR"
    Sub PMChangeRequest()
        Try


            boolFormLoaded = False
            LoadXML(frmPMChangeRequest, PMChangeRequestFormID, PMChangeRequestXML)
            frmPMChangeRequest = oApplication.Forms.Item(PMChangeRequestFormID)
            oDBDSHeader = frmPMChangeRequest.DataSources.DBDataSources.Item("@AIS_OPMCR")
            oDBDSDetail = frmPMChangeRequest.DataSources.DBDataSources.Item("@AIS_PMCR1")
            oMatrix = frmPMChangeRequest.Items.Item("Matrix1").Specific


            frmPMChangeRequest.Freeze(True)


            Me.InitForm()
            Me.DefineModeForFields()


        Catch ex As Exception
            oApplication.StatusBar.SetText("Load  PM request Form Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmPMChangeRequest.Freeze(False)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmPMChangeRequest.Freeze(True)
            LoadComboBoxSeries(frmPMChangeRequest.Items.Item("c_series").Specific, UDOID)
            setComboBoxValue(frmPMChangeRequest.Items.Item("c_ToDept").Specific, "select code,name from OUDP")
            setComboBoxValue(frmPMChangeRequest.Items.Item("C_FDept").Specific, "select code,name from OUDP")
            setComboBoxValue(frmPMChangeRequest.Items.Item("C_Status").Specific, "EXEC [dbo].[@AIS_Maintenance_PMChangeRequest_GetStatusMaster]")
            Dim oCmbo As SAPbouiCOM.ComboBox = frmPMChangeRequest.Items.Item("C_Status").Specific
            oCmbo.Select("W", SAPbouiCOM.BoSearchKey.psk_ByValue)
            LoadDocumentDate(frmPMChangeRequest.Items.Item("t_DocDate").Specific)
            oMatrix.Clear()
            SetNewLine(oMatrix, oDBDSDetail)
   
        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmPMChangeRequest.Freeze(False)
        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub CreateFormForActivityList()
        Try

            Dim sQuery As String
            Dim CP As SAPbouiCOM.FormCreationParams = oApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)
            Dim oItem As SAPbouiCOM.Item
            Dim oButton As SAPbouiCOM.Button
            Dim oStaticText As SAPbouiCOM.StaticText
            Dim oEditText As SAPbouiCOM.EditText
            Dim oGrid As SAPbouiCOM.Grid

            CP.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Sizable
            CP.UniqueID = "PMChangeRequest"
            CP.FormType = "200"

            oForm = oApplication.Forms.AddEx(CP)
            ' Set form width and height 
            oForm.Height = 260
            oForm.Width = 500
            oForm.Title = "PM Activity Details"


            '' Add a Grid item to the form 
            oItem = oForm.Items.Add("MyGrid", SAPbouiCOM.BoFormItemTypes.it_GRID)
            ' Set the grid dimentions and position 
            oItem.Left = 6
            oItem.Top = 0
            oItem.Width = 500
            oItem.Height = 200
            ' Set the grid data 
            oGrid = oItem.Specific
            oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Auto
            oForm.DataSources.DataTables.Add("MyDataTable")


            sQuery = "execute [@AIS_Main_PMChangeRequest_GetPMActivityHeaderDetails]'" & frmPMChangeRequest.Items.Item("t_MCode").Specific.value & "'"
            oForm.DataSources.DataTables.Item(0).ExecuteQuery(sQuery)

            oGrid.DataTable = oForm.DataSources.DataTables.Item("MyDataTable")
            oGrid.AutoResizeColumns()


            oGrid.Columns.Item(1).Editable = False
            oGrid.Columns.Item(2).Editable = False

            oGrid.Columns.Item(0).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox
            ' Add OK Button 
            oItem = oForm.Items.Add("Bt_Copy", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 6
            oItem.Top = 200
            oItem.Width = 65
            oItem.Height = 20
            oButton = oItem.Specific
            oButton.Caption = "Submit"
            ' Add CANCEL Button 
            oItem = oForm.Items.Add("2", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
            oItem.Left = 90
            oItem.Top = 200
            oItem.Width = 65
            oItem.Height = 20
            oButton = oItem.Specific



        Catch ex As Exception

            oApplication.StatusBar.SetText("Create Form For Quotation Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally

            oForm.Visible = True

        End Try
    End Sub
    Sub DefineModeForFields()
        Try
            frmPMChangeRequest.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmPMChangeRequest.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmPMChangeRequest.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub ItemEvent(FormUID As String, pVal As SAPbouiCOM.ItemEvent, BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID
                                Case "t_MCode"
                                    oDBDSHeader.SetValue("U_MCCode", 0, Trim(oDataTable.GetValue("ResCode", 0)))
                                    oDBDSHeader.SetValue("U_MCName", 0, Trim(oDataTable.GetValue("ResName", 0)))
                                    'oMatrix.Clear()
                                    'oDBDSDetail.Clear()
                                    'Dim strMCCode As String = frmPMChangeRequest.Items.Item("t_MCode").Specific.value.ToString.Trim
                                    'Dim sQuery As String = String.Empty
                                    'sQuery = "EXEC [dbo].[@AIS_Main_PMChangeRequest_GetPMActivityDetails] '" & strMCCode & "'"
                                    'Dim rsetItem As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                    'If rsetItem.RecordCount > 0 Then
                                    '    rsetItem.MoveFirst()
                                    'End If
                                    'For iRowID As Integer = 0 To rsetItem.RecordCount - 1
                                    '    oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                                    '    oDBDSDetail.SetValue("LineId", iRowID, iRowID + 1)
                                    '    oDBDSDetail.SetValue("U_BaseEntry", iRowID, Trim(rsetItem.Fields.Item("DocEntry").Value))
                                    '    oDBDSDetail.SetValue("U_BaseNum", iRowID, Trim(rsetItem.Fields.Item("DocNum").Value))
                                    '    oDBDSDetail.SetValue("U_Task", iRowID, Trim(rsetItem.Fields.Item("U_Task").Value))
                                    '    oDBDSDetail.SetValue("U_PMDate", iRowID, CDate(rsetItem.Fields.Item("U_SDFrom").Value).ToString("yyyyMMdd"))

                                    '    rsetItem.MoveNext()
                                    'Next
                                Case "t_ReqBy"
                                    oDBDSHeader.SetValue("U_ReqBy", 0, Trim(oDataTable.GetValue("empID", 0)))
                                    oDBDSHeader.SetValue("U_ReqName", 0, Trim(oDataTable.GetValue("lastName", 0)) + Trim(oDataTable.GetValue("firstName", 0)))
                                Case "t_Aname"
                                    oDBDSHeader.SetValue("U_AName", 0, Trim(oDataTable.GetValue("lastName", 0)) + Trim(oDataTable.GetValue("firstName", 0)))
                           

                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmPMChangeRequest.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmPMChangeRequest.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmPMChangeRequest.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If

                            Case "31"
                                oMatrix.Clear()
                                oDBDSDetail.Clear()
                                Dim strMCCode As String = frmPMChangeRequest.Items.Item("t_MCode").Specific.value.ToString.Trim
                                Dim strFromdate As String = frmPMChangeRequest.Items.Item("T_FromDate").Specific.value.ToString.Trim
                                Dim strTodate As String = frmPMChangeRequest.Items.Item("t_ToDate").Specific.value.ToString.Trim
                                Dim oc_DType As SAPbouiCOM.ComboBox = frmPMChangeRequest.Items.Item("31").Specific
                                Dim strSerialCode As String = oc_DType.Selected.Value


                                Dim sQuery As String = String.Empty
                                sQuery = "EXEC [dbo].[@AIS_Main_PMChangeReguest_LoadResourceDetails] '" & strMCCode & "','" + strSerialCode + "','" + strFromdate + "','" + strTodate + "'"
                                Dim rsetItem As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                If rsetItem.RecordCount > 0 Then
                                    rsetItem.MoveFirst()
                                End If
                                For iRowID As Integer = 0 To rsetItem.RecordCount - 1
                                    oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                                    oDBDSDetail.SetValue("LineId", iRowID, iRowID + 1)
                                    oDBDSDetail.SetValue("U_BaseEntry", iRowID, Trim(rsetItem.Fields.Item("Code").Value))
                                    oDBDSDetail.SetValue("U_BaseNum", iRowID, Trim(rsetItem.Fields.Item("U_Code").Value))
                                    oDBDSDetail.SetValue("U_Frequency", iRowID, Trim(rsetItem.Fields.Item("U_Frequency").Value))
                                    oDBDSDetail.SetValue("U_Task", iRowID, Trim(rsetItem.Fields.Item("U_Task").Value))
                                    'oDBDSDetail.SetValue("U_LUDate", iRowID, CDate(rsetItem.Fields.Item("U_LastUpdateDate").Value).ToString("yyyyMMdd"))
                                    'oDBDSDetail.SetValue("U_SDFrom", iRowID, CDate(rsetItem.Fields.Item("U_PreparedDate").Value).ToString("yyyyMMdd"))
                                    'oDBDSDetail.SetValue("U_STFrom", iRowID, Trim(rsetItem.Fields.Item("U_SFromTime").Value))
                                    'oDBDSDetail.SetValue("U_STTo", iRowID, Trim(rsetItem.Fields.Item("U_SToTime").Value))
                                    'oDBDSDetail.SetValue("U_SDTo", iRowID, CDate(rsetItem.Fields.Item("U_LastUpdateDate").Value).ToString("yyyyMMdd"))

                                    rsetItem.MoveNext()
                                Next
                                oMatrix.LoadFromDataSource()
                                'LoadProductionPlanOrderDetails(strMCCode)
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "matrix1"
                                Select Case pVal.ColUID
                                    Case "Selected"
                                        If pVal.BeforeAction = False Then
                                            Try


                                            Catch ex As Exception
                                            Finally
                                            End Try

                                        End If
                                End Select

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID
                            Case "Matrix1"
                                Select Case pVal.ColUID
                                    Case "Task"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix, oDBDSDetail, pVal.Row, pVal.ColUID)
                                        End If

                                End Select

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "Bt_Copy"
                                If pVal.BeforeAction = False Then
                                    CreateFormForActivityList()
                                End If
                         

                        End Select
                    Catch ex As Exception
                        StatusBarWarningMsg("Click Event Failed:")
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.ActionSuccess And frmPMChangeRequest.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
    Sub ItemEvent_SubGrid(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        Select Case pVal.ItemUID
                            Case "Bt_Copy"
                                oMatrix.Clear()
                                oDBDSDetail.Clear()

                                Dim oGrid As SAPbouiCOM.Grid = oForm.Items.Item("MyGrid").Specific

                                For i As Integer = 0 To ogrid.Rows.Count - 1
                                    Dim sDocEntry As String = oGrid.DataTable.Columns.Item("DocEntry").Cells.Item(i).Value.ToString().Trim()
                                    If oGrid.DataTable.Columns.Item("Selected").Cells.Item(i).Value.ToString().Trim.ToUpper() = "Y" Then
                                        Dim sQuery As String = String.Empty
                                        sQuery = "EXEC [dbo].[@AIS_Main_PMChangeRequest_GetPMActivityDetails]  '" & sDocEntry & "'"
                                        Dim rsetItem As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                        If rsetItem.RecordCount > 0 Then
                                            rsetItem.MoveFirst()
                                        End If
                                        For iRowID As Integer = 0 To rsetItem.RecordCount - 1
                                            oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                                            oDBDSDetail.SetValue("LineId", iRowID, iRowID + 1)
                                            oDBDSDetail.SetValue("U_BaseNum", iRowID, Trim(rsetItem.Fields.Item("DocNum").Value))
                                            oDBDSDetail.SetValue("U_BaseEntry", iRowID, Trim(rsetItem.Fields.Item("DocEntry").Value))
                                            oDBDSDetail.SetValue("U_BaseLine", iRowID, Trim(rsetItem.Fields.Item("LineId").Value))
                                            oDBDSDetail.SetValue("U_Task", iRowID, Trim(rsetItem.Fields.Item("U_Task").Value))
                                            oDBDSDetail.SetValue("U_PMDate", iRowID, CDate(rsetItem.Fields.Item("U_SDFrom").Value).ToString("yyyyMMdd"))
                                           
                                            rsetItem.MoveNext()
                                        Next
                                        oMatrix.LoadFromDataSource()
                                    End If

                                Next



                        End Select
                    Catch ex As Exception
                        Msg("Click Event Failed : " & ex.Message)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("SubGrid Item Event Failed" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        End Try
    End Sub


  
    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try
                        Dim PurchaseOrderNo As Boolean = False
                        Dim sQuery = " Select  T0.DocEntry  From OPOR T0   Where T0.U_BaseEntry ='" + oDBDSHeader.GetValue("DocEntry", 0) + "'"
                        Dim rsetEmpDets As SAPbobsCOM.Recordset = DoQuery(sQuery)
                        If rsetEmpDets.RecordCount > 0 Then
                            PurchaseOrderNo = True
                        End If
                        sQuery = " Select  T0.DocEntry  From OWTR   T0   Where T0.U_BaseEntry ='" + oDBDSHeader.GetValue("DocEntry", 0) + "'"
                        Dim rsetEmpDets2 As SAPbobsCOM.Recordset = DoQuery(sQuery)
                        If rsetEmpDets2.RecordCount > 0 Then
                            PurchaseOrderNo = True
                        Else
                            PurchaseOrderNo = False
                        End If
                        If PurchaseOrderNo = True Then
                            frmPMChangeRequest.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                        Else
                            frmPMChangeRequest.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Dim rsetUpadteIndent As SAPbobsCOM.Recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    Try
                        If BusinessObjectInfo.BeforeAction Then


                        End If

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add/Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try


            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
End Class
