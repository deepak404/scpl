﻿
Imports System.Xml
Imports System.Collections.Specialized
Imports System.IO
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data
Imports SAPLib

Public Class CAPAReport

    Dim frmCAPAReport As SAPbouiCOM.Form
    Dim oDBDSHeader, oDBDSDetail, oDBDSDetail2, oDBDSDetail3 As SAPbouiCOM.DBDataSource
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim oMatrix1 As SAPbouiCOM.Matrix
    Dim oMatrix2 As SAPbouiCOM.Matrix

    Dim boolFormLoaded As Boolean = False
    Dim sFormUID As String
    Dim UDOID As String = "CAPA"
    Sub CAPAReport()

        Try
            boolFormLoaded = False
            LoadXML(frmCAPAReport, CAPAReportFormID, CAPAReportXML)
            frmCAPAReport = oApplication.Forms.Item(CAPAReportFormID)
            oDBDSHeader = frmCAPAReport.DataSources.DBDataSources.Item("@AIS_OCAPA")
            oDBDSDetail = frmCAPAReport.DataSources.DBDataSources.Item("@AIS_CAPA1")
            oDBDSDetail2 = frmCAPAReport.DataSources.DBDataSources.Item("@AIS_CAPA2")
            oDBDSDetail3 = frmCAPAReport.DataSources.DBDataSources.Item("@AIS_CAPA3")

            oMatrix = frmCAPAReport.Items.Item("57").Specific
            oMatrix1 = frmCAPAReport.Items.Item("58").Specific
            oMatrix2 = frmCAPAReport.Items.Item("59").Specific
            frmCAPAReport.Freeze(True)
            ' 
            frmCAPAReport.Items.Item("44").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

            Me.InitForm()
            Me.DefineModeForFields()


        Catch ex As Exception
            oApplication.StatusBar.SetText("Load CA-PA Report Form Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmCAPAReport.Freeze(False)
        End Try
    End Sub

    Sub InitForm()
        Try
            frmCAPAReport.Freeze(True)
            LoadComboBoxSeries(frmCAPAReport.Items.Item("c_series").Specific, UDOID)
            LoadDocumentDate(frmCAPAReport.Items.Item("t_DocDate").Specific)
            setComboBoxValue(frmCAPAReport.Items.Item("c_shift").Specific, "EXEC [dbo].[@AIS_Maintenance_GetShiftMaster] ")
            oMatrix.Clear()
            SetNewLine(oMatrix, oDBDSDetail)
            ' setComboBoxValue(oMatrix.Columns.Item("c_Status").Cells.Item(1).Specific, "EXEC [dbo].[@AIS_Maintain_GetStatusMaster]  ")
            oMatrix1.Clear()
            SetNewLine(oMatrix1, oDBDSDetail2)
            'setComboBoxValue(oMatrix1.Columns.Item("c_Status2").Cells.Item(1).Specific, "EXEC [dbo].[@AIS_Maintain_GetStatusMaster]  ")
            oMatrix2.Clear()
            SetNewLine(oMatrix2, oDBDSDetail3)
            'setComboBoxValue(oMatrix2.Columns.Item("c_Status1").Cells.Item(1).Specific, "EXEC [dbo].[@AIS_Maintain_GetStatusMaster]  ")

            Dim oCmbSerial As SAPbouiCOM.CheckBox = frmCAPAReport.Items.Item("c_PONC").Specific
            If oCmbSerial.Checked = True Then
                Try
                    frmCAPAReport.Items.Item("t_BaseNo").Specific.value = String.Empty
                Catch ex As Exception
                End Try
                frmCAPAReport.Items.Item("t_BaseNo").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                frmCAPAReport.Items.Item("t_BaseNo").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            Else
                Try
                    frmCAPAReport.Items.Item("t_BaseNo").Specific.value = String.Empty
                Catch ex As Exception
                End Try

                frmCAPAReport.Items.Item("t_BaseNo").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                frmCAPAReport.Items.Item("t_BaseNo").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            End If


        Catch ex As Exception
            oApplication.StatusBar.SetText("InitForm Method Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
            frmCAPAReport.Freeze(False)
        End Try
    End Sub
    ''' <summary>
    ''' Method to define the form modes
    ''' -1 --> All modes 
    '''  1 --> OK
    '''  2 --> Add
    '''  4 --> Find
    '''  5 --> View
    ''' </summary>
    ''' <remarks></remarks>
    ''' 


    Sub DefineModeForFields()
        Try
            frmCAPAReport.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmCAPAReport.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmCAPAReport.Items.Item("c_series").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            '4
            frmCAPAReport.Items.Item("t_DocNum").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            frmCAPAReport.Items.Item("t_DocDate").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)




        Catch ex As Exception
            oApplication.StatusBar.SetText("Define Mode For Fields Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Function Validation() As Boolean
        Try

            Return True
        Catch ex As Exception
            oApplication.StatusBar.SetText("Validation Function Failed: " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Function

    Sub LineTotalDetails()
        Dim dBaseAmount As Double = 0
        Dim dTaxRate As Double = 0
        Dim dLineTotal As Double = 0
        For i As Integer = 1 To oMatrix.VisualRowCount
            If oMatrix.Columns.Item("ICode").Cells.Item(i).Specific.value.Trim.Equals("") = False Then
                dBaseAmount = dBaseAmount + CDbl(oMatrix.Columns.Item("bamount").Cells.Item(i).Specific.value.Trim)
                dTaxRate = dTaxRate + CDbl(oMatrix.Columns.Item("taxrate").Cells.Item(i).Specific.value.Trim)
                dLineTotal = dLineTotal + CDbl(oMatrix.Columns.Item("total").Cells.Item(i).Specific.value.Trim)
            End If
        Next
        frmCAPAReport.Items.Item("t_BaseAmou").Specific.value = dBaseAmount.ToString().Trim
        frmCAPAReport.Items.Item("t_TaxAmoun").Specific.value = dTaxRate.ToString().Trim
        frmCAPAReport.Items.Item("t_Total").Specific.value = dLineTotal.ToString().Trim

    End Sub

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        Dim oDataTable As SAPbouiCOM.DataTable
                        Dim oCFLE As SAPbouiCOM.ChooseFromListEvent
                        oCFLE = pVal
                        oDataTable = oCFLE.SelectedObjects
                        If Not oDataTable Is Nothing Then
                            Select Case pVal.ItemUID
                                Case "t_Customer"
                                    oDBDSHeader.SetValue("U_CardCode", 0, Trim(oDataTable.GetValue("CardCode", 0)))
                                    oDBDSHeader.SetValue("U_CardName", 0, Trim(oDataTable.GetValue("CardName", 0)))

                                Case "t_Pro"
                                    oDBDSHeader.SetValue("U_Product", 0, Trim(oDataTable.GetValue("ItemCode", 0)))
                                    oDBDSHeader.SetValue("U_ItemName", 0, Trim(oDataTable.GetValue("ItemName", 0)))
                                Case "t_MAch"
                                    oDBDSHeader.SetValue("U_Machine", 0, Trim(oDataTable.GetValue("ResCode", 0)))
                                    oDBDSHeader.SetValue("U_MacName", 0, Trim(oDataTable.GetValue("ResName", 0)))
                                Case "t_BaseNo"
                                    oDBDSHeader.SetValue("U_BaseNum", 0, Trim(oDataTable.GetValue("DocNum", 0)))
                                    oDBDSHeader.SetValue("U_BaseEntry", 0, Trim(oDataTable.GetValue("DocEntry", 0)))


                                    'Case "t_Rew"
                                    '    oDBDSHeader.SetValue("U_ReviewBy", 0, Trim(oDataTable.GetValue("firstName", 0)))

                                    'Case "t_Appr"
                                    '    oDBDSHeader.SetValue("U_ApprovBy", 0, Trim(oDataTable.GetValue("firstName", 0)))

                                    'Case "t_rais"
                                    '    oDBDSHeader.SetValue("U_Raisedby", 0, Trim(oDataTable.GetValue("firstName", 0)))

                                Case "matPreSale"
                                    Select Case pVal.ColUID
                                        Case "ICode"
                                            'Dim sQuery = "Select ITem from OITM Where ItemCode='" & Trim(oDataTable.GetValue("ItemCode", 0)) & "'"

                                            'Dim rsetItem As SAPbobsCOM.Recordset = DoQuery(sQuery)
                                            'If rsetItem.RecordCount > 0 Then
                                            '    rsetItem.MoveFirst()
                                            '    Dim ItemUOM As String = Trim(rsetItem.Fields.Item("U_ItemCode").Value)
                                            'End If

                                            'Manual

                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_ItemCode", pVal.Row - 1, Trim(oDataTable.GetValue("ItemCode", 0)))
                                            oDBDSDetail.SetValue("U_ItemName", pVal.Row - 1, Trim(oDataTable.GetValue("ItemName", 0)))
                                            oDBDSDetail.SetValue("U_UOM", pVal.Row - 1, "Manual")
                                            oDBDSDetail.SetValue("U_Quantity", pVal.Row - 1, 1)
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            SetNewLine(oMatrix, oDBDSDetail, oMatrix.VisualRowCount, pVal.ColUID)
                                        Case "TaxCode"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_TaxCode", pVal.Row - 1, Trim(oDataTable.GetValue("Code", 0)))
                                            oDBDSDetail.SetValue("U_TaxRate", pVal.Row - 1, Trim(oDataTable.GetValue("Rate", 0)))
                                            'oMatrix.LoadFromDataSource()
                                            'oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()

                                            'oMatrix.FlushToDataSource()
                                            Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_BaseAmount", pVal.Row - 1)) + CDbl(Trim(oDataTable.GetValue("Rate", 0)))
                                            oDBDSDetail.SetValue("U_LineTotal", pVal.Row - 1, dblValue)
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                            LineTotalDetails()

                                        Case "WhsCod"
                                            oMatrix.FlushToDataSource()
                                            oDBDSDetail.SetValue("U_WhsCode", pVal.Row - 1, Trim(oDataTable.GetValue("WhsCode", 0)))
                                            oDBDSDetail.SetValue("U_WhsName", pVal.Row - 1, Trim(oDataTable.GetValue("WhsName", 0)))
                                            oMatrix.LoadFromDataSource()
                                            oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                                    End Select

                            End Select
                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Choose From List Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "c_PONC"
                                If pVal.BeforeAction = False Then
                                    Dim oCmbSerial As SAPbouiCOM.CheckBox = frmCAPAReport.Items.Item("c_PONC").Specific
                                    If oCmbSerial.Checked = True Then
                                        Try
                                            frmCAPAReport.Items.Item("t_BaseNo").Specific.value = String.Empty
                                        Catch ex As Exception
                                        End Try
                                        frmCAPAReport.Items.Item("t_BaseNo").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
                                        frmCAPAReport.Items.Item("t_BaseNo").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

                                    Else
                                        Try
                                            frmCAPAReport.Items.Item("t_BaseNo").Specific.value = String.Empty
                                        Catch ex As Exception
                                        End Try

                                        frmCAPAReport.Items.Item("t_BaseNo").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                                        frmCAPAReport.Items.Item("t_BaseNo").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                                    End If
                                End If
                            Case "1"
                                If pVal.ActionSuccess And frmCAPAReport.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If

                            Case "44"
                                If pVal.BeforeAction = False Then
                                    frmCAPAReport.PaneLevel = 2
                                End If
                            Case "45"
                                If pVal.BeforeAction = False Then
                                    frmCAPAReport.PaneLevel = 3
                                End If
                            Case "46"
                                If pVal.BeforeAction = False Then
                                    frmCAPAReport.PaneLevel = 4
                                End If
                            Case "47"
                                If pVal.BeforeAction = False Then
                                    frmCAPAReport.PaneLevel = 5
                                End If
                            Case "48"
                                If pVal.BeforeAction = False Then
                                    frmCAPAReport.PaneLevel = 6
                                End If
                            Case "49"
                                If pVal.BeforeAction = False Then
                                    frmCAPAReport.PaneLevel = 7
                                End If
                            Case "50"
                                If pVal.BeforeAction = False Then
                                    frmCAPAReport.PaneLevel = 8
                                End If
                            Case "74"
                                If pVal.BeforeAction = False Then
                                    frmCAPAReport.PaneLevel = 9
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        Select Case pVal.ItemUID

                            'Case "matPreSale"
                            '    Select Case pVal.ColUID
                            '        Case "V_1", "V_8"
                            '            If pVal.BeforeAction = False And pVal.ItemChanged Then
                            '                oMatrix.FlushToDataSource()
                            '                Dim TaxAmount As Double = CDbl(oDBDSDetail.GetValue("U_TaxRate", pVal.Row - 1))
                            '                Dim dblValue As Double = CDbl(oDBDSDetail.GetValue("U_Quantity", pVal.Row - 1)) * CDbl(oDBDSDetail.GetValue("U_UnitPrice", pVal.Row - 1))
                            '                oDBDSDetail.SetValue("U_BaseAmount", pVal.Row - 1, dblValue)
                            '                oDBDSDetail.SetValue("U_LineTotal", pVal.Row - 1, dblValue + TaxAmount)
                            '                oMatrix.LoadFromDataSource()
                            '                oMatrix.Columns.Item(pVal.ColUID).Cells.Item(pVal.Row).Click()
                            '                LineTotalDetails()
                            '            End If
                            Case "58"
                                Select Case pVal.ColUID
                                    Case "t_RC2"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix1, oDBDSDetail2, pVal.Row, pVal.ColUID)
                                        End If
                                End Select
                            Case "59"
                                Select Case pVal.ColUID
                                    Case "t_RC1"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix2, oDBDSDetail3, pVal.Row, pVal.ColUID)
                                        End If
                                End Select
                            Case "57"
                                Select Case pVal.ColUID
                                    Case "t_RC"
                                        If pVal.BeforeAction = False And pVal.ItemChanged = True Then
                                            SetNewLine(oMatrix, oDBDSDetail, pVal.Row, pVal.ColUID)
                                        End If
                                End Select


                        End Select
                    Catch ex As Exception
                    End Try

                Case (SAPbouiCOM.BoEventTypes.et_COMBO_SELECT)
                    Try
                        Select Case pVal.ItemUID
                            Case "c_series"
                                If frmCAPAReport.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.BeforeAction = False Then
                                    'Get the Serial Number Based On Series...
                                    Dim oCmbSerial As SAPbouiCOM.ComboBox = frmCAPAReport.Items.Item("c_series").Specific
                                    Dim strSerialCode As String = oCmbSerial.Selected.Value
                                    Dim strDocNum As Long = frmCAPAReport.BusinessObject.GetNextSerialNumber(strSerialCode, UDOID)
                                    oDBDSHeader.SetValue("DocNum", 0, strDocNum)
                                End If
                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Combo Select Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    Try
                        Select Case pVal.ItemUID
                            Case "1"
                                If pVal.ActionSuccess And frmCAPAReport.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                    InitForm()
                                End If

                        End Select
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Item Pressed Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Item Event Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.MenuUID
                Case "1282"
                    Me.InitForm()

            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Menu Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD, SAPbouiCOM.BoEventTypes.et_FORM_DATA_UPDATE
                    Try
                        If BusinessObjectInfo.ActionSuccess Then


                        End If
                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Add ,Update Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        BubbleEvent = False
                    Finally
                    End Try
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    Try

                    Catch ex As Exception
                        oApplication.StatusBar.SetText("Form Data Load Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Finally
                    End Try
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Form Data Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

    Sub RightClickEvent(ByRef EventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
        Try
            Select Case EventInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK
                    If oApplication.Forms.Item(EventInfo.FormUID).Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And EventInfo.BeforeAction Then
                    End If
            End Select
        Catch ex As Exception
            oApplication.StatusBar.SetText("Right Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub

End Class


