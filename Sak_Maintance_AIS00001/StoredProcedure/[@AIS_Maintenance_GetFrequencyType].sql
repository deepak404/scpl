
create Procedure [@AIS_Maintenance_GetFrequencyType]
AS
Begin

Select 'D' as Code,'Daily' as Name
Union All
Select 'W','Weekly'
Union All
Select 'M','Monthly'
Union All
Select 'Y','Yearly'
END