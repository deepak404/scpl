
ALTER Procedure [dbo].[@AIS_Maintenance_GetTypeMaster]
AS
Begin

Select 'M' as Code,'Mechanical' as Name
Union All
Select 'E','Electrical'
Union All
Select 'O','Operator'

END