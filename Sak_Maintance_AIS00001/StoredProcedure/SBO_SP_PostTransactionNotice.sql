
ALTER proc [dbo].[SBO_SP_PostTransactionNotice]

@object_type nvarchar(20), 				-- SBO Object Type
@transaction_type nchar(1),			-- [A]dd, [U]pdate, [D]elete, [C]ancel, C[L]ose
@num_of_cols_in_key int,
@list_of_key_cols_tab_del nvarchar(255),
@list_of_cols_val_tab_del nvarchar(255)

AS

begin

-- Return values
declare @error  int				-- Result (0 for no error)
declare @error_message nvarchar (200) 		-- Error string to be displayed
select @error = 0
select @error_message = N'Ok'

--------------------------------------------------------------------------------------------------------------------------------
If (@object_type ='OBDA' AND @transaction_type IN (N'A'))
Begin
 EXEC [dbo].[@AIS_Maintenance_BRequest_CloseStatus]@list_of_cols_val_tab_del 
  EXEC [dbo].[@AIS_BActivity_CloseStatus]@list_of_cols_val_tab_del 
END
If (@object_type ='OPMA' AND @transaction_type IN (N'A'))
Begin
EXEC [dbo].[@AIS_PMActivity_CloseStatus]
End
--------------------------------------------------------------------------------------------------------------------------------

-- Select the return values
select @error, @error_message

end