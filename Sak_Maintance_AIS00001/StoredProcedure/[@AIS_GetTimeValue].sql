Alter FUNCTION  [dbo].[@AIS_GetTimeValue](@InTime int, @OutTime int)
returns Decimal(10,2) as
BEGIN
Declare @In_Time as Datetime 
Declare @Out_Time as Datetime 
set @In_Time = STUFF(RIGHT('0000' + CAST(@InTime as varchar), 4),3,0,':')
set @Out_Time = STUFF(RIGHT('0000' + CAST(@OutTime as varchar), 4),3,0,':')
set @Out_Time = case when @In_Time < @Out_Time then  @Out_Time +1 else @Out_Time end
return( replace(substring(convert(varchar,convert(time,@Out_Time - @In_Time)),0,6),':','.'))
END
