 
ALTER procedure [dbo].[@AIS_Main_PMActivity_LoadResourceDetails] (@MCCode varchar(50),@U_Type Varchar(max),@FromDate varchar(max),@ToDate varchar(max) )
as
begin
IF (@U_Type='M')
Begin
Select  Code,U_Code ,U_Frequency,U_Task,U_LastUpdateDate , U_PreparedDate ,



Case When U_Frequency='D' Then  DateAdd(day,U_Duration, U_PreparedDate )    

  When U_Frequency='W' Then  DateAdd(WEEK,U_Duration, U_PreparedDate )

  When U_Frequency='M' Then  DateAdd(MONTH,U_Duration, U_PreparedDate )

  When U_Frequency='Y' Then  DateAdd(YEAR,U_Duration, U_PreparedDate ) END      as U_SDTo,U_SFromTime, U_SToTime 
  from [@AIS_RSC9] Where U_Code=@MCCode
		AND U_PreparedDate<=@FromDate    AND  
		Case When U_Frequency='D' Then  DateAdd(day,U_Duration, U_PreparedDate )    
		When U_Frequency='W' Then  DateAdd(WEEK,U_Duration, U_PreparedDate )
		When U_Frequency='M' Then  DateAdd(MONTH,U_Duration, U_PreparedDate )
		When U_Frequency='Y' Then  DateAdd(YEAR,U_Duration, U_PreparedDate ) END >=@ToDate


end 
Else IF (@U_Type='E')
Begin
Select  Code,U_Code ,U_Frequency,U_Task,U_LastUpdateDate , U_PreparedDate ,



Case When U_Frequency='D' Then  DateAdd(day,U_Duration, U_PreparedDate )    

  When U_Frequency='W' Then  DateAdd(WEEK,U_Duration, U_PreparedDate )

  When U_Frequency='M' Then  DateAdd(MONTH ,U_Duration, U_PreparedDate )

  When U_Frequency='Y' Then  DateAdd(YEAR  ,U_Duration, U_PreparedDate ) END      as U_SDTo,U_SFromTime, U_SToTime from [@AIS_RSC11] Where U_Code=@MCCode
  
  
  		AND U_PreparedDate<=@FromDate    AND  
		Case When U_Frequency='D' Then  DateAdd(day,U_Duration, U_PreparedDate )    
		When U_Frequency='W' Then  DateAdd(WEEK,U_Duration, U_PreparedDate )
		When U_Frequency='M' Then  DateAdd(MONTH,U_Duration, U_PreparedDate )
		When U_Frequency='Y' Then  DateAdd(YEAR,U_Duration, U_PreparedDate ) END >=@ToDate

end 


Else IF (@U_Type='O')
Begin
Select  Code,U_Code ,U_Frequency,U_Task,U_LastUpdateDate , U_PreparedDate ,



Case When U_Frequency='D' Then  DateAdd(day,U_Duration, U_PreparedDate )    

  When U_Frequency='W' Then  DateAdd(WEEK,U_Duration, U_PreparedDate )

  When U_Frequency='M' Then  DateAdd(MONTH ,U_Duration, U_PreparedDate )

  When U_Frequency='Y' Then  DateAdd(YEAR  ,U_Duration, U_PreparedDate ) END      as U_SDTo,U_SFromTime, U_SToTime
   from [@AIS_RSC12] Where U_Code=@MCCode 
   		AND U_PreparedDate<=@FromDate    AND  
		Case When U_Frequency='D' Then  DateAdd(day,U_Duration, U_PreparedDate )    
		When U_Frequency='W' Then  DateAdd(WEEK,U_Duration, U_PreparedDate )
		When U_Frequency='M' Then  DateAdd(MONTH,U_Duration, U_PreparedDate )
		When U_Frequency='Y' Then  DateAdd(YEAR,U_Duration, U_PreparedDate ) END >=@ToDate

end 

end 




