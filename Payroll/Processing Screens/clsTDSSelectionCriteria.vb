﻿Public Class clsTDSSelectionCriteria
    Public Const FormType = "MNU_TDSSC"
    Dim oForm As SAPbouiCOM.Form
    Dim oComboMonthFrom, oComboYear, oComboDeptFrom, oComboMonthTo, oComboDeptTo As SAPbouiCOM.ComboBox
    Dim oRS As SAPbobsCOM.Recordset
    Dim SQL, FID, EID, SMonth, EMonth, SDept, EDept, IntYear As String

    Public Sub LoadScreen()
        oForm = objAddOn.objUIXml.LoadScreenXML("TDSSelectionCriteria.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
        oComboYear = oForm.Items.Item("16").Specific

        oComboMonthFrom = oForm.Items.Item("8").Specific
        oComboMonthTo = oForm.Items.Item("10").Specific

        oComboDeptFrom = oForm.Items.Item("12").Specific
        oComboDeptTo = oForm.Items.Item("14").Specific
        LoadYear()
        LoadMonth()
        LoadDept()
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        If pVal.ItemUID = "1" Then
                            If Validate(FormUID) = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Before Action - Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        If pVal.ItemUID = "1" Then
                            If Validate(FormUID) = True Then

                                oComboMonthFrom = oForm.Items.Item("8").Specific
                                oComboMonthTo = oForm.Items.Item("10").Specific

                                oComboDeptFrom = oForm.Items.Item("12").Specific
                                oComboDeptTo = oForm.Items.Item("14").Specific

                                IntYear = oComboYear.Selected.Value

                                FID = oForm.Items.Item("4").Specific.string
                                EID = oForm.Items.Item("6").Specific.string
                                If oComboMonthFrom.Selected Is Nothing Then
                                    SMonth = ""
                                Else
                                    SMonth = oComboMonthFrom.Selected.Value
                                End If

                                If oComboMonthTo.Selected Is Nothing Then
                                    EMonth = ""
                                Else
                                    EMonth = oComboMonthTo.Selected.Value
                                End If

                                If oComboDeptFrom.Selected Is Nothing Then
                                    SDept = ""
                                Else
                                    SDept = oComboDeptFrom.Selected.Value
                                End If

                                If oComboDeptTo.Selected Is Nothing Then
                                    EDept = ""
                                Else
                                    EDept = oComboDeptTo.Selected.Value
                                End If
                                objAddOn.objConsolidateTaxAmount.LoadScreen(IntYear, FID, EID, SMonth, EMonth, SDept, EDept)
                            End If
                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("After Action - Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        If pVal.ItemUID = "4" Or pVal.ItemUID = "6" Then
                            ChooseItem(FormUID, pVal)
                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("After Action - CFL Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try
            End Select
        End If
    End Sub


#Region " Load Year,Month,Department from the Table"
    Sub LoadYear()
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select Code,Name from [@year] order by Code")
            If oRS.RecordCount > 0 Then
                For Int As Integer = 0 To oRS.RecordCount
                    oComboYear.ValidValues.Add(oRS.Fields.Item("Code").Value, oRS.Fields.Item("Name").Value)
                    'oComboYearTo.ValidValues.Add(oRS.Fields.Item("Code").Value, oRS.Fields.Item("Name").Value)
                    oRS.MoveNext()
                Next
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Year Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
        
    End Sub

    Sub LoadMonth()
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select Code,Name from [@AIS_MONTH] order by Code")
            If oRS.RecordCount > 0 Then
                For Int As Integer = 0 To oRS.RecordCount
                    oComboMonthFrom.ValidValues.Add(oRS.Fields.Item("Code").Value, oRS.Fields.Item("Name").Value)
                    oComboMonthTo.ValidValues.Add(oRS.Fields.Item("Code").Value, oRS.Fields.Item("Name").Value)
                    oRS.MoveNext()
                Next
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Month Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
       
    End Sub

    Sub LoadDept()
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select Code,Name from OUDP ORDER BY CODE")
            If oRS.RecordCount > 0 Then
                For Int As Integer = 0 To oRS.RecordCount
                    oComboDeptFrom.ValidValues.Add(oRS.Fields.Item("Code").Value, oRS.Fields.Item("Name").Value)
                    oComboDeptTo.ValidValues.Add(oRS.Fields.Item("Code").Value, oRS.Fields.Item("Name").Value)
                    oRS.MoveNext()
                Next
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Month Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
        
    End Sub
#End Region

#Region "Choose From List"
    Public Sub ChooseItem(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        Dim objdt As SAPbouiCOM.DataTable
        objcfl = pval
        objdt = objcfl.SelectedObjects
        If objdt Is Nothing Then
        Else
            Try
                Select Case pval.ItemUID
                    Case "4"
                        '  oForm.Items.Item("21").Specific.string = objdt.GetValue("firstName", 0) + " " + objdt.GetValue("lastName", 0)
                        oForm.Items.Item("4").Specific.string = objdt.GetValue("empID", 0)
                    Case "6"
                        '  oForm.Items.Item("22").Specific.string = objdt.GetValue("firstName", 0) + " " + objdt.GetValue("lastName", 0)
                        oForm.Items.Item("6").Specific.string = objdt.GetValue("empID", 0)
                End Select
            Catch ex As Exception

            End Try
        End If
    End Sub
#End Region

#Region "Validate Event"
    Public Function Validate(ByVal FormUID As String) As Boolean
        oForm = objAddOn.objApplication.Forms.Item(FormUID)
        If oForm.Items.Item("16").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select Year", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf (oForm.Items.Item("4").Specific.value > oForm.Items.Item("6").Specific.value) Then
            objAddOn.objApplication.SetStatusBarMessage("From Employee ID Should not Greater than To Employee ID", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

End Class
