﻿Public Class clsConsolidateTaxAmount
    Public Const FormType = "MNU_CDDTA"
    Dim oForm As SAPbouiCOM.Form
    Dim oGrid As SAPbouiCOM.Grid
    Dim oRS As SAPbobsCOM.Recordset
    Dim SQL As String
    Dim oGridCol As SAPbouiCOM.GridColumn

    Public Sub LoadScreen(ByVal Year As String, ByVal SID As String, ByVal EID As String, ByVal SMon As String, ByVal EMon As String, ByVal SDept As String, ByVal EDpet As String)
        oForm = objAddOn.objUIXml.LoadScreenXML("ConsolidatedDrillDownTaxAmount.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
        oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oGrid = oForm.Items.Item("3").Specific
        oGrid.DataTable = oForm.DataSources.DataTables.Add("3")
        SQL = "declare @Year varchar(4) set @Year='" & Year & "' ;with a as (select convert(date,cast((cast(@year as int)-1) as varchar(4))+'0401') D," & _
       "  1 [Num] union all select DATEADD(month,1,D),Num+1 from a where D<CONVERT(date,@year+'0301') )select c.ID ,c.[EMP Name] ,DATENAME(MONTH,D) [Month]" & _
        " ,cast((cast(@year as int)-1) as varchar(4))[From Year ],@Year [To Year],c.TaxAmount from a ,(select T0.empID [ID] " & _
        ",ISNULL(T0.firstName ,'')+ ' '+ISNULL(T0.middleName ,'')+' ' +ISNULL(T0.lastName ,'') [EMP Name] ,T2.U_ttds/12 [TaxAmount],T0.dept [Dept] " & _
        "from OHEM T0 left join [@AIS_EMPSSTP] T1 on T0.empID =T1.U_eid left join [@AIS_EPSSTP11] T2 on T1.Code =T2.Code) C where 1=1 "

        If SID.ToString <> "" Then
            SQL += vbCrLf + " and c.ID>='" & SID.ToString & "'"
        End If

        If EID.ToString <> "" Then
            SQL += vbCrLf + " and c.ID<='" & EID.ToString & "'"
        End If

        If SMon.ToString <> "" Then
            SQL += vbCrLf + " and Num>='" & SMon.ToString & "'"
        End If
        If EMon.ToString <> "" Then
            SQL += vbCrLf + " and Num<='" & EMon.ToString & "'"
        End If
        If SDept.ToString <> "" Then
            SQL += vbCrLf + " and c.Dept='" & SDept.ToString & "'"
        End If
        If EDpet.ToString <> "" Then
            SQL += vbCrLf + " and c.Dept='" & EDpet.ToString & "'"
        End If

        SQL += " order by c.[EMP Name] "

        oGrid.DataTable.ExecuteQuery(SQL)
        oGrid.CollapseLevel = 1
        'oGridCol = oGrid.Columns.Item(oGrid.Columns.Count)
        'oGridCol.Visible = False

    End Sub
End Class
