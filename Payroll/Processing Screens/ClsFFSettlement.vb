﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Sql

Public Class ClsFFSettlement
    Public Const FormType = "MNU_OFFS"
    Dim oForm As SAPbouiCOM.Form
    Dim oRS As SAPbobsCOM.Recordset
    Dim SQL, StrMonth, EID, strsql, last, AcctCode, JDate As String
    Dim IMonth, IYear As Integer
    Dim Count As Integer = 0
    Dim B As Integer = 0
    Dim Amount, GAmount, GYear As Double
    Dim FrmDate, ToDate As String

    Public Sub LoadScreen()
        oForm = objAddOn.objUIXml.LoadScreenXML("FFSettlement.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        oForm.Items.Item("41").Specific.value = oForm.BusinessObject.GetNextSerialNumber("-1", "OFFS")
        oForm.DataBrowser.BrowseBy = 41
        objAddOn.objPaySlip.SAPassWord()
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        If pVal.ItemUID = "1" And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            If Validation(FormUID) = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Before Action - Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    End Try
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        ChooseItem(FormUID, pVal)
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("CFL Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        If pVal.ItemUID = "11" Then
                            If Validation(FormUID) = False Then
                                BubbleEvent = False
                                Exit Sub
                            Else
                                oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRS.DoQuery("SELECT DATENAME(month, '" & CDate(oForm.Items.Item("8").Specific.string).ToString("yyyyMMdd") & "') AS [Month Name]")
                                StrMonth = oRS.Fields.Item("Month Name").Value
                                EID = oForm.Items.Item("10").Specific.string
                                IMonth = CDate(oForm.Items.Item("8").Specific.string).ToString("yyyyMMdd")

                                'oRS.DoQuery("select * from [@AIS_OSPS] where U_month =" & Mid(IMonth, 5, 2) - 1 & " and U_year =" & Left(IMonth, 4) & " and U_empid ='" & EID & "'")
                                'If oRS.RecordCount > 0 Then
                                '    Count = Count + 1
                                '    SalaryCalculation(FormUID, Mid(IMonth, 5, 2), Left(IMonth, 4), StrMonth, EID)
                                'Else
                                '    SalaryCalculation(FormUID, Mid(IMonth, 5, 2) - 1, Left(IMonth, 4), StrMonth, EID)
                                'End If

                                'If Count = 0 Then
                                '    SalaryCalculation(FormUID, Mid(IMonth, 5, 2), Left(IMonth, 4), StrMonth, EID)
                                'End If
                                'FrmDate = oForm.Items.Item("6").Specific.value
                                'ToDate = oForm.Items.Item("8").Specific.value

                                'oForm.Items.Item("26").Specific.value = GratuityAmount(oForm.Items.Item("10").Specific.string, FrmDate, ToDate)

                                oRS.DoQuery("select SUM(convert(numeric(19,2),U_amount)) [Salary] from [@AIS_OSPS] where isnull(U_pay,'N')='N' and U_heads ='Net Earnings'  and U_empid ='" & oForm.Items.Item("10").Specific.string & "' and U_heads not in ('Pay','Remarks')")

                                oForm.Items.Item("14").Specific.value = oRS.Fields.Item("Salary").Value

                                oRS.DoQuery("select SUM(convert(numeric(19,2),U_amount)) [Salary] from [@AIS_OSPS] where isnull(U_pay,'N')='N' and U_heads ='Net Deductions'  and U_empid ='" & oForm.Items.Item("10").Specific.string & "' and U_heads not in ('Pay','Remarks')")

                                oForm.Items.Item("22").Specific.value = oRS.Fields.Item("Salary").Value

                                oForm.Items.Item("26").Specific.value = GratuityAmount(oForm.Items.Item("10").Specific.string, CDate(oForm.Items.Item("6").Specific.string).ToString("yyyyMMdd"), CDate(oForm.Items.Item("8").Specific.string).ToString("yyyyMMdd"))

                                oRS.DoQuery("select isnull(SUM(T1.U_amtpaid ),0) [Reimb] from [@AIS_ORED] T0 left join [@AIS_RED1] T1 on T0.DocEntry =T1.DocEntry where U_ecode ='" & oForm.Items.Item("10").Specific.string & "' and isnull(U_pay ,'N')='N'")

                                oForm.Items.Item("16").Specific.value = oRS.Fields.Item("Reimb").Value

                                Calculation(FormUID)

                                ' CalculationforSalary(FormUID, EID, Mid(IMonth, 5, 2), Left(IMonth, 4))
                            End If

                            '----------------------Create Journal Voucher Function calling -----------------------------------------------------
                        ElseIf pVal.ItemUID = "42" And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            EID = oForm.Items.Item("10").Specific.string
                            oRS.DoQuery("select  BatchNum,BtfStatus from obtf where BtfStatus  ='O' and Memo ='" & EID & " - Full and Final Settlement" & "'")

                            If oRS.RecordCount > 0 Then
                                objAddOn.objApplication.StatusBar.SetText("Journal Vourcher Already Created. JV Number : " & oRS.Fields.Item("BatchNum").Value & " The status is : " & oRS.Fields.Item("BtfStatus").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            Else
                                oRS.DoQuery("select AcctCode [CreditAccount] from OACT where FormatCode ='" & oForm.Items.Item("35").Specific.string & "'")
                                AcctCode = oRS.Fields.Item("CreditAccount").Value
                                Amount = oForm.Items.Item("30").Specific.value
                                JDate = oForm.Items.Item("8").Specific.string

                                oRS.DoQuery("select AcctCode [DebitAccount] from OACT where FormatCode =(select U_cspac  from [@AIS_GLAC] where U_brnch =(select branch from OHEM where empID ='" & oForm.Items.Item("10").Specific.string & "'))")

                                JournalVoucher(AcctCode, EID, Amount, JDate, oRS.Fields.Item("DebitAccount").Value)
                            End If

                            oRS.DoQuery("select TransId,Number  from ojdt where Memo ='" & EID & " - Full and Final Settlement'")

                            If oRS.RecordCount > 0 Then
                                objAddOn.objApplication.StatusBar.SetText("Already Created Journal Entry : " & oRS.Fields.Item("Number").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            Else
                                oRS.DoQuery("select AcctCode from OACT where FormatCode in(select U_ffsc from [@AIS_GLAC]) or FormatCode in(select U_cspac  from [@AIS_GLAC])")

                                Dim Debit, Credit As String

                                oRS.MoveFirst()
                                Credit = oRS.Fields.Item("AcctCode").Value
                                oRS.MoveLast()
                                Debit = oRS.Fields.Item("AcctCode").Value

                                JournalEntry(Credit, EID, oForm.Items.Item("32").Specific.value, oForm.Items.Item("8").Specific.string, Debit)
                            End If

                        ElseIf pVal.ItemUID = "40" Then
                            Dim EID As String
                            Dim Reimbursement, Encash, Gratuity, otherAdd, OtherDed, SettlementAmt As Double

                            EID = oForm.Items.Item("10").Specific.string
                            If EID <> "" Then
                                Reimbursement = oForm.Items.Item("16").Specific.value
                                Encash = oForm.Items.Item("18").Specific.value
                                Gratuity = oForm.Items.Item("26").Specific.value
                                otherAdd = oForm.Items.Item("20").Specific.value
                                OtherDed = oForm.Items.Item("24").Specific.value
                                SettlementAmt = oForm.Items.Item("30").Specific.value
                                GenerateReport(FormUID, EID, Encash, Gratuity, otherAdd, OtherDed, Reimbursement, SettlementAmt)
                            End If

                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("After Action Click Event Failure  - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    Try
                        If pVal.ItemUID = "10" And oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            CheckFromDB(FormUID, oForm.Items.Item("10").Specific.string)
                        ElseIf (pVal.ItemUID = "20" Or pVal.ItemUID = "24" Or pVal.ItemUID = "30") And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            Calculation(FormUID)
                        End If

                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("After Action Lost Focus Event Failure  - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    End Try
            End Select
        End If
    End Sub

#Region " Calculate Gratuity Amount"
    Function GratuityAmount(ByVal EmpID As String, ByVal StDate As String, ByVal EdDate As String) As String
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            SQL = "select datediff(MONTH ,'" & StDate & "','" & EdDate & "')+1 [Monthk]"
            oRS.DoQuery(SQL)
            GYear = oRS.Fields.Item("Monthk").Value / 12
            If GYear > 5 Then

                SQL = "select (isnull(SUM(U_amt),0)/2)*round(" & GYear & ",0)[Amount] from [@AIS_EPSSTP1] where U_descrpn in('Basic','DA') and Code = " & _
                    " (select Code from [@AIS_EMPSSTP] where U_eid ='" & EmpID & "') "
                oRS.DoQuery(SQL)
                GAmount = oRS.Fields.Item("Amount").Value
                Return oRS.Fields.Item("Amount").Value
            End If
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Gratuity Calculation Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return 0
        End Try
    End Function
#End Region

#Region "Create Journal Voucher for the Employee"
    Sub JournalVoucher(ByVal AcctCode As String, ByVal EID As String, ByVal Amount As Double, ByVal Jdate As String, ByVal DebAcct As String)
        Try
            Dim lRetCode As Integer
            Dim oJV As SAPbobsCOM.JournalVouchers
            oJV = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalVouchers)
            oJV.JournalEntries.DueDate = Jdate
            oJV.JournalEntries.TaxDate = Jdate
            oJV.JournalEntries.ReferenceDate = Jdate
            oJV.JournalEntries.Memo = EID & " - Full and Final Settlement"

            oJV.JournalEntries.SetCurrentLine(0)
            oJV.JournalEntries.Lines.AccountCode = DebAcct ' "5631005PL10000" 
            oJV.JournalEntries.Lines.Debit = Amount
            oJV.JournalEntries.Lines.Add()

            oJV.JournalEntries.Lines.AccountCode = AcctCode ' "5631005PL10000"
            oJV.JournalEntries.Lines.Credit = Amount
            oJV.JournalEntries.Lines.Add()
            lRetCode = oJV.Add()
            If lRetCode <> 0 Then
                objAddOn.objApplication.SetStatusBarMessage("Journal Voucher Not Created - " & objAddOn.objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Else
                objAddOn.objApplication.StatusBar.SetText("Journal Vourcher Created Successfully.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("JournalVoucher Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub

    Sub JournalEntry(ByVal AcctCode As String, ByVal EID As String, ByVal Amount As Double, ByVal Jdate As String, ByVal DebAcct As String)
        Try
            Dim lRetCode As Integer
            Dim oJV As SAPbobsCOM.JournalEntries
            oJV = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oJournalEntries)
            oJV.DueDate = Jdate
            oJV.TaxDate = Jdate
            oJV.ReferenceDate = Jdate
            oJV.Memo = EID & " - Full and Final Settlement"

            oJV.SetCurrentLine(0)
            oJV.Lines.AccountCode = DebAcct ' "5631005PL10000" 
            oJV.Lines.Debit = Amount
            oJV.Lines.Add()

            oJV.Lines.AccountCode = AcctCode ' "5631005PL10000"
            oJV.Lines.Credit = Amount
            oJV.Lines.Add()
            lRetCode = oJV.Add()
            If lRetCode <> 0 Then
                objAddOn.objApplication.SetStatusBarMessage("Journal Entry Created Failure - " & objAddOn.objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Else
                objAddOn.objApplication.StatusBar.SetText("Journal Entry Created Successfully.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Journal Entry Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
#End Region

#Region "Calulate the Salary,Reimbursement,..... from the Table"
    Public Sub SalaryCalculation(ByVal FormUID As String, ByVal Month As Integer, ByVal Year As Integer, ByVal StrMonth As String, ByVal EID As String)
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            last = objAddOn.objSalaryprocess.get_last_date(Year, StrMonth)
            objAddOn.objSalaryprocess.PayCodeColumns(Month, Year, last)
            objAddOn.objSalaryprocess.BenefitCodeColumns()
            objAddOn.objSalaryprocess.DeductionCodeColumns(Month, Year, objAddOn.objSalaryprocess.get_last_date(Year, StrMonth))
            objAddOn.objSalaryprocess.ReimbursementColumns()
            objAddOn.objSalaryprocess.LoanandAdvancesColumns()
            LoadMatrix(Month, Year, last, EID)

            ' SQL = "select "
            ' Calculation(FormUID)
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Salary Calculation Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub

#End Region

#Region "Form a Query Group and Load the Grid Values"
    Public Sub LoadMatrix(ByVal Month As String, ByVal year As Integer, ByVal last As String, ByVal EID As String)
        'This is for Earnings heads and totals:

        strsql = "select * ,x.Arrears +x.[Over Time] +"
        For PInt As Integer = 0 To PayHead.Length - 1
            If PayHead(PInt) = "" Then
            Else
                strsql += "X.[" & PayHead(PInt) & "]+"
            End If
        Next

        For BInt As Integer = 0 To BenHeads.Length - 1
            If BenHeads(BInt) = "" Then
            Else
                strsql += "X.[" & BenHeads(BInt) & "]+"
            End If
        Next

        For IntR As Integer = 0 To ReimbHeads.Length - 1
            If ReimbHeads(IntR) = "" Then
            Else
                strsql += "X.[" & ReimbHeads(IntR) & "]+"
            End If
        Next
        strsql = strsql.Substring(0, strsql.Length - 1)
        strsql += " [Net Earnings],"

        'This is for Net Duductions Total:
        For Dint As Integer = 0 To DedHeads.Length - 1
            If DedHeads(Dint) = "" Then
            Else
                strsql += "X.[" & DedHeads(Dint) & "]+"
            End If
        Next

        For Lint As Integer = 0 To LoanHeads.Length - 1
            If LoanHeads(Lint) = "" Then
            Else
                strsql += "X.[" & LoanHeads(Lint) & "]+"
            End If
        Next

        strsql += "0 [Net Deductions], "

        'This is for Net Pay Total:- For Additions:
        strsql += "round((x.Arrears +x.[Over Time] +"
        For PInt As Integer = 0 To PayHead.Length - 1
            If PayHead(PInt) = "" Then
            Else
                strsql += "X.[" & PayHead(PInt) & "]+"
            End If
        Next

        For BInt As Integer = 0 To BenHeads.Length - 1
            If BenHeads(BInt) = "" Then
            Else
                strsql += "X.[" & BenHeads(BInt) & "]+"
            End If
        Next
        For IntR As Integer = 0 To ReimbHeads.Length - 1
            If ReimbHeads(IntR) = "" Then
            Else
                strsql += "X.[" & ReimbHeads(IntR) & "]+"
            End If
        Next
        strsql = strsql.Substring(0, strsql.Length - 1)

        strsql += ")-("
        'This for Net Pay Total Deductions:

        For Dint As Integer = 0 To DedHeads.Length - 1
            If DedHeads(Dint) = "" Then
            Else
                strsql += "X.[" & DedHeads(Dint) & "]+"
            End If
        Next

        For Lint As Integer = 0 To LoanHeads.Length - 1
            If LoanHeads(Lint) = "" Then
            Else
                strsql += "X.[" & LoanHeads(Lint) & "]+"
            End If
        Next

        strsql += "0),0) [Net Pay],convert(varchar(2),'') as 'Pay',convert(varchar(253),'') as 'Remarks',convert(Numeric(3),'" & Month & "') as 'Month',convert(Numeric(6),'" & year & "') as 'Year' "


        'This is for calculating amounts based on theier Attendance:
        strsql += " from (select distinct t0.empID [Emp ID],isnull(t0.firstName,'')+' '+isnull(t0.middleName,'')+' '+isnull(t0.lastName,'') as [Employee Name],'" & last & "' as [Total Days] ,"
        strsql += "(select sum(T4.U_payday)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & Month & "' and T4 .U_year ='" & year & "' ) as [Payable Days],"
        strsql += "(select sum(T4.U_lop)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & Month & "' and T4 .U_year ='" & year & "' ) as 'LOP', "
        'strsql += "(select sum(U_amt) from [@AIS_EPSSTP7] where Code=t1.Code) as 'Loan',"
        strsql += "(select sum(U_htotal)from [@AIS_EPSSTP4] where Code=t1.Code) as 'Arrears'"
        strsql += ",isnull((select U_amount from [@AIS_OVERT] where U_Code=t1.U_eid and U_month =" & Month & " and U_year ='" & year & "'),0) as 'Over Time'"
        '  MsgBox(PayCode.Length)
        For IntP As Integer = 0 To PayCode.Length - 1
            strsql += PayCode(IntP) & ""
        Next

        For IntB As Integer = 0 To Benefits.Length - 1
            strsql += Benefits(IntB)
        Next

        For IntD As Integer = 0 To DedCode.Length - 1
            strsql += DedCode(IntD)
        Next

        For IntR As Integer = 0 To ReimbCode.Length - 1
            strsql += ReimbCode(IntR)
        Next

        For IntL As Integer = 0 To LoanCode.Length - 1
            strsql += LoanCode(IntL)
        Next


        strsql += "from [@AIS_EMPSSTP] t1 left outer join OHEM t0 on t0.empID = t1.U_eid  where T0.active='Y' and t1.U_eid='" & EID & "' ) X"

        oRS.DoQuery(strsql)
        B = B + 1
        'For IntI As Integer = 1 To oRS.RecordCount
        SQL = "insert into [@AIS_SALARY](Code,Name,U_eid,U_ename,U_ded,U_net,U_month,U_year) select '" & B & "','" & B & "','" & oRS.Fields.Item("Emp ID").Value & "','" & oRS.Fields.Item("Employee Name").Value & "', " & _
            " '" & oRS.Fields.Item("Net Deductions").Value & "','" & oRS.Fields.Item("Net Earnings").Value & "'," & _
            "'" & oRS.Fields.Item("Month").Value & "','" & oRS.Fields.Item("Year").Value & "'"

        oRS.DoQuery(SQL)


    End Sub
#End Region

#Region "Calculate salary,Reimbursement and deductions"
    Sub CalculationforSalary(ByVal FormUID As String, ByVal EID As String, ByVal Month As Integer, ByVal Year As Integer)
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select sum(U_ded) [Deduction] ,sum(U_net)[Net Pay],(select sum(U_amtpaid) from [@AIS_ORED] T0 join [@AIS_RED1] " & _
                      "  T1 on T0.DocEntry =T1.DocEntry where T0.U_month ='" & Month & "' and T0.U_year ='" & Year & "' and T0.U_ecode ='" & EID & "' " & _
                      " and T1.U_pay ='Y' ) [Reimbursement] from [@AIS_SALARY] where U_eid='" & EID & "' and U_month =" & Month & " and U_year=" & Year & "")

            oForm.Items.Item("22").Specific.value = oRS.Fields.Item("Deduction").Value
            oForm.Items.Item("14").Specific.value = oRS.Fields.Item("Net Pay").Value
            oForm.Items.Item("16").Specific.value = oRS.Fields.Item("Reimbursement").Value

            Calculation(FormUID)
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("CalculationforSalary Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
#End Region

#Region "Calculate the values and enter the current values"
    Public Sub Calculation(ByVal FormUID As String)
        Try
            Dim Salary, Reimbursement, Leave, OAdd, Oded, Ded, Graruity As Decimal
            Salary = oForm.Items.Item("14").Specific.value
            Reimbursement = oForm.Items.Item("16").Specific.value
            Leave = oForm.Items.Item("18").Specific.value
            OAdd = oForm.Items.Item("20").Specific.value
            Oded = oForm.Items.Item("24").Specific.value
            Ded = oForm.Items.Item("22").Specific.value
            Graruity = oForm.Items.Item("26").Specific.value

            oForm.Items.Item("28").Specific.value = (Salary + Reimbursement + Leave + OAdd + Graruity) - (Oded + Ded)
            oForm.Items.Item("32").Specific.value = oForm.Items.Item("28").Specific.value - oForm.Items.Item("30").Specific.value

            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("Delete from  [@AIS_SALARY]")

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Calculation Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub
#End Region

#Region "Choose From List Event for Select EMPID and Account Code"
    Public Sub ChooseItem(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        Dim objdt As SAPbouiCOM.DataTable
        objcfl = pval
        objdt = objcfl.SelectedObjects
        If objdt Is Nothing Then
        Else
            Try
                Select Case pval.ItemUID
                    Case "10"

                        '  MsgBox(String.Format(objAddOn.objGenFunc.GetSBODateString(objdt.GetValue("startDate", 0)), "dd/MMM/yyyy"))
                        'oForm.Items.Item("6").Specific.string = String.Format(objAddOn.objGenFunc.GetSBODateString(objdt.GetValue("startDate", 0)), "dd/MMM/yyyy")
                        oForm.Items.Item("6").Specific.string = objdt.GetValue("startDate", 0)
                        oForm.Items.Item("4").Specific.value = objdt.GetValue("firstName", 0) + " " + objdt.GetValue("middleName", 0) + " " + objdt.GetValue("lastName", 0)
                        oForm.Items.Item("10").Specific.Value = objdt.GetValue("empID", 0)
                    Case "35"
                        oForm.Items.Item("35").Specific.Value = objdt.GetValue("FormatCode", 0)
                End Select
            Catch ex As Exception

            End Try

        End If
    End Sub
#End Region

#Region "Validation for Empty Document should not to be Add"
    Public Function Validation(ByVal FormUID) As Boolean
        Try
            If oForm.Items.Item("10").Specific.Value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select Employee Code", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            ElseIf oForm.Items.Item("4").Specific.Value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Enter the Employee Name ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            ElseIf oForm.Items.Item("6").Specific.Value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Enter the Start Date in Employee Master", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            ElseIf oForm.Items.Item("8").Specific.Value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Enter the End Date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
            Return True
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Vaidation Funtion Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)

        End Try

    End Function
#End Region

#Region "Check from the DB If Already Exists"
    Public Sub CheckFromDB(ByVal FromUID As String, ByVal EID As String)
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select DocEntry from [@AIS_OFFS] where U_eid='" & EID & "'")
            If oRS.RecordCount > 0 Then
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                oForm.Items.Item("41").Specific.value = oRS.Fields.Item("DocEntry").Value
                oForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                'Else
                '    oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                '    oForm.Items.Item("41").Specific.value = oForm.BusinessObject.GetNextSerialNumber("-1", "OFFS")
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Check From DB Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub
#End Region

#Region "Add and Find Mode Functions"
    Public Sub AddMode()
        oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        oForm.Items.Item("41").Specific.value = oForm.BusinessObject.GetNextSerialNumber("-1", "OFFS")
    End Sub

    Public Sub FindMode()
        oForm.Items.Item("4").Enabled = True
        oForm.Items.Item("6").Enabled = -True
        oForm.ActiveItem = 10
    End Sub
#End Region

#Region " Generate Full and Final Settlement Report"
    Public Sub GenerateReport(ByVal FormUID As String, ByVal EmpID As String, ByVal EnCash As Double, ByVal Gratuity As Double, _
                              ByVal OtherAdd As Double, ByVal OtherDed As Double, ByVal Reimb As Double, ByVal Settlement As Double)

        Dim sender As System.Object
        Dim e As System.EventArgs
        sender = ""
        e = Nothing
        Dim RptFrm As FrmRpt
        RptFrm = New FrmRpt
        RptFrm.Refresh()
        Dim cryRpt As New ReportDocument
        Dim objConInfo As New CrystalDecisions.Shared.ConnectionInfo
        Dim oLogonInfo As New CrystalDecisions.Shared.TableLogOnInfo
        Dim intCounter As Integer

        cryRpt.Load(System.Windows.Forms.Application.StartupPath & "\Reports\FFSettement.rpt")

        Dim crParameterValues As New ParameterValues
        Dim crParameterDiscreteValue As New ParameterDiscreteValue

        Dim SerNam, DbName, UID As String
        SerNam = objAddOn.objCompany.Server
        DbName = objAddOn.objCompany.CompanyDB
        UID = objAddOn.objCompany.DbUserName


        oLogonInfo.ConnectionInfo.ServerName = SerNam
        oLogonInfo.ConnectionInfo.DatabaseName = DbName
        oLogonInfo.ConnectionInfo.UserID = UID
        oLogonInfo.ConnectionInfo.Password = SAPwd '"AIS@1234"
        ' oLogonInfo.ConnectionInfo.Password = "Pass@123"

        For intCounter = 0 To cryRpt.Database.Tables.Count - 1
            cryRpt.Database.Tables(intCounter).ApplyLogOnInfo(oLogonInfo)
        Next

        cryRpt.SetParameterValue("EmpID@", EmpID)
        cryRpt.SetParameterValue("Encash@", EnCash)
        cryRpt.SetParameterValue("Gratuity@", Gratuity)
        cryRpt.SetParameterValue("OtherAdd@", OtherAdd)
        cryRpt.SetParameterValue("OtherDed@", OtherDed)
        cryRpt.SetParameterValue("Reimb@", Reimb)
        cryRpt.SetParameterValue("Settlement@", Settlement)

        RptFrm.CRViewer.ReportSource = Nothing
        RptFrm.CRViewer.ReportSource = cryRpt
        ' RptFrm.CRViewer.Refresh()
        System.Windows.Forms.Application.Run(RptFrm)

    End Sub
#End Region

End Class
