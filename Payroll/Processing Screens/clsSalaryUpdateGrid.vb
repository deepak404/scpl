﻿Public Class clsSalaryUpdateGrid
    Public Const FormType = "MNU_SUGRD"
    Dim oForm As SAPbouiCOM.Form
    Dim oGrid As SAPbouiCOM.Grid
    Dim Query, Pay As String
    Dim oRs As SAPbobsCOM.Recordset
    Dim FinYear As String
    Dim oGridColumn As SAPbouiCOM.EditTextColumn
    Dim Month, SelectYear As String

    Public Sub LoadScreen(ByVal MonthName As String, ByVal IntMonth As String, ByVal Year As String)
        Try
            oForm = objAddOn.objUIXml.LoadScreenXML("SalaryUpdateGrid.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            ' oForm.Freeze(True)
            FinYear = FindFinancialYear(Year & IntMonth.ToString & "01", Year & IntMonth.ToString & objAddOn.objSalaryprocess.get_last_date(Year, MonthName))
            Month = IntMonth
            SelectYear = Year
            oGrid = oForm.Items.Item("3").Specific
            oForm.DataSources.DataTables.Add("DT")

            oGrid.DataTable = oForm.DataSources.DataTables.Item("DT")
            Query = "select x.EmpID ,x.[Employee Name] ,sum(x.Earnings)[Ear] ,sum(x.Deduction )[Ded],sum(x.[Net Pay]) [Net Pay] ,''[Pay] from  ( " & _
                    " select U_empid [EmpID],U_ename [Employee Name],U_amount [Earnings],0 [Deduction],0 [Net Pay]  from [@AIS_OSPS] " & _
                    " where U_month ='" & IntMonth & "' and U_year ='" & Year & "' and U_fyear ='" & FinYear & "' and U_heads ='Net Earnings' and ISNULL(U_pay,'N')='N' " & _
                    " union all " & _
                    " select U_empid [EmpID],U_ename [Employee Name],0,U_amount [Deduction] ,0 from [@AIS_OSPS] " & _
                    " where U_month ='" & IntMonth & "' and U_year ='" & Year & "' and U_fyear ='" & FinYear & "' and U_heads ='Net Deductions' and ISNULL(U_pay,'N')='N' " & _
                    " union all " & _
                    " select U_empid [EmpID],U_ename [Employee Name],0,0,U_amount [Net Pay]  from [@AIS_OSPS]  " & _
                    " where U_month ='" & IntMonth & "' and U_year ='" & Year & "' and U_fyear ='" & FinYear & "' and U_heads ='Net Pay 'and ISNULL(U_pay,'N')='N' " & _
                    " ) x group by x.EmpID ,x.[Employee Name]  order by cast(x.EmpID  as int)"
            oGrid.DataTable.ExecuteQuery(Query)

            oGrid.Columns.Item(oGrid.Columns.Count - 1).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox
            oGrid.Columns.Item("Pay").Editable = True

            For i As Integer = 0 To oGrid.Columns.Count - 2
                oGridColumn = oGrid.Columns.Item(i)
                oGridColumn.Editable = False
            Next
            '  oForm.Freeze(False)
        Catch ex As Exception
            oForm.Freeze(False)
        End Try
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "1" Then
                            Pay = ""
                            Pay = "("
                            For IntI As Integer = 0 To oGrid.Rows.Count - 1
                                If oGrid.DataTable.GetValue("Pay", IntI) = "Y" Then
                                    Pay += " '" & oGrid.DataTable.GetValue("EmpID", IntI) & "',"
                                End If
                            Next
                            Pay = Pay + "'')"

                            If Pay = "('')" Then
                                objAddOn.objApplication.StatusBar.SetText("Please select atleast one value ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                                BubbleEvent = False
                                Exit Sub
                            Else
                                Query = "update [@AIS_OSPS] set U_pay ='Y' where U_fyear ='" & FinYear & "' and U_month ='" & Month & "' and " & _
                                        " U_year ='" & SelectYear & "' and U_empid in  " & Pay & " "
                                oRs.DoQuery(Query)

                                objAddOn.objApplication.StatusBar.SetText("Process Updated Successfully ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            End If
                        End If
                End Select
            Else
                Select Case pVal.EventType

                End Select
            End If
        Catch ex As Exception

        End Try
    End Sub



#Region "Get Financial Year for Salary Processing"

    Function FindFinancialYear(ByVal FDate As String, ByVal TDate As String) As String
        Try
            Dim strsql As String
            oRs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strsql = "select distinct Category from OFPR where F_RefDate >='" & FDate & "' and T_RefDate<='" & TDate & "'"
            oRs.DoQuery(strsql)
            Return oRs.Fields.Item("Category").Value
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Get Financial Year Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
        Return ""
    End Function
#End Region
End Class
