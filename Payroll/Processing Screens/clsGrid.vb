﻿Public Class clsGrid
    Public Const formtype = "MNU_GRD"
    Dim objForm As SAPbouiCOM.Form
    Dim SDate, EDate As String
    Dim Ename, CaptionName, Eid As String
    Dim oGrid As SAPbouiCOM.Grid
    Dim rowsGrid As Integer
    Public Sub LoadScreen(ByVal month As String, ByVal intj As String, ByVal Year As String)
        objForm = objAddOn.objUIXml.LoadScreenXML("Grid.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.State = SAPbouiCOM.BoFormStateEnum.fs_Maximized
        oGrid = objForm.Items.Item("3").Specific
        oGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single
        objForm.SupportedModes = 1
    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            objForm = objAddOn.objApplication.Forms.Item(FormUID)
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
                    oGrid = objForm.Items.Item("3").Specific
                    If pVal.ItemUID = "3" And pVal.ColUID <> "EmpID" Then
                        Ename = oGrid.DataTable.GetValue("EmpName", pVal.Row)
                        Eid = oGrid.DataTable.GetValue("EmpID", pVal.Row)
                        CaptionName = oGrid.Columns.Item(pVal.ColUID).TitleObject.Caption.ToString
                        rowsGrid = pVal.Row
                        objAddOn.objInOutTime.LoadScreen(Ename, CaptionName, Eid)
                        BubbleEvent = False
                        Exit Sub
                    ElseIf pVal.ItemUID = "3" And pVal.ColUID = "EmpID" Then
                        rowsGrid = pVal.Row
                    End If
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Dim i As Integer
                    If pVal.ItemUID = "4" Then
                        Dim FindString As String
                        oGrid = objForm.Items.Item("3").Specific
                        FindString = objForm.Items.Item("5").Specific.string
                        For i = 0 To oGrid.DataTable.Rows.Count - 1
                            If FindString = oGrid.DataTable.Columns.Item("EmpID").Cells.Item(i).Value Then
                                oGrid.Rows.SelectedRows.Add(i)
                                Exit For
                            End If
                        Next
                    End If
            End Select
        End If
    End Sub

#Region "Modify the Attendance in Grid"
    Public Sub Update(ByVal value1 As String)
        oGrid = objForm.Items.Item("3").Specific
        oGrid.DataTable.SetValue("Name", rowsGrid, value1)
    End Sub
    Public Sub UpdateStatus(ByVal value2 As String)
        oGrid.DataTable.SetValue(CaptionName, rowsGrid, value2)
    End Sub
#End Region

#Region "Get Last Date Function"
    Public Function get_last_date(ByVal year As Integer, ByVal mntname As String)
        Dim lastdate As Integer = DatePart(DateInterval.Day, DateSerial(year, Month(CDate(mntname + "1,1990")) + 1, 0))
        Return lastdate
    End Function
#End Region

#Region "Load Grid"
    Public Sub LoadGrid(ByVal SName As String, ByVal EName As String, ByVal SDept As String, ByVal EDept As String, ByVal SBranch As String, ByVal EBranch As String, ByVal month As String, ByVal intj As String, ByVal Year As String)
        Dim gridEditCol As SAPbouiCOM.EditTextColumn
        Dim strSQL As String
        Dim Rs As SAPbobsCOM.Recordset
        Dim a As String
        a = "01" + "/" + intj.ToString + "/" + Year.ToString
        Dim b As String = get_last_date(Year, month)
        strSQL = "select DATEDIFF(dd,'" & Year.ToString + "/" + intj.ToString + "/" + "01" & "','" & Year.ToString + "/" + intj.ToString + "/" + b & "') +1'Day'"

        'strSQL = "select DATEDIFF (dd,convert(date,'" & a & "',108),convert(date,getdate(),108))+1"

        Rs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Rs.DoQuery(strSQL)
        'c = CDate(Rs.Fields.Item("Day").Value.ToString + "-" + intj.ToString + "-" + Year.ToString)

        Dim oGrid As SAPbouiCOM.Grid
        Dim i As Integer
        Dim Str As String
        oGrid = objForm.Items.Item("3").Specific
        oGrid.DataTable = objForm.DataSources.DataTables.Add("3")
        Str = "select empID as EmpID,firstName+' '+lastName as EmpName,dept as Dept,branch as Branch"
        For i = Left(a, 2) To Rs.Fields.Item("Day").Value.ToString
            If Len(i.ToString) = 1 Then
                Str += vbCrLf + ",isnull((select U_attd from [@AIS_INOUTTIME]where U_eid=EmpID and convert(date,U_date)='" & Year.ToString + intj.ToString + "0" + i.ToString & "'),'P                ') '" & i.ToString + "-" + intj.ToString + "-" + Year.ToString & "'"
            Else
                Str += vbCrLf + ",isnull((select U_attd from [@AIS_INOUTTIME]where U_eid=EmpID and convert(date,U_date)='" & Year.ToString + intj.ToString + i.ToString & "'),'P                ') '" & i.ToString + "-" + intj.ToString + "-" + Year.ToString & "'"
            End If
        Next
        Str += vbCrLf + "  from OHEM where active='Y' "
        If SName.ToString <> "" Then
            Str += vbCrLf + " and empID>='" & SName.ToString & "'"
        End If
        If EName.ToString <> "" Then
            Str += vbCrLf + " and empID<='" & EName.ToString & "'"
        End If
        If SDept.ToString <> "" Then
            Str += vbCrLf + " and dept='" & SDept.ToString & "'"
        End If
        If EDept.ToString <> "" Then
            Str += vbCrLf + " and dept='" & EDept.ToString & "'"
        End If
        If SBranch.ToString <> "" Then
            Str += vbCrLf + " and branch='" & SBranch.ToString & "'"
        End If
        If EBranch.ToString <> "" Then
            Str += vbCrLf + " and branch='" & EBranch.ToString & "'"
        End If
        Try
            objForm.Freeze(True)
            oGrid.DataTable.ExecuteQuery(Str)

            gridEditCol = oGrid.Columns.Item(0)
            gridEditCol.LinkedObjectType = 171
            gridEditCol.Editable = False

            gridEditCol = oGrid.Columns.Item(2)
            gridEditCol.Visible = False

            gridEditCol = oGrid.Columns.Item(3)
            gridEditCol.Visible = False

            gridEditCol = oGrid.Columns.Item(1)
            gridEditCol.Editable = False

            For i = 4 To 35
                Try
                    gridEditCol = oGrid.Columns.Item(i)
                    gridEditCol.LinkedObjectType = "AIS_INOUTTIME"
                    gridEditCol.Editable = False
                Catch

                End Try

            Next
            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
        End Try


    End Sub
#End Region

End Class
