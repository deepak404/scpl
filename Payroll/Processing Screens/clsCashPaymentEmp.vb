﻿Public Class clsCashPaymentEmp
    Public Const FormType = "MNU_OCPT"
    Public oForm As SAPbouiCOM.Form
    Dim oComboMonth, oComboYear As SAPbouiCOM.ComboBox
    Dim oRS, oRS1 As SAPbobsCOM.Recordset
    Dim strSQL, StrSQL1 As String
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim Row, IntYear As Integer
    Dim oCheck As SAPbouiCOM.CheckBox
    Dim StrMonth As String

    Public Sub LoadScreen()
        oForm = objAddOn.objUIXml.LoadScreenXML("CashPaymentEmp.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)

        oForm.Items.Item("8").Specific.value = oForm.BusinessObject.GetNextSerialNumber("-1", "AIS_OCPT")
        oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        oMatrix = oForm.Items.Item("7").Specific
        oComboMonth = oForm.Items.Item("4").Specific
        oComboYear = oForm.Items.Item("6").Specific
        Combo_Month()
        Combo_year()
    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        Try
                            If pVal.ItemUID = "1" And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                If Validate(FormUID) = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                            End If
                            If pVal.ItemUID = "1" And oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                DeleteRecords(FormUID, pVal.Row)
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("Before Action Validate Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                        End Try
                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                        Try
                            oComboMonth = oForm.Items.Item("4").Specific
                            oComboYear = oForm.Items.Item("6").Specific
                            If (oComboMonth.Selected Is Nothing Or oComboYear.Selected Is Nothing) Then
                            Else
                                If oMatrix.RowCount > 0 Then
                                    Dim i As Integer
                                    For i = oMatrix.RowCount To 1 Step -1
                                        oMatrix.DeleteRow(i)
                                    Next
                                End If
                                ValuesLoad(FormUID, oComboMonth.Selected.Value, oComboYear.Selected.Value)
                            End If

                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("Combo Select Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        Try
                            If pVal.ItemUID = "1" And oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.ActionSuccess = True Then
                                oForm.Close()
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("Item Presses Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                        End Try
                End Select
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub

#Region "Values Are Loaded in Matrix"
    Public Sub ValuesLoad(ByVal FormUID As String, ByVal StrMonth As String, ByVal IntYear As Integer)
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            'Taking the Values from Employee Master Data:
            oForm.Freeze(True)
            objAddOn.objApplication.SetStatusBarMessage("Please Wait the Values are Loading.....", SAPbouiCOM.BoMessageTime.bmt_Long, False)
            strSQL = "select T0.Empid ,ISNULL(T0.firstName ,'')+' '+ISNULL(T0.middleName,'')+' '+ISNULL(T0.lastName,'') as Name " & _
                " ,T1.U_amount [Amount] from OHEM T0 join [@AIS_OSPS]  T1 on T0.empID =T1.U_empid " & _
                " where T0.U_pmode ='C' and T1.U_month ='" & StrMonth & "' and U_year ='" & IntYear & "' and U_heads ='Net Pay'"
            oRS.DoQuery(strSQL)
            If oRS.RecordCount > 0 Then
                For Row = 1 To oRS.RecordCount
                    oMatrix.AddRow()
                    oMatrix.Columns.Item("V_-1").Cells.Item(Row).Specific.value = oMatrix.RowCount
                    oMatrix.Columns.Item("V_5").Cells.Item(Row).Specific.value = oRS.Fields.Item("Empid").Value
                    oMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = oRS.Fields.Item("Name").Value
                    oMatrix.Columns.Item("V_3").Cells.Item(Row).Specific.value = StrMonth
                    oMatrix.Columns.Item("V_2").Cells.Item(Row).Specific.value = IntYear
                    oMatrix.Columns.Item("V_1").Cells.Item(Row).Specific.value = oRS.Fields.Item("Amount").Value
                    'Check the same data in Data Base:
                    StrSQL1 = "select * from [@AIS_CPT1] where U_month='" & StrMonth & "' and U_year ='" & IntYear & "' and U_eid='" & oRS.Fields.Item("Empid").Value & "'"
                    oRS1.DoQuery(StrSQL1)
                    If oRS1.RecordCount > 0 Then
                        oMatrix.Columns.Item("V_-1").Cells.Item(Row).Specific.value = oMatrix.RowCount
                        oMatrix.Columns.Item("V_5").Cells.Item(Row).Specific.value = oRS1.Fields.Item("U_eid").Value
                        oMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = oRS1.Fields.Item("U_ename").Value
                        oMatrix.Columns.Item("V_3").Cells.Item(Row).Specific.value = oRS1.Fields.Item("U_month").Value
                        oMatrix.Columns.Item("V_2").Cells.Item(Row).Specific.value = oRS1.Fields.Item("U_year").Value
                        oMatrix.Columns.Item("V_1").Cells.Item(Row).Specific.value = oRS1.Fields.Item("U_amount").Value
                        oCheck = oMatrix.Columns.Item("V_0").Cells.Item(Row).Specific
                        If oRS1.Fields.Item("U_paid").Value = "Y" Then
                            oCheck.Checked = True
                        Else
                            oCheck.Checked = False
                        End If
                    End If
                    oRS.MoveNext()
                Next
            End If
            oForm.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage("Values Are Loaded Based on Selected Criteria", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Catch ex As Exception
            oForm.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            oForm.Freeze(False)
        End Try
    End Sub
#End Region

#Region "Delete the Same Records"
    Public Sub DeleteRecords(ByVal FormUID As String, ByVal RowNo As Integer)
        Dim i As Integer
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            For i = 1 To oMatrix.VisualRowCount
                strSQL = "select * from [@AIS_CPT1] where U_eid='" & oMatrix.Columns.Item("V_5").Cells.Item(i).Specific.string & "' and U_month='" & oComboMonth.Selected.Value & "' and U_year ='" & oComboYear.Selected.Value & "'"
                oRS.DoQuery(strSQL)
                If oRS.RecordCount > 0 Then
                    For Row = 1 To oRS.RecordCount
                        StrSQL1 = "delete  from  [@AIS_CPT1] where  U_empid ='" & oMatrix.Columns.Item("V_5").Cells.Item(i).Specific.string & "' and U_month='" & oMatrix.Columns.Item("V_3").Cells.Item(i).Specific.string & "' and  U_year='" & oMatrix.Columns.Item("V_2").Cells.Item(i).Specific.string & "'"
                        oRS1.DoQuery(StrSQL1)
                    Next
                End If
            Next
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Delete Record Function - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
#End Region

#Region "Load Month and Year"
    Public Sub Combo_Month()
        oComboMonth = oForm.Items.Item("4").Specific
        oComboMonth.ValidValues.Add("01", "January")
        oComboMonth.ValidValues.Add("02", "February")
        oComboMonth.ValidValues.Add("03", "March")
        oComboMonth.ValidValues.Add("04", "April")
        oComboMonth.ValidValues.Add("05", "May")
        oComboMonth.ValidValues.Add("06", "June")
        oComboMonth.ValidValues.Add("07", "July")
        oComboMonth.ValidValues.Add("08", "Augugst")
        oComboMonth.ValidValues.Add("09", "September")
        oComboMonth.ValidValues.Add("10", "October")
        oComboMonth.ValidValues.Add("11", "November")
        oComboMonth.ValidValues.Add("12", "December")
    End Sub

    Public Sub Combo_year()
        oComboYear = oForm.Items.Item("6").Specific
        oComboYear.ValidValues.Add("2012", "2012")
        oComboYear.ValidValues.Add("2013", "2013")
        oComboYear.ValidValues.Add("2014", "2014")
        oComboYear.ValidValues.Add("2015", "2015")
        oComboYear.ValidValues.Add("2016", "2016")
        oComboYear.ValidValues.Add("2017", "2017")
        oComboYear.ValidValues.Add("2018", "2018")
        oComboYear.ValidValues.Add("2019", "2019")
        oComboYear.ValidValues.Add("2020", "2020")
        oComboYear.ValidValues.Add("2021", "2021")
        oComboYear.ValidValues.Add("2022", "2022")
        oComboYear.ValidValues.Add("2023", "2023")
        oComboYear.ValidValues.Add("2024", "2024")
        oComboYear.ValidValues.Add("2025", "2025")

    End Sub
#End Region

#Region "Valiodate Event"
    Public Function Validate(ByVal FomUID As String) As Boolean
        If oComboMonth.Selected Is Nothing Then
            objAddOn.objApplication.SetStatusBarMessage("please select Month", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf oComboYear.Selected Is Nothing Then
            objAddOn.objApplication.SetStatusBarMessage("please select Year", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

End Class
