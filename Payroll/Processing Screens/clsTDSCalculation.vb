﻿Public Class clsTDSCalculation
    Public Const FormType = "MNU_TDSCALC"
    Dim oForm As SAPbouiCOM.Form
    Dim oRS As SAPbobsCOM.Recordset
    Dim SQL As String
    Dim Amount As Double = 0
    Dim SMon, EMon, EYear, SYear, Count, IntNPMnth As Integer
    Dim EID As String
    Dim Ocombo As SAPbouiCOM.ComboBox
    Dim Basic, HRA, DA, Total As Double
    Dim RentPaid, FiftyPer, HRARcd, RntPDTenPer, TransAllow, MedReimb As Double
    Dim InFromHousing, RelonHomeLoan, OtherIncome, GrandTotalIncome, GrossTaxSalary, Minimum, TotalTDSAmount As Double
    Public Sub LoadScreen()
        oForm = objAddOn.objUIXml.LoadScreenXML("TDSCalculation.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE

    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    Try
                        If pVal.ItemUID = "1" And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            If Validation(FormUID) = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Before Action Validate Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try

            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        If pVal.ItemUID = "9" Then
                            'Ocombo = oForm.Items.Item("6").Specific
                            'oRS.DoQuery("select MIN(F_RefDate)[SDate],MAX(T_RefDate ) [EDate] ,Category  from OFPR where Category ='" & Ocombo.Selected.Value & "' group by Category ")

                            SMon = "04" ' Mid(oRS.Fields.Item("SDate").Value, 5, 2)
                            SYear = Now.Year  'Left(oRS.Fields.Item("SDate").Value, 4)
                            EMon = "03" 'Mid(oRS.Fields.Item("EDate").Value, 5, 2)
                            EYear = Now.Year + 1 'Left(oRS.Fields.Item("EDate").Value, 4)
                            EID = oForm.Items.Item("8").Specific
                            TDSCalcuationAmount(FormUID, EID, EMon, EYear, SMon, SYear)
                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("After Action Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try
            End Select
        End If
    End Sub

#Region "Calculate the TDS Amount"
    Sub TDSCalcuationAmount(ByVal FormUID As String, ByVal EID As String, ByVal EMonth As Integer, ByVal EYear As Integer, ByVal SMonth As Integer, ByVal SYear As Integer)
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            'Taking the values from DB processed salary Details:

            oRS.DoQuery("DECLARE @sql varchar(MAX) declare @columnscsv varchar(MAX) DECLARE @sql1 varchar(MAX) " & _
                        "select @columnscsv = COALESCE(@columnscsv + '],[','') + name from [@AIS_PAYCODE] " & _
                        "select @sql1=COALESCE(@sql1 + ']+[','') + name from [@AIS_PAYCODE]  where Name <>'HRA' " & _
                        "set @columnscsv = '[' + @columnscsv + ']' set @sql1 = '[' + @sql1 + ']' " & _
                        "SET @sql = ' SELECT  ' + @columnscsv + ', '+ @sql1 +' as Total FROM " & _
                        "(select SUM(convert(float,x.Amount ) )[Amount] ,x.Heads from(select (U_amount)[Amount] ,U_heads [Heads] " & _
                        "from [@AIS_OSPS] where U_empid =''" & EID & "'' and U_month >=''" & SMonth & "'' " & _
                        "and U_month <=''" & EMonth & "'' and U_year >=''" & SYear & "'' and U_year <=''" & EYear & "'' and U_heads not in(''Pay'',''Remarks'') " & _
                        "and U_heads in(select Name from [@AIS_PAYCODE] ) ) x group by x.Heads  ) a " & _
                        "PIVOT (SUM(amount) for Heads in (' + @columnscsv + ')) as PVT' EXEC (@sql) ")


            Basic = oRS.Fields.Item("Basic").Value
            HRA = oRS.Fields.Item("HRA").Value
            ' DA = oRS.Fields.Item("DA").Value
            Amount = oRS.Fields.Item("Total").Value
            'Number of month processed salary:

            oRS.DoQuery("select distinct max(U_month) [Con] from [@AIS_OSPS] where U_empid ='" & EID & "' and U_month >='" & SMonth & "' and " & _
                        " U_month <='" & EMonth & "' and U_year >='" & SYear & "' and U_year <='" & EYear & "' group by U_month ")

            Count = oRS.RecordCount

            oRS.DoQuery("select isnull(SUM(U_amt),0) [Amount],isnull(U_arpaid,0) [Act Rent Paid] " & _
                        " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where U_descrpn ='Basic' and U_eid =" & EID & " ),0)[Basic] " & _
                        " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where U_descrpn ='HRA' and U_eid =" & EID & " ),0)[HRA] " & _
                        " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where U_descrpn ='DA' and U_eid =" & EID & " ),0)[DA] " & _
                        " from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where U_eid =" & EID & "  and T1.U_descrpn <>'HRA' group by U_arpaid  ")

            'Earnings:
            Basic += oRS.Fields.Item("Basic").Value
            HRA += oRS.Fields.Item("HRA").Value
            DA += oRS.Fields.Item("DA").Value

            IntNPMnth = 12 - Count
            Amount += oRS.Fields.Item("Amount").Value * IntNPMnth

            'Deductions:
            RentPaid = oRS.Fields.Item("Act Rent Paid").Value

            FiftyPer = (Basic + HRA) * 0.5
            HRARcd = HRA
            RntPDTenPer = RentPaid - ((Basic + HRA) * 0.1)

            'Find the Minimum of those three values:
            Minimum = Math.Min(Math.Min(FiftyPer, HRARcd), RntPDTenPer)
            HRARcd = HRA - Minimum
            ' oForm.Items.Item("49").Specific.value = HRARcd

            'Deductions
            'TransAllow = oForm.Items.Item("").Specific.value
            'MedReimb = oForm.Items.Item("").Specific.value

            GrossTaxSalary = Amount - (Minimum + TransAllow + MedReimb)

            'InFromHousing = oForm.Items.Item("").Specific.value
            'RelonHomeLoan = oForm.Items.Item("").Specific.value
            'OtherIncome = oForm.Items.Item("").Specific.value

            GrandTotalIncome = ((GrossTaxSalary + InFromHousing) - RelonHomeLoan) + OtherIncome

            oRS.DoQuery("select isnull(sum(T1.U_amt),0) Amount from [@AIS_EMPSSTP] T0 " & _
                        " left join [@AIS_EPSSTP8]  T1 on T0.Code =T1.Code where T0.U_eid =" & EID & "")

            TotalTDSAmount = GrandTotalIncome - oRS.Fields.Item("Amount").Value


        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("TDSCalcuationAmount Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
#End Region


#Region "Validation for Empty Document should not to be Add"
    Public Function Validation(ByVal FormUID) As Boolean
        Try
            If oForm.Items.Item("8").Specific.Value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select Employee Code", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            ElseIf oForm.Items.Item("6").Specific.Value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select Financial Year", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
            Return True
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Vaidation Funtion Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Function
#End Region

End Class
