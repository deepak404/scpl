﻿Public Class ClsPrSal
    Public Const formtype = "PrSal"
    Dim objForm As SAPbouiCOM.Form
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim objRS As SAPbobsCOM.Recordset
    Dim StrSQL, QueryGrp As String
    Dim objForm1 As SAPbouiCOM.Form
    Dim oHeader, oDetail As SAPbouiCOM.DBDataSource
    Dim I As Integer
    Public Sub LoadScreen(ByVal StrDept As String, ByVal StrBranch As String, ByVal IntYear As Integer, ByVal IntMonth As Integer, ByVal strMonth As String)
        objForm = objAddOn.objUIXml.LoadScreenXML("PrSal.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        oHeader = objForm.DataSources.DBDataSources.Item(0)
        oDetail = objForm.DataSources.DBDataSources.Item(1)
        objForm.Items.Item("6").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_PRSAL")
        MatrixLoad(StrDept, StrBranch, IntYear, IntMonth)
        Mon = strMonth
        Yr = IntYear
        IMon = IntMonth
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction Then
                objForm = objAddOn.objApplication.Forms.Item(FormUID)
                Select Case (pVal.EventType)
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            If validate(FormUID) = False Then
                                BubbleEvent = False
                                ' objAddOn.objApplication.SetStatusBarMessage("Enter the Mandatory Fields", SAPbouiCOM.BoMessageTime.bmt_Short, True)

                            End If
                        End If
                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        If pVal.ItemUID = "4" Then
                            LineTotal(FormUID)
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "3" Then
                            Dim Amount As Decimal
                            Amount = objForm.Items.Item("11").Specific.value
                            ' JournalEntry(Amount)
                            JECreation()
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            objForm.Close()
                        End If
                End Select

            End If
        Catch ex As Exception

        End Try
    End Sub

#Region "Matrix Load"
    Public Sub MatrixLoad(ByVal StrDept As String, ByVal StrBranch As String, ByVal Year As String, ByVal Month As String)
        Try
            objAddOn.objApplication.SetStatusBarMessage("Please Wait Values are Loading.......", SAPbouiCOM.BoMessageTime.bmt_Long, False)
            objForm.Freeze(True)
            Dim i As Integer
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            '     StrSQL = " SELECT A.* FROM(select b.U_empid,b.U_empname,b.U_Payday,b.U_npay  from [@AIS_ALPS] a join [@AIS_ALPSL] b on a.docentry=b.docentry where 1=1"
            StrSQL = "select A.* from(select x.U_empid ,x.U_ename,max(x.[Net Pay] )[Net pay] ,max(x.PDays )[PDays] from " & _
                " (select U_empid ,max(U_ename)[U_ename] , max(U_amount)[Net Pay],''[PDays] from [@AIS_OSPS] where U_month ='" & Month & "' and U_year ='" & Year & "' " & _
                " and U_heads ='Net Pay' and U_heads not in ('Pay','Remarks') and U_pay='Y' group by U_empid  union all " & _
                " select U_empid ,U_ename ,'', U_amount from [@AIS_OSPS] where U_month ='" & Month & "' and U_year ='" & Year & "' " & _
                " and U_heads ='Payable Days' and U_heads not in ('Pay','Remarks') and U_pay='Y') X left join OHEM T1 on x.U_empid =T1 .empID where T1.Active='Y' "

            If StrBranch.ToString <> "" Then
                StrSQL += vbCrLf + " and T1.U_branch = '" & StrBranch & "'"
            End If
            If StrDept.ToString <> "" Then
                StrSQL += vbCrLf + " and T1.U_Dept = '" & StrDept & "'"
            End If
            StrSQL += " group by x.U_empid ,x.U_ename) A left join ("
            '  StrSQL += vbCrLf + " )A Left Join (select b.U_empid,b.U_empname,b.U_pdays,b.U_netpay  from [@AIS_PRSAL] a join [@AIS_PRSAL1] b on a.docentry=b.docentry where 1=1"

            StrSQL += " select b.U_empid,b.U_empname,b.U_pdays,b.U_netpay  from [@AIS_PRSAL] a join [@AIS_PRSAL1] b on a.docentry=b.docentry " & _
                " join OHEM c on b.U_empid =c.empID where U_month ='" & Month & "' and U_year ='" & Year & "' "

            If StrBranch.ToString <> "" Then
                StrSQL += vbCrLf + " and a.U_branch = '" & StrBranch & "'"
            End If
            If StrDept.ToString <> "" Then
                StrSQL += vbCrLf + " and a.U_Dept = '" & StrDept & "'"
            End If
            StrSQL += vbCrLf + ") B on a.U_empid =b.U_empid  where (b.U_empid is null)"

            objRS.DoQuery(StrSQL)

            objMatrix = objForm.Items.Item("4").Specific
            'objForm.Items.Item("7").Specific.value = StrBranch
            'objForm.Items.Item("8").Specific.value = StrDept
            'objForm.Items.Item("9").Specific.value = Month
            'objForm.Items.Item("10").Specific.value = Year

            oHeader.SetValue("U_month", oHeader.Size - 1, Month)
            oHeader.SetValue("U_year", oHeader.Size - 1, Year)
            oHeader.SetValue("U_dept", oHeader.Size - 1, StrDept)
            oHeader.SetValue("U_branch", oHeader.Size - 1, StrBranch)

            If objRS.RecordCount > 0 Then
                For i = 0 To objRS.RecordCount - 1

                    oDetail.SetValue("LineId", oDetail.Size - 1, i + 1)
                    oDetail.SetValue("U_empid", oDetail.Size - 1, objRS.Fields.Item("U_empid").Value)
                    oDetail.SetValue("U_empname", oDetail.Size - 1, objRS.Fields.Item("U_ename").Value)
                    oDetail.SetValue("U_pdays", oDetail.Size - 1, objRS.Fields.Item("PDays").Value)
                    oDetail.SetValue("U_netpay", oDetail.Size - 1, objRS.Fields.Item("Net pay").Value)
                    oDetail.InsertRecord(oDetail.Size)
                    oDetail.Offset = oDetail.Size - 1

                    'objMatrix.Columns.Item("V_-1").Cells.Item(i).Specific.value = objMatrix.RowCount
                    'objMatrix.Columns.Item("V_3").Cells.Item(i).Specific.value = objRS.Fields.Item("U_empid").Value
                    'objMatrix.Columns.Item("V_2").Cells.Item(i).Specific.value = objRS.Fields.Item("U_empname").Value
                    'objMatrix.Columns.Item("V_1").Cells.Item(i).Specific.value = objRS.Fields.Item("U_payday").Value
                    'objMatrix.Columns.Item("V_0").Cells.Item(i).Specific.value = objRS.Fields.Item("U_npay").Value
                    objRS.MoveNext()
                Next i
                objMatrix.LoadFromDataSourceEx()
                objForm.Freeze(False)
                objAddOn.objApplication.SetStatusBarMessage("Values are Loaded Based on Selected Criteria", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Else
                objAddOn.objApplication.SetStatusBarMessage("Already Journal Entries Posted For the Same.", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                objForm.Freeze(False)
                objForm.Close()
            End If
        Catch ex As Exception
            objForm.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try


    End Sub
#End Region

#Region "Validation"
    Public Function validate(ByVal formuid As String)

        objForm = objAddOn.objApplication.Forms.Item(formuid)
        objMatrix = objForm.Items.Item("4").Specific
        If objMatrix.VisualRowCount > 0 Then
            If objMatrix.Columns.Item("3").Cells.Item(1).Specific.string = "" Then
                Return False
            ElseIf objMatrix.Columns.Item("2").Cells.Item(1).Specific.string = "" Then
                Return False
            ElseIf objMatrix.Columns.Item("1").Cells.Item(1).Specific.string = "" Then
                Return False
            ElseIf objMatrix.Columns.Item("0").Cells.Item(1).Specific.string = "" Then
                Return False
            End If
        Else
            objAddOn.objApplication.SetStatusBarMessage("Enter the data", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Line Totals"
    Public Sub LineTotal(ByVal FormUID As String)
        Dim tot As Double
        Dim i As Integer
        objMatrix = objForm.Items.Item("4").Specific
        For i = 1 To objMatrix.VisualRowCount
            If CDbl(objMatrix.Columns.Item("V_0").Cells.Item(i).Specific.value) > 0 Then
                tot = tot + objMatrix.GetCellSpecific("V_0", i).value
                objForm.Items.Item("11").Specific.value = tot
            End If
        Next i
        objForm.Refresh()
        objForm.Update()
    End Sub
#End Region

#Region "Journal Entry Creation for Common Payroll"
    Sub JECreation()
        Try
            Dim objRS1, objRS2 As SAPbobsCOM.Recordset

            Dim strSQL1 As String

            objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS2 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            objAddOn.objApplication.Menus.Item("1540").Activate()
            objForm1 = objAddOn.objApplication.Forms.ActiveForm
            objForm1 = objAddOn.objApplication.Forms.Item(objForm1.UniqueID)
            objRS1.DoQuery("select TransId,Memo,Number from OJDT where Memo='" & Mon + " - " + Yr & "'")
            If objRS1.RecordCount > 0 Then
                objForm1.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                objForm1.Items.Item("141").Specific.value = objRS1.Fields.Item("Number").Value
                objForm1.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            Else
                objForm1.Items.Item("10").Specific.value = Mon + " - " + Yr
                objAddOn.objApplication.SetStatusBarMessage("Please Wait the Values are Loading......", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                objForm1.Freeze(True)
                objMatrix = objForm1.Items.Item("76").Specific
                strSQL1 = "select Code,Name,U_glac [Account] from [@AIS_PAYCODE] union all select Code,Name,U_glac [Account] from [@AIS_DDC] "
                objRS1.DoQuery(strSQL1)
                QueryGrp = ""
                For IntI As Integer = 0 To objRS1.RecordCount - 1
                    QueryGrp += " SELECT round(sum(CAST(T0.U_amount AS numeric(19,2))),0,0)[Amount],'" & objRS1.Fields.Item("Name").Value & "' [Head],'" & objRS1.Fields.Item("Account").Value & "' [Account]  FROM [@AIS_OSPS]T0 where U_heads ='" & objRS1.Fields.Item("Name").Value & "' and T0.U_month='" & IMon & "' and T0.U_year ='" & Yr & "' and U_pay='Y' union all"
                    objRS1.MoveNext()
                Next
                QueryGrp += " SELECT round(sum(CAST(T0.U_amount AS numeric(19,2))),0,0)[Amount],'0' [Head],'0'[Account] FROM [@AIS_OSPS] T0 where (T0.U_heads='Net Earnings'  or T0.U_heads= 'Net Deductions') and T0.U_month='" & IMon & "' and T0.U_year ='" & Yr & "' and U_pay='Y' "
                objRS2.DoQuery(QueryGrp)
                For I = 1 To objRS2.RecordCount - 1

                    objMatrix.Columns.Item("1").Cells.Item(I).Specific.value = objRS2.Fields.Item("Account").Value
                    objMatrix.Columns.Item("5").Cells.Item(I).Specific.value = objRS2.Fields.Item("Amount").Value
                    objRS2.MoveNext()
                Next

                objRS.DoQuery("select U_dsac [Salary Payasble] from [@AIS_GLAC]")

                objMatrix.Columns.Item("1").Cells.Item(I).Specific.value = objRS.Fields.Item("Salary Payasble").Value
                objMatrix.Columns.Item("6").Cells.Item(I).Click(SAPbouiCOM.BoCellClickType.ct_Regular, 0)

                'This for Reimbursement,Loan,Benefits:

                objRS1.DoQuery("select Code,Name,U_pgla [Debit],U_egla [Credit] from [@AIS_BCODE] union all select Code,Name,U_pgla [Debit],U_egla [Credit] from [@AIS_DREIM] where U_insal='Y' union all select Code,Name,U_pac  [Debit],U_iac  [Credit] from [@AIS_LONADV] ")
                QueryGrp = ""
                For IntI As Integer = 0 To objRS1.RecordCount - 1
                    QueryGrp += " SELECT isnull(round(sum(CAST(T0.U_amount AS numeric(19,2))),0,0),0) [Amount],'" & objRS1.Fields.Item("Debit").Value & "'[Debit],'" & objRS1.Fields.Item("Credit").Value & "'[Credit] FROM [@AIS_OSPS]T0 where U_heads ='" & objRS1.Fields.Item("Name").Value & "' and T0.U_month='" & IMon & "' and T0.U_year ='" & Yr & "' union All"
                    objRS1.MoveNext()
                Next
                QueryGrp = QueryGrp.Remove(QueryGrp.Length - 9)
                objRS2.DoQuery(QueryGrp)
                For I = objMatrix.RowCount To objMatrix.RowCount + objRS2.RecordCount * 2
                    If objRS2.EoF = False Then
                        objMatrix.Columns.Item("1").Cells.Item(I).Specific.value = objRS2.Fields.Item("Debit").Value
                        objMatrix.Columns.Item("5").Cells.Item(I).Specific.value = objRS2.Fields.Item("Amount").Value

                        objMatrix.Columns.Item("1").Cells.Item(I + 1).Specific.value = objRS2.Fields.Item("Credit").Value
                        objMatrix.Columns.Item("6").Cells.Item(I + 1).Specific.value = objRS2.Fields.Item("Amount").Value
                        I = I + 1
                        objRS2.MoveNext()
                    End If
                Next

                'This is for Reimbursement Details Except Include Salary:
                objRS.DoQuery("select T2.U_pgla [Debit],T2.U_egla [Credit],sum(T1.U_amtpaid) [Amount]  from [@AIS_ORED] T0 " & _
                             " left outer join [@AIS_red1] T1 on T0.DocEntry =T1 .DocEntry left outer join [@AIS_DREIM] T2 on T1.U_desc =T2.Name " & _
                            " where T2.U_insal='N' and T1 .U_pay='Y' and T0.U_month ='" & IMon & "' and T0.U_year ='" & Yr & "' group by T2 .U_pgla ,T2.U_egla ")

                For I = objMatrix.RowCount To objMatrix.RowCount + objRS.RecordCount * 2
                    If objRS.EoF = False Then
                        objMatrix.Columns.Item("1").Cells.Item(I).Specific.value = objRS.Fields.Item("Debit").Value
                        objMatrix.Columns.Item("5").Cells.Item(I).Specific.value = objRS.Fields.Item("Amount").Value

                        objMatrix.Columns.Item("1").Cells.Item(I + 1).Specific.value = objRS.Fields.Item("Credit").Value
                        objMatrix.Columns.Item("6").Cells.Item(I + 1).Specific.value = objRS.Fields.Item("Amount").Value
                        I = I + 1
                        objRS.MoveNext()
                    End If
                Next
            End If
            objForm1.Freeze(False)
        Catch ex As Exception
            objForm1.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage("Journal Entry Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
            objForm1.Freeze(False)
        End Try
    End Sub
#End Region

#Region "Journal Entry"
    Public Sub JournalEntry(ByVal Amount As Decimal)
        Try

            Dim objRS1, objRS2 As SAPbobsCOM.Recordset

            Dim strSQL1, strSQL2 As String

            objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS2 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            objAddOn.objApplication.Menus.Item("1540").Activate()
            objForm1 = objAddOn.objApplication.Forms.ActiveForm
            objForm1 = objAddOn.objApplication.Forms.Item(objForm1.UniqueID)
            objRS1.DoQuery("select TransId,Memo,Number from OJDT where Memo='" & Mon + " - " + Yr & "'")
            If objRS1.RecordCount > 0 Then
                objForm1.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                objForm1.Items.Item("141").Specific.value = objRS1.Fields.Item("Number").Value
                objForm1.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            Else
                objForm1.Items.Item("10").Specific.value = Mon + " - " + Yr
                objAddOn.objApplication.SetStatusBarMessage("Please Wait the Values are Loading......", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                objForm1.Freeze(True)
                objMatrix = objForm1.Items.Item("76").Specific
                strSQL1 = "select round(SUM(T0.U_basic),0,0)[Basic],round(SUM(T0.U_hra),0,0) [HRA],round(SUM(T0.U_con),0,0)[Conveyance]" & _
                        ",round(SUM(T0.U_medall ),0,0)[Medical],round(SUM(T0.U_eduall ),0,0)[Education],round(SUM(T0.U_tele ),0,0)[Telephone]" & _
                        ",round(SUM(T0.U_treim ),0,0)[Reimbursement],round(SUM(T0.U_tarrer ),0,0)[Arrear],round(SUM(T0.U_totot ),0,0)[OT] " & _
                        ",round(SUM(T0.U_oadd ),0,0)[Other Addition],round(SUM(T0.U_bonus ),0,0)[Bonus],round(SUM(T0.U_paidhol ),0,0)[Earned PH]" & _
                        ",round(SUM(T0.U_tloan ),0,0) [Loan],round(SUM(T0.U_bonpble ),0,0)[Bonus Payable] ,round(SUM(T0.U_pf),0,0)[PF]" & _
                        ",round(SUM(T0.U_esi),0,0)[ESI],round(SUM(T0.U_pt ),0,0)[PT],round(SUM(T0.U_it),0,0)[IT],round(SUM(T0.U_canded ),0,0)[Canteen]" & _
                        ",round(SUM(T0.U_dpaidhol ),0,0)[Deducted PH],round(SUM(T0.U_dedot ),0,0)[Deducted OT],round(SUM(T0.U_lwf ),0,0)[LWF]" & _
                        ",round(SUM(T0.U_tds ),0,0)[TDS],round(SUM(T0.U_oded ),0,0)[Other Deductions]  from [@AIS_ALPSL] T0 " & _
                        "left outer join [@AIS_PRSAL1] T1 on T0.U_empid =T1.U_empid where T0.U_month='" & IMon & "' and T0.U_year='" & Yr & "'"
                objRS1.DoQuery(strSQL1)




                strSQL2 = "select Distinct (select U_glac from  [@AIS_DDC] where Name ='PF')[PF],(select U_glac from  [@AIS_DDC] where Name ='EST')[ESI]" & _
                    ",(select U_glac from  [@AIS_DDC] where Name ='PT') [PT],(select U_glac from  [@AIS_DDC] where Name ='IT')[IT]" & _
                    ",(select U_glac from  [@AIS_DDC] where Name ='Canteen') [Canteen],(select U_glac from  [@AIS_DDC] where Name ='Paid Holidays') [PH]" & _
                    ",(select U_glac from  [@AIS_DDC] where Name ='OT') [DOT],(select U_glac from  [@AIS_DDC] where Name ='LWF') [LWF]" & _
                    ",(select U_glac from  [@AIS_DDC] where Name ='TDS')[DTDS],(select U_glac from  [@AIS_DDC] where Name ='Bonus Payable')[Bonus Payable]" & _
                    ",(select U_glac from  [@AIS_PAYCODE] where Name ='Basic')[Basic],(select U_glac from  [@AIS_PAYCODE] where Name ='HRA') [HRA]" & _
                    ",(select U_glac from  [@AIS_PAYCODE] where Name ='Conveyance') [Conveyance],(select U_glac from  [@AIS_PAYCODE] where Name ='Med All') [Medical]" & _
                    ",(select U_glac from  [@AIS_PAYCODE] where Name ='Edu All') [Education],(select U_glac from  [@AIS_PAYCODE] where Name ='Telephone') [Telephone]" & _
                    ",(select U_ctdsc from [@AIS_GLAC] where U_brnch ='2') as [CTDS],(select U_cspac from [@AIS_GLAC] where U_brnch ='2') as [Salary Payable],(select U_doaddc from [@AIS_GLAC] where U_brnch ='2') as [Other Addition],(select U_codedc from [@AIS_GLAC] where U_brnch ='2') as [Other Deductions] " & _
                    ",(select U_dvpfc from [@AIS_GLAC] where U_brnch ='2') as [PF Employeer],(select U_pgla from [@AIS_BCODE] where Code ='Bonus') [DPay],(select U_egla from [@AIS_BCODE] where Code ='Bonus') [CPay] from [@AIS_DDC] "

                objRS2.DoQuery(strSQL2)
                'Location
                Dim oCombo As SAPbouiCOM.ComboBox = objForm1.Items.Item("2000002018").Specific
                oCombo.Select(2, SAPbouiCOM.BoSearchKey.psk_Index)
                'Additions
                objMatrix.Columns.Item("1").Cells.Item(1).Specific.value = objRS2.Fields.Item("Basic").Value
                objMatrix.Columns.Item("5").Cells.Item(1).Specific.value = objRS1.Fields.Item("Basic").Value

                objMatrix.Columns.Item("1").Cells.Item(2).Specific.value = objRS2.Fields.Item("HRA").Value
                objMatrix.Columns.Item("5").Cells.Item(2).Specific.value = objRS1.Fields.Item("HRA").Value

                objMatrix.Columns.Item("1").Cells.Item(3).Specific.value = objRS2.Fields.Item("Conveyance").Value
                objMatrix.Columns.Item("5").Cells.Item(3).Specific.value = objRS1.Fields.Item("Conveyance").Value

                objMatrix.Columns.Item("1").Cells.Item(4).Specific.value = objRS2.Fields.Item("Medical").Value
                objMatrix.Columns.Item("5").Cells.Item(4).Specific.value = objRS1.Fields.Item("Medical").Value

                objMatrix.Columns.Item("1").Cells.Item(5).Specific.value = objRS2.Fields.Item("Education").Value
                objMatrix.Columns.Item("5").Cells.Item(5).Specific.value = objRS1.Fields.Item("Education").Value

                objMatrix.Columns.Item("1").Cells.Item(6).Specific.value = objRS2.Fields.Item("Telephone").Value
                objMatrix.Columns.Item("5").Cells.Item(6).Specific.value = objRS1.Fields.Item("Telephone").Value

                'Other Addtions Accounts:
                ' MsgBox(objRS2.Fields.Item("Other Addition").Value)
                objMatrix.Columns.Item("1").Cells.Item(7).Specific.value = objRS2.Fields.Item("Other Addition").Value
                objMatrix.Columns.Item("5").Cells.Item(7).Specific.value = (objRS1.Fields.Item("Other Addition").Value + objRS1.Fields.Item("Arrear").Value & _
                                                                        +objRS1.Fields.Item("OT").Value + objRS1.Fields.Item("Reimbursement").Value & _
                                                                          +objRS1.Fields.Item("Earned PH").Value + objRS1.Fields.Item("Loan").Value)



                'Deductions

                objMatrix.Columns.Item("1").Cells.Item(8).Specific.value = objRS2.Fields.Item("PF").Value
                objMatrix.Columns.Item("6").Cells.Item(8).Specific.value = objRS1.Fields.Item("PF").Value * 2

                objMatrix.Columns.Item("1").Cells.Item(9).Specific.value = objRS2.Fields.Item("PT").Value
                objMatrix.Columns.Item("6").Cells.Item(9).Specific.value = objRS1.Fields.Item("PT").Value

                objMatrix.Columns.Item("1").Cells.Item(10).Specific.value = objRS2.Fields.Item("IT").Value
                objMatrix.Columns.Item("6").Cells.Item(10).Specific.value = objRS1.Fields.Item("IT").Value

                objMatrix.Columns.Item("1").Cells.Item(11).Specific.value = objRS2.Fields.Item("Other Deductions").Value
                objMatrix.Columns.Item("6").Cells.Item(11).Specific.value = (objRS1.Fields.Item("Canteen").Value + objRS1.Fields.Item("TDS").Value & _
                                                                            +objRS1.Fields.Item("Other Deductions").Value + objRS1.Fields.Item("LWF").Value & _
                                                                            +objRS1.Fields.Item("Loan").Value + objRS1.Fields.Item("Bonus Payable").Value & _
                                                                            +objRS1.Fields.Item("Deducted PH").Value)
                'PF Employeer
                objMatrix.Columns.Item("1").Cells.Item(12).Specific.value = objRS2.Fields.Item("PF Employeer").Value
                objMatrix.Columns.Item("6").Cells.Item(12).Specific.value = objRS1.Fields.Item("PF").Value
                'salary Payable Account
                objMatrix.Columns.Item("1").Cells.Item(13).Specific.value = objRS2.Fields.Item("Salary Payable").Value
                objMatrix.Columns.Item("6").Cells.Item(13).Click(SAPbouiCOM.BoCellClickType.ct_Regular, 0)

                'TDS Accounts - Debit:
                objMatrix.Columns.Item("1").Cells.Item(14).Specific.value = objRS2.Fields.Item("DTDS").Value
                objMatrix.Columns.Item("5").Cells.Item(14).Specific.value = objRS1.Fields.Item("TDS").Value

                'TDS Account - Credit
                objMatrix.Columns.Item("1").Cells.Item(15).Specific.value = objRS2.Fields.Item("CTDS").Value
                objMatrix.Columns.Item("6").Cells.Item(15).Specific.value = objRS1.Fields.Item("TDS").Value

                'Bonus Debit
                objMatrix.Columns.Item("1").Cells.Item(16).Specific.value = objRS2.Fields.Item("DPay").Value
                objMatrix.Columns.Item("5").Cells.Item(16).Specific.value = objRS1.Fields.Item("Bonus").Value

                'Bonus Credit
                objMatrix.Columns.Item("1").Cells.Item(17).Specific.value = objRS2.Fields.Item("CPay").Value
                objMatrix.Columns.Item("6").Cells.Item(17).Specific.value = objRS1.Fields.Item("Bonus").Value



                objForm1.Freeze(False)
                objAddOn.objApplication.SetStatusBarMessage("Values Are Loaded", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            End If
        Catch ex As Exception
            objForm1.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
            objForm1.Freeze(False)
        End Try
    End Sub
#End Region



End Class
