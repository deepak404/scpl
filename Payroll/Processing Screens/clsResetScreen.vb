﻿Public Class clsResetScreen
    Public Const FormType = "MNU_RSET"
    Dim oForm As SAPbouiCOM.Form
    Dim RS As SAPbobsCOM.Recordset
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim oDataTable As SAPbouiCOM.DataTable
    Dim oCheck As SAPbouiCOM.CheckBox
    Dim Code As String
    Public Sub LoadScreen()
        Try
            oForm = objAddOn.objUIXml.LoadScreenXML("ResetScreen.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            oMatrix = oForm.Items.Item("4").Specific
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.string = oMatrix.VisualRowCount
            LoadMatrix()

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("LoadScreen Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType

                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "3" Then
                            For Row As Integer = 1 To oMatrix.VisualRowCount
                                oCheck = oMatrix.Columns.Item("V_0").Cells.Item(Row).Specific
                                oCheck.Checked = True
                            Next
                        End If
                        If pVal.ItemUID = "1" Then
                            For Line As Integer = 1 To oMatrix.VisualRowCount
                                oCheck = oMatrix.Columns.Item("V_0").Cells.Item(Line).Specific
                                If oCheck.Checked = True Then
                                    Code = oMatrix.Columns.Item("V_2").Cells.Item(Line).Specific.string
                                    ResetReimDetails(FormUID, Code)
                                    LeaveDetailsReset(FormUID, Code)
                                End If
                            Next
                            objAddOn.objApplication.SetStatusBarMessage("Leave Details and Reimbursement Details Reset Successfully", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                        End If



                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        If pVal.ItemUID = "4" And pVal.ColUID = "V_2" And (oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or oForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            oMatrix = oForm.Items.Item("4").Specific
                            If oMatrix.Columns.Item("V_2").Cells.Item(oMatrix.VisualRowCount).Specific.value <> "" Then
                                oMatrix.AddRow()
                                oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.VisualRowCount).Specific.string = oMatrix.VisualRowCount
                                oMatrix.ClearRowData(oMatrix.VisualRowCount)
                            End If
                        End If
                End Select
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Item Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub

#Region "Reset the Reimbursement Details"
    Sub ResetReimDetails(ByVal FormUID As String, ByVal Code As String)
        Try
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            RS.DoQuery("update [@AIS_EPSSTP3] set U_amt =0 where U_remhed ='" & Code & "'")
            '  objAddOn.objApplication.SetStatusBarMessage("Reimbursement Details Reset Successfully", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Reset Reimbursement Details Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region

#Region "Reset the Leave Details"
    Sub LeaveDetailsReset(ByVal FormUID As String, ByVal Code As String)
        Try
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)


            RS.DoQuery("select * from [@AIS_DLCODE] where U_cfprobper='Y' and Code ='" & Code & "' ")

            If RS.RecordCount > 0 Then
                RS.DoQuery("update [@AIS_EPSSTP6] set U_dayavi=0,U_levbal=U_maxday where U_lcode='" & Code & "' ")
            Else
                RS.DoQuery("update [@AIS_EPSSTP6] set U_dayavi=0,U_levbal=U_maxday where U_lcode='" & Code & "' ")
            End If


            'objAddOn.objApplication.SetStatusBarMessage("Leave Details Reset Successfully", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Reset Leave Details Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region

#Region "Load the Reimbursement Heads and Leave Code's from the Master"
    Sub LoadMatrix()
        Try
            objAddOn.objApplication.SetStatusBarMessage("Please Wait the values are Loading......", SAPbouiCOM.BoMessageTime.bmt_Long, False)
            oForm.Freeze(True)
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            RS.DoQuery("select Code,Name from [@AIS_DREIM] union all select Code,Name from [@AIS_DLCODE] ")
            For IntI As Integer = 1 To RS.RecordCount
                oMatrix.AddRow()
                oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.RowCount).Specific.value = oMatrix.VisualRowCount
                oMatrix.Columns.Item("V_2").Cells.Item(oMatrix.RowCount).Specific.value = RS.Fields.Item("Code").Value
                oMatrix.Columns.Item("V_1").Cells.Item(oMatrix.RowCount).Specific.value = RS.Fields.Item("Name").Value
                RS.MoveNext()
            Next
            oForm.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage("Values are Loaded", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Catch ex As Exception
            oForm.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage("Load Matrix Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
            oForm.Freeze(False)
        End Try

    End Sub
#End Region

End Class
