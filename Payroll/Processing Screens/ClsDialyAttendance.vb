Public Class ClsDialyAttendance
    Public Const formtype = "DialyAttendance"
    Public objForm As SAPbouiCOM.Form
    Dim ShowFolderBrowserThread As Threading.Thread
    Dim FolderBrowser1 As OpenFileDialog
    Dim oEdit As SAPbouiCOM.EditText
    Dim objDialyAttendance As ClsDialyAttendance
    Dim objRS As SAPbobsCOM.Recordset
    Dim StrPath As String
    Dim Str(30) As String


#Region "LoadScreen"
    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("DialyAttendance.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)

    End Sub
#End Region

#Region "ItemEvent"
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction Then
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "3" Then
                            BrowseFolderDialog()
                        ElseIf pVal.ItemUID = "4" Then
                            StrPath = objForm.Items.Item("2").Specific.string
                            ExcelFileReader(StrPath)
                            ' ExcelFileReader("C:\Users\srini.m\Desktop\Attendance  Sheet for payroll process.xls")
                            objForm.Close()
                        End If
                End Select
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "OpenFile"
    Public Sub BrowseFolderDialog()
        Try
            ShowFolderBrowserThread = New System.Threading.Thread(AddressOf ShowFolderBrowser)
            If ShowFolderBrowserThread.ThreadState = Threading.ThreadState.Unstarted Then
                ShowFolderBrowserThread.SetApartmentState(Threading.ApartmentState.STA)
                ShowFolderBrowserThread.Start()
            Else
                If ShowFolderBrowserThread.ThreadState = Threading.ThreadState.Stopped Then
                    ShowFolderBrowserThread.Start()
                    ShowFolderBrowserThread.Join()
                End If
            End If
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.Message)
        End Try
    End Sub
    Public Sub ShowFolderBrowser()
        ShowFolderBrowserThread = Nothing
        Dim FolderBrowser1 As New OpenFileDialog
        Dim shortfilename As String = ""
        Dim P() As Process
        Dim i As Integer
        Dim MyWindow As ClsWindowWrapper
        P = Process.GetProcessesByName("SAP Business One")
        If P.Length <> 0 Then
            ' For i = 0 To P.Length - 1
            MyWindow = New ClsWindowWrapper(P(i).MainWindowHandle)
            If Trim(objAddOn.objCompany.AttachMentPath) <> "" Then
                FolderBrowser1.InitialDirectory = objAddOn.objCompany.AttachMentPath
                FolderBrowser1.Filter = "xlsx (*.xlsx)|*.xlsx|All files (*.*)|*.*"
                FolderBrowser1.FilterIndex = 2
                FolderBrowser1.Multiselect = False
                If FolderBrowser1.CheckPathExists() Then
                    If FolderBrowser1.ShowDialog(MyWindow) = DialogResult.OK Then
                        oEdit = objForm.Items.Item("2").Specific
                        oEdit.String = FolderBrowser1.FileName
                        StrPath = objForm.Items.Item("2").Specific.string
                    Else
                        System.Windows.Forms.Application.ExitThread()
                    End If
                Else
                    objAddOn.objApplication.MessageBox("There is no attachment folder")
                End If
            Else
                objAddOn.objApplication.MessageBox("Specify the Attachment Folder Path in Administation Module")
            End If
            '  Next
        End If

    End Sub
#End Region

#Region "Excel Reader"
    Public Sub ExcelFileReader(ByVal StrPath As String)
        Dim ExcelApp As New Microsoft.Office.Interop.Excel.Application
        Dim ExcelWorkbook As Microsoft.Office.Interop.Excel.Workbook = Nothing
        Dim ExcelWorkSheet As Microsoft.Office.Interop.Excel.Worksheet = Nothing
        Dim excelRng As Microsoft.Office.Interop.Excel.Range
        Try
            ExcelWorkbook = ExcelApp.Workbooks.Open(StrPath)
            ExcelWorkSheet = ExcelWorkbook.ActiveSheet
            excelRng = ExcelWorkSheet.Range("A1")
            Dim RowIndex As Integer = 17
            Dim MonthDays As Integer
            Dim FromDate, ToDate, Month As String
            While excelRng.Range("B" & RowIndex & "").Text <> "" And excelRng.Range("C" & RowIndex & "").Text <> ""
                RowIndex = RowIndex + 1
            End While
            Dim IntMonth, IntYear As Integer
            Dim StrMonth, StrEmpID, StrEmpName, StrStatus As String
            IntMonth = excelRng.Range("C12").Text
            StrMonth = excelRng.Range("C13").Text
            IntYear = excelRng.Range("C14").Text
            MonthDays = get_last_date(IntYear, StrMonth)
            If IntMonth >= 10 Then
                Month = IntMonth
            Else
                Month = "0" + IntMonth
            End If
            FromDate = IntYear.ToString + "-" + Month.ToString + "-" + "01"
            ToDate = IntYear.ToString + "-" + Month.ToString + "-" + MonthDays.ToString

            ' Dim StrSQL As String = "select DocEntry as Result from [@AIS_INOUTTIME] where U_eid='" & StrEmpID & "' and U_date='" & Date2 & "'"
            Dim StrSQL As String = "select DocEntry  from [@AIS_INOUTTIME] where U_date between '" & FromDate & "' and '" & ToDate & "'"
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS.DoQuery(StrSQL)
            If objRS.RecordCount > 0 Then
                StrSQL = "delete from [@AIS_INOUTTIME] where U_date between '" & FromDate & "' and '" & ToDate & "'"
                objRS.DoQuery(StrSQL)
            End If

            Dim i As Integer = 17
            For i = 17 To RowIndex - 1
                Dim oGeneralService As SAPbobsCOM.GeneralService
                Dim oGeneralData As SAPbobsCOM.GeneralData
                Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
                'Dim oChild As SAPbobsCOM.GeneralData
                'Dim oChildren As SAPbobsCOM.GeneralDataCollection
                oGeneralService = objAddOn.objCompany.GetCompanyService.GetGeneralService("AIS_INOUTTIME")
                oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
                oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
                Str(0) = "D"
                Str(1) = "E"
                Str(2) = "F"
                Str(3) = "G"
                Str(4) = "H"
                Str(5) = "I"
                Str(6) = "J"
                Str(7) = "K"
                Str(8) = "L"
                Str(9) = "M"
                Str(10) = "N"
                Str(11) = "O"
                Str(12) = "P"
                Str(13) = "Q"
                Str(14) = "R"
                Str(15) = "S"
                Str(16) = "T"
                Str(17) = "U"
                Str(18) = "V"
                Str(19) = "W"
                Str(20) = "X"
                Str(21) = "Y"
                Str(22) = "Z"
                Str(23) = "AA"
                Str(24) = "AB"
                Str(25) = "AC"
                Str(26) = "AD"
                Str(27) = "AE"
                Str(28) = "AF"
                Str(29) = "AG"
                Str(30) = "AH"
                StrEmpID = excelRng.Range("B" & i & "").Text
                StrEmpName = excelRng.Range("C" & i & "").Text
                Dim j As Integer
                For j = 0 To MonthDays - 1
                    StrStatus = excelRng.Range(Str(j) & i & "").Text
                    objAddOn.objApplication.SetStatusBarMessage("Please Wait...Attendance Loading for " & StrEmpID & " - " & StrEmpName & "", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    Dim Day As Integer
                    Dim Date1 As Date
                    Dim Date2 As String
                    Dim InTime As DateTime
                    Dim OutTime As DateTime
                    Dim OverTime As DateTime
                    Dim TotalTime As DateTime

                    If j + 1 > 10 Then
                        Day = j + 1
                    Else
                        Day = "0" + j + 1
                    End If
                    If IntMonth >= 10 Then
                        Date1 = IntYear.ToString + "-" + IntMonth.ToString + "-" + Day.ToString
                    Else
                        Date1 = IntYear.ToString + "-" + "0" + IntMonth.ToString + "-" + Day.ToString
                    End If
                    Date2 = CDate(Date1).ToString("yyyyMMdd")
                    InTime = InTime.AddHours(0)
                    OutTime = OutTime.AddHours(0)
                    OverTime = OverTime.AddHours(0)
                    TotalTime = TotalTime.AddHours(0)

                    'If Result > 0 Then
                    '    oGeneralParams.SetProperty("DocEntry", objRS.Fields.Item("Result").Value)
                    '    oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                    '    oGeneralData.SetProperty("U_eid", StrEmpID.ToString)
                    '    oGeneralData.SetProperty("U_ename", StrEmpName.ToString)
                    '    oGeneralData.SetProperty("U_date", Date1)
                    '    oGeneralData.SetProperty("U_attd", StrStatus.ToString)
                    '    oGeneralData.SetProperty("U_intime", InTime)
                    '    oGeneralData.SetProperty("U_otime", OutTime)
                    '    oGeneralData.SetProperty("U_ovrtime", OverTime)
                    '    oGeneralData.SetProperty("U_total", TotalTime)
                    '    oGeneralService.Update(oGeneralData)
                    '    ' objAddOn.objApplication.SetStatusBarMessage("Already Posted Attendance to this Employee for this Month " & StrEmpID & " - " & StrEmpName & "", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    'Else
                    oGeneralData.SetProperty("U_eid", StrEmpID.ToString)
                    oGeneralData.SetProperty("U_ename", StrEmpName.ToString)
                    oGeneralData.SetProperty("U_date", Date1)
                    oGeneralData.SetProperty("U_attd", StrStatus.ToString)
                    oGeneralData.SetProperty("U_intime", InTime)
                    oGeneralData.SetProperty("U_otime", OutTime)
                    oGeneralData.SetProperty("U_ovrtime", OverTime)
                    oGeneralData.SetProperty("U_total", TotalTime)
                    oGeneralService.Add(oGeneralData)
                    'End If
                Next
            Next
            ExcelWorkbook.Close()
            ExcelApp.Quit()
            objAddOn.objApplication.SetStatusBarMessage("Attendance Posted", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short)
        End Try
    End Sub
#End Region

#Region "Get Last Date Function"
    Public Function get_last_date(ByVal year As Integer, ByVal mntname As String)
        Dim lastdate As Integer = DatePart(DateInterval.Day, DateSerial(year, Month(CDate(mntname + "1,1990")) + 1, 0))
        Return lastdate
    End Function
#End Region

End Class
