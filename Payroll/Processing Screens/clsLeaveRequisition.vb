﻿Public Class clsLeaveRequisition
    Public Const formtype = "LeaveRequisition"
    Dim objForm As SAPbouiCOM.Form
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim objRS As SAPbobsCOM.Recordset
    Dim Str As String
    Dim objCFL As SAPbouiCOM.ChooseFromListEvent
    Dim objDT As SAPbouiCOM.DataTable
    Dim objItem As SAPbouiCOM.Item
    Dim i As Integer
    Dim objCheckBox As SAPbouiCOM.CheckBox
    Dim objCombo As SAPbouiCOM.ComboBox
    Dim longDateDiff As Long

    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("LeaveRequisition.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        AddFUN()
        RequestNO()
        objForm.AutoManaged = True
        objItem = objForm.Items.Item("9")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        LeaveAPPR()
        Status()
        objMatrix = objForm.Items.Item("28").Specific
        'objMatrix.Columns.Item("V_0").Editable = False
        objForm.Items.Item("24").Enabled = True
        objForm.ActiveItem = "10"
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        If validation(objForm.UniqueID) Then
                        Else
                            BubbleEvent = False
                        End If
                        'ElseIf    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        objMatrix = objForm.Items.Item("28").Specific
                        objCombo = objMatrix.Columns.Item("V_2").Cells.Item(objMatrix.VisualRowCount).Specific
                        If objCombo.Selected Is Nothing Then
                            objMatrix.DeleteRow(objMatrix.VisualRowCount)
                        End If
                        'End If
                    End If
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    If pVal.ItemUID = "21" Then
                        If DateDiffNew() Then
                        Else
                            BubbleEvent = False
                        End If
                    End If
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    objCFL = pVal
                    objDT = objCFL.SelectedObjects
                    If pVal.ItemUID = "10" Then
                        ChooseEmpNO()
                    End If
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        RequestNO()
                        objAddOn.objLeaveRequisition.LoadDocumentDate(objForm.Items.Item("13").Specific)
                        objCombo = objForm.Items.Item("23").Specific
                        objCombo.Select(1, SAPbouiCOM.BoSearchKey.psk_Index)
                        objMatrix = objForm.Items.Item("28").Specific
                        objForm.DataSources.DBDataSources.Item("@AIS_LVREQ1").Clear()
                        objMatrix.AddRow()
                        objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
                        LeaveCode()
                        'AddFUN()
                        RequestNO()
                        Status()
                    ElseIf pVal.ItemUID = "31" Then
                        Cancel()
                    End If
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    objMatrix = objForm.Items.Item("28").Specific
                    If pVal.ItemUID = "28" And pVal.ColUID = "V_2" Then
                        'BalanceDays()
                        ' ApplyingDays()
                        'objCombo = objMatrix.Columns.Item("V_2").Cells.Item(objMatrix.VisualRowCount).Specific
                        'If objCombo.Selected Is Nothing Then
                        'Else
                        '    objMatrix.AddRow()
                        '    objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
                        'End If

                    ElseIf pVal.ItemUID = "28" And pVal.ColUID = "V_0" Then
                        If ValidateLines(objForm.UniqueID, pVal.Row) Then
                        Else
                            BubbleEvent = False
                        End If
                    ElseIf pVal.ItemUID = "21" Then
                        DateDiffr()
                    End If
                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    objMatrix = objForm.Items.Item("28").Specific
                    If pVal.ItemUID = "28" And pVal.ColUID = "V_2" Then
                        LeaveBalDays(FormUID, pVal.Row)
                        ApplyingDays()
                        objCombo = objMatrix.Columns.Item("V_2").Cells.Item(objMatrix.VisualRowCount).Specific
                        If objCombo.Selected Is Nothing Then
                        Else
                            objForm.DataSources.DBDataSources.Item("@AIS_LVREQ1").Clear()
                            objMatrix.AddRow()
                            objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
                        End If

                    End If

                    If pVal.ItemUID = "24" Then
                        objCombo = objForm.Items.Item("24").Specific

                        Manager("" & objCombo.Selected.Value & "")

                    End If
            End Select
        End If
    End Sub

#Region "Leace Balance Days"
    Public Sub LeaveBalDays(ByVal FormUID As String, ByVal RowNo As Integer)
        Dim objRs As SAPbobsCOM.Recordset
        Dim strSQL As String
        objMatrix = objForm.Items.Item("28").Specific
        objCombo = objMatrix.Columns.Item("V_2").Cells.Item(RowNo).Specific
        objRs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        strSQL = "select  T0.U_eid,T0.U_ename,T1.U_lcode,T1.U_levbal from [@AIS_EMPSSTP] T0 join [@AIS_EPSSTP6] T1 on T0.code=T1.Code where T0.U_eid='" & objForm.Items.Item("10").Specific.string & "' and T0.U_ename='" & objForm.Items.Item("11").Specific.string & "' and T1.U_lcode='" & objCombo.Selected.Value & "'"
        objRs.DoQuery(strSQL)
        If objRs.RecordCount > 0 Then
            objMatrix.Columns.Item("V_1").Cells.Item(RowNo).Specific.value = objRs.Fields.Item("U_levbal").Value
        Else
            'objAddOn.objApplication.SetStatusBarMessage("Still not enter the Attendance ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            'Exit Sub
        End If
    End Sub
#End Region

#Region "Leave Requisition Number"
    Public Sub RequestNO()
        Str = "select NextNumber From NNM1 where SeriesName='Primary' and ObjectCode='AIS_LVREQ'"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(Str)
        objForm.Items.Item("9").Specific.string = objRS.Fields.Item("NextNumber").Value

    End Sub
#End Region

#Region "Load Current Date Function"
    Function LoadDocumentDate(ByVal oEditText As SAPbouiCOM.EditText) As Boolean
        Try
            oEditText.Active = True
            oEditText.String = "A"
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("LoadDocumentDate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            Return False
        Finally
        End Try
        Return True
    End Function
#End Region

#Region "Choose From List"
    Public Sub ChooseEmpNO()
        Try
            objForm.Items.Item("10").Specific.value = objDT.GetValue("empID", 0)
        Catch ex As Exception
        End Try
        objCombo = objForm.Items.Item("24").Specific
        objForm.Items.Item("11").Specific.String = objDT.GetValue("firstName", 0) + " " + objDT.GetValue("lastName", 0)
        objForm.Items.Item("12").Specific.String = objDT.GetValue("workStreet", 0)
        objForm.Items.Item("14").Specific.String = objDT.GetValue("workCity", 0)

        '  objForm.Items.Item("36").Specific.String = objDT.GetValue("manager", 0)

        '  Str = "  SELECT distinct c1.firstname+' '+c1.lastName 'manager',c1.lastname 'lastname',c2.firstname+' '+c2.lastName 'employee', c1.manager 'managerid'  FROM ohem AS c1, ohem AS c2  WHERE c1.empid = c2.manager and c2.firstname+' '+c2.lastName='" & objForm.Items.Item("11").Specific.string & "'"

        Str = "select empID,isnull(firstName ,'')+' '+ isnull(middleName,'')+' '+ ISNULL(lastName,'') [manager],userId [USERID] " & _
            " from OHEM where empID =(select manager from OHEM where empID ='" & objForm.Items.Item("10").Specific.string & "')"

        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(Str)

        If CStr(objRS.Fields.Item("USERID").Value) = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Update the Reporting Manager in Employee Master Data", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Else
            objForm.Items.Item("36").Specific.string = objRS.Fields.Item("USERID").Value
            '  objCombo.Select(objRS.Fields.Item("empID").Value, SAPbouiCOM.BoSearchKey.psk_ByDescription)
            ' Manager(objRS.Fields.Item("empID").Value)
        End If

    End Sub
#End Region

#Region "Leave Approval"
    Public Sub LeaveAPPR()
        Dim i As Integer
        Try
            objCombo = objForm.Items.Item("24").Specific
            For Me.i = 1 To objCombo.ValidValues.Count
                objCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
            Next Me.i
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Str = "SELECT distinct c1.firstname+' '+c1.lastName 'manager',c1.lastname 'lastname', c1.manager 'managerid', c1.empid  FROM ohem AS c1, ohem AS c2  WHERE c1.empid = c2.manager "
            objRS.DoQuery(Str)
            For i = 1 To objRS.RecordCount
                objCombo.ValidValues.Add(objRS.Fields.Item("empid").Value, objRS.Fields.Item("manager").Value)
                objRS.MoveNext()
            Next i
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Cancel"
    Public Sub Cancel()
        objCheckBox = objForm.Items.Item("31").Specific
        objCombo = objForm.Items.Item("23").Specific
        If objCheckBox.Checked = True Then
            If objCombo.Selected.Value = "" Then
                objCombo.Select(2, SAPbouiCOM.BoSearchKey.psk_Index)
                objForm.Items.Item("23").Enabled = True
            Else
                objCombo.Select(2, SAPbouiCOM.BoSearchKey.psk_Index)
                objForm.Items.Item("23").Enabled = True
            End If
        ElseIf objCheckBox.Checked = False Then
            objCombo.Select(1, SAPbouiCOM.BoSearchKey.psk_Index)
            objForm.Items.Item("23").Enabled = True
        End If
    End Sub
#End Region

#Region "Applying Days"
    Public Sub ApplyingDays()
        objMatrix = objForm.Items.Item("28").Specific
        objMatrix.Columns.Item("V_0").Cells.Item(objMatrix.VisualRowCount).Specific.value = objForm.Items.Item("22").Specific.value
    End Sub
#End Region

#Region "Leave Code from DataBase"
    Public Sub LeaveCode()
        Dim i As Integer
        objCombo = objMatrix.Columns.Item("V_2").Cells.Item(objMatrix.VisualRowCount).Specific
        For Me.i = 1 To objCombo.ValidValues.Count
            objCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
        Next Me.i
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objMatrix = objForm.Items.Item("28").Specific
        Str = "Select Code,Name from [@AIS_DLCODE]"
        objRS.DoQuery(Str)
        For i = 1 To objRS.RecordCount
            objCombo.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next
    End Sub
#End Region

#Region "Balance Days From Kalpana"
    Public Sub BalanceDays()
        Dim objRS1 As SAPbobsCOM.Recordset
        Dim str1 As String
        Dim maxdays As Double
        Dim tkndays As Double
        Dim baldays As Double

        objMatrix = objForm.Items.Item("28").Specific

        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Str = "select Code,Name,U_maxypd from [@AIS_DLCODE]"
        objRS.DoQuery(Str)
        str1 = "select U_eid,U_ename,U_cl,U_el,U_sl from [@AIS_SUMATTEND1] where U_eid='" & objForm.Items.Item("10").Specific.string & "'"
        objRS1.DoQuery(str1)
        objCombo = objMatrix.Columns.Item("V_2").Cells.Item(objMatrix.VisualRowCount).Specific
        If objCombo.Selected.Description.Contains("cl") Then
            maxdays = objRS.Fields.Item("U_maxypd").Value
            tkndays = objRS1.Fields.Item("U_cl").Value
            baldays = maxdays - tkndays
            objMatrix.Columns.Item("V_1").Cells.Item(objMatrix.VisualRowCount).Specific.String = baldays
        ElseIf objCombo.Selected.Description.Contains("el") Then
            maxdays = objRS.Fields.Item("U_maxypd").Value
            tkndays = objRS1.Fields.Item("U_el").Value
            baldays = maxdays - tkndays
            objMatrix.Columns.Item("V_1").Cells.Item(objMatrix.VisualRowCount).Specific.String = baldays
        ElseIf objCombo.Selected.Description.Contains("sl") Then
            maxdays = objRS.Fields.Item("U_maxypd").Value
            tkndays = objRS1.Fields.Item("U_sl").Value
            baldays = maxdays - tkndays
            objMatrix.Columns.Item("V_1").Cells.Item(objMatrix.VisualRowCount).Specific.String = baldays
        End If
    End Sub
#End Region

#Region "Add Function"
    Public Sub AddFUN()
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.Items.Item("9").Enabled = False
        objForm.Items.Item("10").Enabled = True
        objForm.ActiveItem = "10"
        objAddOn.objLeaveRequisition.LoadDocumentDate(objForm.Items.Item("13").Specific)
        objCombo = objForm.Items.Item("23").Specific
        objCombo.Select(1, SAPbouiCOM.BoSearchKey.psk_Index)
        objMatrix = objForm.Items.Item("28").Specific
        objForm.DataSources.DBDataSources.Item("@AIS_LVREQ1").Clear()
        objMatrix.AddRow()
        objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
        LeaveCode()
        RequestNO()
        Status()
        objForm.ActiveItem = "10"
    End Sub
#End Region

#Region "Status"
    Public Sub Status()
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Str = "select a.userId'userid',b.U_NAME'username',b.USER_CODE'usercode' from ohem a join OUSR b on a.userId=b.USERID where a.userId=b.userid "
        objRS.DoQuery(Str)
        For Me.i = 1 To objRS.RecordCount
            If objAddOn.objCompany.UserSignature = objRS.Fields.Item("userid").Value Then
                objForm.ActiveItem = 22
                objForm.Items.Item("23").Enabled = False
                objForm.Items.Item("24").Enabled = False
            End If
            objRS.MoveNext()
        Next i
    End Sub

#End Region

#Region "Validation"
    Private Function validation(ByRef formuid As String)
        If objForm.Items.Item("10").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("enter emp no", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("20").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("enter from date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("21").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("enter to date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        'Manager()
        Return True
    End Function
#End Region

#Region "Date Difference Calculation"
    Public Sub DateDiffr()
        Dim strFromDate As String
        Dim strToDate As String
        Dim longDateDiff As Long
        strFromDate = objForm.Items.Item("20").Specific.String
        strToDate = objForm.Items.Item("21").Specific.String
        longDateDiff = DateDiff(DateInterval.Day, objAddOn.objGenFunc.GetDateTimeValue(strFromDate), objAddOn.objGenFunc.GetDateTimeValue(strToDate))
        objForm.Items.Item("22").Specific.value = longDateDiff + 1
    End Sub

    Public Function DateDiffNew()
        Dim strFromDate As String
        Dim strToDate As String
        strFromDate = objForm.Items.Item("20").Specific.String
        strToDate = objForm.Items.Item("21").Specific.String
        longDateDiff = DateDiff(DateInterval.Day, objAddOn.objGenFunc.GetDateTimeValue(strFromDate), objAddOn.objGenFunc.GetDateTimeValue(strToDate))
        If longDateDiff < 0 Then
            objAddOn.objApplication.SetStatusBarMessage("To date should be after the from date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Line Level Validation"
    Public Function ValidateLines(ByRef formUID As String, ByVal row As Integer)
        objMatrix = objForm.Items.Item("28").Specific
        If objMatrix.Columns.Item("V_1").Cells.Item(row).Specific.value <> "" Then
            If Val(objMatrix.Columns.Item("V_1").Cells.Item(row).Specific.value) > Val(objMatrix.Columns.Item("V_0").Cells.Item(row).Specific.value) Then
                Return True
            Else
                objAddOn.objApplication.SetStatusBarMessage("Applying days should be less than or equal to balance days", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
        End If
        Return True
    End Function
#End Region

#Region "Manager"
    Public Sub Manager(ByVal EmpID As String)
        '  objCombo = objForm.Items.Item("24").Specific
        ' Str = "select a.userId,b.U_NAME'username',b.USER_CODE'usercode' from ohem a join OUSR b on a.userId=b.USERID where a.userId=b.userid and a.firstname+' '+a.lastName='" & objCombo.Selected.Description & "'"
        Str = "select userid from OHEM where empID ='" & EmpID & "'"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(Str)
        objForm.Items.Item("36").Specific.String = objRS.Fields.Item("username").Value
    End Sub
#End Region

#Region "Alert"
    Public Sub alert()
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Str = "select a.U_reqno,a.U_date,a.U_empno,a.U_empname,a.U_days,a.U_status,a.U_leavappr,b.U_leavcode,b.U_appdays from [@AIS_LVREQ] a join [@AIS_LVREQ1] b on a.DocEntry=b.DocEntry where CheckBox.Checked = True And UPDATE.DATE = GETDATE())"
        'Str += "" + "from [@AIS_LVREQ] a join [@AIS_LVREQ1] b on a.DocEntry=b.DocEntry"
        'Str += "" + "where CheckBox.Checked = True And UPDATE.DATE = GETDATE())"
        objRS.DoQuery(Str)
    End Sub
#End Region

#Region "Find Mode"
    Public Sub FindMode()
        objForm.ActiveItem = "9"
        objForm.Items.Item("11").Enabled = True
        objForm.Items.Item("12").Enabled = True
        objForm.Items.Item("13").Enabled = True
        objForm.Items.Item("14").Enabled = True
        objForm.Items.Item("22").Enabled = True
    End Sub
#End Region

End Class
