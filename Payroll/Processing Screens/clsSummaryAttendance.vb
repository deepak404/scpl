﻿Public Class clsSummaryAttendance
    Dim objForm1 As SAPbouiCOM.Form
    Dim objForm As SAPbouiCOM.Form
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim i As String
    Dim j As Integer
    Public Const formtype = "MNU_SUMATT"
    Dim RS As SAPbobsCOM.Recordset
    Dim SQL As String
    Dim oDBDSHeader, oDBDSDetail As SAPbouiCOM.DBDataSource
    Public Sub loadscreen(ByVal month As String, ByVal intj As String, ByVal year As Integer)
        objForm1 = objAddOn.objUIXml.LoadScreenXML("SummaryAttendance.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm1.State = SAPbouiCOM.BoFormStateEnum.fs_Maximized
        objForm1.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        oDBDSHeader = objForm1.DataSources.DBDataSources.Item(0)
        oDBDSDetail = objForm1.DataSources.DBDataSources.Item(1)

        objAddOn.objgeneralsetting.GeneralSettings()

        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        SQL = "select U_frmdt,U_todt from [@AIS_PDSPC] where Name ='" & month & "'"
        RS.DoQuery(SQL)
        FrmDate = RS.Fields.Item("U_frmdt").Value
        Todate = RS.Fields.Item("U_todt").Value

        objForm1.Items.Item("4").Specific.string = CDate(FrmDate).ToString("yyyyMMdd")
        objForm1.Items.Item("6").Specific.string = CDate(Todate).ToString("yyyyMMdd")

        'objForm1.Items.Item("4").Specific.string = "01" + intj.ToString + year.ToString
        i = get_last_date(year, month)
        ' objForm1.Items.Item("6").Specific.string = i.ToString + intj.ToString + year.ToString
        lastdate = i
        objForm1.ActiveItem = "7"
        objForm1.Items.Item("4").Enabled = False
        objForm1.ActiveItem = "8"
        objForm1.Items.Item("8").Specific.value = objForm1.BusinessObject.GetNextSerialNumber("-1", "AIS_SUMATTEND")
        objForm1.Items.Item("6").Enabled = False
        objMatrix = objForm1.Items.Item("7").Specific
        objMatrix.Columns.Item("V_1").Visible = False
        objMatrix.Columns.Item("V_13").Visible = False
        objForm1.DataBrowser.BrowseBy = 8
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = False Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    'objMatrix = objForm1.Items.Item("7").Specific
                    'If pVal.ColUID = "V_4" Or pVal.ColUID = "V_5" Or pVal.ColUID = "V_6" Or pVal.ColUID = "V_7" Or pVal.ColUID = "V_8" Or pVal.ColUID = "V_9" Or pVal.ColUID = "V_10" Then
                    '    CalPayableDays(FormUID, pVal.Row)
                    'End If
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "1" And objForm1.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        DeleteRecord(FormUID, pVal.Row)
                    End If
                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    objMatrix = objForm1.Items.Item("7").Specific
                    If pVal.ItemUID = "7" And pVal.ColUID = "V_-1" Then
                        Dim FDate, Ldate, EID As String
                        FDate = CDate(objForm1.Items.Item("4").Specific.string).ToString("yyyyMMdd")
                        Ldate = CDate(objForm1.Items.Item("6").Specific.string).ToString("yyyyMMdd")
                        EID = objMatrix.Columns.Item("V_12").Cells.Item(pVal.Row).Specific.string
                        LoadGrid(FDate, Ldate, EID)
                    End If

            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    'objMatrix = objForm1.Items.Item("7").Specific
                    'If pVal.ColUID = "V_10" Or pVal.ColUID = "V_3" Then
                    '    If objForm1.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    '        If Validate(FormUID, pVal.Row) = False Then
                    '            BubbleEvent = False
                    '            Exit Sub
                    '        End If
                    '    End If
                    'End If
                    objMatrix = objForm1.Items.Item("7").Specific
                    If pVal.ColUID = "V_4" Or pVal.ColUID = "V_5" Or pVal.ColUID = "V_6" Or pVal.ColUID = "V_7" Or pVal.ColUID = "V_8" Or pVal.ColUID = "V_9" Or pVal.ColUID = "V_10" Then
                        If CalPayableDays(FormUID, pVal.Row) = False Then
                            BubbleEvent = False
                            Exit Sub
                        End If
                    End If
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Dim Title, ID As String
                    Dim Value As Double
                    If pVal.ItemUID = "1" And objForm1.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        objMatrix = objForm1.Items.Item("7").Specific
                        'This for CL update

                        For j = 1 To objMatrix.VisualRowCount
                            If objMatrix.Columns.Item("V_7").Cells.Item(j).Specific.value <> 0 Then
                                Title = objMatrix.Columns.Item("V_7").Title.ToString
                                ID = objMatrix.Columns.Item("V_12").Cells.Item(j).Specific.value
                                Value = objMatrix.Columns.Item("V_7").Cells.Item(j).Specific.value
                                UpdateLeaveRecords(FormUID, Title, Value, ID)
                            End If
                        Next

                        'This for EL Update

                        For j = 1 To objMatrix.VisualRowCount
                            If objMatrix.Columns.Item("V_6").Cells.Item(j).Specific.value <> 0 Then
                                Title = objMatrix.Columns.Item("V_6").Title.ToString
                                ID = objMatrix.Columns.Item("V_12").Cells.Item(j).Specific.value
                                Value = objMatrix.Columns.Item("V_6").Cells.Item(j).Specific.value
                                UpdateLeaveRecords(FormUID, Title, Value, ID)
                            End If
                        Next

                        'This for SL Update

                        For j = 1 To objMatrix.VisualRowCount
                            If objMatrix.Columns.Item("V_5").Cells.Item(j).Specific.value <> 0 Then
                                Title = objMatrix.Columns.Item("V_5").Title.ToString
                                ID = objMatrix.Columns.Item("V_12").Cells.Item(j).Specific.value
                                Value = objMatrix.Columns.Item("V_5").Cells.Item(j).Specific.value
                                UpdateLeaveRecords(FormUID, Title, Value, ID)
                            End If
                        Next

                    ElseIf pVal.ItemUID = "1" And objForm1.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.ActionSuccess = True Then
                        objForm1.Close()

                    End If

            End Select
        End If
    End Sub

#Region "Update the Leave"
    Public Sub UpdateLeaveRecords(ByVal FormUID As String, ByVal Title As String, ByVal Value As Double, ByVal EID As String)
        Try
            Dim oRS1 As SAPbobsCOM.Recordset
            Dim SQL1 As String = ""
            oRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            SQL1 = "update [@AIS_EPSSTP6] set U_levbal =U_levbal -" & Value & ",U_dayavi =U_dayavi+" & Value & "  from [@AIS_EPSSTP6] T0 " & _
                "join [@AIS_EMPSSTP] T1 on T0.Code=T1.Code " & _
                "where U_lcode ='" & Title & "' and T1.U_eid ='" & EID & "' "
            oRS1.DoQuery(SQL1)
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
#End Region

#Region "Load Grid"
    Public Sub LoadGrid(ByVal Fdate As String, ByVal Ldate As String, ByVal EID As String)
        Dim objRS As SAPbobsCOM.Recordset
        Dim strSQL As String
        '  Dim objGrid As SAPbouiCOM.Grid
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        strSQL = ""

    End Sub
#End Region

#Region "Load Matrix"
    Public Sub loadmatrix(ByVal SName As String, ByVal EName As String, ByVal SDept As String, ByVal EDept As String, ByVal SBranch As String, ByVal EBranch As String, ByVal Month As String, ByVal Year As Integer, ByVal IntMonth As String)
        Try
            Dim Str As String
            Dim objRS As SAPbobsCOM.Recordset
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            'For delete the values an Existing values in DB
            objRS.DoQuery("delete from [@AIS_SUMATTEND1] where U_mnth ='" & IntMonth & "' and U_year ='" & Year & "'")

            'Display the values Based on Selection Criteria:
            If WeekEnd = "Y" Then
                Str = "select (x.P +x.PH+x.TOF +x.CO +x.WH+x.CL+x.SL+x.EL+x.PH) [Payable days] ,* from(select cast(U_eid as int) [Eid] " & _
                " ,max(cast(ISNULL(firstName,'') As nvarchar))+' '+max(CAST( ISNULL(middleName,'')As nvarchar))+' '+max(CAST( ISNULL(lastName,'') as nvarchar)) [Emp Name] " & _
               ",sum( case when U_attd='P' then 1 else 0 end) as P,SUM(case when U_attd='PH' then 0.5 else 0 end) as PH " & _
               " ,SUM (case when U_attd='TOF' then 1 else 0 end) as TOF,SUM(case when U_attd='CO' then 1 else 0 end) as CO " & _
               " ,SUM(case when U_attd='WH' then 1 else 0 end) as WH,SUM(case when U_attd ='L' then 1 else 0 end) as L" & _
               " ,SUM(case when U_attd='A' then 1 else 0 end) A,SUM(case when U_attd='SL' then 1 else 0 end ) as SL " & _
               " ,SUM(case when U_attd='LOP' then 1 else 0 end) as LOP,SUM(case when U_attd='CL' then 1 else 0 end) as CL" & _
               " ,SUM(case when U_attd='EL' then 1 else 0 end) as EL,SUM(case when U_attd='H' then 1 else 0 end) as H" & _
               " ,SUM(case when U_attd='WO' then 1 else 0 end) as WO from [@AIS_INOUTTIME] left outer join OHEM on empID=U_eid  " & _
               " where active='Y' and U_date>='" & CDate(FrmDate).ToString("yyyyMMdd") & "' and U_date<='" & CDate(Todate).ToString("yyyyMMdd") & "' "

            Else

                Str = "select (x.P +x.PH+x.TOF +x.CO +x.WH+x.WO+x.H+x.CL+x.SL+x.EL+x.PH) [Payable days] ,* from(select cast(U_eid as int) [Eid] " & _
                " ,max(cast(ISNULL(firstName,'') As nvarchar))+' '+max(CAST( ISNULL(middleName,'')As nvarchar))+' '+max(CAST( ISNULL(lastName,'') as nvarchar)) [Emp Name] " & _
               ",sum( case when U_attd='P' then 1 else 0 end) as P,SUM(case when U_attd='PH' then 0.5 else 0 end) as PH " & _
               " ,SUM (case when U_attd='TOF' then 1 else 0 end) as TOF,SUM(case when U_attd='CO' then 1 else 0 end) as CO " & _
               " ,SUM(case when U_attd='WH' then 1 else 0 end) as WH,SUM(case when U_attd ='L' then 1 else 0 end) as L" & _
               " ,SUM(case when U_attd='A' then 1 else 0 end) A,SUM(case when U_attd='SL' then 1 else 0 end ) as SL " & _
               " ,SUM(case when U_attd='LOP' then 1 else 0 end) as LOP,SUM(case when U_attd='CL' then 1 else 0 end) as CL" & _
               " ,SUM(case when U_attd='EL' then 1 else 0 end) as EL,SUM(case when U_attd='H' then 1 else 0 end) as H" & _
               " ,SUM(case when U_attd='WO' then 1 else 0 end) as WO from [@AIS_INOUTTIME] left outer join OHEM on empID=U_eid  " & _
               " where active='Y' and U_date>='" & CDate(FrmDate).ToString("yyyyMMdd") & "' and U_date<='" & CDate(Todate).ToString("yyyyMMdd") & "' "
            End If



            If SName.ToString <> "" Then
                Str += vbCrLf + " and empID>='" & SName.ToString & "'"
            End If
            If EName.ToString <> "" Then
                Str += vbCrLf + " and empID<='" & EName.ToString & "'"
            End If
            If SDept.ToString <> "" Then
                Str += vbCrLf + "and dept='" & SDept.ToString & "'"
            End If
            If EDept.ToString <> "" Then
                Str += vbCrLf + "and dept='" & EDept.ToString & "'"
            End If
            If SBranch.ToString <> "" Then
                Str += vbCrLf + "and branch='" & SBranch.ToString & "'"
            End If
            If EBranch.ToString <> "" Then
                Str += vbCrLf + "and branch='" & EBranch.ToString & "'"
            End If
            Str += vbCrLf + "group by U_eid) X order by x.Eid "

            objRS.DoQuery(Str)
            If objRS.RecordCount > 0 Then
                objAddOn.objApplication.SetStatusBarMessage("Please Wait Values are Loading....", SAPbouiCOM.BoMessageTime.bmt_Long, False)
                objForm1.Freeze(True)
                For l As Integer = 0 To objRS.RecordCount - 1

                    'Fill the  Details

                    oDBDSDetail.SetValue("LineId", oDBDSDetail.Size - 1, l + 1)
                    oDBDSDetail.SetValue("U_eid", oDBDSDetail.Size - 1, objRS.Fields.Item("Eid").Value)
                    oDBDSDetail.SetValue("U_ename", oDBDSDetail.Size - 1, objRS.Fields.Item("Emp Name").Value)
                    oDBDSDetail.SetValue("U_pdays", oDBDSDetail.Size - 1, (objRS.Fields.Item("P").Value + objRS.Fields.Item("PH").Value + objRS.Fields.Item("TOF").Value + objRS.Fields.Item("WH").Value))
                    oDBDSDetail.SetValue("U_weekoff", oDBDSDetail.Size - 1, objRS.Fields.Item("WO").Value)
                    oDBDSDetail.SetValue("U_paidholi", oDBDSDetail.Size - 1, objRS.Fields.Item("H").Value)
                    oDBDSDetail.SetValue("U_cl", oDBDSDetail.Size - 1, objRS.Fields.Item("CL").Value + objRS.Fields.Item("PH").Value)
                    oDBDSDetail.SetValue("U_el", oDBDSDetail.Size - 1, objRS.Fields.Item("EL").Value)
                    oDBDSDetail.SetValue("U_sl", oDBDSDetail.Size - 1, objRS.Fields.Item("SL").Value)
                    oDBDSDetail.SetValue("U_compoff", oDBDSDetail.Size - 1, objRS.Fields.Item("CO").Value)
                    oDBDSDetail.SetValue("U_absent", oDBDSDetail.Size - 1, (objRS.Fields.Item("A").Value + objRS.Fields.Item("L").Value))
                    oDBDSDetail.SetValue("U_lop", oDBDSDetail.Size - 1, objRS.Fields.Item("LOP").Value)
                    oDBDSDetail.SetValue("U_ot", oDBDSDetail.Size - 1, 0)
                    oDBDSDetail.SetValue("U_payday", oDBDSDetail.Size - 1, objRS.Fields.Item("Payable days").Value)
                    oDBDSDetail.SetValue("U_month", oDBDSDetail.Size - 1, Month)
                    oDBDSDetail.SetValue("U_year", oDBDSDetail.Size - 1, Year)
                    oDBDSDetail.SetValue("U_mnth", oDBDSDetail.Size - 1, IntMonth)
                    oDBDSDetail.InsertRecord(oDBDSDetail.Size)
                    oDBDSDetail.Offset = oDBDSDetail.Size - 1
                    objRS.MoveNext()
                Next l
                objMatrix.LoadFromDataSourceEx()

                objForm1.Freeze(False)
                objAddOn.objApplication.SetStatusBarMessage("Selected Values are Loaded", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Else
                objAddOn.objApplication.MessageBox("There Is No Matching Record Found Based on Selected Criteria")
            End If
        Catch ex As Exception
            objForm1.Freeze(False)
            objAddOn.objApplication.MessageBox(ex.Message, -5001, vbOKCancel)
        End Try

    End Sub
#End Region

#Region "Get Last Date Function"
    Public Function get_last_date(ByVal year As Integer, ByVal mntname As String)
        Dim lastdate As Integer = DatePart(DateInterval.Day, DateSerial(year, Month(CDate(mntname + "1,1990")) + 1, 0))
        Return lastdate
    End Function
#End Region

#Region "Calculate Payable Days"
    Public Function CalPayableDays(ByVal FormUID As String, ByVal RowNo As Integer) As Boolean
        Dim PD, WOFF, PAID, CL, EL, SL, CO, ABS, LOP, PAYDAYS As Double
        objMatrix = objForm1.Items.Item("7").Specific
        PD = objMatrix.Columns.Item("V_10").Cells.Item(RowNo).Specific.value
        WOFF = objMatrix.Columns.Item("V_9").Cells.Item(RowNo).Specific.value
        PAID = objMatrix.Columns.Item("V_8").Cells.Item(RowNo).Specific.value
        CL = objMatrix.Columns.Item("V_7").Cells.Item(RowNo).Specific.value
        EL = objMatrix.Columns.Item("V_6").Cells.Item(RowNo).Specific.value
        CO = objMatrix.Columns.Item("V_16").Cells.Item(RowNo).Specific.value
        SL = objMatrix.Columns.Item("V_5").Cells.Item(RowNo).Specific.value
        ABS = objMatrix.Columns.Item("V_4").Cells.Item(RowNo).Specific.value
        LOP = objMatrix.Columns.Item("V_14").Cells.Item(RowNo).Specific.value
        PAYDAYS = (PD + WOFF + PAID + CL + EL + SL + CO)
        If CheckPayableDays(FormUID, RowNo, PAYDAYS) = False Then
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Validate Event"
    Public Function Validate(ByVal FormUID As String, ByVal RowNo As Integer) As Boolean
        objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
        If objMatrix.Columns.Item("V_10").Cells.Item(RowNo).Specific.value >= Left(objForm1.Items.Item("4").Specific.string, 2) And objMatrix.Columns.Item("V_10").Cells.Item(RowNo).Specific.value <= Val(Left(objForm1.Items.Item("6").Specific.string, 2)) Or objMatrix.Columns.Item("V_10").Cells.Item(RowNo).Specific.value = "0.0" Then
        Else
            objAddOn.objApplication.SetStatusBarMessage("Please Check Present Days", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False

        End If
        Return True
    End Function
#End Region

#Region "Checking Payable Days"
    Public Function CheckPayableDays(ByVal FormUID As String, ByVal RowNo As Integer, ByVal PAYDAYS As Double) As Boolean
        objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
        Dim StrSQL, Day As String
        Dim objRS As SAPbobsCOM.Recordset
        StrSQL = "select DATEDIFF(dd,'" & CDate(objForm1.Items.Item("4").Specific.string).ToString("yyyyMMdd") & "','" & CDate(objForm1.Items.Item("6").Specific.string).ToString("yyyyMMdd") & "') +1'Day'"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(StrSQL)
        Day = objRS.Fields.Item("Day").Value
        If PAYDAYS <= Day Then
            objMatrix = objForm1.Items.Item("7").Specific
            objMatrix.Columns.Item("V_3").Cells.Item(RowNo).Specific.value = PAYDAYS
            objForm1.Update()
            objForm1.Refresh()
        Else
            objAddOn.objApplication.MessageBox("The Date Exceeds for Payable Days")
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Delete the Record "
    Public Sub DeleteRecord(ByVal FormUID As String, ByVal RowNo As Integer)
        objMatrix = objForm1.Items.Item("7").Specific
        Dim objRS, objRS1 As SAPbobsCOM.Recordset
        Dim strSQL As String
        Dim i As Integer
        objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        For j = 1 To objMatrix.VisualRowCount
            strSQL = "select distinct U_payday,U_absent,U_cl,U_paidholi,U_ot,U_pdays ,U_sl ,U_weekoff,U_el,U_month,U_year from [@AIS_SUMATTEND1] where U_eid ='" & objMatrix.Columns.Item("V_12").Cells.Item(j).Specific.value & "' and U_month='" & objMatrix.Columns.Item("V_1").Cells.Item(j).Specific.value & "' and U_year='" & objMatrix.Columns.Item("V_13").Cells.Item(j).Specific.value & "'"
            objRS1.DoQuery(strSQL)
            If objRS1.RecordCount > 0 Then
                For i = 1 To objRS1.RecordCount
                    strSQL = "delete  from [@AIS_SUMATTEND1] where  U_eid ='" & objMatrix.Columns.Item("V_12").Cells.Item(j).Specific.value & "' and U_month='" & objMatrix.Columns.Item("V_1").Cells.Item(j).Specific.value & "' and U_year='" & objMatrix.Columns.Item("V_13").Cells.Item(j).Specific.value & "'"
                    objRS.DoQuery(strSQL)
                Next
            End If
        Next
    End Sub
#End Region

End Class
