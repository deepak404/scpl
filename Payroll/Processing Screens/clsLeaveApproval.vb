﻿Public Class clsLeaveApproval
    Public Const formtype = "LEAVEAPPROVAL"
    Dim objForm As SAPbouiCOM.Form
    Dim objGrid As SAPbouiCOM.Grid
    Dim objCombo As SAPbouiCOM.ComboBoxColumn
    Dim objCombobox As SAPbouiCOM.ComboBox
    Dim objRS As SAPbobsCOM.Recordset
    Dim str As String
    Dim i As Integer
    Public Sub loadscreen()
        Try
            objForm = objAddOn.objUIXml.LoadScreenXML("leavapproval.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
            objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            objForm.State = SAPbouiCOM.BoFormStateEnum.fs_Maximized
            objGrid = objForm.Items.Item("5").Specific
            LoadGrid()
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.Before_Action = True Then
            objform = objAddOn.objApplication.Forms.Item(FormUID)
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    If pVal.ItemUID = "5" And pVal.ColUID = "Status" Then
                        ComboSelect(pVal.Row)
                    End If
            End Select
        End If
    End Sub

#Region "Approval"
    Public Sub Approval()
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        str = "select a.userId'managerid',b.U_NAME'username',b.USER_CODE'usercode' from ohem a join OUSR b on a.userId=b.USERID where a.userId=b.userid "
        objRS.DoQuery(str)
        If objRS.RecordCount = 0 Then
            objAddOn.objApplication.MessageBox("You are not authorised to open this window", 1, "ok")
        End If
        For Me.i = 1 To objRS.RecordCount
            If objAddOn.objCompany.UserSignature = objRS.Fields.Item("managerid").Value Then
                loadscreen()
                Exit Sub
            Else
                ' objAddOn.objApplication.MessageBox("You are not authorised to open this window", 1, "ok")
            End If
            objRS.MoveNext()
        Next
    End Sub
#End Region

#Region "Load Grid"
    Public Sub LoadGrid()
        i = i + 1
        objGrid.DataTable = objForm.DataSources.DataTables.Add("LEAVAPP" & i)
        str = "select a.U_reqno 'RequestNo',a.U_date 'ReqDate',a.U_empno 'EmpID',a.U_empname 'EmployeeName',a.U_frmdate 'From Date'" & _
            " ,a.U_todate 'to Date',a.U_days 'Leave Days',b.U_leavcode 'Leave Code',b.U_baldays 'Leave Balance',b.U_appdays 'Applying Days'" & _
            " ,a.U_status 'Status',a.U_remarks 'Remarks' from [@AIS_LVREQ] a join [@AIS_LVREQ1] b on a.DocEntry=b.DocEntry " & _
            " where isnull(a.U_cancel,'N')<>'Y' and (isnull(a.U_Status,'U')='U' or a.U_status='') and a.U_manager='" & objAddOn.objCompany.UserSignature & "'"
        objGrid.DataTable.ExecuteQuery(str)
        objGrid.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single
        objGrid.Columns.Item("RequestNo").Editable = False
        objGrid.Columns.Item("ReqDate").Editable = False
        objGrid.Columns.Item("EmpID").Editable = False
        objGrid.Columns.Item("EmployeeName").Editable = False
        objGrid.Columns.Item("From Date").Editable = False
        objGrid.Columns.Item("to Date").Editable = False
        objGrid.Columns.Item("Leave Days").Editable = False
        objGrid.Columns.Item("Leave Code").Editable = False
        objGrid.Columns.Item("Leave Balance").Editable = False
        objGrid.Columns.Item("Applying Days").Editable = False
        objGrid.Columns.Item("Remarks").Editable = False
        objGrid.Columns.Item("Status").Editable = True
        objGrid.Columns.Item("Status").Type = SAPbouiCOM.BoGridColumnType.gct_ComboBox
        objCombo = objGrid.Columns.Item("Status")
        objCombo.ValidValues.Add("U", "Unapproved")
        objCombo.ValidValues.Add("A", "Approved")
        objCombo.ValidValues.Add("R", "Rejected")
    End Sub
#End Region
    
#Region "Combo Box Selection"
    Public Sub ComboSelect(ByVal rowno As Integer)

        Try
            If objCombo.GetSelectedValue(rowno).Value = "A" Then
                ' objForm = objAddOn.objUIXml.LoadScreenXML("LeaveRequisition.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, "Leaverequest")
                objAddOn.objApplication.ActivateMenuItem("MNU_LEAVREQ")
                objForm = objAddOn.objApplication.Forms.ActiveForm
                objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                objForm.Items.Item("9").Specific.String = objGrid.DataTable.GetValue("RequestNo", rowno)
                objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                'objCombobox = objForm.Items.Item("23").Specific
                'objCombobox.Select(0, SAPbouiCOM.BoSearchKey.psk_Index)
                objForm.Items.Item("23").Enabled = True
                objForm.Items.Item("10").Enabled = False
                objForm.Items.Item("20").Enabled = False
                objForm.Items.Item("21").Enabled = False
                objForm.Items.Item("24").Enabled = False

            ElseIf objCombo.GetSelectedValue(rowno).Value = "R" Then
                'objForm = objAddOn.objUIXml.LoadScreenXML("LeaveRequisition.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, "Leaverequest")
                objAddOn.objApplication.ActivateMenuItem("MNU_LEAVREQ")
                objForm = objAddOn.objApplication.Forms.ActiveForm
                objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                objForm.Items.Item("9").Specific.String = objGrid.DataTable.GetValue("RequestNo", rowno)
                objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                'objCombobox = objForm.Items.Item("23").Specific
                'objCombobox.Select(2, SAPbouiCOM.BoSearchKey.psk_Index)
                objForm.Items.Item("23").Enabled = True
                objForm.Items.Item("10").Enabled = False
                objForm.Items.Item("20").Enabled = False
                objForm.Items.Item("21").Enabled = False
                objForm.Items.Item("24").Enabled = False
            Else

            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region

End Class