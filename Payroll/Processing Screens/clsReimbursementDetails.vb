﻿Public Class clsReimbursementDetails
    Public Const FormType = "MNI_ORED"
    Dim oForm As SAPbouiCOM.Form
    Dim oComboMonth, oComboYear As SAPbouiCOM.ComboBox
    Dim oMatrix As SAPbouiCOM.Matrix
    Dim objCFLEvent As SAPbouiCOM.ChooseFromListEvent
    Dim objDataTable As SAPbouiCOM.DataTable
    Dim RS As SAPbobsCOM.Recordset
    Dim SQL, EmpID As String
    Dim IMonth, IYear As Integer
    Public Sub LoadScreen()
        Try
            oForm = objAddOn.objUIXml.LoadScreenXML("ReimbursementEntry.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            oForm.Items.Item("13").Specific.value = oForm.BusinessObject.GetNextSerialNumber("-1", "AIS_ORED")
            oComboMonth = oForm.Items.Item("9").Specific
            oComboYear = oForm.Items.Item("11").Specific
            ComboMonth()
            ComboYear()
            '  ReimbursementSelection()
            oForm.SupportedModes = -1
            oForm.DataBrowser.BrowseBy = 13
            oMatrix = oForm.Items.Item("12").Specific
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.RowCount).Specific.value = oMatrix.RowCount
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Loadscreen Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        Try
                            If pVal.ItemUID = "1" And oForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                If Validate(FormUID, pVal.Row) = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("Before Action - Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try

                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        Try
                            If pVal.ItemUID = "5" Then
                                ChooseFromList(FormUID, pVal)
                            ElseIf pVal.ItemUID = "12" And pVal.ColUID = "V_0" Then
                                ChooseFromListMatrix(FormUID, pVal)
                            End If

                        Catch ex As Exception
                            ' objAddOn.objApplication.SetStatusBarMessage("After Action - Choose From List Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try
                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        Try
                            If pVal.ItemUID = "12" Then
                                If oMatrix.Columns.Item("V_0").Cells.Item(oMatrix.RowCount).Specific.value <> "" Then
                                    oMatrix.AddRow()
                                    oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.RowCount).Specific.value = oMatrix.RowCount
                                End If
                            End If

                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("After Action - Lost Focus Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try
                    Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                        Try
                            If (pVal.ItemUID = "9" Or pVal.ItemUID = "11") Then
                                If (oComboMonth.Selected Is Nothing Or oComboYear.Selected Is Nothing) Then
                                Else
                                    EmpID = oForm.Items.Item("5").Specific.string
                                    IMonth = oComboMonth.Selected.Value
                                    IYear = oComboYear.Selected.Value
                                    CheckFromDB(FormUID, EmpID, IMonth, IYear)
                                End If

                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("After Action - Combo Select Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        Try
                            If pVal.ItemUID = "1" And oForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.ActionSuccess = True Then
                                oForm.Close()
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("After Action - Item Pressed Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try
                End Select
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Item Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

#Region "To Find the Values From the DB"
    Sub CheckFromDB(ByVal FormUID As String, ByVal EID As String, ByVal IntMonth As Integer, ByVal IntYear As Integer)
        Try
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            RS.DoQuery("select T0.DocEntry  from [@AIS_ORED] T0 left outer join [@AIS_RED1] T1 on T0 .DocEntry =T1.DocEntry where T0.U_ecode ='" & EID & "' and T0.U_month =" & IntMonth & " and t0.U_year =" & IntYear & "")
            If RS.RecordCount > 0 Then
                oForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                oForm.Items.Item("13").Specific.value = RS.Fields.Item("DocEntry").Value
                oForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                oForm.Items.Item("5").Enabled = False
                oForm.Items.Item("9").Enabled = False
                oForm.Items.Item("11").Enabled = False
            Else
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Chcek From DB Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region

#Region "Choose From List Event"
    Sub ChooseFromList(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent)
        objCFLEvent = pVal
        objDataTable = objCFLEvent.SelectedObjects
        If objDataTable Is Nothing Then
        Else
            Try
                oForm.Items.Item("5").Specific.value = objDataTable.GetValue("empID", 0)
            Catch ex As Exception

            End Try
            oForm.Items.Item("7").Specific.value = objDataTable.GetValue("firstName", 0) + " " + objDataTable.GetValue("middleName", 0) + " " + objDataTable.GetValue("lastName", 0)
        End If
    End Sub

    Sub ChooseFromListMatrix(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent)
        objCFLEvent = pVal
        objDataTable = objCFLEvent.SelectedObjects
        If objDataTable Is Nothing Then
        Else
            Try
                oMatrix.Columns.Item("V_0").Cells.Item(pVal.Row).Specific.value = objDataTable.GetValue("Code", 0)
            Catch ex As Exception

            End Try
            oMatrix.Columns.Item("V_5").Cells.Item(pVal.Row).Specific.value = objDataTable.GetValue("Name", 0)
            oMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value = objDataTable.GetValue("U_limit", 0)
            oMatrix.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.value = objDataTable.GetValue("U_limit", 0)
        End If
    End Sub
#End Region

#Region "Validation Event"
    Public Function Validate(ByVal FormUID As String, ByVal RowNo As Integer) As Boolean
        Try
            If oForm.Items.Item("5").Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select Employee Code", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            ElseIf oComboMonth.Selected Is Nothing Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select Month", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            ElseIf oComboYear.Selected Is Nothing Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select Year", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            ElseIf oMatrix.RowCount = 0 Then
                objAddOn.objApplication.SetStatusBarMessage("Should be add atleast One Row", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            ElseIf oMatrix.Columns.Item("V_0").Cells.Item(1).Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Should Not Be Add Empty Row", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Validate Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
        Return True
    End Function
#End Region

#Region "Add Month and Year to combo Box "
    Sub ComboMonth()
        oComboMonth.ValidValues.Add("1", "January")
        oComboMonth.ValidValues.Add("2", "February")
        oComboMonth.ValidValues.Add("3", "March")
        oComboMonth.ValidValues.Add("4", "April")
        oComboMonth.ValidValues.Add("5", "May")
        oComboMonth.ValidValues.Add("6", "June")
        oComboMonth.ValidValues.Add("7", "July")
        oComboMonth.ValidValues.Add("8", "August")
        oComboMonth.ValidValues.Add("9", "September")
        oComboMonth.ValidValues.Add("10", "October")
        oComboMonth.ValidValues.Add("11", "November")
        oComboMonth.ValidValues.Add("12", "December")
    End Sub

    Sub ComboYear()
        oComboYear.ValidValues.Add("2012", "2012")
        oComboYear.ValidValues.Add("2013", "2013")
        oComboYear.ValidValues.Add("2014", "2014")
        oComboYear.ValidValues.Add("2015", "2015")
        oComboYear.ValidValues.Add("2016", "2016")
        oComboYear.ValidValues.Add("2017", "2017")
        oComboYear.ValidValues.Add("2018", "2018")
        oComboYear.ValidValues.Add("2019", "2019")
        oComboYear.ValidValues.Add("2020", "2020")
        oComboYear.ValidValues.Add("2021", "2021")
        oComboYear.ValidValues.Add("2022", "2022")
        oComboYear.ValidValues.Add("2023", "2023")
        oComboYear.ValidValues.Add("2024", "2024")
        oComboYear.ValidValues.Add("2025", "2025")

    End Sub
#End Region

#Region "Reimbiursement CFL Conditions"
    Public Sub ReimbursementSelection()
        Dim Objcfl As SAPbouiCOM.ChooseFromList
        Dim objChooseCollection As SAPbouiCOM.ChooseFromListCollection
        Dim objConditions As SAPbouiCOM.Conditions
        Dim objcondition As SAPbouiCOM.Condition

        objChooseCollection = oForm.ChooseFromLists
        Objcfl = objChooseCollection.Item("CFL_3")
        objConditions = Objcfl.GetConditions()
        objcondition = objConditions.Add()
        objcondition.Alias = "U_insal"
        objcondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
        objcondition.CondVal = "N"
        Objcfl.SetConditions(objConditions)
    End Sub
#End Region

#Region "Disable the Employee ID,Month and year clicking Navigation Buttons"
    Public Sub DisableItems()
        oForm.Items.Item("5").Enabled = False
        oForm.Items.Item("7").Enabled = False
        oForm.Items.Item("9").Enabled = False
        oForm.Items.Item("11").Enabled = False
    End Sub
#End Region

#Region "Whem I am Clicking Add Mode "
    Public Sub AddModeFunction()
        oForm.Close()
        LoadScreen()
    End Sub
#End Region

#Region "When I am Clicking FInd Mode"
    Public Sub FindModeFunction()
        oForm.Items.Item("5").Enabled = True
        oForm.Items.Item("7").Enabled = True
        oForm.Items.Item("9").Enabled = True
        oForm.Items.Item("11").Enabled = True
        oForm.ActiveItem = 5
    End Sub
#End Region

#Region "Matrix_sno"
    Public Sub MatrixNo()
        oMatrix = oForm.Items.Item("13").Specific
        For intLoop As Integer = 1 To oMatrix.VisualRowCount
            oMatrix.Columns.Item("V_-1").Cells.Item(intLoop).Specific.value = intLoop
        Next intLoop
    End Sub
#End Region

End Class
