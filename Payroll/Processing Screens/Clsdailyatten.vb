'Daily Attendance

Public Class ClsDailyAtten
    Dim objForm As SAPbouiCOM.Form
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim check As SAPbouiCOM.CheckBox
    Public Sub Define()
        Dim i As Integer
        Dim tablename As String
        For i = 0 To objAddOn.objApplication.Menus.Item("51200").SubMenus.Count - 1
            tablename = objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(i).String
            tablename = tablename.Remove(tablename.IndexOf("-"))
            If Trim(tablename) = "AIS_DDASS" Then
                objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(i).Activate()
                DATTEND = objAddOn.objApplication.Forms.ActiveForm.TypeEx
                Exit For
            End If
        Next i
    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)

        If pVal.BeforeAction = False Then
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    objForm = objAddOn.objApplication.Forms.Item(FormUID)
                    objMatrix = objForm.Items.Item("3").Specific
                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        If ValidateCheckbox(FormUID) = False Then
                            BubbleEvent = False
                            Exit Sub
                        End If
                    End If
            End Select
        End If
    End Sub
    Public Function ValidateCheckbox(ByVal FormUID As String) As Boolean
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        objMatrix = objForm.Items.Item("3").Specific
        Dim i As Integer
        For i = 1 To objMatrix.VisualRowCount
            If objMatrix.Columns.Item("U_default").Cells.Item(i).Specific.Checked = True Then
                If objMatrix.Columns.Item("U_leave").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_compooff").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False

                ElseIf objMatrix.Columns.Item("U_firses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False

                ElseIf objMatrix.Columns.Item("U_secses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False

                ElseIf objMatrix.Columns.Item("U_woutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False

                ElseIf objMatrix.Columns.Item("U_hwithpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False

                ElseIf objMatrix.Columns.Item("U_hwoutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If
            If objMatrix.Columns.Item("U_leave").Cells.Item(i).Specific.Checked = True Then
                If objMatrix.Columns.Item("U_default").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_compooff").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_firses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_secses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_woutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_hwithpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_hwoutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If
            If objMatrix.Columns.Item("U_compooff").Cells.Item(i).Specific.Checked = True Then
                If objMatrix.Columns.Item("U_leave").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_default").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_firses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_secses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_woutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_hwithpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_hwoutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If
            If objMatrix.Columns.Item("U_firses").Cells.Item(i).Specific.Checked = True Then
                If objMatrix.Columns.Item("U_leave").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_default").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_compooff").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_secses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_woutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_hwithpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_hwoutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If
            If objMatrix.Columns.Item("U_secses").Cells.Item(i).Specific.Checked = True Then
                If objMatrix.Columns.Item("U_leave").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_default").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_firses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_compooff").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_woutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_hwithpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_hwoutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If
            If objMatrix.Columns.Item("U_woutpay").Cells.Item(i).Specific.Checked = True Then
                If objMatrix.Columns.Item("U_leave").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_default").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_firses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_secses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_compooff").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_hwithpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_hwoutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If
            If objMatrix.Columns.Item("U_hwithpay").Cells.Item(i).Specific.Checked = True Then
                If objMatrix.Columns.Item("U_leave").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_default").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_firses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_secses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_woutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_compooff").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_hwoutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If
            If objMatrix.Columns.Item("U_hwoutpay").Cells.Item(i).Specific.Checked = True Then
                If objMatrix.Columns.Item("U_leave").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_default").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_firses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_secses").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_woutpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_compooff").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("U_hwithpay").Cells.Item(i).Specific.Checked = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Please Select Any one", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If
        Next
        Return True
    End Function
End Class
