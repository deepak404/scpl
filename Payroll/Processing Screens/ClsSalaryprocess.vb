Public Class ClsSalaryprocess
    Public Const formtype = "Salaryprocess"
    Public objForm As SAPbouiCOM.Form
    Public oActiveForm As SAPbouiCOM.Form
    Public objMatrix As SAPbouiCOM.Matrix
    Public strsql As String
    Public intk As Integer
    Public objrs, oRS, oRS1, oRS2 As SAPbobsCOM.Recordset
    Dim Date1 As String
    Dim last, EID As String
    Dim i As Integer
    Dim oGrid As SAPbouiCOM.Grid
    Dim FromAmount, ToAmount, PTAmount, PT, AlreadyPaid, BasicAmount, Count As Double
    Dim RentPaid, FiftyPer, HRARcd, RntPDTenPer, TransAllow, MedReimb, PF, TotPerquisites, TotInvestment, TotDeduction, Metro, NonMetro As Double
    Dim InFromHousing, RelonHomeLoan, OtherExcemption, GrandTotalIncome, GrossTaxSalary, Minimum, TotalTDSAmount, OtherAllowances, OverTime, EduChess, Surcharge As Double
    Dim SMon, EMon, EYear, SYear As Integer
    Dim PaidTDS, UpperLimit, LowerLimit, Percentage, Gross As Double
    Dim Basic, HRA, DA, Total As Double
    Dim IntNPMnth, IntI, Count80C As Integer
    Dim TDSSlabAmt, TDS As Double
    Dim EndMonth As String
    Dim Amount As Double = 0
    Dim FinYear As String
    Public emplid As String

    Public Sub LoadScreen(ByVal StrDept As String, ByVal StrBranch As String, ByVal Year As Integer, ByVal Month As String, ByVal StrMonth As String)
        Try
            FinYear = FindFinancialYear(Year & Month.ToString & "01", Year & Month.ToString & get_last_date(Year, StrMonth))
            If FinYear = "" Then
                objAddOn.objApplication.StatusBar.SetText("Please Create Financial Period ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Exit Sub
            End If
            objForm = objAddOn.objUIXml.LoadScreenXML("SalaryProcessGrid.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
            objForm.State = SAPbouiCOM.BoFormStateEnum.fs_Maximized
            objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS2 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oGrid = objForm.Items.Item("3").Specific
            oGrid.DataTable = objForm.DataSources.DataTables.Add("DT1")
            objForm.Freeze(True)
            ' last = get_last_date(Year, StrMonth)

            If StrMonth = "February" Then
                oRS1.DoQuery("update [@AIS_EPSSTP2] set U_amt =300 where Code in(select Code  from [@AIS_EMPSSTP] where U_eid in(select empID  from OHEM where workState ='MH' and Active ='Y')) and U_descrpn =(select Name from [@AIS_DDC] where U_pt ='Y' and U_amt =200) ")
            Else
                oRS1.DoQuery("update [@AIS_EPSSTP2] set U_amt =200 where Code in(select Code  from [@AIS_EMPSSTP] where U_eid in(select empID  from OHEM where workState ='MH' and Active ='Y')) and U_descrpn =(select Name from [@AIS_DDC] where U_pt ='Y' and U_amt =300) ")

            End If

            'If StrMonth = "March" Then
            '    oRS1.DoQuery("Select empID from OHEM where active='Y'")
            '    For IntI As Integer = 1 To oRS1.RecordCount
            '        EID = oRS1.Fields.Item("empID").Value
            '        UpdateProfessionalTax(EID, Year - 1, Year, 4, Month)
            '        oRS1.MoveNext()
            '    Next
            'End If
            'If StrMonth <> "March" Then
            '    oRS1.DoQuery("Select empID from OHEM where active='Y'")
            '    For IntI As Integer = 1 To oRS1.RecordCount
            '        EID = oRS1.Fields.Item("empID").Value
            '        UpdatePT200(EID)
            '        oRS1.MoveNext()
            '    Next
            'End If

            objAddOn.objgeneralsetting.GeneralSettings()
            objAddOn.objStatutory.TDSDate()
            If TDSSetting = "Y" Then
                If TDSFromDate.Length = 1 Then
                    SMon = "0" + TDSFromDate
                Else
                    SMon = TDSFromDate
                End If
                If TDSToDate.Length = 1 Then
                    EMon = "0" + TDSFromDate
                Else
                    EMon = TDSFromDate
                End If
                oRS1.DoQuery("Select empID from OHEM where active='Y'")
                For IntI As Integer = 1 To oRS1.RecordCount
                    EID = oRS1.Fields.Item("empID").Value
                    TDSCalcuationAmount(EID, EMon, TDSToYear, SMon, TDSFromYear)
                    oRS1.MoveNext()
                Next
            End If

            oRS.DoQuery("select U_proday [Last] from [@AIS_PDSPC] where Name ='" & StrMonth & "'")
            last = oRS.Fields.Item("Last").Value

            PayCodeColumns(Month, Year, last)
            BenefitCodeColumns()
            DeductionCodeColumns(Month, Year, last)
            ReimbursementColumns()
            LoanandAdvancesColumns()

            '  Columns()
            LoadMatrix(Month, Year, StrDept, StrBranch)

            'If Month.ToString.Length = 1 Then
            '    Month = "0" & Month
            'End If

            'objForm.Items.Item("4").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_ALPS")
            'objMatrix = objForm.Items.Item("15").Specific
            'Date1 = Year.ToString + "-" + Month.ToString + "-" + "01"

            'objrs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            'objrs.DoQuery("select U_proday from [@AIS_PDSPC] where Name='" & StrMonth & "'")
            'last = objrs.Fields.Item("U_proday").Value

            'load(StrDept, StrBranch, Year, Month, StrMonth, last)
        Catch ex As Exception
            objForm.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage("Loadscreen Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
            objForm.Freeze(False)
        End Try
        objForm.Freeze(False)

    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        'If pVal.ItemUID = "1" Then
                        '    If validate(FormUID, pVal.Row) = False Then
                        '        BubbleEvent = False
                        '        objAddOn.objApplication.SetStatusBarMessage("Without Values Can't Add", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        '        Exit Sub
                        '        'objAddOn.objApplication.SetStatusBarMessage("Enter the Mandatory Fields", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        '    End If
                        'objMatrix = objForm.Items.Item("15").Specific
                        'Dim I As Integer
                        'objForm.Freeze(True)
                        'For I = objMatrix.VisualRowCount To 1 Step -1
                        '    If objMatrix.Columns.Item("13").Cells.Item(I).Specific.checked = False Then
                        '        objMatrix.DeleteRow(I)
                        '        'I = I - 1
                        '    End If
                        'Next
                        'objForm.Freeze(False)
                        ' End If

                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        ' LineTotal(pVal.Row)
                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK

                        If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            objAddOn.objApplication.StatusBar.SetText("Please Wait While Salary Processing...", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            SaveToData(FormUID, pVal.Row)
                            objAddOn.objApplication.StatusBar.SetText("Salary Processed.", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        End If

                        'This is for Grid:----------------------------
                        If pVal.ItemUID = "4" Then
                            For Row As Integer = 0 To oGrid.Rows.Count - 1
                                objForm.Freeze(True)
                                oGrid.DataTable.SetValue("Pay", Row, "Y")
                                objForm.Freeze(False)
                            Next

                        ElseIf pVal.ItemUID = "6" Then
                            For Row As Integer = 0 To oGrid.Rows.Count - 1
                                objForm.Freeze(True)
                                oGrid.DataTable.SetValue("Pay", Row, "N")
                                objForm.Freeze(False)
                            Next
                        End If
                        '--------------------------------------------------------------------------------------
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            objForm.Close()
                            '    objAddOn.objPrSal.LoadScreen()

                        End If
                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        Dim OAdd, ODed As Double
                        Dim BA, HRA, Con, Med, Edu, Tel, Reimb, Arr, AOT, Bon, BenPH As Double
                        Dim Loan, DBon, PF, ESI, PT, IT, Cant, DPH, DOT, LWF, TDS As Double
                        If (pVal.ColUID = "V_3" Or pVal.ColUID = "9") Then

                            BA = objMatrix.Columns.Item("V_8").Cells.Item(pVal.Row).Specific.string 'Basic
                            HRA = objMatrix.Columns.Item("V_7").Cells.Item(pVal.Row).Specific.string ' HRA
                            Con = objMatrix.Columns.Item("V_6").Cells.Item(pVal.Row).Specific.string 'Conveyance
                            Med = objMatrix.Columns.Item("V_5").Cells.Item(pVal.Row).Specific.string 'Medical Expenses
                            Edu = objMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.string 'Education Expenses
                            Tel = objMatrix.Columns.Item("V_14").Cells.Item(pVal.Row).Specific.string 'Telephone Expenses
                            Reimb = objMatrix.Columns.Item("5").Cells.Item(pVal.Row).Specific.string 'Total Reimbursement
                            Arr = objMatrix.Columns.Item("7").Cells.Item(pVal.Row).Specific.string 'Total Arrears
                            AOT = objMatrix.Columns.Item("V_15").Cells.Item(pVal.Row).Specific.string 'Over Time Amount
                            OAdd = objMatrix.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.string 'Other Addition
                            Bon = objMatrix.Columns.Item("V_2").Cells.Item(pVal.Row).Specific.string 'Bonus
                            BenPH = objMatrix.Columns.Item("V_1").Cells.Item(pVal.Row).Specific.String  'Paid Holidays

                            'Deductions
                            Loan = objMatrix.Columns.Item("6").Cells.Item(pVal.Row).Specific.string 'Total Loan
                            DBon = objMatrix.Columns.Item("V_0").Cells.Item(pVal.Row).Specific.String   'Bonus Payable
                            PF = objMatrix.Columns.Item("V_13").Cells.Item(pVal.Row).Specific.string 'PF
                            ESI = objMatrix.Columns.Item("V_12").Cells.Item(pVal.Row).Specific.string 'ESI
                            PT = objMatrix.Columns.Item("V_11").Cells.Item(pVal.Row).Specific.string 'PT
                            IT = objMatrix.Columns.Item("V_10").Cells.Item(pVal.Row).Specific.string 'IT
                            Cant = objMatrix.Columns.Item("V_16").Cells.Item(pVal.Row).Specific.string 'Canteen
                            DPH = objMatrix.Columns.Item("V_17").Cells.Item(pVal.Row).Specific.string 'Deducted Payable Holidays
                            DOT = objMatrix.Columns.Item("V_18").Cells.Item(pVal.Row).Specific.string 'Deducted Overtime Amount
                            LWF = objMatrix.Columns.Item("V_20").Cells.Item(pVal.Row).Specific.string 'LWF
                            TDS = objMatrix.Columns.Item("V_19").Cells.Item(pVal.Row).Specific.string 'TDS
                            ODed = objMatrix.Columns.Item("9").Cells.Item(pVal.Row).Specific.string 'Other deduction

                            'Net Earnings,Net Deductions,Net Amount:
                            objMatrix.Columns.Item("10").Cells.Item(pVal.Row).Specific.value = (BA + HRA + Con + Med + Edu + Tel + Reimb + Arr + AOT + Bon + BenPH + OAdd)
                            objMatrix.Columns.Item("11").Cells.Item(pVal.Row).Specific.value = (Loan + DBon + PF + ESI + PT + IT + Cant + DPH + DOT + LWF + TDS + ODed)
                            objMatrix.Columns.Item("12").Cells.Item(pVal.Row).Specific.value = (objMatrix.Columns.Item("10").Cells.Item(pVal.Row).Specific.value - objMatrix.Columns.Item("11").Cells.Item(pVal.Row).Specific.value)
                            objForm.Refresh()
                            objForm.Update()
                        End If

                        'LineTotal(pVal.Row)

                End Select
            End If

        Catch ex As Exception
            objForm.Freeze(False)
            objAddOn.objApplication.MessageBox(ex.Message)
        End Try
    End Sub

#Region "Update the PT 200"
    Sub UpdatePT200(ByVal EID As String)
        Try
            oRS.DoQuery("select sum(U_amt) [Amount] from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP2] T1 on T0.Code =T1.Code where T0.U_eid =" & EID & " and U_descrpn=(select Name from [@AIS_DDC] where U_pt='Y')")

            If oRS.Fields.Item("Amount").Value >= 200 Then

                strsql = "update [@AIS_EPSSTP2] set U_amt=200 " & _
                    " where Code=(select Code from [@AIS_EMPSSTP] where U_eid =" & EID & ") and U_descrpn = (select Name from [@AIS_DDC] where U_pt ='Y')"

                oRS.DoQuery(strsql)
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("UpdatePT200 is failure- " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
#End Region

#Region "Update The Professinal Tax to Employee"
    Sub UpdateProfessionalTax(ByVal EID As String, ByVal SYear As Integer, ByVal EYear As Integer, ByVal SMonth As Integer, ByVal EMonth As Integer)
        Try

            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            'Already Paid Basic and DA Amount:
            'oRS.DoQuery(" select isnull(SUM(convert(float,U_amount) ),0)[Amount] from [@AIS_OSPS] " & _
            '            " where U_heads in('Basic','DA') and U_heads not in('Pay','Remarks') and U_empid =" & EID & "")

            'BasicAmount = oRS.Fields.Item("Amount").Value

            'To get Count payable Month:
            strsql = "select  U_month [Con] from [@AIS_OSPS] where U_empid ='" & EID & "' and U_fyear='" & FinYear & "' group by U_month "
            oRS.DoQuery(strsql)
            Count = oRS.RecordCount


            'To Be paid Basic and DA:
            'oRS.DoQuery("select sum(U_amt) [Amount] from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where T0.U_eid =" & EID & " and U_descrpn in('Basic','DA')")

            oRS.DoQuery("select salary [Amount]  from OHEM where empID='" & EID & "'")


            BasicAmount = oRS.Fields.Item("Amount").Value

            'Professional Tax Slab amount:
            strsql = " select b.U_frmamt [From Amount],U_toamt [To Amount],U_ptamt [PT Amount] from [@AIS_DPTS] A " & _
                " left join  [@AIS_PTS1] B on A.Code =B.Code where U_state =(select workState [State] from " & _
                " OHEM T0 left join [@AIS_EMPSSTP] T1 on T0.empID =T1.U_eid where T1.U_eid =" & EID & ") "

            oRS.DoQuery(strsql)

            For IntI As Integer = 1 To oRS.RecordCount

                FromAmount = oRS.Fields.Item("From Amount").Value
                ToAmount = oRS.Fields.Item("To Amount").Value
                PTAmount = oRS.Fields.Item("PT Amount").Value

                If BasicAmount >= FromAmount And BasicAmount <= ToAmount Then
                    PT = PTAmount * 12
                End If
                oRS.MoveNext()
            Next

            'Update the PT Amount in Employee Salary Setup:
            strsql = "select isnull(SUM(convert(float,U_amount) ),0)[Amount] from [@AIS_OSPS] where U_empid ='" & EID & "' " & _
           " and U_fyear ='" & FinYear & "' and U_heads in (select Name from [@AIS_DDC] where U_pt ='Y') and U_heads not in('Pay','Remarks')"

            oRS.DoQuery(strsql)

            AlreadyPaid = oRS.Fields.Item("Amount").Value


            oRS.DoQuery("select sum(U_amt) [Amount] from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP2] T1 on T0.Code =T1.Code where T0.U_eid =" & EID & " and U_descrpn=(select Name from [@AIS_DDC] where U_pt='Y')")

            If oRS.Fields.Item("Amount").Value = 200 Then
                PT += 100

                strsql = "update [@AIS_EPSSTP2] set U_amt=(select CASE when " & PT & "-" & AlreadyPaid & ">0 then " & PT & "-" & AlreadyPaid & "  else 0 end ) " & _
                   " where Code=(select Code from [@AIS_EMPSSTP] where U_eid =" & EID & ") and U_descrpn = (select Name from [@AIS_DDC] where U_pt ='Y')"

                oRS.DoQuery(strsql)
            End If

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Update Professional Tax Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub

#End Region

#Region "Save the Data Through DI API"

    Public Sub SaveToData(ByVal FormUID As String, ByVal RowNo As Integer)
        Try

            Dim oGeneralService As SAPbobsCOM.GeneralService
            Dim oGeneralData As SAPbobsCOM.GeneralData
            Dim oGeneralParams As SAPbobsCOM.GeneralDataParams
            'Dim oChildren As SAPbobsCOM.GeneralDataCollection
            'Dim oChild As SAPbobsCOM.GeneralData
            Dim EmpID, EmpName, value, Title, Pay As String
            Dim IntMonth, IntYear As Integer
            oGeneralService = objAddOn.objCompany.GetCompanyService.GetGeneralService("AIS_OSPS")
            oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
            oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
            Dim DocEntrySQL, DocEntry As String
            Dim Result As Integer
            Dim DftSeries As String = ""
            Try
                Dim RS1 As SAPbobsCOM.Recordset
                RS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                RS1.DoQuery("select DfltSeries  from ONNM where ObjectCode='AIS_OSPS'")
                '= GetOneValue("select DfltSeries  from ONNM where ObjectCode='AIS_OSPS' ")
                If (RS1.RecordCount > 0) Then
                    DftSeries = RS1.Fields.Item("DfltSeries").Value
                End If

            Catch ex As Exception

            End Try
           

            Dim RS As SAPbobsCOM.Recordset
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            DocEntrySQL = "select count(DocEntry) as Count from [@AIS_OSPS]"
            RS.DoQuery(DocEntrySQL)
            DocEntry = RS.Fields.Item("Count").Value + 1
            '  Dim Cir As Cursor
            Cursor.Current = Cursors.WaitCursor
            For Header As Integer = 0 To oGrid.Rows.Count - 1

                EmpID = oGrid.DataTable.GetValue("Emp ID", Header)
                EmpName = oGrid.DataTable.GetValue("Employee Name", Header)
                Pay = oGrid.DataTable.GetValue("Pay", Header)
                If Pay = "" Then
                    Pay = "N"

                End If
                For Line As Integer = 2 To oGrid.Columns.Count - 3

                    Title = oGrid.Columns.Item(Line).TitleObject.Caption.ToString
                    '   MsgBox(oGrid.Columns.Item(Line).TitleObject.Caption.ToString)

                    Title = oGrid.Columns.Item(Line).TitleObject.Caption.ToString
                    value = oGrid.DataTable.GetValue("" & Title & "", Header)
                    IntMonth = oGrid.DataTable.GetValue("Month", Header)
                    IntYear = oGrid.DataTable.GetValue("Year", Header)
                    Dim SQL As String = "select DocEntry as Result from [@AIS_OSPS] where U_empid='" & EmpID & "' and U_heads='" & Title & "' and U_month='" & IntMonth & "' and U_year='" & IntYear & "'"

                    objrs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    objrs.DoQuery(SQL)
                    Result = objrs.Fields.Item("Result").Value
                    If Result > 0 Then
                        ' oGeneralData.SetProperty("DocEntry", objrs.Fields.Item("Result").Value)
                        oGeneralParams.SetProperty("DocEntry", objrs.Fields.Item("Result").Value)
                        oGeneralData = oGeneralService.GetByParams(oGeneralParams)
                        oGeneralData.SetProperty("U_empid", EmpID.ToString)
                        emplid = EmpID.ToString
                        oGeneralData.SetProperty("U_ename", EmpName.ToString)
                        oGeneralData.SetProperty("U_heads", Title.ToString)
                        oGeneralData.SetProperty("U_amount", value.ToString)
                        oGeneralData.SetProperty("U_month", IntMonth.ToString)
                        oGeneralData.SetProperty("U_year", IntYear.ToString)
                        oGeneralData.SetProperty("U_fyear", FinYear.ToString)
                        oGeneralData.SetProperty("U_pay", Pay.ToString)
                        oGeneralService.Update(oGeneralData) '
                        'objAddOn.objApplication.SetStatusBarMessage("Updated Successfully for - " & EmpName & " - " & IntMonth & "", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Else
                        oGeneralData.SetProperty("U_empid", EmpID.ToString)
                        oGeneralData.SetProperty("U_ename", EmpName.ToString)
                        oGeneralData.SetProperty("U_heads", Title.ToString)
                        oGeneralData.SetProperty("U_amount", value.ToString)
                        oGeneralData.SetProperty("U_month", IntMonth.ToString)
                        oGeneralData.SetProperty("U_year", IntYear.ToString)
                        oGeneralData.SetProperty("U_fyear", FinYear.ToString)
                        oGeneralData.SetProperty("U_pay", Pay.ToString)
                        oGeneralData.SetProperty("Series", DftSeries)

                        'oGeneralData.SetProperty("DocDate", "20160416")
                        oGeneralService.Add(oGeneralData)
                    End If
                Next
            Next
            Cursor.Current = Cursors.Default
            objForm.Close()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Save To Data Functions Failure -'" & emplid & "' " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)

        End Try
    End Sub
#End Region

#Region "Form a Query Group and Load the Grid Values"
    Public Sub LoadMatrix(ByVal Month As String, ByVal year As Integer, Optional ByVal Dept As String = "", Optional ByVal Branch As String = "")
        'This is for Earnings heads and totals:

        strsql = "select * from (select * ,x.Arrears +x.[Over Time] +"
        For PInt As Integer = 0 To PayHead.Length - 1
            Dim PayName As String = PayHead(PInt)
            If PayHead(PInt) = "" Then
            Else
                If PayName <> "EPF" And PayName <> "EESIC" Then
                    strsql += "X.[" & PayHead(PInt) & "]+"
                End If

            End If
        Next

        For BInt As Integer = 0 To BenHeads.Length - 1
            If BenHeads(BInt) = "" Then
            Else
                strsql += "X.[" & BenHeads(BInt) & "]+"
            End If
        Next

        For IntR As Integer = 0 To ReimbHeads.Length - 1
            If ReimbHeads(IntR) = "" Then
            Else
                strsql += "X.[" & ReimbHeads(IntR) & "]+"
            End If
        Next
        strsql = strsql.Substring(0, strsql.Length - 1)
        strsql += " [Net Earnings],"

        'This is for Net Duductions Total:
        For Dint As Integer = 0 To DedHeads.Length - 1
            If DedHeads(Dint) = "" Then
            Else
                strsql += "X.[" & DedHeads(Dint) & "]+"
            End If
        Next

        For Lint As Integer = 0 To LoanHeads.Length - 1
            If LoanHeads(Lint) = "" Then
            Else
                strsql += "X.[" & LoanHeads(Lint) & "]+"
            End If
        Next

        strsql += "0 [Net Deductions], "

        'This is for Net Pay Total:- For Additions:

        If RoundOff = "Y" Then
            strsql += "round((x.Arrears +x.[Over Time] +"
            For PInt As Integer = 0 To PayHead.Length - 1
                Dim payname1 As String = PayHead(PInt)
                If PayHead(PInt) = "" Then
                Else
                    If payname1 <> "EPF" And payname1 <> "EESIC" Then
                        strsql += "X.[" & PayHead(PInt) & "]+"
                    End If

                End If
            Next

            For BInt As Integer = 0 To BenHeads.Length - 1
                If BenHeads(BInt) = "" Then
                Else
                    strsql += "X.[" & BenHeads(BInt) & "]+"
                End If
            Next
            For IntR As Integer = 0 To ReimbHeads.Length - 1
                If ReimbHeads(IntR) = "" Then
                Else
                    strsql += "X.[" & ReimbHeads(IntR) & "]+"
                End If
            Next
            strsql = strsql.Substring(0, strsql.Length - 1)

            strsql += ")-("
            'This for Net Pay Total Deductions:

            For Dint As Integer = 0 To DedHeads.Length - 1
                If DedHeads(Dint) = "" Then
                Else
                    strsql += "X.[" & DedHeads(Dint) & "]+"
                End If
            Next

            For Lint As Integer = 0 To LoanHeads.Length - 1
                If LoanHeads(Lint) = "" Then
                Else
                    strsql += "X.[" & LoanHeads(Lint) & "]+"
                End If
            Next
            strsql += "0),0) [Net Pay], "

        Else
            strsql += "(x.Arrears +x.[Over Time] +"
            For PInt As Integer = 0 To PayHead.Length - 1
                Dim payname2 As String = PayHead(PInt)
                If PayHead(PInt) = "" Then
                Else
                    If payname2 <> "EPF" And payname2 <> "EESIC" Then
                        strsql += "X.[" & PayHead(PInt) & "]+"
                    End If

                End If
            Next

            For BInt As Integer = 0 To BenHeads.Length - 1
                If BenHeads(BInt) = "" Then
                Else
                    strsql += "X.[" & BenHeads(BInt) & "]+"
                End If
            Next
            For IntR As Integer = 0 To ReimbHeads.Length - 1
                If ReimbHeads(IntR) = "" Then
                Else
                    strsql += "X.[" & ReimbHeads(IntR) & "]+"
                End If
            Next
            strsql = strsql.Substring(0, strsql.Length - 1)

            strsql += ")-("
            'This for Net Pay Total Deductions:

            For Dint As Integer = 0 To DedHeads.Length - 1
                If DedHeads(Dint) = "" Then
                Else
                    strsql += "X.[" & DedHeads(Dint) & "]+"
                End If
            Next

            For Lint As Integer = 0 To LoanHeads.Length - 1
                If LoanHeads(Lint) = "" Then
                Else
                    strsql += "X.[" & LoanHeads(Lint) & "]+"
                End If
            Next
            strsql += "0) [Net Pay], "
        End If

        strsql += " convert(varchar(2),'') as 'Pay',convert(varchar(253),'') as 'Remarks',convert(Numeric(3),'" & Month & "') as 'Month',convert(Numeric(6),'" & year & "') as 'Year' "


        'This is for calculating amounts based on theier Attendance:
        strsql += " from (select distinct t0.empID [Emp ID],max(cast(ISNULL(firstName,'') As nvarchar))+' '+max(CAST( ISNULL(middleName,'')As nvarchar))+' '+max(CAST( ISNULL(lastName,'') as nvarchar)) as [Employee Name],'" & last & "' as [Total Days] ,"
        strsql += "(select sum(T4.U_payday)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & Month & "' and T4 .U_year ='" & year & "' ) as [Payable Days],"
        strsql += "(select sum(T4.U_lop)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & Month & "' and T4 .U_year ='" & year & "' ) as 'LOP', "
        'strsql += "(select sum(U_amt) from [@AIS_EPSSTP7] where Code=t1.Code) as 'Loan',"
        strsql += "isnull((select sum(U_total)from [@AIS_EPSSTP4] where Code=t1.Code),0) as 'Arrears'"
        strsql += ",isnull((select U_amount from [@AIS_OVERT] where U_Code=t1.U_eid and U_month =" & Month & " and U_year ='" & year & "'),0) as 'Over Time'"
        '  MsgBox(PayCode.Length)
        For IntP As Integer = 0 To PayCode.Length - 1
            strsql += PayCode(IntP) & ""
        Next

        For IntB As Integer = 0 To Benefits.Length - 1
            strsql += Benefits(IntB)
        Next

        For IntD As Integer = 0 To DedCode.Length - 1
            strsql += DedCode(IntD)
        Next

        For IntR As Integer = 0 To ReimbCode.Length - 1
            strsql += ReimbCode(IntR)
        Next

        For IntL As Integer = 0 To LoanCode.Length - 1
            strsql += LoanCode(IntL)
        Next


        strsql += "from [@AIS_EMPSSTP] t1 left outer join OHEM t0 on t0.empID = t1.U_eid  where T0.active='Y' "

        If Dept.ToString <> "" Then
            strsql += vbCrLf + " and Dept='" & Dept.ToString & "'"
        End If

        If Branch.ToString <> "" Then
            strsql += vbCrLf + " and Branch='" & Branch.ToString & "'"
        End If

        strsql += vbCrLf + " group by empID ,code,U_eid ) X ) Y"

        If ZeroAmount = "N" Then
            strsql += " where Y.[Net Pay] >0"
        End If

        '  If oGrid.DataTable.Rows.Count > 0 Then
        oGrid.DataTable.ExecuteQuery(strsql)
        oGrid.Columns.Item(oGrid.Columns.Count - 4).Type = SAPbouiCOM.BoGridColumnType.gct_CheckBox

        oGrid.Columns.Item(oGrid.Columns.Count - 1).Visible = False
        oGrid.Columns.Item(oGrid.Columns.Count - 2).Visible = False


        For IntRow As Integer = 0 To oGrid.Columns.Count - 5
            oGrid.Columns.Item(IntRow).Editable = False
        Next IntRow

        Dim rowheader As SAPbouiCOM.RowHeaders
        rowheader = oGrid.RowHeaders
        rowheader.TitleObject.Caption = "#"
        For i = 0 To oGrid.Rows.Count - 1
            rowheader.SetText(i, i + 1)
        Next i
        'Else
        'objAddOn.objApplication.SetStatusBarMessage("Please Change the Selection Criteria", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        'End If

        'objrs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        'objrs.DoQuery(strsql)
        'If objrs.RecordCount > 0 Then
        '    For i = 0 To objrs.RecordCount - 1
        '        objMatrix.AddRow()
        '        objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
        '        For j As Integer = 1 To objMatrix.Columns.Count - 6
        '            MsgBox(objMatrix.Columns.Item(j + 1).Title.ToString)
        '            objMatrix.Columns.Item(j).Cells.Item(objMatrix.RowCount).Specific.value = objrs.Fields.Item("" & objMatrix.Columns.Item(j + 1).Title.ToString & "").Value
        '        Next
        '    Next
        'End If
    End Sub
#End Region

#Region "Dynamically Columns Generated for PayCode"
    Public Sub PayCodeColumns(ByVal IntMonth As Integer, ByVal IntYear As Integer, ByVal LastDate As String)
        Try
            Dim RS As SAPbobsCOM.Recordset
            Dim SQL As String
            ' objMatrix = objForm.Items.Item("3").Specific
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            SQL = "select Name from [@AIS_PAYCODE] "
            RS.DoQuery(SQL)
            For i = 0 To RS.RecordCount - 1
                'objMatrix = objForm.Items.Item("3").Specific
                'objMatrix.Columns.Add("P_" & i, SAPbouiCOM.BoFormItemTypes.it_EDIT)
                'objMatrix.Columns.Item("P_" & i).Width = 60
                'objMatrix.Columns.Item("P_" & i).Editable = False
                'objMatrix.Columns.Item("P_" & i).TitleObject.Caption = RS.Fields.Item("Name").Value
                'objMatrix.Columns.Item("P_" & i).DataBind.SetBound(True, "@AIS_SPS1", "U_amount")
                PayHead(i) = RS.Fields.Item("Name").Value
                'PayCode(i) = ",isnull(isnull((select U_amt from [@AIS_EPSSTP1] where U_descrpn ='" & CStr(objMatrix.Columns.Item("P_" & i).TitleObject.Caption.ToString) & "' and Code=t1.Code),0)/'" & LastDate & "' * (select sum(T4.U_payday)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & IntMonth & "' and T4 .U_year ='" & IntYear & "' ),0) as '" & CStr(objMatrix.Columns.Item("P_" & i).TitleObject.Caption.ToString) & "'"
                If RoundOff <> "Y" Then
                    PayCode(i) = ",isnull(isnull((select U_amt from [@AIS_EPSSTP1] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0)/'" & LastDate & "' * (select sum(T4.U_payday)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & IntMonth & "' and T4 .U_year ='" & IntYear & "' ),0) as '" & RS.Fields.Item("Name").Value & "'"
                Else
                    PayCode(i) = ",round(isnull(isnull((select U_amt from [@AIS_EPSSTP1] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0)/'" & LastDate & "' * (select sum(T4.U_payday)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & IntMonth & "' and T4 .U_year ='" & IntYear & "' ),0),0) as '" & RS.Fields.Item("Name").Value & "'"
                End If


                ' TitleObject(i) = CStr(objMatrix.Columns.Item("C_" & i).TitleObject.Caption.ToString)
                RS.MoveNext()
            Next i
            'objForm.Refresh()
            'objForm.Update()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Pay codes Functions Dynamic Columns Problems - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub
#End Region

#Region "Dedction code Columns"
    Public Sub DeductionCodeColumns(ByVal IntMonth As Integer, ByVal IntYear As Integer, ByVal LastDate As Integer)
        Try
            Dim RS As SAPbobsCOM.Recordset
            Dim SQL As String
            ' objMatrix = objForm.Items.Item("3").Specific
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            SQL = "select Name from [@AIS_DDC]"
            RS.DoQuery(SQL)
            For i = 0 To RS.RecordCount - 1
                'objMatrix = objForm.Items.Item("3").Specific
                'objMatrix.Columns.Add("D_" & i, SAPbouiCOM.BoFormItemTypes.it_EDIT)
                'objMatrix.Columns.Item("D_" & i).Width = 60
                'objMatrix.Columns.Item("D_" & i).Editable = False
                'objMatrix.Columns.Item("D_" & i).TitleObject.Caption = RS.Fields.Item("Name").Value
                'objMatrix.Columns.Item("D_" & i).DataBind.SetBound(True, "@AIS_SPS1", "U_amount")
                If RS.Fields.Item("Name").Value = GetOneValue("select Name from [@AIS_DDC] where U_esi ='Y'") Then
                    'DedCode(i) = ",isnull(isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='" & CStr(objMatrix.Columns.Item("D_" & i).TitleObject.Caption.ToString) & "' and Code=t1.Code),0)/'" & LastDate & "' * (select sum(T4.U_payday)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & IntMonth & "' and T4 .U_year ='" & IntYear & "' ),0) as '" & CStr(objMatrix.Columns.Item("D_" & i).TitleObject.Caption.ToString) & "'"
                    If RoundOff <> "Y" Then
                        DedCode(i) = ",isnull(isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0)/'" & LastDate & "' * (select sum(T4.U_payday)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & IntMonth & "' and T4 .U_year ='" & IntYear & "' ),0) as '" & RS.Fields.Item("Name").Value & "'"
                    Else
                        DedCode(i) = ",round(isnull(isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0)/'" & LastDate & "' * (select sum(T4.U_payday)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & IntMonth & "' and T4 .U_year ='" & IntYear & "' ),0),0) as '" & RS.Fields.Item("Name").Value & "'"
                    End If

                End If
                If RS.Fields.Item("Name").Value = GetOneValue("select Name from [@AIS_DDC] where U_pf ='Y'") Then
                    If RS.Fields.Item("Name").Value = GetOneValue("select Name from [@AIS_DDC] where U_pf ='Y'") Then
                        'DedCode(i) = ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='" & CStr(objMatrix.Columns.Item("D_" & i).TitleObject.Caption.ToString) & "' and Code=t1.Code),0) as '" & CStr(objMatrix.Columns.Item("D_" & i).TitleObject.Caption.ToString) & "'"
                        If RoundOff = "Y" Then
                            DedCode(i) = " ,case when isnull(isnull((select U_amt from [@AIS_EPSSTP1] where U_descrpn ='Basic' and Code=t1.Code),0)/'" & LastDate & "' * (select sum(T4.U_payday) " &
                                " from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & IntMonth & "' and T4 .U_year ='" & IntYear & "' ),0)< (select U_pflim from [@AIS_STAT]) then " &
                                  "round(isnull(isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0)/'" & LastDate & "' * (select sum(T4.U_payday)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & IntMonth & "' and T4 .U_year ='" & IntYear & "' ),0),0) " &
                          " else round( isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='PF' and Code=t1.Code),0),0) end as '" & RS.Fields.Item("Name").Value & "'"
                        Else
                            DedCode(i) = " ,case when isnull(isnull((select U_amt from [@AIS_EPSSTP1] where U_descrpn ='Basic' and Code=t1.Code),0)/'" & LastDate & "' * (select sum(T4.U_payday) " &
                                " from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & IntMonth & "' and T4 .U_year ='" & IntYear & "' ),0)< (select U_pflim from [@AIS_STAT]) then " &
                                  "isnull(isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0)/'" & LastDate & "' * (select sum(T4.U_payday)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & IntMonth & "' and T4 .U_year ='" & IntYear & "' ),0) " &
                          " else  isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='PF' and Code=t1.Code),0) end as '" & RS.Fields.Item("Name").Value & "'"
                        End If

                    Else
                        If RoundOff <> "Y" Then
                            DedCode(i) = ",isnull((select case when (U_descrpn='PF' and U_amt<780) then 780 else U_amt end U_amt from [@AIS_EPSSTP2] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0) as '" & RS.Fields.Item("Name").Value & "'"
                        Else
                            DedCode(i) = ",round(isnull((select case when (U_descrpn='PF' and U_amt<780) then 780 else U_amt end U_amt from [@AIS_EPSSTP2] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0),0) as '" & RS.Fields.Item("Name").Value & "'"
                        End If

                    End If
                End If

                If (RS.Fields.Item("Name").Value <> GetOneValue("select Name from [@AIS_DDC] where U_esi ='Y'") Or RS.Fields.Item("Name").Value <> GetOneValue("select Name from [@AIS_DDC] where U_pf ='Y'")) Then
                    If RoundOff <> "Y" Then
                        DedCode(i) = ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0) as '" & RS.Fields.Item("Name").Value & "'"
                    Else
                        DedCode(i) = ",round(isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0),0) as '" & RS.Fields.Item("Name").Value & "'"
                    End If

                End If
                DedHeads(i) = RS.Fields.Item("Name").Value
                ' TitleObject(i) = CStr(objMatrix.Columns.Item("C_" & i).TitleObject.Caption.ToString)
                RS.MoveNext()
            Next i
            'objForm.Refresh()
            'objForm.Update()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Deductions Functions Dynamic Columns Problems - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub
#End Region

#Region "Benefit Code Columns"
    Public Sub BenefitCodeColumns()
        Try
            Dim RS As SAPbobsCOM.Recordset
            Dim SQL As String
            ' objMatrix = objForm.Items.Item("3").Specific
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            SQL = "select Name from [@AIS_BCODE] where U_atv ='Y'"
            RS.DoQuery(SQL)
            If RS.RecordCount > 0 Then

                For i = 0 To RS.RecordCount - 1
                    'objMatrix = objForm.Items.Item("3").Specific
                    'objMatrix.Columns.Add("B_" & i, SAPbouiCOM.BoFormItemTypes.it_EDIT)
                    'objMatrix.Columns.Item("B_" & i).Width = 60
                    'objMatrix.Columns.Item("B_" & i).Editable = False
                    'objMatrix.Columns.Item("B_" & i).TitleObject.Caption = RS.Fields.Item("Name").Value
                    'objMatrix.Columns.Item("B_" & i).DataBind.SetBound(True, "@AIS_SPS1", "U_amount")
                    ' Benefits(i) = ",isnull((select U_amt from [@AIS_EPSSTP5] where U_descrpn ='" & CStr(objMatrix.Columns.Item("B_" & i).TitleObject.Caption.ToString) & "' and Code=t1.Code),0) as '" & CStr(objMatrix.Columns.Item("B_" & i).TitleObject.Caption.ToString) & "'"
                    If RoundOff <> "Y" Then
                        Benefits(i) = ",isnull((select U_amt from [@AIS_EPSSTP5] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0) as '" & RS.Fields.Item("Name").Value & "'"
                    Else
                        Benefits(i) = ",round(isnull((select U_amt from [@AIS_EPSSTP5] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0),0) as '" & RS.Fields.Item("Name").Value & "'"
                    End If
                    BenHeads(i) = RS.Fields.Item("Name").Value
                    ' TitleObject(i) = CStr(objMatrix.Columns.Item("C_" & i).TitleObject.Caption.ToString)
                    RS.MoveNext()
                Next i
            Else
                Array.Clear(Benefits, 0, Benefits.Length)
                Array.Clear(BenHeads, 0, BenHeads.Length)
            End If
            'objForm.Refresh()
            'objForm.Update()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Benefits Functins Dynamic Columns Problems - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try

    End Sub
#End Region

#Region "Reimbursement Columns"
    Sub ReimbursementColumns()
        Try
            Dim RS As SAPbobsCOM.Recordset
            Dim SQL As String
            ' objMatrix = objForm.Items.Item("3").Specific
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            SQL = "select Name from [@AIS_DREIM] where U_insal ='Y' and U_active ='Y'"
            RS.DoQuery(SQL)
            If RS.RecordCount > 0 Then

                For i = 0 To RS.RecordCount - 1
                    If RoundOff <> "Y" Then
                        ReimbCode(i) = ",isnull((select U_amt from [@AIS_EPSSTP3] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0) as '" & RS.Fields.Item("Name").Value & "'"
                    Else
                        ReimbCode(i) = ",round(isnull((select U_amt from [@AIS_EPSSTP3] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0),0) as '" & RS.Fields.Item("Name").Value & "'"
                    End If

                    ReimbHeads(i) = RS.Fields.Item("Name").Value
                    RS.MoveNext()
                Next i
            Else

                Array.Clear(ReimbCode, 0, ReimbCode.Length)
                Array.Clear(ReimbHeads, 0, ReimbHeads.Length)
            End If
            'objForm.Refresh()
            'objForm.Update()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Reimbursement Columns Function Problems - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region

#Region "Loan And Advances Columns"
    Sub LoanandAdvancesColumns()
        Try
            Dim RS As SAPbobsCOM.Recordset
            Dim SQL As String
            ' objMatrix = objForm.Items.Item("3").Specific
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            SQL = "select Name from [@AIS_LONADV] where U_atv ='Y' and U_dfsal ='Y'"
            RS.DoQuery(SQL)
            If RS.RecordCount > 0 Then
                For i = 0 To RS.RecordCount - 1
                    If RoundOff <> "Y" Then
                        LoanCode(i) = ",isnull((select U_amt from [@AIS_EPSSTP7] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0) as '" & RS.Fields.Item("Name").Value & "'"
                    Else
                        LoanCode(i) = ",round(isnull((select U_amt from [@AIS_EPSSTP7] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0),0) as '" & RS.Fields.Item("Name").Value & "'"
                    End If

                    LoanHeads(i) = RS.Fields.Item("Name").Value
                    RS.MoveNext()
                Next i
            Else
                Array.Clear(LoanCode, 0, LoanCode.Length)
                Array.Clear(LoanHeads, 0, LoanHeads.Length)
            End If
            'objForm.Refresh()
            'objForm.Update()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Loan and Advances Columns Function Problems - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region

#Region "Arrears Code Columns Query Generated "
    Sub ArrearsColumns()
        Try
            Dim RS As SAPbobsCOM.Recordset
            Dim SQL As String
            ' objMatrix = objForm.Items.Item("3").Specific
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            SQL = "select Name from [@AIS_LONADV] "
            RS.DoQuery(SQL)
            For i = 0 To RS.RecordCount - 1
                LoanCode(i) = ",isnull((select U_amt from [@AIS_EPSSTP4] where U_descrpn ='" & RS.Fields.Item("Name").Value & "' and Code=t1.Code),0) as '" & RS.Fields.Item("Name").Value & "'"
                LoanHeads(i) = RS.Fields.Item("Name").Value
                RS.MoveNext()
            Next i
            'objForm.Refresh()
            'objForm.Update()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Arrears Columns Function Problems - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region

#Region "Dynamically Added Extra Fileds Like Net Deductions,Net Earnings......."
    Public Sub Columns()
        Try
            objMatrix = objForm.Items.Item("3").Specific
            objMatrix.Columns.Add("C_" & i + 1, SAPbouiCOM.BoFormItemTypes.it_EDIT)
            objMatrix.Columns.Item("C_" & i + 1).Width = 60
            objMatrix.Columns.Item("C_" & i + 1).Editable = True
            objMatrix.Columns.Item("C_" & i + 1).TitleObject.Caption = "Other Additions"
            objMatrix.Columns.Item("C_" & i + 1).DataBind.SetBound(True, "@AIS_SPS1", "U_amount")

            objMatrix = objForm.Items.Item("3").Specific
            objMatrix.Columns.Add("C_" & i + 2, SAPbouiCOM.BoFormItemTypes.it_EDIT)
            objMatrix.Columns.Item("C_" & i + 2).Width = 60
            objMatrix.Columns.Item("C_" & i + 2).Editable = True
            objMatrix.Columns.Item("C_" & i + 2).TitleObject.Caption = "Other Deductions"
            objMatrix.Columns.Item("C_" & i + 2).DataBind.SetBound(True, "@AIS_SPS1", "U_amount")

            objMatrix = objForm.Items.Item("3").Specific
            objMatrix.Columns.Add("C_" & i + 3, SAPbouiCOM.BoFormItemTypes.it_EDIT)
            objMatrix.Columns.Item("C_" & i + 3).Width = 60
            objMatrix.Columns.Item("C_" & i + 3).Editable = False
            objMatrix.Columns.Item("C_" & i + 3).TitleObject.Caption = "Net Earnings"
            objMatrix.Columns.Item("C_" & i + 3).DataBind.SetBound(True, "@AIS_SPS1", "U_amount")

            objMatrix = objForm.Items.Item("3").Specific
            objMatrix.Columns.Add("C_" & i + 4, SAPbouiCOM.BoFormItemTypes.it_EDIT)
            objMatrix.Columns.Item("C_" & i + 4).Width = 60
            objMatrix.Columns.Item("C_" & i + 4).Editable = False
            objMatrix.Columns.Item("C_" & i + 4).TitleObject.Caption = "Net Deduction"
            objMatrix.Columns.Item("C_" & i + 4).DataBind.SetBound(True, "@AIS_SPS1", "U_amount")

            objMatrix = objForm.Items.Item("3").Specific
            objMatrix.Columns.Add("C_" & i + 5, SAPbouiCOM.BoFormItemTypes.it_EDIT)
            objMatrix.Columns.Item("C_" & i + 5).Width = 60
            objMatrix.Columns.Item("C_" & i + 5).Editable = False
            objMatrix.Columns.Item("C_" & i + 5).TitleObject.Caption = "Net Amount"
            objMatrix.Columns.Item("C_" & i + 5).DataBind.SetBound(True, "@AIS_SPS1", "U_amount")

            objMatrix = objForm.Items.Item("3").Specific
            objMatrix.Columns.Add("C_" & i + 6, SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
            objMatrix.Columns.Item("C_" & i + 6).Width = 60
            objMatrix.Columns.Item("C_" & i + 6).Editable = True
            objMatrix.Columns.Item("C_" & i + 6).TitleObject.Caption = "Pay"
            objMatrix.Columns.Item("C_" & i + 6).DataBind.SetBound(True, "@AIS_SPS1", "U_amount")

            objMatrix = objForm.Items.Item("3").Specific
            objMatrix.Columns.Add("C_" & i + 7, SAPbouiCOM.BoFormItemTypes.it_EDIT)
            objMatrix.Columns.Item("C_" & i + 7).Width = 150
            objMatrix.Columns.Item("C_" & i + 7).Editable = True
            objMatrix.Columns.Item("C_" & i + 7).TitleObject.Caption = "Remarks"
            objMatrix.Columns.Item("C_" & i + 7).DataBind.SetBound(True, "@AIS_SPS1", "U_amount")

            objMatrix = objForm.Items.Item("3").Specific
            objMatrix.Columns.Add("C_" & i + 8, SAPbouiCOM.BoFormItemTypes.it_EDIT)
            objMatrix.Columns.Item("C_" & i + 8).Width = 60
            objMatrix.Columns.Item("C_" & i + 8).Editable = False
            objMatrix.Columns.Item("C_" & i + 8).TitleObject.Caption = "Month"
            objMatrix.Columns.Item("C_" & i + 8).DataBind.SetBound(True, "@AIS_SPS1", "U_amount")

            objMatrix = objForm.Items.Item("3").Specific
            objMatrix.Columns.Add("C_" & i + 9, SAPbouiCOM.BoFormItemTypes.it_EDIT)
            objMatrix.Columns.Item("C_" & i + 9).Width = 60
            objMatrix.Columns.Item("C_" & i + 9).Editable = False
            objMatrix.Columns.Item("C_" & i + 9).TitleObject.Caption = "Year"
            objMatrix.Columns.Item("C_" & i + 9).DataBind.SetBound(True, "@AIS_SPS1", "U_amount")
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Dynamic Columns Funtions Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region

#Region "Load the Values in the Matrix"
    Public Sub load(ByVal StrDept As String, ByVal StrBranch As String, ByVal Year As String, ByVal Month As String, ByVal StrMonth As String, ByVal last As String)
        Try
            objAddOn.objApplication.SetStatusBarMessage("Please Wait Values are Loading........", SAPbouiCOM.BoMessageTime.bmt_Long, False)
            objForm.Freeze(True)

            strsql = "select distinct t0.empID,t0.firstName+' '+t0.lastName as Name,T1.U_ptotal Pay,t1.U_dtotal Ded,t1.U_rtotal Reimb,T0.U_incent as Incent,"
            strsql += "(select sum(T4.U_payday)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & Month & "' and T4 .U_year ='" & Year & "' ) as 'Present',"
            strsql += "(select sum(T4.U_lop)  from [@AIS_SUMATTEND1] T4 where T4.U_eid =t0.empID and t4.U_mnth='" & Month & "' and T4 .U_year ='" & Year & "' ) as 'LOP',"
            strsql += "(select sum(U_amt) from [@AIS_EPSSTP7] where Code=t1.Code) as 'Loan',"
            strsql += "(select sum(U_htotal)from [@AIS_EPSSTP4] where Code=t1.Code) as 'Arrear'"
            strsql += ",isnull((select U_amount from [@AIS_OVERT] where U_Code=t1.U_eid and U_month =" & Month & " and U_year ='" & Year & "'),0) as 'OverTime'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP1] where U_descrpn ='Basic' and Code=t1.Code),0) as 'Basic'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP1] where U_descrpn ='HRA' and Code=t1.Code),0) as 'HRA'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP1] where U_descrpn ='Conveyance' and Code=t1.Code),0) as 'Con'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP1] where U_descrpn ='Med All' and Code=t1.Code),0) as 'Medical'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP1] where U_descrpn ='Edu All' and Code=t1.Code),0) as 'Education'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP1] where U_descrpn ='Telephone' and Code=t1.Code),0) as 'Telephone'"

            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='PF' and Code=t1.Code),0) as 'PF'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='ESI' and Code=t1.Code),0) as 'ESI'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='PT' and Code=t1.Code),0) as 'PT'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='IT' and Code=t1.Code),0) as 'IT'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='Canteen' and Code=t1.Code),0) as 'Canteen'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='Paid Holidays' and Code=t1.Code),0) as 'Ded Paid Holidays' "
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='OT' and Code=t1.Code),0) as 'DOT'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='LWF' and Code=t1.Code),0) as 'LWF'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='TDS' and Code=t1.Code),0) as 'TDS'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='Salary Advance' and Code=t1.Code),0) as 'Adv'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP2] where U_descrpn ='Bonus Payable' and Code=t1.Code),0) as 'Bonus Payable'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP5] where U_descrpn ='Bonus' and Code=t1.Code),0) as 'Bonus'"
            'strsql += ",isnull((select U_amt from [@AIS_EPSSTP5] where U_descrpn ='Paid Holidays' and Code=t1.Code),0) as 'Ben Paid Holidays' from [@AIS_EMPSSTP] t1"
            strsql += " join OHEM t0 on t0.empID = t1.U_eid  where 1=1 "

            'If Date1.ToString <> "" Then
            '    strsql += vbCrLf + " and Convert(Date,j.U_sdate,103)='" & Date1 & "'"
            'End If

            If StrDept.ToString <> "" Then
                strsql += vbCrLf + " and b.Name='" & StrDept & "'"
            End If
            If StrBranch.ToString <> "" Then
                strsql += vbCrLf + " and c.Name = '" & StrBranch & "'"
            End If
            strsql += "group by t1.U_eid ,t0.empID ,t0.firstName ,t0.lastName,T1.U_ptotal,t1.U_dtotal ,t1.U_rtotal,t1.Code,t0.U_incent "

            objrs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objrs.DoQuery(strsql)
            objForm.Items.Item("9").Specific.value = StrDept
            objForm.Items.Item("8").Specific.value = StrBranch
            objForm.Items.Item("7").Specific.value = Year
            objForm.Items.Item("10").Specific.value = Month
            If objrs.RecordCount > 0 Then
                For intk = 1 To objrs.RecordCount
                    objMatrix.AddRow()
                    objMatrix.Columns.Item("1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
                    ' Dim BP, HRA, Con, Med, Edu, Tel, PF, PDays As Double
                    ' PDays = objrs.Fields.Item("Present").Value - objrs.Fields.Item("LOP").Value

                    objMatrix.Columns.Item("2").Cells.Item(intk).Specific.string = objrs.Fields.Item("EmpID").Value
                    objMatrix.Columns.Item("3").Cells.Item(intk).Specific.String = objrs.Fields.Item("Name").Value
                    objMatrix.Columns.Item("V_9").Cells.Item(intk).Specific.string = last
                    objMatrix.Columns.Item("V_21").Cells.Item(intk).Specific.string = Year
                    objMatrix.Columns.Item("V_22").Cells.Item(intk).Specific.string = Month
                    objMatrix.Columns.Item("4").Cells.Item(intk).Specific.string = objrs.Fields.Item("Present").Value
                    objMatrix.Columns.Item("V_8").Cells.Item(intk).Specific.string = objrs.Fields.Item("Basic").Value / last * objrs.Fields.Item("Present").Value
                    objMatrix.Columns.Item("V_7").Cells.Item(intk).Specific.string = objrs.Fields.Item("HRA").Value / last * objrs.Fields.Item("Present").Value
                    objMatrix.Columns.Item("V_6").Cells.Item(intk).Specific.string = objrs.Fields.Item("Con").Value / last * objrs.Fields.Item("Present").Value
                    objMatrix.Columns.Item("V_5").Cells.Item(intk).Specific.string = objrs.Fields.Item("Medical").Value / last * objrs.Fields.Item("Present").Value
                    objMatrix.Columns.Item("V_4").Cells.Item(intk).Specific.string = objrs.Fields.Item("Education").Value / last * objrs.Fields.Item("Present").Value
                    objMatrix.Columns.Item("V_14").Cells.Item(intk).Specific.string = objrs.Fields.Item("Telephone").Value / last * objrs.Fields.Item("Present").Value
                    objMatrix.Columns.Item("5").Cells.Item(intk).Specific.string = objrs.Fields.Item("Reimb").Value
                    objMatrix.Columns.Item("7").Cells.Item(intk).Specific.string = objrs.Fields.Item("Arrear").Value
                    objMatrix.Columns.Item("V_15").Cells.Item(intk).Specific.string = objrs.Fields.Item("OverTime").Value
                    objMatrix.Columns.Item("V_3").Cells.Item(intk).Specific.string = 0.0 'Other Addition
                    objMatrix.Columns.Item("V_2").Cells.Item(intk).Specific.string = objrs.Fields.Item("Bonus").Value 'Bonus
                    objMatrix.Columns.Item("V_1").Cells.Item(intk).Specific.String = objrs.Fields.Item("Ben Paid Holidays").Value 'Paid Holidays

                    'Deductions
                    objMatrix.Columns.Item("6").Cells.Item(intk).Specific.string = objrs.Fields.Item("Loan").Value
                    objMatrix.Columns.Item("V_0").Cells.Item(intk).Specific.String = objrs.Fields.Item("Bonus Payable").Value 'Bonus Payable
                    objMatrix.Columns.Item("V_13").Cells.Item(intk).Specific.string = objrs.Fields.Item("PF").Value / last * objrs.Fields.Item("Present").Value
                    objMatrix.Columns.Item("V_12").Cells.Item(intk).Specific.string = objrs.Fields.Item("ESI").Value
                    objMatrix.Columns.Item("V_11").Cells.Item(intk).Specific.string = objrs.Fields.Item("PT").Value
                    objMatrix.Columns.Item("V_10").Cells.Item(intk).Specific.string = objrs.Fields.Item("IT").Value
                    objMatrix.Columns.Item("V_16").Cells.Item(intk).Specific.string = objrs.Fields.Item("Canteen").Value
                    objMatrix.Columns.Item("V_17").Cells.Item(intk).Specific.string = objrs.Fields.Item("Ded Paid Holidays").Value
                    objMatrix.Columns.Item("V_18").Cells.Item(intk).Specific.string = objrs.Fields.Item("DOT").Value
                    objMatrix.Columns.Item("V_20").Cells.Item(intk).Specific.string = objrs.Fields.Item("LWF").Value
                    objMatrix.Columns.Item("V_19").Cells.Item(intk).Specific.string = objrs.Fields.Item("TDS").Value
                    objMatrix.Columns.Item("V_23").Cells.Item(intk).Specific.string = objrs.Fields.Item("Adv").Value
                    objMatrix.Columns.Item("9").Cells.Item(intk).Specific.string = 0.0 'Other deduction
                    Dim Ear, Ded As Double
                    Ear = (objrs.Fields.Item("Basic").Value / last * objrs.Fields.Item("Present").Value + _
                          objrs.Fields.Item("HRA").Value / last * objrs.Fields.Item("Present").Value + _
                          objrs.Fields.Item("Con").Value / last * objrs.Fields.Item("Present").Value + _
                          objrs.Fields.Item("Medical").Value / last * objrs.Fields.Item("Present").Value + _
                          objrs.Fields.Item("Education").Value / last * objrs.Fields.Item("Present").Value + _
                          objrs.Fields.Item("Telephone").Value / last * objrs.Fields.Item("Present").Value + objrs.Fields.Item("Reimb").Value + _
                   objrs.Fields.Item("Arrear").Value + objrs.Fields.Item("OverTime").Value + objrs.Fields.Item("Bonus").Value + _
                    objrs.Fields.Item("Ben Paid Holidays").Value)

                    Ded = (objrs.Fields.Item("Loan").Value + objrs.Fields.Item("Bonus Payable").Value + objrs.Fields.Item("PF").Value / last * objrs.Fields.Item("Present").Value + objrs.Fields.Item("ESI").Value + _
                        objrs.Fields.Item("PT").Value + objrs.Fields.Item("IT").Value + objrs.Fields.Item("Canteen").Value + objrs.Fields.Item("Ded Paid Holidays").Value + _
                       objrs.Fields.Item("DOT").Value + objrs.Fields.Item("LWF").Value + objrs.Fields.Item("TDS").Value + objrs.Fields.Item("Adv").Value)

                    objMatrix.Columns.Item("10").Cells.Item(intk).Specific.string = Ear 'Net Earnings
                    objMatrix.Columns.Item("11").Cells.Item(intk).Specific.string = Ded 'Net Deductions
                    objMatrix.Columns.Item("12").Cells.Item(intk).Specific.string = Ear - Ded  'Net Pay


                    'Dim objRS1 As SAPbobsCOM.Recordset
                    'objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    'Dim strSQL1 As String
                    'strSQL1 = "select DocEntry from [@AIS_ALPS] T0 join [@AIS_ALPSL] T1 on T0.DocEntry=T1.DocEntry where T0.U_month='" & Month & "' and T0.U_Year='" & Year & "'"
                    'objRS1.DoQuery(strSQL1)
                    'If objRS1.RecordCount > 0 Then
                    '        objMatrix = objForm.Items.Item("15").Specific
                    '        objMatrix.Columns.Item("2").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_empid").Value
                    '        objMatrix.Columns.Item("3").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_empname").Value
                    '        objMatrix.Columns.Item("4").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_payday").Value
                    '        objMatrix.Columns.Item("5").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_treim").Value
                    '        objMatrix.Columns.Item("6").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_tloan").Value
                    '        objMatrix.Columns.Item("7").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_tarrer").Value
                    '        objMatrix.Columns.Item("8").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_tot").Value
                    '        objMatrix.Columns.Item("V_3").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_oadd").Value
                    '        objMatrix.Columns.Item("V_0").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_oadd1").Value
                    '        objMatrix.Columns.Item("V_2").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_ind").Value
                    '        objMatrix.Columns.Item("V_1").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_team").Value
                    '        objMatrix.Columns.Item("9").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_oded").Value
                    '        objMatrix.Columns.Item("10").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_near").Value
                    '        objMatrix.Columns.Item("11").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_nded").Value
                    '        objMatrix.Columns.Item("12").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_npay").Value

                    '        objMatrix.Columns.Item("V_9").Cells.Item(intk).Specific.string = objRS1.Fields.Item("U_tdays").Value
                    '        objMatrix.Columns.Item("V_8").Cells.Item(intk).Specific.string = objRS1.Fields.Item("U_basic").Value
                    '        objMatrix.Columns.Item("V_7").Cells.Item(intk).Specific.string = objRS1.Fields.Item("U_hra").Value
                    '        objMatrix.Columns.Item("V_6").Cells.Item(intk).Specific.string = objRS1.Fields.Item("U_con").Value
                    '        objMatrix.Columns.Item("V_5").Cells.Item(intk).Specific.string = objRS1.Fields.Item("U_per").Value
                    '        objMatrix.Columns.Item("V_4").Cells.Item(intk).Specific.string = objRS1.Fields.Item("U_ivbp").Value
                    '        objMatrix.Columns.Item("V_13").Cells.Item(intk).Specific.string = objRS1.Fields.Item("U_pf").Value
                    '        objMatrix.Columns.Item("V_12").Cells.Item(intk).Specific.string = objRS1.Fields.Item("U_esi").Value
                    '        objMatrix.Columns.Item("V_11").Cells.Item(intk).Specific.string = objRS1.Fields.Item("U_pt").Value
                    '        objMatrix.Columns.Item("V_10").Cells.Item(intk).Specific.string = objRS1.Fields.Item("U_it").Value



                    '        Dim objcheck As SAPbouiCOM.CheckBox
                    '        objcheck = objMatrix.Columns.Item("13").Cells.Item(intk).Specific
                    '        If objRS1.Fields.Item("U_pay").Value = "Y" Then
                    '            objcheck.Checked = True
                    '        Else
                    '            objcheck.Checked = False
                    '        End If
                    '        objMatrix.Columns.Item("14").Cells.Item(intk).Specific.value = objRS1.Fields.Item("U_remark").Value
                    'End If
                    objrs.MoveNext()
                Next intk
                objForm.Freeze(False)
                objAddOn.objApplication.SetStatusBarMessage("Selected values are Loaded", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Else
                objAddOn.objApplication.SetStatusBarMessage("No Matching Records Found", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                objForm.Freeze(False)
            End If

        Catch ex As Exception
            objForm.Freeze(False)
            objAddOn.objApplication.MessageBox(ex.Message)
            objForm.Freeze(False)
        End Try

    End Sub
#End Region

#Region "Delete the Record "
    Public Sub DeleteRecord(ByVal FormUID As String, ByVal RowNo As Integer)
        objMatrix = objForm.Items.Item("15").Specific
        Dim objRS, objRS1 As SAPbobsCOM.Recordset
        Dim strSQL, strSQL1 As String
        Dim i, j As Integer

        objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        'MsgBox(objMatrix.VisualRowCount)
        For j = 1 To objMatrix.VisualRowCount
            strSQL = "select * from [@AIS_ALPS] T0 join [@AIS_ALPSL] T1 on T0.DocEntry=T1.DocEntry where T1.U_empid='" & objMatrix.Columns.Item("2").Cells.Item(j).Specific.value & "' and T0.U_month='" & objForm.Items.Item("10").Specific.value & "' and T0.U_Year='" & objForm.Items.Item("7").Specific.value & "'"
            objRS1.DoQuery(strSQL)
            If objRS1.RecordCount > 0 Then
                For i = 1 To objRS1.RecordCount
                    strSQL1 = "delete  from [@AIS_ALPSL] where  DocEntry ='" & objRS1.Fields.Item("DocEntry").Value & "' and U_empid = '" & objRS1.Fields.Item("U_empid").Value & "' and U_month = '" & objRS1.Fields.Item("U_month").Value & "' and U_year = '" & objRS1.Fields.Item("U_year").Value & "'"
                    objRS.DoQuery(strSQL1)
                    objRS1.MoveNext()
                Next
            End If
        Next
    End Sub
#End Region

#Region "Line Total"
    'Public Sub LineTotal(ByVal RowNo)

    '    Dim IntC, IntD, IntE, IntF, IntG, IntH, IntI, IntJ, IntK, IntL As Double

    '    If objMatrix.Columns.Item("4").Cells.Item(RowNo).Specific.string = "" Then
    '        IntC = 0
    '    Else
    '        IntC = objMatrix.Columns.Item("4").Cells.Item(RowNo).Specific.value
    '    End If
    '    If objMatrix.Columns.Item("5").Cells.Item(RowNo).Specific.string = "" Then
    '        IntD = 0
    '    Else
    '        IntD = objMatrix.Columns.Item("5").Cells.Item(RowNo).Specific.value
    '    End If
    '    If objMatrix.Columns.Item("6").Cells.Item(RowNo).Specific.string = "" Then
    '        IntE = 0
    '    Else
    '        IntE = objMatrix.Columns.Item("6").Cells.Item(RowNo).Specific.value
    '    End If
    '    If objMatrix.Columns.Item("7").Cells.Item(RowNo).Specific.string = "" Then
    '        IntF = 0
    '    Else
    '        IntF = objMatrix.Columns.Item("7").Cells.Item(RowNo).Specific.value
    '    End If
    '    If objMatrix.Columns.Item("8").Cells.Item(RowNo).Specific.string = "" Then
    '        IntG = 0
    '    Else
    '        IntG = objMatrix.Columns.Item("8").Cells.Item(RowNo).Specific.value
    '    End If
    '    If objMatrix.Columns.Item("V_0").Cells.Item(RowNo).Specific.string = "" Then
    '        IntH = 0
    '    Else
    '        IntH = objMatrix.Columns.Item("V_0").Cells.Item(RowNo).Specific.value
    '    End If
    '    If objMatrix.Columns.Item("9").Cells.Item(RowNo).Specific.string = "" Then
    '        IntI = 0
    '    Else
    '        IntI = objMatrix.Columns.Item("9").Cells.Item(RowNo).Specific.value
    '    End If

    '    IntJ = IntD + IntF + IntG + IntH + TM + INDI + OAdd + EBP + EHRA + ECON + EPP + EIVBP
    '    objMatrix.Columns.Item("10").Cells.Item(RowNo).Specific.value = IntJ
    '    IntK = IntE + IntI + DPF + DPT + DESI + DIT
    '    objMatrix.Columns.Item("11").Cells.Item(RowNo).Specific.value = IntK
    '    IntL = IntJ - IntK
    '    objMatrix.Columns.Item("12").Cells.Item(RowNo).Specific.value = IntL
    '    objForm.Update()
    '    objForm.Refresh()
    'End Sub
#End Region

#Region "Get Last Date Function"
    Public Function get_last_date(ByVal year As Integer, ByVal mntname As String)
        Dim lastdate As Integer = DatePart(DateInterval.Day, DateSerial(year, Month(CDate(mntname + "1,1990")) + 1, 0))
        Return lastdate
    End Function
#End Region

#Region "Function Get One Value From DB"

    Public Function GetOneValue(ByVal SQL As String) As String
        Try
            Dim oRS3 As SAPbobsCOM.Recordset
            Dim Data As String
            oRS3 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS3.DoQuery(SQL)
            If oRS3.RecordCount > 0 Then
                Data = oRS3.Fields.Item("Name").Value
                Return Data
            Else
                Return ""

            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Function Get One Value From DB Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return ""
        End Try

    End Function

#End Region

#Region "Calculate Team and Individual Performance Value"

    Public Sub CalTeamInd(ByVal FormUID As String, ByVal Row As Integer, ByVal EID As String, ByVal Team As Double, ByVal Ind As Double)
        Dim objRs1 As SAPbobsCOM.Recordset
        Dim SQL As String
        Dim Total, Previous As Double
        objRs1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        SQL = "select emplcost as Cost,U_vari as Variable from OHEM where empID='" & EID & "'"
        objRs1.DoQuery(SQL)
        Total = objRs1.Fields.Item("Cost").Value * objRs1.Fields.Item("Variable").Value / 100

        If Team <> 0 Then
            Previous = objMatrix.Columns.Item("10").Cells.Item(Row).Specific.value
            ' objMatrix.Columns.Item("10").Cells.Item(Row).Specific.value = Previous + Total * Team / 100
            TM = Total * Team / 100
            'objForm.Update()
            objForm.Refresh()
        End If

        If Ind <> 0 Then
            Previous = objMatrix.Columns.Item("10").Cells.Item(Row).Specific.value
            ' objMatrix.Columns.Item("10").Cells.Item(Row).Specific.value = Previous + Total * Ind / 100
            INDI = Total * Ind / 100
            'objForm.Update()
            objForm.Refresh()
        End If


    End Sub
#End Region

#Region "Calculate the TDS Amount"
    Sub TDSCalcuationAmount(ByVal EID As String, ByVal EMonth As Integer, ByVal EYear As Integer, ByVal SMonth As Integer, ByVal SYear As Integer)
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim MonCount As Integer
            Dim MonBasic, MonHRA, HRAExcepmt As Double
            Dim MetroEmp As String
            Dim oRs1 As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            'To find Already Paid PF Amount:
            strsql = "select isnull(SUM(convert(float,U_amount) ),0)[Amount] from [@AIS_OSPS] where U_empid ='" & EID & "' " & _
                " and U_fyear ='" & FinYear & "' and U_heads in (select Name from [@AIS_DDC] where U_pf ='Y') and U_heads not in('Pay','Remarks')"

            oRS.DoQuery(strsql)

            PF = oRS.Fields.Item("Amount").Value

            ' To Find Already Paid Professional Tax Amount:
            strsql = "select isnull(SUM(convert(float,U_amount) ),0)[Amount] from [@AIS_OSPS] where U_empid ='" & EID & "' " & _
                " and U_fyear ='" & FinYear & "' and U_heads in (select Name from [@AIS_DDC] where U_pt ='Y') and U_heads not in('Pay','Remarks')"

            oRS.DoQuery(strsql)

            PT = oRS.Fields.Item("Amount").Value



            'To Find Arrears Amount from Processed Salary:
            strsql = "select isnull(SUM(convert(float,U_amount) ),0)[Over Time] from [@AIS_OSPS] where U_empid ='" & EID & "' " & _
               " and U_fyear ='" & FinYear & "' and U_heads in('Arrears') and U_heads not in('Pay','Remarks')"
            oRS.DoQuery(strsql)

            OverTime = oRS.Fields.Item("Over Time").Value
            'To find the Overtime Amount:
            strsql = "select SUM(U_amount )[Amount] from [@AIS_OVERT] where U_fyear='" & FinYear & "' and U_code ='" & EID & "'"
            oRS.DoQuery(strsql)
            OverTime += oRS.Fields.Item("Amount").Value

            'Query for find the Metro and Non Metro Percentage:
            strsql = "select isnull(U_metro,0) [Metro],isnull(U_nmetro,0) [Non Metro] from [@AIS_STAT] "
            oRS.DoQuery(strsql)

            Metro = oRS.Fields.Item("Metro").Value
            NonMetro = oRS.Fields.Item("Non Metro").Value


            'To get Count payable Month:

            strsql = "select CASE when  convert(date,(select startDate  from OHEM where empID ='" & EID & "'),108)>convert(date,(select U_tdsfrom from [@AIS_stat]),108) " & _
                     " then datediff(m,convert(date,(select startDate  from OHEM where empID ='" & EID & "'),108) " & _
                     " ,convert(date,(select U_tdsto from [@AIS_stat]),108)) +1 else 12 end [MonthCount] ,(select isnull(U_arpaid,0) from [@AIS_EMPSSTP] where U_eid ='" & EID & "') [Rent Paid]"

            oRS.DoQuery(strsql)
            MonCount = oRS.Fields.Item("MonthCount").Value
            Count = oRS.Fields.Item("MonthCount").Value

            strsql = " select U_month [Con],U_year [Year],(select isnull(U_arpaid,0) from [@AIS_EMPSSTP] where U_eid ='" & EID & "') [Rent Paid], " & _
                    " (select isnull(U_metro,'N') from [@AIS_EMPSSTP] where U_eid ='" & EID & "') [Metro]  from [@AIS_OSPS] " & _
                    " where U_fyear ='" & FinYear & "' and U_empid ='" & EID & "' group by U_month ,U_year "
            oRs1.DoQuery(strsql)
            Count = Count - oRs1.RecordCount

            Minimum = 0
            Basic = 0
            HRA = 0
            Amount = 0
            HRAExcepmt = 0
            RentPaid = oRS.Fields.Item("Rent Paid").Value / MonCount
            MetroEmp = oRs1.Fields.Item("Metro").Value

            '  objForm.Items.Item("60").Specific.value = oRs1.RecordCount 'Paste the Processed Month Number in Processed Month Box


            '---------------------------------------------------- HRA Excemption Start ------------------------------------------------------------------

            For IntK As Integer = 1 To oRs1.RecordCount

                ' If RentPaid = 0 Then Exit For ' if Actual Rent didn't give then Exit For Loop:

                'Taking the values from DB processed salary Details:

                strsql = "DECLARE @sql varchar(MAX) declare @columnscsv varchar(MAX) DECLARE @sql1 varchar(MAX) " & _
                            " select @columnscsv = COALESCE(@columnscsv + '],[','') + U_descrpn from [@AIS_EPSSTP1] T0 left join [@AIS_EMPSSTP] T1 on T0.Code =T1.Code where U_tax ='Y' and T1.U_eid ='" & EID & "'  " & _
                            " select @sql1=COALESCE(@sql1 + ']+[','') +  U_descrpn from [@AIS_EPSSTP1] T0 left join [@AIS_EMPSSTP] T1 on T0.Code =T1.Code where U_tax ='Y' and T1.U_eid ='" & EID & "'  " & _
                            " set @columnscsv = '[' + @columnscsv + ']' set @sql1 = '[' + @sql1 + ']' " & _
                            " SET @sql = ' SELECT  ' + @columnscsv + ', '+ @sql1 +' as Total FROM " & _
                            " (select SUM(convert(float,x.Amount ) )[Amount] ,x.Heads from(select (U_amount)[Amount] ,U_heads [Heads] " & _
                            " from [@AIS_OSPS] where U_empid =''" & EID & "'' and " & _
                            " U_fyear=''" & FinYear & "'' and U_month=''" & oRs1.Fields.Item("Con").Value & "'' and U_year=''" & oRs1.Fields.Item("Year").Value & "'' and U_heads not in(''Pay'',''Remarks'') " & _
                            " and U_heads in(select U_descrpn from [@AIS_EPSSTP1] T0 left join [@AIS_EMPSSTP] T1 on T0.Code =T1.Code where U_tax =''Y'' and T1.U_eid =''" & EID & "''  " & _
                            " ) ) x group by x.Heads  ) a " & _
                            " PIVOT (SUM(amount) for Heads in (' + @columnscsv + ')) as PVT' EXEC (@sql) "
                oRS.DoQuery(strsql)

                Basic += oRS.Fields.Item("Basic").Value
                HRA += oRS.Fields.Item("HRA").Value
                ' DA = oRS.Fields.Item("DA").Value
                Amount += oRS.Fields.Item("Total").Value

                MonBasic = oRS.Fields.Item("Basic").Value
                MonHRA = oRS.Fields.Item("HRA").Value

                If RentPaid = 0 Then
                    Minimum = 0
                    ' objForm.Items.Item("49").Specific.value = Minimum
                Else
                    If MetroEmp = "Y" Then
                        FiftyPer = (MonBasic + DA) * Metro / 100
                        HRARcd = MonHRA
                        RntPDTenPer = RentPaid - ((MonBasic + DA) * 0.1)

                        'Find the Minimum of those three values:
                        Minimum = Math.Min(Math.Min(FiftyPer, HRARcd), RntPDTenPer)
                        If Minimum < 0 Then
                            ' objForm.Items.Item("49").Specific.value = 0
                            HRAExcepmt += 0
                        Else
                            HRAExcepmt += Minimum
                        End If
                    Else
                        FiftyPer = (MonBasic + DA) * NonMetro / 100
                        HRARcd = MonHRA
                        RntPDTenPer = RentPaid - ((MonBasic + DA) * 0.1)

                        'Find the Minimum of those three values:
                        Minimum = Math.Min(Math.Min(FiftyPer, HRARcd), RntPDTenPer)
                        ' HRARcd = HRA - Minimum
                        If Minimum < 0 Then
                            HRAExcepmt += 0
                        Else
                            HRAExcepmt += Minimum
                        End If
                    End If
                End If
                oRs1.MoveNext()
            Next

            '   objForm.Items.Item("62").Specific.value = Amount 'Paste the Processed Total Amount in Processed Salary Box:

            '------------------------------------------- Taking the values to be processed Month from the Employee Master Start -----------------------------------------------
            'Remaining Month Data's for the FInancial Year:


            strsql = "select isnull(SUM(U_amt),0) [Total],isnull(U_arpaid,0) [Act Rent Paid],isnull(T0.U_metro,'N') [Metro]  " & _
                  " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where U_descrpn ='Basic' and U_eid =" & EID & " ),0)[Basic] " & _
                  " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where U_descrpn ='HRA' and U_eid =" & EID & " ),0)[HRA] " & _
                  " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where U_descrpn ='DA' and U_eid =" & EID & " ),0)[DA] " & _
                  " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP2] T1 on T0.Code =T1.Code where U_descrpn in (select Name from [@AIS_DDC] where U_pf ='Y') and U_eid =" & EID & " ),0)[PF]" & _
                  " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP2] T1 on T0.Code =T1.Code where U_descrpn in (select Name from [@AIS_DDC] where U_pt ='Y') and U_eid =" & EID & " ),0)[PT]" & _
                  " from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where U_eid =" & EID & " and U_tax='Y' group by U_arpaid,U_metro  "
            oRS.DoQuery(strsql)

            For IntK As Integer = 1 To Count
                'Earnings:
                '  If RentPaid = 0 Then Exit For

                ' MsgBox(oRS.Fields.Item("Total").Value)
                Amount += oRS.Fields.Item("Total").Value
                Basic += oRS.Fields.Item("Basic").Value
                HRA += oRS.Fields.Item("HRA").Value
                DA += oRS.Fields.Item("DA").Value
                PF += oRS.Fields.Item("PF").Value

                MonBasic = oRS.Fields.Item("Basic").Value
                MonHRA = oRS.Fields.Item("HRA").Value

                If RentPaid = 0 Then
                    Minimum = 0
                    '  objForm.Items.Item("49").Specific.value = Minimum
                Else
                    If MetroEmp = "Y" Then
                        FiftyPer = (MonBasic + DA) * Metro / 100
                        HRARcd = MonHRA
                        RntPDTenPer = RentPaid - ((MonBasic + DA) * 0.1)

                        'Find the Minimum of those three values:
                        Minimum = Math.Min(Math.Min(FiftyPer, HRARcd), RntPDTenPer)
                        If Minimum < 0 Then
                            ' objForm.Items.Item("49").Specific.value = 0
                            HRAExcepmt += 0
                        Else
                            HRAExcepmt += Minimum
                        End If
                    Else
                        FiftyPer = (MonBasic + DA) * NonMetro / 100
                        HRARcd = MonHRA
                        RntPDTenPer = RentPaid - ((MonBasic + DA) * 0.1)

                        'Find the Minimum of those three values:
                        Minimum = Math.Min(Math.Min(FiftyPer, HRARcd), RntPDTenPer)
                        ' HRARcd = HRA - Minimum
                        If Minimum < 0 Then
                            HRAExcepmt += 0
                        Else
                            HRAExcepmt += Minimum
                        End If
                    End If
                End If

            Next

            'objForm.Items.Item("54").Specific.value = Count
            'objForm.Items.Item("47").Specific.value = oRS.Fields.Item("Total").Value * Count
            'objForm.Items.Item("49").Specific.value = HRAExcepmt  'HRA Excemption Value in Text Box

            '-------------------------------------- Taking the values to be processed Month from the Employee Master End -------------------------------------------------------------



            '------------------------------- HRA Excemption End -------------------------------------------------------------------------------------



            strSQL = "select salary  from OHEM where empID =" & EID & ""
            oRS.DoQuery(strSQL)

            Gross = oRS.Fields.Item("Salary").Value

            strsql = " select b.U_frmamt [From Amount],U_toamt [To Amount],U_ptamt [PT Amount] from [@AIS_DPTS] A " & _
             " left join  [@AIS_PTS1] B on A.Code =B.Code where U_state =(select workState [State] from " & _
             " OHEM T0 left join [@AIS_EMPSSTP] T1 on T0.empID =T1.U_eid where T1.U_eid =" & EID & ") "

            oRS.DoQuery(strSQL)
            For IntI = 1 To oRS.RecordCount
                FromAmount = oRS.Fields.Item("From Amount").Value
                ToAmount = oRS.Fields.Item("To Amount").Value
                PTAmount = oRS.Fields.Item("PT Amount").Value

                If Gross >= FromAmount And Gross <= ToAmount Then
                    PT += PTAmount * Count
                End If
                oRS.MoveNext()
            Next
            'Previous Employment Details:

            strsql = "select SUM(isnull(U_other,0) +isnull(U_sal,0)) [Amount],sum(ISNULL(U_pf,0 ))[PF] ,sum(ISNULL(U_pt ,0))[PT] " & _
                " from [@AIS_EPSSTP10] where Code =(select Code from [@AIS_EMPSSTP] where U_eid ='" & EID & "')"
            oRS.DoQuery(strsql)

            Amount += (oRS.Fields.Item("Amount").Value + OverTime)
            PF += oRS.Fields.Item("PF").Value
            PT += oRS.Fields.Item("PT").Value

            'To find the benefits taxable values:
            strsql = " select isnull(SUM(convert(float,U_amount) ),0)[Benefits] from [@AIS_OSPS] where U_empid =" & EID & " " & _
                " and U_fyear ='" & FinYear & "' and " & _
                " U_heads in(select T1.U_descrpn from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP5] T1 on T0.Code =T1.Code " & _
                " where T0.U_eid =" & EID & " and T1.U_tax ='Y' ) and U_heads not in('Pay','Remarks')  "

            oRS.DoQuery(strSQL)
            Amount += oRS.Fields.Item("Benefits").Value

            'To find Un Paid Benefits Amounts:
            strsql = "select SUM(U_amt ) [Benefits] from [@AIS_EPSSTP5] T0 join [@AIS_EMPSSTP] T1 on T0.Code =T1.Code " & _
                 " where T1.U_eid ='" & EID & "' and T0.U_bcode in(select Code from [@AIS_BCODE] where U_atv ='Y' ) and T0.U_tax ='Y'"

            oRS.DoQuery(strsql)
            Amount += oRS.Fields.Item("Benefits").Value


            GrossTaxSalary = Amount - (HRAExcepmt + TransAllow + MedReimb)
            '  GrossTaxSalary = Amount - (Minimum + 9600 + 15000)

            'strSQL = "select isnull(sum(T1.U_amt),0) Amount from [@AIS_EMPSSTP] T0 " & _
            '         " left join [@AIS_EPSSTP8]  T1 on T0.Code =T1.Code where T0.U_eid =" & EID & " and T1.U_pvisn='Excempt'"
            'oRS.DoQuery(strSQL)


            GrandTotalIncome = GrossTaxSalary
            ' OtherAllowances = 0
            strSQL = "select U_pvisn [Provision],isnull(sum(T1.U_amt),0) Amount,T2.U_qualamt [Limit] " & _
                     "  from [@AIS_EMPSSTP] T0   left join [@AIS_EPSSTP8]  T1 on T0.Code =T1.Code  " & _
                    "  left join [@AIS_PRVMSTR] T2 on T1.U_pvisn =T2.Code " & _
                    "  where T0.U_eid =" & EID & " and U_pvisn is not null group by U_pvisn ,T2.U_qualamt "

            oRS.DoQuery(strSQL)
            OtherExcemption = 0
            Count80C = 0
            If oRS.RecordCount > 0 Then
                For IntI = 0 To oRS.RecordCount - 1
                    If oRS.Fields.Item("Provision").Value = "80C" Then
                        Count80C = 1
                        TotInvestment = oRS.Fields.Item("Amount").Value + PF
                        If oRS.Fields.Item("Limit").Value > TotInvestment Then
                            TotDeduction = TotInvestment
                        Else
                            TotDeduction = oRS.Fields.Item("Limit").Value
                        End If
                    Else
                        If oRS.Fields.Item("Limit").Value > oRS.Fields.Item("Amount").Value Then
                            OtherExcemption += oRS.Fields.Item("Amount").Value
                        Else
                            OtherExcemption += oRS.Fields.Item("Limit").Value
                        End If
                    End If
                    oRS.MoveNext()
                Next
            Else
                TotDeduction = PF
            End If

            If Count80C = 0 Then
                TotDeduction = PF
            End If

            '  TotInvestment = oRS.Fields.Item("Amount").Value

            'strSQL = "select isnull(sum(T1.U_amtte),0) Amount from [@AIS_EMPSSTP] T0 " & _
            '           " left join [@AIS_EPSSTP9]  T1 on T0.Code =T1.Code where T0.U_eid =" & EID & ""
            'oRS.DoQuery(strSQL)

            'TotPerquisites = oRS.Fields.Item("Amount").Value


            ' objForm.Items.Item("56").Specific.value = OverTime

            TotalTDSAmount = GrandTotalIncome - (TotDeduction + OtherExcemption + PT)

            ' objForm.Items.Item("96").Specific.value = TotalTDSAmount
            ' objForm.Items.Item("117").Specific.value = TotalTDSAmount
            ' objForm.Items.Item("94").Specific.value = TotDeduction

            strsql = "select sum(U_inhsp +U_prbspf +U_cgain +U_othsurc +U_licmex +U_clremib +U_dbursmnt )-sum(U_othdect) [Amount] " & _
                " from [@AIS_EPSSTP11] where Code =(select Code from [@AIS_EMPSSTP] where U_eid ='" & EID & "')"
            oRS.DoQuery(strsql)

            TotalTDSAmount += oRS.Fields.Item("Amount").Value

            Dim TotIncome As Double = TotalTDSAmount

            TotalTDSAmount = TDSAmountBasedSlab(EID, Math.Round(TotalTDSAmount / 10, MidpointRounding.AwayFromZero) * 10)

            oRS.DoQuery("select U_educess +U_heducess [EduChess],isnull(U_rebate,0)[Rebate],ISNULL( U_totlim,0)[Limit] from [@AIS_STAT] ")
            If TotIncome <= oRS.Fields.Item("Limit").Value Then 'Total Income Limit for Rebate Calculation:
                'Education Chess Amount Calculation:
                If TotalTDSAmount <= oRS.Fields.Item("Rebate").Value Then
                    TotalTDSAmount = 0
                Else
                    TotalTDSAmount = TotalTDSAmount - oRS.Fields.Item("Rebate").Value
                End If
            End If
            'objForm.Items.Item("112").Specific.value = TotalTDSAmount * oRS.Fields.Item("EduChess").Value / 100
            EduChess = Math.Round(TotalTDSAmount * oRS.Fields.Item("EduChess").Value / 100, 0)

            'Surcharge Calaucation for TDS:
            oRS.DoQuery("select U_surcharge [Surcharge] ,U_surcglim [Limit] from [@AIS_STAT] ")
            If oRS.Fields.Item("Limit").Value > TotalTDSAmount * oRS.Fields.Item("Surcharge").Value / 100 Then
                ' objForm.Items.Item("110").Specific.value = TotalTDSAmount * oRS.Fields.Item("Surcharge").Value / 100
                Surcharge = Math.Round(TotalTDSAmount * oRS.Fields.Item("Surcharge").Value / 100, 0)
            Else
                Surcharge = oRS.Fields.Item("Limit").Value
            End If

            'Already Paid TDS Amount:

            strsql = "select isnull(SUM(convert(float,U_amount) ),0)[Amount] from [@AIS_OSPS] where U_empid ='" & EID & "' " & _
                " and U_fyear ='" & FinYear & "' and " & _
                " U_heads =(select Name  from [@AIS_DDC] where U_tds ='Y') and U_heads not in('Pay','Remarks')"

            oRS.DoQuery(strsql)

            PaidTDS = oRS.Fields.Item("Amount").Value

            'PaidTDS = oRS.Fields.Item("Amount").Value + objForm.Items.Item("69").Specific.value
            ' objForm.Items.Item("104").Specific.value = PaidTDS
            strsql = "select SUM(U_txdets) [Amount] from [@AIS_EPSSTP10] where Code =(select Code from [@AIS_EMPSSTP] where U_eid ='" & EID & "')"
            oRS.DoQuery(strsql)
            PaidTDS += oRS.Fields.Item("Amount").Value

            Dim TotTDS As Double = TotalTDSAmount

            TotalTDSAmount = (TotalTDSAmount + EduChess + Surcharge) - PaidTDS
            If PaidTDS >= TotTDS Then
                TotalTDSAmount = 0
            Else
                TotalTDSAmount = Replace(TotalTDSAmount, "-", "")
            End If

            UpdateTDSAmount(EID, TotalTDSAmount, Count)

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("TDSCalcuationAmount Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
#End Region

#Region "Update Current TDS Value to Particular Employee"

    Sub UpdateTDSAmount(ByVal EID As String, ByVal Amount As Double, ByVal Month As Integer)
        Try
            Dim MonthTDS As Double
            MonthTDS = Amount / Month

            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strsql = "update [@AIS_EPSSTP2] set U_amt ='" & MonthTDS & "' where U_descrpn =(select Name from [@AIS_DDC] where U_tds ='Y') " & _
                " and Code =(select Code from [@AIS_EMPSSTP] where U_eid ='" & EID & "')"
            oRS.DoQuery(strsql)

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("UpdateTDSAmount Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub

#End Region

#Region "Find TDS Amount Based on Government Slab"

    Public Function TDSAmountBasedSlab(ByVal EID As String, ByVal Amount As Double) As Double
        Try

            TDS = Amount
            TDSSlabAmt = 0
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select U_tdsslab [Slab] from OHEM where empID=" & EID & "")

            strsql = "select T1.U_llmt [Lower Limit] ,T1.U_ulmt [Upper Limit] ,T1.U_percnt [Percent] " & _
                " from [@AIS_OTDS] T0 left join [@AIS_TDS1] T1 on T0.Code =T1.Code  where T0.U_code ='TDS-" & oRS.Fields.Item("Slab").Value & "' "

            oRS.DoQuery(strsql)
            If oRS.RecordCount > 0 Then
                For IntI As Integer = 1 To oRS.RecordCount
                    LowerLimit = oRS.Fields.Item("Lower Limit").Value - 1
                    UpperLimit = oRS.Fields.Item("Upper Limit").Value
                    Percentage = oRS.Fields.Item("Percent").Value
                    ' If UpperLimit <= Amount And LowerLimit >= Amount Then
                    'If (Amount >= LowerLimit Or Amount <= UpperLimit) And UpperLimit <> 0.0 And (Amount > UpperLimit Or Amount > LowerLimit) Then
                    '    'TDS = TDS - LowerLimit
                    '    If UpperLimit < Amount Or UpperLimit <> 0.0 Then
                    '        If UpperLimit < Amount Then
                    '            TDS = UpperLimit - LowerLimit
                    '        Else
                    '            TDS = Amount - LowerLimit
                    '        End If
                    '    End If
                    '    TDSSlabAmt += TDS * Percentage / 100
                    'End If


                    If Amount >= LowerLimit Or Amount <= UpperLimit Then
                        If UpperLimit < Amount And UpperLimit <> 0.0 Then
                            If UpperLimit < Amount Then
                                TDS = UpperLimit - LowerLimit
                            Else
                                TDS = Amount - LowerLimit
                            End If
                            TDSSlabAmt += TDS * Percentage / 100
                        Else
                            TDS = Amount - LowerLimit
                            If TDS.ToString.Contains("-") Then
                            Else
                                TDSSlabAmt += TDS * Percentage / 100
                            End If

                        End If

                    End If
                    oRS.MoveNext()
                Next
                Return TDSSlabAmt
            Else
                Return 0
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("TDS Amount Calculation Based on Slab Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Function

#End Region

#Region "Get Financial Year for Salary Processing"

    Function FindFinancialYear(ByVal FDate As String, ByVal TDate As String) As String
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strsql = "select distinct Category from OFPR where F_RefDate >='" & FDate & "' and T_RefDate<='" & TDate & "'"
            oRS.DoQuery(strsql )
            Return oRS.Fields.Item("Category").Value
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Get Financial Year Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
        Return ""
    End Function
#End Region

End Class
