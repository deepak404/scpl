﻿Public Class ClsAttendance
    Public Const formtype = "Attendance"
    Dim objForm As SAPbouiCOM.Form
    Dim objrs As SAPbobsCOM.Recordset
    Dim objCombo As SAPbouiCOM.ComboBox
    Dim strName As String
    Dim RS As SAPbobsCOM.Recordset
    Dim objCombo1, objCombo2, objCombo3, objCombo4 As SAPbouiCOM.ComboBox
    Dim inti, intj As String
    Dim SName, EName, SDept, EDept, SBranch, EBranch As String

    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("SelectionAttendance.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        Dept()
        Branch()
        FormLoad()
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
        Month()
    End Sub

    Public Sub itemevent(ByVal formUID As String, ByRef pval As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pval.BeforeAction = False Then
            Select Case pval.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    objForm = objAddOn.objApplication.Forms.Item(formUID)
                    'objCombo = objForm.Items.Item("6").Specific
                    'If pval.ItemUID = "6" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                    '    If objCombo.Selected.Description = "DefineNew" Then
                    '        DefineNewTable()
                    '    End If
                    If pval.ItemUID = "1" Then
                        If Validate(formUID) Then
                            Details()
                            objAddOn.objShowGrid.LoadGrid(SName, EName, SDept, EDept, SBranch, EBranch, inti, intj, objCombo.Selected.Description)
                            objForm.Close()
                        End If
                    End If
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    If pval.ItemUID = "8" Or pval.ItemUID = "10" Then
                        ChooseFromList(formUID, pval)
                    End If
            End Select
        Else
            Select Case pval.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pval.ItemUID = "1" Then
                        If Validate(formUID) Then
                        Else
                            BubbleEvent = False
                        End If
                    End If
            End Select
        End If
    End Sub
#Region "CHOOSE FROM LIST"
    Public Sub ChooseFromList(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        Dim odt As SAPbouiCOM.DataTable
        objcfl = pval
        odt = objcfl.SelectedObjects
        If odt Is Nothing Then
        Else
            Try
                Select Case pval.ItemUID
                    Case "8"
                        objForm.Items.Item("8").Specific.value = odt.GetValue("firstName", 0)
                    Case "10"
                        objForm.Items.Item("10").Specific.value = odt.GetValue("firstName", 0)
                End Select
            Catch ex As Exception
            End Try
        End If
    End Sub
#End Region
#Region "DROP DOWN"
#Region "MONTH AND YEAR"
    Public Sub Month()
        objCombo = objForm.Items.Item("4").Specific
        objCombo.ValidValues.Add("January", "01")
        objCombo.ValidValues.Add("February", "02")
        objCombo.ValidValues.Add("March", "03")
        objCombo.ValidValues.Add("April", "04")
        objCombo.ValidValues.Add("May", "05")
        objCombo.ValidValues.Add("June", "06")
        objCombo.ValidValues.Add("July", "07")
        objCombo.ValidValues.Add("August", "08")
        objCombo.ValidValues.Add("September", "09")
        objCombo.ValidValues.Add("October", "10")
        objCombo.ValidValues.Add("November", "11")
        objCombo.ValidValues.Add("December", "12")
    End Sub
    'Public Sub DefineNewTable()
    '    Dim i As Integer
    '    Dim tablename As String
    '    For i = 0 To objAddOn.objApplication.Menus.Item("51200").SubMenus.Count - 1
    '        tablename = objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(i).String
    '        tablename = tablename.Remove(tablename.IndexOf("-"))
    '        If Trim(tablename) = "AIS_ATTENDANCENO" Then
    '            definenew = True
    '            objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(i).Activate()
    '            Noobj = objAddOn.objApplication.Forms.ActiveForm.TypeEx
    '            Exit For
    '        End If
    '    Next i
    'End Sub
    Public Sub FormLoad()
        Dim i As Integer
        objCombo = objForm.Items.Item("6").Specific
        While objCombo.ValidValues.Count > 0
            objCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
        End While
        strName = "select Code,Name from [@AIS_ATTENDANCENO]"
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery(strName)
        For i = 1 To RS.RecordCount
            Try
                objCombo.ValidValues.Add(RS.Fields.Item("Code").Value, RS.Fields.Item("Name").Value)
                RS.MoveNext()
            Catch ex As Exception

            End Try
        Next i
        RS.MoveLast()
        objCombo.ValidValues.Add("01", "2010")
        objCombo.ValidValues.Add("02", "2011")
        objCombo.ValidValues.Add("03", "2012")
        objCombo.ValidValues.Add("04", "2013")
        objCombo.ValidValues.Add("05", "2014")
        objCombo.ValidValues.Add("06", "2015")
        objCombo.ValidValues.Add("07", "2016")
        objCombo.ValidValues.Add("08", "2017")
        objCombo.ValidValues.Add("09", "2018")
        objCombo.ValidValues.Add("10", "2019")
        objCombo.ValidValues.Add("11", "2020")
    End Sub

#End Region
#Region "DEPT"
    Public Sub Dept()
        Dim strName As String
        Dim RS As SAPbobsCOM.Recordset
        Dim intLoop As Integer
        Dim objCombox As SAPbouiCOM.ComboBox
        objCombo = objForm.Items.Item("19").Specific
        objCombox = objForm.Items.Item("16").Specific

        ''adding values to combo box
        strName = "select distinct Name,Remarks from OUDP"
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery(strName)
        RS.MoveFirst()
        For intLoop = 1 To RS.RecordCount
            Try
                objCombo.ValidValues.Add(intLoop, RS.Fields.Item("Name").Value)
                objCombox.ValidValues.Add(intLoop, RS.Fields.Item("Name").Value)
                RS.MoveNext()
            Catch ex As Exception

            End Try

        Next intLoop
    End Sub
#End Region
#Region "BRANCH"
    Public Sub Branch()
        Dim objCombox As SAPbouiCOM.ComboBox
        Dim intLoop As Integer
        objCombo = objForm.Items.Item("20").Specific
        objCombox = objForm.Items.Item("18").Specific
        ''adding values to combo box
        strName = "select distinct location from OLCT"
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery(strName)
        RS.MoveFirst()
        For intLoop = 1 To RS.RecordCount
            Try
                objCombo.ValidValues.Add(intLoop, RS.Fields.Item("Location").Value)
                objCombox.ValidValues.Add(intLoop, RS.Fields.Item("Location").Value)
                RS.MoveNext()
            Catch ex As Exception

            End Try

        Next intLoop
    End Sub
#End Region
#End Region
#Region "VALIDATE"
    Public Function Validate(ByVal FormUID As String)
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        If objForm.Items.Item("4").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Select Month", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("6").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Select Year", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region



#Region "CHECK DETAILS"
    Public Sub Details()
        objCombo = objForm.Items.Item("4").Specific
        intj = objCombo.Selected.Description
        inti = objCombo.Selected.Value
        objCombo = objForm.Items.Item("6").Specific
        objAddOn.objShowGrid.LoadScreen(inti, intj, objCombo.Selected.Description)
        SName = objForm.Items.Item("8").Specific.string
        EName = objForm.Items.Item("10").Specific.string
        objCombo1 = objForm.Items.Item("19").Specific
        objCombo2 = objForm.Items.Item("20").Specific
        objCombo3 = objForm.Items.Item("16").Specific
        objCombo4 = objForm.Items.Item("18").Specific
        If objCombo1.Selected Is Nothing Then
            SDept = ""
        Else
            SDept = objCombo1.Selected.Value
        End If

        If objCombo3.Selected Is Nothing Then
            EDept = ""
        Else
            EDept = objCombo3.Selected.Value
        End If

        If objCombo2.Selected Is Nothing Then
            SBranch = ""
        Else
            SBranch = objCombo2.Selected.Description
        End If

        If objCombo4.Selected Is Nothing Then
            EBranch = ""
        Else
            EBranch = objCombo4.Selected.Description
        End If
    End Sub
#End Region
End Class
