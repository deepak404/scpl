﻿Public Class clsSalaryProcessUpdate
    Public Const FormType = "MNU_SALUP"
    Dim oForm As SAPbouiCOM.Form
    Dim oRS As SAPbobsCOM.Recordset
    Dim oComboMonth, oComboYear As SAPbouiCOM.ComboBox
    Dim Month, Year As Integer

    Public Sub LoadScreeen()
        oForm = objAddOn.objUIXml.LoadScreenXML("SalaryProcessUpdate.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        oComboMonth = oForm.Items.Item("4").Specific
        oComboYear = oForm.Items.Item("6").Specific
        LoadMonth()
        LoadYear()
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "1" Then
                            If Validate(FormUID) = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If
                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "1" Then
                            objAddOn.objSalaryUpdateGrid.LoadScreen(oComboMonth.Selected.Description, oComboMonth.Selected.Value, oComboYear.Selected.Value)

                        End If
                End Select
            End If

        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Item Event Main Try Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
#Region "Validate Event"
    Public Function Validate(ByVal FormUID As String) As Boolean
        Try
            oForm = objAddOn.objApplication.Forms.Item(FormUID)
            If oForm.Items.Item("4").Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select Month", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            ElseIf oForm.Items.Item("6").Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please select Year", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
            Return True
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Before Action - Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Function
#End Region

#Region "Load Month and Year Into Combo Box"

    Sub LoadMonth()
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select Code,Name from [@AIS_MONTH] order by Code")
            If oRS.RecordCount > 0 Then
                For Int As Integer = 0 To oRS.RecordCount
                    oComboMonth.ValidValues.Add(oRS.Fields.Item("Code").Value, oRS.Fields.Item("Name").Value)
                    oRS.MoveNext()
                Next
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Month Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub

    Sub LoadYear()
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select Code,Name from [@year] order by Code")
            If oRS.RecordCount > 0 Then
                For Int As Integer = 0 To oRS.RecordCount
                    oComboYear.ValidValues.Add(oRS.Fields.Item("Code").Value, oRS.Fields.Item("Name").Value)
                    oRS.MoveNext()
                Next
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Year Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub

#End Region


End Class
