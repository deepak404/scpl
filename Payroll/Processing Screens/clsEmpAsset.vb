﻿Public Class clsEmpAsset
    Public Const FormType = "AIS_OAST"
    Dim objForm1 As SAPbouiCOM.Form
    Dim oMatrix As SAPbouiCOM.Matrix
    Public Sub LoadScreen(ByVal EmpID As String, ByVal EmpName As String)
        objForm1 = objAddOn.objUIXml.LoadScreenXML("EmpAsset.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        ' objForm1 = objAddOn.objApplication.Forms.ActiveForm
        '  objForm1 = objAddOn.objApplication.Forms.ActiveForm
        oMatrix = objForm1.Items.Item("7").Specific
        objForm1.DataBrowser.BrowseBy = 8
        Dim objRS As SAPbobsCOM.Recordset
        Dim strSQL As String
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        strSQL = "select Code,U_empid from [@AIS_OAST] where  U_empid='" & EmpID & "'"
        objRS.DoQuery(strSQL)
        If objRS.RecordCount > 0 Then
            objForm1.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            objForm1.Items.Item("8").Specific.value = objRS.Fields.Item("Code").Value
            objForm1.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            'oMatrix = objForm1.Items.Item("7").Specific
            'oMatrix.AddRow()
            'oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.RowCount).Specific.value = oMatrix.RowCount
        Else
            objForm1.ActiveItem = 8
            objForm1.Items.Item("8").Specific.value = objForm1.BusinessObject.GetNextSerialNumber("-1", "AIS_OAST")
            objForm1.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            objForm1.Items.Item("8").Specific.value = EmpID
            objForm1.Items.Item("4").Specific.value = EmpID
            objForm1.Items.Item("6").Specific.value = EmpName
            oMatrix = objForm1.Items.Item("7").Specific
            objForm1.DataSources.DBDataSources.Item("@AIS_OAST").Clear()
            oMatrix.AddRow()
            oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.RowCount).Specific.value = oMatrix.RowCount
        End If
    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = False Then
                objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        If pVal.ItemUID = "7" Then
                            If oMatrix.Columns.Item("V_6").Cells.Item(oMatrix.VisualRowCount).Specific.value <> "" Then
                                objForm1.DataSources.DBDataSources.Item("@AIS_AST1").Clear()
                                oMatrix.AddRow()
                                oMatrix.Columns.Item("V_-1").Cells.Item(oMatrix.RowCount).Specific.value = oMatrix.RowCount
                            End If
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        Try
                            objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
                            If pVal.ItemUID = "1" And pVal.ActionSuccess = True And (objForm1.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or objForm1.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                                objForm1.Close()
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("Item Pressesed Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try

                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" And objForm1.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            If Validate(FormUID) = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                            ' DeleteAnEmptyRow(FormUID)
                        End If
                End Select
            End If
        Catch ex As Exception
            'objAddOn.objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

#Region "Delete An Empty Lines"
    Public Sub DeleteAnEmptyRow(ByVal FormUID As String)
        Dim i As Integer
        For i = oMatrix.VisualRowCount To 1 Step -1
            If oMatrix.Columns.Item("V_6").Cells.Item(i).Specific.value = "" Then
                oMatrix.DeleteRow(i)
                i = i - 1
            End If
        Next
    End Sub
#End Region
    
#Region "Validate Function"
    Public Function Validate(ByVal FormUID As String) As Boolean
        objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
        Try
            If oMatrix.Columns.Item("V_6").Cells.Item(1).Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Enter Atleast Row", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
            Return True
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Function
#End Region
   
End Class
