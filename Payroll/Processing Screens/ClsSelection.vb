Public Class ClsSelection
    Public objForm As SAPbouiCOM.Form
    Public Const Formtype = "SalSelection"
    Dim objCombobox, objCombobox1, objCombobox2, objCombobox3, objCombobox4 As SAPbouiCOM.ComboBox
    Dim objButton, ObjOption As SAPbouiCOM.OptionBtn
    Dim objButton1 As SAPbouiCOM.OptionBtn
    Dim objRS As SAPbobsCOM.Recordset
    Dim strSQL, StrDept, StrBranch, StrMonth, IntMonth As String
    Dim IntLoop, IntYear As Integer
    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("processsal.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, Formtype)
        ObjOption = objForm.Items.Item("14").Specific
        ObjOption.Selected = True
        OptionBt()
        Dept()
        Branch()
        Year()
        MonthDropdown()
    End Sub


#Region "item"
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "15" Then
                            If validate(FormUID) = False Then
                                BubbleEvent = False
                            End If
                        End If
                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "15" Then
                            objCombobox = objForm.Items.Item("4").Specific
                            objCombobox1 = objForm.Items.Item("6").Specific
                            objCombobox2 = objForm.Items.Item("8").Specific
                            objCombobox4 = objForm.Items.Item("12").Specific
                            IntYear = objCombobox4.Selected.Value
                            IntMonth = objCombobox.Selected.Description
                            StrMonth = objCombobox.Selected.Value
                            If objCombobox1.Selected Is Nothing Then
                                StrDept = ""
                            Else
                                StrDept = objCombobox1.Selected.Value
                            End If
                            If objCombobox2.Selected Is Nothing Then
                                StrBranch = ""
                            Else
                                StrBranch = objCombobox2.Selected.Value
                            End If
                            If objButton1.Selected Then
                                objAddOn.objSalaryprocess.LoadScreen(StrDept, StrBranch, IntYear, IntMonth, StrMonth)
                                'objForm.Close()
                            ElseIf objButton.Selected Then
                                objAddOn.objPrSal.LoadScreen(StrDept, StrBranch, IntYear, IntMonth, StrMonth)
                            End If
                        End If
                End Select
            End If
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Month"
    Public Sub MonthDropdown()
        objCombobox = objForm.Items.Item("4").Specific
        'objCombobox.ExpandType = SAPbouiCOM.BoExpandType.et_ValueOnly
        objCombobox.ValidValues.Add("January", "01")
        objCombobox.ValidValues.Add("February", "02")
        objCombobox.ValidValues.Add("March", "03")
        objCombobox.ValidValues.Add("April", "04")
        objCombobox.ValidValues.Add("May", "05")
        objCombobox.ValidValues.Add("June", "06")
        objCombobox.ValidValues.Add("July", "07")
        objCombobox.ValidValues.Add("August", "08")
        objCombobox.ValidValues.Add("September", "09")
        objCombobox.ValidValues.Add("October", "10")
        objCombobox.ValidValues.Add("November", "11")
        objCombobox.ValidValues.Add("December", "12")
    End Sub
#End Region

#Region "Department"
    Public Sub Dept()
        objCombobox1 = objForm.Items.Item("6").Specific
        strSQL = "select Code,Name from OUDP order by Code "
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        'objCombobox1.ExpandType = SAPbouiCOM.BoExpandType.et_ValueOnly
        For Me.IntLoop = 1 To objRS.RecordCount
            objCombobox1.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next IntLoop
    End Sub
#End Region

#Region "Branch"
    Public Sub Branch()
        objCombobox2 = objForm.Items.Item("8").Specific
        strSQL = "select Code,Name from OUBR order by Code "
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        'objCombobox2.ExpandType = SAPbouiCOM.BoExpandType.et_ValueOnly
        For Me.intLoop = 1 To objRS.RecordCount
            objCombobox2.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next intLoop
    End Sub
#End Region

#Region "Year"
    Public Sub Year()
        objCombobox4 = objForm.Items.Item("12").Specific
        'Dim I As Integer
        'objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        'objRS.DoQuery("select left( (select (Convert(date,GETDATE(),103))),4) Year")
        'I = objRS.Fields.Item("Year").Value
        ''objCombobox4.ExpandType = SAPbouiCOM.BoExpandType.et_ValueOnly
        'objCombobox4.ValidValues.Add(2010, "")
        'objCombobox4.ValidValues.Add(2011, "")
        'objCombobox4.ValidValues.Add(2012, "")
        'objCombobox4.ValidValues.Add(2013, "")
        'objCombobox4.ValidValues.Add(2014, "")
        'objCombobox4.ValidValues.Add(2015, "")
        'objCombobox4.ValidValues.Add(2016, "")
        'objCombobox4.ValidValues.Add(2017, "")
        'objCombobox4.ValidValues.Add(2018, "")
        'objCombobox4.ValidValues.Add(2019, "")
        'If I > 2019 Then
        '    objCombobox4.ValidValues.Add(I, "")
        'End If

        strSQL = "select Code,Name from [@YEAR] order by Code"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        'objCombobox2.ExpandType = SAPbouiCOM.BoExpandType.et_ValueOnly
        For Me.IntLoop = 1 To objRS.RecordCount
            objCombobox4.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next IntLoop
    End Sub
#End Region

#Region "Groupwith"
    Public Sub OptionBt()
        objButton = objForm.Items.Item("13").Specific
        objButton.GroupWith("14")
        objButton1 = objForm.Items.Item("14").Specific
        objButton1.GroupWith("13")
    End Sub
#End Region

#Region "Validation"
    Public Function validate(FormUID)
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        objCombobox = objForm.Items.Item("12").Specific
        objCombobox1 = objForm.Items.Item("4").Specific
        If objCombobox.Selected Is Nothing Then
            objAddOn.objApplication.SetStatusBarMessage("Month and Year is Mandatory ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False

        End If
        If objCombobox1.Selected Is Nothing Then
            objAddOn.objApplication.SetStatusBarMessage("Month and Year is Mandatory ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False

        End If
        Return True
    End Function

#End Region
   

End Class
