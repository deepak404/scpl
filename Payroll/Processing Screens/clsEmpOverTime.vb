﻿Public Class clsEmpOverTime
    Public Const formtype = "Time"
    Dim objForm As SAPbouiCOM.Form
    'Public objOb As SAPbouiCOM.OptionBtn
    'Public objOb1 As SAPbouiCOM.OptionBtn
    'Public objOb2 As SAPbouiCOM.OptionBtn
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim objRS As SAPbobsCOM.Recordset
    'Dim objButton As SAPbouiCOM.OptionBtn
    Dim str As String
    Dim strSQL As String
    Dim objCombo, oComboMonth, oComboYear As SAPbouiCOM.ComboBox
    Dim objDataTable As SAPbouiCOM.DataTable
    Dim objCFLEvent As SAPbouiCOM.ChooseFromListEvent
    Dim intLoop As Integer
    Dim U_ecode, U_ename As String
    Dim StrMonth As String
    Dim IntYear As String

    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("OverTime.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        ' objForm.AutoManaged = True
        objMatrix = objForm.Items.Item("8").Specific
        objMatrix.AddRow()
        objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.VisualRowCount).Specific.string = objMatrix.VisualRowCount
        'objButton = objForm.Items.Item("6").Specific
        objForm.DataBrowser.BrowseBy = 10
        WorkHR()
        SerialNo()
        objCombo = objForm.Items.Item("6").Specific
        MonthLoad()
        objCombo = objForm.Items.Item("3").Specific
        YearLoad()
        EmpType()
    End Sub

    Public Sub itemevent(ByVal formUID As String, ByRef pval As SAPbouiCOM.ItemEvent, ByRef bubbleevent As Boolean)
        If pval.BeforeAction = False Then
            Select Case pval.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    objCFLEvent = pval
                    objDataTable = objCFLEvent.SelectedObjects
                    objMatrix = objForm.Items.Item("8").Specific
                    If pval.ItemUID = "8" And pval.ColUID = "V_6" Then
                        EmployeeCFL(pval.Row)
                    End If
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    If pval.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And pval.ActionSuccess = True Then
                        AddRow()
                        ' SerialNo()
                    End If
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    If pval.ItemUID = "8" And pval.ColUID = "V_4" Then
                        OTTime(pval.Row)
                        EmpType()
                    ElseIf pval.ItemUID = "8" And (pval.ColUID = "V_8" Or pval.ColUID = "V_1") Then
                        objForm = objAddOn.objApplication.Forms.Item(formUID)
                        If objMatrix.Columns.Item("V_1").Cells.Item(pval.Row).Specific.string <> "" Then
                            AmountCalc(pval.Row, formUID)
                        End If

                    End If
                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    If pval.ItemUID = "6" Or pval.ItemUID = "3" Then
                        oComboMonth = objForm.Items.Item("6").Specific
                        oComboYear = objForm.Items.Item("3").Specific
                        If oComboMonth.Selected Is Nothing Or oComboYear.Selected Is Nothing Then
                        Else
                            objAddOn.objApplication.SetStatusBarMessage("Please Wait....", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                            objForm.Freeze(True)
                            LoadValues(oComboMonth.Selected.Description, oComboYear.Selected.Description)
                            objForm.Freeze(False)
                            objAddOn.objApplication.SetStatusBarMessage("Values Loaded", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                        End If
                    End If
            End Select
        Else
            Select Case pval.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pval.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        'RecordSet(pval.Row)
                    End If
                    If pval.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        If Validate(formUID) = False Then
                            bubbleevent = False
                        End If
                    ElseIf pval.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        If objMatrix.Columns.Item("V_6").Cells.Item(objMatrix.VisualRowCount).Specific.value = "" Then
                            objMatrix.DeleteRow(objMatrix.VisualRowCount)
                        End If
                    End If
                    'If pval.ItemUID = "5" Or pval.ItemUID = "6" Or pval.ItemUID = "7" Then
                    '    GroupWith()
                    'End If
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    If pval.ItemUID = "8" And pval.ColUID = "V_6" And (objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                        objMatrix = objForm.Items.Item("8").Specific
                        If objMatrix.Columns.Item("V_6").Cells.Item(objMatrix.VisualRowCount).Specific.value <> "" Then
                            objMatrix.AddRow()
                            objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.VisualRowCount).Specific.string = objMatrix.VisualRowCount
                            objMatrix.ClearRowData(objMatrix.VisualRowCount)
                        End If
                    End If
            End Select
        End If
    End Sub

#Region "Taking Values From Data Base"
    Public Sub LoadValues(ByVal Month As String, ByVal year As Integer)
        Try
            Dim objRS As SAPbobsCOM.Recordset
            Dim strSQL As String
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strSQL = "select T1.Code from [@AIS_OVERT] T0 join [@AIS_OVERTIME] T1 on T0.Code=T1.Code where T0.U_month='" & Month & "' and T0.U_year='" & year & "'"
            objRS.DoQuery(strSQL)
            If objRS.RecordCount > 0 Then
                objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                objForm.Items.Item("10").Specific.value = objRS.Fields.Item("Code").Value
                objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            End If
            'objForm.Items.Item("6").Enabled = False
            'objForm.Items.Item("3").Enabled = False
            ' objForm.Items.Item("10").Enabled = False
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.Message)
            objForm.Freeze(False)
        End Try
    End Sub
#End Region

    Public Sub EmployeeCFL(ByVal Row As Integer)
        If objDataTable Is Nothing Then
        Else
            objMatrix = objForm.Items.Item("8").Specific
            Try
                objMatrix.Columns.Item("V_5").Cells.Item(Row).Specific.value = objDataTable.GetValue("firstName", 0) + " " + objDataTable.GetValue("middleName", 0) + " " + objDataTable.GetValue("lastName", 0)
                objMatrix.Columns.Item("V_6").Cells.Item(Row).Specific.value = objDataTable.GetValue("empID", 0)
            Catch ex As Exception
            End Try

        End If
    End Sub

    'Public Sub GroupWith()
    '    objOb = objForm.Items.Item("5").Specific
    '    objOb.GroupWith("7")
    '    objOb1 = objForm.Items.Item("6").Specific
    '    objOb1.GroupWith("7")
    '    objOb2 = objForm.Items.Item("5").Specific
    '    objOb2.GroupWith("6")
    'End Sub

    'Public Sub GroupWith()
    '    objOb2 = objForm.Items.Item("5").Specific
    '    objOb2.GroupWith("6")
    '    objOb2.GroupWith("7")
    '    objOb1 = objForm.Items.Item("6").Specific
    '    objOb1.GroupWith("7")
    '    objOb1.GroupWith("5")
    '    objOb = objForm.Items.Item("7").Specific
    '    objOb.GroupWith("6")
    '    objOb.GroupWith("5")
    'End Sub

    'Public Sub GroupWith()
    '    objOb2 = objForm.Items.Item("5").Specific
    '    objOb2.GroupWith("6")
    '    objOb = objForm.Items.Item("7").Specific
    '    objOb.GroupWith("6")
    'End Sub

    Public Sub EmpType()
        Try
            Dim i As Integer
            objMatrix = objForm.Items.Item("8").Specific
            If objMatrix.RowCount > 0 Then
                objCombo = objMatrix.Columns.Item("V_2").Cells.Item(objMatrix.VisualRowCount).Specific
                For i = 1 To objCombo.ValidValues.Count
                    objCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
                Next i
                str = "select Name,descriptio from OHST"
                Dim objRecordset As SAPbobsCOM.Recordset
                objRecordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                objRecordset.DoQuery(str)
                For i = 1 To objRecordset.RecordCount
                    objCombo.ValidValues.Add(objRecordset.Fields.Item("Name").Value, objRecordset.Fields.Item("descriptio").Value)
                    objRecordset.MoveNext()
                Next i
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub WorkHR()
        Dim i As Integer
        objMatrix = objForm.Items.Item("8").Specific
        If objMatrix.RowCount > 0 Then
            objCombo = objMatrix.Columns.Item("V_4").Cells.Item(objMatrix.VisualRowCount).Specific
            For i = 1 To objCombo.ValidValues.Count
                objCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
            Next i
            str = "select Code,Name from [@AIS_DWHS]"
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS.DoQuery(str)
            For i = 1 To objRS.RecordCount
                objCombo.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
                objRS.MoveNext()
            Next i
        End If
    End Sub

    Public Sub SerialNo()
        objForm.ActiveItem = "10"
        objForm.Items.Item("10").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_OVERTIME")
        ' objForm.Items.Item("10").Enabled = False
    End Sub

#Region "Add Mode"
    Public Sub AddMode()
        objForm.Items.Item("6").Enabled = True
        objForm.Items.Item("3").Enabled = True
        objForm.Items.Item("10").Enabled = True
        SerialNo()
    End Sub
#End Region

    Public Sub AmountCalc(ByVal RowNo As Integer, ByVal FormUID As String)
        Try
            Dim a As Decimal
            Dim b As Decimal
            Dim c As Decimal
            'objForm = objAddOn.objApplication.Forms.Item(FormUID)
            a = objMatrix.Columns.Item("V_1").Cells.Item(RowNo).Specific.string
            ' MsgBox(a)
            b = objMatrix.Columns.Item("V_8").Cells.Item(RowNo).Specific.string
            c = a * b
            objMatrix.Columns.Item("V_9").Cells.Item(RowNo).Specific.string = c
        Catch ex As Exception

        End Try

    End Sub

    Public Sub AddRow()
        objMatrix = objForm.Items.Item("8").Specific
        If objMatrix.Columns.Item("V_6").Cells.Item(objMatrix.VisualRowCount).Specific.value <> "" Then
            objForm.DataSources.DBDataSources.Item("@AIS_OVERT").Clear()
            objMatrix.AddRow(1)
            objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.VisualRowCount).Specific.string = objMatrix.VisualRowCount
            '  objForm.ActiveItem = "8"
        End If

    End Sub

    Public Sub OTTime(ByVal row As Integer)
        Dim objRecordset As SAPbobsCOM.Recordset
        objRecordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objMatrix = objForm.Items.Item("8").Specific
        str = "select U_ot'OT',U_eid,U_ename from [@AIS_SUMATTEND1] where  U_eid= '" & objMatrix.Columns.Item("V_6").Cells.Item(row).Specific.string & "'"
        objRecordset.DoQuery(str)
        objMatrix.Columns.Item("V_3").Cells.Item(row).Specific.string = objRecordset.Fields.Item("OT").Value
    End Sub

    'Public Sub RecordSet(ByVal row As Integer)
    '    Dim objCombo1 As SAPbouiCOM.ComboBox
    '    Dim objRecordset As SAPbobsCOM.Recordset
    '    Dim j As Integer
    '    objCombo = objMatrix.Columns.Item("V_4").Cells.Item(objMatrix.VisualRowCount).Specific
    '    objCombo1 = objMatrix.Columns.Item("V_2").Cells.Item(objMatrix.VisualRowCount).Specific
    '    Try
    '        objOb = objForm.Items.Item("5").Specific
    '    objOb1 = objForm.Items.Item("6").Specific
    '    objOb2 = objForm.Items.Item("7").Specific
    '    objRecordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
    '    str = "select t2.code,t1.U_name,t1.U_workhr,t1.U_minhrs,t1.U_ratehr,convert(varchar,t1.U_effdate,103)'U_effdate',t1.U_emptpe,t2.U_lower 'lower' from [@AIS_OVERT] t1 join [@AIS_OVERTIME] t2 on t1.code=t2.code where t1.U_code = '" & objMatrix.Columns.Item("V_6").Cells.Item(objMatrix.VisualRowCount).Specific.string & "'"
    '    objRecordset.DoQuery(str)
    '    objMatrix.Columns.Item("V_5").Cells.Item(objMatrix.VisualRowCount).Specific.value = objRecordset.Fields.Item("U_name").Value
    '    objCombo.Select(objRecordset.Fields.Item("U_workhr").Value, SAPbouiCOM.BoSearchKey.psk_ByDescription)
    '    objCombo1.Select(objRecordset.Fields.Item("U_emptpe").Value, SAPbouiCOM.BoSearchKey.psk_ByDescription)
    '    objMatrix.Columns.Item("V_3").Cells.Item(objMatrix.VisualRowCount).Specific.value = objRecordset.Fields.Item("U_minhrs").Value
    '    objMatrix.Columns.Item("V_1").Cells.Item(objMatrix.VisualRowCount).Specific.value = objRecordset.Fields.Item("U_ratehr").Value
    '    objMatrix.Columns.Item("V_0").Cells.Item(objMatrix.VisualRowCount).Specific.string = objRecordset.Fields.Item("U_effdate").Value
    '    objForm.Items.Item("10").Specific.value = objRecordset.Fields.Item("Code").Value
    '    j = objRecordset.Fields.Item("lower").Value
    '        '(lower=2,exact=1,higher=3)
    '    If j = 1 Then
    '        objOb1.Selected = True
    '    ElseIf j = 2 Then
    '        objOb.Selected = True
    '    ElseIf j = 3 Then
    '        objOb2.Selected = True
    '        End If
    '    Catch ex As Exception
    '        'MsgBox(ex.ToString)
    '    End Try
    'End Sub

    Public Function Validate(ByVal FormUID As String)
        'objForm = objAddOn.objApplication.Forms.Item(FormUID)
        'objOb = objForm.Items.Item("5").Specific
        'objOb1 = objForm.Items.Item("6").Specific
        'objOb2 = objForm.Items.Item("7").Specific
        'If Not (objOb.Selected Or objOb1.Selected Or objOb2.Selected) Then
        '    objAddOn.objApplication.SetStatusBarMessage("Select any Time", SAPbouiCOM.BoMessageTime.bmt_Short, True)
        '    Return False
        'End If
        objMatrix = objForm.Items.Item("8").Specific
        If objMatrix.VisualRowCount > 0 Then
            For intLoop = 1 To objMatrix.VisualRowCount - 1
                If objMatrix.Columns.Item("V_6").Cells.Item(1).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Select Code", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("V_5").Cells.Item(1).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Enter Name", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("V_4").Cells.Item(1).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Select WorkHour", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("V_3").Cells.Item(1).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Enter OT minimum hrs", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                    'ElseIf objMatrix.Columns.Item("V_2").Cells.Item(1).Specific.value = "" Then
                    '    objAddOn.objApplication.SetStatusBarMessage("Select Employee Type", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    '    Return False
                End If
            Next intLoop
            If objMatrix.Columns.Item("V_6").Cells.Item(objMatrix.VisualRowCount).Specific.value = "" Then
                objMatrix.DeleteRow(objMatrix.VisualRowCount)
            End If
            If objMatrix.VisualRowCount = 0 Then
                objMatrix.AddRow()
                objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.VisualRowCount).Specific.String = objMatrix.VisualRowCount
                objAddOn.objApplication.SetStatusBarMessage("Please Enter Atleast One Row", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
        End If
        Return True
    End Function

#Region "Load Month and Year Values"
    Public Sub MonthLoad()
        objCombo.ValidValues.Add("January", "01")
        objCombo.ValidValues.Add("February", "02")
        objCombo.ValidValues.Add("March", "03")
        objCombo.ValidValues.Add("April", "04")
        objCombo.ValidValues.Add("May", "05")
        objCombo.ValidValues.Add("June", "06")
        objCombo.ValidValues.Add("July", "07")
        objCombo.ValidValues.Add("August", "08")
        objCombo.ValidValues.Add("September", "09")
        objCombo.ValidValues.Add("October", "10")
        objCombo.ValidValues.Add("November", "11")
        objCombo.ValidValues.Add("December", "12")
    End Sub

    Public Sub YearLoad()
        objCombo.ValidValues.Add("2013", "2013")
        objCombo.ValidValues.Add("2014", "2014")
        objCombo.ValidValues.Add("2015", "2015")
        objCombo.ValidValues.Add("2016", "2016")
        objCombo.ValidValues.Add("2017", "2017")
        objCombo.ValidValues.Add("2018", "2018")
        objCombo.ValidValues.Add("2019", "2019")
        objCombo.ValidValues.Add("2020", "2020")
        objCombo.ValidValues.Add("2021", "2021")
        objCombo.ValidValues.Add("2022", "2022")
        objCombo.ValidValues.Add("2023", "2023")
        objCombo.ValidValues.Add("2024", "2024")
        objCombo.ValidValues.Add("2025", "2025")
        objCombo.ValidValues.Add("2026", "2026")
    End Sub
#End Region
End Class
