﻿Public Class clsDailyOT
    Public Const formtype = "MNU_ODOT"
    Public objForm As SAPbouiCOM.Form
    Dim ShowFolderBrowserThread As Threading.Thread
    Dim FolderBrowser1 As OpenFileDialog
    Dim oEdit As SAPbouiCOM.EditText
    Dim objDialyAttendance As ClsDialyAttendance
    Dim objRS As SAPbobsCOM.Recordset
    Dim StrPath As String
    Dim Str(30) As String
    Dim IretCode As Double

#Region "LoadScreen"
    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("DailyOT.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
    End Sub
#End Region

#Region "ItemEvent"
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction Then
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "3" Then
                            BrowseFolderDialog()
                        ElseIf pVal.ItemUID = "4" Then
                            StrPath = objForm.Items.Item("2").Specific.string
                            ExcelFileReader(StrPath)
                            ' ExcelFileReader("D:\Latest Source Codes\Greata_Payroll\Document\OT Sheet for Payroll Processing.xls")
                            objForm.Close()
                        End If
                End Select
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "OpenFile"
    Private Sub BrowseFolderDialog()
        Try
            ShowFolderBrowserThread = New System.Threading.Thread(AddressOf ShowFolderBrowser)
            If ShowFolderBrowserThread.ThreadState = Threading.ThreadState.Unstarted Then
                ShowFolderBrowserThread.SetApartmentState(Threading.ApartmentState.STA)
                ShowFolderBrowserThread.Start()
            Else
                If ShowFolderBrowserThread.ThreadState = Threading.ThreadState.Stopped Then
                    ShowFolderBrowserThread.Start()
                    ShowFolderBrowserThread.Join()
                End If
            End If
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.Message)
        End Try
    End Sub
    Private Sub ShowFolderBrowser()
        ShowFolderBrowserThread = Nothing
        Dim FolderBrowser1 As New OpenFileDialog
        Dim shortfilename As String = ""
        Dim P() As Process
        Dim i As Integer
        Dim MyWindow As ClsWindowWrapper
        P = Process.GetProcessesByName("SAP Business One")
        If P.Length <> 0 Then
            For i = 0 To P.Length - 1
                MyWindow = New ClsWindowWrapper(P(i).MainWindowHandle)
                If Trim(objAddOn.objCompany.AttachMentPath) <> "" Then
                    FolderBrowser1.InitialDirectory = objAddOn.objCompany.AttachMentPath
                    FolderBrowser1.Filter = "xlsx (*.xlsx)|*.xlsx|All files (*.*)|*.*"
                    FolderBrowser1.FilterIndex = 2
                    FolderBrowser1.Multiselect = False
                    If FolderBrowser1.CheckPathExists() Then
                        If FolderBrowser1.ShowDialog(MyWindow) = DialogResult.OK Then
                            oEdit = objForm.Items.Item("2").Specific
                            oEdit.String = FolderBrowser1.FileName
                            StrPath = objForm.Items.Item("2").Specific.string
                        Else
                            System.Windows.Forms.Application.ExitThread()
                        End If
                    Else
                        objAddOn.objApplication.MessageBox("There is no attachment folder")
                    End If
                Else
                    objAddOn.objApplication.MessageBox("Specify the Attachment Folder Path in Administation Module")
                End If
            Next
        End If

    End Sub
#End Region

#Region "Excel Reader"
    Public Sub ExcelFileReader(ByVal StrPath As String)
        Dim ExcelApp As New Microsoft.Office.Interop.Excel.Application
        Dim ExcelWorkbook As Microsoft.Office.Interop.Excel.Workbook = Nothing
        Dim ExcelWorkSheet As Microsoft.Office.Interop.Excel.Worksheet = Nothing
        Dim excelRng As Microsoft.Office.Interop.Excel.Range

        Try
            ExcelWorkbook = ExcelApp.Workbooks.Open(StrPath)
            ExcelWorkSheet = ExcelWorkbook.ActiveSheet
            excelRng = ExcelWorkSheet.Range("A1")
            Dim RowIndex As Integer = 5
            '  Dim MonthDays As Integer
            While excelRng.Range("A" & RowIndex & "").Text <> "" And excelRng.Range("B" & RowIndex & "").Text <> ""
                RowIndex = RowIndex + 1
            End While
            Dim IntYear, OTMinHrs, RatePerHR, Code, ActualTime, Amount As Integer
            Dim StrMonth, StrEmpID, StrEmpName, WorkHour, EMPType, Frame, IntMonth As String
            StrMonth = excelRng.Range("B1").Text
            IntYear = excelRng.Range("B2").Text
            If StrMonth = "January" Then
                IntMonth = "01"
            ElseIf StrMonth = "February" Then
                IntMonth = "02"
            ElseIf StrMonth = "March" Then
                IntMonth = "03"
            ElseIf StrMonth = "April" Then
                IntMonth = "04"
            ElseIf StrMonth = "May" Then
                IntMonth = "05"
            ElseIf StrMonth = "June" Then
                IntMonth = "06"
            ElseIf StrMonth = "July" Then
                IntMonth = "07"
            ElseIf StrMonth = "August" Then
                IntMonth = "08"
            ElseIf StrMonth = "September" Then
                IntMonth = "09"
            ElseIf StrMonth = "October" Then
                IntMonth = "10"
            ElseIf StrMonth = "November" Then
                IntMonth = "11"
            ElseIf StrMonth = "December" Then
                IntMonth = "12"
            End If
            Dim oGeneralService As SAPbobsCOM.GeneralService
            Dim oGeneralData As SAPbobsCOM.GeneralData
            Dim oChildren As SAPbobsCOM.GeneralDataCollection
            Dim oChild As SAPbobsCOM.GeneralData
            oGeneralService = objAddOn.objCompany.GetCompanyService.GetGeneralService("AIS_OVERTIME")
            oGeneralData = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
            Dim CodeSQL, FinYear As String
            Dim RS As SAPbobsCOM.Recordset
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            CodeSQL = "select max(CAST(Code as numeric) ) [Count] from [@AIS_OVERTIME] "
            RS.DoQuery(CodeSQL)
            Code = RS.Fields.Item("Count").Value + 1

            FinYear = objAddOn.objSalaryprocess.FindFinancialYear(IntYear & IntMonth.ToString & "01", IntYear & IntMonth.ToString & get_last_date(IntYear, StrMonth))
            If FinYear = "" Then
                objAddOn.objApplication.StatusBar.SetText("Please Create Financial Period ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Exit Sub
            End If
            Dim i As Integer = 5
            For i = 5 To RowIndex - 1
                StrEmpID = excelRng.Range("A" & i & "").Text
                StrEmpName = excelRng.Range("B" & i & "").Text
                WorkHour = excelRng.Range("C" & i & "").Text
                OTMinHrs = excelRng.Range("D" & i & "").Text
                EMPType = excelRng.Range("E" & i & "").Text
                RatePerHR = excelRng.Range("F" & i & "").Text
                Frame = excelRng.Range("G" & i & "").Text
                ActualTime = excelRng.Range("H" & i & "").Text
                Amount = excelRng.Range("I" & i & "").Text
                Dim StrSQL As String = "select 1 as Result from [@AIS_OVERT] where U_code='" & StrEmpID & "' and U_month='" & IntMonth & "' and U_year='" & IntYear & "'"
                Dim Result As Integer
                objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                objRS.DoQuery(StrSQL)
                Result = objRS.Fields.Item("Result").Value
                If Result = 1 Then
                    objAddOn.objApplication.SetStatusBarMessage("Already Posted OT to this Employee for " & StrMonth & "", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Else
                    objAddOn.objApplication.StatusBar.SetText("Overtime Posting for - " & StrEmpName.ToString, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    oGeneralData.SetProperty("Code", Code.ToString)
                    oChildren = oGeneralData.Child("AIS_OVERT")
                    oChild = oChildren.Add()
                    oChild.SetProperty("U_code", StrEmpID.ToString)
                    oChild.SetProperty("U_name", StrEmpName.ToString)
                    oChild.SetProperty("U_workhr", WorkHour.ToString)
                    oChild.SetProperty("U_minhrs", OTMinHrs.ToString)
                    oChild.SetProperty("U_emptpe", EMPType.ToString)
                    oChild.SetProperty("U_ratehr", RatePerHR.ToString)
                    oChild.SetProperty("U_frame", Frame.ToString)
                    oChild.SetProperty("U_month", IntMonth.ToString)
                    oChild.SetProperty("U_year", IntYear.ToString)
                    oChild.SetProperty("U_atime", ActualTime.ToString)
                    oChild.SetProperty("U_amount", Amount.ToString)
                    oChild.SetProperty("U_fyear", FinYear.ToString)
                End If
            Next
            oGeneralService.Add(oGeneralData)
            ExcelWorkbook.Close()
            objAddOn.objApplication.SetStatusBarMessage("OT Posted for the Month of " & StrMonth & "", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Catch ex As Exception
            'objAddOn.objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short)
            MsgBox(ex.ToString)
        End Try
    End Sub
#End Region

#Region "Get Last Date Function"
    Public Function get_last_date(ByVal year As Integer, ByVal mntname As String)
        Dim lastdate As Integer = DatePart(DateInterval.Day, DateSerial(year, Month(CDate(mntname + "1,1990")) + 1, 0))
        Return lastdate
    End Function
#End Region

End Class
