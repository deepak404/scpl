﻿Public Class ClsIncomeLossHouseProperty
    Dim objForm As SAPbouiCOM.Form
    Public Const FormType = "AIS_OILH"
    Dim oRS As SAPbobsCOM.Recordset
    Dim SQL As String
    Dim oOption As SAPbouiCOM.OptionBtn
    Public Sub LoadScreen(ByVal EmpID As String, ByVal EmpName As String)
        Try
            objForm = objAddOn.objUIXml.LoadScreenXML("IncomeLossHouse.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            SQL = "select Code from [@AIS_OILH] where U_empid='" & EmpID & "'"
            oRS.DoQuery(SQL)
            If oRS.RecordCount > 0 Then
                objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                objForm.ActiveItem = 23
                objForm.Items.Item("23").Specific.value = oRS.Fields.Item("Code").Value
                objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            Else
                objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                objForm.Items.Item("23").Specific.value = objAddOn.objGenFunc.GetCode("[@AIS_OILH]")
                objForm.Items.Item("4").Specific.string = EmpID
                objForm.Items.Item("6").Specific.string = EmpName
            End If
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Loadscreen Fuction Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)

        End Try
    End Sub
    Public Sub Itemevent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            If Validation(FormUID) = False Then
                                BubbleEvent = False
                                Exit Sub
                            Else
                                ' IncomeLossCalculation()
                            End If
                        End If
                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "1" And (objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE) And pVal.ActionSuccess = True Then
                            objAddOn.objEmpSalarySetup.IncomeLossAmount(objForm.Items.Item("22").Specific.value)
                            objForm.Close()
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        If pVal.ItemUID = "10" Or pVal.ItemUID = "12" Or pVal.ItemUID = "14" Or pVal.ItemUID = "16" Or pVal.ItemUID = "18" Or pVal.ItemUID = "20" Or pVal.ItemUID = "24" Then
                            oOption = objForm.Items.Item("7").Specific
                            If oOption.Selected = True Then
                                IncomeLossCalculation()
                            Else
                                RentedIncomeLossCalculation()
                            End If
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.ItemUID = "7" Or pVal.ItemUID = "8" Then
                            oOption = objForm.Items.Item("7").Specific
                            If oOption.Selected = True Then
                                objForm.Items.Item("12").Specific.value = 0.0
                                objForm.Items.Item("16").Specific.value = 0.0
                                objForm.Items.Item("18").Specific.value = 0.0
                                objForm.Items.Item("20").Specific.value = 0.0
                                objForm.Items.Item("22").Specific.value = 0.0
                                objForm.Items.Item("24").Specific.value = 0.0

                                objForm.Items.Item("16").Enabled = False
                                objForm.Items.Item("18").Enabled = False
                                objForm.Items.Item("20").Enabled = False
                                objForm.ActiveItem = 12
                                objForm.Items.Item("24").Enabled = False

                            Else
                                objForm.Items.Item("12").Specific.value = 0.0
                                objForm.Items.Item("16").Specific.value = 0.0
                                objForm.Items.Item("18").Specific.value = 0.0
                                objForm.Items.Item("20").Specific.value = 0.0
                                objForm.Items.Item("22").Specific.value = 0.0
                                objForm.Items.Item("24").Specific.value = 0.0

                                objForm.Items.Item("16").Enabled = True
                                objForm.Items.Item("18").Enabled = True
                                objForm.Items.Item("20").Enabled = False
                                objForm.Items.Item("24").Enabled = True
                            End If
                        End If
                End Select
            End If
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Item Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub

#Region "Income /Loss Calculations"
    Sub IncomeLossCalculation()
        Try
            Dim Interest As Double
            ' PAmount = IIf(objForm.Items.Item("10").Specific.value = "", 0, objForm.Items.Item("10").Specific.value)
            Interest = IIf(objForm.Items.Item("12").Specific.value = "", 0, objForm.Items.Item("12").Specific.value)
            ' EMI = IIf(objForm.Items.Item("14").Specific.value = "", 0, objForm.Items.Item("14").Specific.value)
            'RentReceived = IIf(objForm.Items.Item("16").Specific.value = "", 0, objForm.Items.Item("16").Specific.value)
            'Prop = IIf(objForm.Items.Item("18").Specific.value = "", 0, objForm.Items.Item("18").Specific.value)
            'Main = IIf(objForm.Items.Item("20").Specific.value = "", 0, objForm.Items.Item("20").Specific.value) * IIf(objForm.Items.Item("24").Specific.value = "", 0, objForm.Items.Item("24").Specific.value)

            objForm.Items.Item("22").Specific.value = -CDbl(Interest)
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Income Loss Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
#End Region

#Region "Self Income /Loss Calculations"
    Sub RentedIncomeLossCalculation()
        Try
            Dim Interest, RentReceived, Prop, Main As Double
            ' PAmount = IIf(objForm.Items.Item("10").Specific.value = "", 0, objForm.Items.Item("10").Specific.value)
            Interest = IIf(objForm.Items.Item("12").Specific.value = "", 0, objForm.Items.Item("12").Specific.value)
            ' EMI = IIf(objForm.Items.Item("14").Specific.value = "", 0, objForm.Items.Item("14").Specific.value)
            RentReceived = IIf(objForm.Items.Item("16").Specific.value = "", 0, objForm.Items.Item("16").Specific.value)
            Prop = IIf(objForm.Items.Item("18").Specific.value = "", 0, objForm.Items.Item("18").Specific.value)

            Main = (IIf(objForm.Items.Item("16").Specific.value = "", 0, objForm.Items.Item("16").Specific.value) - IIf(objForm.Items.Item("18").Specific.value = "", 0, objForm.Items.Item("18").Specific.value)) * IIf(objForm.Items.Item("24").Specific.value = "", 0, objForm.Items.Item("24").Specific.value) / 100

            'MsgBox(IIf(objForm.Items.Item("16").Specific.value = "", 0, objForm.Items.Item("16").Specific.value))
            'MsgBox(IIf(objForm.Items.Item("18").Specific.value = "", 0, objForm.Items.Item("18").Specific.value))
            'MsgBox(IIf(objForm.Items.Item("24").Specific.value = "", 0, objForm.Items.Item("24").Specific.value))
            'MsgBox((IIf(objForm.Items.Item("16").Specific.value = "", 0, objForm.Items.Item("16").Specific.value) - IIf(objForm.Items.Item("18").Specific.value = "", 0, objForm.Items.Item("18").Specific.value)))


            objForm.Items.Item("20").Specific.value = Main

            objForm.Items.Item("22").Specific.value = CDbl(RentReceived) - CDbl(Interest + Main + Prop)
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Income Loss Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
#End Region


#Region "Validation "
    Function Validation(ByVal FormUID As String)
        Try
            If objForm.Items.Item("4").Specific.value = "" Then
                objAddOn.objApplication.StatusBar.SetText("Employee ID Should not Empty.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return False
            ElseIf objForm.Items.Item("6").Specific.value = "" Then
                objAddOn.objApplication.StatusBar.SetText("Employee Name Should not Empty.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return False
            ElseIf objForm.Items.Item("12").Specific.value > 150000 Then
                objAddOn.objApplication.StatusBar.SetText("Interest Limit Should not Exceed", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                Return False
            End If
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("validation Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        End Try
        Return True
    End Function
#End Region
End Class
