﻿Public Class clsSelectionCriteria
    Dim objForm As SAPbouiCOM.Form
    Public Const formtype = "MNU_SELC"
    Dim objCombo As SAPbouiCOM.ComboBox
    Dim inti As String
    Dim intj As String
    Dim SName, EName, SDept, EDept, SBranch, EBranch As String
    Dim objCombo1, objCombo3, objCombo4 As SAPbouiCOM.ComboBox
    Dim objCombo2 As SAPbouiCOM.ComboBox
    Dim strstring As String
    Private Property strSQL As String
    Private Property objRS As SAPbobsCOM.Recordset

    Public Sub loadscreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("SelectionCriteria.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
        objCombo = objform.Items.Item("4").Specific
        objCombo.ValidValues.Add("January", "01")
        objCombo.ValidValues.Add("February", "02")
        objCombo.ValidValues.Add("March", "03")
        objCombo.ValidValues.Add("April", "04")
        objCombo.ValidValues.Add("May", "05")
        objCombo.ValidValues.Add("June", "06")
        objCombo.ValidValues.Add("July", "07")
        objCombo.ValidValues.Add("August", "08")
        objCombo.ValidValues.Add("September", "09")
        objCombo.ValidValues.Add("October", "10")
        objCombo.ValidValues.Add("November", "11")
        objCombo.ValidValues.Add("December", "12")

        objCombo = objform.Items.Item("6").Specific
        objCombo.ValidValues.Add("2010", "2010")
        objCombo.ValidValues.Add("2011", "2011")
        objCombo.ValidValues.Add("2012", "2012")
        objCombo.ValidValues.Add("2013", "2013")
        objCombo.ValidValues.Add("2014", "2014")
        objCombo.ValidValues.Add("2015", "2015")
        objCombo.ValidValues.Add("2016", "2016")
        objCombo.ValidValues.Add("2017", "2017")
        objCombo.ValidValues.Add("2018", "2018")
        objCombo.ValidValues.Add("2019", "2019")
        objCombo.ValidValues.Add("2020", "2020")
        Combo1()
        Combo2()

        Combo()
        objAddOn.objApplication.Menus.Item("1281").Enabled = False
        objAddOn.objApplication.Menus.Item("1282").Enabled = False
        objForm.SupportedModes = 1
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = False Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    If pVal.ItemUID = "8" Or pVal.ItemUID = "10" Then
                        ChooseItem(FormUID, pVal)
                    End If
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    'objCombo = objform.Items.Item("4").Specific
                    If pVal.ItemUID = "4" Or pVal.ItemUID = "6" Then
                        objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                    ElseIf pVal.ItemUID = "1" Then
                        If Validate(FormUID) = True Then
                            objCombo = objForm.Items.Item("4").Specific
                            intj = objCombo.Selected.Description
                            inti = objCombo.Selected.Value
                            objCombo = objForm.Items.Item("6").Specific
                            objAddOn.objSummaryAttendance.loadscreen(inti, intj, objCombo.Selected.Value)

                            SName = objForm.Items.Item("8").Specific.string
                            EName = objForm.Items.Item("10").Specific.string
                            objCombo1 = objForm.Items.Item("19").Specific
                            objCombo2 = objForm.Items.Item("20").Specific
                            objCombo3 = objForm.Items.Item("16").Specific
                            objCombo4 = objForm.Items.Item("18").Specific
                            If objCombo1.Selected Is Nothing Then
                                SDept = ""
                            Else
                                SDept = objCombo1.Selected.Value
                            End If

                            If objCombo3.Selected Is Nothing Then
                                EDept = ""
                            Else
                                EDept = objCombo3.Selected.Value
                            End If

                            If objCombo2.Selected Is Nothing Then
                                SBranch = ""
                            Else
                                SBranch = objCombo2.Selected.Value
                            End If

                            If objCombo4.Selected Is Nothing Then
                                EBranch = ""
                            Else
                                EBranch = objCombo4.Selected.Value
                            End If

                            objAddOn.objSummaryAttendance.loadmatrix(SName, EName, SDept, EDept, SBranch, EBranch, inti, objCombo.Selected.Value, intj)
                            'If Validate(FormUID) = True Then
                            '    objForm.Close()
                            'End If

                        End If
                    End If

            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "1" Then
                        If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            If Validate(FormUID) = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If
                    End If
            End Select
        End If
    End Sub

#Region "Choose From List"
    Public Sub ChooseItem(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        Dim objdt As SAPbouiCOM.DataTable
        objcfl = pval
        objdt = objcfl.SelectedObjects
        If objdt Is Nothing Then
        Else
            Try
                Select Case pval.ItemUID
                    Case "8"
                        objForm.Items.Item("21").Specific.string = objdt.GetValue("firstName", 0) + " " + objdt.GetValue("lastName", 0)
                        objForm.Items.Item("8").Specific.string = objdt.GetValue("empID", 0)
                    Case "10"
                        objForm.Items.Item("22").Specific.string = objdt.GetValue("firstName", 0) + " " + objdt.GetValue("lastName", 0)
                        objForm.Items.Item("10").Specific.string = objdt.GetValue("empID", 0)
                End Select
            Catch ex As Exception

            End Try
          

        End If
    End Sub
#End Region

#Region "Validate Event"
    Public Function Validate(ByVal FormUID As String) As Boolean
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        If objForm.Items.Item("4").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select Month", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("6").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select Year", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        Else

        End If
        Return True
    End Function
#End Region

#Region "Combo"
    Public Sub Combo()
        Dim strName As String
        Dim RS As SAPbobsCOM.Recordset
        Dim intLoop As Integer
        Dim objCombox As SAPbouiCOM.ComboBox
        objCombo = objForm.Items.Item("20").Specific
        objCombox = objForm.Items.Item("18").Specific

        ''adding values to combo box
        strName = "select Code,Name from OUBR order by Code"
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery(strName)
        RS.MoveFirst()
        For intLoop = 1 To RS.RecordCount
            Try
                objCombo.ValidValues.Add(RS.Fields.Item("Code").Value, RS.Fields.Item("Name").Value)
                objCombox.ValidValues.Add(RS.Fields.Item("Code").Value, RS.Fields.Item("Name").Value)
                RS.MoveNext()
            Catch ex As Exception

            End Try

        Next intLoop
    End Sub
#End Region

#Region "Department"
    Public Sub Combo1()
        Dim intloop As Integer
        objCombo = objForm.Items.Item("19").Specific
        strSQL = "select Code,Name from OUDP order by Code "
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        'objCombo.ExpandType = SAPbouiCOM.BoExpandType.et_ValueOnly
        For intloop = 1 To objRS.RecordCount
            objCombo.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next
    End Sub
#End Region

#Region "Department1"
    Public Sub Combo2()
        Dim intloop As Integer
        objCombo = objForm.Items.Item("16").Specific
        strSQL = "select Code,Name from OUDP order by Code "
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        'objCombo.ExpandType = SAPbouiCOM.BoExpandType.et_ValueOnly
        For intloop = 1 To objRS.RecordCount
            objCombo.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next
    End Sub
#End Region

    Private Function intLoop() As Integer
        Throw New NotImplementedException
    End Function


End Class
