Module modEnum
    Public definenew As Boolean = False
    Public Flag As String
    Public PerquisiteCod As String
    Public FMSA As String
    Public PCYC As String
    Public DATTEND As String
    Public PerquisiteCat As String
    Public Provision As String
    Public colnumber As Integer
    Public lastdate, TM, INDI As Integer
    Public Basic, OAdd As Double
    Public CFormUID As String
    ' Public EBP, EHRA, ECON, EIVBP, EPP, DPF, DESI, DPT, DIT As Double
    Public Mon, Yr As String
    Public IMon As Integer
    Public FrmDate, Todate, TDSFromDate, TDSToDate, TDSFromYear, TDSToYear As String
    Public PayCode(25), DedCode(25), Benefits(25), ReimbCode(25), LoanCode(25), ArrearCode(25) As String
    Public PayHead(25), DedHeads(25), BenHeads(25), ReimbHeads(25), LoanHeads(25), ArrearHeads(25) As String
    Public WeekEnd, BreakTime, RoundOff, EmpCode, BasisFormula, Segment, TDSSetting, ZeroAmount, SAPwd As String
    Public HWKey() As String = New String() {"Z0464796071", "C0529684196", "L0287935425"}
End Module
