Imports System.IO
Imports System.Xml
Public Class clsAddOn
#Region "Declaration"
    Public WithEvents objApplication As SAPbouiCOM.Application
    Public objCompany As SAPbobsCOM.Company
    Dim oProgBarx As SAPbouiCOM.ProgressBar
    Public objGenFunc As Mukesh.SBOLib.GeneralFunctions
    Public objUIXml As Mukesh.SBOLib.UIXML
    Public ZB_row As Integer = 0
    Public objBenefitCode As clsBenefitCode
    Public objWorkhourstp As clsWorkhourstp
    Public objLoanAdvance As clsloanadvance
    Public DFFlag As Boolean = True

    Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
    Dim oUserTablesMD As SAPbobsCOM.UserTablesMD
    Dim ret As Long
    Dim str As String
    Dim objForm As SAPbouiCOM.Form
    Dim MenuCount As Integer = 0

    '' class
    Public objProvisionMaster As clsProvisionMaster
    Public objPerquisiteCategory As clsPerquisiteCategory
    Public objPerquisiteCode As clsPerquisiteCode
    Public objLeaveCode As ClsLeaveCode
    Public objStatutory As ClsStatutory
    Public objDailyAtten As ClsDailyAtten
    Public objEmptype As clsEmpType
    Public objGLAccount As clsGLAccount
    Public objEmpOverTime As clsEmpOverTime

    Public objDefineWorkHours As ClsDefineWorkHours
    Public objDefineReimbursement As ClsDefineReimbursement
    Public objDefineDeductionCode As ClsDefineDeductionCode
    Public objDefineProfessionalTaxSlab As ClsDefineProfessionalTaxSlab

    Public objDeducDet As clsDeducDet
    Public objTdsSlab As clsTdsSlab
    Public obj24Q As cls24Q

    Public objgeneralsetting As clsgeneralsetting
    Public objprocesscycle As clsprocesscycle
    Public objpaycode As clspaycode
    Public objSelectionCriteria As clsSelectionCriteria
    Public objSummaryAttendance As clsSummaryAttendance

    Public objFormulaSetup As clsFormulaSetup
    'Document Screen
    Public objEmpSalarySetup As clsEmpSalarySetup
    Public objEmpMaster As clsEmpMaster

    Public objselection As ClsSelection

    Public objSalaryprocess As ClsSalaryprocess

    Public objPrSal As ClsPrSal

    Public objLeaveRequisition As clsLeaveRequisition
    Public objLeaveApproval As clsLeaveApproval

    Public objChange As ClsChange
    Public objDaySelectionCriteria As clsDaySelectionCriteria
    Public objInOutTime As clsInOutTime
    Public objGrid As clsGrid
    Public objShowGrid As clsShowGrid
    Public objAttendance As ClsAttendance
    Public objouttime As clsOutTime
    Public objBenefitDetail As clsBenefitCodeDetail
    Public objLoanAdvaneSchedule As clsLoanAdvanceSchedule
    ' Public objEmpMasterData As clsEmpMasterData
    Public objVariablePay As ClsVariablePay
    Public objConsolidatedMonth As ClsConsolidatedMonth
    Public objConsolidatedQuarter As ClsConsolidatedQuarter
    Public objDialyAttendance As ClsDialyAttendance
    Public objDailyOT As clsDailyOT
    Public objEmpAsset As clsEmpAsset
    Public objCashPaymentEmp As clsCashPaymentEmp
    Public objReimbursementDetails As clsReimbursementDetails
    Public objResetScreen As clsResetScreen
    Public objFFSettlement As ClsFFSettlement
    Public objTDSSelectionCriteria As clsTDSSelectionCriteria
    Public objConsolidateTaxAmount As clsConsolidateTaxAmount
    Public objTDSCalculation As clsTDSCalculation
    Public objIncomeLossHouseProperty As ClsIncomeLossHouseProperty

    'Report
    Public objPaySlip As ClsPaySlip
    Public objRptFrm As FrmRpt
    Public objSalaryProcessReport As clsSalaryProcessReport
    Public objSalaryProcessed As clsSalaryProcessed
    Public objPaymentAdvice As clsPaymentAdvice

    Public objAssetRpt As clsAssetRpt
    Public objPFESIRpt As clsPFESIRpt
    Public objOverTimeRpt As clsOverTimeRpt
    Public objInvestmentRpt As clsInvestmentRpt
    Public objInOutTimeRpt As clsInOutTimeRpt
    Public objDailyAttendRpt As clsDailyAttendRpt
    Public objLveDetRpt As clsLveDetRpt
    Public objYearClaimRpt As clsYearClaimRpt
    Public objClaimDisburse As clsClaimDisburse
    Public objGratuityReport As clsGratuityReport
    Public objLeaveStatusReport As clsLeaveStatusReport

    Public objPFCalculationReport As clsPFCalculationReport
    Public objForm16Report As clsForm16Report
    Public obj24QReport As cls24QReport

    Public objSalaryProcessUpdate As clsSalaryProcessUpdate
    Public objSalaryUpdateGrid As clsSalaryUpdateGrid
    Public oReport As FrmRpt
#End Region

    Private Function CheckLicense() As Boolean
        Try
            Dim onewfom As SAPbouiCOM.Form
            Dim HardWareKey As String
            Dim oRS As SAPbobsCOM.Recordset
            oRS = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objAddOn.objApplication.Menus.Item("257").Activate()
            onewfom = objAddOn.objApplication.Forms.ActiveForm
            HardWareKey = onewfom.Items.Item("79").Specific.string
            onewfom.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

            ' For I As Integer = 0 To HWKey.Length - 1
            'If HWKey(I) = HardWareKey Then
            ''oRS.DoQuery("select datediff(DD ,'20140514',convert(date,getdate(),108))+1")
            'oRS.DoQuery("select datediff(DD ,'20141222',convert(date,getdate(),108))+1")
            ''If oRS.Fields.Item(0).Value <= 15 Then
            'If oRS.Fields.Item(0).Value <= 30 Then
            '    Return True
            'Else
            '    MessageBox.Show("Trail Period has been Expired.  Please Contact AIS")
            '    Return False
            'End If
            'Return True
            ' End If
            ' Next
            ' MessageBox.Show("You are not Authorized Run the Addon.  Please Contact AIS")
            Return True

        Catch ex As Exception
            objApplication.SetStatusBarMessage("Check License Function Failue - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Function

    Public Sub Intialize()
        Dim objSBOConnector As New Mukesh.SBOLib.SBOConnector
        objApplication = objSBOConnector.GetApplication(System.Environment.GetCommandLineArgs.GetValue(1))
        objCompany = objSBOConnector.GetCompany(objApplication)

        If CheckLicense() = False Then
            End
        Else
            Try
                createObjects()
                loadMenu()
                createTables()
                createUDOs()
                objAddOn.objprocesscycle.LoadValuestoDB()
                stat()
                objAddOn.objPaySlip.SAPassWord()
                objAddOn.objgeneralsetting.GeneralSettings()
                ''--------------Here --------------
            Catch ex As Exception
                objAddOn.objApplication.MessageBox(ex.ToString)
                End
            End Try
            objApplication.SetStatusBarMessage("Payroll Addon connected  successfully!", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End If
    End Sub
    Private Sub createUDOs()
        Dim ct1(1) As String
        ct1(0) = "AIS_WRKHRSTPL"
        createUDO1("AIS_BCODE", "AIS_BCODE", "Benefit Code", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)
        createUDO("AIS_WRKHRSTP", "AIS_WRKHRSTP", "AIS_WRKHRSTP", ct1, SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)
        createUDO1("AIS_LONADV", "AIS_LONADV", "Loan and Advance", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)

        createUDO1("AIS_STAT", "AIS_Stat", "AIS_Staturation", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)

        createUDO1("AIS_DLCODE", "AIS_Dlcode", "Leave Code", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)

        Dim ct2(2) As String
        ct2(0) = "AIS_PTS1"
        createUDO1("AIS_DWHS", "AIS_DWHS", "AIS_DWHS", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)
        createUDO1("AIS_DREIM", "AIS_DREIM", "Reimbursement", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)
        createUDO1("AIS_DDC", "AIS_DDC", "Deduction Code", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)
        createUDO("AIS_DPTS", "AIS_DPTS", "AIS_DPTS", ct2, SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)

        Dim ct3(1) As String
        ct3(0) = "AIS_TDS1"
        createUDO1("AIS_DEDDET", "AIS_DEDDET", "AIS_DEDDET", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)
        createUDO("AIS_OTDS", "AIS_OTDS", "AIS_OTDS", ct3, SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)

        createUDO1("AIS_PDSGS", "AIS_PDSGS", "AIS_PDSGS", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)
        createUDO1("AIS_PAYCODE", "AIS_PAYCODE", "Pay Code", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)
        Dim ct5(1) As String
        ct5(0) = "AIS_SUMATTEND1"
        createUDO("AIS_SUMATTEND", "AIS_SUMATTEND", "AIS_SUMATTEND", ct5, SAPbobsCOM.BoUDOObjType.boud_Document, False, True)

        Dim ct4(1) As String
        ct4(0) = "AIS_OVERT"
        createUDO("AIS_OVERTIME", "AIS_OVERTIME", "AIS_OVERTIME", ct4, SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)
        createUDO1("AIS_GLAC", "AIS_GLAC", "AIS_GLAC", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)

        createUDO1("AIS_FORMULA", "AIS_FORMULA", "AIS_FORMULA", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)

        'Document Screens
        Dim ct6(11), ct7(1), ct8(1) As String

        ct6(0) = "AIS_EPSSTP1"
        ct6(1) = "AIS_EPSSTP2"
        ct6(2) = "AIS_EPSSTP3"
        ct6(3) = "AIS_EPSSTP4"
        ct6(4) = "AIS_EPSSTP5"
        ct6(5) = "AIS_EPSSTP6"
        ct6(6) = "AIS_EPSSTP7"
        ct6(7) = "AIS_EPSSTP8"
        ct6(8) = "AIS_EPSSTP9"
        ct6(9) = "AIS_EPSSTP10"
        ct6(10) = "AIS_EPSSTP11"
        createUDO("AIS_EMPSSTP", "AIS_ESTP", "AIS_ESTP", ct6, SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)

        ct7(0) = "AIS_ALPSL"
        createUDO("AIS_ALPS", "AIS_ALPS", "AIS_ALPS", ct7, SAPbobsCOM.BoUDOObjType.boud_Document, False, True)
        ct8(0) = "AIS_PRSAL1"
        createUDO("AIS_PRSAL", "AIS_PRSAL", "AIS_PRSAL", ct8, SAPbobsCOM.BoUDOObjType.boud_Document, False, True)

        Dim ct9(1) As String
        ct9(0) = "AIS_LVREQ1"
        createUDO("AIS_LVREQ", "AIS_LVREQ", "AIS_LVREQ", ct9, SAPbobsCOM.BoUDOObjType.boud_Document, False, False)

        createUDO1("AIS_INOUTTIME", "AIS_INOUTTIME", "AIS_INOUTTIME", SAPbobsCOM.BoUDOObjType.boud_Document, False, True)

        Dim ct10(1), ct11(1) As String
        ct10(0) = "AIS_BNDT1"
        ct11(0) = "AIS_LNAD1"
        createUDO("AIS_OBNDT", "AIS_OBNDT", "AIS_OBNDT", ct10, SAPbobsCOM.BoUDOObjType.boud_Document, False, True)
        createUDO("AIS_OLNAD", "AIS_OLNAD", "AIS_OLNAD", ct11, SAPbobsCOM.BoUDOObjType.boud_Document, False, True)

        Dim ct12(2) As String
        ct12(0) = "AIS_VPAYM"
        ct12(1) = "AIS_VPAYQ"
        ct12(2) = "AIS_VPAYH"
        createUDO("AIS_VPAY", "AIS_VP", "AIS_VP", ct12, SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)

        Dim ct13(1), ct14(1), ct15(1), ct16(1) As String
        'Assets:
        ct13(0) = "AIS_AST1"
        createUDO("AIS_OAST", "AIS_OAST", "Employee Assets", ct13, SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)
        'Cash Payment
        ct14(0) = "AIS_CPT1"
        createUDO("AIS_OCPT", "AIS_OCPT", "Employee Cash Payment", ct14, SAPbobsCOM.BoUDOObjType.boud_Document, False, True)
        'Reimbursement Entry Details:
        ct15(0) = "AIS_RED1"
        createUDO("AIS_ORED", "AIS_ORED", "Reimbursement Details Entry", ct15, SAPbobsCOM.BoUDOObjType.boud_Document, False, True)
        'Employee Salary Processing for New:
        ct16(0) = "AIS_SPS1"
        createUDO("AIS_OSPS", "AIS_OSPS", "Employee Salary Process", ct16, SAPbobsCOM.BoUDOObjType.boud_Document, False, True)
        'Full and Final Settlement:
        createUDO1("AIS_OFFS", "OFFS", "FF Settlement", SAPbobsCOM.BoUDOObjType.boud_Document, False, True)
        'Income/Loss House Property:
        createUDO1("AIS_OILH", "AIS_OILH", "Income Loss", SAPbobsCOM.BoUDOObjType.boud_MasterData, False, True)


    End Sub

    Private Sub createObjects()
        'Library Object Initilisation
        objGenFunc = New Mukesh.SBOLib.GeneralFunctions(objCompany)
        objUIXml = New Mukesh.SBOLib.UIXML(objApplication)
        'Business Object Initialisation
        objBenefitCode = New clsBenefitCode
        objWorkhourstp = New clsWorkhourstp
        objLoanAdvance = New clsloanadvance

        objProvisionMaster = New clsProvisionMaster
        objPerquisiteCategory = New clsPerquisiteCategory
        objPerquisiteCode = New clsPerquisiteCode


        objStatutory = New ClsStatutory
        objDailyAtten = New ClsDailyAtten
        objLeaveCode = New ClsLeaveCode

        objEmptype = New clsEmpType
        objGLAccount = New clsGLAccount
        objEmpOverTime = New clsEmpOverTime

        objDefineWorkHours = New ClsDefineWorkHours
        objDefineReimbursement = New ClsDefineReimbursement
        objDefineDeductionCode = New ClsDefineDeductionCode
        objDefineProfessionalTaxSlab = New ClsDefineProfessionalTaxSlab

        objDeducDet = New clsDeducDet
        objTdsSlab = New clsTdsSlab
        obj24Q = New cls24Q

        objgeneralsetting = New clsgeneralsetting
        objprocesscycle = New clsprocesscycle
        objpaycode = New clspaycode
        objSelectionCriteria = New clsSelectionCriteria
        objSummaryAttendance = New clsSummaryAttendance

        objFormulaSetup = New clsFormulaSetup
        'Document Screen
        objEmpSalarySetup = New clsEmpSalarySetup
        objEmpMaster = New clsEmpMaster
        objSalaryprocess = New ClsSalaryprocess
        objselection = New ClsSelection
        objPrSal = New ClsPrSal
        objLeaveRequisition = New clsLeaveRequisition
        objLeaveApproval = New clsLeaveApproval

        objChange = New ClsChange
        objDaySelectionCriteria = New clsDaySelectionCriteria
        objInOutTime = New clsInOutTime
        objGrid = New clsGrid
        objShowGrid = New clsShowGrid
        objAttendance = New ClsAttendance
        objouttime = New clsOutTime
        objBenefitDetail = New clsBenefitCodeDetail
        objLoanAdvaneSchedule = New clsLoanAdvanceSchedule
        ' objEmpMasterData = New clsEmpMasterData

        objVariablePay = New ClsVariablePay
        objConsolidatedMonth = New ClsConsolidatedMonth
        objConsolidatedQuarter = New ClsConsolidatedQuarter
        objDialyAttendance = New ClsDialyAttendance
        objDailyOT = New clsDailyOT
        objEmpAsset = New clsEmpAsset
        objCashPaymentEmp = New clsCashPaymentEmp
        objReimbursementDetails = New clsReimbursementDetails
        objResetScreen = New clsResetScreen
        objFFSettlement = New ClsFFSettlement
        objTDSSelectionCriteria = New clsTDSSelectionCriteria
        objConsolidateTaxAmount = New clsConsolidateTaxAmount
        objTDSCalculation = New clsTDSCalculation
        objIncomeLossHouseProperty = New ClsIncomeLossHouseProperty
        'Report
        objPaySlip = New ClsPaySlip
        objRptFrm = New FrmRpt
        objSalaryProcessReport = New clsSalaryProcessReport
        objSalaryProcessed = New clsSalaryProcessed
        objPaymentAdvice = New clsPaymentAdvice

        objAssetRpt = New clsAssetRpt
        objPFESIRpt = New clsPFESIRpt
        objOverTimeRpt = New clsOverTimeRpt
        objInvestmentRpt = New clsInvestmentRpt
        objInOutTimeRpt = New clsInOutTimeRpt
        objDailyAttendRpt = New clsDailyAttendRpt
        objLveDetRpt = New clsLveDetRpt
        objYearClaimRpt = New clsYearClaimRpt
        objClaimDisburse = New clsClaimDisburse
        objGratuityReport = New clsGratuityReport
        objLeaveStatusReport = New clsLeaveStatusReport

        objPFCalculationReport = New clsPFCalculationReport
        objForm16Report = New clsForm16Report
        obj24QReport = New cls24QReport

        objSalaryProcessUpdate = New clsSalaryProcessUpdate
        objSalaryUpdateGrid = New clsSalaryUpdateGrid
        oReport = New FrmRpt

    End Sub

    Private Sub objApplication_ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) Handles objApplication.ItemEvent
        Try
            Select Case pVal.FormTypeEx

                Case clsBenefitCode.Formtype
                    objBenefitCode.Itemevent(FormUID, pVal, BubbleEvent)
                Case clsWorkhourstp.Formtype
                    objWorkhourstp.Itemevent(FormUID, pVal, BubbleEvent)
                Case Flag
                    If pVal.Before_Action = False Then
                        If pVal.EventType = SAPbouiCOM.BoEventTypes.et_FORM_CLOSE Then
                            objLoanAdvance.TypeFun(clsloanadvance.formtype)

                        End If
                    End If
                Case clsloanadvance.formtype
                    objLoanAdvance.itemevent(FormUID, pVal, BubbleEvent)

                Case "Time"
                    objEmpOverTime.itemevent(FormUID, pVal, BubbleEvent)

                Case clsGLAccount.type
                    objGLAccount.itemevent(FormUID, pVal, BubbleEvent)
                Case clsEmpOverTime.formtype
                    objEmpOverTime.itemevent(FormUID, pVal, BubbleEvent)

                Case "10000010"
                    objGLAccount.itemevent(FormUID, pVal, BubbleEvent)

                Case "Statutory"
                    objStatutory.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "LeaveCode"
                    objLeaveCode.ItemEvent(FormUID, pVal, BubbleEvent)
                Case DATTEND
                    objDailyAtten.ItemEvent(FormUID, pVal, BubbleEvent)

                Case ClsDefineWorkHours.formtype
                    objDefineWorkHours.ItemEvent(FormUID, pVal, BubbleEvent)
                Case ClsDefineReimbursement.formtype
                    objDefineReimbursement.ItemEvent(FormUID, pVal, BubbleEvent)
                Case ClsDefineDeductionCode.formtype
                    objDefineDeductionCode.ItemEvent(FormUID, pVal, BubbleEvent)
                Case ClsDefineProfessionalTaxSlab.formtype
                    objDefineProfessionalTaxSlab.ItemEvent(FormUID, pVal, BubbleEvent)

                Case ClsSelection.Formtype
                    objselection.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "MNU_DEDUCTOR"
                    objDeducDet.itemevent(FormUID, pVal, BubbleEvent)
                Case "MNU_TDS"
                    objTdsSlab.itemevent(FormUID, pVal, BubbleEvent)

                Case "MNU_GS"
                    objgeneralsetting.ItemEvent(FormUID, pVal, BubbleEvent)
                Case PCYC
                    objprocesscycle.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "MNU_PCDE"
                    objpaycode.itemevent(FormUID, pVal, BubbleEvent)
                Case "MNU_SELC"
                    objSelectionCriteria.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "MNU_SUMATT"
                    objSummaryAttendance.ItemEvent(FormUID, pVal, BubbleEvent)

                Case "MNU_FORMUL"
                    objFormulaSetup.itemevent(FormUID, pVal, BubbleEvent)

                Case clsEmpSalarySetup.Formtype
                    objEmpSalarySetup.Itemevent(FormUID, pVal, BubbleEvent)
                Case "60100"
                    objEmpMaster.itemevent(FormUID, pVal, BubbleEvent)
                Case ClsSalaryprocess.formtype
                    objSalaryprocess.ItemEvent(FormUID, pVal, BubbleEvent)
                Case ClsPrSal.formtype
                    objPrSal.ItemEvent(FormUID, pVal, BubbleEvent)


                Case clsLeaveRequisition.formtype
                    objLeaveRequisition.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsLeaveApproval.formtype
                    objLeaveApproval.ItemEvent(FormUID, pVal, BubbleEvent)

                Case Provision
                    objProvisionMaster.ItemEvent(FormUID, pVal, BubbleEvent)

                Case PerquisiteCat
                    objPerquisiteCategory.ItemEvent(FormUID, pVal, BubbleEvent)

                Case PerquisiteCod
                    objPerquisiteCode.ItemEvent(FormUID, pVal, BubbleEvent)

                Case "MNU_DWSC"
                    objDaySelectionCriteria.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "MNU_IOT"
                    objInOutTime.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "60100"
                    '    objEmpMasterData .ItemEvent(FormUID, pVal, BubbleEvent)
                Case "MNU_GRD"
                    objGrid.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "Attendance"
                    objDaySelectionCriteria.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "ShowAttendance"
                    objGrid.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "MNU_OT"
                    objInOutTime.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "MNU_BNDT"
                    objBenefitDetail.ItemEvent(FormUID, pVal, BubbleEvent)
                Case "MNU_LNAD"
                    objLoanAdvaneSchedule.ItemEvent(FormUID, pVal, BubbleEvent)

                Case ClsVariablePay.formtype
                    objVariablePay.ItemEvent(FormUID, pVal, BubbleEvent)
                Case ClsDialyAttendance.formtype
                    objDialyAttendance.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsDailyOT.formtype
                    objDailyOT.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsEmpAsset.FormType
                    objEmpAsset.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsCashPaymentEmp.FormType
                    objCashPaymentEmp.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsReimbursementDetails.FormType
                    objReimbursementDetails.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsResetScreen.FormType
                    objResetScreen.ItemEvent(FormUID, pVal, BubbleEvent)
                Case ClsFFSettlement.FormType
                    objFFSettlement.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsTDSSelectionCriteria.FormType
                    objTDSSelectionCriteria.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsTDSCalculation.FormType
                    objTDSCalculation.ItemEvent(FormUID, pVal, BubbleEvent)
                Case ClsIncomeLossHouseProperty.FormType
                    objIncomeLossHouseProperty.ItemEvent(FormUID, pVal, BubbleEvent)

                    'Report
                Case ClsPaySlip.formtype
                    objPaySlip.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsSalaryProcessed.FormType
                    objSalaryProcessed.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsPaymentAdvice.FormType
                    objPaymentAdvice.ItemEvent(FormUID, pVal, BubbleEvent)

                Case clsAssetRpt.FormType
                    objAssetRpt.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsPFESIRpt.FormType
                    objPFESIRpt.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsOverTimeRpt.FormType
                    objOverTimeRpt.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsInvestmentRpt.FormType
                    objInvestmentRpt.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsInOutTimeRpt.FormType
                    objInOutTimeRpt.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsDailyAttendRpt.FormType
                    objDailyAttendRpt.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsLveDetRpt.FormType
                    objLveDetRpt.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsYearClaimRpt.FormType
                    objYearClaimRpt.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsClaimDisburse.FormType
                    objClaimDisburse.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsGratuityReport.FormType
                    objGratuityReport.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsLeaveStatusReport.FormType
                    objLeaveStatusReport.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsPFCalculationReport.FormType
                    objPFCalculationReport.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsForm16Report.FormType
                    objForm16Report.ItemEvent(FormUID, pVal, BubbleEvent)

                Case clsSalaryProcessUpdate.FormType
                    objSalaryProcessUpdate.ItemEvent(FormUID, pVal, BubbleEvent)
                Case clsSalaryUpdateGrid.FormType
                    objSalaryUpdateGrid.ItemEvent(FormUID, pVal, BubbleEvent)
            End Select

        Catch ex As Exception
            ' MsgBox(ex.ToString)
            ' objAddOn.objApplication.StatusBar.SetText(ex.ToString, SAPbouiCOM.BoMessageTime.bmt_Short)
        End Try
    End Sub
    Private Sub objApplication_FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean) Handles objApplication.FormDataEvent
        Try
            If BusinessObjectInfo.FormTypeEx = "149" And (BusinessObjectInfo.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD) And BusinessObjectInfo.BeforeAction = False And BusinessObjectInfo.ActionSuccess = True Then
            ElseIf BusinessObjectInfo.FormTypeEx = "143" And (BusinessObjectInfo.EventType = SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD) And BusinessObjectInfo.BeforeAction = False And BusinessObjectInfo.ActionSuccess = True Then
            End If
        Catch ex As Exception
            'MsgBox(ex.ToString)
            objAddOn.objApplication.StatusBar.SetText(ex.ToString, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
        End Try
    End Sub
    Public Shared Function ErrorHandler(ByVal p_ex As Exception, ByVal objApplication As SAPbouiCOM.Application)
        Dim sMsg As String = Nothing
        If p_ex.Message = "Form - already exists [66000-11]" Then
            Return True
            Exit Function
        End If
        Return False
    End Function
    Private Sub objApplication_MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) Handles objApplication.MenuEvent
        If pVal.BeforeAction = True Then
            If pVal.MenuUID = "1283" Then
                Select Case objAddOn.objApplication.Forms.ActiveForm.TypeEx
                    Case ClsLeaveCode.formtype
                        objForm = objAddOn.objApplication.Forms.ActiveForm
                        ' objLeaveCode.RemoveFunction(pVal, BubbleEvent)
                        If objGenFunc.RemoveMenu("select DocEntry from [@AIS_inouttime] where U_attd ='" & objForm.Items.Item("6").Specific.String & "'") = True Then
                            objAddOn.objApplication.StatusBar.SetText("We Can't Remove this Code", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            BubbleEvent = False
                            Exit Sub
                        End If

                    Case clspaycode.formtype
                        objForm = objAddOn.objApplication.Forms.ActiveForm
                        ' objLeaveCode.RemoveFunction(pVal, BubbleEvent)
                        If objGenFunc.RemoveMenu("select top 1 DocEntry  from [@AIS_OSPS] where U_heads ='" & objForm.Items.Item("4").Specific.string & "'") = True Then
                            objAddOn.objApplication.StatusBar.SetText("We Can't Remove this Code", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            BubbleEvent = False
                            Exit Sub
                        End If

                    Case ClsDefineDeductionCode.formtype
                        objForm = objAddOn.objApplication.Forms.ActiveForm
                        If objGenFunc.RemoveMenu("select top 1 DocEntry  from [@AIS_OSPS] where U_heads ='" & objForm.Items.Item("4").Specific.string & "'") = True Then
                            objAddOn.objApplication.StatusBar.SetText("We Can't Remove this Code", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            BubbleEvent = False
                            Exit Sub
                        End If


                    Case ClsDefineReimbursement.formtype
                        objForm = objAddOn.objApplication.Forms.ActiveForm
                        If objGenFunc.RemoveMenu("select top 1 U_heads from [@AIS_OSPS] where U_heads ='" & objForm.Items.Item("6").Specific.string & "' union all select U_code  from [@AIS_RED1] where U_desc='" & objForm.Items.Item("6").Specific.string & "'") = True Then
                            objAddOn.objApplication.StatusBar.SetText("We Can't Remove this Code", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            BubbleEvent = False
                            Exit Sub
                        End If

                    Case clsBenefitCode.Formtype
                        objForm = objAddOn.objApplication.Forms.ActiveForm
                        If objGenFunc.RemoveMenu("select top 1 DocEntry  from [@AIS_OSPS] where U_heads ='" & objForm.Items.Item("6").Specific.string & "'") = True Then
                            objAddOn.objApplication.StatusBar.SetText("We Can't Remove this Code", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            BubbleEvent = False
                            Exit Sub
                        End If

                    Case clsloanadvance.formtype
                        objForm = objAddOn.objApplication.Forms.ActiveForm
                        If objGenFunc.RemoveMenu("select top 1 DocEntry  from [@AIS_OSPS] where U_heads ='" & objForm.Items.Item("6").Specific.string & "'") = True Then
                            objAddOn.objApplication.StatusBar.SetText("We Can't Remove this Code", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            BubbleEvent = False
                            Exit Sub
                        End If
                End Select
            End If
        Else
            Try
                Select Case pVal.MenuUID
                    Case "MNU_BCODE"
                        objBenefitCode.Loadscreen()
                    Case "MNU_WRKHURSTP"
                        objWorkhourstp.Loadscreen()
                    Case "MNU_LADVANCE"
                        objLoanAdvance.Loadscreen()
                    Case "MNU_LADVANCET"
                        objLoanAdvance.Define1()

                    Case "MNU_PROVSN"
                        objProvisionMaster.LoadScreen()
                    Case "MNU_PERCAT"
                        objPerquisiteCategory.LoadScreen()
                    Case "MNU_PERCOD"
                        objPerquisiteCode.LoadScreen()

                    Case "MNU_EMPTYPE"
                        objEmptype.DefineNew1()
                    Case "MNU_EMPPAY"
                        objEmptype.DefineNew()
                    Case "10000010"
                        objGLAccount.LoadScreen()
                    Case "MNU_OVERTIME"
                        objEmpOverTime.LoadScreen()
                    Case "MNU_GLDET"
                        objGLAccount.LoadScreen()

                    Case "Mnu_stat"
                        objStatutory.LoadScreen()
                    Case "MNU_Dailyatten"
                        objDailyAtten.Define()
                    Case "MNU_LeaveCode"
                        objLeaveCode.LoadScreen()

                    Case "MNU_WORKHOURS"
                        objDefineWorkHours.LoadScreen()
                    Case "MNU_REIMBURSEMENT"
                        objDefineReimbursement.LoadScreen()
                    Case "MNU_DEDUCTION"
                        objDefineDeductionCode.loadscreen()
                    Case "MNU_PROFFTAX"
                        objDefineProfessionalTaxSlab.LoadScreen()

                    Case "MNU_DEDUCTOR"
                        objDeducDet.LoadScreen()
                    Case "MNU_TDS"
                        objTdsSlab.LoadScreen1()
                    Case "MNU_24Q"
                        obj24Q.Define()

                    Case "mnu_gene"
                        objgeneralsetting.loadscreen()
                    Case "MNU_PC"
                        objprocesscycle.define()
                    Case "MNU_PCDE"
                        objpaycode.loadscreen()
                    Case "MNU_SELC"
                        objSelectionCriteria.loadscreen()

                    Case "MNU_PRSALSELN"
                        objselection.LoadScreen()

                    Case "MNU_FORMUL"
                        objFormulaSetup.loadscreen()

                    Case "MNU_EMPSTP"
                        objEmpSalarySetup.LoadScreen()

                    Case "MNU_LEAVREQ"
                        objLeaveRequisition.LoadScreen()
                    Case "MNU_LEAVAPP"
                        objAddOn.objLeaveApproval.Approval()

                    Case "MNU_DWSC"
                        objDaySelectionCriteria.loadscreen()
                    Case "MNU_SHWADNCE"
                        objDaySelectionCriteria.loadscreen()
                        'Case "MNU_BNDT"
                        '    objBenefitDetail.Loadscreen()
                        'Case "MNU_LNAD"
                        '    objLoanAdvaneSchedule.Loadscreen()
                        'Case "3590"
                        '    objEmpMasterData.LoadScreen()
                    Case "MNU_VARPAY"
                        objVariablePay.LoadScreen()
                    Case "MNU_CPDNORDR"
                        objDialyAttendance.LoadScreen()
                    Case "MNU_ODOT"
                        objDailyOT.LoadScreen()
                    Case "MNI_ORED"
                        objReimbursementDetails.LoadScreen()
                    Case "MNU_RSET"
                        objResetScreen.LoadScreen()
                    Case "MNU_OFFS"
                        objFFSettlement.LoadScreen()
                    Case "MNU_TDSSC"
                        objTDSSelectionCriteria.LoadScreen()

                    Case "MNU_TDSCALC"
                        objTDSCalculation.LoadScreen()

                        'Report
                    Case "MNU_PAYSLIP"
                        objPaySlip.LoadScreen()
                    Case "MNU_SALPCD"
                        objSalaryProcessed.LoadScreen()
                    Case "MNU_OCPT"
                        objCashPaymentEmp.LoadScreen()
                    Case "MNU_PADCE"
                        objPaymentAdvice.LoadScreen()
                    Case "MNU_ASTRPT"
                        objAssetRpt.LoadScreen()
                    Case "MNU_PFESIRPT"
                        objPFESIRpt.LoadScreen()
                    Case "MNU_OTRPT"
                        objOverTimeRpt.LoadScreen()
                    Case "MNU_INVRPT"
                        objInvestmentRpt.LoadScreen()
                    Case "MNU_IOTR"
                        objInOutTimeRpt.LoadScreen()
                    Case "MNU_DARPT"
                        objDailyAttendRpt.LoadScreen()
                    Case "MNU_LVDETPRT"
                        objLveDetRpt.LoadScreen()
                    Case "MNU_YRCLMRPT"
                        objYearClaimRpt.LoadScreen()
                    Case "MNU_CLMDSRPT"
                        objClaimDisburse.LoadScreen()
                    Case "MNU_GTYPRT"
                        objGratuityReport.LoadScreen()
                    Case "MNU_LVSTATRPT"
                        objLeaveStatusReport.LoadScreen()

                    Case "MNU_PFCALRPT"
                        objPFCalculationReport.LoadScreeen()
                    Case "MNU_FRM16RPT"
                        objForm16Report.LoadScreen()
                    Case "MNU_24QRPT"
                        obj24QReport.GenerateReport()
                    Case "MNU_SALUP"
                        objSalaryProcessUpdate.LoadScreeen()
                End Select
                If pVal.MenuUID = "1281" Then
                    Select Case objAddOn.objApplication.Forms.ActiveForm.TypeEx
                        Case "Statutory"
                            objStatutory.FindMode()
                        Case "LeaveCode"
                            objLeaveCode.FindMode()

                        Case "DefineDeductionCode"
                            objDefineDeductionCode.FindMode()
                        Case "DefineProfessionalTaxSlab"
                            objDefineProfessionalTaxSlab.FindMode()
                        Case "MNU_PCDE"
                            objpaycode.find_mode()
                        Case "MNU_TDS"
                            objTdsSlab.findmode()
                        Case "MNU_FORMUL"
                            objFormulaSetup.findmode()
                        Case clsEmpOverTime.formtype
                            objEmpOverTime.AddRow()
                        Case clsLeaveRequisition.formtype
                            objLeaveRequisition.FindMode()
                        Case "MNU_BNDT"
                            objBenefitDetail.FindMode()
                        Case "MNU_LNAD"
                            objLoanAdvaneSchedule.FindMode()
                        Case clsGLAccount.type
                            objGLAccount.find()
                        Case clsReimbursementDetails.FormType
                            objReimbursementDetails.FindModeFunction()
                        Case "MNU_OFFS"
                            objFFSettlement.FindMode()
                    End Select
                End If
                If pVal.MenuUID = "1282" Then
                    Select Case objAddOn.objApplication.Forms.ActiveForm.TypeEx
                        Case "Statutory"
                            objStatutory.AddMode()
                        Case "LeaveCode"
                            objLeaveCode.AddMode()
                        Case "MNU_PCDE"
                            objpaycode.add()
                        Case clsWorkhourstp.Formtype
                            objWorkhourstp.SerialNo()
                        Case clsLeaveRequisition.formtype
                            objLeaveRequisition.AddFUN()
                            objLeaveRequisition.RequestNO()
                        Case "MNU_TDS"
                            objTdsSlab.SerialNo()
                        Case "MNU_FORMUL"
                            objFormulaSetup.addmode()
                        Case "MNU_TDS"
                            objTdsSlab.addmode()
                        Case "MNU_DEDUCTOR"
                            objDeducDet.addmode()
                        Case "MNU_BNDT"
                            objBenefitDetail.AddMode()
                        Case "MNU_LNAD"
                            objLoanAdvaneSchedule.AddMode()
                        Case clsEmpOverTime.formtype
                            objEmpOverTime.AddMode()
                        Case clsGLAccount.type
                            objGLAccount.Add()
                        Case clsReimbursementDetails.FormType
                            objReimbursementDetails.AddModeFunction()
                        Case "MNU_OFFS"
                            objFFSettlement.AddMode()
                        Case ClsDefineDeductionCode.formtype
                            objDefineDeductionCode.AddMode()
                        Case ClsDefineProfessionalTaxSlab.formtype
                            objDefineProfessionalTaxSlab.AddModeFunction()
                    End Select

                End If

                If pVal.MenuUID = "1289" Or pVal.MenuUID = "1290" Or pVal.MenuUID = "1291" Or pVal.MenuUID = "1288" Then
                    Select Case objAddOn.objApplication.Forms.ActiveForm.TypeEx
                        Case "MNU_FORMUL"
                            If pVal.BeforeAction = False Then
                                objFormulaSetup.CheckCode()
                            End If
                        Case "MNI_ORED"
                            objReimbursementDetails.DisableItems()
                    End Select
                End If

            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
        End If
    End Sub
    Private Sub loadMenu()
        Try
            Dim JCMenus As New XmlDocument

            'If objApplication.Menus.Exists("MPL_1") Then
            '    objApplication.Menus.RemoveEx("MPL_1")
            'End If

            'If objApplication.Menus.Item("43520").SubMenus.Exists("Payroll") Then Return
            'MenuCount = objApplication.Menus.Item("43520").SubMenus.Count
            'CreateMenu("", MenuCount + 1, "Payroll", SAPbouiCOM.BoMenuType.mt_POPUP, "Mnu_payrol", objApplication.Menus.Item("43520"))

            JCMenus.Load(System.Windows.Forms.Application.StartupPath & "\addMenus.xml")
            objApplication.LoadBatchActions(JCMenus.InnerXml)

            'objApplication.Menus.Item("Mnu_payrol").Image = System.Windows.Forms.Application.StartupPath & "\p1.bmp"

            'Dim oMeniItem As SAPbouiCOM.MenuItem = objAddOn.objApplication.Menus.Item("Mnu_payrol")
            'oMeniItem.Image = System.Windows.Forms.Application.StartupPath & "\p1.bmp"

            'objApplication.Menus.Item("Mnu_payrol").Image = System.Windows.Forms.Application.StartupPath & "\images.jpg"

        Catch ex As Exception

        End Try
        'objUIXml.LoadMenuXML("addmenus.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded)
        'If objApplication.Menus.Item("43520").SubMenus.Exists("BENEFIT") Then Return
        'MenuCount = objApplication.Menus.Item("43520").SubMenus.Count


        'CreateMenu("", 1, "benefit code", SAPbouiCOM.BoMenuType.mt_STRING, "Mnu_bcode", objApplication.Menus.Item("MNU_AISBC"))
        'CreateMenu("", 2, "work hour setup", SAPbouiCOM.BoMenuType.mt_STRING, "Mnu_wrkhurstp", objApplication.Menus.Item("MNU_AISBC"))

    End Sub
    Private Function CreateMenu(ByVal ImagePath As String, ByVal Position As Int32, ByVal DisplayName As String, ByVal MenuType As SAPbouiCOM.BoMenuType, ByVal UniqueID As String, ByVal ParentMenu As SAPbouiCOM.MenuItem) As SAPbouiCOM.MenuItem
        Try
            Dim oMenuPackage As SAPbouiCOM.MenuCreationParams
            oMenuPackage = objApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
            oMenuPackage.Image = ImagePath
            oMenuPackage.Position = Position
            oMenuPackage.Type = MenuType
            oMenuPackage.UniqueID = UniqueID
            oMenuPackage.String = DisplayName
            ParentMenu.SubMenus.AddEx(oMenuPackage)
        Catch ex As Exception
            objApplication.StatusBar.SetText("Menu Already Exists", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_None)
        End Try
        Return ParentMenu.SubMenus.Item(UniqueID)
    End Function
    Private Sub createTables()

        Dim objUDFEngine As New Mukesh.SBOLib.UDFEngine(objCompany)
        objAddOn.objApplication.SetStatusBarMessage("Creating Tables Please Wait...", SAPbouiCOM.BoMessageTime.bmt_Long, False)

        '-----Benifit code-----------
        objUDFEngine.CreateTable("AIS_BCODE", "Benefitcode details", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddAlphaField("@AIS_BCODE", "pgla", "Payable GL account", 50)
        objUDFEngine.AddAlphaField("@AIS_BCODE", "egla", "Expendature GL account", 100)
        objUDFEngine.addField("@AIS_BCODE", "esia", "ESI applicable", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_BCODE", "esila", "ESI limit applicable", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_BCODE", "atv", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")

        '--workhoursetup-----
        objUDFEngine.CreateTable("AIS_WRKHRSTP", "Work hour setup", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddNumericField("@AIS_WRKHRSTP", "sid", "Selection Id", 10)
        objUDFEngine.AddDateField("@AIS_WRKHRSTP", "shdate", "Schedule date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_WRKHRSTP", "strdate", "Schedule start date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_WRKHRSTP", "edate", "End date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddAlphaField("@AIS_WRKHRSTP", "wrkhur", "Work hour", 20)
        objUDFEngine.addField("@AIS_WRKHRSTP", "dept", "Department", SAPbobsCOM.BoFieldTypes.db_Alpha, 25, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")


        '----workhourlines-------
        objUDFEngine.CreateTable("AIS_WRKHRSTPL", "Work hour setup lines", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_WRKHRSTPL", "eid", "Emp Id", 50)
        objUDFEngine.AddAlphaField("@AIS_WRKHRSTPL", "ename", "Emp Name", 100)
        objUDFEngine.AddDateField("@AIS_WRKHRSTPL", "stdate", "Schedule start date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_WRKHRSTPL", "edate", "Schedule end date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddAlphaField("@AIS_WRKHRSTPL", "wrhurcde", "Work hour code", 20)
        objUDFEngine.AddAlphaField("@AIS_WRKHRSTPL", "apporcde", "Applied overtime code", 50)

        '----Loanandadvance-----
        objUDFEngine.CreateTable("AIS_LONADV", "Loan and Advances ", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddAlphaField("@AIS_LONADV", "type", "Type", 20, )
        objUDFEngine.AddAlphaField("@AIS_LONADV", "pac", "Principal Account", 100)
        objUDFEngine.AddAlphaField("@AIS_LONADV", "iac", "Interest Account", 100)
        objUDFEngine.addField("@AIS_LONADV", "dfsal", "Detect From Salary", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_LONADV", "atv", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")

        '---Define Loan Advance type-----
        objUDFEngine.CreateTable("AIS_LONADVT", "Define Loan Advance type ", SAPbobsCOM.BoUTBTableType.bott_NoObject)

        '//Provision Master 

        objUDFEngine.CreateTable("AIS_PRVMSTR", "Provision Master", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        objUDFEngine.AddNumericField("@AIS_PRVMSTR", "qualamt", "Qualifying Amount", 10)
        objUDFEngine.addField("@AIS_PRVMSTR", "active", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")

        'Perquisite Category

        objUDFEngine.CreateTable("AIS_PRQSCTY", "Perquisite Category", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        objUDFEngine.AddNumericField("@AIS_PRQSCTY", "sno", "Serial Number", 10)

        'Perquisite Code

        objUDFEngine.CreateTable("AIS_PRQSCDE", "Perquisite Code", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        objUDFEngine.AddAlphaField("@AIS_PRQSCDE", "descrptn", "Description", 100)
        objUDFEngine.AddAlphaField("@AIS_PRQSCDE", "category", "Category", 100)
        objUDFEngine.addField("@AIS_PRQSCDE", "active", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")

        'statutory screen

        'statutory report period
        objUDFEngine.CreateTable("AIS_STAT", "Statutory Details", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddDateField("@AIS_STAT", "tdsfrom", "TDS From", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_STAT", "tdsto", "TDS To", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_STAT", "esifrom", "ESI From", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_STAT", "esito", "ESI To", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_STAT", "pffrom", "PF From", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_STAT", "pfto", "PF To", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_STAT", "lefrom", "Leave Enhance From", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_STAT", "leto", "Leave Enhance To", SAPbobsCOM.BoFldSubTypes.st_None)
        'pf details
        objUDFEngine.AddAlphaField("@AIS_STAT", "regoff", "Regional office", 50)
        objUDFEngine.AddAlphaField("@AIS_STAT", "subregoff", "SUB Regional office", 50)
        objUDFEngine.AddAlphaField("@AIS_STAT", "pfcode", "PF Code", 50)
        objUDFEngine.AddAlphaField("@AIS_STAT", "pfgcode", "PF Group Code", 50)
        objUDFEngine.AddFloatField("@AIS_STAT", "pfcemper", "PF contribution Employer", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_STAT", "pfshare", "PF Employeer Share", SAPbobsCOM.BoFldSubTypes.st_Percentage)

        objUDFEngine.AddFloatField("@AIS_STAT", "pfcempee", "PF contribution Employee", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_STAT", "vpfcempe", "VPF contribution Employee", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_STAT", "adcgs", "Admin Charges", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_STAT", "dliac", "DLI A/c 21", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_STAT", "adcgsre", "Admin Charges Remitted", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_STAT", "edliac", "EDLI A/c 22", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddNumericField("@AIS_STAT", "pflim", "PF Limit", 10)
        objUDFEngine.AddNumericField("@AIS_STAT", "pfagelim", "PF Age Limit", 10)
        'ESI Details 
        objUDFEngine.AddAlphaField("@AIS_STAT", "regesiy", "Regional ESI Year", 50)
        objUDFEngine.AddFloatField("@AIS_STAT", "esicemper", "ESI contribution Employer", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_STAT", "esicempee", "ESI contribution Employee", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddAlphaField("@AIS_STAT", "esicode", "ESI Code", 50)
        objUDFEngine.AddNumericField("@AIS_STAT", "esiwage", "ESI Wage Limit", 10)
        'TAX Details
        objUDFEngine.AddFloatField("@AIS_STAT", "educess", "Education Cess", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_STAT", "heducess", "Higher Education Cess", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddNumericField("@AIS_STAT", "siagelim", "Senior Citizen Age Limit", 10)
        objUDFEngine.AddNumericField("@AIS_STAT", "surcglim", "Surcharge Limit", 10)
        objUDFEngine.AddFloatField("@AIS_STAT", "surcharge", "Sur Charge", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_STAT", "metro", "Metro Percentage", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_STAT", "nmetro", "Non-Metro Percentage", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_STAT", "rebate", "Rebate", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_STAT", "totlim", "Total Limit", SAPbobsCOM.BoFldSubTypes.st_Sum)
        'Daily Attandece Status


        objUDFEngine.CreateTable("AIS_DDASS", "Define Daily Attendance Status", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        'objUDFEngine.addField("@AIS_DDASS", "default", "Default", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        'objUDFEngine.addField("@AIS_DDASS", "leave", "Leave", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        'objUDFEngine.addField("@AIS_DDASS", "compooff", "Compo Off", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        'objUDFEngine.addField("@AIS_DDASS", "firses", "First Session", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        'objUDFEngine.addField("@AIS_DDASS", "secses", "Second Session", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        'objUDFEngine.addField("@AIS_DDASS", "woutpay", "With Out Pay", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        'objUDFEngine.addField("@AIS_DDASS", "hwithpay", "Half Day With Pay", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        'objUDFEngine.addField("@AIS_DDASS", "hwoutpay", "Half Day With Out Pay", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")

        'Define Leave Code

        objUDFEngine.CreateTable("AIS_DLCODE", "Define Leave Code", SAPbobsCOM.BoUTBTableType.bott_MasterData)

        objUDFEngine.addField("@AIS_DLCODE", "active", "active", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")
        objUDFEngine.AddNumericField("@AIS_DLCODE", "maxypd", "Maximum earned leave", 10)
        objUDFEngine.AddNumericField("@AIS_DLCODE", "avapop", "Probation period after ", 10)
        objUDFEngine.addField("@AIS_DLCODE", "propcalc", "Probation calendar days", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DLCODE", "propphy", "Probation  Phyiscall days", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DLCODE", "cfprobper", " carry forwarded", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddNumericField("@AIS_DLCODE", "llad", "Leave lapse after days ", 10)
        objUDFEngine.AddAlphaField("@AIS_DLCODE", "remarks", "Remarks", 250)
        objUDFEngine.addField("@AIS_DLCODE", "calclaps", "Probation lap calendar days", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DLCODE", "phyclaps", "Probation lap physical days", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")
        objUDFEngine.addField("@AIS_DLCODE", "propcalc", "Probation calendar days", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DLCODE", "noclaps", "No of leaves", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddNumericField("@AIS_DLCODE", "minench", "Min encashable leave", 10)
        objUDFEngine.AddNumericField("@AIS_DLCODE", "maxencha", "Max encashable leave", 10)
        objUDFEngine.AddAlphaField("@AIS_DLCODE", "paygla", "Payable G/l", 30)
        objUDFEngine.AddAlphaField("@AIS_DLCODE", "expgla", "ExpenditG/L Account", 30)
        objUDFEngine.addField("@AIS_DLCODE", "fieacu", "Accrual", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DLCODE", "fieencash", "Encash", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DLCODE", "fieearn", "earned", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")



        'objUDFEngine.addField("AIS_DLCODE", "calclaps&quot..

        'PAYMENT MODE
        objUDFEngine.CreateTable("AIS_EMP", "Payment Mode", SAPbobsCOM.BoUTBTableType.bott_NoObject)

        'EMPLOYEE TYPE
        objUDFEngine.CreateTable("AIS_EMPT", "Employee Type", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        objUDFEngine.addField("@AIS_EMPT", "Categry", "Categry", SAPbobsCOM.BoFieldTypes.db_Alpha, 50, SAPbobsCOM.BoFldSubTypes.st_None, "T,P,TR,O", "Temporary,Permanent,Trainee,Outsourced", "P")

        'G/L ACCOUNT DETERMINATION
        objUDFEngine.CreateTable("AIS_GLAC", "AIS_GLAC", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "brnch", "brnch", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "cspac", "salary payable", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "cvpfc", "Valunter Providend Fund", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "ctdsc", "TDS Account", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "cpftc", "Professional Tax", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "codedc", "Other Deduction", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "cgac", "Gratuity Account", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "dsac", "Salary Account", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "dvpfc", "Volunter Providend Fund", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "doaddc", "Other Additon", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "dbonc", "Bonus Account", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "dovrc", "Overtime Account", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "dgac", "Gratuity Account", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "dlec", "Leave Enhancement", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "ffsc", "FF Settlement Code", 50)

        objUDFEngine.AddAlphaField("@AIS_GLAC", "cspan", "salary payable", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "cvpfn", "Valunter Providend Fund", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "ctdsn", "TDS Account", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "cpftn", "Professional Tax", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "codedn", "Other Deduction", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "cgan", "Gratuity Account", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "dsan", "Salary Account", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "dvpfn", "Volunter Providend Fund", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "doaddn", "Other Additon", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "dbonn", "Bonus Account", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "dovrn", "Overtime Account", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "dgan", "Gratuity Account", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "dlen", "Leave Enhancement", 50)
        objUDFEngine.AddAlphaField("@AIS_GLAC", "ffsn", "FF Settlement Name", 100)



        'OVER TIME
        objUDFEngine.CreateTable("AIS_OVERTIME", "AIS_OVERTIME", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.addField("@AIS_OVERTIME", "lower", "lower", SAPbobsCOM.BoFieldTypes.db_Alpha, 50, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "YES,NO", "Y")
        objUDFEngine.addField("@AIS_OVERTIME", "exact", "exact", SAPbobsCOM.BoFieldTypes.db_Alpha, 50, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "YES,NO", "N")
        objUDFEngine.addField("@AIS_OVERTIME", "higher", "higher", SAPbobsCOM.BoFieldTypes.db_Alpha, 50, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "YES,NO", "N")

        objUDFEngine.CreateTable("AIS_OVERT", "AIS_OVERT", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_OVERT", "code", "code", 50)
        objUDFEngine.AddAlphaField("@AIS_OVERT", "name", "name", 50)
        objUDFEngine.AddAlphaField("@AIS_OVERT", "workhr", "workhr", 50)
        objUDFEngine.AddFloatField("@AIS_OVERT", "minhrs", "minhrs", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OVERT", "atime", "Actual Time", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.addField("@AIS_OVERT", "frame", "Frame Setting", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Lower,Exact,High", "Lower,Exact,High", "")
        objUDFEngine.AddNumericField("@AIS_OVERT", "amount", "Amount", 10)
        objUDFEngine.AddAlphaField("@AIS_OVERT", "emptpe", "emptpe", 50)
        objUDFEngine.AddNumericField("@AIS_OVERT", "ratehr", "ratehr", 10)
        objUDFEngine.addField("@AIS_OVERT", "month", "Month", SAPbobsCOM.BoFieldTypes.db_Alpha, 15, SAPbobsCOM.BoFldSubTypes.st_None, "01,02,03,04,05,06,07,08,09,10,11,12", "January,February,March,April,May,June,July,August,September,October,November,December", "")
        objUDFEngine.addField("@AIS_OVERT", "year", "Year", SAPbobsCOM.BoFieldTypes.db_Alpha, 10, SAPbobsCOM.BoFldSubTypes.st_None, "2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025", "2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,2025", "")
        objUDFEngine.AddAlphaField("@AIS_OVERT", "fyear", "Financial Year", 50)


        'DEFINE WORK HOUR
        objUDFEngine.CreateTable("AIS_DWHS", "DefineWorkHours", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.addField("@AIS_DWHS", "stime", "shift start time", SAPbobsCOM.BoFieldTypes.db_Date, 10, SAPbobsCOM.BoFldSubTypes.st_Time, "", "", "")
        objUDFEngine.addField("@AIS_DWHS", "bstime", "start break time", SAPbobsCOM.BoFieldTypes.db_Date, 10, SAPbobsCOM.BoFldSubTypes.st_Time, "", "", "")
        objUDFEngine.addField("@AIS_DWHS", "betime", "end break time", SAPbobsCOM.BoFieldTypes.db_Date, 10, SAPbobsCOM.BoFldSubTypes.st_Time, "", "", "")
        objUDFEngine.addField("@AIS_DWHS", "etime", "shift end time", SAPbobsCOM.BoFieldTypes.db_Date, 10, SAPbobsCOM.BoFldSubTypes.st_Time, "", "", "")
        objUDFEngine.addField("@AIS_DWHS", "flexhrs", "flexi hours", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DWHS", "overlap", "over lap", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DWHS", "ot", "ot", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")

        'DEFINE REIMBURSEMENT
        objUDFEngine.CreateTable("AIS_DREIM", "DefineReimbursement", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddAlphaField("@AIS_DREIM", "egla", "expendature GL account", 100)
        objUDFEngine.AddAlphaField("@AIS_DREIM", "pgla", "payable GL account", 100)
        objUDFEngine.addField("@AIS_DREIM", "type", "type", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.AddNumericField("@AIS_DREIM", "rate", "rate", 10)
        objUDFEngine.AddNumericField("@AIS_DREIM", "limit", "limit", 10)
        objUDFEngine.addField("@AIS_DREIM", "active", "active", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")
        objUDFEngine.addField("@AIS_DREIM", "insal", "Include Salary", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")


        'DEFINE DEDUCTIONCODE
        objUDFEngine.CreateTable("AIS_DDC", "DefineDeductionCode", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddAlphaField("@AIS_DDC", "glac", "GL account code", 100)
        objUDFEngine.addField("@AIS_DDC", "tds", "TDS", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DDC", "pf", "PF", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DDC", "esi", "ESI", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DDC", "ecess", "ECess", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DDC", "hedces", "HEdCess", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DDC", "srcge", "Surcharge", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DDC", "pt", "PT", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DDC", "active", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")
        objUDFEngine.addField("@AIS_DDC", "fixed", "Fixed", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DDC", "vpf", "VPF", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_DDC", "varpay", "Variyable Pay", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")

        'DEFINE PROFESSIONAL TAX SLAB
        objUDFEngine.CreateTable("AIS_DPTS", "DefineProfessionalTaxSlab", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddAlphaField("@AIS_DPTS", "state", "State", 100)
        objUDFEngine.AddNumericField("@AIS_DPTS", "maxamt", "MaxAmt", 10)

        objUDFEngine.CreateTable("AIS_PTS1", "ProfessionalTaxSlab", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddNumericField("@AIS_PTS1", "frmamt", "FromAmt", 10)
        objUDFEngine.AddNumericField("@AIS_PTS1", "toamt", "ToAmt", 10)
        objUDFEngine.AddNumericField("@AIS_PTS1", "ptamt", "PTAmt", 10)



        'DEFINE DEDUCTOR DETAILS
        objUDFEngine.CreateTable("AIS_DEDDET", "DeductorDetails", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.addField("@AIS_DEDDET", "Brnch", "Branch/Division", SAPbobsCOM.BoFieldTypes.db_Alpha, 50, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "hdoffc", "headoffice", 20)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "govt", "dedtygovt", 10)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "others", "dedtyothr", 10)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "addr1", "Addressline1", 250)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "addr2", "Addressline2", 250)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "addr3", "Addressline3", 250)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "addr4", "Addressline4", 250)
        objUDFEngine.addField("@AIS_DEDDET", "state", "state", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.AddNumericField("@AIS_DEDDET", "pncod", "pincode", 10)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "email", "email", 50)
        objUDFEngine.AddNumericField("@AIS_DEDDET", "phone", "phone", 10)
        objUDFEngine.AddNumericField("@AIS_DEDDET", "phn", "phn", 10)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "adrcgy", "addresschangeyes", 10)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "adrcgn", "addresschangeyno", 10)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "dednam", "deductorname", 20)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "fthnam", "fathername", 20)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "desgn", "designation", 20)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "adr1", "Addresline1", 250)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "adr2", "Addresline2", 250)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "adr3", "Addresline3", 250)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "adr4", "Addresline4", 250)
        objUDFEngine.addField("@AIS_DEDDET", "stte", "state1", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.AddNumericField("@AIS_DEDDET", "pincod", "pincode1", 10)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "emal", "email1", 50)
        objUDFEngine.AddNumericField("@AIS_DEDDET", "phne", "phone1", 10)
        objUDFEngine.AddNumericField("@AIS_DEDDET", "phn1", "phn1", 10)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "adrchy", "addresschngeyes", 10)
        objUDFEngine.AddAlphaField("@AIS_DEDDET", "adrchn", "addresschngeyno", 10)

        'tds calculation slab(master data)
        objUDFEngine.CreateTable("AIS_OTDS", "TdsCalculationslab", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddAlphaField("@AIS_OTDS", "code", "TDSCode", 100)

        'master lines data'
        objUDFEngine.CreateTable("AIS_TDS1", "TdsCalslab1", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddNumericField("@AIS_TDS1", "llmt", "lowerlimit", 10)
        objUDFEngine.AddNumericField("@AIS_TDS1", "ulmt", "upperlimit", 10)
        objUDFEngine.addField("@AIS_TDS1", "cond", "conditions", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.AddFloatField("@AIS_TDS1", "percnt", "percentage", SAPbobsCOM.BoFldSubTypes.st_Percentage)

        '24Q
        objUDFEngine.CreateTable("AIS_24QSTCD", "24Q State Code", SAPbobsCOM.BoUTBTableType.bott_NoObject)

        'Payroll Details Setup:
        objUDFEngine.CreateTable("AIS_PDSGS", "Payroll Details Setup ", SAPbobsCOM.BoUTBTableType.bott_MasterData)

        objUDFEngine.addField("@AIS_PDSGS", "actoary", "ACTOARYes", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")
        objUDFEngine.addField("@AIS_PDSGS", "actoarn", "ACTOARNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")

        objUDFEngine.addField("@AIS_PDSGS", "iweldy", "IWELDYes", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")
        objUDFEngine.addField("@AIS_PDSGS", "iweldn", "IWELDNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")

        objUDFEngine.addField("@AIS_PDSGS", "ibtwhy", "IBTWHYes", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "Y")
        objUDFEngine.addField("@AIS_PDSGS", "ibtwhn", "IBTWHNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")

        objUDFEngine.addField("@AIS_PDSGS", "rosy", "RoundOffSalaryYes", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")
        objUDFEngine.addField("@AIS_PDSGS", "rosn", "RoundOffSalaryNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "Y")

        objUDFEngine.addField("@AIS_PDSGS", "ecud", "EMPCodeUserDefined", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "Y")
        objUDFEngine.addField("@AIS_PDSGS", "ecsg", "EMPCodeSystemGenerated", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")

        objUDFEngine.addField("@AIS_PDSGS", "asary", "ASARYes", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")
        objUDFEngine.addField("@AIS_PDSGS", "asarn", "ASARNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")

        objUDFEngine.addField("@AIS_PDSGS", "scbfy", "SCBFYes", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "Y")
        objUDFEngine.addField("@AIS_PDSGS", "scbfn", "SCBFNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")

        objUDFEngine.addField("@AIS_PDSGS", "usay", "UseSegAcctsYes", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "Y")
        objUDFEngine.addField("@AIS_PDSGS", "usan", "UseSegAcctsNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")

        objUDFEngine.addField("@AIS_PDSGS", "atdscy", "AutoTDSCalcYes", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "Y")
        objUDFEngine.addField("@AIS_PDSGS", "atdscn", "AutoTDSCalcNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")

        objUDFEngine.addField("@AIS_PDSGS", "fbtdy", "FBTDedFrmCTCYes", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")
        objUDFEngine.addField("@AIS_PDSGS", "fbtdn", "FBTDedFrmCTCNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")

        objUDFEngine.addField("@AIS_PDSGS", "zaspy", "ZeroAtSalPrsingYes", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "Y")
        objUDFEngine.addField("@AIS_PDSGS", "zaspn", "ZeroAtSalPrsingNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")

        objUDFEngine.addField("@AIS_PDSGS", "lapy", "LAPYes", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")
        objUDFEngine.addField("@AIS_PDSGS", "lapn", "LAPNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")

        objUDFEngine.addField("@AIS_PDSGS", "mlhy", "MLHYes", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")
        objUDFEngine.addField("@AIS_PDSGS", "mlhn", "MLHNo", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "yes,No", "N")

        objUDFEngine.AddAlphaField("@AIS_PDSGS", "path", "File Path", 253)


        'Process Cycle
        objUDFEngine.CreateTable("AIS_PDSPC", "Process Cycle", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        objUDFEngine.AddDateField("@AIS_PDSPC", "frmdt", "From Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_PDSPC", "todt", "To Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddNumericField("@AIS_PDSPC", "proday", "Process Days", 10)


        'Define Pay Code
        objUDFEngine.CreateTable("AIS_PAYCODE", "DefinePayCode", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddAlphaField("@AIS_PAYCODE", "glac", "GL account code", 100)
        objUDFEngine.addField("@AIS_PAYCODE", "pf", "PF", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_PAYCODE", "ga", "GA", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_PAYCODE", "esi", "ESI", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_PAYCODE", "esilim", "ESILimit", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_PAYCODE", "sec10", "Section10", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_PAYCODE", "pt", "PT", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_PAYCODE", "active", "Active", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "Y")
        objUDFEngine.addField("@AIS_PAYCODE", "fixed", "Fixed", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_PAYCODE", "vpf", "VPF", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")

        'Summary Attendance Details
        objUDFEngine.CreateTable("AIS_SUMATTEND", "Summary Attendance", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddDateField("@AIS_SUMATTEND", "sdate", "startdate", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_SUMATTEND", "edate", "enddate", SAPbobsCOM.BoFldSubTypes.st_None)

        'Summary Attendance Details Line 1:
        objUDFEngine.CreateTable("AIS_SUMATTEND1", "Summary Attendance Line1", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AIS_SUMATTEND1", "eid", "employeeid", 100)
        objUDFEngine.AddAlphaField("@AIS_SUMATTEND1", "ecode", "employeecode", 100)
        objUDFEngine.AddAlphaField("@AIS_SUMATTEND1", "ename", "employeename", 100)
        objUDFEngine.AddFloatField("@AIS_SUMATTEND1", "pdays", "presentdays", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_SUMATTEND1", "weekoff", "weeklyoff", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_SUMATTEND1", "paidholi", "paidholidays", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_SUMATTEND1", "cl", "Casual Leave", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_SUMATTEND1", "el", "EarnLeave", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_SUMATTEND1", "sl", "Seek Leave", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_SUMATTEND1", "compoff", "Comp Off", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_SUMATTEND1", "absent", "Absent", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_SUMATTEND1", "lop", "Loss of Pay", SAPbobsCOM.BoFldSubTypes.st_Price)
        'objUDFEngine.AddFloatField("@AIS_SUMATTEND1", "absent", "Absent", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_SUMATTEND1", "payday", "Payable Days", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_SUMATTEND1", "ot", "OT", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddAlphaField("@AIS_SUMATTEND1", "month", "Month", 100)
        objUDFEngine.AddAlphaField("@AIS_SUMATTEND1", "year", "Year", 100)
        objUDFEngine.AddNumericField("@AIS_SUMATTEND1", "mnth", "Month Number", 3)


        'Define Formula Setup
        objUDFEngine.CreateTable("AIS_FORMULA", "Formula Setup", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddAlphaField("@AIS_FORMULA", "Code", "Code", 50)
        objUDFEngine.AddAlphaField("@AIS_FORMULA", "Name", "Name", 100)
        objUDFEngine.addField("@AIS_FORMULA", "Frmtyp", "formulatype", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.addField("@AIS_FORMULA", "Varpay", "varpaycode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.addField("@AIS_FORMULA", "Varot", "variableot", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.addField("@AIS_FORMULA", "Varded", "variableded", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.addField("@AIS_FORMULA", "Bencod", "benefitcode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.addField("@AIS_FORMULA", "Paycod", "paycode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.addField("@AIS_FORMULA", "DedCod", "Deductioncode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.addField("@AIS_FORMULA", "Oprtn", "operation", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.addField("@AIS_FORMULA", "Num", "Number", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.addField("@AIS_FORMULA", "Frmla", "formula", SAPbobsCOM.BoFieldTypes.db_Memo, 64000, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")

        'Variable Pay Master
        objUDFEngine.CreateTable("AIS_VPAY", "Variable Pay Master", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.addField("@AIS_VPAY", "type", "VariablePay Type", SAPbobsCOM.BoFieldTypes.db_Alpha, 10, SAPbobsCOM.BoFldSubTypes.st_None, "M,Q", "Monthly Wise,Quarterly Wise", "M")
        objUDFEngine.AddAlphaField("@AIS_VPAY", "month", "Month", 30)
        objUDFEngine.AddAlphaField("@AIS_VPAY", "mon", "Month1", 30)
        objUDFEngine.AddAlphaField("@AIS_VPAY", "year", "Year", 30)
        objUDFEngine.AddAlphaField("@AIS_VPAY", "hmonth", "Month Half Yearly", 30)

        objUDFEngine.CreateTable("AIS_VPAYM", "Variable Pay Monthly", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_VPAYM", "empid", "Emp ID", 20)
        objUDFEngine.AddAlphaField("@AIS_VPAYM", "empname", "Emp Name", 50)
        objUDFEngine.AddAlphaField("@AIS_VPAYM", "month", "Month", 50)
        objUDFEngine.AddFloatField("@AIS_VPAYM", "target", "Target", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AIS_VPAYM", "achieve", "Achieved", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AIS_VPAYM", "percent", "Percentage", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_VPAYM", "tot", "Total Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.addField("@AIS_VPAYM", "pay", "Pay Process", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_VPAYM", "flag", "Flag", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "0,1", "0,1", "0")
        objUDFEngine.AddAlphaField("@AIS_VPAYM", "year", "Year", 25)


        objUDFEngine.CreateTable("AIS_VPAYQ", "Variable Pay Quaterly", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_VPAYQ", "empid", "Emp ID", 20)
        objUDFEngine.AddAlphaField("@AIS_VPAYQ", "empname", "Emp Name", 50)
        objUDFEngine.AddAlphaField("@AIS_VPAYQ", "month", "Month", 50)
        objUDFEngine.AddFloatField("@AIS_VPAYQ", "target", "Target", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AIS_VPAYQ", "achm1", "Achieved", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AIS_VPAYQ", "achm2", "Achieved", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AIS_VPAYQ", "achm3", "Achieved", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AIS_VPAYQ", "achieve", "Achieved", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AIS_VPAYQ", "percent", "Percentage", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_VPAYQ", "tot", "Total Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.addField("@AIS_VPAYQ", "pay", "Pay Process", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_VPAYQ", "flag", "Flag", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "0,1", "0,1", "0")
        objUDFEngine.AddAlphaField("@AIS_VPAYQ", "year", "Year", 25)

        objUDFEngine.CreateTable("AIS_VPAYH", "Variable Pay Half-Yearly", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_VPAYH", "empid", "Emp ID", 20)
        objUDFEngine.AddAlphaField("@AIS_VPAYH", "empname", "Emp Name", 50)
        objUDFEngine.AddAlphaField("@AIS_VPAYH", "month", "Month", 50)
        objUDFEngine.AddAlphaField("@AIS_VPAYH", "year", "Year", 25)
        objUDFEngine.AddFloatField("@AIS_VPAYH", "percent", "Percentage", SAPbobsCOM.BoFldSubTypes.st_Percentage)
        objUDFEngine.AddFloatField("@AIS_VPAYH", "tot", "Total Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.addField("@AIS_VPAYH", "pay", "Pay Process", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_VPAYH", "flag", "Flag", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "0,1", "0,1", "0")

        objUDFEngine.CreateTable("AIS_MONTH", "MonthTable", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        objUDFEngine.CreateTable("YEAR", "Year Table", SAPbobsCOM.BoUTBTableType.bott_NoObject)

        objUDFEngine.CreateTable("AIS_SALARY", "SALARY TABLE", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        objUDFEngine.AddAlphaField("@AIS_SALARY", "eid", "EMP ID", 10)
        objUDFEngine.AddAlphaField("@AIS_SALARY", "ename", "EMP Name", 100)
        objUDFEngine.AddFloatField("@AIS_SALARY", "ded", "Deductions", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_SALARY", "net", "Net Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddNumericField("@AIS_SALARY", "month", "Month", 3)
        objUDFEngine.AddNumericField("@AIS_SALARY", "year", "Year", 5)

        objUDFEngine.CreateTable("AIS_QUTR", "QuaterMonth", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        objUDFEngine.CreateTable("AIS_HALF", "Half Year", SAPbobsCOM.BoUTBTableType.bott_NoObject)


        'Document Screeen Tables 
        '---------------------------------------------employee Master-------------------
        objUDFEngine.AddDateField("OHEM", "rldate", "Reliving Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddNumericField("OHEM", "fix", "Fixed", 4)
        objUDFEngine.AddNumericField("OHEM", "vari", "Variable", 4)
        objUDFEngine.AddAlphaField("OHEM", "ratio", "Ratio", 20)
        objUDFEngine.addField("OHEM", "incent", "Incent Payable", SAPbobsCOM.BoFieldTypes.db_Alpha, 50, SAPbobsCOM.BoFldSubTypes.st_None, "Q,M,H", "Quarterly,Monthly,Half-Yearly", "M")
        objUDFEngine.AddAlphaField("OHEM", "pan", "PAN number", 50)
        objUDFEngine.AddAlphaField("OHEM", "pf", "PF number", 50)
        objUDFEngine.AddFloatField("OHEM", "bonus", "Bonus Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.addField("OHEM", "pmode", "Payment Mode", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.AddDateField("OHEM", "cdate", "Conformation Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddFloatField("@HEM3", "salhist", "Salary History", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.addField("OHEM", "tdsslab", "Gender for TDS", SAPbobsCOM.BoFieldTypes.db_Alpha, 20, SAPbobsCOM.BoFldSubTypes.st_None, "Male,Female,Senior Citizen", "Male,Female,Senior Citizen", "")
        objUDFEngine.addField("OHEM", "pflim", "PF Limit Applicable", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "")
        objUDFEngine.AddAlphaField("OHEM", "ecode", "Employee Code", 50)



        '--------employee salary setup-----------
        objUDFEngine.CreateTable("AIS_EMPSSTP", "Employee Salary Setup", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddAlphaField("@AIS_EMPSSTP", "eid", "Employee ID", 100)
        objUDFEngine.AddAlphaField("@AIS_EMPSSTP", "ename", "Employee Name", 250)
        objUDFEngine.AddDateField("@AIS_EMPSSTP", "lprdate", "Lasr Pay Revision Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddAlphaField("@AIS_EMPSSTP", "dectn", "Deduction", 20)
        objUDFEngine.AddNumericField("@AIS_EMPSSTP", "vpfamt", "VPF Amount", 10)
        objUDFEngine.AddAlphaField("@AIS_EMPSSTP", "otcode", "OT Code", 20)
        objUDFEngine.AddAlphaField("@AIS_EMPSSTP", "otbfm", "OT Basic Formula", 20)
        objUDFEngine.AddFloatField("@AIS_EMPSSTP", "otrate", "OT Rate", SAPbobsCOM.BoFldSubTypes.st_Rate)
        objUDFEngine.AddAlphaField("@AIS_EMPSSTP", "month", "Month", 25)
        objUDFEngine.AddAlphaField("@AIS_EMPSSTP", "year", "Year", 25)
        objUDFEngine.addField("@AIS_EMPSSTP", "nodues", "No Dues", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_EMPSSTP", "metro", "Metro or Non Metro", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddNumericField("@AIS_EMPSSTP", "arpaid", "Actual Rent Paid Monthly", 10)
        objUDFEngine.AddFloatField("@AIS_EMPSSTP", "ptotal", "pay code Total", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_EMPSSTP", "dtotal", "Deduction code Total", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_EMPSSTP", "rtotal", "Reimbursement Total", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_EMPSSTP", "btotal", "Benefit code Total", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_EMPSSTP", "total", "Total", SAPbobsCOM.BoFldSubTypes.st_Sum)


        '------------paycode lines------------
        objUDFEngine.CreateTable("AIS_EPSSTP1", "Pay Code Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP1", "pcode", "Pay Code", 20)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP1", "descrpn", "Description", 50)
        objUDFEngine.AddNumericField("@AIS_EPSSTP1", "accode", "Account Code", 10)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP1", "bsicfm", " Basic Formula", 50)
        objUDFEngine.AddFloatField("@AIS_EPSSTP1", "amt", "Amount", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.addField("@AIS_EPSSTP1", "tax", "Taxable", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddAlphaField("@AIS_EPSSTP1", "expnfm", " Excemption Formula", 20)
        objUDFEngine.AddFloatField("@AIS_EPSSTP1", "expdamt", " Excemptec Amount", SAPbobsCOM.BoFldSubTypes.st_Price)

        '-----------deduction code lines-----------
        objUDFEngine.CreateTable("AIS_EPSSTP2", "Deduction Code Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP2", "dcode", "Deduction Code", 20)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP2", "descrpn", "Description", 50)
        objUDFEngine.AddNumericField("@AIS_EPSSTP2", "accode", "Account Code", 10)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP2", "bsicfm", " Basic Formula", 50)
        objUDFEngine.AddFloatField("@AIS_EPSSTP2", "amt", "Amount", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.addField("@AIS_EPSSTP2", "tax", "Taxable", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddAlphaField("@AIS_EPSSTP2", "expnfm", " Excemption Formula", 20)
        objUDFEngine.AddFloatField("@AIS_EPSSTP2", "expdamt", " Excemptec Amount", SAPbobsCOM.BoFldSubTypes.st_Price)

        '-----------reimbursement lies-----------------
        objUDFEngine.CreateTable("AIS_EPSSTP3", "Reimbursement Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP3", "remhed", "Reimbursement Head", 20)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP3", "descrpn", "Description", 50)
        objUDFEngine.AddNumericField("@AIS_EPSSTP3", "payac", "Pay Account ", 10)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP3", "epnac", " Expense Account", 10)
        objUDFEngine.AddFloatField("@AIS_EPSSTP3", "limit", "Limit", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP3", "maxamt", " Max Amount", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP3", "amt", "  Amount", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP3", "balamt", "Balance Amount", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddAlphaMemoField("@AIS_EPSSTP3", "remarks", "Remarks", 250)

        '-------------arrear lines----------
        objUDFEngine.CreateTable("AIS_EPSSTP4", "Arrear Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP4", "arrcode", "Arrear Code", 20)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP4", "descrpn", "Description", 50)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP4", "descrpn", "Description", 50)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP4", "descrpn", "Description", 50)
        objUDFEngine.AddDateField("@AIS_EPSSTP4", "eftfrm", "Effective From", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_EPSSTP4", "eftto", " Effective To", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP4", "month", "Month", 20)
        objUDFEngine.addField("@AIS_EPSSTP4", "year", " Year", SAPbobsCOM.BoFieldTypes.db_Alpha, 10, SAPbobsCOM.BoFldSubTypes.st_None, "2010-11,2011-12,2012-13,2013-14,2014-15,2015-16,2016-17,2017-18,2018-19,2019-20,2020-21", "1,2,3,4,5,6,7,8,9,10,11", "")
        objUDFEngine.AddAlphaField("@AIS_EPSSTP4", "bsicfm", " Basic Formula", 50)
        objUDFEngine.AddNumericField("@AIS_EPSSTP4", "prevstr", "Previous Structure", 10)
        objUDFEngine.AddNumericField("@AIS_EPSSTP4", "newstr", "New Structure", 10)
        objUDFEngine.AddFloatField("@AIS_EPSSTP4", "total", "Line Total", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_EPSSTP4", "htotal", "Head Total", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddAlphaMemoField("@AIS_EPSSTP4", "remarks", "Remarks", 250)

        '------------benefit lines-----------
        objUDFEngine.CreateTable("AIS_EPSSTP5", "Benefits Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP5", "bcode", "Benefit Code", 20)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP5", "descrpn", "Description", 50)
        objUDFEngine.AddNumericField("@AIS_EPSSTP5", "actcode", "Account Code ", 10)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP5", "bsicfm", " Basic Formula", 50)
        objUDFEngine.AddFloatField("@AIS_EPSSTP5", "amt", " Amount", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.addField("@AIS_EPSSTP5", "tax", "Taxable", SAPbobsCOM.BoFieldTypes.db_Alpha, 1, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddAlphaField("@AIS_EPSSTP5", "expnfm", " Excemption Formula", 20)
        objUDFEngine.AddFloatField("@AIS_EPSSTP5", "expdamt", " Excemptec Amount", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.addField("@AIS_EPSSTP5", "year", " Year", SAPbobsCOM.BoFieldTypes.db_Alpha, 10, SAPbobsCOM.BoFldSubTypes.st_None, "2010-11,2011-12,2012-13,2013-14,2014-15,2015-16,2016-17,2017-18,2018-19,2019-20,2020-21", "1,2,3,4,5,6,7,8,9,10,11", "")

        '-------------leave lines---------
        objUDFEngine.CreateTable("AIS_EPSSTP6", "Leave Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP6", "lcode", "Leave Code", 20)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP6", "descrpn", "Description", 50)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP6", "bsicfm", " Basic Formula", 50)
        objUDFEngine.AddNumericField("@AIS_EPSSTP6", "maxday", "Maximum Days", 10)
        objUDFEngine.AddNumericField("@AIS_EPSSTP6", "dayavi", " Day Availed", 10)
        objUDFEngine.AddNumericField("@AIS_EPSSTP6", "levbal", " leave Balance", 10)

        '-------------loan and advance lines---------
        objUDFEngine.CreateTable("AIS_EPSSTP7", "Loan and Advance Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP7", "loncode", "Loan Code", 20)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP7", "descrpn", "Description", 50)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP7", "type", "Type", 50)
        objUDFEngine.AddFloatField("@AIS_EPSSTP7", "lonamt", " Loan Amount", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP7", "instyp", "Interest Type", 30)
        objUDFEngine.AddNumericField("@AIS_EPSSTP7", "rate", " Rate", 10)
        objUDFEngine.AddDateField("@AIS_EPSSTP7", "lonfrm", "Loan From", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_EPSSTP7", "lonto", " Loan To", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddFloatField("@AIS_EPSSTP7", "amt", "  Amount", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP7", "insmnt", "Instalment", SAPbobsCOM.BoFldSubTypes.st_Price)
        'objUDFEngine.AddAlphaField("@AIS_EPSSTP7", "insmnt", "Instalment", 30)

        '-------------Investment Details---------
        objUDFEngine.CreateTable("AIS_EPSSTP8", "Investment Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP8", "type", "Type", 20, "R,A", "Rebate,A", "R")
        objUDFEngine.AddAlphaField("@AIS_EPSSTP8", "plccode", "Pollcy Code", 30)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP8", "dscrpn", "Description", 30)
        objUDFEngine.AddDateField("@AIS_EPSSTP8", "dcdate", "Declaration Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP8", "pvisn", "Provision", 30)
        objUDFEngine.AddFloatField("@AIS_EPSSTP8", "amt", "  Amount", SAPbobsCOM.BoFldSubTypes.st_Price)


        '-------------Perquisite Details---------
        objUDFEngine.CreateTable("AIS_EPSSTP9", "Perquisites Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP9", "pcode", "Perquisite Code", 30)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP9", "dscrpn", "Description", 30)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP9", "catgry", "Category", 30)
        objUDFEngine.AddFloatField("@AIS_EPSSTP9", "vlperst", "Value of Perquisite", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP9", "amtpe", "  Amount Paid by Employee", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.addField("@AIS_EPSSTP9", "year", " Year", SAPbobsCOM.BoFieldTypes.db_Alpha, 10, SAPbobsCOM.BoFldSubTypes.st_None, "2010-11,2011-12,2012-13,2013-14,2014-15,2015-16,2016-17,2017-18,2018-19,2019-20,2020-21", "1,2,3,4,5,6,7,8,9,10,11", "2013-14")
        objUDFEngine.AddFloatField("@AIS_EPSSTP9", "amtte", "  Amount Taxable Employee", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddAlphaField("@AIS_EPSSTP9", "perstus", "Perquisite U/s", 20, "17(12)", "17(21)", "")
        objUDFEngine.AddAlphaField("@AIS_EPSSTP9", "type", "Type", 20, "N,A", "Normal,Arrear", "N")

        '-------------Salary Details---------
        objUDFEngine.CreateTable("AIS_EPSSTP10", "Salary Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "salmnth", "Salary for month  ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "mnth", "Month  ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "hra", "HRA Taxable  ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "convync", " Conveyance ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "vlprst", "Value of perquisite", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "protax", " Profession Tax ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "other", " Others ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "pcdmnth", "Processed Month ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "pcdsal", "Processed Salary  ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "tiuhs", "Income Under Head Salary", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "sal", "Salary  ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "txdets", " Tax Deducted at Sources ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "pf", "PF  ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "vpf", " VPF ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "pt", " PT ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "currpf", "Current VPF ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP10", "currpt", "Current PT ", SAPbobsCOM.BoFldSubTypes.st_Price)

        '-------------Income Details---------
        objUDFEngine.CreateTable("AIS_EPSSTP11", "Income Details", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "sal", " Salary ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "inhsp", "  Income/Loss Praporty", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "prbspf", " Profit/Loss Profession ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "cgain", " Capital Gains ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "othsurc", " Other Sources ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "licmex", " Loss/Income Claim exempt ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "othdect", "Others Deductions ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "gincme", " Gross Total Income", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "ldetch", " Less Deduction Chapter ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "totxin", "Total Tax Income", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "clremib", " Claim Reimb", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "dbursmnt", " Dibursement", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "nttxin", "Net Tax Income  ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "ttds", " TDS on Net Tax Income ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "tdsdet", "TDS Already Deducted", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "relief", "Relief", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "nttxpy", " Net Taxable Payable ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "surchge", " Surcharge ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "edcess", " Education Cess ", SAPbobsCOM.BoFldSubTypes.st_Price)
        objUDFEngine.AddFloatField("@AIS_EPSSTP11", "hedcess", " Higher Education Cess ", SAPbobsCOM.BoFldSubTypes.st_Price)

        'Income/Loss House Property calculation:
        objUDFEngine.CreateTable("AIS_OILH", "Income Loss House", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddAlphaField("@AIS_OILH", "empid", "Emp ID", 20)
        objUDFEngine.AddAlphaField("@AIS_OILH", "empname", "Emp Name", 100)
        objUDFEngine.addField("@AIS_OILH", "self", "Self Occupied", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_OILH", "rent", "Rented Out", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddFloatField("@AIS_OILH", "pamt", "Principal Amt", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OILH", "int", "Interest Amt", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OILH", "preemi", "Pre EMI", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OILH", "rentrcd", "Rental Received", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OILH", "ppaid", "Property Paid", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OILH", "main", "Maintainance", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OILH", "total", "Total Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OILH", "per", "Percentage", SAPbobsCOM.BoFldSubTypes.st_Percentage)




        'Investment Details Table
        objUDFEngine.CreateTable("AIS_INVST", "Investment Details", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        objUDFEngine.AddAlphaField("@AIS_INVST", "prvsn", "Provisions", 20)

        '------------Employee master-----------
        objUDFEngine.AddDateField("OHEM", "rldate", "Reliving Date", SAPbobsCOM.BoFldSubTypes.st_None)

        'Salary Process Header Table
        objUDFEngine.CreateTable("AIS_ALPS", "Salary Process", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddAlphaField("@AIS_ALPS", "month", "Month", 10)
        objUDFEngine.AddAlphaField("@AIS_ALPS", "year", "Year", 10)
        objUDFEngine.AddAlphaField("@AIS_ALPS", "branch", "Branch", 50)
        objUDFEngine.AddAlphaField("@AIS_ALPS", "Dept", "Dept", 50)

        'Salary Process Line Table
        objUDFEngine.CreateTable("AIS_ALPSL", "Salary Process", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AIS_ALPSL", "empid", "EMP ID", 50)
        objUDFEngine.AddAlphaField("@AIS_ALPSL", "empname", "EMP NAME", 50)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "tdays", "Total Days", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "payday", "Payable Days", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "basic", "Earned Basic", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "hra", "Earned HRA", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "con", "Earned Convy", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "medall", "Medical Allowance", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "eduall", "Educational Allowance", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "tele", "Telephone Allowance", SAPbobsCOM.BoFldSubTypes.st_Sum)

        objUDFEngine.AddFloatField("@AIS_ALPSL", "treim", "Total Reimbursement", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "tloan", "Total Loan", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "tarrer", "Total Arrears", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "totot", "Total OT", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "oadd", "Other Additions1", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "bonus", "bonus", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "paidhol", "Paid Holidays", SAPbobsCOM.BoFldSubTypes.st_Sum)

        objUDFEngine.AddFloatField("@AIS_ALPSL", "pf", "Deducted PF", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "esi", "Deducted ESI", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "adv", "Advance", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "pt", "Deducted PT", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "it", "Deducted IT", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "bonpble", "Bonus Payable", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "canded", "Canteen Deduction", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "dpaidhol", "Deduct Paid Holidays", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "dedot", "Deducted OT", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "lwf", "LWF", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "tds", "Deducted TDS", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "adv", "Salary Advance", SAPbobsCOM.BoFldSubTypes.st_Sum)

        objUDFEngine.AddFloatField("@AIS_ALPSL", "oded", "Other Deductions", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "near", "Net Earning", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "nded", "Net Deductions", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_ALPSL", "npay", "Net Pay", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.addField("@AIS_ALPSL", "pay", "Pay Process", SAPbobsCOM.BoFieldTypes.db_Alpha, 4, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddAlphaField("@AIS_ALPSL", "remark", "Remarks", 250)
        objUDFEngine.AddAlphaField("@AIS_ALPSL", "month", "Month", 3)
        objUDFEngine.AddAlphaField("@AIS_ALPSL", "year", "Year", 5)

        '  ------ Process Salery Pay
        objUDFEngine.CreateTable("AIS_PRSAL", "Process Salery Pay", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddAlphaField("@AIS_PRSAL", "month", "Month", 27)
        objUDFEngine.AddAlphaField("@AIS_PRSAL", "year", "Year", 27)
        objUDFEngine.AddAlphaField("@AIS_PRSAL", "dept", "Dept", 27)
        objUDFEngine.AddAlphaField("@AIS_PRSAL", "branch", "Branch", 27)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "tot", "Total Net Pay", SAPbobsCOM.BoFldSubTypes.st_Sum)
        'Additions
        objUDFEngine.AddFloatField("@AIS_PRSAL", "basic", "Basic", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "hra", "HRA", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "con", "Conveyance", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "medall", "Medical", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "eduall", "Education", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "tel", "Telephone", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "reimb", "Reimbursement", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "arrear", "Arrear", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "ot", "OverTime", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "oadd", "Other Addition", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "ebonus", "Earned Bonus", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "epdholi", "Earned Paid Holidays", SAPbobsCOM.BoFldSubTypes.st_Sum)

        'Deductions
        objUDFEngine.AddFloatField("@AIS_PRSAL", "loan", "Loan", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "bonpble", "Bonus Payable", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "pf", "Providend Fund", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "esi", "ESI", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "pt", "Professional Tax", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "it", "Income Tax", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "cant", "Canteen", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "dpdholi", "Ded Paid Holidays", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "dot", "Ded OverTime", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "lwf", "LWF", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "tds", "TDS", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_PRSAL", "oded", "Other Deductions", SAPbobsCOM.BoFldSubTypes.st_Sum)


        objUDFEngine.CreateTable("AIS_PRSAL1", "Process Sal Lines", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AIS_PRSAL1", "empid", "Employee ID", 10)
        objUDFEngine.AddAlphaField("@AIS_PRSAL1", "empname", "Employee Name", 27)
        objUDFEngine.AddFloatField("@AIS_PRSAL1", "pdays", "Paid Days", SAPbobsCOM.BoFldSubTypes.st_Quantity)
        objUDFEngine.AddFloatField("@AIS_PRSAL1", "netpay", "Net Pay", SAPbobsCOM.BoFldSubTypes.st_Sum)



        'Leave Requisition:
        objUDFEngine.CreateTable("AIS_LVREQ", "Leave Details", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddNumericField("@AIS_LVREQ", "reqno", "Request No", 10)
        objUDFEngine.AddAlphaField("@AIS_LVREQ", "empno", "employee no", 50)
        objUDFEngine.AddDateField("@AIS_LVREQ", "date", "date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddAlphaField("@AIS_LVREQ", "empname", "employee name", 100)
        objUDFEngine.AddAlphaField("@AIS_LVREQ", "addr", "employee address", 100)
        objUDFEngine.AddDateField("@AIS_LVREQ", "frmdate", "from date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_LVREQ", "todate", "to date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddNumericField("@AIS_LVREQ", "days", "leave days", 10)
        objUDFEngine.addField("@AIS_LVREQ", "status", "leave status", SAPbobsCOM.BoFieldTypes.db_Alpha, 25, SAPbobsCOM.BoFldSubTypes.st_None, "A,U,R", "Approved,Unapproved,Rejected", "")
        objUDFEngine.addField("@AIS_LVREQ", "cancel", "cancel", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.addField("@AIS_LVREQ", "alert", "alert", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddAlphaField("@AIS_LVREQ", "leavappr", "leave approver", 100)
        objUDFEngine.AddAlphaField("@AIS_LVREQ", "remarks", "remarks", 200)
        objUDFEngine.AddAlphaField("@AIS_LVREQ", "manager", "manager", 50)


        objUDFEngine.CreateTable("AIS_LVREQ1", "Leave Details Line", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AIS_LVREQ1", "leavcode", "leavecode", 25)
        objUDFEngine.AddNumericField("@AIS_LVREQ1", "appdays", "leave applied", 10)
        objUDFEngine.AddNumericField("@AIS_LVREQ1", "baldays", "balance days", 10)

        objUDFEngine.CreateTable("AIS_LVUSER", "User and Manager", SAPbobsCOM.BoUTBTableType.bott_NoObject)
        objUDFEngine.AddAlphaField("@AIS_LVUSER", "usrid", "user id", 25)
        objUDFEngine.AddAlphaField("@AIS_LVUSER", "mngrid", "manager id", 25)


        'In out Time - Document Table
        objUDFEngine.CreateTable("AIS_INOUTTIME", "Emp InOut Time", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddAlphaField("@AIS_INOUTTIME", "status", "status", 50)
        objUDFEngine.AddAlphaField("@AIS_INOUTTIME", "eid", "EmpID", 50)
        objUDFEngine.AddAlphaField("@AIS_INOUTTIME", "ename", "EmpName", 100)
        objUDFEngine.AddDateField("@AIS_INOUTTIME", "date", "DailyDate", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddAlphaField("@AIS_INOUTTIME", "attd", "DailyAttd", 15)
        objUDFEngine.AddDateField("@AIS_INOUTTIME", "intime", "InTime", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddDateField("@AIS_INOUTTIME", "otime", "EmpOuttime", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddDateField("@AIS_INOUTTIME", "ovrtime", "EmpOvertime", SAPbobsCOM.BoFldSubTypes.st_Time)
        objUDFEngine.AddDateField("@AIS_INOUTTIME", "total", "EmpTotalTime", SAPbobsCOM.BoFldSubTypes.st_Time)


        'Benefit Detail Transaction
        objUDFEngine.CreateTable("AIS_OBNDT", "Benefit Detail", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddAlphaField("@AIS_OBNDT", "empid", "Emp Id", 50)
        objUDFEngine.AddAlphaField("@AIS_OBNDT", "empnam", "Emp Name", 50)
        objUDFEngine.AddAlphaField("@AIS_OBNDT", "empcd", "Emp Code", 50)
        objUDFEngine.AddFloatField("@AIS_OBNDT", "amount", "Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)

        objUDFEngine.CreateTable("AIS_BNDT1", "Benefit Detail line", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.addField("@AIS_BNDT1", "paid", "Paid", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.AddDateField("@AIS_BNDT1", "date", "Due Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddNumericField("@AIS_BNDT1", "amnt", "Amount1", 10)
        objUDFEngine.AddAlphaMemoField("@AIS_BNDT1", "remarks", "Remarks", 250)

        'Loan Advance Schedule Setup
        objUDFEngine.CreateTable("AIS_OLNAD", "Loan Advance Schedule", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddAlphaField("@AIS_OLNAD", "empid", "Emp Id", 50)
        objUDFEngine.AddAlphaField("@AIS_OLNAD", "empnam", "Emp Name", 50)
        objUDFEngine.AddAlphaField("@AIS_OLNAD", "lntyp", "Loan Type", 50)
        objUDFEngine.AddAlphaField("@AIS_OLNAD", "lnfrm", "Loan Period From", 50)
        objUDFEngine.AddAlphaField("@AIS_OLNAD", "lnto", "Loan Period To", 50)

        objUDFEngine.CreateTable("AIS_LNAD1", "Loan Advance Schedule Line", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddDateField("@AIS_LNAD1", "date", "Due Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddNumericField("@AIS_LNAD1", "amount", "Amount", 10)
        objUDFEngine.AddNumericField("@AIS_LNAD1", "intrst", "Interest", 10)
        objUDFEngine.AddNumericField("@AIS_LNAD1", "total", "Total", 10)
        objUDFEngine.addField("@AIS_LNAD1", "paid", "Paid", SAPbobsCOM.BoFieldTypes.db_Alpha, 5, SAPbobsCOM.BoFldSubTypes.st_None, "", "", "")
        objUDFEngine.AddAlphaField("@AIS_LNAD1", "remrk", "Remarks", 50)

        'Employee Assets Details:
        objUDFEngine.CreateTable("AIS_OAST", "Employee Assets", SAPbobsCOM.BoUTBTableType.bott_MasterData)
        objUDFEngine.AddAlphaField("@AIS_OAST", "empid", "Employee ID", 25)
        objUDFEngine.AddAlphaField("@AIS_OAST", "name", "Employee Name", 100)

        objUDFEngine.CreateTable("AIS_AST1", "Employee Assets", SAPbobsCOM.BoUTBTableType.bott_MasterDataLines)
        objUDFEngine.AddAlphaField("@AIS_AST1", "aid", "Asset ID", 100)
        objUDFEngine.AddAlphaField("@AIS_AST1", "desc", "Descrption", 100)
        objUDFEngine.AddFloatField("@AIS_AST1", "value", "Value", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_AST1", "issqty", "Issued Quantity", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_AST1", "retqty", "Return Quantity", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddDateField("@AIS_AST1", "isdate", "Issued Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddAlphaField("@AIS_AST1", "isby", "Issued By", 100)
        objUDFEngine.AddDateField("@AIS_AST1", "rtndate", "Return Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.addField("@AIS_AST1", "return", "Return", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddAlphaField("@AIS_AST1", "remarks", "Remarks", 253)


        'Cash payment Emplpyees Lists:
        objUDFEngine.CreateTable("AIS_OCPT", "Cash payment Employee List", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.CreateTable("AIS_CPT1", "Cash Payment Line", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AIS_CPT1", "eid", "Emp ID", 25)
        objUDFEngine.AddAlphaField("@AIS_CPT1", "ename", "Emp Name", 100)
        objUDFEngine.AddAlphaField("@AIS_CPT1", "month", "Month", 15)
        objUDFEngine.AddNumericField("@AIS_CPT1", "year", "year", 5)
        objUDFEngine.AddFloatField("@AIS_CPT1", "amount", "Total Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.addField("@AIS_CPT1", "paid", "Paid", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")

        'Reimbursement Entry Details: -  Header:
        objUDFEngine.CreateTable("AIS_ORED", "Reimbusement Entry Details", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddAlphaField("@AIS_ORED", "ecode", "Employee Code", 25)
        objUDFEngine.AddAlphaField("@AIS_ORED", "ename", "Employee Name", 100)
        objUDFEngine.AddNumericField("@AIS_ORED", "month", "Month", 3)
        objUDFEngine.AddNumericField("@AIS_ORED", "year ", "Year", 5)
        'Reimbursement Entry Details: - Line  Items:
        objUDFEngine.CreateTable("AIS_RED1", "Reimbusement Entry Line", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AIS_RED1", "code", "Reimbursement Code", 25)
        objUDFEngine.AddAlphaField("@AIS_RED1", "desc", "Reimbursement Description", 100)
        objUDFEngine.AddFloatField("@AIS_RED1", "limit", "Reimbursement Limit", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_RED1", "avlamt", "Available Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_RED1", "clmamt", "Claim Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_RED1", "amtpaid", "Paid Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.addField("@AIS_RED1", "pay", "Paid or Not", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")


        'Employee Salary Processing for Dynamically Generated columns:
        objUDFEngine.CreateTable("AIS_OSPS", "Salary Processing Header", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddAlphaField("@AIS_OSPS", "empid", "Employee ID", 25)
        objUDFEngine.AddAlphaField("@AIS_OSPS", "ename", "Employee Name", 100)
        objUDFEngine.AddAlphaField("@AIS_OSPS", "heads", "Title Objects", 100)
        objUDFEngine.AddAlphaField("@AIS_OSPS", "amount", "Amount", 253)
        objUDFEngine.AddNumericField("@AIS_OSPS", "month", "Processed Month", 3)
        objUDFEngine.AddNumericField("@AIS_OSPS", "year", "Processed Year", 6)
        objUDFEngine.AddAlphaField("@AIS_OSPS", "fyear", "Fin Year", 40)
        objUDFEngine.addField("@AIS_OSPS", "pay", "Paid or Not", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")


        objUDFEngine.CreateTable("AIS_SPS1", "Salary Processing Line", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objUDFEngine.AddAlphaField("@AIS_SPS1", "empid", "Employee ID", 25)
        objUDFEngine.AddAlphaField("@AIS_SPS1", "ename", "Employee Name", 100)
        objUDFEngine.AddFloatField("@AIS_SPS1", "tdays", "Total Days", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_SPS1", "pdays", "Payable Days", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddAlphaField("@AIS_SPS1", "heads", "Title Objects", 100)
        objUDFEngine.AddFloatField("@AIS_SPS1", "amount", "Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.addField("@AIS_SPS1", "pay", "Paid or Not", SAPbobsCOM.BoFieldTypes.db_Alpha, 3, SAPbobsCOM.BoFldSubTypes.st_None, "Y,N", "Yes,No", "N")
        objUDFEngine.AddAlphaField("@AIS_SPS1", "remarks", "Remarks", 253)
        objUDFEngine.AddNumericField("@AIS_SPS1", "month", "Processed Month", 3)
        objUDFEngine.AddNumericField("@AIS_SPS1", "year", "Processed Year", 6)


        'Full and Final Settlements:
        objUDFEngine.CreateTable("AIS_OFFS", "Full & Final Settlement", SAPbobsCOM.BoUTBTableType.bott_Document)
        objUDFEngine.AddAlphaField("@AIS_OFFS", "eid", "Employee ID", 20)
        objUDFEngine.AddAlphaField("@AIS_OFFS", "ename", "Employee Name", 100)
        objUDFEngine.AddDateField("@AIS_OFFS", "sdate", "Date of Joining", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddDateField("@AIS_OFFS", "edate", "End Date", SAPbobsCOM.BoFldSubTypes.st_None)
        objUDFEngine.AddFloatField("@AIS_OFFS", "salary", "Emp Salary", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OFFS", "reimb", "Reimbursement", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OFFS", "lencash", "Leave Encashment", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OFFS", "oadd", "Other Additions", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OFFS", "ded", "Deductions", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OFFS", "oded", "Other Deductions", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OFFS", "grat", "Gratuity Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OFFS", "final", "Final Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OFFS", "setamt", "Settlement Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddFloatField("@AIS_OFFS", "balamt", "Balance Amount", SAPbobsCOM.BoFldSubTypes.st_Sum)
        objUDFEngine.AddAlphaField("@AIS_OFFS", "setact", "Settlement Account", 50)
        objUDFEngine.AddAlphaField("@AIS_OFFS", "remark", "Remarks", 253)

        'SA Password Table:

        objUDFEngine.CreateTable("AIS_SAPWD", "SQL Credential", SAPbobsCOM.BoUTBTableType.bott_NoObject)


        objAddOn.objApplication.SetStatusBarMessage("All Tables & Fields Created successfully", SAPbouiCOM.BoMessageTime.bmt_Short, False)

        '*******************  Table ******************* End**********************'
    End Sub
    Private Sub objApplication_RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean) Handles objApplication.RightClickEvent
        If eventInfo.BeforeAction Then
        End If
    End Sub
    Private Sub createUDO1(ByVal tblname As String, ByVal udocode As String, ByVal udoname As String, ByVal type As SAPbobsCOM.BoUDOObjType, Optional ByVal DfltForm As Boolean = False, Optional ByVal FindForm As Boolean = False)
        objAddOn.objApplication.SetStatusBarMessage("UDO Created Please Wait..", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
        Dim creationPackage As SAPbouiCOM.FormCreationParams
        Dim objform As SAPbouiCOM.Form
        'Dim i As Integer
        Dim c_Yes As SAPbobsCOM.BoYesNoEnum = SAPbobsCOM.BoYesNoEnum.tYES
        Dim lRetCode As Long
        oUserObjectMD = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
        If Not oUserObjectMD.GetByKey(udocode) Then
            oUserObjectMD.Code = udocode
            oUserObjectMD.Name = udoname
            oUserObjectMD.ObjectType = type
            oUserObjectMD.TableName = tblname
            oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
            If DfltForm = True Then
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES


            End If
            If FindForm = True Then
                If type = SAPbobsCOM.BoUDOObjType.boud_MasterData Then
                    oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES

                    oUserObjectMD.FindColumns.ColumnAlias = "Code"
                    oUserObjectMD.FindColumns.ColumnDescription = "Code"
                    oUserObjectMD.FindColumns.Add()
                    oUserObjectMD.FindColumns.ColumnAlias = "Name"
                    oUserObjectMD.FindColumns.ColumnDescription = "Name"
                    oUserObjectMD.FindColumns.Add()


                ElseIf type = SAPbobsCOM.BoUDOObjType.boud_Document Then
                    oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                    Select Case udoname
                    End Select
                End If
            End If
            'If childTable.Length > 0 Then
            '    For i = 0 To childTable.Length - 2
            '        If Trim(childTable(i)) <> "" Then
            '            oUserObjectMD.ChildTables.TableName = childTable(i)
            '            oUserObjectMD.ChildTables.Add()
            '        End If
            '    Next
            'End If
            lRetCode = oUserObjectMD.Add()
            If lRetCode <> 0 Then

                MsgBox("error" + CStr(lRetCode))
                MsgBox(objAddOn.objCompany.GetLastErrorDescription)
            Else

            End If
            If DfltForm = True Then
                creationPackage = objAddOn.objApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)
                ' Need to set the parameter with the object unique ID
                creationPackage.ObjectType = "1"
                creationPackage.UniqueID = udoname
                creationPackage.FormType = udoname
                creationPackage.BorderStyle = SAPbouiCOM.BoFormTypes.ft_Fixed
                objform = objAddOn.objApplication.Forms.AddEx(creationPackage)
            End If
        End If

        System.Runtime.InteropServices.Marshal.ReleaseComObject(oUserObjectMD)
        oUserObjectMD = Nothing
        GC.Collect()
        objAddOn.objApplication.SetStatusBarMessage("UDO Created Successfully..    " & udoname & "", SAPbouiCOM.BoMessageTime.bmt_Short, False)
    End Sub

    Private Sub createUDO(ByVal tblname As String, ByVal udocode As String, ByVal udoname As String, ByVal childTable() As String, ByVal type As SAPbobsCOM.BoUDOObjType, Optional ByVal DfltForm As Boolean = False, Optional ByVal FindForm As Boolean = False)
        Dim oUserObjectMD As SAPbobsCOM.UserObjectsMD
        Dim creationPackage As SAPbouiCOM.FormCreationParams
        Dim objform As SAPbouiCOM.Form
        Dim i As Integer
        'Dim c_Yes As SAPbobsCOM.BoYesNoEnum = SAPbobsCOM.BoYesNoEnum.tYES
        Dim lRetCode As Long
        oUserObjectMD = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
        If Not oUserObjectMD.GetByKey(udoname) Then
            oUserObjectMD.Code = udocode
            oUserObjectMD.Name = udoname
            oUserObjectMD.ObjectType = type
            oUserObjectMD.TableName = tblname
            oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
            oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
            If DfltForm = True Then
                oUserObjectMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanClose = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tYES
                oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanLog = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tNO
                oUserObjectMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES

                oUserObjectMD.FormColumns.FormColumnAlias = "Code"
                oUserObjectMD.FormColumns.FormColumnDescription = "Code"
                oUserObjectMD.FormColumns.Add()
                oUserObjectMD.FormColumns.FormColumnAlias = "Name"
                oUserObjectMD.FormColumns.FormColumnDescription = "Name"
                oUserObjectMD.FormColumns.Add()
            End If
            If FindForm = True Then
                If type = SAPbobsCOM.BoUDOObjType.boud_MasterData Then
                    oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                    Select Case udocode

                        Case "UDO Code"
                            oUserObjectMD.FindColumns.ColumnAlias = "U_Code"
                            oUserObjectMD.FindColumns.ColumnDescription = "Code"
                            oUserObjectMD.FindColumns.Add()
                            oUserObjectMD.FindColumns.ColumnAlias = "U_Name"
                            oUserObjectMD.FindColumns.ColumnDescription = "Name"
                            oUserObjectMD.FindColumns.Add()

                    End Select
                Else
                    oUserObjectMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
                    Select Case udocode

                        Case "UDO Code"
                            oUserObjectMD.FindColumns.ColumnAlias = "DocNum"
                            oUserObjectMD.FindColumns.ColumnDescription = "DocNum"
                            oUserObjectMD.FindColumns.Add()
                            oUserObjectMD.FindColumns.ColumnAlias = "U_workno"
                            oUserObjectMD.FindColumns.ColumnDescription = "Printing work order no"
                            oUserObjectMD.FindColumns.Add()
                            oUserObjectMD.FindColumns.ColumnAlias = "U_custcode"
                            oUserObjectMD.FindColumns.ColumnDescription = "Customer Code"
                            oUserObjectMD.FindColumns.Add()
                            oUserObjectMD.FindColumns.ColumnAlias = "U_custname"
                            oUserObjectMD.FindColumns.ColumnDescription = "Customer Name"
                            oUserObjectMD.FindColumns.Add()
                            oUserObjectMD.FindColumns.ColumnAlias = "U_date"
                            oUserObjectMD.FindColumns.ColumnDescription = "Date"
                            oUserObjectMD.FindColumns.Add()


                    End Select
                End If
            End If
            If childTable.Length > 0 Then
                For i = 0 To childTable.Length - 2
                    If Trim(childTable(i)) <> "" Then
                        oUserObjectMD.ChildTables.TableName = childTable(i)
                        oUserObjectMD.ChildTables.Add()
                    End If
                Next
            End If
            lRetCode = oUserObjectMD.Add()
            If lRetCode <> 0 Then
                'MsgBox("error" + CStr(lRetCode))
                'MsgBox(objAddOn.objCompany.GetLastErrorDescription)
            Else

            End If
            If DfltForm = True Then
                creationPackage = objAddOn.objApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)
                ' Need to set the parameter with the object unique ID
                creationPackage.ObjectType = "1"
                creationPackage.UniqueID = udoname
                creationPackage.FormType = udoname
                creationPackage.BorderStyle = SAPbouiCOM.BoFormTypes.ft_Fixed
                objform = objAddOn.objApplication.Forms.AddEx(creationPackage)
            End If
        End If
        objAddOn.objApplication.SetStatusBarMessage("UDO Created Successfully..   + '" & udoname & "'", SAPbouiCOM.BoMessageTime.bmt_Short, False)
    End Sub

    Private Sub objApplication_AppEvent(ByVal EventType As SAPbouiCOM.BoAppEventTypes) Handles objApplication.AppEvent
        If EventType = SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged Or EventType = SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition Or EventType = SAPbouiCOM.BoAppEventTypes.aet_ShutDown Then
            Try
                ' objUIXml.LoadMenuXML("RemoveMenu.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded)
                If objCompany.Connected Then objCompany.Disconnect()
                objCompany = Nothing
                objApplication = Nothing
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objCompany)
                System.Runtime.InteropServices.Marshal.ReleaseComObject(objApplication)
                GC.Collect()
            Catch ex As Exception
            End Try
            End
        End If
    End Sub

    Private Sub applyFilter()
        Dim oFilters As SAPbouiCOM.EventFilters
        Dim oFilter As SAPbouiCOM.EventFilter
        oFilters = New SAPbouiCOM.EventFilters
        'Item Master Data 
        oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_GOT_FOCUS)
        oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_MENU_CLICK)
        oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_FORM_ACTIVATE)
        oFilter = oFilters.Add(SAPbouiCOM.BoEventTypes.et_RIGHT_CLICK)
    End Sub

    Public Sub stat()
        Dim objRs As SAPbobsCOM.Recordset
        objRs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRs.DoQuery("select 1 from [@AIS_STAT]")
        If objRs.RecordCount = 0 Then
            Dim objGD As SAPbobsCOM.GeneralData
            Dim objGS As SAPbobsCOM.GeneralService
            ' Dim i, j As Integer
            objGS = objAddOn.objCompany.GetCompanyService.GetGeneralService("AIS_Stat")
            objGD = objGS.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData)
            objGD.SetProperty("Code", "1")
            objGS.Add(objGD)
        End If
    End Sub

End Class


