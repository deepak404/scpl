﻿Public Class ClsDefineDeductionCode
    Public Const formtype = "DefineDeductionCode"
    Dim objForm As SAPbouiCOM.Form
    Dim objCFL As SAPbouiCOM.ChooseFromListEvent
    Dim objDT As SAPbouiCOM.DataTable
    Dim objItem As SAPbouiCOM.Item

    Public Sub loadscreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("DefineDeductionCode.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.Items.Item("6").Enabled = False
        objForm.Items.Item("6").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_DDC")
        'objForm.EnableMenu("1281", True)
        Try
            objAddOn.objApplication.Menus.Item("1281").Enabled = True
        Catch ex As Exception

        End Try

        ' objAddOn.objApplication.Menus.Item("1282").Enabled = True
        objForm.DataBrowser.BrowseBy = "21"
        objForm.AutoManaged = True
        AccountCodeSelection() ' Set Condition for CFL:
        objItem = objForm.Items.Item("4")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        objItem = objForm.Items.Item("6")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" Then
                            If objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                If FormValidation() = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                            End If
                        End If
                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.ItemUID = "9" Then
                            GlCFL(pVal)
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                            objForm.Items.Item("6").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_DDC")

                        ElseIf pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            objForm.Items.Item("6").Enabled = False
                            objForm.Items.Item("4").Enabled = False
                        End If

                End Select
            End If
        Catch ex As Exception

        End Try

    End Sub

#Region "FindMode and Add Mode"
    Public Sub FindMode()
        objForm.Items.Item("6").Enabled = True
        objForm.ActiveItem = 6
    End Sub

    Public Sub AddMode()
        objForm.Close()
        loadscreen()
    End Sub

#End Region

#Region "G/L cfl"
    Public Sub GlCFL(Number)
        Try
            objCFL = Number
            objDT = objCFL.SelectedObjects
            objForm.Items.Item("9").Specific.Value = objDT.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Choose From List Conditions"
    Public Sub AccountCodeSelection()
        Dim Objcfl As SAPbouiCOM.ChooseFromList
        Dim objChooseCollection As SAPbouiCOM.ChooseFromListCollection
        Dim objConditions As SAPbouiCOM.Conditions
        Dim objcondition As SAPbouiCOM.Condition

        objChooseCollection = objForm.ChooseFromLists
        Objcfl = objChooseCollection.Item("CFL_2")
        objConditions = Objcfl.GetConditions()
        objcondition = objConditions.Add()
        objcondition.Alias = "Postable"
        objcondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
        objcondition.CondVal = "Y"
        Objcfl.SetConditions(objConditions)
    End Sub

#End Region

#Region "Validation"
    Public Function FormValidation() As Boolean
        If objForm.Items.Item("6").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Code is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("4").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Name is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("9").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("G/L Account No is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf (objAddOn.objpaycode.ValidationForExistance("select Code from [@AIS_DDC] where Name='" & objForm.Items.Item("4").Specific.string & "'")) = True Then
            objAddOn.objApplication.SetStatusBarMessage("Already Exists Name in the following Table ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

End Class
