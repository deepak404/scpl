﻿Public Class ClsDefineReimbursement
    Public Const formtype = "DefineReimbursement"
    Dim objForm As SAPbouiCOM.Form
    Dim objCFL As SAPbouiCOM.ChooseFromListEvent
    Dim objDT As SAPbouiCOM.DataTable
    Dim objComboBox As SAPbouiCOM.ComboBox
    Dim objItem As SAPbouiCOM.Item

    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("DefineReimbursement.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        '    objForm.ActiveItem = 4
        objForm.Items.Item("4").Click()

        objForm.EnableMenu("1281", True)
        objForm.DataBrowser.BrowseBy = "21"
        objForm.AutoManaged = True

        objItem = objForm.Items.Item("4")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        objItem = objForm.Items.Item("6")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        For INTI As Integer = 2 To 3
            AccountCodeSelection("CFL_" & INTI)
        Next
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" Then
                            If objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                                If FormValidation() = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                            End If
                        End If
                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.ItemUID = "8" Then
                            EglCFL(pVal)
                        ElseIf pVal.ItemUID = "10" Then
                            PglCFL(pVal)
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "12" Then
                            objComboBox = objForm.Items.Item("12").Specific
                            Type()
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            objForm.Items.Item("6").Enabled = False
                            objForm.Items.Item("4").Enabled = False
                        End If
                End Select
            End If
        Catch ex As Exception

        End Try

    End Sub

#Region "Choose From List Conditions"
    Public Sub AccountCodeSelection(ByVal CFL As String)
        Dim Objcfl As SAPbouiCOM.ChooseFromList
        Dim objChooseCollection As SAPbouiCOM.ChooseFromListCollection
        Dim objConditions As SAPbouiCOM.Conditions
        Dim objcondition As SAPbouiCOM.Condition

        objChooseCollection = objForm.ChooseFromLists
        Objcfl = objChooseCollection.Item("" & CFL & "")
        objConditions = Objcfl.GetConditions()
        objcondition = objConditions.Add()
        objcondition.Alias = "Postable"
        objcondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
        objcondition.CondVal = "Y"
        Objcfl.SetConditions(objConditions)
    End Sub

#End Region

#Region "G/L CFL"
    Public Sub EglCFL(Number)
        Try
            objCFL = Number
            objDT = objCFL.SelectedObjects
            objForm.Items.Item("8").Specific.Value = objDT.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub
    Public Sub PglCFL(Number)
        Try
            objCFL = Number
            objDT = objCFL.SelectedObjects
            objForm.Items.Item("10").Specific.Value = objDT.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Type"
    Public Sub Type()
        If objComboBox.ValidValues.Count > 0 Then
            Dim IntJ As Integer
            For IntJ = objComboBox.ValidValues.Count - 1 To 0 Step -1
                objComboBox.ValidValues.Remove(IntJ, SAPbouiCOM.BoSearchKey.psk_Index)
            Next
        End If
        Dim StrSQL3 As String
        Dim objRS3 As SAPbobsCOM.Recordset
        StrSQL3 = "select Code from [@AIS_DDC] "
        objRS3 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS3.DoQuery(StrSQL3)
        If objRS3.RecordCount > 0 Then
            Dim i As Integer
            For i = 1 To objRS3.RecordCount
                objComboBox.ValidValues.Add(i, objRS3.Fields.Item("Code").Value)
                objRS3.MoveNext()
            Next i
        End If
    End Sub
#End Region

#Region "Validation"
    Public Function FormValidation() As Boolean
        If objForm.Items.Item("4").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Code is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("6").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Name is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("8").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Expenditure G/L Account is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("10").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Payable G/L Account is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("12").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Type is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("14").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Rate is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("16").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Limit is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region
End Class
