﻿Public Class ClsDefineProfessionalTaxSlab
    Public Const formtype = "DefineProfessionalTaxSlab"
    Dim objForm As SAPbouiCOM.Form
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim objComboBox As SAPbouiCOM.ComboBox
    Dim objItem As SAPbouiCOM.Item
    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("DefineProfessionalTaxSlab.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.Items.Item("9").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_DPTS")
        objMatrix = objForm.Items.Item("7").Specific
        objForm.DataBrowser.BrowseBy = "10"
        objForm.EnableMenu("1281", True)
        objForm.AutoManaged = True
        objItem = objForm.Items.Item("4")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        objItem = objForm.Items.Item("6")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        objItem = objForm.Items.Item("9")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        State()
    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        If pVal.ItemUID = "7" And pVal.ColUID = "V_1" Then
                            If objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                                If FromTo(pVal.Row) = False Then
                                    BubbleEvent = False
                                End If
                            End If
                        End If
                        If pVal.ItemUID = "7" And pVal.ColUID = "V_0" Then
                            If objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                                If ValidateAmount(pVal) = False Then
                                    BubbleEvent = False
                                End If
                            End If
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_CLICK

                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" Then
                            If objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                                If FormValidation() = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                            End If
                        End If

                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" Then
                            If objMatrix.Columns.Item("V_2").Cells.Item(objMatrix.RowCount).Specific.Value = "" Then
                                objMatrix.DeleteRow(objMatrix.RowCount)
                            End If
                            'ElseIf pVal.ItemUID = "4" Then
                            '    State()
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                            objForm.Items.Item("9").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_DPTS")
                        ElseIf pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            objForm.Items.Item("6").Enabled = False
                            objForm.Items.Item("4").Enabled = False
                            objForm.Items.Item("9").Enabled = False
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        If pVal.ItemUID = "6" Then
                            If objMatrix.RowCount = 0 Then
                                objMatrix.AddRow()
                                objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.RowCount
                            End If
                        End If
                        If pVal.ItemUID = "7" And pVal.ColUID = "V_1" Then
                            If objMatrix.Columns.Item("V_2").Cells.Item(objMatrix.VisualRowCount).Specific.Value <> "" Then
                                objForm.DataSources.DBDataSources.Item("@AIS_PTS1").Clear()
                                objMatrix.AddRow()
                                objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.VisualRowCount).Specific.value = objMatrix.VisualRowCount
                            End If
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                        objComboBox = objForm.Items.Item("4").Specific
                        Dim objRS As SAPbobsCOM.Recordset
                        Dim strSQL As String
                        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        strSQL = "select * from [@AIS_DPTS] T0 join [@AIS_PTS1] T1 on T0.Code=T1.Code where T0.U_state='" & objComboBox.Selected.Value & "'"
                        objRS.DoQuery(strSQL)
                        Try
                            If objRS.RecordCount > 0 Then
                                objForm.Freeze(True)
                                objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                objForm.Items.Item("9").Enabled = True
                                objForm.Items.Item("9").Specific.value = objRS.Fields.Item("Code").Value
                                objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                objForm.DataSources.DBDataSources.Item("@AIS_PTS1").Clear()
                                objMatrix.AddRow()
                                objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
                                objForm.Freeze(False)
                            End If
                        Catch ex As Exception
                            objForm.Freeze(False)
                        End Try
                End Select
            End If
        Catch ex As Exception

        End Try

    End Sub
#Region "State"
    Public Sub State()
        Dim StrSQL As String
        Dim objRS As SAPbobsCOM.Recordset
        Dim intLoop As Integer
        objComboBox = objForm.Items.Item("4").Specific
        StrSQL = "select Code,Name from OCST where Country='IN'"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(StrSQL)
        objRS.MoveFirst()
        For intLoop = 1 To objRS.RecordCount
            Try
                objComboBox.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
                objRS.MoveNext()
            Catch ex As Exception
            End Try
        Next intLoop
    End Sub
#End Region

#Region "FindMode"
    Public Sub FindMode()
        objForm.Items.Item("9").Enabled = True
        objForm.ActiveItem = 9
    End Sub

#End Region

#Region "From and To Amount Range"
    Public Function FromTo(ByVal RowNo As Integer) As Boolean
        Dim StrSQL1 As String
        Dim objRS1 As SAPbobsCOM.Recordset
        Dim intResult As Integer
        objComboBox = objForm.Items.Item("4").Specific
        StrSQL1 = "select 1 from [@AIS_DPTS] T0 join [@AIS_PTS1] T1 on T0.Code=T1.Code where T1.U_frmamt=" & objMatrix.Columns.Item("V_2").Cells.Item(RowNo).Specific.value & " and T1.U_toamt=" & objMatrix.Columns.Item("V_1").Cells.Item(RowNo).Specific.value & " and T0.U_state='" & objComboBox.Selected.Value & "'"
        objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS1.DoQuery(StrSQL1)
        intResult = objRS1.Fields.Item(0).Value
        If intResult = 1 Then
            objAddOn.objApplication.SetStatusBarMessage("Both From and To amount together Already exist", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objMatrix.Columns.Item("V_2").Cells.Item(RowNo).Specific.value > objMatrix.Columns.Item("V_1").Cells.Item(RowNo).Specific.value Then
            objAddOn.objApplication.SetStatusBarMessage("From Amount is greater than To amount ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Validation"
    Public Function FormValidation() As Boolean
        If objForm.Items.Item("9").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Code is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("4").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("state is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("6").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Max amount is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objMatrix.VisualRowCount > 0 Then
            For IntI As Integer = 1 To objMatrix.VisualRowCount - 1
                If objMatrix.Columns.Item("V_2").Cells.Item(IntI).Specific.Value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("From amount is missing : Line No : " & IntI, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("V_1").Cells.Item(IntI).Specific.Value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("To amount is missing : Line No : " & IntI, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("V_0").Cells.Item(IntI).Specific.Value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Professional Tax amount is missing : Line No : " & IntI, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            Next
        Else
            objAddOn.objApplication.SetStatusBarMessage("Must Enter atleast One record im matrix", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If


        Return True
    End Function
#End Region

#Region "Validate Amount"
    Public Function ValidateAmount(ByRef pVal As SAPbouiCOM.ItemEvent) As Boolean
        If pVal.ItemUID = "7" And pVal.ColUID = "V_0" Then
            'MsgBox(objMatrix.Columns.Item("V_0").Cells.Item(pVal.Row).Specific.value)
            'MsgBox(objForm.Items.Item("6").Specific.value)
            If (objMatrix.Columns.Item("V_0").Cells.Item(pVal.Row).Specific.value < objForm.Items.Item("6").Specific.value) Then
            Else
                objAddOn.objApplication.SetStatusBarMessage("The professional tax Amount should be less than Maximum Amount", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
        End If
        Return True
    End Function
#End Region

#Region "Add Mode Function"
    Public Sub AddModeFunction()
        Try
            objForm.Items.Item("9").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_DPTS")
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Add Mode Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
#End Region

End Class
