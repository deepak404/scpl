Public Class ClsLeaveCode
    Public Const formtype = "LeaveCode"
    Public objForm As SAPbouiCOM.Form
    Public objItem As SAPbouiCOM.Item
    Public objOb As SAPbouiCOM.OptionBtn
    Public objOb1 As SAPbouiCOM.OptionBtn

    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("Leavecode.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.Items.Item("3").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
        objForm.PaneLevel = 1
        GroupWith()
        If objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
            objForm.Items.Item("15").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            'objForm.Items.Item("22").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
        End If
        objForm.Items.Item("50").Visible = True
        objForm.Items.Item("50").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_Dlcode")
        objForm.ActiveItem = "6"
        objForm.Items.Item("50").Visible = False
        objForm.DataBrowser.BrowseBy = "50"
        '   objAddOn.objApplication.Menus.Item("1281").Enabled = True

    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "1" And (objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            If Validate() = False Then
                                BubbleEvent = False
                            End If
                            'If Validate1() = False Then
                            '    objAddOn.objApplication.SetStatusBarMessage("Please Enter New Code", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                            '    BubbleEvent = False
                            'End If

                        End If

                End Select
            Else
                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        If pVal.ItemUID = "6" Then
                            If objForm.Items.Item("6").Specific.string = "" Then
                            Else
                                'objForm.Items.Item("6").Enabled = False
                            End If

                        End If
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.ItemUID = "4" Then
                            objForm.PaneLevel = 2
                            GroupWith1()
                        End If
                        If pVal.ItemUID = "3" Then
                            objForm.PaneLevel = 1
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.ItemUID = "29" Then
                            Payable_Gl(pVal)
                        End If
                        If pVal.ItemUID = "31" Then
                            Exp_Gl(pVal)
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.ActionSuccess = True Then
                            objForm.Items.Item("6").Enabled = True
                            objForm.Items.Item("50").Visible = True
                            objForm.Items.Item("50").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_Dlcode")
                            If objForm.PaneLevel = 1 Then
                                objForm.ActiveItem = "6"
                                objForm.Items.Item("50").Visible = False
                            End If
                            If objForm.PaneLevel = 2 Then
                                objForm.ActiveItem = "35"
                                objForm.Items.Item("50").Visible = False
                            End If
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_CLICK

                        If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            FindMode()
                        End If
                End Select
            End If
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.Message)
        End Try
    End Sub
#Region "Payable G/l Account"
    Public Sub Payable_Gl(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("29").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region "Exp G/l Account"
    Public Sub Exp_Gl(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("31").Specific.Value = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception

        End Try
    End Sub
#End Region
    Public Sub GroupWith()
        objOb = objForm.Items.Item("15").Specific
        objOb.GroupWith("16")

    End Sub
    Public Sub GroupWith1()
        objOb = objForm.Items.Item("22").Specific
        objOb.GroupWith("23")
        objOb1 = objForm.Items.Item("17").Specific
        objOb1.GroupWith("23")
        objOb = objForm.Items.Item("22").Specific
        objOb.GroupWith("17")

    End Sub
#Region "Validate"
    Public Function Validate()
        If objForm.Items.Item("6").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter the Leave Code", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("9").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter the Name", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("11").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter the Max Period", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("13").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter the Available", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("35").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter the Leave Lapse After", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("25").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter the Min Enchaseble Days", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("27").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter the Max Enchaseble Days", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("29").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter the Payable G/l Acc", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("31").Specific.string = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter the Expanditur G/l Acc", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("25").Specific.string > objForm.Items.Item("27").Specific.string Then
            objAddOn.objApplication.SetStatusBarMessage("Please check Max & Min Enchasebale", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
            'ElseIf objForm.Items.Item("11").Specific.value < objForm.Items.Item("13").Specific.value Then
            '    objAddOn.objApplication.SetStatusBarMessage("Please give should be minimum of Maximum Days", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '    Return False
        End If
        Return True
    End Function
#End Region
    '#Region "code Validate"
    '    Public Function Validate1()

    '        Return True
    '    End Function
    '#End Region
    Public Sub FindMode()
        objForm.Items.Item("6").Enabled = True
    End Sub
    Public Sub AddMode()
        objForm.Items.Item("6").Enabled = True
        objForm.Items.Item("50").Visible = True
        objForm.Items.Item("50").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_Dlcode")
        If objForm.PaneLevel = 1 Then
            objForm.ActiveItem = "6"
            objForm.Items.Item("50").Visible = False
        End If
        If objForm.PaneLevel = 2 Then
            objForm.ActiveItem = "35"
            objForm.Items.Item("50").Visible = False
        End If
    End Sub

#Region "Checking Remove the Code "
    Public Sub RemoveFunction(ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                If objAddOn.objGenFunc.RemoveMenu("select DocEntry from [@AIS_inouttime] where U_attd ='" & objForm.Items.Item("6").Specific.String & "'") = True Then
                    BubbleEvent = False
                    Exit Sub
                    objAddOn.objApplication.StatusBar.SetText("We Can't Remove this Code", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                End If
            End If
           
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Remove Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
#End Region

End Class
