Public Class ClsConsolidatedMonth

#Region "Declaration"
    Public Const formtype = "ConsolidatedMonth"
    Dim objform As SAPbouiCOM.Form
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim objEmpID, objEmpName As SAPbouiCOM.StaticText
#End Region

#Region "LoadScreen"
    Public Sub LoadScreen(ByVal EmpID As Integer, ByVal EmpName As String, ByVal Year As Integer)
        objform = objAddOn.objUIXml.LoadScreenXML("ConsolidatedPage.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objEmpID = objform.Items.Item("4").Specific
        objEmpName = objform.Items.Item("6").Specific
        objform.Freeze(True)
        objEmpID.Caption = EmpID
        objEmpName.Caption = EmpName
        LoadMatrix(CFormUID, EmpID, Year)
        objform.Freeze(False)
    End Sub
#End Region

#Region "Load Matrix"
    Public Sub LoadMatrix(ByVal CFormUID As String, ByVal EmpID As Integer, ByVal Year As Integer)
        Try
            Dim StrSQL As String
            Dim objdatatable As SAPbouiCOM.DataTable
            Dim objRS As SAPbobsCOM.Recordset
            'objform = objAddOn.objApplication.Forms.Item(CFormUID)
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            StrSQL = "declare @code int ='" & EmpID & "'"
            StrSQL += vbCrLf + "declare @year int ='" & Year & "'"
            StrSQL += vbCrLf + ";with cte as("
            StrSQL += vbCrLf + "select Distinct a.Name,a.Code,c.U_target,c.U_achieve,c.U_percent,isnull(c.U_empid,@code)U_empid,isnull(b.U_year,@year)year from [@AIS_MONTH] a "
            StrSQL += vbCrLf + "left join [@AIS_VPAY] b on a.Name=b.U_month"
            StrSQL += vbCrLf + "left join [@AIS_VPAYM] c on c.Code=b.Code "
            StrSQL += vbCrLf + ")"
            StrSQL += vbCrLf + "select Name,U_target,U_achieve,U_percent from cte "
            StrSQL += vbCrLf + "where cte.U_empid=@code and cte.year=@year order by cte.Code ASC"
            objdatatable = objform.DataSources.DataTables.Item("DT_0")
            objdatatable.ExecuteQuery(StrSQL)
            objMatrix = objform.Items.Item("9").Specific
            objMatrix.LoadFromDataSource()
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub
#End Region

End Class
