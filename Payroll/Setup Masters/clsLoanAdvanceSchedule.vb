﻿Public Class clsLoanAdvanceSchedule
    Public Const Formtype = "MNU_LNAD"
    Dim objForm As SAPbouiCOM.Form
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim strSQL As String
    Dim objRs As SAPbobsCOM.Recordset
    Dim objbutton As SAPbouiCOM.Button

    Public Sub Loadscreen(ByVal ID As String, ByVal Name As String, ByVal Type As String, ByVal FromDate As String, ByVal ToDate As String, ByVal Amount As Integer)
        objForm = objAddOn.objUIXml.LoadScreenXML("LoanAdvanceDetails.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, Formtype)
        strSQL = "select * from [@AIS_OLNAD] where U_empid='" & ID & "' and U_empnam='" & Name & "'"
        objRs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRs.DoQuery(strSQL)
        If objRs.RecordCount > 0 Then
            objbutton = objForm.Items.Item("15").Specific
            objbutton.Caption = Amount
            objForm.Items.Item("9").Specific.value = Type
            objForm.Items.Item("11").Specific.value = FromDate
            objForm.Items.Item("13").Specific.value = ToDate
            objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            objForm.Items.Item("16").Specific.value = objRs.Fields.Item("DocEntry").Value
            objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            objMatrix = objForm.Items.Item("14").Specific
            objMatrix.AddRow()
            objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
        Else
            objForm.Items.Item("16").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_OLNAD")
            objForm.Items.Item("5").Specific.value = ID
            objForm.Items.Item("7").Specific.value = Name
            objForm.Items.Item("9").Specific.value = Type
            objForm.Items.Item("11").Specific.value = FromDate
            objForm.Items.Item("13").Specific.value = ToDate
            objbutton = objForm.Items.Item("15").Specific
            objbutton.Caption = Amount
            objMatrix = objForm.Items.Item("14").Specific
            objMatrix.AddRow()
            objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount

        End If

    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    objForm = objAddOn.objApplication.Forms.Item(FormUID)
                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        If Validation(FormUID, pVal.Row) = False Then
                            BubbleEvent = False
                            Exit Sub
                        End If
                    ElseIf pVal.ItemUID = "1" And (objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                        DeletedEmptyRow(FormUID, pVal.Row)
                    End If
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    If pVal.ItemUID = "5" Then
                        ChooseItem(FormUID, pVal)
                    End If
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    objMatrix = objForm.Items.Item("12").Specific
                    If pVal.ItemUID = "12" And pVal.ColUID = "V_1" Or pVal.ColUID = "V_0" Then
                        Addrow(FormUID, pVal.Row)
                    End If
            End Select
        End If
    End Sub

#Region "Choose From List"
    Public Sub ChooseItem(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        Try
            Dim objcfl As SAPbouiCOM.ChooseFromListEvent
            Dim objdt As SAPbouiCOM.DataTable
            objcfl = pval
            objdt = objcfl.SelectedObjects
            If objdt Is Nothing Then
            Else
                Select Case pval.ItemUID
                    Case "5"
                        objForm.Items.Item("7").Specific.string = objdt.GetValue("firstName", 0) + " " + objdt.GetValue("lastName", 0)
                        objForm.Items.Item("5").Specific.string = objdt.GetValue("empID", 0)
                End Select

            End If
        Catch ex As Exception

        End Try

    End Sub
#End Region

#Region "Validation"
    Public Function Validation(ByVal FormUID As String, ByVal RowNo As Integer)
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        If objForm.Items.Item("5").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select Employee ID", False)
            Return False
        ElseIf objForm.Items.Item("9").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter Benefit Code", False)
            Return False
        ElseIf objForm.Items.Item("12").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter an Amount", False)
            Return False
        ElseIf objMatrix.RowCount > 0 Then
            If objMatrix.Columns.Item("V_1").Cells.Item(1).Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Enter Due Date", False)
                Return False
            ElseIf objMatrix.Columns.Item("V_0").Cells.Item(1).Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Enter An Amount", False)
                Return False
            End If
        End If
        Return True
    End Function
#End Region

#Region "Deleted Empty Row"
    Public Sub DeletedEmptyRow(ByVal FormUID As String, ByVal RowNo As Integer)
        objMatrix = objForm.Items.Item("12").Specific
        If objMatrix.Columns.Item("V_1").Cells.Item(objMatrix.VisualRowCount).Specific.value = "" Or objMatrix.Columns.Item("V_0").Cells.Item(objMatrix.VisualRowCount).Specific.value = "" Then
            objMatrix.DeleteRow(objMatrix.VisualRowCount)
        End If
    End Sub
#End Region

#Region "Add Row"
    Public Sub Addrow(ByVal FormUID As String, ByVal RowNo As Integer)
        objMatrix = objForm.Items.Item("12").Specific
        If objMatrix.Columns.Item("V_1").Cells.Item(RowNo).Specific.value = "" And objMatrix.Columns.Item("V_0").Cells.Item(RowNo).Specific.value = "" Then
        Else
            objMatrix.AddRow()
            objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
        End If
    End Sub
#End Region

#Region "Menu Event"
    Public Sub FindMode()
        objForm.Items.Item("7").Enabled = True
        objForm.ActiveItem = "5"
    End Sub
    Public Sub AddMode()
        objForm.ActiveItem = "5"
        objForm.Items.Item("7").Enabled = False
    End Sub
#End Region

End Class
