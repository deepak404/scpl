﻿Public Class clsPerquisiteCategory
    Dim objForm As SAPbouiCOM.Form
    Dim objMatrix As SAPbouiCOM.Matrix

    Public Sub LoadScreen()
        Dim strTableName As String
        Dim intLoop As Integer
        For intLoop = 0 To objAddOn.objApplication.Menus.Item("51200").SubMenus.Count - 1
            strTableName = objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).String
            strTableName = strTableName.Remove(strTableName.IndexOf("-"))
            If Trim(strTableName) = "AIS_PRQSCTY" Then
                objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).Activate()
                PerquisiteCat = objAddOn.objApplication.Forms.ActiveForm.TypeEx
                Exit For
            End If
        Next
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)

        Select Case pVal.EventType
            Case SAPbouiCOM.BoEventTypes.et_CLICK

                Try
                    Select Case pVal.ItemUID
                        Case "1"
                            'Add,Update Event
                            objForm = objAddOn.objApplication.Forms.ActiveForm
                            If pVal.BeforeAction = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                                If Me.ValidateAll() = False Then
                                    System.Media.SystemSounds.Asterisk.Play()
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                            End If
                    End Select
                Catch ex As Exception
                    objAddOn.objApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    BubbleEvent = False
                Finally
                End Try
        End Select
    End Sub

   
    Function ValidateAll() As Boolean
        Try

            Dim strserialno = ""
            Dim strcodecc = ""
            Dim strnamenn = ""
            objMatrix = objForm.Items.Item("3").Specific
            For i As Integer = 1 To objMatrix.VisualRowCount

                If objMatrix.Columns.Item("Code").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Line No." & i & " Code Should Not Be Left Empty")
                    objMatrix.Columns.Item("Code").Cells.Item(i).Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                    Return False

                End If

                Dim strcode = objMatrix.Columns.Item("Code").Cells.Item(i).Specific.value.ToString.Trim
                If strcodecc.Contains("/" + strcode + "/") = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Line No." & i & " Code. Should Not be Duplicate")
                    objMatrix.Columns.Item("Code").Cells.Item(i).Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                    Return False
                End If
                strcodecc = strcodecc + "/" + strcode + "/"

                If objMatrix.Columns.Item("Name").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Line No." & i & " Name Should Not Be Left Empty")
                    objMatrix.Columns.Item("Name").Cells.Item(i).Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                    Return False
                End If

                Dim strname = objMatrix.Columns.Item("Name").Cells.Item(i).Specific.value.ToString.Trim
                If strname.Contains("/" + strname + "/") = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Line No." & i & " Name. Should Not be Duplicate")
                    objMatrix.Columns.Item("Name").Cells.Item(i).Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                    Return False
                End If
                strnamenn = strnamenn + "/" + strname + "/"

                If objMatrix.Columns.Item("U_sno").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Line No." & i & " Serial No. Should Not Be Left Empty")
                    objMatrix.Columns.Item("U_sno").Cells.Item(i).Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                    Return False
                End If

                Dim strsno = objMatrix.Columns.Item("U_sno").Cells.Item(i).Specific.value.ToString.Trim
                If strserialno.Contains("/" + strsno + "/") = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Line No." & i & " Serial No. Should Not be Duplicate")
                    objMatrix.Columns.Item("U_sno").Cells.Item(i).Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                    Return False
                End If
                strserialno = strserialno + "/" + strsno + "/"

            Next

            ValidateAll = True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try
    End Function
End Class
