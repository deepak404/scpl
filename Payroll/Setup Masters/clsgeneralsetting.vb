﻿Public Class clsgeneralsetting
    Dim objForm As SAPbouiCOM.Form
    Dim objRD As SAPbouiCOM.OptionBtn
    Public Const formtype = "MNU_GS"
    Dim objRS As SAPbobsCOM.Recordset
    Dim objEdit As SAPbouiCOM.EditText
    Dim ShowFolderBrowserThread As Threading.Thread
    Dim FolderBrowser1 As OpenFileDialog
    Dim str As String
    Public Sub loadscreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("generalsetting.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        'objAddOn.objApplication.Menus.Item("1283").Enabled = False
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        str = "select 1 from [@AIS_PDSGS]"
        objRS.DoQuery(str)
        If objRS.RecordCount <> 0 Then
            objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            objForm.Items.Item("43").Specific.value = 1
            objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
        Else
            objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            objForm.Items.Item("43").Visible = True
            objForm.Items.Item("43").Specific.value = 1
        End If
    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = False Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.ActionSuccess = True Then
                        objForm.Close()
                    End If
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "46" Then
                        BrowseFolderDialog()
                    End If
            End Select
        End If
    End Sub

#Region "General Setting Taking from DB"

    Public Sub GeneralSettings()
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        str = "select CASE when U_iweldy ='1' then 'Y' else 'N' end [WeekEnd] " & _
            " ,CASE when U_ibtwhy ='1' then 'Y' else 'N' end [Break Time] " & _
            " ,CASE when U_rosn ='1' then 'Y' else 'N' end [Round Off] " & _
            " ,CASE when U_ecud ='1' then 'Y' else 'N' end [Emp Code] " & _
            " ,CASE when U_scbfy ='1' then 'Y' else 'N' end [Basis Formula] " & _
            ",CASE when U_usay ='1' then 'Y' else 'N' end [Segment] " & _
            " ,CASE when U_atdscy ='1' then 'Y' else 'N' end [TDS] " & _
            " ,CASE when U_zaspy ='1' then 'Y' else 'N' end [Zero Amount] from [@AIS_PDSGS] "

        objRS.DoQuery(str)
        WeekEnd = objRS.Fields.Item("WeekEnd").Value
        BreakTime = objRS.Fields.Item("Break Time").Value
        RoundOff = objRS.Fields.Item("Round Off").Value
        EmpCode = objRS.Fields.Item("Emp Code").Value
        BasisFormula = objRS.Fields.Item("Basis Formula").Value
        Segment = objRS.Fields.Item("Segment").Value
        TDSSetting = objRS.Fields.Item("TDS").Value
        ZeroAmount = objRS.Fields.Item("Zero Amount").Value

    End Sub
#End Region

#Region "OpenFile"

    Public Sub BrowseFolderDialog()
        Try
            ShowFolderBrowserThread = New System.Threading.Thread(AddressOf ShowFolderBrowser)
            If ShowFolderBrowserThread.ThreadState = Threading.ThreadState.Unstarted Then
                ShowFolderBrowserThread.SetApartmentState(Threading.ApartmentState.STA)
                ShowFolderBrowserThread.Start()
            Else
                If ShowFolderBrowserThread.ThreadState = Threading.ThreadState.Stopped Then
                    ShowFolderBrowserThread.Start()
                    ShowFolderBrowserThread.Join()
                End If
            End If
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.Message)
        End Try
    End Sub

    Public Sub ShowFolderBrowser()
        ShowFolderBrowserThread = Nothing
        Dim FolderBrowser1 As New OpenFileDialog
        Dim shortfilename As String = ""
        Dim P() As Process
        Dim i As Integer
        Dim MyWindow As ClsWindowWrapper
        P = Process.GetProcessesByName("SAP Business One")
        If P.Length <> 0 Then
            For i = 0 To P.Length - 1
                MyWindow = New ClsWindowWrapper(P(i).MainWindowHandle)
                If Trim(objAddOn.objCompany.AttachMentPath) <> "" Then
                    FolderBrowser1.InitialDirectory = objAddOn.objCompany.AttachMentPath
                    'FolderBrowser1.Filter = "xlsx (*.xlsx)|*.xlsx|All files (*.*)|*.*"
                    FolderBrowser1.FilterIndex = 2
                    FolderBrowser1.Multiselect = False
                    If FolderBrowser1.CheckPathExists() Then
                        If FolderBrowser1.ShowDialog(MyWindow) = DialogResult.OK Then
                            objEdit = objForm.Items.Item("45").Specific
                            objEdit.String = FolderBrowser1.FileName
                            '  StrPath = objForm.Items.Item("2").Specific.string
                        Else
                            System.Windows.Forms.Application.ExitThread()
                        End If
                    Else
                        objAddOn.objApplication.MessageBox("There is no attachment folder")
                    End If
                Else
                    objAddOn.objApplication.MessageBox("Specify the Attachment Folder Path in Administation Module")
                End If
            Next
        End If

    End Sub

#End Region

End Class
