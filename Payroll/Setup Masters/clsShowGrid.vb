﻿Public Class clsShowGrid
    Public Const formtype = "ShowAttendance"
    Dim objForm As SAPbouiCOM.Form
    Dim SDate, EDate, a As String
    Dim Ename, CaptionName, Eid, momthno, monthnm As String
    Dim oGrid As SAPbouiCOM.Grid
    Dim rowsGrid, yearval, i, j As Integer
    Public Sub LoadScreen(ByVal month As String, ByVal intj As String, ByVal Year As String)
        objForm = objAddOn.objUIXml.LoadScreenXML("AttendanceRegister.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)

        If pVal.BeforeAction = True Then
            objForm = objAddOn.objApplication.Forms.Item(FormUID)
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
                    oGrid = objForm.Items.Item("3").Specific
                    If pVal.ItemUID = "3" And pVal.ColUID <> "EmpID" Then
                        Ename = oGrid.DataTable.GetValue("Name", pVal.Row)
                        Eid = oGrid.DataTable.GetValue("EmpID", pVal.Row)
                        CaptionName = oGrid.Columns.Item(pVal.ColUID).TitleObject.Caption.ToString
                        rowsGrid = pVal.Row
                        objAddOn.objouttime.LoadScreen(Ename, CaptionName, Eid)
                        BubbleEvent = False
                        Exit Sub
                    ElseIf pVal.ItemUID = "3" And pVal.ColUID = "EmpID" Then
                        rowsGrid = pVal.Row
                    End If
            End Select
        End If

    End Sub

    Public Sub UpdateShow(ByVal value As String)
        oGrid = objForm.Items.Item("3").Specific
        oGrid.DataTable.SetValue("Name", rowsGrid, value)
    End Sub
    Public Sub UpdateS(ByVal value2 As String)
        oGrid.DataTable.SetValue(CaptionName, rowsGrid, value2)
    End Sub


#Region "Get Last Date Function"
    Public Function get_last_date(ByVal year As Integer, ByVal mntname As String)
        Dim lastdate As Integer = DatePart(DateInterval.Day, DateSerial(year, Month(CDate(mntname + "1,1990")) + 1, 0))
        Return lastdate
    End Function
#End Region

#Region "Load Grid"
    Public Sub LoadGrid(ByVal SName As String, ByVal EName As String, ByVal SDept As String, ByVal EDept As String, ByVal SBranch As String, ByVal EBranch As String, ByVal month As String, ByVal intj As String, ByVal Year As String)
        Dim gridEditCol As SAPbouiCOM.EditTextColumn
        Dim strSQL As String
        Dim Rs As SAPbobsCOM.Recordset
        Dim c As String
        momthno = intj
        yearval = Year
        monthnm = month
        a = CDate("01" + "/" + intj.ToString + "/" + Year.ToString)
        Dim b As String = get_last_date(Year, month)
        strSQL = "select DATEDIFF(dd,'" & CDate(a).ToString("yyyyMMdd") & "','" & CDate(b + "/" + intj.ToString + "/" + Year.ToString).ToString("yyyyMMdd") & "') +1'Day'"
        Rs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Rs.DoQuery(strSQL)
        c = CDate(Rs.Fields.Item("Day").Value.ToString + "/" + intj.ToString + "/" + Year.ToString)


        Dim i As Integer
        Dim Str As String
        oGrid = objForm.Items.Item("3").Specific
        oGrid.DataTable = objForm.DataSources.DataTables.Add("3")
        Str = "select empID as EmpID,firstName as Name,dept as Dept,branch as Branch"
        For i = Left(a, 2) To Rs.Fields.Item("Day").Value.ToString
            Str += vbCrLf + ",convert(varchar,'N') '" & i.ToString + "/" + intj.ToString + "/" + Year.ToString & "'"
        Next
        Str += vbCrLf + "  from OHEM where  1=1 "
        If SName.ToString <> "" Then
            Str += vbCrLf + " and firstName>='" & SName.ToString & "'"
        End If
        If EName.ToString <> "" Then
            Str += vbCrLf + " and firstName<='" & EName.ToString & "'"
        End If
        If SDept.ToString <> "" Then
            Str += vbCrLf + "and dept>='" & SDept.ToString & "'"
        End If
        If EDept.ToString <> "" Then
            Str += vbCrLf + "and dept<='" & EDept.ToString & "'"
        End If
        If SBranch.ToString <> "" Then
            Str += vbCrLf + "and branch>='" & SBranch.ToString & "'"
        End If
        If EBranch.ToString <> "" Then
            Str += vbCrLf + "and branch<='" & EBranch.ToString & "'"
        End If
        Try
            objForm.Freeze(True)
            oGrid.DataTable.ExecuteQuery(Str)
            LoadValue()
            gridEditCol = oGrid.Columns.Item(0)
            gridEditCol.LinkedObjectType = 171
            gridEditCol.Editable = False

            gridEditCol = oGrid.Columns.Item(2)
            gridEditCol.Visible = False

            gridEditCol = oGrid.Columns.Item(3)
            gridEditCol.Visible = False

            gridEditCol = oGrid.Columns.Item(1)
            gridEditCol.Editable = False

            For i = 4 To 35
                Try
                    gridEditCol = oGrid.Columns.Item(i)
                    gridEditCol.LinkedObjectType = "AIS_INOUTTIME"
                    gridEditCol.Editable = False
                Catch

                End Try

            Next
            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
        End Try


    End Sub
    Public Sub LoadValue()
        Try

            Dim strSQL As String
            Dim Rs, rs1 As SAPbobsCOM.Recordset
            ' Dim j As Integer
            Dim b As String = get_last_date(yearval, monthnm)
            For i = 0 To oGrid.Rows.Count - 1
                strSQL = "select DATEDIFF(dd,'" & CDate(a).ToString("yyyyMMdd") & "','" & CDate(b + "/" + momthno.ToString + "/" + yearval.ToString).ToString("yyyyMMdd") & "') +1'Day'"
                Rs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                Rs.DoQuery(strSQL)
                For j = 1 To Rs.Fields.Item("Day").Value.ToString
                    strSQL = "select U_attd from [@AIS_INOUTTIME] where U_eid='" & oGrid.DataTable.GetValue("EmpID", i) & " ' and CONVERT (varchar,U_Date,103)='" & CDate(j.ToString + "/" + momthno.ToString + "/" + yearval.ToString) & "'"
                    rs1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    rs1.DoQuery(strSQL)
                    If rs1.RecordCount > 0 Then
                        oGrid.DataTable.SetValue(j.ToString + "/" + momthno.ToString + "/" + yearval.ToString, i, rs1.Fields.Item(0).Value.ToString)
                    End If
                Next
            Next
        Catch ex As Exception
            MsgBox(j)
            MsgBox(ex.ToString)
        End Try
    End Sub
#End Region

End Class
