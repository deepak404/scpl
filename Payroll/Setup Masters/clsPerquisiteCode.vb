﻿Public Class clsPerquisiteCode
    Dim objRS As SAPbobsCOM.Recordset
    Dim objForm As SAPbouiCOM.Form
    Dim objMatrix As SAPbouiCOM.Matrix

    Public Sub LoadScreen()
        Dim strTableName As String
        Dim intLoop As Integer
        For intLoop = 0 To objAddOn.objApplication.Menus.Item("51200").SubMenus.Count - 1
            strTableName = objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).String
            strTableName = strTableName.Remove(strTableName.IndexOf("-"))
            If Trim(strTableName) = "AIS_PRQSCDE" Then
                objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).Activate()
                PerquisiteCod = objAddOn.objApplication.Forms.ActiveForm.TypeEx
                Exit For
            End If
        Next
        FMSDescription()
        FMSCategory()
    End Sub

    Public Sub FMSDescription()
        Dim queryid As Integer
        Dim objquery As SAPbobsCOM.UserQueries
        Dim str As String
        Dim lretcode As Long
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objquery = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserQueries)
        Try
            objRS.DoQuery("select * from OUQR where Qname ='PERQST_DSCR1'")
            If objRS.RecordCount = 0 Then
                objquery.Query = "select Name from [@AIS_PRQSCTY] where Code=$[@AIS_PRQSCDE.U_category.0]"
                objquery.QueryDescription = "PERQST_DSCR1"
                objquery.QueryCategory = "-1"
                lretcode = objquery.Add
                If lretcode <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                Else
                End If
            End If
            str = "select QueryID from CSHS where FormID='" & PerquisiteCod & "'and ItemID='3'and (ColID='U_category' or ColID= 'U_descrptn')"

            objRS.DoQuery(str)

            If objRS.RecordCount > 0 Then
                Exit Sub
            Else
                objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                objRS.DoQuery("select IntrnalKey from OUQR where Qname ='PERQST_DSCR1'")
                queryid = objRS.Fields.Item("IntrnalKey").Value
                Dim objFMS As SAPbobsCOM.FormattedSearches
                objFMS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oFormattedSearches)
                objFMS.FormID = PerquisiteCod
                objFMS.ColumnID = "U_descrptn"
                objFMS.ItemID = 3
                objFMS.Action = SAPbobsCOM.BoFormattedSearchActionEnum.bofsaQuery
                objFMS.QueryID = queryid
                objFMS.FieldID = "U_category"
                objFMS.Refresh = SAPbobsCOM.BoYesNoEnum.tYES
                lretcode = objFMS.Add
                If lretcode <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub FMSCategory()
        Dim queryid As Integer
        Dim lretcode As Long
        Dim str As String
        Dim objquery As SAPbobsCOM.UserQueries
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objquery = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserQueries)
        Try
            objRS.DoQuery("select * from OUQR where Qname ='PERQST_CTGY2'")
            If objRS.RecordCount = 0 Then
                objquery.Query = "select Code from [@AIS_PRQSCTY]"
                objquery.QueryDescription = "PERQST_CTGY2"
                objquery.QueryCategory = "-1"
                lretcode = objquery.Add
                If lretcode <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                Else
                End If
            End If
            str = "select QueryID from CSHS where FormID='" & PerquisiteCod & "'and ItemID='3'and (ColID='U_category')"

            objRS.DoQuery(Str)

            If objRS.RecordCount > 0 Then
                Exit Sub
            Else
                objRS.DoQuery("select IntrnalKey from OUQR where Qname ='PERQST_CTGY2'")
                queryid = objRS.Fields.Item("IntrnalKey").Value
                Dim objFMS As SAPbobsCOM.FormattedSearches
                objFMS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oFormattedSearches)
                objFMS.FormID = PerquisiteCod
                objFMS.ColumnID = "U_category"
                objFMS.ItemID = 3
                objFMS.Action = SAPbobsCOM.BoFormattedSearchActionEnum.bofsaQuery
                objFMS.QueryID = queryid
                lretcode = objFMS.Add
                If lretcode <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)

        Select Case pVal.EventType
            Case SAPbouiCOM.BoEventTypes.et_CLICK

                Try
                    Select Case pVal.ItemUID
                        Case "1"
                            'Add,Update Event
                            objForm = objAddOn.objApplication.Forms.ActiveForm
                            If pVal.BeforeAction = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                                If Me.ValidateAll() = False Then
                                    System.Media.SystemSounds.Asterisk.Play()
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                            End If
                    End Select
                Catch ex As Exception
                    objAddOn.objApplication.StatusBar.SetText("Click Event Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    BubbleEvent = False
                Finally
                End Try
        End Select
    End Sub


    Function ValidateAll() As Boolean
        Try

            Dim strserialno = ""
            Dim strcodecc = ""
            Dim strnamenn = ""
            objMatrix = objForm.Items.Item("3").Specific
            For i As Integer = 1 To objMatrix.VisualRowCount


                If objMatrix.Columns.Item("U_category").Cells.Item(i).Specific.value.ToString.Trim.Equals("") = True Then
                    objAddOn.objApplication.SetStatusBarMessage("Line No." & i & " Category. Should Not Be Left Empty")
                    objMatrix.Columns.Item("U_category").Cells.Item(i).Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                    Return False
                End If

            Next

            ValidateAll = True
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Validate Function Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
            ValidateAll = False
        Finally
        End Try
    End Function

End Class
