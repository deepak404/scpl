﻿Public Class clsEmpSalarySetup
    Public objForm As SAPbouiCOM.Form
    Public Const Formtype = "empsalsetup"
    Dim objButton As SAPbouiCOM.OptionBtn
    Dim objButton1 As SAPbouiCOM.OptionBtn
    Dim objCFLEvent As SAPbouiCOM.ChooseFromListEvent
    Dim objDataTable As SAPbouiCOM.DataTable
    Dim objCombo As SAPbouiCOM.ComboBox
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim objMatrix1 As SAPbouiCOM.Matrix
    Dim objMatrix2 As SAPbouiCOM.Matrix
    Dim objMatrix3 As SAPbouiCOM.Matrix
    Dim objMatrix4 As SAPbouiCOM.Matrix
    Dim objMatrix5 As SAPbouiCOM.Matrix
    Dim objMatrix6 As SAPbouiCOM.Matrix
    Dim objMatrix7 As SAPbouiCOM.Matrix
    Dim objMatrix8 As SAPbouiCOM.Matrix
    Dim objDecMatrix As SAPbouiCOM.Matrix

    Dim objRS As SAPbobsCOM.Recordset
    Dim ONewItem As SAPbouiCOM.Item
    Dim objFolder As SAPbouiCOM.Folder
    Dim strSQL As String
    Dim valofper, amtpaid, amttax As Double
    Dim intLoop, Month As Integer
    Dim objComboYear, objComboMonth As SAPbouiCOM.ComboBox

    Dim oRS As SAPbobsCOM.Recordset
    Dim Basic, HRA, DA, Total As Double
    Dim RentPaid, FiftyPer, HRARcd, RntPDTenPer, TransAllow, MedReimb, PF, PT, TotPerquisites, TotInvestment, TotDeduction, Metro, NonMetro As Double
    Dim InFromHousing, RelonHomeLoan, OtherExcemption, GrandTotalIncome, GrossTaxSalary, Minimum, TotalTDSAmount, OtherAllowances, OverTime As Double
    Dim PaidTDS, UpperLimit, LowerLimit, Percentage, FromAmount, ToAmount, PTAmount, Gross As Double
    Dim SMon, EMon, EYear, SYear, Count, IntNPMnth, IntI, Count80C As Integer
    Dim TDSSlabAmt, TDS As Double
    Dim EID, EndMonth As String
    Dim Amount As Double = 0
    Dim FinYear, TDSFYear, TDSTYear As String

#Region "Loadscreen"
    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("EmpSalarySetup.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, Formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.Items.Item("129").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_ESTP")
        objForm = objAddOn.objApplication.Forms.Item(objForm.UniqueID)
        objMatrix2 = objForm.Items.Item("32").Specific
        objMatrix2.Columns.Item("V_2").Editable = True
        objMatrix2.Columns.Item("V_1").Editable = True
        objMatrix3 = objForm.Items.Item("33").Specific
        objMatrix3.Columns.Item("V_10").Editable = True
        LoadscreenFUN()
        objAddOn.objgeneralsetting.GeneralSettings()
        'LoadMonthandYear()
        '  ReimbursementSelection()
    End Sub
#End Region

#Region "Itemevent"
    Public Sub Itemevent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            objForm = objAddOn.objApplication.Forms.Item(FormUID)
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If HeaderValidateFUN() = False Then
                            BubbleEvent = False
                            Exit Sub
                        End If
                        If pVal.ItemUID = "1" Then
                            objMatrix2 = objForm.Items.Item("32").Specific
                            For I As Integer = 1 To objMatrix2.RowCount
                                If objMatrix2.Columns.Item("V_4").Cells.Item(I).Specific.value < objMatrix2.Columns.Item("V_3").Cells.Item(I).Specific.value Then
                                    objAddOn.objApplication.SetStatusBarMessage("Reimbursement Limit is Exceeds - " & objMatrix2.Columns.Item("V_7").Cells.Item(I).Specific.string, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                    BubbleEvent = False
                                    Exit Sub
                                    Exit For
                                End If
                            Next
                        End If
                    End If
                    

                    'If pVal.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    '    objForm = objAddOn.objApplication.Forms.Item(FormUID)
                    '    If MatrixValidationFun() Then
                    '    Else
                    '        BubbleEvent = False
                    '    End If
                    'End If

                    'If pVal.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    '    objForm = objAddOn.objApplication.Forms.Item(FormUID)
                    '    If Matrix1ValidationFun() Then
                    '    Else
                    '        BubbleEvent = False
                    '    End If
                    'End If
                    'If pVal.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    '    objForm = objAddOn.objApplication.Forms.Item(FormUID)
                    '    If Matrix2ValidationFun() Then
                    '    Else
                    '        BubbleEvent = False
                    '    End If
                    'End If
                    'If pVal.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    '    objForm = objAddOn.objApplication.Forms.Item(FormUID)
                    '    If Matrix4ValidationFun() Then
                    '    Else
                    '        BubbleEvent = False
                    '    End If
                    'End If
                    'If pVal.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    '    objForm = objAddOn.objApplication.Forms.Item(FormUID)
                    '    If Matrix5ValidationFun() Then
                    '    Else
                    '        BubbleEvent = False
                    '    End If
                    'End If
                    'If pVal.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    '    objForm = objAddOn.objApplication.Forms.Item(FormUID)
                    '    If Matrix6ValidationFun() Then
                    '    Else
                    '        BubbleEvent = False
                    '    End If
                    'End If
                    'If pVal.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    '    objForm = objAddOn.objApplication.Forms.Item(FormUID)
                    '    If Matrix7ValidationFun() Then
                    '    Else
                    '        BubbleEvent = False
                    '    End If
                    'End If
                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        DeleteEmptyRow(FormUID, pVal.Row)
                    End If

                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    If pVal.ItemUID = "30" And pVal.ColUID = "V_0" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        objMatrix = objForm.Items.Item("30").Specific
                        If objMatrix.Columns.Item("V_0").Cells.Item(objMatrix.VisualRowCount).Specific.String <> "" Then
                            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP1").Clear()
                            objMatrix.AddRow()
                            objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
                        End If
                    ElseIf pVal.ItemUID = "31" And pVal.ColUID = "V_0" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        objMatrix1 = objForm.Items.Item("31").Specific
                        If objMatrix1.Columns.Item("V_0").Cells.Item(objMatrix1.VisualRowCount).Specific.String <> "" Then
                            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP2").Clear()
                            objMatrix1.AddRow()
                            objMatrix1.Columns.Item("V_-1").Cells.Item(objMatrix1.RowCount).Specific.value = objMatrix1.VisualRowCount
                        End If
                    ElseIf pVal.ItemUID = "32" And pVal.ColUID = "V_0" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        objMatrix2 = objForm.Items.Item("32").Specific
                        If objMatrix2.Columns.Item("V_0").Cells.Item(objMatrix2.VisualRowCount).Specific.String <> "" Then
                            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP3").Clear()
                            objMatrix2.AddRow()
                            objMatrix2.Columns.Item("V_-1").Cells.Item(objMatrix2.RowCount).Specific.value = objMatrix2.VisualRowCount
                        End If
                    ElseIf pVal.ItemUID = "33" And pVal.ColUID = "V_0" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        objMatrix3 = objForm.Items.Item("33").Specific
                        If objMatrix3.Columns.Item("V_0").Cells.Item(objMatrix3.VisualRowCount).Specific.String <> "" Then
                            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP4").Clear()
                            objMatrix3.AddRow()
                            objMatrix3.Columns.Item("V_-1").Cells.Item(objMatrix3.RowCount).Specific.value = objMatrix3.VisualRowCount
                        End If
                    ElseIf pVal.ItemUID = "34" And pVal.ColUID = "V_0" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        objMatrix4 = objForm.Items.Item("34").Specific
                        If objMatrix4.Columns.Item("V_0").Cells.Item(objMatrix4.VisualRowCount).Specific.String <> "" Then
                            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP5").Clear()
                            objMatrix4.AddRow()
                            objMatrix4.Columns.Item("V_-1").Cells.Item(objMatrix4.RowCount).Specific.value = objMatrix4.VisualRowCount
                        End If
                    ElseIf pVal.ItemUID = "35" And pVal.ColUID = "V_0" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        objMatrix5 = objForm.Items.Item("35").Specific
                        If objMatrix5.Columns.Item("V_0").Cells.Item(objMatrix5.VisualRowCount).Specific.String <> "" Then
                            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP6").Clear()
                            objMatrix5.AddRow()
                            objMatrix5.Columns.Item("V_-1").Cells.Item(objMatrix5.RowCount).Specific.value = objMatrix5.VisualRowCount
                        End If
                    ElseIf pVal.ItemUID = "36" And pVal.ColUID = "V_0" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        objMatrix6 = objForm.Items.Item("36").Specific
                        If objMatrix6.Columns.Item("V_0").Cells.Item(objMatrix6.VisualRowCount).Specific.String <> "" Then
                            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP7").Clear()
                            objMatrix6.AddRow()
                            objMatrix6.Columns.Item("V_-1").Cells.Item(objMatrix6.RowCount).Specific.value = objMatrix6.VisualRowCount
                        End If
                    ElseIf pVal.ItemUID = "44" And pVal.ColUID = "V_5" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        objMatrix7 = objForm.Items.Item("44").Specific
                        If objMatrix7.Columns.Item("V_5").Cells.Item(objMatrix7.VisualRowCount).Specific.String <> "" Then
                            objMatrix7.AddRow()
                            objMatrix7.Columns.Item("V_-1").Cells.Item(objMatrix7.RowCount).Specific.value = objMatrix7.VisualRowCount
                        End If
                    ElseIf pVal.ItemUID = "45" And pVal.ColUID = "V_8" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        objMatrix8 = objForm.Items.Item("45").Specific
                        If objMatrix8.Columns.Item("V_8").Cells.Item(objMatrix8.VisualRowCount).Specific.String <> "" Then
                            objMatrix8.AddRow()
                            objMatrix8.Columns.Item("V_-1").Cells.Item(objMatrix8.RowCount).Specific.value = objMatrix8.VisualRowCount
                        End If
                    End If
                    If pVal.ItemUID = "54" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        If LineValidation() Then
                        Else
                            BubbleEvent = False
                        End If
                    End If
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
                    If pVal.ItemUID = "30" And pVal.ColUID = "V_0" Then
                        objMatrix = objForm.Items.Item("30").Specific
                        objAddOn.objApplication.Menus.Item("MNU_PCDE").Activate()
                        Paylink(pVal.Row)
                    ElseIf pVal.ItemUID = "30" And pVal.ColUID = "V_6" Then
                        objMatrix = objForm.Items.Item("30").Specific
                        objAddOn.objApplication.Menus.Item("MNU_PCDE").Activate()
                        Paylink(pVal.Row)
                    End If
                    If pVal.ItemUID = "31" And pVal.ColUID = "V_0" Then
                        objMatrix1 = objForm.Items.Item("31").Specific
                        objAddOn.objApplication.Menus.Item("MNU_DEDUCTION").Activate()
                        DeductLink(pVal.Row)
                    ElseIf pVal.ItemUID = "31" And pVal.ColUID = "V_6" Then
                        objMatrix1 = objForm.Items.Item("31").Specific
                        objAddOn.objApplication.Menus.Item("MNU_DEDUCTION").Activate()
                        DeductLink(pVal.Row)
                    End If
                    If pVal.ItemUID = "32" And pVal.ColUID = "V_0" Then
                        objMatrix3 = objForm.Items.Item("32").Specific
                        objAddOn.objApplication.Menus.Item("MNU_REIMBURSEMENT").Activate()
                        ReimburshmentLink(pVal.Row)
                    ElseIf pVal.ItemUID = "32" And pVal.ColUID = "V_6" Then
                        objMatrix3 = objForm.Items.Item("32").Specific
                        objAddOn.objApplication.Menus.Item("MNU_REIMBURSEMENT").Activate()
                        ReimburshmentLink(pVal.Row)
                    ElseIf pVal.ItemUID = "32" And pVal.ColUID = "V_5" Then
                        objMatrix3 = objForm.Items.Item("32").Specific
                        objAddOn.objApplication.Menus.Item("MNU_REIMBURSEMENT").Activate()
                        ReimburshmentLink(pVal.Row)
                    End If
                    If pVal.ItemUID = "34" And pVal.ColUID = "V_0" Then
                        objMatrix4 = objForm.Items.Item("34").Specific
                        objAddOn.objApplication.Menus.Item("MNU_BCODE").Activate()
                        BenefitLink(pVal.Row)
                    ElseIf pVal.ItemUID = "34" And pVal.ColUID = "V_7" Then
                        objMatrix4 = objForm.Items.Item("34").Specific
                        objAddOn.objApplication.Menus.Item("MNU_BCODE").Activate()
                        BenefitLink(pVal.Row)
                    ElseIf pVal.ItemUID = "34" And pVal.ColUID = "V_5" Then
                        Dim ID, Name, BCode As String
                        Dim Amount As Integer
                        ID = objForm.Items.Item("4").Specific.string
                        Name = objForm.Items.Item("6").Specific.string
                        BCode = objMatrix4.Columns.Item("V_0").Cells.Item(pVal.Row).Specific.string
                        Amount = objMatrix4.Columns.Item("V_5").Cells.Item(pVal.Row).Specific.value
                        objAddOn.objBenefitDetail.Loadscreen(ID, Name, BCode, Amount)
                    End If
                    If pVal.ItemUID = "35" And pVal.ColUID = "V_0" Then
                        objMatrix5 = objForm.Items.Item("35").Specific
                        objAddOn.objApplication.Menus.Item("MNU_LeaveCode").Activate()
                        LeaveLink(pVal.Row)
                    End If
                    If pVal.ItemUID = "36" And pVal.ColUID = "V_0" Then
                        objMatrix6 = objForm.Items.Item("36").Specific
                        objAddOn.objApplication.Menus.Item("MNU_LADVANCE").Activate()
                        LoanadvanceLink(pVal.Row)
                    ElseIf pVal.ItemUID = "36" And pVal.ColUID = "V_2" Then
                        Dim ID, Name, Type, FromDate, ToDate As String
                        Dim Amount As Integer
                        ID = objForm.Items.Item("4").Specific.string
                        Name = objForm.Items.Item("6").Specific.string
                        Type = objMatrix6.Columns.Item("V_8").Cells.Item(pVal.Row).Specific.string
                        FromDate = objMatrix6.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.string
                        ToDate = objMatrix6.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.string
                        Amount = objMatrix6.Columns.Item("V_7").Cells.Item(pVal.Row).Specific.value
                        objAddOn.objLoanAdvaneSchedule.Loadscreen(ID, Name, Type, FromDate, ToDate, Amount)
                    End If
                    If pVal.ItemUID = "45" And pVal.ColUID = "V_8" Then
                        'objAddOn.objApplication.Menus.Item("MNU_PERCOD").Activate()
                        objMatrix8 = objForm.Items.Item("45").Specific
                        PerquisiteCodeLink()
                    End If
                    If pVal.ItemUID = "44" And pVal.ColUID = "V_2" Then
                        objMatrix7 = objForm.Items.Item("44").Specific
                        objAddOn.objApplication.Menus.Item("MNU_PROVSN").Activate()
                        ProvisionCodeLink()
                    End If

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "22" Then
                        objForm.PaneLevel = 1
                    ElseIf pVal.ItemUID = "23" Then
                        objForm.PaneLevel = 2
                        objMatrix1 = objForm.Items.Item("31").Specific
                        Addrow()
                    ElseIf pVal.ItemUID = "24" Then
                        objForm.PaneLevel = 3
                        objMatrix2 = objForm.Items.Item("32").Specific
                        Addrow()
                    ElseIf pVal.ItemUID = "25" Then
                        objForm.PaneLevel = 4
                        objMatrix3 = objForm.Items.Item("33").Specific
                        Addrow()
                    ElseIf pVal.ItemUID = "26" Then
                        objForm.PaneLevel = 5
                        objMatrix4 = objForm.Items.Item("34").Specific
                        Addrow()
                    ElseIf pVal.ItemUID = "27" Then
                        objForm.PaneLevel = 6
                        objMatrix5 = objForm.Items.Item("35").Specific
                        Addrow()
                    ElseIf pVal.ItemUID = "29" Then
                        objForm.PaneLevel = 8
                        objMatrix6 = objForm.Items.Item("36").Specific
                        Addrow()
                    End If
                    If pVal.ItemUID = "28" Then
                        objForm.PaneLevel = 7
                    End If
                    If (pVal.ItemUID = "76" Or pVal.ItemUID = "115") Then

                        StatuatoryPeriods(FormUID)

                        FinYear = objAddOn.objSalaryprocess.FindFinancialYear(CDate(TDSFYear).ToString("yyyyMMdd"), CDate(TDSTYear).ToString("yyyyMMdd"))
                        If FinYear = "" Then
                            objAddOn.objApplication.StatusBar.SetText("Please Create Financial Period ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                            Exit Sub
                        End If
                        objAddOn.objStatutory.TDSDate()
                        If TDSFromDate.Length = 1 Then
                            SMon = "0" + TDSFromDate
                        Else
                            SMon = TDSFromDate
                        End If
                        If TDSToDate.Length = 1 Then
                            EMon = "0" + TDSFromDate
                        Else
                            EMon = TDSFromDate
                        End If
                        EID = objForm.Items.Item("4").Specific.value
                        objAddOn.objgeneralsetting.GeneralSettings()
                        TDSCalcuationAmount(EID, EMon, TDSToYear, SMon, TDSFromYear, FinYear)

                        ' Calculation()
                        'ElseIf pVal.ItemUID = "115" Then
                        '    IncmeCalculation()
                    ElseIf pVal.ItemUID = "132" Then
                        UpdateTDSAmount(FormUID)
                    End If
                    If pVal.ItemUID = "22" Or pVal.ItemUID = "23" Or pVal.ItemUID = "24" Or pVal.ItemUID = "25" Or pVal.ItemUID = "26" Or pVal.ItemUID = "27" Or pVal.ItemUID = "28" Or pVal.ItemUID = "29" Then
                        Addrow()
                    End If
                    If pVal.ItemUID = "134" Then
                        objAddOn.objIncomeLossHouseProperty.LoadScreen(objForm.Items.Item("4").Specific.string, objForm.Items.Item("6").Specific.string)
                    End If

                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    objCombo = objForm.Items.Item("43").Specific
                    If pVal.ItemUID = "43" Then
                        If objCombo.Selected.Value = "INVESTMENT" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            Investment()
                        ElseIf objCombo.Selected.Value = "PERQUISITES" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            Perquisites(FormUID, pVal.Row)
                        ElseIf objCombo.Selected.Value = "SALARY" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            SalaryTab()
                        ElseIf objCombo.Selected.Value = "INCOMEDETAILS" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            IncomeDetails()
                            ICMESalary()
                        End If
                    End If

                    'objComboMonth = objForm.Items.Item("126").Specific
                    'objComboYear = objForm.Items.Item("128").Specific
                    'If objComboMonth.Selected.Value <> "" And objComboYear.Selected.Value <> "" And objForm.Items.Item("4").Specific.value <> "" Then
                    '    CheckFromDB(FormUID)
                    'End If


                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    objCFLEvent = pVal
                    objDataTable = objCFLEvent.SelectedObjects
                    If pVal.ItemUID = "4" Then
                        EmpidCFL()
                        Nodues()
                    End If
                    If pVal.ItemUID = "16" Then
                        OTCodeCFL()
                    End If
                    If pVal.ItemUID = "30" And pVal.ColUID = "V_0" Then
                        objMatrix = objForm.Items.Item("30").Specific
                        PaycodeCFL(pVal.Row)
                        CalcPaycode(FormUID, pVal.Row, "PayCode")
                    End If
                    If pVal.ItemUID = "31" And pVal.ColUID = "V_0" Then
                        objMatrix1 = objForm.Items.Item("31").Specific
                        DeductioncodeCFL(pVal.Row)
                        'DeductionCalculation(FormUID, pVal.Row)
                        CalcPaycode(FormUID, pVal.Row, "Detection")
                    End If
                    If pVal.ItemUID = "32" And pVal.ColUID = "V_0" Then
                        objMatrix2 = objForm.Items.Item("32").Specific
                        ReimbursementCFL(pVal.Row)
                    End If
                    If pVal.ItemUID = "34" And pVal.ColUID = "V_0" Then
                        objMatrix4 = objForm.Items.Item("34").Specific
                        BenefitcodeCFL(pVal.Row)
                        'BenefitsCalc(FormUID, pVal.Row)
                    End If
                    If pVal.ItemUID = "35" And pVal.ColUID = "V_0" Then
                        objMatrix5 = objForm.Items.Item("35").Specific
                        LeavecodeCFL(pVal.Row)
                        Absentday(pVal.Row)
                        LeaveCarryForward(pVal.Row)
                    End If
                    If pVal.ItemUID = "36" And pVal.ColUID = "V_0" Then
                        objMatrix6 = objForm.Items.Item("36").Specific
                        LoanAdvanceCFL(pVal.Row)
                    End If

                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    objMatrix = objForm.Items.Item("30").Specific
                    If pVal.ItemUID = "30" And pVal.ColUID = "V_0" Then
                        PaycodeBF(pVal.Row)
                        CalcPaycode(FormUID, pVal.Row, "PayCode")
                    End If
                    objMatrix1 = objForm.Items.Item("31").Specific
                    If pVal.ItemUID = "31" And pVal.ColUID = "V_0" Then
                        '  DeductionBF(pVal.Row)
                        CalcPaycode(FormUID, pVal.Row, "PayCode")
                    End If
                    objMatrix2 = objForm.Items.Item("32").Specific
                    If pVal.ItemUID = "32" And pVal.ColUID = "V_8" Then
                        ReimbursehmentFUN(pVal.Row)
                    ElseIf pVal.ItemUID = "32" And (pVal.ColUID = "V_3" Or pVal.ColUID = "V_2") Then
                        ReimbLineTotal(FormUID, pVal.Row)
                        ReimbHeadTotal()
                    End If
                    objMatrix3 = objForm.Items.Item("33").Specific
                    If pVal.ItemUID = "33" And pVal.ColUID = "V_10" Then
                        ArrearHeadTotal()
                    End If
                    objMatrix4 = objForm.Items.Item("34").Specific
                    If pVal.ItemUID = "34" And pVal.ColUID = "V_5" Then
                        BenefitHeadTotal()
                    End If
                    objMatrix5 = objForm.Items.Item("35").Specific
                    If pVal.ItemUID = "35" And (pVal.ColUID = "V_3" Or pVal.ColUID = "V_2") Then
                        EmpLeaveCal(FormUID, pVal.Row)
                    End If
                    If pVal.ItemUID = "35" And pVal.ColUID = "V_0" Then
                        MaximumDaysFun(FormUID, pVal.Row)
                    End If
                    objMatrix6 = objForm.Items.Item("36").Specific
                    If pVal.ItemUID = "36" And pVal.ColUID = "V_0" Then
                        LoanAdvancFUN(pVal.Row)
                    End If
                    If pVal.ItemUID = "36" And pVal.ColUID = "V_2" Then
                        objMatrix6 = objForm.Items.Item("36").Specific
                        InstalmentFun(pVal.Row)
                    End If
                    objMatrix = objForm.Items.Item("30").Specific
                    If pVal.ItemUID = "30" And (pVal.ColUID = "V_5" Or pVal.ColUID = "V_4" Or pVal.ColUID = "V_0") Then
                        DocTotal1()
                    ElseIf pVal.ItemUID = "31" And (pVal.ColUID = "V_5" Or pVal.ColUID = "V_4" Or pVal.ColUID = "V_0") Then
                        objMatrix1 = objForm.Items.Item("31").Specific
                        DocTotal2()
                    End If
                    If pVal.ItemUID = "4" And objForm.PaneLevel = 1 And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        CheckFromDB(FormUID)
                        objMatrix = objForm.Items.Item("30").Specific
                        If objMatrix.VisualRowCount = 0 Then
                            objMatrix.AddRow()
                            objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
                        End If
                    End If
                    If pVal.ItemUID = "45" And pVal.ColUID = "V_8" Then
                        PerquisiteDES(pVal.Row)
                    End If
                    objMatrix8 = objForm.Items.Item("45").Specific
                    If pVal.ItemUID = "45" And (pVal.ColUID = "V_5" Or pVal.ColUID = "V_4") Then
                        AmountofTax(pVal.Row)
                    End If
                    If pVal.ItemUID = "45" And pVal.ColUID = "V_8" Then
                        If objMatrix8.Columns.Item("V_8").Cells.Item(objMatrix8.VisualRowCount).Specific.value <> "" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            objMatrix8.AddRow()
                            objMatrix8.Columns.Item("V_-1").Cells.Item(objMatrix8.RowCount).Specific.value = objMatrix8.VisualRowCount
                            PerquisiteTab(FormUID, pVal.Row)
                        End If


                        'ElseIf objMatrix8.Columns.Item("V_8").Cells.Item(objMatrix8.VisualRowCount).Specific.value <> "" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        '    objMatrix8.AddRow()
                        '    objMatrix8.Columns.Item("V_-1").Cells.Item(objMatrix8.RowCount).Specific.value = objMatrix8.VisualRowCount
                    End If
                    If pVal.ItemUID = "54" Then
                        objMatrix = objForm.Items.Item("30").Specific
                        MonthSalary(pVal.Row)
                    End If

                    If pVal.ItemUID = "8" Then
                        Dim dat, year As String

                        'MsgBox(Right(objForm.Items.Item("8").Specific.string, 2))
                        dat = Right(objForm.Items.Item("8").Specific.string, 2)
                        year = "20" + dat
                        'ProcessedSalary(FormUID, year)


                    End If

                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    If pVal.ItemUID = "30" Then
                        objMatrix = objForm.Items.Item("30").Specific
                        objCombo = objMatrix.Columns.Item("V_5").Cells.Item(pVal.Row).Specific
                        If objCombo.Selected.Value = "HRA" Then
                            PayCodeCalc(pVal.Row, pVal.ItemUID)
                        ElseIf objCombo.Selected.Value = "GA" Then
                            PayCodeCalc(pVal.Row, pVal.ItemUID)
                        End If
                    End If
                    If pVal.ItemUID = "31" Then
                        objMatrix1 = objForm.Items.Item("31").Specific
                        objCombo = objMatrix1.Columns.Item("V_5").Cells.Item(pVal.Row).Specific
                        If objCombo.Selected.Value = "PF" Then
                            '  DeductnCodeCalc(pVal.Row)
                        End If
                    End If
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        objForm.Close()
                    End If
            End Select
        End If
    End Sub
#End Region

#Region "Deduction Calculation"

    Public Sub DeductionCalculation(ByVal FormUID As String, ByVal Row As Integer)
        Dim objRs1, RS, rs1 As SAPbobsCOM.Recordset
        Dim SQL As String
        Dim PF As Double
        objMatrix = objForm.Items.Item("30").Specific
        objMatrix1 = objForm.Items.Item("31").Specific
        objRs1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        rs1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        SQL = "select salary as Cost,workCountr [Country] from OHEM where empID='" & objForm.Items.Item("4").Specific.string & "'"
        objRs1.DoQuery(SQL)
        Dim GrossSalary As Double = CDbl(objRs1.Fields.Item("cost").Value)
        If objMatrix1.Columns.Item("V_7").Cells.Item(Row).Specific.string = "PT" Then
            SQL = "select U_frmamt [From Amount],U_toamt [To Amount],U_ptamt [PT Amt] from [@AIS_PTS1] T0 " & _
                " join [@AIS_DPTS] T1 on T0.Code =T1.Code where T1.U_state =(select workState from OHEM where empID ='" & objForm.Items.Item("4").Specific.string & "') "
            RS.DoQuery(SQL)

            For I As Integer = 0 To RS.RecordCount - 1
                If objRs1.Fields.Item("cost").Value >= RS.Fields.Item("From Amount").Value And objRs1.Fields.Item("cost").Value <= RS.Fields.Item("To Amount").Value Then
                    objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = RS.Fields.Item("PT Amt").Value
                End If
                RS.MoveNext()
            Next
        End If

        RS.DoQuery("select isnull(U_pfcempee,0) as PF,isnull(U_pflim,0) as Limit from [@AIS_STAT]")

        SQL = "select isnull(U_pflim,'N') [PF Limit] from OHEM where empID='" & objForm.Items.Item("4").Specific.string & "'"
        objRs1.DoQuery(SQL)

        objMatrix = objForm.Items.Item("30").Specific
        If objMatrix1.Columns.Item("V_7").Cells.Item(Row).Specific.string = "PF" Then
            For I As Integer = 1 To objMatrix.RowCount
                Dim aaa As String = objMatrix.Columns.Item("V_7").Cells.Item(I).Specific.string
                If objMatrix.Columns.Item("V_7").Cells.Item(I).Specific.string = "Basic" Or objMatrix.Columns.Item("V_7").Cells.Item(I).Specific.string = "BASIC" Then

                    objRS.DoQuery("select Name from [@AIS_paycode] where U_pf='Y' and Name='Basic'")

                    If objRS.RecordCount > 0 Then
                        If objRs1.Fields.Item("PF Limit").Value = "Y" Then

                            If GrossSalary > RS.Fields.Item("Limit").Value Then
                                PF = RS.Fields.Item("Limit").Value * RS.Fields.Item("PF").Value / 100
                                objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = PF
                            Else
                                PF = objMatrix.Columns.Item("V_4").Cells.Item(I).Specific.string * RS.Fields.Item("PF").Value / 100
                                objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = PF
                            End If
                        Else
                            PF = objMatrix.Columns.Item("V_4").Cells.Item(I).Specific.string * RS.Fields.Item("PF").Value / 100
                            objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = PF
                        End If
                    Else
                        objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = 0.0
                    End If
                End If
            Next I
        End If
        ''-----------------------Code For ESI------------------------

        rs1.DoQuery("select isnull(U_esicempee,0) as PF,isnull(U_esiwage,0) as Limit from [@AIS_STAT]")
        If objMatrix1.Columns.Item("V_7").Cells.Item(Row).Specific.string = "ESI" Then
            For I As Integer = 1 To objMatrix.RowCount
                Dim aaa As String = objMatrix.Columns.Item("V_7").Cells.Item(I).Specific.string
                If objMatrix.Columns.Item("V_7").Cells.Item(I).Specific.string = "Basic" Or objMatrix.Columns.Item("V_7").Cells.Item(I).Specific.string = "BASIC" Then

                    objRS.DoQuery("select Name from [@AIS_paycode] where U_esi='Y' and Name='Basic'")

                    If objRS.RecordCount > 0 Then
                        'If objRs1.Fields.Item("PF Limit").Value = "Y" Then

                        If GrossSalary <= rs1.Fields.Item("Limit").Value Then
                            PF = rs1.Fields.Item("Limit").Value * rs1.Fields.Item("PF").Value / 100
                            objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = PF
                        Else
                            objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = 0.0
                            '    PF = objMatrix.Columns.Item("V_4").Cells.Item(I).Specific.string * RS.Fields.Item("PF").Value / 100
                            '    objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = PF
                        End If
                        'Else
                        '    PF = objMatrix.Columns.Item("V_4").Cells.Item(I).Specific.string * RS.Fields.Item("PF").Value / 100
                        '    objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = PF
                        'End If
                    Else
                        objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = 0.0
                    End If
                End If
            Next I
        End If
        '------------------------------------------------------------------------
    End Sub
#End Region

#Region "Calculate Paycodes"
    Public Sub CalcPaycode(ByVal FormUID As String, ByVal Row As Integer, ByVal GridName As String)
        Dim objRs1 As SAPbobsCOM.Recordset
        Dim SQL As String
        Dim BP, HRA, MedAll, TransAllow, CON, EduAll, Tele, TB, ERC, EC, EPF, EESIC, SpecialAllowance As Double

        Dim MedAllFlag As Boolean = False
        Dim TraAllFlag As Boolean = False
        Dim SpAllFlag As Boolean = False

        MedAll = 1250
        TransAllow = 1600


        objMatrix = objForm.Items.Item("30").Specific
        objDecMatrix = objForm.Items.Item("31").Specific

        objRs1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        SQL = "select salary from OHEM where empID='" & objForm.Items.Item("4").Specific.string & "'"
        objRs1.DoQuery(SQL)
        ' MsgBox(objRs1.Fields.Item("salary").Value)
        For lcount As Integer = 0 To objMatrix.VisualRowCount - 1

            If (objMatrix.Columns.Item("V_7").Cells.Item(lcount + 1).Specific.value.ToString.Trim = "MEDALL") Then
                MedAllFlag = True
            End If
            If (objMatrix.Columns.Item("V_7").Cells.Item(lcount + 1).Specific.value.ToString.Trim = "TRANSALL") Then
                TraAllFlag = True
            End If
            If (objMatrix.Columns.Item("V_7").Cells.Item(lcount + 1).Specific.value.ToString.Trim = "SPECALL") Then
                SpAllFlag = True
            End If

        Next

        If (CDbl(objRs1.Fields.Item("salary").Value)) > 15000 Then

            BP = objRs1.Fields.Item("salary").Value * 0.5 'Basic
            HRA = BP * 0.5  'HRA
            TB = BP * 0.16
            ERC = BP * 0.12
            EC = BP * 0.12
            ' Dim epf1 As Double = 13.36 / 100
            '  EPF = 15000 * 0.1336





            If (MedAllFlag = False) Then MedAll = 0
            If (TransAllow = False) Then TransAllow = 0


            Dim specialallowance1 As Double
            EESIC = 0
            specialallowance1 = BP + HRA + MedAll + TransAllow
            SpecialAllowance = objRs1.Fields.Item("salary").Value - specialallowance1
            If (SpAllFlag = False) Then SpecialAllowance = 0


            If (BP + MedAll + TransAllow + SpecialAllowance) > 15000 Then
                EPF = (15000 * 12) / 100
            Else
                EPF = ((BP + MedAll + TransAllow + (objRs1.Fields.Item("salary").Value - (BP + HRA + MedAll + TransAllow))) * 12) / 100
            End If


        Else

            If (objRs1.Fields.Item("salary").Value >= 12501) Then
                BP = objRs1.Fields.Item("salary").Value * 0.5
                HRA = BP * 0.5
            ElseIf (objRs1.Fields.Item("salary").Value > 8000 And objRs1.Fields.Item("salary").Value <= 12500) Then
                BP = 6250
                HRA = (objRs1.Fields.Item("salary").Value / 2) * 0.5
            Else
                BP = 5800
                HRA = (objRs1.Fields.Item("salary").Value - BP)
            End If


            'BP = objRs1.Fields.Item("salary").Value * 0.5
            ' HRA = BP * 0.5
            TB = BP * 0.16
            ERC = BP * 0.12
            EC = BP * 0.12
            ' EPF = objRs1.Fields.Item("salary").Value * 0.1336


         
            ' EPF = ((BP + MedAll + TransAllow + (objRs1.Fields.Item("salary").Value - (BP + HRA + MedAll + TransAllow))) * 12) / 100



            If (MedAllFlag = False) Then MedAll = 0
            If (TraAllFlag = False) Then TransAllow = 0


            EESIC = (objRs1.Fields.Item("salary").Value * 1.75) / 100
            Dim specialallowance1 As Double
            specialallowance1 = BP + HRA + MedAll + TransAllow
            SpecialAllowance = objRs1.Fields.Item("salary").Value - specialallowance1

            If (SpAllFlag = False) Then SpecialAllowance = 0

            Dim PTax As Double = 0


            If (BP + MedAll + TransAllow + SpecialAllowance) > 15000 Then
                EPF = (15000 * 12) / 100
            Else
                EPF = ((BP + MedAll + TransAllow + (objRs1.Fields.Item("salary").Value - (BP + HRA + MedAll + TransAllow))) * 12) / 100
            End If


            'Try


            'Dim query As String = "select U_ptamt  from [@ais_PTS1] where '" & (objRs1.Fields.Item("salary").Value * 6) & "' between U_frmamt and U_toamt "
            'Dim RSValue As SAPbobsCOM.Recordset
            'RSValue = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            'RSValue.DoQuery(query)
            'If (RSValue.RecordCount > 0) Then
            '        PTax = (RSValue.Fields.Item("U_ptamt").Value * 2) / 12

            'Else
            '    query = "select Top 1 U_ptamt  from [@ais_PTS1] order by code desc"
            '    RSValue.DoQuery(query)
            '        PTax = (RSValue.Fields.Item("U_ptamt").Value * 2) / 12
            '    End If

            'Catch ex As Exception

            'End Try

            'Dim NetSalaryESI As Double = 0
            'EESIC = ((BP + HRA + TransAllow + MedAll + SpecialAllowance) - (PTax + EPF + 10)) * 1.75 / 100

        End If

        'Detection 
        If (GridName.ToString.Trim = "Detection") Then

            Try


                If objDecMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "PT" Then
                    Dim query As String = "select U_ptamt  from [@ais_PTS1] where '" & (objRs1.Fields.Item("salary").Value * 6) & "' between U_frmamt and U_toamt "
                    Dim RSValue As SAPbobsCOM.Recordset
                    RSValue = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    RSValue.DoQuery(query)
                    If (RSValue.RecordCount > 0) Then
                        objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = (RSValue.Fields.Item("U_ptamt").Value * 2) / 12

                    Else
                        query = "select Top 1 U_ptamt  from [@ais_PTS1] order by code desc"
                        RSValue.DoQuery(query)
                        objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = (RSValue.Fields.Item("U_ptamt").Value * 2) / 12
                    End If
                End If

            Catch ex As Exception

            End Try

            If objDecMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "PF" Then
                objDecMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = EPF
            End If

            'If objDecMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "PF" Then
            '    objDecMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = EPF
            'End If

            If objDecMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "ESI" Then
                objDecMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = EESIC
            End If

            If objDecMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "TNLWF" Then
                objDecMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = 10
            End If

            'If objDecMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "PT" Then
            '    objDecMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = 10
            'End If

        End If




        'MedAll = objRs1.Fields.Item("salary").Value * 0.1
        'CON = objRs1.Fields.Item("salary").Value * 0.05
        'EduAll = objRs1.Fields.Item("salary").Value * 0.05
        'Tele = objRs1.Fields.Item("salary").Value * 0.05

        If (GridName.ToString.Trim = "PayCode") Then

            If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "BASIC" Then
                'BP = Fixed * 0.28
                objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = BP
            End If
            If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "EPF" Then
                'BP = Fixed * 0.28
                objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = EPF
            End If
            If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "EESIC" Then
                'BP = Fixed * 0.28
                objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = EESIC
            End If
            If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "MEDALL" Then
                'BP = Fixed * 0.28
                objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = MedAll
            End If
            If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "TRANSALL" Then
                'BP = Fixed * 0.28
                objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = TransAllow
            End If
            If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "SPECALL" Then
                'BP = Fixed * 0.28
                objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = SpecialAllowance


            End If
            If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "HRA" Then
                'HRA = BP / 2
                objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = HRA
            End If
            If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "TB" Then
                'HRA = BP / 2
                objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = TB
            End If
            If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "ERC" Then
                'HRA = BP / 2
                objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = ERC
            End If
            If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "EC" Then
                'HRA = BP / 2
                objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = EC

            End If
        End If

        'If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "Conveyance" Then
        '    'CON = 800
        '    objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = CON
        'End If

        'If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "Med All" Then
        '    'CON = 800
        '    objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = MedAll
        'End If

        'If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "Edu All" Then
        '    'CON = 800
        '    objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = EduAll
        'End If

        'If objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.string = "Telephone" Then
        '    'CON = 800
        '    objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = Tele
        'End If

    End Sub
#End Region

#Region "Calculate BeneFits"
    Public Sub BenefitsCalc(ByVal FormUID As String, ByVal Row As Integer)
        Dim CCP, CCG, MR As Double
        CCP = Basic * 13.61 / 100
        CCG = Basic * 4.8 / 100
        MR = 1250
        objMatrix = objForm.Items.Item("34").Specific
        If objMatrix.Columns.Item("V_8").Cells.Item(Row).Specific.string = "CCP" Then
            objMatrix.Columns.Item("V_5").Cells.Item(Row).Specific.value = CCP
        End If

        If objMatrix.Columns.Item("V_8").Cells.Item(Row).Specific.string = "CCG" Then
            objMatrix.Columns.Item("V_5").Cells.Item(Row).Specific.value = CCG
        End If

        If objMatrix.Columns.Item("V_8").Cells.Item(Row).Specific.string = "MR" Then
            objMatrix.Columns.Item("V_5").Cells.Item(Row).Specific.value = MR
        End If

    End Sub
#End Region

#Region "Find the Statuatory Periods"
    Sub StatuatoryPeriods(ByVal FormUID As String)
        Try
            Dim objRS As SAPbobsCOM.Recordset
            Dim Query As String
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Query = "select U_tdsfrom [FYear],U_tdsto [TYear]  from [@AIS_STAT] "
            objRS.DoQuery(Query)
            If objRS.RecordCount > 0 Then
                TDSFYear = objRS.Fields.Item("FYear").Value
                TDSTYear = objRS.Fields.Item("TYear").Value
            End If
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Statuatory Periods Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
#End Region

#Region "Calculate the TDS Amount"
    Sub TDSCalcuationAmount(ByVal EID As String, ByVal EMonth As Integer, ByVal EYear As Integer, ByVal SMonth As Integer, ByVal SYear As Integer, ByVal FinYear As String)

        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Dim MonCount As Integer
            Dim MonBasic, MonHRA, HRAExcepmt As Double
            Dim MetroEmp As String
            Dim oRs1 As SAPbobsCOM.Recordset = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            'To find Already Paid PF Amount:
            strSQL = "select isnull(SUM(convert(float,U_amount) ),0)[Amount] from [@AIS_OSPS] where U_empid ='" & EID & "' " & _
                " and U_fyear ='" & FinYear & "' and U_heads in (select Name from [@AIS_DDC] where U_pf ='Y') and U_heads not in('Pay','Remarks')"

            oRS.DoQuery(strSQL)

            PF = oRS.Fields.Item("Amount").Value

            ' To Find Already Paid Professional Tax Amount:
            strSQL = "select isnull(SUM(convert(float,U_amount) ),0)[Amount] from [@AIS_OSPS] where U_empid ='" & EID & "' " & _
                " and U_fyear ='" & FinYear & "' and U_heads in (select Name from [@AIS_DDC] where U_pt ='Y') and U_heads not in('Pay','Remarks')"

            oRS.DoQuery(strSQL)

            PT = oRS.Fields.Item("Amount").Value



            'To Find Arrears Amount from Processed Salary:
            strSQL = "select isnull(SUM(convert(float,U_amount) ),0)[Over Time] from [@AIS_OSPS] where U_empid ='" & EID & "' " & _
               " and U_fyear ='" & FinYear & "' and U_heads in('Arrears') and U_heads not in('Pay','Remarks')"
            oRS.DoQuery(strSQL)

            OverTime = oRS.Fields.Item("Over Time").Value
            'To find the Overtime Amount:
            strSQL = "select SUM(U_amount )[Amount] from [@AIS_OVERT] where U_fyear='" & FinYear & "' and U_code ='" & EID & "'"
            oRS.DoQuery(strSQL)
            OverTime += oRS.Fields.Item("Amount").Value

            'Query for find the Metro and Non Metro Percentage:
            strSQL = "select isnull(U_metro,0) [Metro],isnull(U_nmetro,0) [Non Metro] from [@AIS_STAT] "
            oRS.DoQuery(strSQL)

            Metro = oRS.Fields.Item("Metro").Value
            NonMetro = oRS.Fields.Item("Non Metro").Value


            'To get Count payable Month:

            strSQL = "select CASE when  convert(date,(select startDate  from OHEM where empID ='" & EID & "'),108)>convert(date,(select U_tdsfrom from [@AIS_stat]),108) " & _
                     " then datediff(m,convert(date,(select startDate  from OHEM where empID ='" & EID & "'),108) " & _
                     " ,convert(date,(select U_tdsto from [@AIS_stat]),108)) +1 else 12 end [MonthCount] ,(select isnull(U_arpaid,0) from [@AIS_EMPSSTP] where U_eid ='" & EID & "') [Rent Paid]"

            oRS.DoQuery(strSQL)
            MonCount = oRS.Fields.Item("MonthCount").Value
            Count = oRS.Fields.Item("MonthCount").Value

            strSQL = " select U_month [Con],U_year [Year],(select isnull(U_arpaid,0) from [@AIS_EMPSSTP] where U_eid ='" & EID & "') [Rent Paid], " & _
                    " (select isnull(U_metro,'N') from [@AIS_EMPSSTP] where U_eid ='" & EID & "') [Metro]  from [@AIS_OSPS] " & _
                    " where U_fyear ='" & FinYear & "' and U_empid ='" & EID & "' group by U_month ,U_year "
            oRs1.DoQuery(strSQL)
            Count = Count - oRs1.RecordCount

            Minimum = 0
            Basic = 0
            HRA = 0
            Amount = 0
            HRAExcepmt = 0
            RentPaid = oRS.Fields.Item("Rent Paid").Value / MonCount
            MetroEmp = oRs1.Fields.Item("Metro").Value

            objForm.Items.Item("60").Specific.value = oRs1.RecordCount 'Paste the Processed Month Number in Processed Month Box


            '---------------------------------------------------- HRA Excemption Start ------------------------------------------------------------------

            For IntK As Integer = 1 To oRs1.RecordCount

                '  If RentPaid = 0 Then Exit For ' if Actual Rent didn't give then Exit For Loop:

                'Taking the values from DB processed salary Details:

                strSQL = "DECLARE @sql varchar(MAX) declare @columnscsv varchar(MAX) DECLARE @sql1 varchar(MAX) " & _
                            " select @columnscsv = COALESCE(@columnscsv + '],[','') + U_descrpn from [@AIS_EPSSTP1] T0 left join [@AIS_EMPSSTP] T1 on T0.Code =T1.Code where U_tax ='Y' and T1.U_eid ='" & EID & "'  " & _
                            " select @sql1=COALESCE(@sql1 + ']+[','') +  U_descrpn from [@AIS_EPSSTP1] T0 left join [@AIS_EMPSSTP] T1 on T0.Code =T1.Code where U_tax ='Y' and T1.U_eid ='" & EID & "'  " & _
                            " set @columnscsv = '[' + @columnscsv + ']' set @sql1 = '[' + @sql1 + ']' " & _
                            " SET @sql = ' SELECT  ' + @columnscsv + ', '+ @sql1 +' as Total FROM " & _
                            " (select SUM(convert(float,x.Amount ) )[Amount] ,x.Heads from(select (U_amount)[Amount] ,U_heads [Heads] " & _
                            " from [@AIS_OSPS] where U_empid =''" & EID & "'' and " & _
                            " U_fyear=''" & FinYear & "'' and U_month=''" & oRs1.Fields.Item("Con").Value & "'' and U_year=''" & oRs1.Fields.Item("Year").Value & "'' and U_heads not in(''Pay'',''Remarks'') " & _
                            " and U_heads in(select U_descrpn from [@AIS_EPSSTP1] T0 left join [@AIS_EMPSSTP] T1 on T0.Code =T1.Code where U_tax =''Y'' and T1.U_eid =''" & EID & "''  " & _
                            " ) ) x group by x.Heads  ) a " & _
                            " PIVOT (SUM(amount) for Heads in (' + @columnscsv + ')) as PVT' EXEC (@sql) "
                oRS.DoQuery(strSQL)

                Basic += oRS.Fields.Item("Basic").Value
                HRA += oRS.Fields.Item("HRA").Value
                ' DA = oRS.Fields.Item("DA").Value
                Amount += oRS.Fields.Item("Total").Value

                MonBasic = oRS.Fields.Item("Basic").Value
                MonHRA = oRS.Fields.Item("HRA").Value

                If RentPaid = 0 Then
                    Minimum = 0
                    objForm.Items.Item("49").Specific.value = Minimum
                Else
                    If MetroEmp = "Y" Then
                        FiftyPer = (MonBasic + DA) * Metro / 100
                        HRARcd = MonHRA
                        RntPDTenPer = RentPaid - ((MonBasic + DA) * 0.1)

                        'Find the Minimum of those three values:
                        Minimum = Math.Min(Math.Min(FiftyPer, HRARcd), RntPDTenPer)
                        If Minimum < 0 Then
                            ' objForm.Items.Item("49").Specific.value = 0
                            HRAExcepmt += 0
                        Else
                            HRAExcepmt += Minimum
                        End If
                    Else
                        FiftyPer = (MonBasic + DA) * NonMetro / 100
                        HRARcd = MonHRA
                        RntPDTenPer = RentPaid - ((MonBasic + DA) * 0.1)

                        'Find the Minimum of those three values:
                        Minimum = Math.Min(Math.Min(FiftyPer, HRARcd), RntPDTenPer)
                        ' HRARcd = HRA - Minimum
                        If Minimum < 0 Then
                            HRAExcepmt += 0
                        Else
                            HRAExcepmt += Minimum
                        End If
                    End If
                End If
                oRs1.MoveNext()
            Next

            objForm.Items.Item("62").Specific.value = Amount 'Paste the Processed Total Amount in Processed Salary Box:

            '------------------------------------------- Taking the values to be processed Month from the Employee Master Start -----------------------------------------------
            'Remaining Month Data's for the FInancial Year:


            strSQL = "select isnull(SUM(U_amt),0) [Total],isnull(U_arpaid,0) [Act Rent Paid],isnull(T0.U_metro,'N') [Metro]  " & _
                  " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where U_descrpn ='Basic' and U_eid =" & EID & " ),0)[Basic] " & _
                  " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where U_descrpn ='HRA' and U_eid =" & EID & " ),0)[HRA] " & _
                  " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where U_descrpn ='DA' and U_eid =" & EID & " ),0)[DA] " & _
                  " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP2] T1 on T0.Code =T1.Code where U_descrpn in (select Name from [@AIS_DDC] where U_pf ='Y') and U_eid =" & EID & " ),0)[PF]" & _
                  " ,isnull((select U_amt from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP2] T1 on T0.Code =T1.Code where U_descrpn in (select Name from [@AIS_DDC] where U_pt ='Y') and U_eid =" & EID & " ),0)[PT]" & _
                  " from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP1] T1 on T0.Code =T1.Code where U_eid =" & EID & " and U_tax='Y' group by U_arpaid,U_metro  "
            oRS.DoQuery(strSQL)

            For IntK As Integer = 1 To Count
                'Earnings:
                '  If RentPaid = 0 Then Exit For

                ' MsgBox(oRS.Fields.Item("Total").Value)
                Amount += oRS.Fields.Item("Total").Value
                Basic += oRS.Fields.Item("Basic").Value
                HRA += oRS.Fields.Item("HRA").Value
                DA += oRS.Fields.Item("DA").Value
                PF += oRS.Fields.Item("PF").Value

                MonBasic = oRS.Fields.Item("Basic").Value
                MonHRA = oRS.Fields.Item("HRA").Value

                If RentPaid = 0 Then
                    Minimum = 0
                    objForm.Items.Item("49").Specific.value = Minimum
                Else
                    If MetroEmp = "Y" Then
                        FiftyPer = (MonBasic + DA) * Metro / 100
                        HRARcd = MonHRA
                        RntPDTenPer = RentPaid - ((MonBasic + DA) * 0.1)

                        'Find the Minimum of those three values:
                        Minimum = Math.Min(Math.Min(FiftyPer, HRARcd), RntPDTenPer)
                        If Minimum < 0 Then
                            ' objForm.Items.Item("49").Specific.value = 0
                            HRAExcepmt += 0
                        Else
                            HRAExcepmt += Minimum
                        End If
                    Else
                        FiftyPer = (MonBasic + DA) * NonMetro / 100
                        HRARcd = MonHRA
                        RntPDTenPer = RentPaid - ((MonBasic + DA) * 0.1)

                        'Find the Minimum of those three values:
                        Minimum = Math.Min(Math.Min(FiftyPer, HRARcd), RntPDTenPer)
                        ' HRARcd = HRA - Minimum
                        If Minimum < 0 Then
                            HRAExcepmt += 0
                        Else
                            HRAExcepmt += Minimum
                        End If
                    End If
                End If

            Next

            objForm.Items.Item("54").Specific.value = Count
            objForm.Items.Item("47").Specific.value = oRS.Fields.Item("Total").Value * Count
            objForm.Items.Item("49").Specific.value = HRAExcepmt  'HRA Excemption Value in Text Box

            '-------------------------------------- Taking the values to be processed Month from the Employee Master End -------------------------------------------------------------




            '------------------------------- HRA Excemption End -------------------------------------------------------------------------------------



            strSQL = "select salary  from OHEM where empID =" & EID & ""
            oRS.DoQuery(strSQL)

            Gross = oRS.Fields.Item("Salary").Value

            strSQL = " select b.U_frmamt [From Amount],U_toamt [To Amount],U_ptamt [PT Amount] from [@AIS_DPTS] A " & _
             " left join  [@AIS_PTS1] B on A.Code =B.Code where U_state =(select workState [State] from " & _
             " OHEM T0 left join [@AIS_EMPSSTP] T1 on T0.empID =T1.U_eid where T1.U_eid =" & EID & ") "

            oRS.DoQuery(strSQL)
            For IntI = 1 To oRS.RecordCount
                FromAmount = oRS.Fields.Item("From Amount").Value
                ToAmount = oRS.Fields.Item("To Amount").Value
                PTAmount = oRS.Fields.Item("PT Amount").Value

                If Gross >= FromAmount And Gross <= ToAmount Then
                    PT += PTAmount * Count
                End If
                oRS.MoveNext()
            Next
            'Previous Employment Details:
            Amount += (objForm.Items.Item("67").Specific.value + OverTime + objForm.Items.Item("58").Specific.value)
            PF += objForm.Items.Item("71").Specific.value
            PT += objForm.Items.Item("75").Specific.value

            'To find the Paid benefits taxable values:
            strSQL = " select isnull(SUM(convert(float,U_amount) ),0)[Benefits] from [@AIS_OSPS] where U_empid =" & EID & " " & _
                " and U_fyear ='" & FinYear & "' and " & _
                " U_heads in(select T1.U_descrpn from [@AIS_EMPSSTP] T0 left join [@AIS_EPSSTP5] T1 on T0.Code =T1.Code " & _
                " where T0.U_eid =" & EID & " and T1.U_tax ='Y' ) and U_heads not in('Pay','Remarks')  "

            oRS.DoQuery(strSQL)
            objForm.Items.Item("62").Specific.value = Val(objForm.Items.Item("62").Specific.value) + oRS.Fields.Item("Benefits").Value
            Amount += oRS.Fields.Item("Benefits").Value

            'To find Un Paid Benefits Amounts:
            strSQL = "select SUM(U_amt ) [Benefits] from [@AIS_EPSSTP5] T0 join [@AIS_EMPSSTP] T1 on T0.Code =T1.Code " & _
                 " where T1.U_eid ='" & EID & "' and T0.U_bcode in(select Code from [@AIS_BCODE] where U_atv ='Y' ) and T0.U_tax ='Y'"

            oRS.DoQuery(strSQL)
            objForm.Items.Item("62").Specific.value = Val(objForm.Items.Item("62").Specific.value) + oRS.Fields.Item("Benefits").Value
            Amount += oRS.Fields.Item("Benefits").Value

            GrossTaxSalary = Amount - (HRAExcepmt + TransAllow + MedReimb)
            '  GrossTaxSalary = Amount - (Minimum + 9600 + 15000)

            'strSQL = "select isnull(sum(T1.U_amt),0) Amount from [@AIS_EMPSSTP] T0 " & _
            '         " left join [@AIS_EPSSTP8]  T1 on T0.Code =T1.Code where T0.U_eid =" & EID & " and T1.U_pvisn='Excempt'"
            'oRS.DoQuery(strSQL)


            GrandTotalIncome = GrossTaxSalary
            ' OtherAllowances = 0
            strSQL = "select U_pvisn [Provision],isnull(sum(T1.U_amt),0) Amount,T2.U_qualamt [Limit] " & _
                     "  from [@AIS_EMPSSTP] T0   left join [@AIS_EPSSTP8]  T1 on T0.Code =T1.Code  " & _
                    "  left join [@AIS_PRVMSTR] T2 on T1.U_pvisn =T2.Code " & _
                    "  where T0.U_eid =" & EID & " and U_active ='Y' and U_pvisn is not null group by U_pvisn ,T2.U_qualamt "

            oRS.DoQuery(strSQL)
            OtherExcemption = 0
            Count80C = 0
            If oRS.RecordCount > 0 Then
                For IntI = 0 To oRS.RecordCount - 1
                    If oRS.Fields.Item("Provision").Value = "80C" Then
                        Count80C = 1
                        TotInvestment = oRS.Fields.Item("Amount").Value + PF
                        If oRS.Fields.Item("Limit").Value > TotInvestment Then
                            TotDeduction = TotInvestment
                        Else
                            TotDeduction = oRS.Fields.Item("Limit").Value
                        End If
                    Else
                        If oRS.Fields.Item("Limit").Value > oRS.Fields.Item("Amount").Value Then
                            OtherExcemption += oRS.Fields.Item("Amount").Value
                        Else
                            OtherExcemption += oRS.Fields.Item("Limit").Value
                        End If
                    End If
                    oRS.MoveNext()
                Next
            Else
                TotDeduction = PF
            End If

            If Count80C = 0 Then
                TotDeduction = PF
            End If

            '  TotInvestment = oRS.Fields.Item("Amount").Value

            'strSQL = "select isnull(sum(T1.U_amtte),0) Amount from [@AIS_EMPSSTP] T0 " & _
            '           " left join [@AIS_EPSSTP9]  T1 on T0.Code =T1.Code where T0.U_eid =" & EID & ""
            'oRS.DoQuery(strSQL)

            'TotPerquisites = oRS.Fields.Item("Amount").Value


            objForm.Items.Item("56").Specific.value = OverTime

            TotalTDSAmount = GrandTotalIncome - (TotDeduction + OtherExcemption + PT)

            '  TotalTDSAmount = Math.Round(TotalTDSAmount / 10, MidpointRounding.AwayFromZero) * 10

            objForm.Items.Item("96").Specific.value = TotalTDSAmount
            objForm.Items.Item("137").Specific.value = PT 'Current PT Amount 
            objForm.Items.Item("135").Specific.value = PF  'Current PF Amount 

            ' objForm.Items.Item("117").Specific.value = TotalTDSAmount

            Calculation() 'Call Salary Calculation Functions
            IncmeCalculation() ' Call Income Tab calculation Tab Function:

            objForm.Items.Item("94").Specific.value = TotDeduction

            objForm.Items.Item("117").Specific.value = Math.Round(objForm.Items.Item("117").Specific.value / 10, MidpointRounding.AwayFromZero) * 10

            TotalTDSAmount = TDSAmountBasedSlab(EID, objForm.Items.Item("117").Specific.value)

            'If Total Income below 500000 then calculate the rebate amount:
            oRS.DoQuery("select U_educess +U_heducess [EduChess],isnull(U_rebate,0)[Rebate],ISNULL( U_totlim,0)[Limit] from [@AIS_STAT] ")

            ' MsgBox(oRS.Fields.Item("Limit").Value)
            If objForm.Items.Item("117").Specific.value <= oRS.Fields.Item("Limit").Value Then

                If TotalTDSAmount <= oRS.Fields.Item("Rebate").Value Then
                    TotalTDSAmount = 0
                Else
                    TotalTDSAmount = TotalTDSAmount - oRS.Fields.Item("Rebate").Value
                End If
            End If

            objForm.Items.Item("102").Specific.value = Math.Round(TotalTDSAmount, 0)

            'Education Chess Amount Calculation:

            objForm.Items.Item("112").Specific.value = Math.Round(TotalTDSAmount * oRS.Fields.Item("EduChess").Value / 100, 0)


            'Surcharge Calaucation for TDS:
            oRS.DoQuery("select U_surcharge [Surcharge] ,U_surcglim [Limit] from [@AIS_STAT] ")
            If oRS.Fields.Item("Limit").Value > TotalTDSAmount * oRS.Fields.Item("Surcharge").Value / 100 Then
                objForm.Items.Item("110").Specific.value = Math.Round(TotalTDSAmount * oRS.Fields.Item("Surcharge").Value / 100, 0)
            Else
                objForm.Items.Item("110").Specific.value = oRS.Fields.Item("Limit").Value
            End If

            'Already Paid TDS Amount:

            strSQL = "select isnull(SUM(convert(float,U_amount) ),0)[Amount] from [@AIS_OSPS] where U_empid ='" & EID & "' " & _
                     " and U_fyear ='" & FinYear & "' and " & _
                     " U_heads =(select Name from [@AIS_DDC] where U_tds ='Y') and U_heads not in('Pay','Remarks')"

            oRS.DoQuery(strSQL)

            PaidTDS = oRS.Fields.Item("Amount").Value + objForm.Items.Item("69").Specific.value

            objForm.Items.Item("104").Specific.value = PaidTDS


        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("TDSCalcuationAmount Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
#End Region

#Region "Find TDS Amount Based on Government Slab"

    Public Function TDSAmountBasedSlab(ByVal EID As String, ByVal Amount As Double) As Double
        Try

            TDS = Amount
            TDSSlabAmt = 0
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select U_tdsslab [Slab] from OHEM where empID=" & EID & "")

            strSQL = "select T1.U_llmt [Lower Limit] ,T1.U_ulmt [Upper Limit] ,T1.U_percnt [Percent] " & _
                " from [@AIS_OTDS] T0 left join [@AIS_TDS1] T1 on T0.Code =T1.Code  where T0.U_code ='TDS-" & oRS.Fields.Item("Slab").Value & "' "

            oRS.DoQuery(strSQL)
            If oRS.RecordCount > 0 Then
                For IntI As Integer = 1 To oRS.RecordCount
                    LowerLimit = oRS.Fields.Item("Lower Limit").Value - 1
                    UpperLimit = oRS.Fields.Item("Upper Limit").Value
                    Percentage = oRS.Fields.Item("Percent").Value
                    ' If UpperLimit <= Amount And LowerLimit >= Amount Then
                    'If (Amount >= LowerLimit Or Amount <= UpperLimit) And UpperLimit <> 0.0 And (Amount > UpperLimit Or Amount > LowerLimit) Then
                    '    'TDS = TDS - LowerLimit
                    '    If UpperLimit < Amount Or UpperLimit <> 0.0 Then
                    '        If UpperLimit < Amount Then
                    '            TDS = UpperLimit - LowerLimit
                    '        Else
                    '            TDS = Amount - LowerLimit
                    '        End If
                    '    End If
                    '    TDSSlabAmt += TDS * Percentage / 100
                    'End If


                    If Amount >= LowerLimit Or Amount <= UpperLimit Then
                        If UpperLimit < Amount And UpperLimit <> 0.0 Then
                            If UpperLimit < Amount Then
                                TDS = UpperLimit - LowerLimit
                            Else
                                TDS = Amount - LowerLimit
                            End If
                            TDSSlabAmt += TDS * Percentage / 100
                        Else
                            TDS = Amount - LowerLimit
                            If TDS.ToString.Contains("-") Then
                            Else
                                TDSSlabAmt += TDS * Percentage / 100
                            End If

                        End If

                    End If
                    oRS.MoveNext()
                Next
                Return TDSSlabAmt
            Else
                Return 0
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("TDS Amount Calculation Based on Slab Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Function

#End Region

#Region "Processed Month and Salary"
    'Public Sub ProcessedSalary(ByVal FormUID As String, ByVal Year As String)
    '    Dim strSQL As String
    '    Dim objRS As SAPbobsCOM.Recordset
    '    objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
    '    strSQL = "select sum(T1.U_npay)as Amount,count(T0.U_month)as Mon from [@AIS_ALPS] T0 join [@AIS_ALPSL] T1 on T0.docentry=T1.docentry where T0.U_year='" & Year & "'"
    '    objRS.DoQuery(strSQL)
    '    objForm.Items.Item("60").Specific.value = objRS.Fields.Item("Mon").Value
    '    objForm.Items.Item("62").Specific.value = objRS.Fields.Item("Amount").Value
    'End Sub
#End Region

#Region "Delete an Empty Row"
    Public Sub DeleteEmptyRow(ByVal FormUID As String, ByVal RowNo As Integer)
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        objMatrix = objForm.Items.Item("30").Specific
        objMatrix1 = objForm.Items.Item("31").Specific
        objMatrix2 = objForm.Items.Item("32").Specific
        objMatrix3 = objForm.Items.Item("33").Specific
        objMatrix4 = objForm.Items.Item("34").Specific
        objMatrix5 = objForm.Items.Item("35").Specific
        objMatrix6 = objForm.Items.Item("36").Specific
        objMatrix7 = objForm.Items.Item("44").Specific
        objMatrix8 = objForm.Items.Item("45").Specific
        If objMatrix.Columns.Item("V_0").Cells.Item(objMatrix.VisualRowCount).Specific.value = "" Then
            objMatrix.DeleteRow(objMatrix.VisualRowCount)
        End If

        If objMatrix1.Columns.Item("V_0").Cells.Item(objMatrix1.VisualRowCount).Specific.value = "" Then
            objMatrix1.DeleteRow(objMatrix1.VisualRowCount)
        End If

        If objMatrix2.Columns.Item("V_0").Cells.Item(objMatrix2.VisualRowCount).Specific.value = "" Then
            objMatrix2.DeleteRow(objMatrix2.VisualRowCount)
        End If

        If objMatrix3.Columns.Item("V_0").Cells.Item(objMatrix3.VisualRowCount).Specific.value = "" Then
            objMatrix3.DeleteRow(objMatrix3.VisualRowCount)
        End If

        If objMatrix4.Columns.Item("V_0").Cells.Item(objMatrix4.VisualRowCount).Specific.value = "" Then
            objMatrix4.DeleteRow(objMatrix4.VisualRowCount)
        End If

        If objMatrix5.Columns.Item("V_0").Cells.Item(objMatrix5.VisualRowCount).Specific.value = "" Then
            objMatrix5.DeleteRow(objMatrix5.VisualRowCount)
        End If

        If objMatrix6.Columns.Item("V_0").Cells.Item(objMatrix6.VisualRowCount).Specific.value = "" Then
            objMatrix6.DeleteRow(objMatrix6.VisualRowCount)
        End If

        If objMatrix7.Columns.Item("V_0").Cells.Item(objMatrix7.VisualRowCount).Specific.value = "" Then
            objMatrix7.DeleteRow(objMatrix7.VisualRowCount)
        End If

        If objMatrix8.Columns.Item("V_8").Cells.Item(objMatrix8.VisualRowCount).Specific.value = "" Then
            objMatrix8.DeleteRow(objMatrix8.VisualRowCount)
        End If

    End Sub
#End Region

#Region "Check From DB Have a Record or Not"
    Public Sub CheckFromDB(ByVal FormUID As String)
        Try
            objForm = objAddOn.objApplication.Forms.Item(FormUID)
            Dim objRS As SAPbobsCOM.Recordset
            Dim strSQL As String
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strSQL = "select * from [@AIS_EMPSSTP] where U_eid='" & objForm.Items.Item("4").Specific.string & "' "
            'and U_month='" & objComboMonth.Selected.Value & "' and U_year='" & objComboYear.Selected.Value & "'"
            objRS.DoQuery(strSQL)
            If objRS.RecordCount > 0 Then
                objForm.Freeze(True)
                objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                objForm.Items.Item("129").Enabled = True
                objForm.Items.Item("129").Specific.value = objRS.Fields.Item("Code").Value
                objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                objForm.Items.Item("4").Enabled = False
                ' UpdateModeAddrow(FormUID)
                'objForm.DataSources.DBDataSources.Item("@AIS_EMPSSTP").Clear()
                'objMatrix.AddRow()
                'objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
                objForm.Freeze(False)
            End If
        Catch ex As Exception
            objForm.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage("Emp ID Checking Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub
#End Region

#Region "Update Mode Addrow Function"
    Public Sub UpdateModeAddrow(ByVal FormUID As String)

        objMatrix = objForm.Items.Item("30").Specific
        objMatrix1 = objForm.Items.Item("31").Specific
        objMatrix2 = objForm.Items.Item("32").Specific
        objMatrix3 = objForm.Items.Item("33").Specific
        objMatrix4 = objForm.Items.Item("34").Specific
        objMatrix5 = objForm.Items.Item("35").Specific
        objMatrix6 = objForm.Items.Item("36").Specific
        objMatrix7 = objForm.Items.Item("44").Specific
        objMatrix8 = objForm.Items.Item("45").Specific

        If objMatrix.Columns.Item("V_0").Cells.Item(objMatrix.RowCount).Specific.value <> "" Then
            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP1").Clear()
            objMatrix.AddRow()
            objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
        End If

        If objMatrix1.Columns.Item("V_0").Cells.Item(objMatrix1.RowCount).Specific.value <> "" Then
            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP2").Clear()
            objMatrix1.AddRow()
            objMatrix1.Columns.Item("V_-1").Cells.Item(objMatrix1.RowCount).Specific.value = objMatrix1.VisualRowCount
        End If

        If objMatrix2.Columns.Item("V_0").Cells.Item(objMatrix2.RowCount).Specific.value <> "" Then
            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP3").Clear()
            objMatrix2.AddRow()
            objMatrix2.Columns.Item("V_-1").Cells.Item(objMatrix2.RowCount).Specific.value = objMatrix2.VisualRowCount
        End If

        If objMatrix3.Columns.Item("V_0").Cells.Item(objMatrix3.RowCount).Specific.value <> "" Then
            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP4").Clear()
            objMatrix3.AddRow()
            objMatrix3.Columns.Item("V_-1").Cells.Item(objMatrix3.RowCount).Specific.value = objMatrix3.VisualRowCount
        End If

        If objMatrix4.Columns.Item("V_0").Cells.Item(objMatrix4.RowCount).Specific.value <> "" Then
            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP5").Clear()
            objMatrix4.AddRow()
            objMatrix4.Columns.Item("V_-1").Cells.Item(objMatrix4.RowCount).Specific.value = objMatrix4.VisualRowCount
        End If

        If objMatrix5.Columns.Item("V_0").Cells.Item(objMatrix5.RowCount).Specific.value <> "" Then
            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP6").Clear()
            objMatrix5.AddRow()
            objMatrix5.Columns.Item("V_-1").Cells.Item(objMatrix5.RowCount).Specific.value = objMatrix5.VisualRowCount
        End If

        If objMatrix6.Columns.Item("V_0").Cells.Item(objMatrix6.RowCount).Specific.value <> "" Then
            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP7").Clear()
            objMatrix6.AddRow()
            objMatrix6.Columns.Item("V_-1").Cells.Item(objMatrix6.RowCount).Specific.value = objMatrix6.VisualRowCount
        End If

        If objMatrix7.Columns.Item("V_0").Cells.Item(objMatrix7.RowCount).Specific.value <> "" Then
            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP8").Clear()
            objMatrix7.AddRow()
            objMatrix7.Columns.Item("V_-1").Cells.Item(objMatrix7.RowCount).Specific.value = objMatrix7.VisualRowCount
        End If

        If objMatrix8.Columns.Item("V_0").Cells.Item(objMatrix8.RowCount).Specific.value <> "" Then
            objForm.DataSources.DBDataSources.Item("@AIS_EPSSTP9").Clear()
            objMatrix8.AddRow()
            objMatrix8.Columns.Item("V_-1").Cells.Item(objMatrix8.RowCount).Specific.value = objMatrix8.VisualRowCount
        End If

    End Sub
#End Region

#Region "Maximum Days Calculation"
    Public Sub MaximumDaysFun(ByVal FormUID As String, ByVal RowNo As Integer)
        Dim objRS, objRS1 As SAPbobsCOM.Recordset
        Dim strSQL, strSQL1 As String
        objComboYear = objForm.Items.Item("128").Specific
        objMatrix5 = objForm.Items.Item("35").Specific
        objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        strSQL1 = "select T1.U_levbal from [@AIS_EMPSSTP] T0 join  [@AIS_EPSSTP6] T1 on T0.Code=T1.Code where T0.U_eid='" & objForm.Items.Item("4").Specific.string & "'and T1.U_lcode='" & objMatrix5.Columns.Item("V_0").Cells.Item(RowNo).Specific.string & "' and T0.U_year='" & objComboYear.Selected.Value & "'"
        objRS1.DoQuery(strSQL1)
        If objRS1.RecordCount > 0 Then
            objMatrix5.Columns.Item("V_3").Cells.Item(RowNo).Specific.value = objRS1.Fields.Item("U_levbal").Value
        Else
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strSQL = "select Code,U_maxypd from [@AIS_DLCODE] where Code='" & objMatrix5.Columns.Item("V_0").Cells.Item(RowNo).Specific.string & "'"
            objRS.DoQuery(strSQL)
            If objRS.RecordCount > 0 Then
                objMatrix5.Columns.Item("V_3").Cells.Item(RowNo).Specific.value = objRS.Fields.Item("U_maxypd").Value
            End If
        End If
    End Sub
#End Region

#Region " Reimbursement Line Total and Head Total"
    Public Sub ReimbLineTotal(ByVal FormUID As String, ByVal RowNo As Integer)
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        Dim a, b, total As Double
        objMatrix2 = objForm.Items.Item("32").Specific
        a = objMatrix2.Columns.Item("V_3").Cells.Item(RowNo).Specific.value
        b = objMatrix2.Columns.Item("V_2").Cells.Item(RowNo).Specific.value
        total = a - b
        objMatrix2.Columns.Item("V_9").Cells.Item(RowNo).Specific.value = total
        objForm.Refresh()
        objForm.Update()
    End Sub
    Public Sub ReimbHeadTotal()
        Dim tot As Double
        Dim i As Integer
        objMatrix2 = objForm.Items.Item("32").Specific
        For i = 1 To objMatrix2.VisualRowCount
            If CDbl(objMatrix2.Columns.Item("V_9").Cells.Item(i).Specific.value) > 0 Then
                tot = tot + objMatrix2.GetCellSpecific("V_9", i).value
                objForm.Items.Item("121").Specific.value = tot
            End If
        Next i
        objForm.Refresh()
        objForm.Update()
    End Sub
#End Region

#Region "Arrear Head Total"
    Public Sub ArrearHeadTotal()
        Dim tot As Double
        Dim i As Integer
        objMatrix3 = objForm.Items.Item("33").Specific
        For i = 1 To objMatrix3.VisualRowCount
            If CDbl(objMatrix3.Columns.Item("V_10").Cells.Item(i).Specific.value) > 0 Then
                tot = tot + objMatrix3.GetCellSpecific("V_10", i).value
                objForm.Items.Item("131").Specific.value = tot
            End If
        Next i
        objForm.Refresh()
        objForm.Update()
    End Sub
#End Region

#Region "Benefits Head Details"
    Public Sub BenefitHeadTotal()
        Dim tot As Double
        Dim i As Integer
        objMatrix4 = objForm.Items.Item("34").Specific
        For i = 1 To objMatrix4.VisualRowCount
            If CDbl(objMatrix4.Columns.Item("V_5").Cells.Item(i).Specific.value) > 0 Then
                tot = tot + objMatrix4.GetCellSpecific("V_5", i).value
                objForm.Items.Item("123").Specific.value = tot
            End If
        Next i
        objForm.Refresh()
        objForm.Update()
    End Sub
#End Region

#Region "Leave Calculation"
    Public Sub EmpLeaveCal(ByVal FormUID As String, ByVal RowNo As Integer)
        'objForm = objAddOn.objApplication.Forms.Item(FormUID)
        Dim a, b, tot As Double
        objMatrix5 = objForm.Items.Item("35").Specific
        a = objMatrix5.Columns.Item("V_3").Cells.Item(RowNo).Specific.value
        b = objMatrix5.Columns.Item("V_2").Cells.Item(RowNo).Specific.value
        tot = a - b
        objMatrix5.Columns.Item("V_1").Cells.Item(RowNo).Specific.value = tot
        objForm.Refresh()
        objForm.Update()
    End Sub
#End Region

#Region "Emp CFL"
    Public Sub EmpidCFL()
        If objDataTable Is Nothing Then
        Else
            Try
                objForm.Items.Item("4").Specific.value = objDataTable.GetValue("empID", 0)
            Catch ex As Exception
            End Try
            objForm.Items.Item("6").Specific.value = objDataTable.GetValue("firstName", 0) + " " + objDataTable.GetValue("lastName", 0)
        End If
    End Sub
    Public Sub OTCodeCFL()
        If objDataTable Is Nothing Then
        Else
            Try
                objForm.Items.Item("16").Specific.value = objDataTable.GetValue("Code", 0)
            Catch ex As Exception
            End Try
        End If
    End Sub
    Public Sub PaycodeCFL(ByVal Row As Integer)
        Dim i As Integer
        For i = 0 To objDataTable.Rows.Count - 1
            Try
                objMatrix.Columns.Item("V_0").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("Code", i)
            Catch ex As Exception
            End Try
            objMatrix.Columns.Item("V_7").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("Name", i)
            objMatrix.Columns.Item("V_6").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("U_glac", i)
            objMatrix.AddRow()
            objMatrix.Columns.Item("V_-1").Cells.Item(Row + i).Specific.value = Row + i
        Next i

    End Sub
    Public Sub DeductioncodeCFL(ByVal Row As Integer)
        Dim i As Integer
        For i = 0 To objDataTable.Rows.Count - 1
            Try
                objMatrix1 = objForm.Items.Item("31").Specific
                objMatrix1.Columns.Item("V_0").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("Code", i)
            Catch ex As Exception
            End Try
            objMatrix1.Columns.Item("V_7").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("Name", i)
            objMatrix1.Columns.Item("V_6").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("U_glac", i)
            objMatrix1.AddRow()
            objMatrix1.Columns.Item("V_-1").Cells.Item(Row + i).Specific.value = Row + i
        Next i

    End Sub
    Public Sub ReimbursementCFL(ByVal Row As Integer)
        Dim i As Integer
        For i = 0 To objDataTable.Rows.Count - 1
            Try
                objMatrix2 = objForm.Items.Item("32").Specific
                objMatrix2.Columns.Item("V_0").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("Code", i)
            Catch ex As Exception
            End Try
            objMatrix2.Columns.Item("V_7").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("Name", i)
            objMatrix2.Columns.Item("V_6").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("U_egla", i)
            objMatrix2.Columns.Item("V_5").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("U_pgla", i)
            objMatrix2.Columns.Item("V_4").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("U_limit", i)
            objMatrix2.AddRow()
            objMatrix2.Columns.Item("V_-1").Cells.Item(Row + i).Specific.value = Row + i
        Next i
    End Sub
    Public Sub BenefitcodeCFL(ByVal Row As Integer)
        Dim i As Integer
        For i = 0 To objDataTable.Rows.Count - 1
            Try
                objMatrix4 = objForm.Items.Item("34").Specific
                objMatrix4.Columns.Item("V_0").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("Code", i)
            Catch ex As Exception
            End Try
            objMatrix4.Columns.Item("V_8").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("Name", i)
            objMatrix4.Columns.Item("V_7").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("U_pgla", i)
            objMatrix4.AddRow()
            objMatrix4.Columns.Item("V_-1").Cells.Item(Row + i).Specific.value = Row + i
        Next i

    End Sub
    Public Sub LeavecodeCFL(ByVal Row As Integer)
        Dim i As Integer
        For i = 0 To objDataTable.Rows.Count - 1
            Try
                objMatrix5 = objForm.Items.Item("35").Specific
                objMatrix5.Columns.Item("V_0").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("Code", i)
            Catch ex As Exception
            End Try
            objMatrix5.Columns.Item("V_5").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("Name", i)
            objMatrix5.Columns.Item("V_3").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("U_maxypd", i)
            objMatrix5.AddRow()
            objMatrix5.Columns.Item("V_-1").Cells.Item(Row + i).Specific.value = Row + i
        Next i
    End Sub
    Public Sub LoanAdvanceCFL(ByVal Row As Integer)
        Dim i As Integer
        For i = 0 To objDataTable.Rows.Count - 1
            Try
                objMatrix6 = objForm.Items.Item("36").Specific
                objMatrix6.Columns.Item("V_0").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("Code", i)
            Catch ex As Exception
            End Try
            objMatrix6.Columns.Item("V_9").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("Name", i)
            objMatrix6.Columns.Item("V_8").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("U_type", i)
            objMatrix6.AddRow()
            objMatrix6.Columns.Item("V_-1").Cells.Item(Row + i).Specific.value = Row + i
        Next i
    End Sub
#End Region

#Region "Groupwith"
    Public Sub GroupwithFun()
        objButton = objForm.Items.Item("13").Specific
        objButton.GroupWith("14")
        objButton1 = objForm.Items.Item("14").Specific
        objButton1.GroupWith("13")
    End Sub
#End Region

#Region "Paycode Calculation"

    Public Sub PaycodeBF(ByVal Row As Integer)
        objMatrix = objForm.Items.Item("30").Specific
        objCombo = objMatrix.Columns.Item("V_5").Cells.Item(Row).Specific
        While objCombo.ValidValues.Count > 0
            objCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
        End While
        objCombo = objMatrix.Columns.Item("V_5").Cells.Item(Row).Specific
        strSQL = "select U_Paycod,Code from [@AIS_FORMULA] where U_Paycod <>''"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        For Me.intLoop = 1 To objRS.RecordCount
            objCombo.ValidValues.Add(objRS.Fields.Item("U_Paycod").Value, objRS.Fields.Item("Code").Value)
            objRS.MoveNext()
        Next intLoop
        objForm.Refresh()
    End Sub

    Public Sub PayCodeCalc(ByVal Row As Integer, ByVal ItemNO As Integer)
        Dim bamt, index1, amount As Double
        Dim strfml(), strmul(), fmla, strsql As String
        Dim Mastercode As String = ""

        'If objForm.PaneLevel = 1 Then Mastercode = "U_Paycod"
        'If objForm.PaneLevel = 2 Then Mastercode = "U_DedCod"
        'If Mastercode = "" Then Exit Sub
        'objMatrix = objForm.Items.Item(ItemNO).Specific
        'MsgBox(objMatrix.RowCount)
        ' MsgBox(objMatrix.Columns.Item("V_5").Cells.Item(Row).Specific.value)
        strsql = "select U_Frmla from [@AIS_FORMULA] where U_Paycod='" & objMatrix.Columns.Item("V_5").Cells.Item(Row).Specific.value & "'"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strsql)
        'fmla = "PF=0.12*Basic"
        Try
            fmla = objRS.Fields.Item("U_Frmla").Value.ToString
            ' fmla = strsql.Replace("%", "/100")
            ' MsgBox(fmla)
            strfml = fmla.Split("=")
            fmla = strfml(1)
            If fmla.Contains("*") Then
                strmul = fmla.Split("*")
                amount = Val(objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value)
                If IsNumeric(strmul(0)) Then
                    bamt = CDbl(strmul(0) * amount)
                    objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                Else
                    index1 = Val(strmul(1))
                    bamt = CDbl(amount * index1)
                    objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                End If
            ElseIf fmla.Contains("+") Then
                strmul = fmla.Split("+")
                amount = Val(objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value)
                If IsNumeric(strmul(0)) Then
                    bamt = CDbl(strmul(0) + amount)
                    objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                Else
                    index1 = Val(strmul(1))
                    bamt = CDbl(amount + index1)
                    objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                End If
            ElseIf fmla.Contains("-") Then
                strmul = fmla.Split("-")
                amount = Val(objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value)
                If IsNumeric(strmul(0)) Then
                    bamt = CDbl(strmul(0) - amount)
                    objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                Else
                    index1 = Val(strmul(1))
                    bamt = CDbl(amount - index1)
                    objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                End If
            ElseIf fmla.Contains("/") Then
                strmul = fmla.Split("/")
                amount = Val(objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value)
                If IsNumeric(strmul(0)) Then
                    bamt = CDbl(strmul(0) / amount)
                    objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                Else
                    index1 = Val(strmul(1))
                    bamt = CDbl(amount / index1)
                    objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

#End Region

#Region "Deductioncode Calculation"

    Public Sub DeductionBF(ByVal Row As Integer)
        objMatrix1 = objForm.Items.Item("31").Specific
        objCombo = objMatrix1.Columns.Item("V_5").Cells.Item(Row).Specific
        While objCombo.ValidValues.Count > 0
            objCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
        End While
        objCombo = objMatrix1.Columns.Item("V_5").Cells.Item(Row).Specific
        strSQL = "select  U_DedCod,Code from [@AIS_FORMULA] where U_DedCod <>''"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        For Me.intLoop = 1 To objRS.RecordCount
            objCombo.ValidValues.Add(objRS.Fields.Item("U_DedCod").Value, objRS.Fields.Item("Code").Value)
            objRS.MoveNext()
        Next intLoop
    End Sub

    Public Sub DeductnCodeCalc(ByVal Row As Integer)
        Dim bamt, index1, amount As Double
        Dim strfml(), strmul(), fmla, strsql As String

        strsql = "select U_Frmla from [@AIS_FORMULA] where U_DedCod='" & objMatrix1.Columns.Item("V_5").Cells.Item(Row).Specific.value & "'"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strsql)
        Try
            fmla = objRS.Fields.Item("U_Frmla").Value.ToString
            strfml = fmla.Split("=")
            fmla = strfml(1)
            If fmla.Contains("*") Then
                strmul = fmla.Split("*")
                amount = Val(objMatrix.Columns.Item("V_4").Cells.Item(1).Specific.value)
                If IsNumeric(strmul(0)) Then
                    bamt = CDbl(strmul(0) * amount)
                    objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                Else
                    index1 = Val(strmul(1))
                    bamt = CDbl(amount * index1)
                    objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                End If
            ElseIf fmla.Contains("+") Then
                strmul = fmla.Split("+")
                amount = Val(objMatrix.Columns.Item("V_4").Cells.Item(1).Specific.value)
                If IsNumeric(strmul(0)) Then
                    bamt = CDbl(strmul(0) + amount)
                    objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                Else
                    index1 = Val(strmul(1))
                    bamt = CDbl(amount + index1)
                    objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                End If
            ElseIf fmla.Contains("-") Then
                amount = Val(objMatrix.Columns.Item("V_4").Cells.Item(1).Specific.value)
                strmul = fmla.Split("-")
                If IsNumeric(strmul(0)) Then
                    bamt = CDbl(strmul(0) - (amount))
                    objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                Else
                    index1 = Val(strmul(1))
                    bamt = CDbl(amount - index1)
                    objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                End If
            ElseIf fmla.Contains("/") Then
                strmul = fmla.Split("/")
                amount = Val(objMatrix.Columns.Item("V_4").Cells.Item(1).Specific.value)
                If IsNumeric(strmul(0)) Then
                    bamt = CDbl(strmul(0) / amount)
                    objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                Else
                    index1 = Val(strmul(1))
                    bamt = CDbl(amount / index1)
                    objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value = bamt
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

#End Region

#Region "Reimbursehment"

    Public Sub ReimbursehmentFUN(ByVal Row As Integer)
        Dim mxamt As Double
        Dim amtclm As Double
        Dim balamt As Double
        Dim newamt As Double
        Dim newbalamt As Double
        Try
            objMatrix2 = objForm.Items.Item("32").Specific
            If Row = 1 Then
                mxamt = objMatrix2.Columns.Item("V_3").Cells.Item(Row).Specific.value
                amtclm = objMatrix2.Columns.Item("V_8").Cells.Item(Row).Specific.value
                objMatrix2.Columns.Item("V_2").Cells.Item(Row).Specific.value = mxamt
                balamt = amtclm - mxamt
                objMatrix2.Columns.Item("V_1").Cells.Item(Row).Specific.value = balamt
            Else
                balamt = objMatrix2.Columns.Item("V_1").Cells.Item(Row - 1).Specific.value
                mxamt = objMatrix2.Columns.Item("V_3").Cells.Item(Row).Specific.value
                amtclm = objMatrix2.Columns.Item("V_8").Cells.Item(Row).Specific.value
                objMatrix2.Columns.Item("V_2").Cells.Item(Row).Specific.value = mxamt
                newamt = balamt + amtclm
                newbalamt = newamt - mxamt
                objMatrix2.Columns.Item("V_1").Cells.Item(Row).Specific.value = newbalamt
            End If
        Catch ex As Exception
        End Try
    End Sub

#End Region

#Region "LoadscreenFunction"
    Public Sub LoadscreenFUN()
        Try
            If objForm.Items.Item("4").Specific.value = "" Then
                objForm.ActiveItem = 4
            End If
            objForm.Items.Item("22").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            objForm.Items.Item("21").Enabled = False
            objForm.Items.Item("30").Visible = True
            objForm.Items.Item("31").Visible = False
            objForm.Items.Item("32").Visible = False
            objForm.Items.Item("33").Visible = False
            objForm.Items.Item("34").Visible = False
            objForm.Items.Item("35").Visible = False
            objForm.Items.Item("36").Visible = False
        Catch ex As Exception
        End Try
        GroupwithFun()
        CreateSubFloder()
        PerquisiteCode()
        '  PerquisiteCAT()
        Addrow()
        ProvisionCode()
        ProvisionName()
        Provisions()
    End Sub
#End Region

#Region "Load Month and Year"
    Public Sub LoadMonthandYear()
        objCombo = objForm.Items.Item("126").Specific
        objCombo.ValidValues.Add("January", "01")
        objCombo.ValidValues.Add("February", "02")
        objCombo.ValidValues.Add("March", "03")
        objCombo.ValidValues.Add("April", "04")
        objCombo.ValidValues.Add("May", "05")
        objCombo.ValidValues.Add("June", "06")
        objCombo.ValidValues.Add("July", "07")
        objCombo.ValidValues.Add("August", "08")
        objCombo.ValidValues.Add("September", "09")
        objCombo.ValidValues.Add("October", "10")
        objCombo.ValidValues.Add("November", "11")
        objCombo.ValidValues.Add("December", "12")

        objCombo = objForm.Items.Item("128").Specific
        objCombo.ValidValues.Add("2010", "2010")
        objCombo.ValidValues.Add("2011", "2011")
        objCombo.ValidValues.Add("2012", "2012")
        objCombo.ValidValues.Add("2013", "2013")
        objCombo.ValidValues.Add("2014", "2014")
        objCombo.ValidValues.Add("2015", "2015")
        objCombo.ValidValues.Add("2016", "2016")
        objCombo.ValidValues.Add("2017", "2017")
        objCombo.ValidValues.Add("2018", "2018")
        objCombo.ValidValues.Add("2019", "2019")
        objCombo.ValidValues.Add("2020", "2020")
    End Sub
#End Region

#Region "Leavecode"
    Public Sub Absentday(ByVal Row As Integer)
        Dim maxday As Double
        Dim dayavl As Double
        Dim balday As Double
        Try
            strSQL = "select distinct A. U_absent from [@AIS_SUMATTEND1] A join [@AIS_EMPSSTP] B  on A.U_eid  =B.Code where b.U_eid = '" & objForm.Items.Item("4").Specific.value & "'"
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS.DoQuery(strSQL)
            objMatrix5.Columns.Item("V_2").Cells.Item(Row).Specific.value = objRS.Fields.Item("U_absent").Value
            dayavl = objRS.Fields.Item("U_absent").Value
            maxday = objMatrix5.Columns.Item("V_3").Cells.Item(Row).Specific.value
            balday = maxday - dayavl
            objMatrix5.Columns.Item("V_1").Cells.Item(Row).Specific.value = balday
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Leave Carry Forward"
    Public Sub LeaveCarryForward(ByVal Row As Integer)
        Dim dat As String
        Dim leave As Double
        objMatrix = objForm.Items.Item("35").Specific
        'MsgBox(Right(objForm.Items.Item("8").Specific.string, 2))
        dat = CDate(objForm.Items.Item("8").Specific.string).ToString("yyyyMMdd")
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        strSQL = "select SUM(T0.U_levbal) as Bal from [@AIS_EPSSTP6] T0 join [@AIS_EMPSSTP] T1 on T0.Code=T1.Code where T1.U_eid ='" & objForm.Items.Item("4").Specific.string & "' and U_lprdate <'" & dat & "' and T0.U_lcode ='EL' group by T1.U_eid"
        objRS.DoQuery(strSQL)
        leave = objMatrix.Columns.Item("V_3").Cells.Item(Row).Specific.value + objRS.Fields.Item("Bal").Value
        objMatrix.Columns.Item("V_3").Cells.Item(Row).Specific.value = leave
    End Sub
#End Region

#Region "Loan and Advance Calculation Function"

    Public Sub LoanAdvancFUN(ByVal Row As Integer)
        Try
            objMatrix6 = objForm.Items.Item("36").Specific
            objCombo = objMatrix6.Columns.Item("V_6").Cells.Item(Row).Specific
            While objCombo.ValidValues.Count > 0
                objCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
            End While
            objCombo = objMatrix6.Columns.Item("V_6").Cells.Item(Row).Specific
            strSQL = "select U_type,Code  from [@AIS_LONADV] "
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS.DoQuery(strSQL)
            For Me.intLoop = 1 To objRS.RecordCount
                objCombo.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("U_type").Value)
                objRS.MoveNext()
            Next intLoop
        Catch ex As Exception
        End Try
    End Sub

    Public Sub InstalmentFun(ByVal Row As Integer)
        Dim amtpay As Double
        Dim newamtpay As Double
        Dim balamtpay As Double
        Try
            objMatrix2 = objForm.Items.Item("36").Specific
            If Row = 1 Then
                amtpay = objMatrix2.Columns.Item("V_2").Cells.Item(Row).Specific.value
                objMatrix2.Columns.Item("V_1").Cells.Item(Row).Specific.value = amtpay
            Else
                balamtpay = objMatrix2.Columns.Item("V_1").Cells.Item(Row - 1).Specific.value
                amtpay = objMatrix2.Columns.Item("V_2").Cells.Item(Row).Specific.value
                newamtpay = balamtpay + amtpay
                objMatrix2.Columns.Item("V_1").Cells.Item(Row).Specific.value = newamtpay
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Link Button Functions to all Tabs"

    Public Sub Paylink(ByVal Row As Integer)
        Try
            Dim objForm1 As SAPbouiCOM.Form
            objForm1 = objAddOn.objApplication.Forms.ActiveForm
            objForm1.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            objForm1.Items.Item("6").Enabled = True
            objForm1.Items.Item("6").Specific.value = objMatrix.Columns.Item("V_0").Cells.Item(Row).Specific.value
            objForm1.Items.Item("1").Click()
            objForm1.Items.Item("6").Enabled = False
        Catch ex As Exception
        End Try
    End Sub
    Public Sub DeductLink(ByVal Row As Integer)
        Try
            Dim objForm1 As SAPbouiCOM.Form
            objForm1 = objAddOn.objApplication.Forms.ActiveForm
            objForm1.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            objForm1.Items.Item("6").Enabled = True
            objForm1.Items.Item("6").Specific.value = objMatrix1.Columns.Item("V_0").Cells.Item(Row).Specific.value
            objForm1.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            'objForm1.Items.Item("6").Enabled = False
            'objForm1.Items.Item("9").Enabled = True
        Catch ex As Exception
        End Try
    End Sub
    Public Sub ReimburshmentLink(ByVal Row As Integer)
        Try
            Dim objForm1 As SAPbouiCOM.Form
            objForm1 = objAddOn.objApplication.Forms.ActiveForm
            objForm1.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            objForm1.Items.Item("4").Enabled = True
            objForm1.Items.Item("4").Specific.value = objMatrix2.Columns.Item("V_0").Cells.Item(Row).Specific.value
            objForm1.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            objForm1.Items.Item("4").Enabled = False
        Catch ex As Exception
        End Try
    End Sub
    Public Sub BenefitLink(ByVal Row As Integer)
        Try
            Dim objForm1 As SAPbouiCOM.Form
            objForm1 = objAddOn.objApplication.Forms.ActiveForm
            objForm1.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            objForm1.Items.Item("4").Enabled = True
            objForm1.Items.Item("4").Specific.value = objMatrix4.Columns.Item("V_0").Cells.Item(Row).Specific.value
            objForm1.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            objForm1.Items.Item("4").Enabled = False
        Catch ex As Exception
        End Try
    End Sub
    Public Sub LeaveLink(ByVal Row As Integer)
        Try
            Dim objForm1 As SAPbouiCOM.Form
            objForm1 = objAddOn.objApplication.Forms.ActiveForm
            objForm1.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            objForm1.Items.Item("6").Enabled = True
            objForm1.Items.Item("6").Specific.value = objMatrix5.Columns.Item("V_0").Cells.Item(Row).Specific.value
            objForm1.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            objForm1.Items.Item("6").Enabled = False
        Catch ex As Exception
        End Try
    End Sub
    Public Sub LoanadvanceLink(ByVal Row As Integer)
        Try
            Dim objForm1 As SAPbouiCOM.Form
            objForm1 = objAddOn.objApplication.Forms.ActiveForm
            objForm1.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            objForm1.Items.Item("4").Enabled = True
            objForm1.Items.Item("4").Specific.value = objMatrix6.Columns.Item("V_0").Cells.Item(Row).Specific.value
            objForm1.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            objForm1.Items.Item("4").Enabled = False
        Catch ex As Exception
        End Try
    End Sub
    Public Sub PerquisiteCodeLink()
        Dim intLoop As Integer
        Dim Tablename As String
        For intLoop = 0 To objAddOn.objApplication.Menus.Item("51200").SubMenus.Count - 1
            Tablename = objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).String
            Tablename = Tablename.Remove(Tablename.IndexOf("-"))
            If Trim(Tablename) = "AIS_PRQSCDE" Then
                objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).Activate()
                Exit For
            End If
        Next intLoop
    End Sub
    Public Sub ProvisionCodeLink()
        Dim intLoop As Integer
        Dim Tablename As String
        For intLoop = 0 To objAddOn.objApplication.Menus.Item("51200").SubMenus.Count - 1
            Tablename = objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).String
            Tablename = Tablename.Remove(Tablename.IndexOf("-"))
            If Trim(Tablename) = "AIS_PRVMST" Then
                objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).Activate()
                Exit For
            End If
        Next intLoop
    End Sub
#End Region

#Region "AddRow"
    Public Sub Addrow()
        Try
            objMatrix1 = objForm.Items.Item("30").Specific
            objMatrix2 = objForm.Items.Item("31").Specific
            objMatrix3 = objForm.Items.Item("32").Specific
            objMatrix4 = objForm.Items.Item("33").Specific
            objMatrix5 = objForm.Items.Item("34").Specific
            objMatrix6 = objForm.Items.Item("35").Specific
            objMatrix7 = objForm.Items.Item("36").Specific

            If Not objMatrix1.VisualRowCount > 0 Then
                objMatrix1.AddRow()
                objMatrix1.Columns.Item("V_-1").Cells.Item(objMatrix1.RowCount).Specific.value = objMatrix1.VisualRowCount
            End If
            If Not objMatrix2.VisualRowCount > 0 Then
                objMatrix2.AddRow()
                objMatrix2.Columns.Item("V_-1").Cells.Item(objMatrix2.RowCount).Specific.value = objMatrix2.VisualRowCount
            End If
            If Not objMatrix3.VisualRowCount > 0 Then
                objMatrix3.AddRow()
                objMatrix3.Columns.Item("V_-1").Cells.Item(objMatrix3.RowCount).Specific.value = objMatrix3.VisualRowCount
            End If
            If Not objMatrix4.VisualRowCount > 0 Then
                objMatrix4.AddRow()
                objMatrix4.Columns.Item("V_-1").Cells.Item(objMatrix4.RowCount).Specific.value = objMatrix4.VisualRowCount
            End If
            If Not objMatrix5.VisualRowCount > 0 Then
                objMatrix5.AddRow()
                objMatrix5.Columns.Item("V_-1").Cells.Item(objMatrix5.RowCount).Specific.value = objMatrix5.VisualRowCount
            End If
            If Not objMatrix6.VisualRowCount > 0 Then
                objMatrix6.AddRow()
                objMatrix6.Columns.Item("V_-1").Cells.Item(objMatrix6.RowCount).Specific.value = objMatrix6.VisualRowCount
            End If
            If Not objMatrix7.VisualRowCount > 0 Then
                objMatrix7.AddRow()
                objMatrix7.Columns.Item("V_-1").Cells.Item(objMatrix7.RowCount).Specific.value = objMatrix7.VisualRowCount
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Doc Total"
    Public Sub DocTotal1()

        Dim tot As Double
        Dim i As Integer
        objMatrix = objForm.Items.Item("30").Specific
        Try
            For i = 1 To objMatrix.VisualRowCount
                If objMatrix.Columns.Item("V_4").Cells.Item(i).Specific.value > 0 Then
                    tot = tot + objMatrix.GetCellSpecific("V_4", i).value
                    objForm.Items.Item("40").Specific.value = tot
                End If
            Next i
            objForm.Refresh()
        Catch ex As Exception
        End Try
    End Sub
    Public Sub DocTotal2()
        Dim tot As Double
        Dim i As Integer
        Try
            For i = 1 To objMatrix1.VisualRowCount
                If objMatrix1.Columns.Item("V_4").Cells.Item(i).Specific.value > 0 Then
                    tot = tot + objMatrix1.GetCellSpecific("V_4", i).value
                    objForm.Items.Item("119").Specific.value = tot
                End If
            Next i
            objForm.Items.Item("119").Specific.value = tot
            objForm.Refresh()
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Nodues"
    Public Sub Nodues()
        Dim nodues, strSQL1, nodues1, strcmp As String
        Dim objRS1 As SAPbobsCOM.Recordset
        strSQL1 = "select convert(varchar(8),getdate(),5)[Sysdate]"
        objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS1.DoQuery(strSQL1)
        nodues1 = objRS1.Fields.Item("Sysdate").Value
        strSQL = "select convert(varchar(8),U_rldate,5)[Rldate] from OHEM where U_rldate <>'' and empID ='" & objForm.Items.Item("4").Specific.value & "'"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        nodues = objRS.Fields.Item("Rldate").Value
        strcmp = StrComp(nodues, nodues1)
        If strcmp = 0 Then
            objForm.Items.Item("21").Enabled = True
        Else
            objForm.Items.Item("21").Enabled = False
        End If
    End Sub
#End Region

#Region "Create Sub Floder"
    Public Sub CreateSubFloder()
        objCombo = objForm.Items.Item("43").Specific
        objCombo.ValidValues.Add("INVESTMENT", "Investment")
        objCombo.ValidValues.Add("PERQUISITES", "Perquisites")
        objCombo.ValidValues.Add("SALARY", "Salary")
        objCombo.ValidValues.Add("INCOMEDETAILS", "IncomeDetails")
    End Sub
#End Region

#Region "Investment Tab"
    Public Sub Investment()
        objForm.PaneLevel = 9
        objForm.Items.Item("43").Visible = True
        objForm.Items.Item("44").Visible = True
        objMatrix7 = objForm.Items.Item("44").Specific
        If objMatrix7.VisualRowCount = 0 Then
            objMatrix7.AddRow()
            objMatrix7.Columns.Item("V_-1").Cells.Item(objMatrix7.RowCount).Specific.value = objMatrix7.VisualRowCount
            ProvisionCode()
        End If
    End Sub
#End Region

#Region "Investment Tab FMS for Provision Code,Name and Provisions"
    Public Sub ProvisionCodeOld()
        Dim queryid As Integer
        Dim objquery As SAPbobsCOM.UserQueries
        Dim str As String
        Dim lretcode As Long
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objquery = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserQueries)
        Try
            objRS.DoQuery("select * from OUQR where Qname ='provisioncode'")
            If objRS.RecordCount = 0 Then
                objquery.Query = "select code from [@AIS_PRVMSTR]"
                objquery.QueryDescription = "provisioncode"
                objquery.QueryCategory = "-1"
                lretcode = objquery.Add
                If lretcode <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                Else
                End If
            End If
            str = "select QueryID from CSHS where FormID='empsalsetup'and ItemID='44'and (ColID='V_2')"
            objRS.DoQuery(str)
            If objRS.RecordCount > 0 Then
                Exit Sub
            Else
                objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                objRS.DoQuery("select IntrnalKey from OUQR where Qname ='provisioncode'")
                queryid = objRS.Fields.Item("IntrnalKey").Value
                Dim objFMS As SAPbobsCOM.FormattedSearches
                objFMS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oFormattedSearches)
                objFMS.FormID = "empsalsetup"
                objFMS.ColumnID = "V_2"
                objFMS.ItemID = 44
                objFMS.Action = SAPbobsCOM.BoFormattedSearchActionEnum.bofsaQuery
                objFMS.QueryID = queryid
                lretcode = objFMS.Add
                If lretcode <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub


    Public Sub ProvisionCode()
        Dim queryid As Integer
        Dim objquery As SAPbobsCOM.UserQueries
        Dim str As String
        Dim lretcode As Long
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objquery = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserQueries)
        Try
            objRS.DoQuery("select * from OUQR where Qname ='Provision_Code'")
            If objRS.RecordCount = 0 Then
                objquery.Query = "SELECT Code from [@AIS_INVST]"
                objquery.QueryDescription = "Provision_Code"
                objquery.QueryCategory = "-1"
                lretcode = objquery.Add
                If lretcode <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                Else
                End If
            End If
            str = "select QueryID from CSHS where FormID='empsalsetup'and ItemID='44'and (ColID='V_5') and QueryId is not null"
            objRS.DoQuery(str)
            If objRS.RecordCount > 0 Then
                Exit Sub
            Else
                objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                objRS.DoQuery("select IntrnalKey from OUQR where Qname ='Provision_Code'")
                queryid = objRS.Fields.Item("IntrnalKey").Value
                Dim objFMS As SAPbobsCOM.FormattedSearches
                objFMS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oFormattedSearches)
                objFMS.FormID = "empsalsetup"
                objFMS.ColumnID = "V_5"
                objFMS.ItemID = 44
                objFMS.Action = SAPbobsCOM.BoFormattedSearchActionEnum.bofsaQuery
                objFMS.QueryID = queryid
                GC.Collect()
                lretcode = objFMS.Add
                If lretcode <> 0 Then
                    '  MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub


    Public Sub ProvisionName()
        Dim queryid As Integer
        Dim objquery As SAPbobsCOM.UserQueries
        Dim str As String
        Dim lretcode As Long
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objquery = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserQueries)
        Try
            objRS.DoQuery("select * from OUQR where Qname ='Provision_Name'")
            If objRS.RecordCount = 0 Then
                objquery.Query = "SELECT Name from [@AIS_INVST]  where code =$[$44.V_5.string]"
                objquery.QueryDescription = "Provision_Name"
                objquery.QueryCategory = "-1"
                lretcode = objquery.Add
                If lretcode <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                Else
                End If
            End If
            str = "select QueryID from CSHS where FormID='empsalsetup'and ItemID='44'and (ColID='V_4')  and QueryId is not null"
            objRS.DoQuery(str)
            If objRS.RecordCount > 0 Then
                Exit Sub
            Else
                objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                objRS.DoQuery("select IntrnalKey from OUQR where Qname ='Provision_Name'")
                queryid = objRS.Fields.Item("IntrnalKey").Value
                Dim objFMS As SAPbobsCOM.FormattedSearches
                objFMS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oFormattedSearches)
                objFMS.FormID = "empsalsetup"
                objFMS.ColumnID = "V_4"
                objFMS.ItemID = 44
                objFMS.Action = SAPbobsCOM.BoFormattedSearchActionEnum.bofsaQuery
                objFMS.QueryID = queryid
                '   objFMS.Refresh = SAPbobsCOM.BoYesNoEnum.tYES
                lretcode = objFMS.Add
                If lretcode <> 0 Then
                    '  MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Provisions()
        Dim queryid As Integer
        Dim objquery As SAPbobsCOM.UserQueries
        Dim str As String
        Dim lretcode As Long
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objquery = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserQueries)
        Try
            objRS.DoQuery("select * from OUQR where Qname ='Provisions'")
            If objRS.RecordCount = 0 Then
                objquery.Query = "SELECT U_prvsn from [@AIS_INVST]  where code =$[$44.V_5.string]"
                objquery.QueryDescription = "Provisions"
                objquery.QueryCategory = "-1"
                lretcode = objquery.Add
                If lretcode <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                Else
                End If
            End If
            str = "select QueryID from CSHS where FormID='empsalsetup'and ItemID='44'and (ColID='V_2') and QueryId is not null"
            objRS.DoQuery(str)
            If objRS.RecordCount > 0 Then
                Exit Sub
            Else
                objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                objRS.DoQuery("select IntrnalKey from OUQR where Qname ='Provisions'")
                queryid = objRS.Fields.Item("IntrnalKey").Value
                Dim objFMS As SAPbobsCOM.FormattedSearches
                objFMS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oFormattedSearches)
                objFMS.FormID = "empsalsetup"
                objFMS.ColumnID = "V_2"
                objFMS.ItemID = 44
                objFMS.Action = SAPbobsCOM.BoFormattedSearchActionEnum.bofsaQuery
                objFMS.QueryID = queryid
                lretcode = objFMS.Add
                If lretcode <> 0 Then
                    '   MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub


#End Region

#Region "Perquisites Tab"
    Public Sub Perquisites(ByVal FormUID As String, ByVal RowNo As Integer)
        objForm.PaneLevel = 10
        objForm.Items.Item("43").Visible = True
        objForm.Items.Item("45").Visible = True
        objMatrix8 = objForm.Items.Item("45").Specific
        If objMatrix8.VisualRowCount = 0 Then
            objMatrix8.AddRow()
            objMatrix8.Columns.Item("V_-1").Cells.Item(objMatrix8.RowCount).Specific.value = objMatrix8.VisualRowCount
            PerquisiteTab(FormUID, RowNo)
        End If
    End Sub
#End Region

#Region "Perquisite Tab"
    Public Sub PerquisiteTab(ByVal FormUID As String, ByVal RowNo As Integer)
        Dim objRS As SAPbobsCOM.Recordset
        Dim strSQL As String
        Dim i As Integer
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        strSQL = "select Code,Name from [@AIS_PRQSCDE]"
        objRS.DoQuery(strSQL)
        If objRS.RecordCount > 0 Then
            For i = 1 To objRS.RecordCount
                objMatrix8 = objForm.Items.Item("45").Specific
                objCombo = objMatrix8.Columns.Item("V_8").Cells.Item(objMatrix8.RowCount).Specific
                objCombo.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
                objRS.MoveNext()
            Next i
        End If

    End Sub
#End Region

#Region "Amount of Taxable Perquisite"
    Public Sub AmountofTax(ByVal Row As Integer)
        Try
            objMatrix8 = objForm.Items.Item("45").Specific
            valofper = objMatrix8.Columns.Item("V_5").Cells.Item(Row).Specific.value
            amtpaid = objMatrix8.Columns.Item("V_4").Cells.Item(Row).Specific.value
            amttax = (valofper - amtpaid)
            objMatrix8.Columns.Item("V_2").Cells.Item(Row).Specific.value = amttax
            objForm.Update()
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Perquisite FMS"
    Public Sub PerquisiteCode()
        Dim queryid As Integer
        Dim objquery As SAPbobsCOM.UserQueries
        Dim str As String
        Dim lretcode As Long
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objquery = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserQueries)
        Try
            objRS.DoQuery("select * from OUQR where Qname ='Perquisitecode'")
            'MsgBox(objRS.RecordCount)
            If objRS.RecordCount = 0 Then
                objquery.Query = "select Code from [@AIS_PRQSCDE]"
                objquery.QueryDescription = "Perquisitecode"
                objquery.QueryCategory = "-1"
                lretcode = objquery.Add
                If lretcode <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                Else
                End If
            End If
            str = "select QueryID from CSHS where FormID='empsalsetup'and ItemID='45'and (ColID='V_8')"
            objRS.DoQuery(str)

            If objRS.RecordCount > 0 Then
                Exit Sub
            Else
                objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                objRS.DoQuery("select IntrnalKey from OUQR where Qname ='Perquisitecode'")
                queryid = objRS.Fields.Item("IntrnalKey").Value
                Dim objFMS As SAPbobsCOM.FormattedSearches
                objFMS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oFormattedSearches)
                objFMS.FormID = "empsalsetup"
                objFMS.ColumnID = "V_8"
                objFMS.ItemID = 45
                objFMS.Action = SAPbobsCOM.BoFormattedSearchActionEnum.bofsaQuery
                objFMS.QueryID = queryid
                lretcode = objFMS.Add
                If lretcode <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Public Sub PerquisiteDES(ByVal Row As Integer)
        objMatrix8 = objForm.Items.Item("45").Specific
        strSQL = "select U_descrptn ,U_category  from [@AIS_PRQSCDE] where Code = '" & objMatrix8.Columns.Item("V_8").Cells.Item(Row).Specific.value & "'"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        Try
            For intLoop = 1 To objMatrix8.VisualRowCount
                objMatrix8.Columns.Item("V_7").Cells.Item(Row).Specific.value = objRS.Fields.Item("U_descrptn").Value
                objMatrix8.Columns.Item("V_6").Cells.Item(Row).Specific.value = objRS.Fields.Item("U_category").Value
            Next intLoop
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Salary Tab"
    Public Sub SalaryTab()
        objForm.PaneLevel = 11
        objForm.Items.Item("43").Visible = True
        objForm.Items.Item("46").Visible = True
        objForm.Items.Item("47").Visible = True
        objForm.Items.Item("48").Visible = True
        objForm.Items.Item("49").Visible = True
        objForm.Items.Item("50").Visible = True
        objForm.Items.Item("51").Visible = True
        objForm.Items.Item("52").Visible = True
        objForm.Items.Item("53").Visible = True
        objForm.Items.Item("54").Visible = True
        objForm.Items.Item("55").Visible = True
        objForm.Items.Item("56").Visible = True
        objForm.Items.Item("57").Visible = True
        objForm.Items.Item("58").Visible = True
        objForm.Items.Item("59").Visible = True
        objForm.Items.Item("60").Visible = True
        objForm.Items.Item("61").Visible = True
        objForm.Items.Item("62").Visible = True
        objForm.Items.Item("63").Visible = True
        objForm.Items.Item("64").Visible = True
        objForm.Items.Item("65").Visible = True
        objForm.Items.Item("66").Visible = True
        objForm.Items.Item("67").Visible = True
        objForm.Items.Item("68").Visible = True
        objForm.Items.Item("69").Visible = True
        objForm.Items.Item("70").Visible = True
        objForm.Items.Item("71").Visible = True
        objForm.Items.Item("72").Visible = True
        objForm.Items.Item("73").Visible = True
        objForm.Items.Item("74").Visible = True
        objForm.Items.Item("75").Visible = True
        objForm.Items.Item("76").Visible = True
    End Sub
#End Region

#Region "Salary for month"
    Public Sub MonthSalary(ByVal Row As Integer)
        Dim total As Double
        Dim mon, processed, TotMon As Integer
        Dim objRS As SAPbobsCOM.Recordset
        Dim strSQL As String
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

        mon = objForm.Items.Item("54").Specific.value
        processed = objForm.Items.Item("60").Specific.value
        TotMon = mon + processed
        If TotMon <= 12 Then
            strSQL = "select salary from OHEM where empID='" & objForm.Items.Item("4").Specific.string & "'"
            objRS.DoQuery(strSQL)
            total = objForm.Items.Item("54").Specific.value * objRS.Fields.Item("salary").Value
            objForm.Items.Item("47").Specific.value = total
        Else
            objAddOn.objApplication.SetStatusBarMessage("Please Check the Month", SAPbouiCOM.BoMessageTime.bmt_Short, True)

        End If



        'objMatrix = objForm.Items.Item("30").Specific
        ''If objMatrix.RowCount >= 1 Then
        ''    bamt = objMatrix.Columns.Item("V_4").Cells.Item(1).Specific.value
        ''    Month = objForm.Items.Item("54").Specific.value
        ''    total = (Month * bamt)
        ''    objForm.Items.Item("47").Specific.value = total
        ''    objMatrix8 = objForm.Items.Item("45").Specific
        ''    valofperste = objMatrix8.Columns.Item("V_2").Cells.Item(1).Specific.value
        ''    objForm.Items.Item("53").Specific.value = valofperste
        'End If
    End Sub

    Public Sub Calculation()
        Dim salmnth, hra, conveyence, valofper, proftax, others, pcdsalary, mon, toatalincme As Double
        salmnth = objForm.Items.Item("47").Specific.value
        mon = objForm.Items.Item("54").Specific.value
        pcdsalary = (salmnth * mon)
        hra = objForm.Items.Item("49").Specific.value
        conveyence = objForm.Items.Item("51").Specific.value
        valofper = objForm.Items.Item("53").Specific.value
        proftax = objForm.Items.Item("56").Specific.value
        others = objForm.Items.Item("58").Specific.value
        pcdsalary = objForm.Items.Item("62").Specific.value
        toatalincme = (salmnth + pcdsalary + conveyence + valofper + others + proftax) - hra
        'objForm.Items.Item("62").Specific.value = toatalincme
        objForm.Items.Item("64").Specific.value = toatalincme
        objForm.Items.Item("78").Specific.value = toatalincme
        ' objForm.Items.Item("60").Specific.value = (objForm.Items.Item("62").Specific.string / objForm.Items.Item("54").Specific.string)
    End Sub
#End Region

#Region "Income Details Tab"
    Public Sub IncomeDetails()
        objForm.PaneLevel = 12
        objForm.Items.Item("43").Visible = True
        objForm.Items.Item("77").Visible = True
        objForm.Items.Item("78").Visible = True
        objForm.Items.Item("79").Visible = True
        objForm.Items.Item("80").Visible = True
        objForm.Items.Item("81").Visible = True
        objForm.Items.Item("82").Visible = True
        objForm.Items.Item("83").Visible = True
        objForm.Items.Item("84").Visible = True
        objForm.Items.Item("85").Visible = True
        objForm.Items.Item("86").Visible = True
        objForm.Items.Item("87").Visible = True
        objForm.Items.Item("88").Visible = True
        objForm.Items.Item("89").Visible = True
        objForm.Items.Item("90").Visible = True
        objForm.Items.Item("91").Visible = True
        objForm.Items.Item("92").Visible = True
        objForm.Items.Item("93").Visible = True
        objForm.Items.Item("94").Visible = True
        objForm.Items.Item("95").Visible = True
        objForm.Items.Item("96").Visible = True
        objForm.Items.Item("97").Visible = True
        objForm.Items.Item("98").Visible = True
        objForm.Items.Item("99").Visible = True
        objForm.Items.Item("100").Visible = True
        objForm.Items.Item("101").Visible = True
        objForm.Items.Item("102").Visible = True
        objForm.Items.Item("103").Visible = True
        objForm.Items.Item("104").Visible = True
        objForm.Items.Item("105").Visible = True
        objForm.Items.Item("106").Visible = True
        objForm.Items.Item("107").Visible = True
        objForm.Items.Item("108").Visible = True
        objForm.Items.Item("109").Visible = True
        objForm.Items.Item("110").Visible = True
        objForm.Items.Item("111").Visible = True
        objForm.Items.Item("112").Visible = True
        objForm.Items.Item("113").Visible = True
        objForm.Items.Item("114").Visible = True
        objForm.Items.Item("115").Visible = True
        objForm.Items.Item("116").Visible = True
        objForm.Items.Item("117").Visible = True
    End Sub
#End Region

#Region "Income Details Calculation"
    Public Sub ICMESalary()
        Dim income As Double
        income = objForm.Items.Item("64").Specific.value
        objForm.Items.Item("78").Specific.value = income
    End Sub

    Public Sub IncmeCalculation()
        Dim Salary, House, Business, CapitalGain, OtherSource, ClaimedExempt, OtherDed, EducationChess, SurCharge, TaxableAmt As Double
        Dim ClaimReimb, Disburse As Double

        Salary = objForm.Items.Item("78").Specific.string
        House = objForm.Items.Item("80").Specific.string
        Business = objForm.Items.Item("82").Specific.string
        CapitalGain = objForm.Items.Item("84").Specific.string
        OtherSource = objForm.Items.Item("86").Specific.string
        ClaimedExempt = objForm.Items.Item("88").Specific.string
        OtherDed = objForm.Items.Item("90").Specific.string

        objForm.Items.Item("92").Specific.value = (Salary + House + Business + CapitalGain + OtherSource + ClaimedExempt) - OtherDed
        'objForm.Items.Item("96").Specific.value = objForm.Items.Item("92").Specific.value
        'objForm.Items.Item("117").Specific.value = objForm.Items.Item("92").Specific.value
        SurCharge = objForm.Items.Item("110").Specific.value
        'TDS Calculation:

        ClaimReimb = objForm.Items.Item("98").Specific.value
        Disburse = objForm.Items.Item("100").Specific.value
        TaxableAmt = objForm.Items.Item("96").Specific.value

        objForm.Items.Item("117").Specific.value = IIf((House + Business + CapitalGain + OtherSource + ClaimedExempt + ClaimReimb + Disburse + TaxableAmt) - OtherDed > 0, (House + Business + CapitalGain + OtherSource + ClaimedExempt + ClaimReimb + Disburse + TaxableAmt) - OtherDed, 0)

        'TotalTDSAmount = objForm.Items.Item("117").Specific.value

        'TotalTDSAmount = TDSAmountBasedSlab(FormUID, TotalTDSAmount)

        'objForm.Items.Item("102").Specific.value = TotalTDSAmount

        ''Education Chess Amount Calculation:
        'oRS.DoQuery("select U_educess +U_heducess [EduChess] from [@AIS_STAT] ")
        'objForm.Items.Item("112").Specific.value = TotalTDSAmount * oRS.Fields.Item("EduChess").Value / 100

        ''Surcharge Calaucation for TDS:
        'oRS.DoQuery("select U_surcharge [Surcharge] ,U_surcglim [Limit] from [@AIS_STAT] ")
        'If oRS.Fields.Item("Limit").Value > TotalTDSAmount * oRS.Fields.Item("Surcharge").Value / 100 Then
        '    objForm.Items.Item("110").Specific.value = TotalTDSAmount * oRS.Fields.Item("Surcharge").Value / 100
        'Else
        '    objForm.Items.Item("110").Specific.value = oRS.Fields.Item("Limit").Value
        'End If

        ''Already Paid TDS Amount:

        'strSQL = "select isnull(SUM(convert(float,U_amount) ),0)[Amount] from [@AIS_OSPS] where U_empid ='" & EID & "' " & _
        '    " and U_month >='" & SMonth & "' and U_month <='" & EMonth & "' and U_year >='" & SYear & "' and U_year <='" & EYear & "' and " & _
        '    " U_heads ='TDS' and U_heads not in('Pay','Remarks')"

        'oRS.DoQuery(strSQL)

        'PaidTDS = oRS.Fields.Item("Amount").Value

        'objForm.Items.Item("104").Specific.value = PaidTDS



        EducationChess = objForm.Items.Item("112").Specific.value
        objForm.Items.Item("108").Specific.value = Replace(((objForm.Items.Item("102").Specific.value + EducationChess + SurCharge) - objForm.Items.Item("104").Specific.value), "-", "")


        If TDSSetting = "Y" Then
            objForm.Items.Item("132").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
        End If

    End Sub

#End Region

#Region "Update Current TDS Value to Particular Employee"
    Sub UpdateTDSAmount(ByVal FormUID As String)
        Try
            Dim MonthTDS As Double
            MonthTDS = objForm.Items.Item("108").Specific.string / Count
            objMatrix2 = objForm.Items.Item("31").Specific
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strSQL = "update [@AIS_EPSSTP2] set U_amt ='" & MonthTDS & "' where U_descrpn =(select Name from [@AIS_DDC] where U_tds ='Y') " & _
                " and Code =(select Code from [@AIS_EMPSSTP] where U_eid ='" & objForm.Items.Item("4").Specific.string & "')"
            oRS.DoQuery(strSQL)

            oRS.DoQuery("select Name from [@AIS_DDC] where U_tds ='Y'")
            For IntI As Integer = 1 To objMatrix2.RowCount
                'MsgBox(objMatrix2.Columns.Item("V_7").Cells.Item(IntI).Specific.string)
                If objMatrix2.Columns.Item("V_7").Cells.Item(IntI).Specific.string = oRS.Fields.Item("Name").Value Then
                    objMatrix2.Columns.Item("V_4").Cells.Item(IntI).Specific.value = MonthTDS
                Else
                    '  objAddOn.objApplication.SetStatusBarMessage("Please Select TDS from Deduction Master", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                End If
            Next
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("UpdateTDSAmount Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
#End Region

#Region "Header Validation"
    Public Function HeaderValidateFUN() As Boolean
        If objForm.Items.Item("4").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Employee ID is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf ExistDocument(objForm.Items.Item("4").Specific.string) = True Then
            objAddOn.objApplication.SetStatusBarMessage("Already Exists.", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
            'ElseIf objForm.Items.Item("128").Specific.Value = "" Then
            '    objAddOn.objApplication.SetStatusBarMessage("Plese Select Year", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '    Return False
        End If
        Return True
    End Function
#End Region

#Region "Checking Already Exist or Not "
    Function ExistDocument(ByVal EmpID As String) As Boolean
        Try
            Dim objRS5 As SAPbobsCOM.Recordset
            Dim Query As String
            objRS5 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Query = "select Code from [@AIS_EMPSSTP] where U_eid ='" & EmpID & "'"
            objRS5.DoQuery(Query)
            If objRS5.RecordCount > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Exist Document Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
            Return False
        End Try
    End Function
#End Region

#Region "Line Validation"
    Public Function LineValidation()
        Month = objForm.Items.Item("54").Specific.value
        If 12 >= Month Then
        Else
            objAddOn.objApplication.SetStatusBarMessage("Total no.of months should not be greater than 12", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Matrix Validation"
    Public Function MatrixValidationFun()
        objMatrix = objForm.Items.Item("30").Specific
        If objMatrix.VisualRowCount > 0 Then
            For Me.intLoop = 1 To objMatrix.VisualRowCount - 1
                If objMatrix.Columns.Item("V_0").Cells.Item(intLoop).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage(" Enter the Paycode at line- " & intLoop, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objForm.Items.Item("4").Specific.Value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Employee ID is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            Next intLoop
            If objMatrix.Columns.Item("V_0").Cells.Item(objMatrix.VisualRowCount).Specific.value = "" Then
                objMatrix.DeleteRow(objMatrix.VisualRowCount)
            End If
            'If objMatrix.VisualRowCount = 0 Then
            '    objMatrix.AddRow()
            '    objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.VisualRowCount).Specific.String = objMatrix.VisualRowCount
            '    objAddOn.objApplication.SetStatusBarMessage("Please Enter Atleast One Row", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '    Return False
            'End If
        End If
        Return True
    End Function
    Public Function Matrix1ValidationFun()
        objMatrix1 = objForm.Items.Item("31").Specific
        If objMatrix1.VisualRowCount > 0 Then
            For Me.intLoop = 1 To objMatrix1.VisualRowCount - 1
                If objMatrix1.Columns.Item("V_0").Cells.Item(intLoop).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage(" Enter the Paycode at line- " & intLoop, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            Next intLoop
            If objMatrix1.Columns.Item("V_0").Cells.Item(objMatrix1.VisualRowCount).Specific.value = "" Then
                objMatrix1.DeleteRow(objMatrix1.VisualRowCount)
            End If
            'If objMatrix1.VisualRowCount = 0 Then
            '    objMatrix1.AddRow()
            '    objMatrix1.Columns.Item("V_-1").Cells.Item(objMatrix1.VisualRowCount).Specific.String = objMatrix1.VisualRowCount
            '    objAddOn.objApplication.SetStatusBarMessage("Please Enter Atleast One Row ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '    Return False
            'End If
        End If
        Return True
    End Function
    Public Function Matrix2ValidationFun()
        objMatrix2 = objForm.Items.Item("32").Specific
        If objMatrix2.VisualRowCount > 0 Then
            For Me.intLoop = 1 To objMatrix2.VisualRowCount - 1
                If objMatrix2.Columns.Item("V_0").Cells.Item(intLoop).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage(" Enter the Deduction at line- " & intLoop, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            Next intLoop
            If objMatrix2.Columns.Item("V_0").Cells.Item(objMatrix2.VisualRowCount).Specific.value = "" Then
                objMatrix2.DeleteRow(objMatrix2.VisualRowCount)
            End If
            'If objMatrix2.VisualRowCount = 0 Then
            '    objMatrix2.AddRow()
            '    objMatrix2.Columns.Item("V_-1").Cells.Item(objMatrix2.VisualRowCount).Specific.String = objMatrix2.VisualRowCount
            '    objAddOn.objApplication.SetStatusBarMessage("Please Enter Atleast One Row ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '    Return False
            'End If
        End If
        Return True
    End Function
    Public Function Matrix4ValidationFun()
        objMatrix4 = objForm.Items.Item("34").Specific
        If objMatrix4.VisualRowCount > 0 Then
            For Me.intLoop = 1 To objMatrix4.VisualRowCount - 1
                If objMatrix4.Columns.Item("V_0").Cells.Item(intLoop).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage(" Enter the Reimbursement Code at line- " & intLoop, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            Next intLoop
            If objMatrix4.Columns.Item("V_0").Cells.Item(objMatrix4.VisualRowCount).Specific.value = "" Then
                objMatrix4.DeleteRow(objMatrix4.VisualRowCount)
            End If
            'If objMatrix4.VisualRowCount = 0 Then
            '    objMatrix4.AddRow()
            '    objMatrix4.Columns.Item("V_-1").Cells.Item(objMatrix4.VisualRowCount).Specific.String = objMatrix4.VisualRowCount
            '    objAddOn.objApplication.SetStatusBarMessage("Please Enter Atleast One Row", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '    Return False
            'End If
        End If
        Return True
    End Function
    Public Function Matrix5ValidationFun()
        objMatrix5 = objForm.Items.Item("35").Specific
        If objMatrix5.VisualRowCount > 0 Then
            For Me.intLoop = 1 To objMatrix5.VisualRowCount - 1
                If objMatrix5.Columns.Item("V_0").Cells.Item(intLoop).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage(" Enter the Arrear code at line- " & intLoop, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            Next intLoop
            If objMatrix5.Columns.Item("V_0").Cells.Item(objMatrix5.VisualRowCount).Specific.value = "" Then
                objMatrix5.DeleteRow(objMatrix5.VisualRowCount)
            End If
            'If objMatrix5.VisualRowCount = 0 Then
            '    objMatrix5.AddRow()
            '    objMatrix5.Columns.Item("V_-1").Cells.Item(objMatrix5.VisualRowCount).Specific.String = objMatrix5.VisualRowCount
            '    objAddOn.objApplication.SetStatusBarMessage("Please Enter Atleast One Row ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '    Return False
            'End If
        End If
        Return True
    End Function
    Public Function Matrix6ValidationFun()
        objMatrix6 = objForm.Items.Item("36").Specific
        If objMatrix6.VisualRowCount > 0 Then
            For Me.intLoop = 1 To objMatrix6.VisualRowCount - 1
                If objMatrix6.Columns.Item("V_0").Cells.Item(intLoop).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage(" Enter the Benefit code at line- " & intLoop, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            Next intLoop
            If objMatrix6.Columns.Item("V_0").Cells.Item(objMatrix6.VisualRowCount).Specific.value = "" Then
                objMatrix6.DeleteRow(objMatrix6.VisualRowCount)
            End If
            'If objMatrix6.VisualRowCount = 0 Then
            '    objMatrix6.AddRow()
            '    objMatrix6.Columns.Item("V_-1").Cells.Item(objMatrix6.VisualRowCount).Specific.String = objMatrix6.VisualRowCount
            '    objAddOn.objApplication.SetStatusBarMessage("Please Enter Atleast One Row ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '    Return False
            'End If
        End If
        Return True
    End Function
    Public Function Matrix7ValidationFun()
        objMatrix7 = objForm.Items.Item("44").Specific
        If objMatrix7.VisualRowCount > 0 Then
            For Me.intLoop = 1 To objMatrix7.VisualRowCount - 1
                If objMatrix7.Columns.Item("V_5").Cells.Item(intLoop).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage(" Enter the Policy code at line- " & intLoop, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            Next intLoop
            If objMatrix7.Columns.Item("V_5").Cells.Item(objMatrix7.VisualRowCount).Specific.value = "" Then
                objMatrix7.DeleteRow(objMatrix7.VisualRowCount)
            End If
            'If objMatrix7.VisualRowCount = 0 Then
            '    objMatrix7.AddRow()
            '    objMatrix7.Columns.Item("V_-1").Cells.Item(objMatrix7.VisualRowCount).Specific.String = objMatrix7.VisualRowCount
            '    objAddOn.objApplication.SetStatusBarMessage("Please Enter Atleast One Row ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '    Return False
            'End If
        End If
        Return True
    End Function
#End Region

#Region "CFL Conditions"
    Public Sub ReimbursementSelection()
        Dim Objcfl As SAPbouiCOM.ChooseFromList
        Dim objChooseCollection As SAPbouiCOM.ChooseFromListCollection
        Dim objConditions As SAPbouiCOM.Conditions
        Dim objcondition As SAPbouiCOM.Condition

        objChooseCollection = objForm.ChooseFromLists
        Objcfl = objChooseCollection.Item("CFL_6")
        objConditions = Objcfl.GetConditions()
        objcondition = objConditions.Add()
        objcondition.Alias = "U_insal"
        objcondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
        objcondition.CondVal = "Y"
        Objcfl.SetConditions(objConditions)
    End Sub
#End Region

#Region "Assing Loss or Gain Amount to the Text Box"
    Public Sub IncomeLossAmount(ByVal Amount As Double)
        Try
            objForm.Items.Item("80").Specific.value = Amount
        Catch ex As Exception
            objAddOn.objApplication.StatusBar.SetText("Income Loss Amount Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
        End Try
    End Sub
#End Region
End Class
