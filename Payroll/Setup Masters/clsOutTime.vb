﻿Public Class clsOutTime
    Public Const formtype = "MNU_OT"
    Dim objForm1 As SAPbouiCOM.Form
    Dim StatEName As SAPbouiCOM.StaticText
    Dim objCombo As SAPbouiCOM.ComboBox
    Dim strSQL As String
    Dim objRS As SAPbobsCOM.Recordset
    Dim i As Integer
    Public Sub LoadScreen(ByVal EName As String, ByVal CaptionName As String, ByVal Eid As String)
        objForm1 = objAddOn.objUIXml.LoadScreenXML("OutTime.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        Dim date1 As String = CDate(CaptionName).ToString("yyyy-MM-dd")
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        strSQL = "select * from [@AIS_INOUTTIME] where U_date='" & date1 & "' and U_eid='" & Eid & "'and U_ename='" & EName & "' "
        objRS.DoQuery(strSQL)
        If objRS.RecordCount > 0 Then
            objForm1.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            objForm1.Title = objForm1.Title + "  -  " + objRS.Fields.Item("U_date").Value
            StatEName = objForm1.Items.Item("3").Specific
            StatEName.Caption = EName
            objForm1.Items.Item("15").Specific.value = objRS.Fields.Item("DocEntry").Value
            objForm1.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            objCombo = objForm1.Items.Item("5").Specific
            objCombo.ValidValues.Add("P", "Present")
            objCombo.ValidValues.Add("L", "Leave")
            objCombo.ValidValues.Add("Resigned", "Resigned")
            objCombo.ValidValues.Add("*", "Missing")
            objCombo.ValidValues.Add("A", "Absent")
            objCombo.ValidValues.Add("Comp.Off", "Compensalory Off")
            objCombo.ValidValues.Add("L-Half", "Half Day Leave")
            objCombo.ValidValues.Add("P-Half", "Present Half Day")
        Else

            objForm1.Items.Item("15").Specific.value = objForm1.BusinessObject.GetNextSerialNumber("-1", "AIS_INOUTTIME")
            objForm1.Title = objForm1.Title + "  -  " + CaptionName.ToString
            StatEName = objForm1.Items.Item("3").Specific
            StatEName.Caption = EName
            objForm1.Items.Item("16").Specific.string = EName
            objForm1.Items.Item("17").Specific.string = CaptionName
            objForm1.Items.Item("14").Specific.string = Eid
            objCombo = objForm1.Items.Item("5").Specific
            objCombo.ValidValues.Add("P", "Present")
            objCombo.ValidValues.Add("L", "Leave")
            objCombo.ValidValues.Add("Resigned", "Resigned")
            objCombo.ValidValues.Add("*", "Missing")
            objCombo.ValidValues.Add("A", "Absent")
            objCombo.ValidValues.Add("Comp.Off", "Compensalory Off")
            objCombo.ValidValues.Add("L-Half", "Half Day Leave")
            objCombo.ValidValues.Add("P-Half", "Present Half Day")
        End If

    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)

        If pVal.BeforeAction = False Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
                    If pVal.ItemUID = "7" Or pVal.ItemUID = "9" Or pVal.ItemUID = "11" Then
                        If objForm1.Items.Item("9").Specific.String = "00:00" Or objForm1.Items.Item("7").Specific.String = "00:00" Or objForm1.Items.Item("9").Specific.String = "" Or objForm1.Items.Item("7").Specific.String = "" Then
                        Else
                            If Validation(FormUID) = True Then
                                CalculateTime(FormUID)
                            End If
                        End If
                    End If
                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
                    objCombo = objForm1.Items.Item("5").Specific

                    If (objCombo.Selected.Value = "L" Or objCombo.Selected.Value = "A") And (objForm1.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or objForm1.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Or objForm1.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                        objForm1.Items.Item("7").Specific.value = 0.0
                        'objForm1.Items.Item("7").Enabled = False
                        objForm1.Items.Item("9").Specific.value = 0.0
                        'objForm1.Items.Item("9").Enabled = False
                        objForm1.Items.Item("11").Specific.value = 0.0
                        'objForm1.Items.Item("11").Enabled = False
                        objForm1.Items.Item("13").Specific.value = 0.0
                        'objForm1.Items.Item("13").Enabled = False
                    Else
                        ' objForm1.Items.Item("7").Enabled = True
                        objForm1.Items.Item("7").Specific.value = ""
                        'objForm1.Items.Item("9").Enabled = True
                        objForm1.Items.Item("9").Specific.value = ""
                        ' objForm1.Items.Item("11").Enabled = True
                        objForm1.Items.Item("11").Specific.value = ""
                        objForm1.ActiveItem = "7"
                    End If
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
                    objCombo = objForm1.Items.Item("5").Specific
                    If pVal.ItemUID = "1" And (objForm1.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Or objForm1.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE) And pVal.ActionSuccess = True Then
                        objAddOn.objShowGrid.UpdateS(objCombo.Selected.Value)
                    End If
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm1.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        objForm1.Close()
                    End If
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
                    If pVal.ItemUID = "1" And (objForm1.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or objForm1.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                        If Validation(FormUID) = False Then
                            BubbleEvent = False
                            Exit Sub
                        End If
                    End If

            End Select
        End If
    End Sub

#Region "Caluculate Time"
    Public Sub CalculateTime(ByVal FormUID As String)
        objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
        Dim str As String
        Dim RS, objRS, RS1, RS2, RS3 As SAPbobsCOM.Recordset
        Dim InTime, OutTime, OverTime As String
        Dim Total, OT, Diff, Tot As Integer
        InTime = objForm1.Items.Item("7").Specific.string
        OutTime = objForm1.Items.Item("9").Specific.string
        RS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS2 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS3 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS1.DoQuery("select DATEdiff(MINUTE,'08:00',(select left((Convert(time,CAST(((select Datediff(mi,convert(datetime,'" & InTime & "', 108),convert(datetime,'" & OutTime & "',108))) / 60) AS VARCHAR(8 )) + ':' + CAST(((select  Datediff(mi,convert(datetime,'" & InTime & "', 108), convert(datetime,'" & OutTime & "',108))) % 60)  AS varchar(2)),5)),5) )) as Total")
        If RS1.Fields.Item("Total").Value < 0 Then
            Diff = RS1.Fields.Item("Total").Value
            Tot = 480 + Diff
            objForm1.Items.Item("11").Specific.value = 0.0
            RS3.DoQuery(" SELECT CAST('" & Tot & "'/ 60 AS VARCHAR(10)) + RIGHT('0' + CAST('" & Tot & "' % 60 AS VARCHAR(2)), 2) AS Duration")

            objForm1.Items.Item("13").Specific.value = RS3.Fields.Item("Duration").Value
        Else
            OT = RS1.Fields.Item("Total").Value
            RS2.DoQuery("SELECT CAST('" & OT & "'/ 60 AS VARCHAR(10)) + RIGHT('0' + CAST('" & OT & "' % 60 AS VARCHAR(2)), 2) AS Duration")
            objForm1.Items.Item("11").Specific.value = RS2.Fields.Item("Duration").Value
            OverTime = objForm1.Items.Item("11").Specific.string
            RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            RS.DoQuery("select DATEDIFF (minute,0,'" & OverTime & "')as Mint")
            Total = RS.Fields.Item("Mint").Value
            str = " select LEFT( Convert(time, CONVERT(datetime, '08:00' ) + CONVERT(datetime, '" & objForm1.Items.Item("11").Specific.string & "' )),5) as Total"
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS.DoQuery(str)
            objForm1.Items.Item("13").Specific.value = objRS.Fields.Item("Total").Value
        End If
    End Sub
#End Region

#Region "Validation"
    Public Function Validation(ByVal FormUID As String) As Boolean
        objForm1 = objAddOn.objApplication.Forms.Item(FormUID)
        '  objCombo = objForm1.Items.Item("5").Specific
        If objForm1.Items.Item("5").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Attendance Missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm1.Items.Item("7").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Enter The In Time", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm1.Items.Item("9").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Enter The Out Time", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm1.Items.Item("7").Specific.string > objForm1.Items.Item("9").Specific.string Then
            objAddOn.objApplication.SetStatusBarMessage("OutTime Should be Greater than InTime", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

End Class
