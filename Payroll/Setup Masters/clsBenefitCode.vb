﻿Public Class clsBenefitCode
    Public objForm As SAPbouiCOM.Form
    Public Const Formtype = "bcode"
    Dim objCFLEvent As SAPbouiCOM.ChooseFromListEvent
    Dim objDataTable As SAPbouiCOM.DataTable
    Dim objItem As SAPbouiCOM.Item
    Public Sub Loadscreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("benefitcode.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, Formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.ActiveItem = 4
        objForm.AutoManaged = True
        objItem = objForm.Items.Item("4")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        objItem = objForm.Items.Item("6")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        For INTI As Integer = 2 To 3
            AccountCodeSelection("CFL_" & INTI)
        Next
    End Sub

#Region "Itemevent"
    Public Sub Itemevent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        objForm.Items.Item("14").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_BCODE")
                    End If
                    objForm = objAddOn.objApplication.Forms.Item(FormUID)
                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        If ValidateFun() Then
                        Else
                            BubbleEvent = False
                        End If
                    End If
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    objCFLEvent = pVal
                    objDataTable = objCFLEvent.SelectedObjects
                    If pVal.ItemUID = "8" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        PaybleGLCFL(pVal.Row)
                        If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                        End If
                    End If
                    If pVal.ItemUID = "10" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        ExpndGLCFL(pVal.Row)
                        If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                        End If
                    End If
            End Select
        End If
    End Sub
#End Region

#Region "Payable and Expenditure GL account"
    Public Sub PaybleGLCFL(ByVal Row As Integer)
        Try
            objForm.Items.Item("8").Specific.value = objDataTable.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub
    Public Sub ExpndGLCFL(ByVal Row As Integer)
        Try
            objForm.Items.Item("10").Specific.value = objDataTable.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Choose From List Conditions"
    Public Sub AccountCodeSelection(ByVal CFL As String)
        Dim Objcfl As SAPbouiCOM.ChooseFromList
        Dim objChooseCollection As SAPbouiCOM.ChooseFromListCollection
        Dim objConditions As SAPbouiCOM.Conditions
        Dim objcondition As SAPbouiCOM.Condition

        objChooseCollection = objForm.ChooseFromLists
        Objcfl = objChooseCollection.Item("" & CFL & "")
        objConditions = Objcfl.GetConditions()
        objcondition = objConditions.Add()
        objcondition.Alias = "Postable"
        objcondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
        objcondition.CondVal = "Y"
        Objcfl.SetConditions(objConditions)
    End Sub

#End Region

#Region "Validate"
    Public Function ValidateFun()
        If objForm.Items.Item("4").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Code is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("6").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Name is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("8").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Payable G/L Account missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("10").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Expenditure G/L Account missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

End Class
