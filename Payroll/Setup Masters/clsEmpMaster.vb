﻿Public Class clsEmpMaster
    Dim objForm As SAPbouiCOM.Form
    Public Const formtype = "Empmaster"
    Dim objedit As SAPbouiCOM.EditText
    Dim objstatic As SAPbouiCOM.StaticText
    Dim objitem As SAPbouiCOM.Item
    Dim objFolder As SAPbouiCOM.Folder
    Dim objcombo As SAPbouiCOM.ComboBox
    Dim oForm As SAPbouiCOM.Form

#Region "Create Button"
    Public Sub CreateItem()
        objitem = objForm.Items.Add("RELD1", SAPbouiCOM.BoFormItemTypes.it_EDIT)
        objitem.Left = "403"
        objitem.Width = "116"
        objitem.Top = "285"
        objitem.Height = "14"
        objedit = objForm.Items.Item("RELD1").Specific
        objitem.FromPane = 4
        objitem.ToPane = 4
        objedit.DataBind.SetBound(True, "OHEM", "U_rldate")

        objitem = objForm.Items.Add("RELD", SAPbouiCOM.BoFormItemTypes.it_STATIC)
        objitem.Left = "247"
        objitem.Width = "100"
        objitem.Top = "285"
        objitem.Height = "14"
        objstatic = objForm.Items.Item("RELD").Specific
        objitem.FromPane = 4
        objitem.ToPane = 4
        objitem.LinkTo = "RELD1"
        objstatic.Caption = "RelivedDate"


        objitem = objForm.Items.Add("Fix", SAPbouiCOM.BoFormItemTypes.it_EDIT)
        objitem.Left = "125"
        objitem.Width = "120"
        objitem.Top = "300"
        objitem.Height = "14"
        objedit = objForm.Items.Item("Fix").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objedit.DataBind.SetBound(True, "OHEM", "U_fix")

        objitem = objForm.Items.Add("Fixed", SAPbouiCOM.BoFormItemTypes.it_STATIC)
        objitem.Width = "120"
        objitem.Height = "14"
        objitem.Left = "18"
        objitem.Top = "300"
        objstatic = objForm.Items.Item("Fixed").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objitem.LinkTo = "Fix"
        objstatic.Caption = "Fixed"

        objitem = objForm.Items.Add("Vari", SAPbouiCOM.BoFormItemTypes.it_EDIT)
        objitem.Left = "125"
        objitem.Width = "120"
        objitem.Top = "315"
        objitem.Height = "14"
        objedit = objForm.Items.Item("Vari").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objedit.DataBind.SetBound(True, "OHEM", "U_vari")


        objitem = objForm.Items.Add("Variable", SAPbouiCOM.BoFormItemTypes.it_STATIC)
        objitem.Width = "120"
        objitem.Height = "14"
        objitem.Left = "18"
        objitem.Top = "315"
        objstatic = objForm.Items.Item("Variable").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objitem.LinkTo = "Vari"
        objstatic.Caption = "Variable"



        objitem = objForm.Items.Add("Rat", SAPbouiCOM.BoFormItemTypes.it_EDIT)
        objitem.Left = "125"
        objitem.Width = "120"
        objitem.Top = "330"
        objitem.Height = "14"
        objedit = objForm.Items.Item("Rat").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objedit.DataBind.SetBound(True, "OHEM", "U_ratio")


        objitem = objForm.Items.Add("Ratio", SAPbouiCOM.BoFormItemTypes.it_STATIC)
        objitem.Width = "120"
        objitem.Height = "14"
        objitem.Left = "18"
        objitem.Top = "330"
        objstatic = objForm.Items.Item("Ratio").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objitem.LinkTo = "Rat"
        objstatic.Caption = "Ratio"


        objitem = objForm.Items.Add("Inc", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
        objitem.Left = "125"
        objitem.Width = "120"
        objitem.Top = "345"
        objitem.Height = "14"
        objcombo = objForm.Items.Item("Inc").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objcombo.DataBind.SetBound(True, "OHEM", "U_incent")


        objitem = objForm.Items.Add("Incent", SAPbouiCOM.BoFormItemTypes.it_STATIC)
        objitem.Width = "120"
        objitem.Height = "14"
        objitem.Left = "18"
        objitem.Top = "345"
        objstatic = objForm.Items.Item("Incent").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objitem.LinkTo = "Inc"
        objstatic.Caption = "Variable Pay"

        objitem = objForm.Items.Add("pan", SAPbouiCOM.BoFormItemTypes.it_EDIT)
        objitem.Left = "125"
        objitem.Width = "120"
        objitem.Top = "360"
        objitem.Height = "14"
        objedit = objForm.Items.Item("pan").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objedit.DataBind.SetBound(True, "OHEM", "U_pan")


        objitem = objForm.Items.Add("PANNUM", SAPbouiCOM.BoFormItemTypes.it_STATIC)
        objitem.Width = "120"
        objitem.Height = "14"
        objitem.Left = "18"
        objitem.Top = "360"
        objstatic = objForm.Items.Item("PANNUM").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objitem.LinkTo = "pan"
        objstatic.Caption = "PAN Number"


        objitem = objForm.Items.Add("pf", SAPbouiCOM.BoFormItemTypes.it_EDIT)
        objitem.Left = "125"
        objitem.Width = "120"
        objitem.Top = "375"
        objitem.Height = "14"
        objedit = objForm.Items.Item("pf").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objedit.DataBind.SetBound(True, "OHEM", "U_pf")

        objitem = objForm.Items.Add("PFNUM", SAPbouiCOM.BoFormItemTypes.it_STATIC)
        objitem.Width = "120"
        objitem.Height = "14"
        objitem.Left = "18"
        objitem.Top = "375"
        objstatic = objForm.Items.Item("PFNUM").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objitem.LinkTo = "pf"
        objstatic.Caption = "PF Number"


        objitem = objForm.Items.Add("bonus", SAPbouiCOM.BoFormItemTypes.it_EDIT)
        objitem.Left = "125"
        objitem.Width = "120"
        objitem.Top = "390"
        objitem.Height = "14"
        objedit = objForm.Items.Item("bonus").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objedit.DataBind.SetBound(True, "OHEM", "U_bonus")

        objitem = objForm.Items.Add("BAmount", SAPbouiCOM.BoFormItemTypes.it_STATIC)
        objitem.Width = "120"
        objitem.Height = "14"
        objitem.Left = "18"
        objitem.Top = "390"
        objstatic = objForm.Items.Item("BAmount").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objitem.LinkTo = "bonus"
        objstatic.Caption = "Bonus Amount"


        objitem = objForm.Items.Add("pmode", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
        objitem.Left = "125"
        objitem.Width = "120"
        objitem.Top = "390"
        objitem.Height = "14"
        objcombo = objForm.Items.Item("pmode").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objcombo.DataBind.SetBound(True, "OHEM", "U_pmode")

        objitem = objForm.Items.Add("paymode", SAPbouiCOM.BoFormItemTypes.it_STATIC)
        objitem.Width = "120"
        objitem.Height = "14"
        objitem.Left = "18"
        objitem.Top = "390"
        objstatic = objForm.Items.Item("paymode").Specific
        objitem.FromPane = 7
        objitem.ToPane = 7
        objitem.LinkTo = "pmode"
        objstatic.Caption = "Payment Mode"


        Dim oRS As SAPbobsCOM.Recordset
        Dim strSQL As String
        oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        strSQL = "select Code,Name from [@AIS_EMP] order by Code"
        oRS.DoQuery(strSQL)
        While oRS.EoF = False
            objcombo.ValidValues.Add(oRS.Fields.Item("Code").Value, oRS.Fields.Item("Name").Value)
            oRS.MoveNext()
        End While

        Dim oButton As SAPbouiCOM.Button
        objitem = objForm.Items.Add("Asset", SAPbouiCOM.BoFormItemTypes.it_BUTTON)
        objitem.Width = "120"
        objitem.Height = "20"
        objitem.Left = "307"
        objitem.Top = "330"
        oButton = objForm.Items.Item("Asset").Specific
        objitem.FromPane = 3
        objitem.ToPane = 3
        oButton.Caption = "Asset"



    End Sub
#End Region

#Region "Itemevent"
    Public Sub itemevent(ByVal formuid As String, ByRef pval As SAPbouiCOM.ItemEvent, ByRef bubbleevent As Boolean)
        Try
            objForm = objAddOn.objApplication.Forms.Item(formuid)
            If pval.BeforeAction = True Then
                Select Case pval.EventType
                    Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
                        objFolder = objForm.Items.Item("23").Specific
                        CreateItem()
                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        If objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            If Validate(formuid) = False Then
                                bubbleevent = False
                                Exit Sub
                            End If
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pval.ItemUID = "2" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            oForm = objAddOn.objApplication.Forms.GetForm(-(objForm.Type), 1)
                            objAddOn.objgeneralsetting.GeneralSettings()
                            If EmpCode = "Y" Then
                                If oForm.Items.Item("U_ecode").Specific.string = "" Then
                                    objAddOn.objApplication.SetStatusBarMessage("Employee ID should not be an Empty in Employee Master Data..", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                    bubbleevent = False
                                    Exit Sub
                                End If
                            End If
                        End If
                End Select

            Else
                Select Case pval.EventType
                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        If pval.ItemUID = "Fix" Or pval.ItemUID = "Vari" Then
                            If objForm.Items.Item("Fix").Specific.value <> "" And objForm.Items.Item("Vari").Specific.value <> "" Then
                                objForm.Items.Item("Rat").Specific.value = Left(objForm.Items.Item("Fix").Specific.value, 2) + " : " + Left(objForm.Items.Item("Vari").Specific.value, 2)
                            End If
                        End If
                        If pval.ItemUID = "99" Or pval.ItemUID = "bonus" Then
                            If objForm.Items.Item("99").Specific.value <> "" Then
                                Dim YearSal, MonthSal As Double
                                ' Dim combo As SAPbouiCOM.ComboBox
                                If objForm.Items.Item("bonus").Specific.value = "" Then
                                    objForm.Items.Item("bonus").Specific.value = 0.0
                                    YearSal = Mid(objForm.Items.Item("99").Specific.value, 4).ToString - objForm.Items.Item("bonus").Specific.value
                                    MonthSal = YearSal / 12
                                    objForm.Items.Item("100").Specific.value = MonthSal
                                Else
                                    YearSal = Mid(objForm.Items.Item("99").Specific.value, 4).ToString - objForm.Items.Item("bonus").Specific.value
                                    MonthSal = YearSal / 12
                                    objForm.Items.Item("100").Specific.value = MonthSal
                                End If
                            End If
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        Dim Name, ID As String
                        ' Dim ID As Integer
                        ID = objForm.Items.Item("33").Specific.string
                        Name = objForm.Items.Item("38").Specific.string + " " + objForm.Items.Item("37").Specific.string
                        If pval.ItemUID = "Asset" Then
                            objAddOn.objEmpAsset.LoadScreen(ID, Name)
                        End If
                End Select
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Validate Function"
    Public Function Validate(ByVal FormUID As String) As Boolean
        Dim Fix, Var, tot As Integer

        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        oForm = objAddOn.objApplication.Forms.GetForm(-(objForm.Type), 1)
        If objForm.Items.Item("Fix").Specific.value <> "" Then
            Fix = objForm.Items.Item("Fix").Specific.value
        Else
            Fix = 0
        End If
        If objForm.Items.Item("Vari").Specific.value <> "" Then
            Var = objForm.Items.Item("Vari").Specific.value
        Else
            Var = 0
        End If

        tot = Fix + Var
        If (tot > 100) Then
            objAddOn.objApplication.SetStatusBarMessage("Percentage Should not be Exceed 100", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

End Class
