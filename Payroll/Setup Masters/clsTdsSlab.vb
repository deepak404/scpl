﻿Public Class clsTdsSlab
    Public Const formtype = "MNU_TDS"
    Dim objForm As SAPbouiCOM.Form
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim objCom As SAPbouiCOM.ComboBox
    Dim objComb, objComboBox As SAPbouiCOM.ComboBox
    Dim rowadd As Boolean = False
    Public Sub LoadScreen1()
        objForm = objAddOn.objUIXml.LoadScreenXML("tdscalslab.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.DataBrowser.BrowseBy = "8"
        'objAddOn.objApplication.Menus.Item("1282").Enabled = True
        objForm.Items.Item("8").Visible = True
        objComboBox = objForm.Items.Item("4").Specific
        objForm.ActiveItem = "4"
        objMatrix = objForm.Items.Item("7").Specific
        objMatrix.AddRow()
        MatrixCombo()
        Combo()
        SerialNo()
    End Sub

#Region "combo"
    Public Sub MatrixCombo()
        objMatrix = objForm.Items.Item("7").Specific
        objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.VisualRowCount).Specific.value = objMatrix.RowCount
        objComb = objMatrix.Columns.Item("V_1").Cells.Item(objMatrix.RowCount).Specific
        objComb.ValidValues.Add("Between", "Between")
        objComb.ValidValues.Add("Less than", "Less than")
        objComb.ValidValues.Add("Less or equal", "Less  or equal")
        objComb.ValidValues.Add("Greater than", "Greater than")
        objComb.ValidValues.Add("Equal", "Equal")
        objComb.ValidValues.Add("greater or equal", "greater or equal")
        objComb.ValidValues.Add("notequal", "notequal")
        objComb.ValidValues.Add("In range", "In Range")
        objComb.ValidValues.Add("Out of Range", "Out Range")
        objComb.ValidValues.Add("Is empty", "Is empty")
        objComb.ValidValues.Add("Is not empty", "Is not empty")
    End Sub

    Public Sub Combo()
        'objForm = objAddOn.objApplication.Forms.Item(FormUID)
        objCom = objForm.Items.Item("4").Specific
        While objCom.ValidValues.Count <> 0
            objCom.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
        End While
        objCom.ValidValues.Add("TDS-MALE", "Male")
        objCom.ValidValues.Add("TDS-FEMALE", "Female")
        objCom.ValidValues.Add("TDS-Senior Citizen", "Senior Citizen")
    End Sub
#End Region

#Region "addmode"
    Public Sub addmode()
        objForm.Items.Item("8").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_OTDS")
        rowadd = True
        objForm.Items.Item("4").Enabled = True
        objForm.Items.Item("6").Enabled = True
        objForm.ActiveItem = "4"
    End Sub
#End Region

#Region "find mode"
    Public Sub findmode()
        objForm.Items.Item("4").Enabled = True
        objForm.Items.Item("6").Enabled = True
        objForm.ActiveItem = "4"
    End Sub
#End Region

#Region "itemevent"
    Public Sub itemevent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef Bubbleevent As Boolean)
        If pVal.BeforeAction = True Then
            objForm = objAddOn.objApplication.Forms.Item(FormUID)
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    objMatrix = objForm.Items.Item("7").Specific
                    'If pVal.ItemUID = "7" And pVal.ColUID = "V_3" Then
                    '    If Val(objMatrix.Columns.Item("V_2").Cells.Item(pVal.Row).Specific.value) <> "" Then
                    '    Else
                    '        objAddOn.objApplication.SetStatusBarMessage("Please enter the Lower limit", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    '        Bubbleevent = False
                    '        Exit Sub
                    '    End If
                    'End If
                    If pVal.ItemUID = "7" And pVal.ColUID = "V_2" Then
                        If objMatrix.Columns.Item("V_2").Cells.Item(pVal.Row).Specific.value <> "" And objMatrix.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.value <> "" Then
                            If Val(objMatrix.Columns.Item("V_2").Cells.Item(pVal.Row).Specific.value) > Val(objMatrix.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.value) Then
                            Else
                                objAddOn.objApplication.SetStatusBarMessage("Please enter the greater value than lower limit", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                Bubbleevent = False
                                Exit Sub
                            End If
                        End If

                    ElseIf pVal.ItemUID = "7" And pVal.ColUID = "V_0" Then
                        If Val(objMatrix.Columns.Item("V_0").Cells.Item(pVal.Row).Specific.value) < 100 Then
                        Else
                            objAddOn.objApplication.SetStatusBarMessage("Please give the percent value ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                            Bubbleevent = False
                            Exit Sub
                        End If

                    ElseIf pVal.ItemUID = "7" And pVal.ColUID = "V_3" Then
                        If objMatrix.Columns.Item("V_2").Cells.Item(pVal.Row).Specific.value <> "" And objMatrix.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.value <> "" Then
                            If Val(objMatrix.Columns.Item("V_2").Cells.Item(pVal.Row).Specific.value) > Val(objMatrix.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.value) Then
                            Else
                                objAddOn.objApplication.SetStatusBarMessage("Please enter the lower value than the upper limit", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                Bubbleevent = False
                                Exit Sub
                            End If
                        End If
                    End If

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        objForm.ActiveItem = "6"
                        If ValidateCombo(FormUID) = False Then
                            Bubbleevent = False
                            Exit Sub
                        End If
                    End If

                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        objMatrix = objForm.Items.Item("7").Specific
                        If objMatrix.Columns.Item("V_3").Cells.Item(objMatrix.VisualRowCount).Specific.value = "" Then
                            objMatrix.DeleteRow(objMatrix.VisualRowCount)
                        End If
                    End If

            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    objMatrix = objForm.Items.Item("7").Specific
                    If pVal.ItemUID = "7" Then
                        If objMatrix.Columns.Item("V_3").Cells.Item(objMatrix.VisualRowCount).Specific.value <> "" Then
                            objForm.DataSources.DBDataSources.Item("@AIS_TDS1").Clear()
                            objMatrix.AddRow()
                            objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.VisualRowCount).Specific.value = objMatrix.VisualRowCount
                        End If
                    End If

                    'Case SAPbouiCOM.BoEventTypes.et_CLICK
                    '    If pVal.ItemUID = "4" Then
                    '        Combo(FormUID)
                    '    End If

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        objForm.Items.Item("8").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_OTDS")
                        objForm.ActiveItem = "4"
                    End If

                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    If pVal.ItemUID = "4" Then

                        Dim objRS As SAPbobsCOM.Recordset
                        Dim strSQL As String
                        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        strSQL = "select Code from [@AIS_OTDS] where U_code ='" & objComboBox.Selected.Value & "'"
                        objRS.DoQuery(strSQL)
                        Try
                            If objRS.RecordCount > 0 Then
                                objForm.Freeze(True)
                                objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                                objForm.Items.Item("8").Enabled = True
                                objForm.Items.Item("8").Specific.value = objRS.Fields.Item("Code").Value
                                objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                                objForm.Items.Item("4").Enabled = False
                                objForm.Items.Item("6").Enabled = False
                                objForm.DataSources.DBDataSources.Item("@AIS_TDS1").Clear()
                                objMatrix.AddRow()
                                objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
                                objForm.Freeze(False)
                            Else
                                objMatrix = objForm.Items.Item("7").Specific
                                SerialNo()
                                If Not objMatrix.VisualRowCount > 0 Then
                                    objMatrix.AddRow()
                                    objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.VisualRowCount).Specific.value = objMatrix.VisualRowCount
                                End If
                            End If
                        Catch ex As Exception
                            objForm.Freeze(False)
                        End Try
                    End If


            End Select
        End If
    End Sub
#End Region

#Region "validate"
    Public Function ValidateCombo(ByVal FormUID As String) As Boolean
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        Dim i As Integer
        If objForm.Items.Item("4").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please enter the code", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("6").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please enter the name", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False

        ElseIf objMatrix.VisualRowCount > 0 Then
            For i = 1 To objMatrix.VisualRowCount
                If objMatrix.Columns.Item("V_3").Cells.Item(i).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Please enter the Lower Limit at line no " & i, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("V_2").Cells.Item(i).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Please enter the Upper Limit at line no " & i, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("V_1").Cells.Item(i).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage("Please enter the Condition at line no " & i, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.Columns.Item("V_0").Cells.Item(i).Specific.value < 0 Then
                    objAddOn.objApplication.SetStatusBarMessage("Please enter the Percentage at line no " & i, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf objMatrix.VisualRowCount > 0 Then
                    If objMatrix.Columns.Item("V_3").Cells.Item(objMatrix.VisualRowCount).Specific.value = "" And objMatrix.Columns.Item("V_2").Cells.Item(objMatrix.VisualRowCount).Specific.value = "" And objMatrix.Columns.Item("V_1").Cells.Item(objMatrix.VisualRowCount).Specific.value = "" And objMatrix.Columns.Item("V_0").Cells.Item(objMatrix.VisualRowCount).Specific.value = 0 And objMatrix.VisualRowCount > 1 Then
                        objMatrix.DeleteRow(objMatrix.VisualRowCount)
                    End If
                End If

            Next i
        End If
        Return True
    End Function



#End Region

#Region "serialno"
    Public Sub SerialNo()
        objForm.Items.Item("8").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_OTDS")
    End Sub
#End Region

End Class



