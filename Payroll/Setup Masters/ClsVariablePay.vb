Public Class ClsVariablePay

#Region "Declaration"
    Public Const formtype = "VariablePay"
    Dim objForm As SAPbouiCOM.Form
    Dim objMatrix, objMatrix1, objMatrix2 As SAPbouiCOM.Matrix
    Dim objCFLEvent As SAPbouiCOM.ChooseFromListEvent
    Dim objDataTable As SAPbouiCOM.DataTable
    Dim objComboMonth, objComboMonth1, objComboYear, objComboVPay, objcombomonth2 As SAPbouiCOM.ComboBox
    Dim objCheck As SAPbouiCOM.CheckBox
#End Region

#Region "LoadScreen"
    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("VariablePay.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.Items.Item("6").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_VP")
        objForm.DataBrowser.BrowseBy = 6

        objMatrix = objForm.Items.Item("5").Specific
        objMatrix1 = objForm.Items.Item("11").Specific
        objMatrix2 = objForm.Items.Item("14").Specific
        objMatrix.Columns.Item("V_0").Editable = True
        objMatrix1.Columns.Item("V_0").Editable = True

        objComboMonth = objForm.Items.Item("8").Specific
        objComboMonth1 = objForm.Items.Item("12").Specific
        objComboYear = objForm.Items.Item("10").Specific
        objComboVPay = objForm.Items.Item("4").Specific

        objForm.Items.Item("11").Visible = False
        objForm.Items.Item("12").Visible = False
        Combo_Month()
        Combo_Year()


    End Sub

#End Region

#Region "ItemEvent"
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "1" Then
                            If objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            Else
                                If Validation() = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                    'ElseIf objMatrix.Columns.Item("V_4").Cells.Item(objMatrix.RowCount).Specific.value = "" Then
                                    '    objMatrix.DeleteRow(objMatrix.RowCount)
                                    'End If
                                    'If objMatrix1.Columns.Item("V_7").Cells.Item(objMatrix1.RowCount).Specific.value = "" Then
                                    '    objMatrix1.DeleteRow(objMatrix1.RowCount)
                                End If
                            End If
                        End If
                        If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            DeleteRecordQuarter(FormUID, pVal.Row)
                            DeleteRecordMonth(FormUID, pVal.Row)
                        End If
                End Select
            Else
                Select Case pVal.EventType
                    'Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    'If (pVal.ItemUID = "5" And pVal.ColUID = "V_4") Then
                    '    EmpidCFL(pVal)
                    'ElseIf (pVal.ItemUID = "11" And pVal.ColUID = "V_7") Then
                    '    EmpidCFL1(pVal)
                    'End If
                    Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        If pVal.ItemUID = "5" And pVal.ColUID = "V_6" Then
                            If objMatrix.Columns.Item("V_4").Cells.Item(objMatrix.RowCount).Specific.value = "" Then
                            Else
                                objForm.Refresh()
                                Percentage_Calc(pVal.Row)
                                'objMatrix.AddRow()
                                'objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.RowCount
                            End If
                        ElseIf pVal.ItemUID = "11" And pVal.ColUID = "V_2" Then
                            If objMatrix1.Columns.Item("V_7").Cells.Item(objMatrix1.RowCount).Specific.value = "" Then
                            Else
                                objForm.Refresh()
                                Percentage_Calc1(pVal.Row)
                                'objMatrix1.AddRow()
                                'objMatrix1.Columns.Item("V_-1").Cells.Item(objMatrix1.RowCount).Specific.value = objMatrix1.RowCount
                            End If



                        End If
                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        If pVal.ItemUID = "11" And pVal.ColUID = "V_2" Then
                            LineTotal(pVal.Row)
                            'Percentage_Calc1(pVal)
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                        If pVal.ItemUID = "4" Then
                            objComboMonth = objForm.Items.Item("8").Specific
                            objComboMonth1 = objForm.Items.Item("12").Specific

                            objComboVPay = objForm.Items.Item("4").Specific
                            If objComboVPay.Selected.Value = "M" Then
                                Combo_Month()
                                objForm.Items.Item("11").Visible = False
                                objForm.Items.Item("12").Visible = False
                                objForm.Items.Item("8").Visible = True
                                objForm.Items.Item("5").Visible = True
                                'objForm.PaneLevel = 0

                            ElseIf objComboVPay.Selected.Value = "Q" Then
                                Combo_Quarter()
                                objForm.Items.Item("5").Visible = False
                                objForm.Items.Item("8").Visible = False
                                objForm.Items.Item("11").Visible = True
                                objForm.Items.Item("12").Visible = True
                                ' objForm.PaneLevel = 1


                            ElseIf objComboVPay.Selected.Value = "H" Then
                                Combo_Halfyear()
                                'objForm.Items.Item("14").Visible = True
                                objForm.Items.Item("5").Visible = False
                                objForm.Items.Item("11").Visible = False
                                objForm.Items.Item("14").Visible = True
                                objForm.Items.Item("13").Visible = True
                                ' objForm.PaneLevel = 2

                                'If objMatrix1.RowCount = 0 Then
                                '    Emp_To_Matrix1()
                                'End If

                            End If
                        End If
                        If pVal.ItemUID = "10" Then
                            objComboVPay = objForm.Items.Item("4").Specific
                            If objComboVPay.Selected.Value = "M" Then
                                If objMatrix.RowCount > 0 Then
                                    Dim i As Integer
                                    For i = objMatrix.RowCount To 1 Step -1
                                        objMatrix.DeleteRow(i)
                                    Next
                                End If
                                Emp_To_Matrix(objComboVPay.Selected.Value)

                            ElseIf objComboVPay.Selected.Value = "Q" Then
                                If objMatrix1.RowCount > 0 Then
                                    Dim i As Integer
                                    For i = objMatrix1.RowCount To 1 Step -1
                                        objMatrix1.DeleteRow(i)
                                    Next
                                End If
                                Emp_To_Matrix1(objComboVPay.Selected.Value)

                            ElseIf objComboVPay.Selected.Value = "H" Then
                                If objMatrix2.RowCount > 0 Then
                                    'Dim i As Integer
                                    'For i = objMatrix1.RowCount To 1 Step -1
                                    '    objMatrix2.DeleteRow(i)
                                    'Next
                                    objMatrix2.Clear()
                                End If
                                Emp_To_Matrix2(objComboVPay.Selected.Value)
                            End If
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                        If pVal.ItemUID = "5" And pVal.ColUID = "V_-1" Then
                            If objMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value = "" Then
                            Else
                                Dim EmpID As Integer
                                Dim EmpName As String
                                Dim Year As Integer
                                EmpID = objMatrix.Columns.Item("V_4").Cells.Item(pVal.Row).Specific.value
                                EmpName = objMatrix.Columns.Item("V_3").Cells.Item(pVal.Row).Specific.value
                                objComboYear = objForm.Items.Item("10").Specific
                                Year = objComboYear.Selected.Value
                                objAddOn.objConsolidatedMonth.LoadScreen(EmpID, EmpName, Year)
                            End If
                        ElseIf pVal.ItemUID = "11" And pVal.ColUID = "V_-1" Then
                            If objMatrix1.Columns.Item("V_7").Cells.Item(pVal.Row).Specific.value = "" Then
                            Else
                                Dim EmpID As Integer
                                Dim EmpName As String
                                Dim Year As Integer
                                EmpID = objMatrix1.Columns.Item("V_7").Cells.Item(pVal.Row).Specific.value
                                EmpName = objMatrix1.Columns.Item("V_6").Cells.Item(pVal.Row).Specific.value
                                objComboYear = objForm.Items.Item("10").Specific
                                Year = objComboYear.Selected.Value
                                objAddOn.objConsolidatedQuarter.LoadScreen(EmpID, EmpName, Year)
                            End If
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                            objForm.Items.Item("6").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_VP")
                            objMatrix = objForm.Items.Item("5").Specific
                            objMatrix1 = objForm.Items.Item("11").Specific

                            'objMatrix1.AddRow()
                            'objMatrix1.Columns.Item("V_-1").Cells.Item(objMatrix1.RowCount).Specific.value = objMatrix1.RowCount
                            objComboMonth = objForm.Items.Item("8").Specific
                            objComboYear = objForm.Items.Item("10").Specific
                            objComboVPay = objForm.Items.Item("4").Specific
                            'objMatrix.AddRow()
                            objForm.Items.Item("11").Visible = False
                            'objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.RowCount
                            Combo_Month()
                            Combo_Year()

                        End If
                End Select
            End If
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try

    End Sub
#End Region

#Region "Delete the Record "
    Public Sub DeleteRecordQuarter(ByVal FormUID As String, ByVal RowNo As Integer)
        Try
            Dim objRS, objRS1 As SAPbobsCOM.Recordset
            Dim strSQL As String
            Dim i, j As Integer
            objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            '   MsgBox(objMatrix1.VisualRowCount)
            For j = 1 To objMatrix1.VisualRowCount
                strSQL = "select * from [@AIS_VPAYQ] T0 join [@AIS_VPAY] T1 on T0.Code=T1.Code where T0.U_empid='" & objMatrix1.Columns.Item("V_7").Cells.Item(j).Specific.string & "' and T0.U_month ='" & objMatrix1.Columns.Item("V_8").Cells.Item(j).Specific.string & "' and T1.U_year ='" & objComboYear.Selected.Value & "' "
                objRS1.DoQuery(strSQL)
                If objRS1.RecordCount > 0 Then
                    For i = 1 To objRS1.RecordCount
                        strSQL = "delete  from [@AIS_VPAYQ]  where  U_empid ='" & objMatrix1.Columns.Item("V_7").Cells.Item(j).Specific.string & "' and U_month='" & objMatrix1.Columns.Item("V_8").Cells.Item(j).Specific.string & "'"
                        objRS.DoQuery(strSQL)
                    Next
                End If
            Next
        Catch ex As Exception

        End Try

    End Sub

    Public Sub DeleteRecordMonth(ByVal FormUID As String, ByVal RowNo As Integer)
        Dim objRS, objRS1 As SAPbobsCOM.Recordset
        Dim strSQL As String
        Dim i, j As Integer
        objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        For j = 1 To objMatrix.VisualRowCount
            strSQL = "select * from [@AIS_VPAYM] T0 join [@AIS_VPAY] T1 on T0.Code=T1.Code where T0.U_empid='" & objMatrix.Columns.Item("V_4").Cells.Item(j).Specific.string & "' and T0.U_month ='" & objMatrix.Columns.Item("V_5").Cells.Item(j).Specific.string & "' and T1.U_year ='" & objComboYear.Selected.Value & "' "
            objRS1.DoQuery(strSQL)
            If objRS1.RecordCount > 0 Then
                For i = 1 To objRS1.RecordCount
                    strSQL = "delete  from [@AIS_VPAYM] where  U_empid ='" & objMatrix.Columns.Item("V_4").Cells.Item(j).Specific.string & "' and U_month='" & objMatrix.Columns.Item("V_5").Cells.Item(j).Specific.string & "'"
                    objRS.DoQuery(strSQL)
                Next
            End If
        Next
    End Sub

#End Region

#Region "Emp_To_Matrix"
    Public Sub Emp_To_Matrix(ByVal Var As String)
        Try
            Dim objRS, objRS1 As SAPbobsCOM.Recordset
            Dim Str, StrSQL, EID, EName As String
            objAddOn.objApplication.SetStatusBarMessage("Please Wait While Loading the Data....", SAPbouiCOM.BoMessageTime.bmt_Long, False)
            objForm.Freeze(True)
            Str = "select empID as EmpID,lastName+' '+ firstName as EmpName from ohem where U_incent='" & Var & "'"
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS.DoQuery(Str)
            If objRS.RecordCount > 0 Then
                Dim i As Integer
                For i = 1 To objRS.RecordCount
                    objMatrix.AddRow()
                    EID = objRS.Fields.Item("EmpID").Value
                    EName = objRS.Fields.Item("EmpName").Value
                    objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.RowCount
                    objMatrix.Columns.Item("V_4").Cells.Item(objMatrix.RowCount).Specific.value = EID
                    objMatrix.Columns.Item("V_3").Cells.Item(objMatrix.RowCount).Specific.value = EName
                    objMatrix.Columns.Item("V_5").Cells.Item(objMatrix.RowCount).Specific.value = objComboMonth.Selected.Value
                    objMatrix.Columns.Item("V_9").Cells.Item(objMatrix.RowCount).Specific.value = objComboYear.Selected.Value
                    StrSQL = "select T0.U_empid,T0.U_empname,T0.U_percent,T0.U_month,T0.U_pay,T0.U_tot,T0.U_year from [@AIS_VPAYM] T0"
                    StrSQL += " join [@AIS_VPAY] T1 on T0.Code =T1.Code where T0.U_empid='" & EID & "' and T1.U_month='" & objComboMonth.Selected.Value & "' and T1.U_year='" & objComboYear.Selected.Value & "'"
                    objRS1.DoQuery(StrSQL)
                    If objRS1.RecordCount > 0 Then
                        objMatrix.Columns.Item("V_4").Cells.Item(objMatrix.RowCount).Specific.value = objRS1.Fields.Item("U_empid").Value
                        objMatrix.Columns.Item("V_3").Cells.Item(objMatrix.RowCount).Specific.value = objRS1.Fields.Item("U_empname").Value
                        objMatrix.Columns.Item("V_5").Cells.Item(objMatrix.RowCount).Specific.value = objRS1.Fields.Item("U_month").Value
                        objMatrix.Columns.Item("V_9").Cells.Item(objMatrix.RowCount).Specific.value = objRS1.Fields.Item("U_year").Value
                        'objMatrix.Columns.Item("V_2").Cells.Item(objMatrix.RowCount).Specific.value = objRS1.Fields.Item("U_target").Value
                        'objMatrix.Columns.Item("V_1").Cells.Item(objMatrix.RowCount).Specific.value = objRS1.Fields.Item("U_achieve").Value
                        objMatrix.Columns.Item("V_6").Cells.Item(objMatrix.RowCount).Specific.value = objRS1.Fields.Item("U_percent").Value
                        objCheck = objMatrix.Columns.Item("V_0").Cells.Item(objMatrix.RowCount).Specific
                        If objRS1.Fields.Item("U_pay").Value = "Y" Then
                            objCheck.Checked = True
                        Else
                            objCheck.Checked = False
                        End If
                        objMatrix.Columns.Item("V_7").Cells.Item(objMatrix.RowCount).Specific.value = objRS1.Fields.Item("U_tot").Value

                    End If
                    objRS.MoveNext()
                Next

            End If
            objForm.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage("Values are Loaded", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try

    End Sub
#End Region

#Region "Emp_to_Matrix1"
    Public Sub Emp_To_Matrix1(ByVal Var As String)
        Try
            Dim objRS, objRS1 As SAPbobsCOM.Recordset
            Dim Str, StrSQL, EID, EName As String
            objAddOn.objApplication.SetStatusBarMessage("Please Wait While Loading the Data....", SAPbouiCOM.BoMessageTime.bmt_Long, False)
            objForm.Freeze(True)
            Str = "select empID as EmpID,lastName+' '+ firstName as EmpName from ohem where U_incent='" & Var & "'"
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS.DoQuery(Str)
            If objRS.RecordCount > 0 Then
                Dim i As Integer
                For i = 1 To objRS.RecordCount
                    objMatrix1.AddRow()
                    EID = objRS.Fields.Item("EmpID").Value
                    EName = objRS.Fields.Item("EmpName").Value
                    objMatrix1.Columns.Item("V_-1").Cells.Item(objMatrix1.RowCount).Specific.value = objMatrix1.RowCount
                    objMatrix1.Columns.Item("V_7").Cells.Item(objMatrix1.RowCount).Specific.value = EID
                    objMatrix1.Columns.Item("V_6").Cells.Item(objMatrix1.RowCount).Specific.value = EName
                    objMatrix1.Columns.Item("V_8").Cells.Item(objMatrix1.RowCount).Specific.value = objComboMonth1.Selected.Value
                    objMatrix1.Columns.Item("V_12").Cells.Item(objMatrix1.RowCount).Specific.value = objComboYear.Selected.Value

                    StrSQL = "select T1.U_type ,T1.U_month ,T1.U_year ,T0 .U_empid ,T0.U_empname,T0.U_month,"
                    StrSQL += "T0.U_pay,T0.U_achm1,T0.U_achm2 ,T0.U_achm3 ,T0.U_tot ,T0.U_percent,T0.U_year from [@AIS_VPAYQ] T0 "
                    StrSQL += "join [@AIS_VPAY] T1 on T0.Code =T1.Code where T0.U_empid='" & EID & "' and T1.U_mon='" & objComboMonth1.Selected.Value & "' and T1.U_year='" & objComboYear.Selected.Value & "'"
                    objRS1.DoQuery(StrSQL)
                    If objRS1.RecordCount > 0 Then

                        objMatrix1.Columns.Item("V_7").Cells.Item(objMatrix1.RowCount).Specific.value = objRS1.Fields.Item("U_empid").Value
                        objMatrix1.Columns.Item("V_6").Cells.Item(objMatrix1.RowCount).Specific.value = objRS1.Fields.Item("U_empname").Value
                        objMatrix1.Columns.Item("V_8").Cells.Item(objMatrix1.RowCount).Specific.value = objRS1.Fields.Item("U_month").Value
                        objMatrix1.Columns.Item("V_12").Cells.Item(objMatrix1.RowCount).Specific.value = objRS1.Fields.Item("U_year").Value
                        'objMatrix1.Columns.Item("V_5").Cells.Item(objMatrix1.RowCount).Specific.value = objRS1.Fields.Item("U_target").Value
                        objMatrix1.Columns.Item("V_4").Cells.Item(objMatrix1.RowCount).Specific.value = objRS1.Fields.Item("U_achm1").Value
                        objMatrix1.Columns.Item("V_3").Cells.Item(objMatrix1.RowCount).Specific.value = objRS1.Fields.Item("U_achm2").Value
                        objMatrix1.Columns.Item("V_2").Cells.Item(objMatrix1.RowCount).Specific.value = objRS1.Fields.Item("U_achm3").Value
                        'objMatrix1.Columns.Item("V_1").Cells.Item(objMatrix1.RowCount).Specific.value = objRS1.Fields.Item("U_achieve").Value
                        objMatrix1.Columns.Item("V_10").Cells.Item(objMatrix1.RowCount).Specific.value = objRS1.Fields.Item("U_percent").Value
                        objCheck = objMatrix1.Columns.Item("V_0").Cells.Item(objMatrix1.RowCount).Specific
                        If objRS1.Fields.Item("U_pay").Value = "Y" Then
                            objCheck.Checked = True
                        Else
                            objCheck.Checked = False
                        End If
                        objMatrix1.Columns.Item("V_11").Cells.Item(objMatrix1.RowCount).Specific.value = objRS1.Fields.Item("U_tot").Value
                    End If
                    objRS.MoveNext()
                Next
            End If
            objForm.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage("Values are Loaded", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub
#End Region

#Region "Load the values Half Yearly"
    Private Sub Emp_To_Matrix2(ByVal Value As String)
        Try
            objcombomonth2 = objForm.Items.Item("13").Specific
            Dim objRS, objRS1 As SAPbobsCOM.Recordset
            Dim Str, StrSQL, EID, EName As String
            objAddOn.objApplication.SetStatusBarMessage("Please Wait While Loading the Data....", SAPbouiCOM.BoMessageTime.bmt_Long, False)
            objForm.Freeze(True)
            Str = "select empID as EmpID,lastName+' '+ firstName as EmpName from ohem where U_incent='" & Value & "'"
            objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objRS.DoQuery(Str)
            If objRS.RecordCount > 0 Then
                Dim i As Integer
                For i = 1 To objRS.RecordCount
                    objMatrix2.AddRow()
                    EID = objRS.Fields.Item("EmpID").Value
                    EName = objRS.Fields.Item("EmpName").Value
                    objMatrix2.Columns.Item("V_-1").Cells.Item(objMatrix2.RowCount).Specific.value = objMatrix2.RowCount
                    objMatrix2.Columns.Item("V_7").Cells.Item(objMatrix2.RowCount).Specific.value = EID
                    objMatrix2.Columns.Item("V_6").Cells.Item(objMatrix2.RowCount).Specific.value = EName
                    objMatrix2.Columns.Item("V_5").Cells.Item(objMatrix2.RowCount).Specific.value = objcombomonth2.Selected.Value
                    objMatrix2.Columns.Item("V_4").Cells.Item(objMatrix2.RowCount).Specific.value = objComboYear.Selected.Value

                    StrSQL = "select T1.U_type ,T1.U_month ,T1.U_year ,T0 .U_empid ,T0.U_empname,T0.U_month,"
                    StrSQL += "T0.U_pay,T0.U_tot ,T0.U_percent from [@AIS_VPAYH] T0 "
                    StrSQL += "join [@AIS_VPAY] T1 on T0.Code =T1.Code where T0.U_empid='" & EID & "' and T1.U_hmonth='" & objcombomonth2.Selected.Value & "' and T1.U_year='" & objComboYear.Selected.Value & "'"
                    objRS1.DoQuery(StrSQL)
                    If objRS1.RecordCount > 0 Then
                        objMatrix2.Columns.Item("V_7").Cells.Item(objMatrix2.RowCount).Specific.value = objRS1.Fields.Item("U_empid").Value
                        objMatrix2.Columns.Item("V_6").Cells.Item(objMatrix2.RowCount).Specific.value = objRS1.Fields.Item("U_empname").Value
                        objMatrix2.Columns.Item("V_5").Cells.Item(objMatrix2.RowCount).Specific.value = objRS1.Fields.Item("U_month").Value
                        objMatrix2.Columns.Item("V_4").Cells.Item(objMatrix2.RowCount).Specific.value = objRS1.Fields.Item("U_year").Value
                        objMatrix2.Columns.Item("V_3").Cells.Item(objMatrix2.RowCount).Specific.value = objRS1.Fields.Item("U_percent").Value
                        objCheck = objMatrix2.Columns.Item("V_1").Cells.Item(objMatrix2.RowCount).Specific
                        If objRS1.Fields.Item("U_pay").Value = "Y" Then
                            objCheck.Checked = True
                        Else
                            objCheck.Checked = False
                        End If
                        objMatrix2.Columns.Item("V_2").Cells.Item(objMatrix2.RowCount).Specific.value = objRS1.Fields.Item("U_tot").Value
                    End If
                    objRS.MoveNext()
                Next
            End If
            objForm.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage("Values are Loaded", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub
#End Region

#Region "EmpID_CFL"
    Public Sub EmpidCFL(ByRef pval As SAPbouiCOM.ItemEvent)
        objCFLEvent = pval
        objDataTable = objCFLEvent.SelectedObjects
        If objDataTable Is Nothing Then
        Else
            Try
                objMatrix.Columns.Item("V_4").Cells.Item(pval.Row).Specific.value = objDataTable.GetValue("empID", 0)
            Catch ex As Exception
                objMatrix.Columns.Item("V_3").Cells.Item(pval.Row).Specific.value = objDataTable.GetValue("firstName", 0) + " " + objDataTable.GetValue("lastName", 0)
            End Try
        End If
    End Sub
#End Region

#Region "EmpID_CFL1"
    Public Sub EmpidCFL1(ByRef pval As SAPbouiCOM.ItemEvent)
        objCFLEvent = pval
        objDataTable = objCFLEvent.SelectedObjects
        If objDataTable Is Nothing Then
        Else
            Try
                objMatrix1.Columns.Item("V_7").Cells.Item(pval.Row).Specific.value = objDataTable.GetValue("empID", 0)
            Catch ex As Exception
                objMatrix1.Columns.Item("V_6").Cells.Item(pval.Row).Specific.value = objDataTable.GetValue("firstName", 0) + " " + objDataTable.GetValue("lastName", 0)
            End Try
        End If
    End Sub
#End Region

#Region "Combo_Month"
    Public Sub Combo_Month()
        Try
            If objComboMonth.ValidValues.Count > 0 Then
                Dim J As Integer
                For J = objComboMonth.ValidValues.Count - 1 To 0 Step -1
                    objComboMonth.ValidValues.Remove(J, SAPbouiCOM.BoSearchKey.psk_Index)
                Next
            End If
            objComboMonth.ValidValues.Add("January", "01")
            objComboMonth.ValidValues.Add("February", "02")
            objComboMonth.ValidValues.Add("March", "03")
            objComboMonth.ValidValues.Add("April", "04")
            objComboMonth.ValidValues.Add("May", "05")
            objComboMonth.ValidValues.Add("June", "06")
            objComboMonth.ValidValues.Add("July", "07")
            objComboMonth.ValidValues.Add("August", "08")
            objComboMonth.ValidValues.Add("September", "09")
            objComboMonth.ValidValues.Add("October", "10")
            objComboMonth.ValidValues.Add("November", "11")
            objComboMonth.ValidValues.Add("December", "12")
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub
#End Region

#Region "Combo_Quarter"
    Public Sub Combo_Quarter()
        Try
            If objComboMonth1.ValidValues.Count > 0 Then
                Dim J As Integer
                For J = objComboMonth1.ValidValues.Count - 1 To 0 Step -1
                    objComboMonth1.ValidValues.Remove(J, SAPbouiCOM.BoSearchKey.psk_Index)
                Next
            End If
            objComboMonth1.ValidValues.Add("Jan-March", "01")
            objComboMonth1.ValidValues.Add("April-June", "02")
            objComboMonth1.ValidValues.Add("July-Sep", "03")
            objComboMonth1.ValidValues.Add("Oct-Dec", "04")
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub
    Private Sub Combo_Halfyear()
        objcombomonth2 = objForm.Items.Item("13").Specific
        Try
            While objcombomonth2.ValidValues.Count > 0
                objcombomonth2.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
            End While
            Dim Orec As SAPbobsCOM.Recordset
            Orec = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            Orec.DoQuery("select Code,Name from [@AIS_HALF]")
            While Not Orec.EoF
                objcombomonth2.ValidValues.Add(Orec.Fields.Item("Code").Value.ToString, Orec.Fields.Item("Name").Value.ToString)
                Orec.MoveNext()
            End While
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub
#End Region

#Region "Combo Year"
    Public Sub Combo_Year()
        Try
            If objComboYear.ValidValues.Count > 0 Then
                Dim J As Integer
                For J = objComboYear.ValidValues.Count - 1 To 0 Step -1
                    objComboYear.ValidValues.Remove(J, SAPbouiCOM.BoSearchKey.psk_Index)
                Next
            End If
            objComboYear.ValidValues.Add("2013", "2013")
            objComboYear.ValidValues.Add("2014", "2014")
            objComboYear.ValidValues.Add("2015", "2015")
            objComboYear.ValidValues.Add("2016", "2016")
            objComboYear.ValidValues.Add("2017", "2017")
            objComboYear.ValidValues.Add("2018", "2018")
            objComboYear.ValidValues.Add("2019", "2019")
            objComboYear.ValidValues.Add("2020", "2020")
            objComboYear.ValidValues.Add("2021", "2021")
            objComboYear.ValidValues.Add("2022", "2022")
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub
#End Region

#Region "Percentage Calculation"
    Public Sub Percentage_Calc(ByVal Row As Integer)
        Try
            Dim percentage As Decimal
            Dim objRS3 As SAPbobsCOM.Recordset
            Dim strSQL3, Amount As String
            objRS3 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strSQL3 = "select T0.U_amt  from [@AIS_EPSSTP5] T0 join [@AIS_EMPSSTP] T1 on T0.Code=T1.Code where T1.U_eid ='" & objMatrix.Columns.Item("V_4").Cells.Item(Row).Specific.string & "' and T0.U_descrpn ='IPLC'"
            objRS3.DoQuery(strSQL3)
            Amount = objRS3.Fields.Item("U_amt").Value
            If Amount <> "" Or Amount <> 0 Then
                percentage = objMatrix.Columns.Item("V_6").Cells.Item(Row).Specific.value
                If percentage < 75 Then
                    objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.value = 0
                ElseIf percentage > 125 Then
                    objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.value = Amount * 125 / 100
                Else
                    objMatrix.Columns.Item("V_7").Cells.Item(Row).Specific.value = Amount * percentage / 100
                End If
            End If

            objForm.Refresh()
            objForm.Update()

        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub
#End Region

#Region "Percentage Calculation"
    Public Sub Percentage_Calc1(ByVal Row As Integer)
        Try
            Dim percentage, M1, M2, M3 As Decimal
            Dim objRS3 As SAPbobsCOM.Recordset
            Dim strSQL3, Amount As String
            M1 = objMatrix1.Columns.Item("V_2").Cells.Item(Row).Specific.value
            M2 = objMatrix1.Columns.Item("V_3").Cells.Item(Row).Specific.value
            M3 = objMatrix1.Columns.Item("V_4").Cells.Item(Row).Specific.value
            objMatrix1.Columns.Item("V_10").Cells.Item(Row).Specific.value = M1 + M2 + M3
            percentage = objMatrix1.Columns.Item("V_10").Cells.Item(Row).Specific.value

            objRS3 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strSQL3 = "select T0.U_amt  from [@AIS_EPSSTP5] T0 join [@AIS_EMPSSTP] T1 on T0.Code=T1.Code where T1.U_eid ='" & objMatrix1.Columns.Item("V_7").Cells.Item(Row).Specific.string & "' and T0.U_descrpn ='IPLC'"
            objRS3.DoQuery(strSQL3)
            Amount = objRS3.Fields.Item("U_amt").Value
            If Amount <> "" Or Amount <> 0 Then
                If percentage < 225 Then
                    objMatrix1.Columns.Item("V_11").Cells.Item(Row).Specific.value = 0
                ElseIf percentage > 375 Then
                    objMatrix1.Columns.Item("V_11").Cells.Item(Row).Specific.value = Amount * 375 / 100
                Else
                    objMatrix1.Columns.Item("V_11").Cells.Item(Row).Specific.value = Amount * percentage / 100

                End If

            End If

            objForm.Refresh()
            objForm.Update()

        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub
#End Region

#Region "Line Total"
    Public Sub LineTotal(ByVal RowNo)
        Dim IntA As Double
        Dim IntB As Double
        Dim IntC As Double
        Dim IntTarget As Double
        IntA = objMatrix1.Columns.Item("V_4").Cells.Item(RowNo).Specific.value
        IntB = objMatrix1.Columns.Item("V_3").Cells.Item(RowNo).Specific.value
        IntC = objMatrix1.Columns.Item("V_2").Cells.Item(RowNo).Specific.value
        IntTarget = IntA + IntB + IntC
        objForm.Update()
        objForm.Refresh()
        objMatrix1.Columns.Item("V_1").Cells.Item(RowNo).Specific.value = IntTarget
        objForm.Update()
        objForm.Refresh()
    End Sub
#End Region

#Region "Validation"
    Public Function Validation() As Boolean

        If objComboMonth.Selected Is Nothing Then
            If objComboVPay.Selected.Value = "M" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select the Month", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
        ElseIf objComboMonth1.Selected Is Nothing Then
            If objComboVPay.Selected.Value = "Q" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select the Month", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If

        ElseIf objComboYear.Selected Is Nothing Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select the Year", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
            'ElseIf objMatrix.RowCount > 0 Then
            '    If objForm.Items.Item("5").Visible = True Then
            '        objMatrix = objForm.Items.Item("5").Specific
            '        If objMatrix.Columns.Item("V_4").Cells.Item(1).Specific.value = "" Then
            '            objAddOn.objApplication.SetStatusBarMessage("Please Select an Employee", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '            Return False
            '        End If
            '    End If
        End If
        Return True
    End Function
#End Region



End Class
