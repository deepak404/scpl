﻿Public Class clsprocesscycle
    Dim objForm As SAPbouiCOM.Form
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim strSQL As String
    Dim objRS As SAPbobsCOM.Recordset
    Public Sub define()
        Dim i As Integer
        Dim TableName As String
        For i = 0 To objAddOn.objApplication.Menus.Item("51200").SubMenus.Count - 1
            TableName = objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(i).String
            TableName = TableName.Remove(TableName.IndexOf("-"))
            If Trim(TableName) = "AIS_PDSPC" Then
                objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(i).Activate()
                PCYC = objAddOn.objApplication.Forms.ActiveForm.TypeEx
                objForm = objAddOn.objApplication.Forms.ActiveForm
                'objMatrix = objForm.Items.Item("3").Specific
                'objMatrix.Columns.Item("U_proday").Editable = False
                Exit For
            End If
        Next i


    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    objForm = objAddOn.objApplication.Forms.Item(FormUID)
                    objMatrix = objForm.Items.Item("3").Specific
                    If pVal.ColUID = "U_todt" Or pVal.ColUID = "U_frmdt" Then
                        If objMatrix.Columns.Item("U_todt").Cells.Item(pVal.Row).Specific.value <> "" Then
                            If validate(FormUID, pVal.Row) = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If

                    End If
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    objForm = objAddOn.objApplication.Forms.Item(FormUID)
                    objMatrix = objForm.Items.Item("3").Specific
                    If pVal.ColUID = "U_todt" Or pVal.ColUID = "U_frmdt" Then
                        If (objMatrix.Columns.Item("U_todt").Cells.Item(pVal.Row).Specific.value <> "" Or objMatrix.Columns.Item("U_frmdt").Cells.Item(pVal.Row).Specific.value <> "") Then
                            If validate(FormUID, pVal.Row) = True Then
                                calc(pVal.Row)
                            End If
                        End If
                    End If
            End Select
        End If

    End Sub

#Region "Validate Function"
    Public Function validate(ByVal FormUID As String, ByVal RowNo As Integer) As Boolean
        objMatrix = objForm.Items.Item("3").Specific
        If objMatrix.Columns.Item("U_frmdt").Cells.Item(RowNo).Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter From Date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objMatrix.Columns.Item("U_todt").Cells.Item(RowNo).Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter To Date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objMatrix.Columns.Item("U_frmdt").Cells.Item(RowNo).Specific.string <> "" And objMatrix.Columns.Item("U_todt").Cells.Item(RowNo).Specific.value <> "" Then
            If objMatrix.Columns.Item("U_frmdt").Cells.Item(RowNo).Specific.value() > objMatrix.Columns.Item("U_todt").Cells.Item(RowNo).Specific.value() Then
                objAddOn.objApplication.SetStatusBarMessage("Please Check To Date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
        End If
        Return True
    End Function
#End Region

#Region "Calculate the Date"
    Public Sub calc(ByVal RowNo As Integer)
        objMatrix = objForm.Items.Item("3").Specific
        strSQL = "select DATEDIFF(dd,'" & CDate(objMatrix.Columns.Item("U_frmdt").Cells.Item(RowNo).Specific.string).ToString("yyyyMMdd") & "','" & CDate(objMatrix.Columns.Item("U_todt").Cells.Item(RowNo).Specific.string).ToString("yyyyMMdd") & "') +1'Day'"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        ' objMatrix.Columns.Item("U_proday").Editable = True

        objMatrix.Columns.Item("U_proday").Cells.Item(RowNo).Specific.value = objRS.Fields.Item("Day").Value
        '  objMatrix.Columns.Item("U_proday").Editable = False
        objForm.Update()
        objForm.Refresh()
    End Sub
#End Region

#Region "Load the Values to DB as Jan to Dec"
    Public Sub LoadValuestoDB()
        Dim objRS As SAPbobsCOM.Recordset
        Dim str As String

        str = "select * from [@AIS_PDSPC]"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(str)
        If objRS.RecordCount < 12 Then
            str = "insert into [@AIS_PDSPC](Code ,Name ) values('01','January')"
            str += vbCrLf + "insert into [@AIS_PDSPC](Code ,Name ) values('02','February')"
            str += vbCrLf + "insert into [@AIS_PDSPC](Code ,Name ) values('03','March')"
            str += vbCrLf + "insert into [@AIS_PDSPC](Code ,Name ) values('04','April')"
            str += vbCrLf + "insert into [@AIS_PDSPC](Code ,Name ) values('05','May')"
            str += vbCrLf + "insert into [@AIS_PDSPC](Code ,Name ) values('06','June')"
            str += vbCrLf + "insert into [@AIS_PDSPC](Code ,Name ) values('07','July')"
            str += vbCrLf + "insert into [@AIS_PDSPC](Code ,Name ) values('08','August')"
            str += vbCrLf + "insert into [@AIS_PDSPC](Code ,Name ) values('09','September')"
            str += vbCrLf + "insert into [@AIS_PDSPC](Code ,Name ) values('10','October')"
            str += vbCrLf + "insert into [@AIS_PDSPC](Code ,Name ) values('11','November')"
            str += vbCrLf + "insert into [@AIS_PDSPC](Code ,Name ) values('12','December')"
            objRS.DoQuery(str)
        End If
    End Sub
#End Region
  
End Class
        
