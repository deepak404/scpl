﻿Public Class clsWorkhourstp
    Public objForm As SAPbouiCOM.Form
    Public Const Formtype = "wrkhurstp"
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim objCFLEvent As SAPbouiCOM.ChooseFromListEvent
    Dim objDataTable As SAPbouiCOM.DataTable
    Dim objCombo As SAPbouiCOM.ComboBox
    Dim objRS As SAPbobsCOM.Recordset
    Dim strSQL, SEndDate As String
    Dim intLoop As Integer
    Dim dept As String

#Region "Loadscreen"
    Public Sub Loadscreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("workhourstp.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, Formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.Items.Item("4").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_WRKHRSTP")

        objForm.ActiveItem = 6

        objMatrix = objForm.Items.Item("13").Specific
        objForm.Items.Item("10").Specific.value = Today.Date
        WorkCode()
        Department()
    End Sub
#End Region

#Region "Department"
    Public Sub Department()
        Dim intloop As Integer
        objCombo = objForm.Items.Item("18").Specific
        strSQL = "select Code,Name from OUDP "
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        'objCombo.ExpandType = SAPbouiCOM.BoExpandType.et_ValueOnly
        For intloop = 1 To objRS.RecordCount
            objCombo.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next
    End Sub
#End Region

#Region "Itemevent"
    Public Sub Itemevent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction Then
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                    If pVal.ItemUID = "13" And (pVal.ColUID = "V_0" Or pVal.ColUID = "V_3") And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        objMatrix = objForm.Items.Item("13").Specific
                        If objMatrix.Columns.Item("V_0").Cells.Item(objMatrix.VisualRowCount).Specific.String <> "" Or objMatrix.Columns.Item("V_0").Cells.Item(objMatrix.VisualRowCount).Specific.String <> "" Then
                            objMatrix.AddRow()
                            objMatrix.ClearRowData(objMatrix.VisualRowCount)
                            objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.VisualRowCount).Specific.value = objMatrix.VisualRowCount
                        End If
                    End If
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If ValidateFun() Then
                        Else
                            BubbleEvent = False
                        End If
                    End If
                    If pVal.ItemUID = "1" And (objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If MatrixValidationFun() Then
                        Else
                            BubbleEvent = False
                        End If
                    End If


            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE And pVal.ActionSuccess = True Then
                        objForm.Items.Item("4").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_WRKHRSTP")
                    End If

                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    objMatrix = objForm.Items.Item("13").Specific
                    objCFLEvent = pVal
                    objDataTable = objCFLEvent.SelectedObjects
                    If pVal.ItemUID = "13" And pVal.ColUID = "V_0" Then
                        EmpidCFL(pVal.Row)
                        DateFun(pVal.Row)
                    End If
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    If pVal.ItemUID = "6" Then
                        If objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            objMatrix = objForm.Items.Item("13").Specific
                            If Not objMatrix.VisualRowCount > 0 Then
                                objMatrix.AddRow()
                                objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.RowCount).Specific.value = objMatrix.VisualRowCount
                            End If
                        End If
                    End If
                    If pVal.ItemUID = "15" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        WorkCodeLoop()
                    End If

                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT

                    objCombo = objForm.Items.Item("18").Specific
                    If objCombo.Selected Is Nothing Then
                    Else
                        dept = objCombo.Selected.Value
                        DeptSelection(dept)
                    End If

            End Select
        End If
    End Sub
#End Region

    Public Sub DeptSelection(ByVal dept As String)
        Dim Objcfl As SAPbouiCOM.ChooseFromList
        Dim objChooseCollection As SAPbouiCOM.ChooseFromListCollection
        Dim objConditions As SAPbouiCOM.Conditions
        Dim objcondition As SAPbouiCOM.Condition

        objChooseCollection = objForm.ChooseFromLists
        Objcfl = objChooseCollection.Item("CFL_2")
        objConditions = Objcfl.GetConditions()
        objcondition = objConditions.Add()
        objcondition.Alias = "dept"
        objcondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
        objcondition.CondVal = dept
        Objcfl.SetConditions(objConditions)
    End Sub


#Region "Emp id cfl"
    Public Sub EmpidCFL(ByVal Row As Integer)
        Dim i As Integer
        For i = 0 To objDataTable.Rows.Count - 1
            Try
                objMatrix.Columns.Item("V_0").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("empID", i)
            Catch ex As Exception
            End Try
            objMatrix.Columns.Item("V_5").Cells.Item(Row + i).Specific.value = objDataTable.GetValue("firstName", i)
            objMatrix.AddRow()
            MatrixNo()
        Next i
    End Sub
#End Region

#Region "Date function"
    Public Sub DateFun(ByVal Row As Integer)
        Dim i As Integer

        For i = 0 To objMatrix.VisualRowCount - 2

            Try
                objMatrix.Columns.Item("V_4").Cells.Item(Row + i).Specific.value = objForm.Items.Item("6").Specific.value
                objMatrix.Columns.Item("V_3").Cells.Item(Row + i).Specific.value = objForm.Items.Item("12").Specific.value
                objCombo = objMatrix.Columns.Item("V_2").Cells.Item(Row + i).Specific
                objMatrix.Columns.Item("V_2").Cells.Item(Row + i).Specific.select(objForm.Items.Item("15").Specific.Selected.value, SAPbouiCOM.BoSearchKey.psk_ByValue)

            Catch ex As Exception
            End Try
        Next i
    End Sub
#End Region

#Region "Workhurcode"
    Public Sub WorkCode()
        objCombo = objForm.Items.Item("15").Specific
        While objCombo.ValidValues.Count > 0
            objCombo.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index)
        End While
        strSQL = "select code,Name from [@AIS_DWHS] "
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        For Me.intLoop = 1 To objRS.RecordCount
            objCombo.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objMatrix.Columns.Item("V_2").ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next intLoop
    End Sub
#End Region

#Region "Matrix_sno"
    Public Sub MatrixNo()
        objMatrix = objForm.Items.Item("13").Specific
        For Me.intLoop = 1 To objMatrix.VisualRowCount
            objMatrix.Columns.Item("V_-1").Cells.Item(intLoop).Specific.value = intLoop
        Next intLoop
    End Sub
#End Region

#Region "WorkhurCodeLoop"
    Public Sub WorkCodeLoop()
        objCombo = objMatrix.Columns.Item("V_2").Cells.Item(1).Specific
        For Me.intLoop = 1 To objMatrix.VisualRowCount
            Try
                objMatrix.Columns.Item("V_2").Cells.Item(intLoop).Specific.select(objForm.Items.Item("15").Specific.Selected.value, SAPbouiCOM.BoSearchKey.psk_ByValue)
            Catch ex As Exception
            End Try
        Next
    End Sub
#End Region

#Region "Validation"
    Public Function ValidateFun()
        If objForm.Items.Item("6").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Schedule start date is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("12").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("End date is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("15").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Work hour code is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("12").Specific.value < objForm.Items.Item("6").Specific.value Then
            objAddOn.objApplication.SetStatusBarMessage("End Date should be Greater than Start Date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Matrix Validation"
    Public Function MatrixValidationFun()
        objMatrix = objForm.Items.Item("13").Specific
        SEndDate = objForm.Items.Item("12").Specific.value
        If objMatrix.VisualRowCount > 0 Then
            For Me.intLoop = 1 To objMatrix.VisualRowCount - 1
                If objMatrix.Columns.Item("V_0").Cells.Item(intLoop).Specific.value = "" Then
                    objAddOn.objApplication.SetStatusBarMessage(" Enter the ItemCode at line- " & intLoop, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                ElseIf SEndDate < objMatrix.Columns.Item("V_3").Cells.Item(intLoop).Specific.value Then
                    objAddOn.objApplication.SetStatusBarMessage("Schedule End Date & End Date should be same at Line-" & intLoop, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False

                End If
            Next intLoop
            If objMatrix.Columns.Item("V_0").Cells.Item(objMatrix.VisualRowCount).Specific.value = "" Then
                objMatrix.DeleteRow(objMatrix.VisualRowCount)
            End If
            If objMatrix.VisualRowCount = 0 Then
                objMatrix.AddRow()
                objMatrix.Columns.Item("V_-1").Cells.Item(objMatrix.VisualRowCount).Specific.String = objMatrix.VisualRowCount
                objAddOn.objApplication.SetStatusBarMessage("Please Enter Atleast One Row", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
                If SEndDate < objMatrix.Columns.Item("V_3").Cells.Item(1).Specific.value Then
                    objAddOn.objApplication.SetStatusBarMessage("Schedule End Date & End Date should be same at Line-" & 1, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If
        End If
        Return True
    End Function
#End Region

    Public Sub SerialNo()
        objForm.Items.Item("4").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_WRKHRSTP")
    End Sub
End Class
