﻿Public Class ClsDefineWorkHours
    Public Const formtype = "DefineWorkHours"
    Dim objForm As SAPbouiCOM.Form
    Dim objItem As SAPbouiCOM.Item
    Dim oOverLap, oOT, oFlex As SAPbouiCOM.CheckBox

    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("DefineWorkHours.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.Items.Item("4").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
        ' objForm.ActiveItem = 4
        '' objForm.EnableMenu("1281", True)
        objAddOn.objApplication.Menus.Item("1281").Enabled = True
        ' objAddOn.objApplication.Menus.Item("1282").Enabled = True
        ' objForm.SupportedModes = -1
        objForm.DataBrowser.BrowseBy = "19"
        objForm.AutoManaged = True
        objItem = objForm.Items.Item("4")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        objItem = objForm.Items.Item("6")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            If FormValidation() = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If
                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        objForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            objForm.Items.Item("4").Enabled = False
                            objForm.Items.Item("6").Enabled = False
                        End If
                End Select
            End If
        Catch ex As Exception

        End Try

    End Sub


#Region "Validation"
    Public Function FormValidation() As Boolean
        oOverLap = objForm.Items.Item("16").Specific
        oOT = objForm.Items.Item("17").Specific
        oFlex = objForm.Items.Item("15").Specific
        If objForm.Items.Item("4").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Code is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("6").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Name is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("8").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Shift start Time  is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("14").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Shift Hour is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("10").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Break Start time is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("12").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Break End time is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
            'ElseIf objForm.Items.Item("8").Specific.Value > objForm.Items.Item("10").Specific.Value Then
            '    objAddOn.objApplication.SetStatusBarMessage("Shift Start Time should be less than Break Start Time", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '    Return False
            'ElseIf objForm.Items.Item("8").Specific.Value > objForm.Items.Item("12").Specific.Value Then
            '    objAddOn.objApplication.SetStatusBarMessage("Shift Start Time should be less than Break End Time", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '    Return False
            'ElseIf objForm.Items.Item("10").Specific.Value > objForm.Items.Item("12").Specific.Value Then
            '    objAddOn.objApplication.SetStatusBarMessage("Break Start Time should be less than Break End Time", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            '    Return False
        ElseIf oFlex.Checked = True And (oOT.Checked = True Or oOverLap.Checked = True) Then
            objAddOn.objApplication.SetStatusBarMessage("Please Check the Flexi Hours Only", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region
End Class
