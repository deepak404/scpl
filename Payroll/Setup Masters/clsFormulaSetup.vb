﻿Imports System.Text.RegularExpressions
Public Class clsFormulaSetup
    Public Const Formtype = "MNU_FORMUL"
    Dim objForm As SAPbouiCOM.Form
    Dim objCombo As SAPbouiCOM.ComboBox
    Dim objCbo As SAPbouiCOM.ComboBox
    Dim objPayCmb As SAPbouiCOM.ComboBox
    Dim objDedCmb As SAPbouiCOM.ComboBox
    Dim objStatc As SAPbouiCOM.StaticText
    Dim objEdit As SAPbouiCOM.EditText
    Dim objbutton As SAPbouiCOM.Button

#Region "Loadscreen"
    Public Sub loadscreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("basicformula.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, Formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.DataBrowser.BrowseBy = "4"
        'objAddOn.objApplication.Menus.Item("1282").Enabled = True
        ' objForm.ActiveItem = "4"
        objForm.Items.Item("4").Click()

        '   objAddOn.objApplication.Menus.Item("1281").Enabled = True
        Combo()
        Cmb()
        PayCombo()
        VariablePay()
        OTCode()
        VariableDed()
        BenefitCode()
        DedCombo()
        Numbers()
    End Sub
#End Region

#Region "Load the All CombomBox Values"

#Region "Varibale Pay Code"
    Public Sub VariablePay()
        Dim objRS As SAPbobsCOM.Recordset
        Dim strSQL As String
        Dim intLoop As Integer
        objPayCmb = objForm.Items.Item("10").Specific
        objPayCmb.ExpandType = SAPbouiCOM.BoExpandType.et_ValueDescription
        strSQL = "select Code,Name from [@AIS_Paycode] "
        'strSQL = "select AliasID,Descr from CUFD where TableID='@AIS_Paycode' and FieldID<9"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        For intLoop = 1 To objRS.RecordCount
            objPayCmb.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next intLoop
    End Sub
#End Region

#Region "Over Time Code"
    Public Sub OTCode()
        Dim objRS As SAPbobsCOM.Recordset
        Dim strSQL As String
        Dim intLoop As Integer
        objPayCmb = objForm.Items.Item("12").Specific
        objPayCmb.ExpandType = SAPbouiCOM.BoExpandType.et_ValueDescription
        strSQL = "select Code,Name from [@AIS_OVERTIME] "
        'strSQL = "select AliasID,Descr from CUFD where TableID='@AIS_Paycode' and FieldID<9"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        For intLoop = 1 To objRS.RecordCount
            objPayCmb.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next intLoop
    End Sub
#End Region

#Region "Variable DeductionCode"
    Public Sub VariableDed()
        Dim strSQL As String
        Dim intLoop As Integer
        Dim objRS As SAPbobsCOM.Recordset
        objDedCmb = objForm.Items.Item("14").Specific
        objDedCmb.ExpandType = SAPbouiCOM.BoExpandType.et_ValueDescription
        strSQL = "select Code,Name from [@AIS_DDC]"
        ' strSQL = "select AliasID,Descr from CUFD where TableID='@AIS_DDC' and FieldID<9"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        For intLoop = 1 To objRS.RecordCount
            objDedCmb.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next intLoop

    End Sub
#End Region

#Region "Benefit Code"
    Public Sub BenefitCode()
        Dim objRS As SAPbobsCOM.Recordset
        Dim strSQL As String
        Dim intLoop As Integer
        objPayCmb = objForm.Items.Item("16").Specific
        objPayCmb.ExpandType = SAPbouiCOM.BoExpandType.et_ValueDescription
        strSQL = "select Code,Name from [@AIS_BCODE] "
        'strSQL = "select AliasID,Descr from CUFD where TableID='@AIS_Paycode' and FieldID<9"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        For intLoop = 1 To objRS.RecordCount
            objPayCmb.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next intLoop
    End Sub
#End Region

#Region "Combo"
    Public Sub Combo()
        objCombo = objForm.Items.Item("20").Specific
        'objCombo.ExpandType = SAPbouiCOM.BoExpandType.et_ValueOnly
        objCombo.ValidValues.Add("+", "")
        objCombo.ValidValues.Add("-", "")
        objCombo.ValidValues.Add("*", "")
        objCombo.ValidValues.Add("/", "")
        objCombo.ValidValues.Add("%", "")
        objCombo.ValidValues.Add("(", "")
        objCombo.ValidValues.Add(")", "")
        objCombo.ValidValues.Add("Round Number,Decimals as Number", "")
        objCombo.ValidValues.Add("Round Number,Type", "")
        objCombo.ValidValues.Add("<", "")
        objCombo.ValidValues.Add("<=", "")
        objCombo.ValidValues.Add("==", "")
        objCombo.ValidValues.Add("!=", "")
        objCombo.ValidValues.Add(">", "")
        objCombo.ValidValues.Add(">=", "")
        objCombo.ValidValues.Add("if () {} else {}", "")
    End Sub
#End Region

#Region "Cmb"
    Public Sub Cmb()
        objCbo = objForm.Items.Item("8").Specific
        objCbo.ExpandType = SAPbouiCOM.BoExpandType.et_ValueOnly
        objCbo.ValidValues.Add("Pay Code", "")
        objCbo.ValidValues.Add("Deduction Code", "")
        objCbo.ValidValues.Add("Exemption", "")
    End Sub
#End Region

#Region "Pay Combo"
    Public Sub PayCombo()
        Dim objRS As SAPbobsCOM.Recordset
        Dim strSQL As String
        Dim intLoop As Integer
        objPayCmb = objForm.Items.Item("18").Specific
        objPayCmb.ExpandType = SAPbouiCOM.BoExpandType.et_ValueDescription
        strSQL = "select Code,Name from [@AIS_Paycode] "
        'strSQL = "select AliasID,Descr from CUFD where TableID='@AIS_Paycode' and FieldID<9"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        For intLoop = 1 To objRS.RecordCount
            objPayCmb.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next intLoop

    End Sub
#End Region

#Region "Dedcombo"
    Public Sub DedCombo()
        Dim strSQL As String
        Dim intLoop As Integer
        Dim objRS As SAPbobsCOM.Recordset
        objDedCmb = objForm.Items.Item("26").Specific
        objDedCmb.ExpandType = SAPbouiCOM.BoExpandType.et_ValueDescription
        strSQL = "select Code,Name from [@AIS_DDC]"
        ' strSQL = "select AliasID,Descr from CUFD where TableID='@AIS_DDC' and FieldID<9"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        For intLoop = 1 To objRS.RecordCount
            objDedCmb.ValidValues.Add(objRS.Fields.Item("Code").Value, objRS.Fields.Item("Name").Value)
            objRS.MoveNext()
        Next intLoop

    End Sub
#End Region

#Region "Numbers in Combo Box"
    Public Sub Numbers()
        objCombo = objForm.Items.Item("22").Specific
        objCombo.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly
        objCombo.ValidValues.Add("0", "0")
        objCombo.ValidValues.Add("1", "1")
        objCombo.ValidValues.Add("2", "2")
        objCombo.ValidValues.Add("3", "3")
        objCombo.ValidValues.Add("4", "4")
        objCombo.ValidValues.Add("5", "5")
        objCombo.ValidValues.Add("6", "6")
        objCombo.ValidValues.Add("7", "7")
        objCombo.ValidValues.Add("8", "8")
        objCombo.ValidValues.Add("9", "9")
    End Sub
#End Region

#End Region

#Region "addmode"
    Public Sub addmode()
        objForm.ActiveItem = "27"
        objForm.Items.Item("27").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_FORMULA")
        objForm.ActiveItem = "4"
    End Sub
#End Region

#Region "Find mode"
    Public Sub findmode()
        objForm.ActiveItem = "4"
    End Sub
#End Region


    Public Sub itemevent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        If validation(FormUID, pVal) = False Then
                            BubbleEvent = False
                            Exit Sub
                        End If
                    End If
                    If pVal.ItemUID = "24" Then
                        Dim Formula As String = objForm.Items.Item("25").Specific.string
                        ValidateFormula(FormUID, Formula)
                    End If
            End Select
        Else
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    ComboChange()
                    If objForm.Items.Item("18").Visible = True Then 'PayCode()
                    ElseIf objForm.Items.Item("26").Visible = True Then 'DeductionCode()
                    End If
                    Formula(FormUID)

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        objForm.Items.Item("27").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_FORMULA")
                        objForm.ActiveItem = "4"
                    End If
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "24" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        If objCbo.Selected.Value = "Pay Code" Then
                            If PayValidate(FormUID, pVal) = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                        ElseIf objCbo.Selected.Value = "Deduction Code" Then
                            If DedValidate(FormUID, pVal) = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If

                        End If

                    End If
            End Select
        End If
    End Sub

#Region "Validate Formula"
    Public Function ValidateFormula(ByVal FormUID As String, ByVal Formula As String) As Boolean
        Try

            ' Dim re As Regex = New Regex("^1?\s*-?\s*(\d{3}|\(\s*\d{3}\s*\))\s*-?\s*\d{3}\s*-?\s*\d{4}$")

            Dim re As Regex = New Regex("^(\S*) - - \[(.*) .....\] \....? (\S*) .*\ (\d*) ([-0-9]*) (\([^]+)\)?")
            '  re.RightToLeft = "^1?\s*-?\s*(\d{3}|\(\s*\d{3}\s*\))\s*-?\s*\d{3}\s*-?\s*\d{4}$"
            'Debug.Print(re.Test(t))
            ' re.Match(Formula)
            Dim M As Match = re.Match(Formula)
            If M.Success Then
                MsgBox("success")
            Else
                MsgBox("Failure")
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Function
#End Region


#Region "Formula"
    Public Sub Formula(ByVal FormUID As String)
        Dim PCode, DCode, OT, BCode, Number, Operation As SAPbouiCOM.ComboBox
        Dim PValue, DValue, OTValue, BValue, OValue, Num, Formula As String
        PCode = objForm.Items.Item("10").Specific
        DCode = objForm.Items.Item("14").Specific
        OT = objForm.Items.Item("12").Specific
        BCode = objForm.Items.Item("16").Specific
        Number = objForm.Items.Item("22").Specific
        Operation = objForm.Items.Item("20").Specific
        If PCode.Selected Is Nothing Then
            PValue = ""
        Else
            PValue = PCode.Selected.Description
            Formula = objForm.Items.Item("25").Specific.string + "{" + PValue + "}"
            'PValue.Remove(0)
            objForm.Items.Item("25").Specific.value = Formula

        End If

        If DCode.Selected Is Nothing Then
            DValue = ""
        Else
            DValue = DCode.Selected.Description
            Formula = objForm.Items.Item("25").Specific.string + "{" + DValue + "}"
            ' objForm.Items.Item("25").Specific.value = ""
            objForm.Items.Item("25").Specific.value = Formula
        End If

        If OT.Selected Is Nothing Then
            BValue = ""
        Else
            OTValue = OT.Selected.Description
            Formula = objForm.Items.Item("25").Specific.string + "{" + OTValue + "}"
            'objForm.Items.Item("25").Specific.value = ""
            objForm.Items.Item("25").Specific.value = Formula
        End If

        If BCode.Selected Is Nothing Then
            BValue = ""
        Else
            BValue = BCode.Selected.Description
            Formula = objForm.Items.Item("25").Specific.string + "{" + BValue + "}"
            ' objForm.Items.Item("25").Specific.value = ""
            objForm.Items.Item("25").Specific.value = Formula
        End If

        If Number.Selected Is Nothing Then
            Num = ""
        Else
            Num = Number.Selected.Description
            Formula = objForm.Items.Item("25").Specific.string + Num
            'objForm.Items.Item("25").Specific.value = ""
            objForm.Items.Item("25").Specific.value = Formula
        End If

        If Operation.Selected Is Nothing Then
            OValue = ""
        Else
            OValue = Operation.Selected.Value
            Formula = objForm.Items.Item("25").Specific.string + OValue
            ' objForm.Items.Item("25").Specific.value = ""
            objForm.Items.Item("25").Specific.value = Formula
        End If

    End Sub
#End Region

#Region "Check Code"
    Public Sub CheckCode()
        If objForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
            ComboChange()
        End If
    End Sub
#End Region

#Region "Combo Change"
    Public Sub ComboChange()
        objCbo = objForm.Items.Item("8").Specific
        Try
            If objCbo.Selected.Value = "Pay Code" Then
                objStatc = objForm.Items.Item("17").Specific
                objStatc.Caption = "Pay Code"
                objForm.Items.Item("18").Visible = True
                objForm.Items.Item("26").Visible = False
                objForm.Update()
            ElseIf objCbo.Selected.Value = "Deduction Code" Then
                objStatc = objForm.Items.Item("17").Specific
                objStatc.Caption = "Deduction Code"
                objForm.Items.Item("18").Visible = False
                objForm.Items.Item("26").Visible = True
                objForm.Update()
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Paycode"
    Public Sub PayCode()
        objPayCmb = objForm.Items.Item("18").Specific
        Try
            If objPayCmb.Selected.Description = "PF" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "PF ="
            ElseIf objPayCmb.Selected.Description = "GA" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "GA = "
            ElseIf objPayCmb.Selected.Description = "ESI" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "ESI = "
            ElseIf objPayCmb.Selected.Description = "ESI Limit" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "ESI Limit = "
            ElseIf objPayCmb.Selected.Description = "Section10" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "Section10 = "
            ElseIf objPayCmb.Selected.Description = "PT" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "PT = "
            ElseIf objPayCmb.Selected.Description = "Active" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "Active = "
            ElseIf objPayCmb.Selected.Description = "Fixed" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "Fixed = "
            ElseIf objPayCmb.Selected.Description = "VPF" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "VPF = "
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Deduction Code"
    Public Sub DeductionCode()
        objDedCmb = objForm.Items.Item("26").Specific
        Try
            If objDedCmb.Selected.Description = "TDS" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "TDS = "
            ElseIf objDedCmb.Selected.Description = "PF" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "PF = "
            ElseIf objDedCmb.Selected.Description = "ESI" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "ESI = "
            ElseIf objDedCmb.Selected.Description = "ECess" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "ECess = "
            ElseIf objDedCmb.Selected.Description = "HEdCess" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "HEdCess = "
            ElseIf objDedCmb.Selected.Description = "Surcharge" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "Surcharge = "
            ElseIf objDedCmb.Selected.Description = "PT" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "PT = "
            ElseIf objDedCmb.Selected.Description = "Active" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "Active = "
            ElseIf objDedCmb.Selected.Description = "Fixed" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "Fixed = "
            ElseIf objDedCmb.Selected.Description = "VPF" Then
                objEdit = objForm.Items.Item("25").Specific
                objEdit.Value = "VPF = "
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Pay Validate"
    Public Function PayValidate(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        objPayCmb = objForm.Items.Item("18").Specific
        objbutton = objForm.Items.Item("24").Specific
        objEdit = objForm.Items.Item("25").Specific
        If objPayCmb.Selected.Value = "PF" Then
            If objEdit.Value <> "PF = 12% * Basic" Then
                objAddOn.objApplication.MessageBox("Please Check The Formula")
                Return False
            Else
                objAddOn.objApplication.MessageBox("Correct Formula")
            End If
        ElseIf objPayCmb.Selected.Value = "GA" Then
            If objEdit.Value <> "GA = 4.81% * Basic" Then
                objAddOn.objApplication.MessageBox("Please Check The Formula")
                Return False
            Else
                objAddOn.objApplication.MessageBox("Correct Formula")
            End If
        End If
        Return True
    End Function
#End Region

#Region "Deduction validate"
    Public Function DedValidate(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        objDedCmb = objForm.Items.Item("26").Specific
        objbutton = objForm.Items.Item("24").Specific
        objEdit = objForm.Items.Item("25").Specific
        If objDedCmb.Selected.Value = "PF" Then
            If objEdit.Value <> "PF = 12% * Basic" Then
                objAddOn.objApplication.MessageBox("Please Check The Formula", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_None)
                Return False
            Else
                objAddOn.objApplication.MessageBox("Correct Formula", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_None)
            End If
            'ElseIf objPayCmb.Selected.Value = "GA" Then
            '    If objedit.Value <> "GA = 4.81% * Basic" Then
            '        objAddOn.objApplication.MessageBox("Please Check The Formula", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_None)
            '        Return False
            '    Else
            '        objAddOn.objApplication.MessageBox("Correct Formula", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_None)
            '    End If
        End If
        Return True
    End Function
#End Region

#Region "Validation"
    Public Function validation(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        If objForm.Items.Item("4").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please enter the code", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("8").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please select the formula type", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        If objForm.Items.Item("18").Visible = True Then
            If objForm.Items.Item("18").Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please select the type of code", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
        ElseIf objForm.Items.Item("26").Visible = True Then
            If objForm.Items.Item("26").Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please select the type of code", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
        End If
        Return True
    End Function
#End Region

End Class
