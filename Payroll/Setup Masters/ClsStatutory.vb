
'Statutory Details

Public Class ClsStatutory
    Public Const FormType = "Statutory"
    Public objForm As SAPbouiCOM.Form
    Public objItem As SAPbouiCOM.Item
    Dim objRs As SAPbobsCOM.Recordset

    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("StatutoryDetails.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        objForm.Freeze(True)
        objRs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        'objForm.ActiveItem = "100"
        Try
            objForm.Items.Item("100").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            objForm.PaneLevel = 1
            objRs.DoQuery("select 1 from [@AIS_STAT]")
            If objRs.RecordCount > 0 Then
                objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                objForm.Items.Item("91").Visible = True
                objForm.Items.Item("91").Specific.string = 1
                objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            Else
                objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                objForm.Items.Item("91").Visible = True
                objForm.Items.Item("91").Specific.value = 1
            End If
            objForm.ActiveItem = "15"
            'objForm.Items.Item("91").Visible = False
            objForm.Freeze(False)
            objAddOn.objApplication.Menus.Item("1281").Enabled = False
        Catch ex As Exception

        End Try

    End Sub
    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        If pVal.ItemUID = "51" Then
                            If age(FormUID) = False Then
                                objAddOn.objApplication.SetStatusBarMessage("Please Enter Valid AGE ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                BubbleEvent = False
                            End If
                        End If
                        If pVal.ItemUID = "72" Then
                            If age1(FormUID) = False Then
                                objAddOn.objApplication.SetStatusBarMessage("Please Enter Valid AGE ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                BubbleEvent = False
                            End If
                        End If



                        If pVal.ItemUID = "1000001" Or pVal.ItemUID = "92" Or pVal.ItemUID = "95" Or pVal.ItemUID = "35" Or pVal.ItemUID = "37" Or pVal.ItemUID = "39" Or pVal.ItemUID = "41" Or pVal.ItemUID = "43" Or pVal.ItemUID = "53" Or pVal.ItemUID = "55" Or pVal.ItemUID = "59" Or pVal.ItemUID = "61" Or pVal.ItemUID = "68" Or pVal.ItemUID = "70" Or pVal.ItemUID = "74" Or pVal.ItemUID = "72" Or pVal.ItemUID = "51" Then
                            'Precentage(condition)
                            'emptyval(FormUID)
                            ' If 'emptyval(FormUID) = False Then
                            '    objAddOn.objApplication.SetStatusBarMessage("Precentage must enter below 100", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                            '    BubbleEvent = False
                            'End If
                            If Precent(FormUID) = False Then
                                objAddOn.objApplication.SetStatusBarMessage("Percentage must enter below 100", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                BubbleEvent = False
                            End If



                        End If
                        If pVal.ItemUID = "23" Or pVal.ItemUID = "25" Or pVal.ItemUID = "27" Or pVal.ItemUID = "29" Or pVal.ItemUID = "15" Or pVal.ItemUID = "17" Or pVal.ItemUID = "19" Or pVal.ItemUID = "21" Then
                            If ToValid(FormUID) = False Then
                                ' objAddOn.objApplication.SetStatusBarMessage("Check Date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "1" And (objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            'null valuable check 

                            If NullCheck(FormUID) = False Then
                                objAddOn.objApplication.SetStatusBarMessage("Please Enter The Mandatory Fields", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                BubbleEvent = False
                            End If
                            Func()
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        objAddOn.objApplication.Menus.Item("1282").Enabled = False

                End Select

            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "100" Then
                            objForm.PaneLevel = 1
                        End If
                        If pVal.ItemUID = "101" Then
                            objForm.PaneLevel = 2
                        End If
                        If pVal.ItemUID = "102" Then
                            objForm.PaneLevel = 3
                        End If
                        If pVal.ItemUID = "103" Then
                            objForm.PaneLevel = 4
                        End If
                        If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            AddMode()
                        End If
                End Select

            End If
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.Message)
        End Try
    End Sub

#Region "To Valid"
    Public Function ToValid(ByVal formuid)
        objForm = objAddOn.objApplication.Forms.Item(formuid)
        If objForm.Items.Item("15").Specific.value <> "" Or objForm.Items.Item("17").Specific.value <> "" Or objForm.Items.Item("19").Specific.value <> "" Or objForm.Items.Item("21").Specific.value <> "" Then

            If objForm.Items.Item("23").Specific.value <> "" And objForm.Items.Item("15").Specific.value <> "" Then
                If objForm.Items.Item("23").Specific.value() < objForm.Items.Item("15").Specific.value() Then
                    objAddOn.objApplication.SetStatusBarMessage("To Date Should be Greater than From Date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If

            If objForm.Items.Item("25").Specific.value <> "" And objForm.Items.Item("17").Specific.value <> "" Then
                If objForm.Items.Item("25").Specific.value() < objForm.Items.Item("17").Specific.value() Then
                    objAddOn.objApplication.SetStatusBarMessage("To Date Should be Greater than From Date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If
            If objForm.Items.Item("27").Specific.value <> "" And objForm.Items.Item("19").Specific.value <> "" Then
                If objForm.Items.Item("27").Specific.value() < objForm.Items.Item("19").Specific.value() Then
                    objAddOn.objApplication.SetStatusBarMessage("To Date Should be Greater than From Date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If

            If objForm.Items.Item("29").Specific.value <> "" And objForm.Items.Item("21").Specific.value <> "" Then
                If objForm.Items.Item("29").Specific.value() < objForm.Items.Item("21").Specific.value() Then
                    objAddOn.objApplication.SetStatusBarMessage("To Date Should be Greater than From Date", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                    Return False
                End If
            End If
        End If
        Return True
    End Function
#End Region

#Region "Null Check "
    Public Function NullCheck(ByVal FormUID) As Boolean
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        If (objForm.Items.Item("15").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("17").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("19").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("21").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("23").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("25").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("27").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("29").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("31").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("33").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("45").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("47").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("49").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("51").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("57").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("63").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("65").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("72").Specific.string = "") Then
            Return False
        ElseIf (objForm.Items.Item("76").Specific.string = "") Then
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Taking From Date and To date for TDS"

    Public Sub TDSDate()
        Dim oRs As SAPbobsCOM.Recordset
        Dim SQL As String
        oRs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        SQL = "select datepart(mm,(select U_tdsfrom from [@AIS_STAT] ))[SMONTH],DATEPART(MM,(select U_tdsto  from [@AIS_STAT] ))[EMONTH]" & _
            ",DATEPART(YYYY,(select U_tdsfrom from [@AIS_STAT]))[SYEAR],DATEPART(YYYY,(select U_tdsto  from [@AIS_STAT] ))[EYEAR]"
        oRs.DoQuery(SQL)
        TDSFromDate = oRs.Fields.Item("SMONTH").Value
        TDSToDate = oRs.Fields.Item("EMONTH").Value
        TDSFromYear = oRs.Fields.Item("SYEAR").Value
        TDSToYear = oRs.Fields.Item("EYEAR").Value
    End Sub

#End Region

#Region "AGE"
    Public Function age(ByVal FormUID) As Boolean
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        If Val(objForm.Items.Item("51").Specific.value) < 18 Then
            Return False
        End If
        Return True
    End Function
#End Region

#Region "AGE1"
    Public Function age1(ByVal FormUID) As Boolean
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        If Val(objForm.Items.Item("72").Specific.value) < 58 Then
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Precentage"
    Public Function Precent(ByVal FormUID) As Boolean
        objForm = objAddOn.objApplication.Forms.Item(FormUID)
        If Val(objForm.Items.Item("35").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("37").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("39").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("41").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("43").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("53").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("55").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("59").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("61").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("68").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("70").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("74").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("92").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("95").Specific.value) > 100 Then
            Return False
        ElseIf Val(objForm.Items.Item("1000001").Specific.value) > 100 Then
            Return False
        End If
        Return True
    End Function
#End Region

    Public Sub AddMode()
        objForm.Freeze(True)
        objRs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objForm.AutoManaged = True
        objRs.DoQuery("select 1 from [@AIS_STAT]")
        If objRs.RecordCount > 0 Then
            objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            objForm.Items.Item("91").Visible = True
            objForm.Items.Item("91").Specific.string = 1
            objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
        Else
            objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
            objForm.Items.Item("91").Visible = True
            objForm.Items.Item("91").Specific.string = 1
        End If
        If objForm.PaneLevel = 1 Then
            objForm.ActiveItem = "15"
        End If
        If objForm.PaneLevel = 2 Then
            objForm.ActiveItem = "31"
        End If
        If objForm.PaneLevel = 3 Then
            objForm.ActiveItem = "57"
        End If
        If objForm.PaneLevel = 4 Then
            objForm.ActiveItem = "68"
        End If
        objForm.Items.Item("91").Visible = False
        objForm.Freeze(False)
    End Sub
    Public Sub FindMode()
        objForm.Freeze(True)
        objForm.Items.Item("91").Visible = True
        objForm.Items.Item("91").Specific.string = 1
        objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
        If objForm.PaneLevel = 1 Then
            objForm.ActiveItem = "15"
        End If
        If objForm.PaneLevel = 2 Then
            objForm.ActiveItem = "31"
        End If
        If objForm.PaneLevel = 3 Then
            objForm.ActiveItem = "57"
        End If
        If objForm.PaneLevel = 4 Then
            objForm.ActiveItem = "68"
        End If

        objForm.Items.Item("91").Visible = False
        objForm.Freeze(False)
    End Sub
    Public Sub func()
        'Strsql = "Insert inte"
    End Sub

    'Public Sub emptyval(ByVal formuid)
    '    Dim A As Integer
    '    objForm = objAddOn.objApplication.Forms.Item(formuid)
    '    A = (objForm.Items.Item("35").Specific.value)
    '    If Val(objForm.Items.Item("35").Specific.string) = 0 Then
    '        objForm.Items.Item("35").Specific.string = 1
    '        Exit Sub
    '    ElseIf Val(objForm.Items.Item("37").Specific.string) = 0 Then
    '        objForm.Items.Item("37").Specific.string = 0
    '    ElseIf Val(objForm.Items.Item("39").Specific.string) = 0 Then
    '        objForm.Items.Item("39").Specific.string = 0
    '    ElseIf Val(objForm.Items.Item("41").Specific.string) = 0 Then
    '        objForm.Items.Item("41").Specific.string = 0
    '    ElseIf Val(objForm.Items.Item("43").Specific.string) = 0 Then
    '        objForm.Items.Item("43").Specific.string = 0
    '    ElseIf Val(objForm.Items.Item("53").Specific.string) = 0 Then
    '        objForm.Items.Item("53").Specific.string = 0
    '    ElseIf Val(objForm.Items.Item("55").Specific.string) = 0 Then
    '        objForm.Items.Item("55").Specific.string = 0
    '    ElseIf Val(objForm.Items.Item("59").Specific.string) = 0 Then
    '        objForm.Items.Item("59").Specific.string = 0
    '    ElseIf Val(objForm.Items.Item("61").Specific.string) = 0 Then
    '        objForm.Items.Item("61").Specific.string = 0
    '    ElseIf Val(objForm.Items.Item("68").Specific.string) = 0 Then
    '        objForm.Items.Item("68").Specific.string = 0
    '    ElseIf Val(objForm.Items.Item("70").Specific.string) = 0 Then
    '        objForm.Items.Item("70").Specific.string = 0
    '    ElseIf Val(objForm.Items.Item("74").Specific.string) = 0 Then
    '        objForm.Items.Item("74").Specific.string = 0
    '    End If

    'End Sub

End Class
