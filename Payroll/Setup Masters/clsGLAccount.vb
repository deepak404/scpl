﻿Public Class clsGLAccount
    Public Const type = "Account"
    Dim objForm As SAPbouiCOM.Form
    Dim objItem As SAPbouiCOM.Item
    Dim objFolder As SAPbouiCOM.Folder
    Dim objStatic As SAPbouiCOM.StaticText
    Dim objEdit As SAPbouiCOM.EditText
    Dim objCombo As SAPbouiCOM.ComboBox
    Dim objLink As SAPbouiCOM.LinkedButton
    Dim Strsql As String
    Dim objrs As SAPbobsCOM.Recordset
    Dim i As Integer

    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("GLdetermination.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, type)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.Items.Item("62").Visible = True
        objForm.Items.Item("62").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_GLAC")
        '  objForm.ActiveItem = "4"
        '    objAddOn.objApplication.Menus.Item("1281").Enabled = True
        ' objAddOn.objApplication.Menus.Item("1282").Enabled = True
        '  objForm.Items.Item("28").Visible = False
        objForm.DataBrowser.BrowseBy = "62"
        combo()
        For IntI As Integer = 2 To 15
            AccountCodeSelection("CFL_" & IntI & "")
        Next
    End Sub

    Public Sub itemevent(ByVal formUID As String, ByRef pval As SAPbouiCOM.ItemEvent, ByRef bubbleevent As Boolean)

        Try
            If pval.BeforeAction = False Then
                Select Case pval.EventType
                    Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
                        Try
                            objForm = objAddOn.objApplication.Forms.Item(formUID)
                        Catch ex As Exception
                            MsgBox(ex.ToString)
                        End Try
                    Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                        If pval.ItemUID = "4" Then
                            objForm = objAddOn.objApplication.Forms.Item(formUID)
                            objCombo = objForm.Items.Item("4").Specific
                            ' coboselect()
                            CheckFromDB(objCombo.Selected.Value)
                            objAddOn.objApplication.Menus.Item("1281").Enabled = True
                            objAddOn.objApplication.Menus.Item("1282").Enabled = True
                        End If

                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pval.ItemUID = "6" Then
                            Sal_Payable_Credit(pval)
                        End If

                        If pval.ItemUID = "14" Then
                            VPFAccount_Credit(pval)
                        End If

                        If pval.ItemUID = "59" Then
                            TDS_Account_Credit(pval)
                        End If

                        If pval.ItemUID = "60" Then
                            Professional_Credit(pval)
                        End If

                        If pval.ItemUID = "20" Then
                            Other_ded_account(pval)
                        End If

                        If pval.ItemUID = "12" Then
                            Gratuity_Account_Credit(pval)
                        End If

                        If pval.ItemUID = "31" Then
                            Sal_Payable_Debit(pval)
                        End If

                        If pval.ItemUID = "22" Then
                            VPFAccount_Debit(pval)
                        End If

                        If pval.ItemUID = "8" Then
                            Other_Add_Debit(pval)
                        End If

                        If pval.ItemUID = "10" Then
                            Bonus_Debit(pval)
                        End If

                        If pval.ItemUID = "18" Then
                            Over_Time_Account_Debit(pval)
                        End If

                        If pval.ItemUID = "24" Then
                            GratuityAccount_Debit(pval)
                        End If

                        If pval.ItemUID = "44" Then
                            Leave_Enhancement_Debit(pval)
                        End If

                        If pval.ItemUID = "64" Then
                            Full_Final_Settlement(pval)
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                        objForm = objAddOn.objApplication.Forms.Item(formUID)
                        If pval.ItemUID = "1" And pval.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                            objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
                            objForm.Items.Item("62").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_PAYCODE")
                        End If

                End Select
            Else
                Select Case pval.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pval.ItemUID = "1" Then
                            objForm.Items.Item("62").Visible = True
                            If objForm.Items.Item("62").Specific.String = "" Then
                                objForm.Items.Item("62").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_GLAC")
                                objForm.ActiveItem = "6"
                            End If
                        End If
                        ' objForm.Items.Item("28").Visible = False
                        If pval.ItemUID = "1" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                            If validate(pval) = False Then
                                objAddOn.objApplication.SetStatusBarMessage("Please Select Branch", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                bubbleevent = False
                            End If
                        End If

                End Select
            End If

        Catch ex As Exception

        End Try

    End Sub
    Public Sub CheckFromDB(ByVal Branch As String)
        Dim objRS As SAPbobsCOM.Recordset
        Dim strSQL As String
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        strSQL = "select * from [@AIS_GLAC] where U_brnch='" & Branch & "'"
        objRS.DoQuery(strSQL)
        If objRS.RecordCount > 0 Then
            objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
            objForm.Items.Item("62").Specific.value = objRS.Fields.Item("Code").Value
            objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
            objForm.Items.Item("4").Enabled = False
        End If
    End Sub


#Region "Choose From List Conditions"
    Public Sub AccountCodeSelection(ByVal CFL As String)
        Dim Objcfl As SAPbouiCOM.ChooseFromList
        Dim objChooseCollection As SAPbouiCOM.ChooseFromListCollection
        Dim objConditions As SAPbouiCOM.Conditions
        Dim objcondition As SAPbouiCOM.Condition

        objChooseCollection = objForm.ChooseFromLists
        Objcfl = objChooseCollection.Item("" & CFL & "")
        objConditions = Objcfl.GetConditions()
        objcondition = objConditions.Add()
        objcondition.Alias = "Postable"
        objcondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
        objcondition.CondVal = "Y"
        Objcfl.SetConditions(objConditions)
    End Sub

#End Region

    Public Sub combo()
        Strsql = "select Code,Location from Olct order by Code"
        objrs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objrs.DoQuery(Strsql)
        objCombo = objForm.Items.Item("4").Specific
        If objrs.RecordCount > 0 Then
            For i = 1 To objrs.RecordCount
                objCombo.ValidValues.Add(objrs.Fields.Item("Code").Value, objrs.Fields.Item("Location").Value)
                objrs.MoveNext()
            Next
        End If
    End Sub

    Public Sub coboselect()
        Dim c As Integer
        Try
            objCombo = objForm.Items.Item("4").Specific
            c = objCombo.Selected.Value
            Strsql = "Select code from [@AIS_GLAC] where U_brnch= '" & c & "' "
            objrs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objrs.DoQuery(Strsql)
            If objrs.RecordCount > 0 Then
                For i = 1 To objrs.RecordCount
                    objForm.Freeze(True)

                    objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                    objForm.Items.Item("62").Visible = True
                    objForm.Items.Item("62").Specific.value = objrs.Fields.Item("Code").Value
                    objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                    'objForm.ActiveItem = "6"
                    'objForm.Items.Item("62").Visible = False
                    '   objForm.Items.Item("4").Enabled = False
                    objForm.Freeze(False)
                    'objForm.Items.Item("6").Specific.string = objrs.Fields.Item("U_spa").String
                    'objForm.Items.Item("8").Specific.string = objrs.Fields.Item("U_ota").String
                    'objForm.Items.Item("10").Specific.string = objrs.Fields.Item("U_oaa").String
                    'objForm.Items.Item("12").Specific.string = objrs.Fields.Item("U_oda").String
                    'objForm.Items.Item("14").Specific.string = objrs.Fields.Item("U_fbtc").String
                    'objForm.Items.Item("18").Specific.string = objrs.Fields.Item("U_fbtr").String
                    'objForm.Items.Item("20").Specific.string = objrs.Fields.Item("U_gac").String
                    'objForm.Items.Item("22").Specific.string = objrs.Fields.Item("U_gad").String
                    'objForm.Items.Item("24").Specific.string = objrs.Fields.Item("U_wpfa").String



                Next
            Else
                'objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE

                'objForm.Freeze(True)
                'objForm.Items.Item("28").Visible = True
                'objForm.Items.Item("28").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_GLAC")
                'objForm.ActiveItem = "4"
                'objForm.Items.Item("28").Visible = False
                'objForm.Freeze(False)
            End If
        Catch ex As Exception

        End Try

    End Sub

    Public Sub find()
        objForm.Items.Item("62").Enabled = True
    End Sub

    Public Sub Add()
        objForm.Items.Item("4").Enabled = True
        objForm.Items.Item("62").Visible = True
        objForm.Items.Item("62").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_GLAC")
        ' objForm.ActiveItem = "4"
        '  objForm.Items.Item("62").Visible = False
    End Sub

#Region "Choose From List for Accounts"
    Public Sub Sal_Payable_Credit(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("61").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("6").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub VPFAccount_Credit(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("32").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("14").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub TDS_Account_Credit(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("33").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("59").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Professional_Credit(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("34").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("60").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Other_ded_account(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("35").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("20").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Gratuity_Account_Credit(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("41").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("12").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Sal_Payable_Debit(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("36").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("31").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub VPFAccount_Debit(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("37").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("22").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Other_Add_Debit(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("38").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("8").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Bonus_Debit(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("39").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("10").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Over_Time_Account_Debit(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("40").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("18").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub GratuityAccount_Debit(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("42").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("24").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Leave_Enhancement_Debit(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("45").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("44").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Full_Final_Settlement(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("1000006").Specific.string = objdt.GetValue("AcctName", 0)
            objForm.Items.Item("64").Specific.string = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub

#End Region
#Region "Exp G/l Account"
    Public Sub OtherAddACC(ByVal current)
        Dim objdt As SAPbouiCOM.DataTable
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        objcfl = current
        objdt = objcfl.SelectedObjects
        ' objForm = objAddOn.objApplication.Forms.Item(formuid)
        'objmatrix = objForm.Items.Item("3").Specific
        Try
            objForm.Items.Item("8").Specific.Value = objdt.GetValue("FormatCode", 0)
        Catch ex As Exception

        End Try
    End Sub
#End Region

    Public Function validate(ByVal pval)
        objCombo = objForm.Items.Item("4").Specific
        If pval.itemuid = "1" And objCombo.Selected Is Nothing Then
            Return False
        ElseIf objForm.Items.Item("6").Specific.String = "" Then
            Return False
        ElseIf objForm.Items.Item("8").Specific.String = "" Then
            Return False
        ElseIf objForm.Items.Item("10").Specific.String = "" Then
            Return False
        ElseIf objForm.Items.Item("12").Specific.String = "" Then
            Return False
        ElseIf objForm.Items.Item("14").Specific.String = "" Then
            Return False
        ElseIf objForm.Items.Item("18").Specific.String = "" Then
            Return False
        ElseIf objForm.Items.Item("20").Specific.String = "" Then
            Return False
        ElseIf objForm.Items.Item("22").Specific.String = "" Then
            Return False
        ElseIf objForm.Items.Item("24").Specific.String = "" Then
            Return False
        End If
        Return True
    End Function

End Class
