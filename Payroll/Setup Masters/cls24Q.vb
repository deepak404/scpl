﻿Public Class cls24Q
    Public Const FormType = "MNU_24Q"
    Dim objForm As SAPbouiCOM.Form
    Dim RS As SAPbobsCOM.Recordset
    Dim intqid As Integer
    Dim objrs As SAPbobsCOM.Recordset

    Public Sub Define()
        Dim intLoop As Integer
        Dim tablename As String
        For intloop = 0 To objAddOn.objApplication.Menus.Item("51200").SubMenus.Count - 1
            tablename = objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).String
            tablename = tablename.Remove(tablename.IndexOf("-"))
            If Trim(tablename) = "AIS_24QSTCD" Then
                objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).Activate()
                FMSA = objAddOn.objApplication.Forms.ActiveForm.TypeEx
                Exit For

            End If
        Next intLoop
        fms()
        fms1()
    End Sub

    Public Sub fms()
        Dim lretcode1, lret As Long
        Dim objfms1 As SAPbobsCOM.FormattedSearches
        Dim objquery As SAPbobsCOM.UserQueries
        Dim objrs As SAPbobsCOM.Recordset
        Dim str As String
        Dim intI As Integer
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        Try
            RS.DoQuery("select * from OUQR where Qname ='Qfmcod'")
            If RS.RecordCount = 0 Then
                objquery = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserQueries)
                objquery.Query = "Select distinct a.Code from OCST a join olct b on a.Country=b.Country where 1=1"
                objquery.QueryDescription = "Qfmcod"
                objquery.QueryCategory = "-1"
                lretcode1 = objquery.Add()
                If lretcode1 <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                End If
            End If
            str = "select ISNULL(QueryID,0) q from CSHS where FormID='" & FMSA & "'and ItemID='3'and ColID='Code' "
            objrs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            objrs.DoQuery(str)
            intI = objrs.Fields.Item("q").Value
            If intI <> 0 Then
                Exit Sub
            Else
                RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                RS.DoQuery("select IntrnalKey from OUQR where Qname ='Qfmcod'")
                intqid = RS.Fields.Item("IntrnalKey").Value
                objfms1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oFormattedSearches)
                objfms1.FormID = FMSA
                objfms1.ColumnID = "Code"
                objfms1.ItemID = 3
                objfms1.Action = SAPbobsCOM.BoFormattedSearchActionEnum.bofsaQuery
                objfms1.QueryID = intqid
                lret = objfms1.Add
                If lret <> 0 Then
                    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub fms1()
        Dim str As String
        Dim lretcode1, lret As Long
        Dim objfms1 As SAPbobsCOM.FormattedSearches
        Dim objquery As SAPbobsCOM.UserQueries
        Dim intI As Integer
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery("select * from OUQR where Qname ='Qfmnam'")
        If RS.RecordCount = 0 Then
            objquery = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserQueries)
            objquery.Query = "Select distinct a.name from OCST a join olct b on a.Country=b.Country where a.Code=$[$3.Code.0]"
            objquery.QueryDescription = "Qfmnam"
            objquery.QueryCategory = "-1"
            lretcode1 = objquery.Add()
        End If

        str = "select ISNULL(QueryID,0) q from CSHS where FormID='" & FMSA & "'and ItemID='3'and ColID= 'Name'"
        objrs = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objrs.DoQuery(str)
        intI = objrs.Fields.Item("q").Value
        If intI <> 0 Then
            Exit Sub
        Else
            'objForm.ActiveItem = "100"

            RS.DoQuery("select IntrnalKey from OUQR where Qname ='Qfmnam'")
            intqid = RS.Fields.Item("IntrnalKey").Value
            objfms1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oFormattedSearches)
            objfms1.FormID = FMSA
            objfms1.ColumnID = "Name"
            objfms1.ItemID = 3
            objfms1.Action = SAPbobsCOM.BoFormattedSearchActionEnum.bofsaQuery
            objfms1.QueryID = intqid
            objfms1.FieldID = "Code"
            objfms1.Refresh = SAPbobsCOM.BoYesNoEnum.tYES
            lret = objfms1.Add
            If lret <> 0 Then
                MsgBox(objAddOn.objCompany.GetLastErrorDescription)
            End If

            'If lretcode1 <> 0 Then
            '    MsgBox(objAddOn.objCompany.GetLastErrorDescription)
        End If
    End Sub
End Class
