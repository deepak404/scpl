﻿Public Class clsloanadvance
    Public objForm As SAPbouiCOM.Form
    Public Const formtype = "ladvance"
    Dim objMatrix As SAPbouiCOM.Matrix
    Dim objCombo As SAPbouiCOM.ComboBox
    Dim objCFLEvent As SAPbouiCOM.ChooseFromListEvent
    Dim objDataTable As SAPbouiCOM.DataTable
    Dim objRS As SAPbobsCOM.Recordset
    Dim strSQL As String
    Dim intLoop As Integer
    Dim objItem As SAPbouiCOM.Item

#Region "Loadscreen"
    Public Sub Loadscreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("LoanAdvance.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objCombo = objForm.Items.Item("8").Specific
        objForm.AutoManaged = True
        objItem = objForm.Items.Item("4")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        objItem = objForm.Items.Item("6")
        objItem.SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, SAPbouiCOM.BoFormMode.fm_OK_MODE, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
        TypeFun(objForm.UniqueID)

        For INTI As Integer = 2 To 3
            AccountCodeSelection("CFL_" & INTI)
        Next
    End Sub
#End Region

#Region "Itemevent"
    Public Sub itemevent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "1" And objform.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        objform = objAddOn.objApplication.Forms.Item(FormUID)
                        If ValidateFun() Then
                        Else
                            BubbleEvent = False
                        End If
                    End If
            End Select
        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    objcflevent = pVal
                    objdatatable = objcflevent.SelectedObjects
                    If pVal.ItemUID = "10" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        PrincipalCFL()
                        If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                        End If
                    End If
                    If pVal.ItemUID = "14" And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        InterestCFL()
                        If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                        End If
                    End If
                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    objcombo = objform.Items.Item("8").Specific
                    If objCombo.Selected.Description = "Define New" And (objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                        Define()
                    End If
            End Select
        End If
    End Sub
#End Region

#Region "Choose From List Conditions"
    Public Sub AccountCodeSelection(ByVal CFL As String)
        Dim Objcfl As SAPbouiCOM.ChooseFromList
        Dim objChooseCollection As SAPbouiCOM.ChooseFromListCollection
        Dim objConditions As SAPbouiCOM.Conditions
        Dim objcondition As SAPbouiCOM.Condition

        objChooseCollection = objForm.ChooseFromLists
        Objcfl = objChooseCollection.Item("" & CFL & "")
        objConditions = Objcfl.GetConditions()
        objcondition = objConditions.Add()
        objcondition.Alias = "Postable"
        objcondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
        objcondition.CondVal = "Y"
        Objcfl.SetConditions(objConditions)
    End Sub

#End Region

#Region "Principal and Interest AC"
    Public Sub PrincipalCFL()
        Try
            objForm.Items.Item("10").Specific.value = objDataTable.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub
    Public Sub InterestCFL()
        Try
            objForm.Items.Item("14").Specific.value = objDataTable.GetValue("FormatCode", 0)
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "Define Loan Advance type"
    Public Sub Define()
        Dim intLoop As Integer
        Dim Tablename As String
        objAddOn.DFFlag = True
        For intLoop = 0 To objAddOn.objApplication.Menus.Item("51200").SubMenus.Count - 1
            Tablename = objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).String
            Tablename = Tablename.Remove(Tablename.IndexOf("-"))
            If Trim(Tablename) = "AIS_LONADVT" Then
                objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).Activate()
                Flag = objAddOn.objApplication.Forms.ActiveForm.TypeEx
                Exit For
            End If
        Next intLoop
    End Sub
#End Region

#Region "Define Loan Advance type menu"
    Public Sub Define1()
        Dim intLoop As Integer
        Dim Tablename As String
        objAddOn.DFFlag = False
        For intLoop = 0 To objAddOn.objApplication.Menus.Item("51200").SubMenus.Count - 1
            Tablename = objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).String
            Tablename = Tablename.Remove(Tablename.IndexOf("-"))
            If Trim(Tablename) = "AIS_LONADVT" Then
                objAddOn.objApplication.Menus.Item("51200").SubMenus.Item(intLoop).Activate()
                Exit For
            End If
        Next intLoop
    End Sub
#End Region

#Region "Type function"
    Public Sub TypeFun(ByVal FormUID As String)

        objCombo = objForm.Items.Item("8").Specific

        ' for removing the combo box values
        If objCombo.ValidValues.Count > 0 Then
            Dim J As Integer
            For J = objCombo.ValidValues.Count - 1 To 0 Step -1
                objCombo.ValidValues.Remove(J, SAPbouiCOM.BoSearchKey.psk_Index)
            Next
        End If

        'For Adding valid values to the combo box
        strSQL = " select Code,Name from [@AIS_LONADVT] order by Name"
        objRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        objRS.DoQuery(strSQL)
        If objRS.RecordCount > 0 Then
            For intLoop = 1 To objRS.RecordCount
                objCombo.ValidValues.Add(intLoop, objRS.Fields.Item("Name").Value)
                objRS.MoveNext()
            Next
        End If
        objCombo.ValidValues.Add(intLoop, "Define New")
    End Sub
#End Region

#Region "Validate"
    Public Function ValidateFun()
        If objForm.Items.Item("4").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Code is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("6").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Name is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("8").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Type is missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("10").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Principal Account missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("14").Specific.Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Interest G/L Account missing", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

End Class
