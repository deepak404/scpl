﻿Public Class clsDeducDet
    Public Const FormType = "MNU_DEDUCTOR"
    Dim objForm As SAPbouiCOM.Form
    Dim objoption As SAPbouiCOM.OptionBtn
    Dim objoption1 As SAPbouiCOM.OptionBtn
    'Dim objoption2 As SAPbouiCOM.OptionBtn
    'Dim objoption3 As SAPbouiCOM.OptionBtn
    Dim objCombo As SAPbouiCOM.ComboBox
    Dim objCombo1 As SAPbouiCOM.ComboBox
    Dim objCombox As SAPbouiCOM.ComboBox

    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("Deducdet.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE
        objForm.DataBrowser.BrowseBy = "55"
        objForm.Items.Item("55").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_DEDDET")
        '  objAddOn.objApplication.Menus.Item("1281").Enabled = True
        objForm.ActiveItem = "4"
        objoption = objForm.Items.Item("7").Specific
        objoption.GroupWith("8")
        objoption = objForm.Items.Item("27").Specific
        objoption.GroupWith("28")
        objoption = objForm.Items.Item("53").Specific
        objoption.GroupWith("54")
        Combo()
        combobox()
    End Sub

#Region "addmode"
    Public Sub addmode()
        objForm.ActiveItem = "55"
        objForm.Items.Item("55").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_DEDDET")
        objForm.ActiveItem = "4"
    End Sub
#End Region

#Region "combo for location"
    Public Sub Combo()
        Dim strName As String
        Dim RS As SAPbobsCOM.Recordset
        Dim intLoop As Integer
        objCombo = objForm.Items.Item("4").Specific
        'objCombo.ExpandType = SAPbouiCOM.BoExpandType.et_ValueDescription
        ''adding values to combo box
        strName = "select distinct Location from OLCT"
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery(strName)
        RS.MoveFirst()
        For intLoop = 1 To RS.RecordCount
            Try
                objCombo.ValidValues.Add(intLoop, RS.Fields.Item("Location").Value)
                RS.MoveNext()
            Catch ex As Exception
            End Try
        Next intLoop
    End Sub
    Public Sub ComboBox()
        Dim strNam As String
        Dim RS1 As SAPbobsCOM.Recordset
        Dim intLoop As Integer
        objCombox = objForm.Items.Item("18").Specific
        'objCombox.ExpandType = SAPbouiCOM.BoExpandType.et_ValueDescription
        objCombo1 = objForm.Items.Item("44").Specific
        'objCombo1.ExpandType = SAPbouiCOM.BoExpandType.et_ValueDescription
        strNam = "select distinct Name from OCST"
        RS1 = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS1.DoQuery(strNam)
        RS1.MoveFirst()
        For intLoop = 1 To RS1.RecordCount
            Try
                objCombox.ValidValues.Add(intLoop, RS1.Fields.Item("Name").Value)
                objCombo1.ValidValues.Add(intLoop, RS1.Fields.Item("Name").Value)
                RS1.MoveNext()
            Catch ex As Exception
            End Try
        Next intLoop
    End Sub
#End Region

    Public Sub itemevent(ByVal formuid As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef Bubbleevent As Boolean)
        If pVal.BeforeAction = False Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    If pVal.ItemUID = "1" And pVal.ActionSuccess = True And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        objForm.Items.Item("55").Specific.value = objForm.BusinessObject.GetNextSerialNumber("-1", "AIS_DEDDET")
                        objForm.ActiveItem = "4"
                    End If
                    ''If pVal.ItemUID = "7" Then
                    ''    objoption = objForm.Items.Item("8").Specific
                    ''    objoption.ValOn = 0
                    ''    objoption.ValOff = 1
                    ''End If
                    ''If pVal.ItemUID = "8" Then
                    ''    objoption = objForm.Items.Item("7").Specific
                    ''    objoption.ValOn = 0
                    ''    objoption.ValOff = 1
                    ''End If
            End Select

        Else
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "1" And objForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        If Validate(formuid, pVal) = False Then
                            Bubbleevent = False
                            Exit Sub

                        End If
                    End If
            End Select
        End If
    End Sub

#Region "validate"

    Public Function Validate(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        Dim rd As SAPbouiCOM.OptionBtn
        Dim rd1 As SAPbouiCOM.OptionBtn
        Dim rd2 As SAPbouiCOM.OptionBtn
        Dim rd3 As SAPbouiCOM.OptionBtn
        Dim rd4 As SAPbouiCOM.OptionBtn
        Dim rd5 As SAPbouiCOM.OptionBtn
        rd = objForm.Items.Item("7").Specific
        rd1 = objForm.Items.Item("8").Specific
        rd2 = objForm.Items.Item("27").Specific
        rd3 = objForm.Items.Item("28").Specific
        rd4 = objForm.Items.Item("53").Specific
        rd5 = objForm.Items.Item("54").Specific
        objForm = objAddOn.objApplication.Forms.Item(FormUID)

        If objForm.Items.Item("4").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select The Branch/Division", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False

        ElseIf rd.Selected = False And rd1.Selected = False Then
            objAddOn.objApplication.SetStatusBarMessage("Please select the deductor type", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False


        ElseIf objForm.Items.Item("18").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please select the state", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("20").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please give the pincode", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False

        ElseIf rd2.Selected = False And rd3.Selected = False Then
            objAddOn.objApplication.SetStatusBarMessage("Please select the change in address", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False

        ElseIf objForm.Items.Item("30").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please give the Deductor's Name", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("32").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please give the Father's Name", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("44").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please select the state", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("46").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please give the pincode", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False

        ElseIf rd4.Selected = False And rd5.Selected = False Then
            objAddOn.objApplication.SetStatusBarMessage("Please select the change in address", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False

        End If
        Return True

    End Function
#End Region


End Class

