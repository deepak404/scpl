﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Sql
Public Class clsSalaryProcessed
    Public Const FormType = "MNU_SALPCD"
    Dim objForm As SAPbouiCOM.Form
    Dim objComboMonth, objComboYear As SAPbouiCOM.ComboBox
    Dim IntMon As Integer
    Dim IntYear As Integer

    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("SalaryProcessed.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        objComboMonth = objForm.Items.Item("4").Specific
        objComboYear = objForm.Items.Item("6").Specific
        Combo_Month()
        Combo_Year()
        objAddOn.objPaySlip.SAPassWord()
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_VALIDATE
                        If Validate(FormUID) = False Then
                            BubbleEvent = False
                            Exit Sub
                        End If
                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "1" Then
                            IntMon = objComboMonth.Selected.Description
                            IntYear = objComboYear.Selected.Value
                            'objAddOn.objSalaryProcessReport.LoadScreen(IntMon, IntYear)
                            GenerateReport(FormUID, IntMon, IntYear)
                        End If
                End Select
            End If
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.Message)
        End Try
    End Sub

#Region "Generate Report"
    Public Sub GenerateReport(ByVal FormUID As String, ByVal IntMonth As Integer, ByVal IntYear As Integer)
        Dim sender As System.Object
        Dim e As System.EventArgs
        sender = ""
        e = Nothing
        Dim RptFrm As FrmRpt
        RptFrm = New FrmRpt
        RptFrm.Refresh()
        Dim cryRpt As New ReportDocument

        Dim objConInfo As New CrystalDecisions.Shared.ConnectionInfo
        Dim oLogonInfo As New CrystalDecisions.Shared.TableLogOnInfo
        'Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
        Dim intCounter As Integer

        cryRpt.Load(System.Windows.Forms.Application.StartupPath & "\Reports\SalaryProcessedReport.rpt")

        Dim crParameterValues As New ParameterValues
        Dim crParameterDiscreteValue As New ParameterDiscreteValue


        Dim SerNam, DbName, UID As String
        SerNam = objAddOn.objCompany.Server
        DbName = objAddOn.objCompany.CompanyDB
        UID = objAddOn.objCompany.DbUserName


        oLogonInfo.ConnectionInfo.ServerName = SerNam
        oLogonInfo.ConnectionInfo.DatabaseName = DbName
        oLogonInfo.ConnectionInfo.UserID = UID
        oLogonInfo.ConnectionInfo.Password = SAPwd '"AIS@1234"
        ' oLogonInfo.ConnectionInfo.Password = "Pass@123"

        For intCounter = 0 To cryRpt.Database.Tables.Count - 1
            cryRpt.Database.Tables(intCounter).ApplyLogOnInfo(oLogonInfo)
        Next

        cryRpt.SetParameterValue("Month@", IntMonth)
        cryRpt.SetParameterValue("Year@", IntYear)

        RptFrm.CRViewer.ReportSource = Nothing
        RptFrm.CRViewer.ReportSource = cryRpt
        RptFrm.CRViewer.Refresh()
        System.Windows.Forms.Application.Run(RptFrm)
    End Sub
#End Region


#Region "Validation for Month and Year"
    Public Function Validate(ByVal FormUID As String)
        Try
            If objForm.Items.Item("4").Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select Month", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            ElseIf objForm.Items.Item("6").Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select Year", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.Message)
        End Try
        Return True
    End Function
#End Region

#Region "Month"
    Public Sub Combo_Month()
        Try
            If objComboMonth.ValidValues.Count > 0 Then
                Dim J As Integer
                For J = objComboMonth.ValidValues.Count - 1 To 0 Step -1
                    objComboMonth.ValidValues.Remove(J, SAPbouiCOM.BoSearchKey.psk_Index)
                Next
            End If
            objComboMonth.ValidValues.Add("January", "01")
            objComboMonth.ValidValues.Add("February", "02")
            objComboMonth.ValidValues.Add("March", "03")
            objComboMonth.ValidValues.Add("April", "04")
            objComboMonth.ValidValues.Add("May", "05")
            objComboMonth.ValidValues.Add("June", "06")
            objComboMonth.ValidValues.Add("July", "07")
            objComboMonth.ValidValues.Add("August", "08")
            objComboMonth.ValidValues.Add("September", "09")
            objComboMonth.ValidValues.Add("October", "10")
            objComboMonth.ValidValues.Add("November", "11")
            objComboMonth.ValidValues.Add("December", "12")
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub
#End Region

#Region "Year"
    Public Sub Combo_Year()
        Try
            If objComboYear.ValidValues.Count > 0 Then
                Dim J As Integer
                For J = objComboYear.ValidValues.Count - 1 To 0 Step -1
                    objComboYear.ValidValues.Remove(J, SAPbouiCOM.BoSearchKey.psk_Index)
                Next
            End If
            objComboYear.ValidValues.Add("2013", "2013")
            objComboYear.ValidValues.Add("2014", "2014")
            objComboYear.ValidValues.Add("2015", "2015")
            objComboYear.ValidValues.Add("2016", "2016")
            objComboYear.ValidValues.Add("2017", "2017")
            objComboYear.ValidValues.Add("2018", "2018")
            objComboYear.ValidValues.Add("2019", "2019")
            objComboYear.ValidValues.Add("2020", "2020")
            objComboYear.ValidValues.Add("2021", "2021")
            objComboYear.ValidValues.Add("2022", "2022")
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub
#End Region

End Class
