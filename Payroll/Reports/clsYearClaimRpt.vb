﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Sql
Public Class clsYearClaimRpt
    Public Const FormType = "MNU_YRCLMRPT"
    Dim oForm As SAPbouiCOM.Form
    Dim oComboMonth1, oComboMonth2, oComboYear As SAPbouiCOM.ComboBox
    Dim FIntMonth, IntYear, TIntMonth As Integer
    Dim FEID, ToEID As String
    Dim sender As System.Object
    Dim e As System.EventArgs
    Dim objCFL As SAPbouiCOM.ChooseFromListEvent
    Dim objDT As SAPbouiCOM.DataTable
    Public Sub LoadScreen()
        Try
            oForm = objAddOn.objUIXml.LoadScreenXML("YearClaimReimbursement.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
            oComboMonth1 = oForm.Items.Item("8").Specific
            oComboMonth2 = oForm.Items.Item("10").Specific
            oComboYear = oForm.Items.Item("12").Specific
            Month()
            Month1()
            Year()
            objAddOn.objPaySlip.SAPassWord()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Loadscreen - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        oForm = objAddOn.objApplication.Forms.Item(FormUID)
                        Try
                            If pVal.ItemUID = "1" Then
                                If (oComboMonth1.Selected Is Nothing Or oComboYear.Selected Is Nothing Or oComboMonth2.Selected Is Nothing) Then
                                    objAddOn.objApplication.SetStatusBarMessage("Month's and Year Mandatory ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                ElseIf (oForm.Items.Item("4").Specific.value = "" Or oForm.Items.Item("6").Specific.value = "") Then
                                    objAddOn.objApplication.SetStatusBarMessage("From Employee and To Employee Should not be an Empty", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                End If
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("Before Action Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try
                End Select

            Else
                Select Case pVal.EventType

                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        Try
                            If (pVal.ItemUID = "4" Or pVal.ItemUID = "6") Then
                                ChooseFromListEvent(pVal, pVal.ItemUID)
                            End If

                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("After Action CFL Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        oForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" Then
                            sender = ""
                            e = Nothing
                            FIntMonth = oComboMonth1.Selected.Value
                            TIntMonth = oComboMonth2.Selected.Value
                            IntYear = oComboYear.Selected.Value
                            FEID = oForm.Items.Item("4").Specific.string
                            ToEID = oForm.Items.Item("6").Specific.string
                            GenerateReport(FormUID, FIntMonth, IntYear, FEID, ToEID, TIntMonth)
                        End If
                End Select
            End If

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Item Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

#Region "Generate Report"
    Public Sub GenerateReport(ByVal FormUID As String, ByVal FMonth As Integer, ByVal Year As Integer, ByVal FrmEID As String, ByVal ToEID As String, ByVal TMonth As Integer)
        Dim sender As System.Object
        Dim e As System.EventArgs
        sender = ""
        e = Nothing
        Dim RptFrm As FrmRpt
        RptFrm = New FrmRpt
        RptFrm.Refresh()
        Dim cryRpt As New ReportDocument

        Dim objConInfo As New CrystalDecisions.Shared.ConnectionInfo
        Dim oLogonInfo As New CrystalDecisions.Shared.TableLogOnInfo
        'Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
        Dim intCounter As Integer

        cryRpt.Load(System.Windows.Forms.Application.StartupPath & "\Reports\YearClaimReport.rpt")

        Dim crParameterValues As New ParameterValues
        Dim crParameterDiscreteValue As New ParameterDiscreteValue


        Dim SerNam, DbName, UID As String
        SerNam = objAddOn.objCompany.Server
        DbName = objAddOn.objCompany.CompanyDB
        UID = objAddOn.objCompany.DbUserName


        oLogonInfo.ConnectionInfo.ServerName = SerNam
        oLogonInfo.ConnectionInfo.DatabaseName = DbName
        oLogonInfo.ConnectionInfo.UserID = UID
        oLogonInfo.ConnectionInfo.Password = SAPwd ' "AIS@1234"
        'oLogonInfo.ConnectionInfo.Password = "Pass@123"

        For intCounter = 0 To cryRpt.Database.Tables.Count - 1
            cryRpt.Database.Tables(intCounter).ApplyLogOnInfo(oLogonInfo)
        Next


        cryRpt.SetParameterValue("FrmMonth@", FMonth)
        cryRpt.SetParameterValue("ToEmpID@", TMonth)
        cryRpt.SetParameterValue("Year@", IntYear)
        cryRpt.SetParameterValue("FrmEmpID@", FrmEID)
        cryRpt.SetParameterValue("ToEmpID@", ToEID)

        RptFrm.CRViewer.ReportSource = Nothing
        RptFrm.CRViewer.ReportSource = cryRpt
        RptFrm.CRViewer.Refresh()
        System.Windows.Forms.Application.Run(RptFrm)
    End Sub
#End Region

#Region "Month Load in Combo Box"
    Public Sub Month()
        Try
            oComboMonth1.ValidValues.Add("1", "January")
            oComboMonth1.ValidValues.Add("2", "February")
            oComboMonth1.ValidValues.Add("3", "March")
            oComboMonth1.ValidValues.Add("4", "April")
            oComboMonth1.ValidValues.Add("5", "May")
            oComboMonth1.ValidValues.Add("6", "June")
            oComboMonth1.ValidValues.Add("7", "July")
            oComboMonth1.ValidValues.Add("8", "August")
            oComboMonth1.ValidValues.Add("9", "September")
            oComboMonth1.ValidValues.Add("10", "October")
            oComboMonth1.ValidValues.Add("11", "November")
            oComboMonth1.ValidValues.Add("12", "December")
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Month Function Error - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
    Public Sub Month1()
        Try
            oComboMonth2.ValidValues.Add("1", "January")
            oComboMonth2.ValidValues.Add("2", "February")
            oComboMonth2.ValidValues.Add("3", "March")
            oComboMonth2.ValidValues.Add("4", "April")
            oComboMonth2.ValidValues.Add("5", "May")
            oComboMonth2.ValidValues.Add("6", "June")
            oComboMonth2.ValidValues.Add("7", "July")
            oComboMonth2.ValidValues.Add("8", "August")
            oComboMonth2.ValidValues.Add("9", "September")
            oComboMonth2.ValidValues.Add("10", "October")
            oComboMonth2.ValidValues.Add("11", "November")
            oComboMonth2.ValidValues.Add("12", "December")
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Month Function Error - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

#End Region

#Region "Load the Year in Combo Box"
    Public Sub Year()
        Try
            oComboYear.ValidValues.Add("2012", "2012")
            oComboYear.ValidValues.Add("2013", "2013")
            oComboYear.ValidValues.Add("2014", "2014")
            oComboYear.ValidValues.Add("2015", "2015")
            oComboYear.ValidValues.Add("2016", "2016")
            oComboYear.ValidValues.Add("2017", "2017")
            oComboYear.ValidValues.Add("2018", "2018")
            oComboYear.ValidValues.Add("2019", "2019")
            oComboYear.ValidValues.Add("2020", "2020")
            oComboYear.ValidValues.Add("2021", "2021")
            oComboYear.ValidValues.Add("2022", "2022")
            oComboYear.ValidValues.Add("2023", "2023")
            oComboYear.ValidValues.Add("2024", "2024")
            oComboYear.ValidValues.Add("2025", "2025")
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Year Function Error - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region

#Region "Choose From List Event"
    Public Sub ChooseFromListEvent(ByVal Number, ByVal ItemUID)
        Try
            objCFL = Number
            objDT = objCFL.SelectedObjects
            oForm.Items.Item("" & ItemUID & "").Specific.Value = objDT.GetValue("empID", 0)
        Catch ex As Exception
        End Try
    End Sub
#End Region

End Class
