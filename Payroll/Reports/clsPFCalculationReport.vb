﻿Imports System.Data
Imports System.Data.SqlClient
Imports Excel = Microsoft.Office.Interop.Excel
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Xml

Public Class clsPFCalculationReport
    Public Const FormType = "MNU_PFCALRPT"
    Dim oForm As SAPbouiCOM.Form
    Dim oRS As SAPbobsCOM.Recordset
    Dim oComboMonth, oComboYear As SAPbouiCOM.ComboBox
    Dim Month, Year As Integer
    Dim oPath As String

    Public Sub LoadScreeen()
        oForm = objAddOn.objUIXml.LoadScreenXML("PFCalculationReport.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        oComboMonth = oForm.Items.Item("4").Specific
        oComboYear = oForm.Items.Item("6").Specific
        LoadMonth()
        LoadYear()
        objAddOn.objPaySlip.SAPassWord()

    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "1" Or pVal.ItemUID = "7" Then
                        If Validate(FormUID) = False Then
                            BubbleEvent = False
                            Exit Sub
                        End If
                    End If
            End Select

        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    If pVal.ItemUID = "7" Then
                        Month = oComboMonth.Selected.Value
                        Year = oComboYear.Selected.Value
                        CreateExcelFile(Month, Year)
                    End If
            End Select
        End If
    End Sub

#Region "Validate Event"
    Public Function Validate(ByVal FormUID As String) As Boolean
        Try
            oForm = objAddOn.objApplication.Forms.Item(FormUID)
            If oForm.Items.Item("4").Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Select Month", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            ElseIf oForm.Items.Item("6").Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please select Year", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
            Return True
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Before Action - Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Function
#End Region

#Region "Load Month and Year Into Combo Box"

    Sub LoadMonth()
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select Code,Name from [@AIS_MONTH] order by Code")
            If oRS.RecordCount > 0 Then
                For Int As Integer = 0 To oRS.RecordCount
                    oComboMonth.ValidValues.Add(oRS.Fields.Item("Code").Value, oRS.Fields.Item("Name").Value)
                    oRS.MoveNext()
                Next
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Month Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub

    Sub LoadYear()
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select Code,Name from [@year] order by Code")
            If oRS.RecordCount > 0 Then
                For Int As Integer = 0 To oRS.RecordCount
                    oComboYear.ValidValues.Add(oRS.Fields.Item("Code").Value, oRS.Fields.Item("Name").Value)
                    oRS.MoveNext()
                Next
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Year Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub

#End Region

#Region "Export to Excel"

    Private Sub CreateExcelFile(ByVal Month As Integer, ByVal Year As Integer)

        Try
            Dim sql As String
            Dim oDataTable As DataTable

            Dim xlApp As Excel.Application
            Dim xlWorkBook As Excel.Workbook
            Dim xlWorkSheet As Excel.Worksheet
            Dim misValue As Object = System.Reflection.Missing.Value

            xlApp = New Excel.Application
            xlWorkBook = xlApp.Workbooks.Add(misValue)
            xlWorkSheet = xlWorkBook.Sheets("sheet1")

            sql = "declare @limit numeric(19,2) =(select Top 1 U_pflim from [@AIS_STAT] ) " & _
                " declare @Emplr1 numeric(19,2)=(select Top 1 U_pfcemper from [@AIS_STAT] ) " & _
                " declare @Emplr2 numeric(19,2)=(select Top 1 U_pfshare from [@AIS_STAT] ) " & _
                " select CAST('' as varchar) [Sl no],'1' [Member ID], '2' [Member Name],'3' [EPF Wages],'4' [EPS Wages], " & _
                " '5' [EPF Contribution],'6' [EPF Remitted],'7' [EPS Contribution],'8' [EPS Remitted] " & _
                " ,'9' [Diff EPF & EPS Contribution  (ER Share)],'10' [Diff EPF and  EPS Contribution  (ER Share) remitted] " & _
                " ,'11'[NCP Days],'12'[Refund of Advances],'13' [Arrear EPF WAGES],'14' [Arrear EPF EE Share] ,'15'[Arrear EPF ER Share] " & _
                " ,'16'[Arrear EPS],'17' [Fathers Name],'18' [ [Relationship with the member], cast ('19' as varchar) [Date of Birth]" & _
                " ,'20' [Gender],cast('21' as varchar) [Date of Joining EPF],cast('22' as varchar) [Date of Joining EPS] " & _
                " ,cast('23' as varchar) [Date of Exit from EPF],cast('24' as varchar) [Date of Exit from EPS],cast('25' as varchar) [Reason for Leaving] " & _
                " union all " & _
                " select cast(ROW_NUMBER() OVER (ORDER BY x.[Member ID]) as varchar) AS [Sl no],x.[Member ID]  ,x.[Member Name] " & _
                " ,x.Basic [EPF Wages],x.Basic [EPS Wages],x.PF [EPF Contribution],x.PF [EPF Remitted] ,round(x.Basic *@Emplr1/100,0) [EPS Contribution] " & _
                " ,round(x.Basic *@Emplr1/100,0) [EPS Remitted] ,round(x.Basic *@Emplr2/100,0) [Diff EPF & EPS Contribution  (ER Share)] " & _
                "  ,round(x.Basic *@Emplr2/100,0) [Diff EPF and  EPS Contribution  (ER Share) remitted]  ,x.[NCP Days] [NCP Days] " & _
                " ,0 [Refund of Advances],0 [Arrear EPF WAGES],0 [Arrear EPF EE Share],0 [Arrear EPF ER Share] ,0[Arrear EPS],x.FatherName [Fathers Name] " & _
                " ,'' [Relationship with the member],cast(x.birthDate as varchar) [Date of Birth],x.sex [Gender] " & _
                "  ,cast(x.startDate as varchar) [Date of Joining EPF],cast(x.startDate as varchar) [Date of Joining EPS] " & _
                " ,cast(x.termDate as varchar) [Date of Exit from EPF] ,cast(x.termDate as varchar) [Date of Exit from EPS] " & _
                " ,x.termReason [Reason for Leaving]from( select distinct  T0.U_empid [Member ID] ,T0.U_ename [Member Name] " & _
                " ,(select case when @limit < max(U_amount) then @limit else MAX(U_amount ) end from [@AIS_OSPS] where U_heads ='Basic' ) [Basic] " & _
                " ,(select max(U_amount) from [@AIS_OSPS] where U_heads =(select Name from [@AIS_DDC] where U_pf ='Y'))[PF] " & _
                " ,T1.U_absent +T1.U_lop [NCP Days],isnull(T2.FatherName,'') [FatherName],isnull(T2.birthDate,'')[birthDate] " & _
                " ,isnull(t2.sex,'')[sex] ,isnull(T2.startDate,'')[startDate],isnull(T2.termDate,'') [termDate] ,isnull(T2.termReason,'') [termReason] " & _
                "  from [@AIS_OSPS] T0 left join [@AIS_SUMATTEND1] T1 on T0.U_empid =T1.U_eid  left join OHEM T2 on T2.empID =T0.U_empid " & _
                " where T0.U_month =" & Month & " and T0.U_year =" & Year & ") x  "

            oRS.DoQuery(sql)

            oDataTable = ConvertRecordset(oRS)

            oRS.DoQuery("select ExcelPath [Path] from OADM")
            If oRS.Fields.Item("Path").Value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Please Enter the Path in General Setting", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Else
                oPath = oRS.Fields.Item("Path").Value
                SetDataTable_To_CSV(oDataTable, oPath & "\" & Month & "_" & Year & ".csv", ",")
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("CreateExcelFile Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try

    End Sub


    Public Function ConvertRecordset(ByVal SAPRecordset As SAPbobsCOM.Recordset) As System.Data.DataTable
        '\ This function will take an SAP recordset from the SAPbobsCOM library and convert it to a more
        '\ easily used ADO.NET datatable which can be used for data binding much easier.
        Dim dtTable As New System.Data.DataTable
        Dim NewCol As System.Data.DataColumn
        Dim NewRow As System.Data.DataRow
        Dim ColCount As Integer
        Try
            For ColCount = 0 To SAPRecordset.Fields.Count - 1
                NewCol = New System.Data.DataColumn(SAPRecordset.Fields.Item(ColCount).Name)
                dtTable.Columns.Add(NewCol)
            Next

            Do Until SAPRecordset.EoF

                NewRow = dtTable.NewRow
                'populate each column in the row we're creating
                For ColCount = 0 To SAPRecordset.Fields.Count - 1

                    NewRow.Item(SAPRecordset.Fields.Item(ColCount).Name) = SAPRecordset.Fields.Item(ColCount).Value

                Next

                'Add the row to the datatable
                dtTable.Rows.Add(NewRow)


                SAPRecordset.MoveNext()
            Loop

            Return dtTable

        Catch ex As Exception
            MsgBox(ex.ToString & Chr(10) & "Error converting SAP Recordset to DataTable", MsgBoxStyle.Exclamation)

            Exit Function

        End Try
    End Function

#End Region

#Region "EXPORT TO CSV FORMAT FOR PF LIMIT"

    Sub SetDataTable_To_CSV(ByVal dtable As DataTable, ByVal path_filename As String, ByVal sepChar As String)


        Dim writer As System.IO.StreamWriter
        Try
            writer = New System.IO.StreamWriter(path_filename)

            ' first write a line with the columns name
            Dim sep As String = ""
            Dim builder As New System.Text.StringBuilder
            For Each col As DataColumn In dtable.Columns
                builder.Append(sep).Append(col.ColumnName)
                sep = sepChar
            Next
            writer.WriteLine(builder.ToString())

            ' then write all the rows
            For Each row As DataRow In dtable.Rows
                sep = ""
                builder = New System.Text.StringBuilder

                For Each col As DataColumn In dtable.Columns
                    builder.Append(sep).Append(row(col.ColumnName))
                    sep = sepChar
                Next
                writer.WriteLine(builder.ToString())
            Next
            objAddOn.objApplication.SetStatusBarMessage("PF Format Generated Successfully", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Finally
            If Not writer Is Nothing Then writer.Close()
        End Try
    End Sub

#End Region

End Class
