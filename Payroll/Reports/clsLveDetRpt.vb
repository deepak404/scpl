﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Sql
Public Class clsLveDetRpt
    Public Const FormType = "MNU_LVDETPRT"
    Dim oForm As SAPbouiCOM.Form
    Dim EID As String
    Dim objCFL As SAPbouiCOM.ChooseFromListEvent
    Dim objDT As SAPbouiCOM.DataTable
    Public Sub LoadScreen()
        oForm = objAddOn.objUIXml.LoadScreenXML("LveDetRpt.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
        objAddOn.objPaySlip.SAPassWord()
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        Try
                            If pVal.ItemUID = "1" Then
                                If ValidateEvent(FormUID) = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("Before Action - Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try
                End Select

            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        Try
                            If pVal.ItemUID = "1" Then
                                EID = oForm.Items.Item("4").Specific.string
                                ' ToDate = CDate(oForm.Items.Item("6").Specific.value).ToString("yyyyMMdd").ToString
                                GenerateReport(FormUID, EID)
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("After Action - Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        Try

                            ChooseFromListEvent(pVal)


                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("After Action CFL Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try
                End Select
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Item Event Failure", SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

#Region "To Generate Report"
    Public Sub GenerateReport(ByVal FormUID As String, ByVal EID As String)
        Dim sender As System.Object
        Dim e As System.EventArgs
        sender = ""
        e = Nothing
        Dim RptFrm As FrmRpt
        RptFrm = New FrmRpt
        RptFrm.Refresh()
        Dim cryRpt As New ReportDocument

        Dim objConInfo As New CrystalDecisions.Shared.ConnectionInfo
        Dim oLogonInfo As New CrystalDecisions.Shared.TableLogOnInfo
        'Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
        Dim intCounter As Integer

        cryRpt.Load(System.Windows.Forms.Application.StartupPath & "\Reports\LveDetRpt.rpt")

        Dim crParameterValues As New ParameterValues
        Dim crParameterDiscreteValue As New ParameterDiscreteValue


        Dim SerNam, DbName, UID As String
        SerNam = objAddOn.objCompany.Server
        DbName = objAddOn.objCompany.CompanyDB
        UID = objAddOn.objCompany.DbUserName


        ' cryRpt.SetParameterValue("@ToDate", TDate)

        oLogonInfo.ConnectionInfo.ServerName = SerNam
        oLogonInfo.ConnectionInfo.DatabaseName = DbName
        oLogonInfo.ConnectionInfo.UserID = UID
        oLogonInfo.ConnectionInfo.Password = SAPwd '"AIS@1234"
        ' oLogonInfo.ConnectionInfo.Password = "Pass@123"

        For intCounter = 0 To cryRpt.Database.Tables.Count - 1
            cryRpt.Database.Tables(intCounter).ApplyLogOnInfo(oLogonInfo)
        Next
        cryRpt.SetParameterValue(0, EID)

        RptFrm.CRViewer.ReportSource = Nothing
        RptFrm.CRViewer.ReportSource = cryRpt
        ' RptFrm.CRViewer.Refresh()
        System.Windows.Forms.Application.Run(RptFrm)

    End Sub
#End Region

#Region "Valiodation Event For From Date and To Date"
    Public Function ValidateEvent(ByVal FormUID As String) As String
        Try
            If oForm.Items.Item("4").Specific.value = "" Then
                objAddOn.objApplication.SetStatusBarMessage("Employee ID Should Be Mandatory", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Validate Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
        Return True
    End Function
#End Region

#Region "Choose From List Event"
    Public Sub ChooseFromListEvent(ByVal Number)
        Try
            objCFL = Number
            objDT = objCFL.SelectedObjects
            oForm.Items.Item("4").Specific.Value = objDT.GetValue("empID", 0)
        Catch ex As Exception
        End Try
    End Sub
#End Region

End Class
