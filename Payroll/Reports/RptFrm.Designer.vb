﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRpt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Crpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.CRViewer = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.BtnPrint = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Crpt
        '
        Me.Crpt.ActiveViewIndex = -1
        Me.Crpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Crpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.Crpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Crpt.Location = New System.Drawing.Point(0, 0)
        Me.Crpt.Name = "Crpt"
        Me.Crpt.SelectionFormula = ""
        Me.Crpt.Size = New System.Drawing.Size(1076, 472)
        Me.Crpt.TabIndex = 0
        Me.Crpt.ViewTimeSelectionFormula = ""
        '
        'CRViewer
        '
        Me.CRViewer.ActiveViewIndex = -1
        Me.CRViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CRViewer.Cursor = System.Windows.Forms.Cursors.Default
        Me.CRViewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CRViewer.Location = New System.Drawing.Point(0, 0)
        Me.CRViewer.Name = "CRViewer"
        Me.CRViewer.Size = New System.Drawing.Size(1076, 472)
        Me.CRViewer.TabIndex = 1
        '
        'BtnPrint
        '
        Me.BtnPrint.Location = New System.Drawing.Point(7, 2)
        Me.BtnPrint.Name = "BtnPrint"
        Me.BtnPrint.Size = New System.Drawing.Size(75, 23)
        Me.BtnPrint.TabIndex = 2
        Me.BtnPrint.Text = "Print"
        Me.BtnPrint.UseVisualStyleBackColor = True
        '
        'FrmRpt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1076, 472)
        Me.Controls.Add(Me.BtnPrint)
        Me.Controls.Add(Me.CRViewer)
        Me.Controls.Add(Me.Crpt)
        Me.Name = "FrmRpt"
        Me.Text = "RptFrm"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Crpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents CRViewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents BtnPrint As System.Windows.Forms.Button
End Class
