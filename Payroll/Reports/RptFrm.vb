﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data
Imports System
Imports System.Windows.Forms
Imports System.IO
Imports System.Text
Imports System.Threading
Imports System.ComponentModel
Imports System.Collections.Generic
Imports System.Linq

Public Class FrmRpt
    Inherits Form
    Public Sub New()
        InitializeComponent()
    End Sub
    Friend WithEvents Crypt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Public Sub RptFrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' CRViewer.RefreshReport()
        Me.WindowState = FormWindowState.Maximized
    End Sub

    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click
        Try
            Dim report As New ReportDocument
            ' report.PrintOptions.PrinterName = "\\network\printer"
            report.Load(System.Windows.Forms.Application.StartupPath & "\Reports\OvertimeReport.rpt", OpenReportMethod.OpenReportByDefault)
            report.PrintToPrinter(1, False, 0, 0)

        Catch ex As Exception
            '  objAddOn.objApplication.StatusBar.SetText("Show Print File Fun Failed:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
        Finally
        End Try
    End Sub
End Class

