﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Sql
Public Class clsLeaveStatusReport
    Public Const FormType = "MNU_LVSTATRPT"
    Dim oForm As SAPbouiCOM.Form
    Dim oComboBranch, oComboFromDpt, oComboToDpt As SAPbouiCOM.ComboBox
    Dim IntMonth, IntYear As Integer
    Dim FEID, ToEID, Branch, FrmDept, ToDept As String
    Dim sender As System.Object
    Dim e As System.EventArgs
    Dim objCFL As SAPbouiCOM.ChooseFromListEvent
    Dim objDT As SAPbouiCOM.DataTable
     Dim RS As SAPbobsCOM.Recordset

    Public Sub LoadScreen()
        Try
            oForm = objAddOn.objUIXml.LoadScreenXML("LeaveStatusRpt.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
            oComboBranch = oForm.Items.Item("CmbBrnch").Specific
            oComboFromDpt = oForm.Items.Item("CmbDepFrm").Specific
            oComboToDpt = oForm.Items.Item("CmbDeptTo").Specific
            BranchMaster()
            DeptMaster()
            objAddOn.objPaySlip.SAPassWord()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Loadscreen - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        oForm = objAddOn.objApplication.Forms.Item(FormUID)
                        Try
                            If pVal.ItemUID = "1" Then
                                If (oComboBranch.Selected Is Nothing Or oComboFromDpt.Selected Is Nothing Or oComboToDpt.Selected Is Nothing) Then
                                    objAddOn.objApplication.SetStatusBarMessage("Month and Year Mandatory ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                ElseIf (oForm.Items.Item("TxtEmpIdfr").Specific.value = "" Or oForm.Items.Item("TxtEmpIdTo").Specific.value = "") Then
                                    objAddOn.objApplication.SetStatusBarMessage("From Employee and To Employee Should not be an Empty", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                End If
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("Before Action Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try
                End Select

            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        Try
                            If (pVal.ItemUID = "TxtEmpIdfr" Or pVal.ItemUID = "TxtEmpIdTo") Then
                                ChooseFromListEvent(pVal, pVal.ItemUID)
                            End If

                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("After Action CFL Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        oForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" Then
                            sender = ""
                            e = Nothing
                            FEID = oForm.Items.Item("TxtEmpIdfr").Specific.string
                            ToEID = oForm.Items.Item("TxtEmpIdTo").Specific.string

                            'If select All in Branch Combo Box 
                            If (oComboBranch.Selected.Value = "0" Or oComboBranch.Selected.Value = "") Then
                                Branch = ""
                                AddParameters()
                            Else
                                Branch = oComboBranch.Selected.Value
                            End If

                            'If select All in From Department 
                            If (oComboFromDpt.Selected.Value = "0" Or oComboFromDpt.Selected.Value = "") Then
                                FrmDept = ""
                                FromDepartmetnParameter()
                            Else
                                FrmDept = oComboBranch.Selected.Value
                            End If

                            'If Select All in To Department:
                            If (oComboToDpt.Selected.Value = "0" Or oComboToDpt.Selected.Value = "") Then
                                ToDept = ""
                                ToDepartmetnParameter()
                            Else
                                ToDept = oComboBranch.Selected.Value
                            End If
                            GenerateReport(FormUID, Branch, FrmDept, ToDept, FEID, ToEID)
                        End If
                End Select
            End If

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Item Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

#Region "Generate Report"
    Public Sub GenerateReport(ByVal FormUID As String, ByVal Branch As String, ByVal FrmDept As String, ByVal ToDept As String, ByVal FrmEID As String, ByVal ToEID As String)
        Dim sender As System.Object
        Dim e As System.EventArgs
        sender = ""
        e = Nothing
        Dim RptFrm As FrmRpt
        RptFrm = New FrmRpt
        RptFrm.Refresh()
        Dim cryRpt As New ReportDocument

        Dim objConInfo As New CrystalDecisions.Shared.ConnectionInfo
        Dim oLogonInfo As New CrystalDecisions.Shared.TableLogOnInfo
        'Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
        Dim intCounter As Integer

        cryRpt.Load(System.Windows.Forms.Application.StartupPath & "\Reports\LeaveStat.rpt")

        Dim crParameterValues As New ParameterValues
        Dim crParameterDiscreteValue As New ParameterDiscreteValue


        Dim SerNam, DbName, UID As String
        SerNam = objAddOn.objCompany.Server
        DbName = objAddOn.objCompany.CompanyDB
        UID = objAddOn.objCompany.DbUserName


        oLogonInfo.ConnectionInfo.ServerName = SerNam
        oLogonInfo.ConnectionInfo.DatabaseName = DbName
        oLogonInfo.ConnectionInfo.UserID = UID
        oLogonInfo.ConnectionInfo.Password = SAPwd '"AIS@1234"
        'oLogonInfo.ConnectionInfo.Password = "Pass@123"

        For intCounter = 0 To cryRpt.Database.Tables.Count - 1
            cryRpt.Database.Tables(intCounter).ApplyLogOnInfo(oLogonInfo)
        Next

        cryRpt.SetParameterValue("Branch@", Branch)
        cryRpt.SetParameterValue("FrmDept@", FrmDept)
        cryRpt.SetParameterValue("ToDept@", ToDept)
        cryRpt.SetParameterValue("FrmEmpID@", FrmEID)
        cryRpt.SetParameterValue("ToEmpID@", ToEID)

        RptFrm.CRViewer.ReportSource = Nothing
        RptFrm.CRViewer.ReportSource = cryRpt
        RptFrm.CRViewer.Refresh()
        System.Windows.Forms.Application.Run(RptFrm)
    End Sub
#End Region

#Region "Choose From List Event"
    Public Sub ChooseFromListEvent(ByVal Number, ByVal ItemUID)
        Try
            objCFL = Number
            objDT = objCFL.SelectedObjects
            oForm.Items.Item("" & ItemUID & "").Specific.Value = objDT.GetValue("empID", 0)
        Catch ex As Exception
        End Try
    End Sub
#End Region

#Region "To Add Parameter Department"
    Public Sub FromDepartmetnParameter()

        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery("select distinct convert(varchar,Code,108) as Code from OUDP order by Code")
        For IntI As Integer = 0 To RS.RecordCount - 1
            FrmDept += "'"
            FrmDept += RS.Fields.Item("Code").Value
            FrmDept += "',"
            RS.MoveNext()
        Next
        FrmDept += "''"
    End Sub

    Public Sub ToDepartmetnParameter()
        Dim RS As SAPbobsCOM.Recordset
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery("select distinct convert(varchar,Code,108) as Code from OUDP order by Code")
        For IntI As Integer = 0 To RS.RecordCount - 1
            ToDept += "'"
            ToDept += RS.Fields.Item("Code").Value
            ToDept += "',"
            RS.MoveNext()
        Next
        ToDept += "''"
    End Sub
#End Region

#Region "Load the Branches from the Branch Master"
    Sub BranchMaster()
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery("select Code,Name from OUBR order by Code")
        oComboBranch.ValidValues.Add("0", "All")
        For IntI As Integer = 0 To RS.RecordCount - 1
            oComboBranch.ValidValues.Add(RS.Fields.Item("Code").Value, RS.Fields.Item("Name").Value)
            RS.MoveNext()
        Next
    End Sub
#End Region

#Region "Load the Department from the Department Master"
    Sub DeptMaster()
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery("select Code,Name from OUDP order by Code")
        oComboFromDpt.ValidValues.Add("0", "All")
        oComboToDpt.ValidValues.Add("0", "All")
        For IntI As Integer = 0 To RS.RecordCount - 1
            oComboFromDpt.ValidValues.Add(RS.Fields.Item("Code").Value, RS.Fields.Item("Name").Value)
            oComboToDpt.ValidValues.Add(RS.Fields.Item("Code").Value, RS.Fields.Item("Name").Value)
            RS.MoveNext()
        Next
    End Sub
#End Region

#Region "Add all Branch Codes from Branch Master to the Parameter"
    Public Sub AddParameters()
        Dim RS As SAPbobsCOM.Recordset
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery("select distinct convert(varchar,Code,108) as Code from OUBR order by Code")
        For IntI As Integer = 0 To RS.RecordCount - 1
            Branch += "'"
            Branch += RS.Fields.Item("Code").Value
            Branch += "',"
            RS.MoveNext()
        Next
        Branch += "''"
        'Branch = Branch.Substring(0, Branch.Length - 1)
    End Sub
#End Region

End Class
