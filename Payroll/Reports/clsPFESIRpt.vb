﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Sql
Public Class clsPFESIRpt
    Public Const FormType = "MNU_PFESIRPT"
    Dim oForm As SAPbouiCOM.Form
    Dim oComboMonth, oComboYear As SAPbouiCOM.ComboBox
    Dim IntMonth, IntYear As Integer
    Dim EID As String
    Dim sender As System.Object
    Dim e As System.EventArgs

    Public Sub LoadScreen()
        Try
            oForm = objAddOn.objUIXml.LoadScreenXML("PFESIRpt.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
            oComboMonth = oForm.Items.Item("4").Specific
            oComboYear = oForm.Items.Item("6").Specific
            Month()
            Year()
            objAddOn.objPaySlip.SAPassWord()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Loadscreen - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        oForm = objAddOn.objApplication.Forms.Item(FormUID)
                        Try
                            If pVal.ItemUID = "1" Then
                                If (oComboMonth.Selected Is Nothing Or oComboYear.Selected Is Nothing) Then
                                    objAddOn.objApplication.SetStatusBarMessage("Month and Year Mandatory ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                End If
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("Before Action Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try
                End Select

            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        oForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" Then
                            sender = ""
                            e = Nothing
                            IntMonth = oComboMonth.Selected.Value
                            IntYear = oComboYear.Selected.Value
                            GenerateReport(FormUID, IntMonth, IntYear)
                        End If
                End Select
            End If

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Item Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

#Region "Generate Report"
    Public Sub GenerateReport(ByVal FormUID As String, ByVal Month As Integer, ByVal Year As Integer)
        Dim sender As System.Object
        Dim e As System.EventArgs
        sender = ""
        e = Nothing
        Dim RptFrm As FrmRpt
        RptFrm = New FrmRpt
        RptFrm.Refresh()
        Dim cryRpt As New ReportDocument

        Dim objConInfo As New CrystalDecisions.Shared.ConnectionInfo
        Dim oLogonInfo As New CrystalDecisions.Shared.TableLogOnInfo
        'Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
        Dim intCounter As Integer

        cryRpt.Load(System.Windows.Forms.Application.StartupPath & "\Reports\PFESIBoth.rpt")

        Dim crParameterValues As New ParameterValues
        Dim crParameterDiscreteValue As New ParameterDiscreteValue


        Dim SerNam, DbName, UID As String
        SerNam = objAddOn.objCompany.Server
        DbName = objAddOn.objCompany.CompanyDB
        UID = objAddOn.objCompany.DbUserName


        oLogonInfo.ConnectionInfo.ServerName = SerNam
        oLogonInfo.ConnectionInfo.DatabaseName = DbName
        oLogonInfo.ConnectionInfo.UserID = UID
        oLogonInfo.ConnectionInfo.Password = SAPwd ' "AIS@1234"
        ' oLogonInfo.ConnectionInfo.Password = "Pass@123"

        For intCounter = 0 To cryRpt.Database.Tables.Count - 1
            cryRpt.Database.Tables(intCounter).ApplyLogOnInfo(oLogonInfo)
        Next

        cryRpt.SetParameterValue("Year@", IntYear)
        cryRpt.SetParameterValue("Month@", IntMonth)

        RptFrm.CRViewer.ReportSource = Nothing
        RptFrm.CRViewer.ReportSource = cryRpt
        RptFrm.CRViewer.Refresh()
        System.Windows.Forms.Application.Run(RptFrm)
    End Sub
#End Region

#Region "Load the Year in Combo Box"
    Public Sub Year()
        Try
            oComboYear.ValidValues.Add("2012", "2012")
            oComboYear.ValidValues.Add("2013", "2013")
            oComboYear.ValidValues.Add("2014", "2014")
            oComboYear.ValidValues.Add("2015", "2015")
            oComboYear.ValidValues.Add("2016", "2016")
            oComboYear.ValidValues.Add("2017", "2017")
            oComboYear.ValidValues.Add("2018", "2018")
            oComboYear.ValidValues.Add("2019", "2019")
            oComboYear.ValidValues.Add("2020", "2020")
            oComboYear.ValidValues.Add("2021", "2021")
            oComboYear.ValidValues.Add("2022", "2022")
            oComboYear.ValidValues.Add("2023", "2023")
            oComboYear.ValidValues.Add("2024", "2024")
            oComboYear.ValidValues.Add("2025", "2025")
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Year Function Error - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region

#Region "Month Load in Combo Box"
    Public Sub Month()
        Try
            oComboMonth.ValidValues.Add("1", "January")
            oComboMonth.ValidValues.Add("2", "February")
            oComboMonth.ValidValues.Add("3", "March")
            oComboMonth.ValidValues.Add("4", "April")
            oComboMonth.ValidValues.Add("5", "May")
            oComboMonth.ValidValues.Add("6", "June")
            oComboMonth.ValidValues.Add("7", "July")
            oComboMonth.ValidValues.Add("8", "August")
            oComboMonth.ValidValues.Add("9", "September")
            oComboMonth.ValidValues.Add("10", "October")
            oComboMonth.ValidValues.Add("11", "November")
            oComboMonth.ValidValues.Add("12", "December")

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Month Function Error - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region
End Class
