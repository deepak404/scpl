﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Sql
Public Class clsPaymentAdvice
    Public Const FormType = "MNU_PADCE"
    Dim oForm As SAPbouiCOM.Form
    Dim oComboMonth, oComboYear, OCombo, oComboPMode As SAPbouiCOM.ComboBox
    Dim IntMonth, IntYear As Integer
    Dim AttachFile, Branch, PMode As String
    Dim sender As System.Object
    Dim e As System.EventArgs
    Dim RS As SAPbobsCOM.Recordset
    Dim strSQL As String

    Public Sub LoadScreen()
        Try
            oForm = objAddOn.objUIXml.LoadScreenXML("PaymentAdvice.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
            oComboMonth = oForm.Items.Item("4").Specific
            oComboYear = oForm.Items.Item("6").Specific
            OCombo = oForm.Items.Item("8").Specific
            ' MsgBox(oForm.Items.Item("6").Specific.string)
            oComboPMode = oForm.Items.Item("10").Specific
            Month()
            Year()
            BranchLoad()
            LoadPaymentType()
            objAddOn.objPaySlip.SAPassWord()
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Loadscreen - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        oForm = objAddOn.objApplication.Forms.Item(FormUID)
                        Try
                            If pVal.ItemUID = "1" Then
                                If (oComboMonth.Selected Is Nothing Or oComboYear.Selected Is Nothing Or OCombo.Selected Is Nothing) Then
                                    objAddOn.objApplication.SetStatusBarMessage("Month and Year Mandatory ", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                                End If
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("Before Action Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try
                End Select

            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        oForm = objAddOn.objApplication.Forms.Item(FormUID)
                        If pVal.ItemUID = "1" Then
                            sender = ""
                            e = Nothing
                            IntMonth = oComboMonth.Selected.Value
                            IntYear = oComboYear.Selected.Value
                            '  MsgBox(OCombo.Selected.Value)
                            OCombo = oForm.Items.Item("8").Specific
                            If (OCombo.Selected.Value = "0" Or OCombo.Selected Is Nothing) Then
                                Branch = ""
                                AddParameters()
                            Else
                                Branch = OCombo.Selected.Value
                            End If

                            If (oComboPMode.Selected.Value = "0" Or oComboPMode.Selected Is Nothing) Then
                                PMode = ""
                                AddPModeParameter()
                                PMode = Mid(PMode, 1, PMode.Length - 3)
                            Else
                                PMode = oComboPMode.Selected.Value
                                PMode = "'" + PMode + "'"
                                ' MsgBox(PMode)

                            End If
                            GenerateReport(FormUID, IntMonth, IntYear, Branch, PMode)
                        End If
                End Select
            End If

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Item Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

#Region "Generate Report"
    Public Sub GenerateReport(ByVal FormUID As String, ByVal Month As Integer, ByVal Year As Integer, ByVal Branch As String, ByVal PMode As String)
        Dim sender As System.Object
        Dim e As System.EventArgs
        sender = ""
        e = Nothing
        Dim RptFrm As FrmRpt
        RptFrm = New FrmRpt
        RptFrm.Refresh()
        Dim cryRpt As New ReportDocument
        Dim objConInfo As New CrystalDecisions.Shared.ConnectionInfo
        Dim oLogonInfo As New CrystalDecisions.Shared.TableLogOnInfo
        Dim intCounter As Integer

        cryRpt.Load(System.Windows.Forms.Application.StartupPath & "\Reports\PaymentAdvice.rpt")

        Dim crParameterValues As New ParameterValues
        Dim crParameterDiscreteValue As New ParameterDiscreteValue

        Dim SerNam, DbName, UID As String
        SerNam = objAddOn.objCompany.Server
        DbName = objAddOn.objCompany.CompanyDB
        UID = objAddOn.objCompany.DbUserName


        oLogonInfo.ConnectionInfo.ServerName = SerNam
        oLogonInfo.ConnectionInfo.DatabaseName = DbName
        oLogonInfo.ConnectionInfo.UserID = UID
        oLogonInfo.ConnectionInfo.Password = SAPwd '"AIS@1234"
        'oLogonInfo.ConnectionInfo.Password = "Pass@123"

        For intCounter = 0 To cryRpt.Database.Tables.Count - 1
            cryRpt.Database.Tables(intCounter).ApplyLogOnInfo(oLogonInfo)
        Next
        Branch = Replace(Branch, """", "")

        cryRpt.SetParameterValue("Month@", IntMonth)
        cryRpt.SetParameterValue("Year@", IntYear)
        cryRpt.SetParameterValue("Branch@", Branch)
        cryRpt.SetParameterValue("PMode@", PMode)

        RptFrm.CRViewer.ReportSource = Nothing
        RptFrm.CRViewer.ReportSource = cryRpt
        ' RptFrm.CRViewer.Refresh()
        System.Windows.Forms.Application.Run(RptFrm)

    End Sub
#End Region

#Region "Load the Year in Combo Box"
    Public Sub Year()
        Try
            oComboYear.ValidValues.Add("2012", "2012")
            oComboYear.ValidValues.Add("2013", "2013")
            oComboYear.ValidValues.Add("2014", "2014")
            oComboYear.ValidValues.Add("2015", "2015")
            oComboYear.ValidValues.Add("2016", "2016")
            oComboYear.ValidValues.Add("2017", "2017")
            oComboYear.ValidValues.Add("2018", "2018")
            oComboYear.ValidValues.Add("2019", "2019")
            oComboYear.ValidValues.Add("2020", "2020")
            oComboYear.ValidValues.Add("2021", "2021")
            oComboYear.ValidValues.Add("2022", "2022")
            oComboYear.ValidValues.Add("2023", "2023")
            oComboYear.ValidValues.Add("2024", "2024")
            oComboYear.ValidValues.Add("2025", "2025")
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Year Function Error - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region

#Region "Month Load in Combo Box"
    Public Sub Month()
        Try
            oComboMonth.ValidValues.Add("1", "January")
            oComboMonth.ValidValues.Add("2", "February")
            oComboMonth.ValidValues.Add("3", "March")
            oComboMonth.ValidValues.Add("4", "April")
            oComboMonth.ValidValues.Add("5", "May")
            oComboMonth.ValidValues.Add("6", "June")
            oComboMonth.ValidValues.Add("7", "July")
            oComboMonth.ValidValues.Add("8", "August")
            oComboMonth.ValidValues.Add("9", "September")
            oComboMonth.ValidValues.Add("10", "October")
            oComboMonth.ValidValues.Add("11", "November")
            oComboMonth.ValidValues.Add("12", "December")

        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Month Function Error - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub
#End Region

#Region "Load the payment Type in Combo Box"
    Sub LoadPaymentType()
        OCombo = oForm.Items.Item("10").Specific
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery("select Code,Name from [@AIS_EMP]")
        OCombo.ValidValues.Add("0", "All")
        For IntRow As Integer = 0 To RS.RecordCount - 1
            OCombo.ValidValues.Add(RS.Fields.Item("Code").Value, RS.Fields.Item("Name").Value)
            RS.MoveNext()
        Next
    End Sub

    Sub AddPModeParameter()
        Dim RS As SAPbobsCOM.Recordset
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery("select Code,Name from [@AIS_EMP]")
        For IntI As Integer = 0 To RS.RecordCount - 1
            PMode += "'"
            PMode += RS.Fields.Item("Code").Value
            PMode += "',"
            RS.MoveNext()
        Next
        PMode += "''"
    End Sub
#End Region

#Region "Load the Branch from Branch Master to Combo Box"
    Sub BranchLoad()
        OCombo = oForm.Items.Item("8").Specific
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery("select Code,Name from OUBR order by Code")
        OCombo.ValidValues.Add("0", "All")
        For IntRow As Integer = 0 To RS.RecordCount - 1
            OCombo.ValidValues.Add(RS.Fields.Item("Code").Value, RS.Fields.Item("Name").Value)
            RS.MoveNext()
        Next
    End Sub

#End Region

#Region "Add all Branch Codes from Branch Master to the Parameter"
    Public Sub AddParameters()
        Dim RS As SAPbobsCOM.Recordset
        RS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        RS.DoQuery("select distinct convert(varchar,Code,108) as Code from OUBR order by Code")
        For IntI As Integer = 0 To RS.RecordCount - 1
            Branch += "'"
            Branch += RS.Fields.Item("Code").Value
            Branch += "',"
            RS.MoveNext()
        Next
        Branch += "''"
        'Branch = Branch.Substring(0, Branch.Length - 1)
    End Sub
#End Region

End Class
