﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Sql

Public Class clsInOutTimeRpt

    Public Const FormType = "MNU_IOTR"
    Dim oForm As SAPbouiCOM.Form
    Dim FrmDate, ToDate As String
    Dim OuserDS As SAPbouiCOM.UserDataSource
    Public Sub LoadScreen()
        oForm = objAddOn.objUIXml.LoadScreenXML("InOutTimeRpt.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
        objAddOn.objPaySlip.SAPassWord()
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        Try
                            If pVal.ItemUID = "1" Then
                                If ValidateEvent(FormUID) = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("Before Action - Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try

                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        Try
                            If pVal.ItemUID = "1" Then
                                FrmDate = oForm.Items.Item("4").Specific.value
                                ToDate = oForm.Items.Item("6").Specific.value
                                'FrmDate = CDate(oForm.Items.Item("4").Specific.value).ToString("yyyyMMdd")
                                'ToDate = CDate(oForm.Items.Item("6").Specific.value).ToString("yyyyMMdd")
                                GenerateReport(FormUID, FrmDate, ToDate)
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("After Action - Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try

                    Case SAPbouiCOM.BoEventTypes.et_GOT_FOCUS
                        'Try
                        '    If (pVal.ItemUID = "4" Or pVal.ItemUID = "6") Then
                        '        If oForm.Items.Item("4").Specific.value <> "" Then
                        '            oForm.Items.Item("4").Specific.value = ""
                        '        ElseIf oForm.Items.Item("4").Specific.value <> "" Then
                        '            oForm.Items.Item("6").Specific.value = ""
                        '        End If
                        '    End If
                        'Catch ex As Exception
                        '    objAddOn.objApplication.SetStatusBarMessage("Before Action - Got Focus Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        'End Try
                End Select
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Item Event Failure", SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

#Region "Generate Report"
    Public Sub GenerateReport(ByVal FormUID As String, ByVal FDate As String, ByVal TDate As String)
        GC.Collect()

        Dim sender As System.Object
        Dim e As System.EventArgs
        sender = ""
        e = Nothing
        Dim RptFrm As FrmRpt
        RptFrm = New FrmRpt
        RptFrm.Refresh()
        Dim cryRpt As New ReportDocument
        Dim objConInfo As New CrystalDecisions.Shared.ConnectionInfo
        Dim oLogonInfo As New CrystalDecisions.Shared.TableLogOnInfo
        Dim intCounter As Integer

        cryRpt.Load(System.Windows.Forms.Application.StartupPath & "\Reports\InOutTime.rpt")

        Dim crParameterValues As New ParameterValues
        Dim crParameterDiscreteValue As New ParameterDiscreteValue

        Dim SerNam, DbName, UID As String
        SerNam = objAddOn.objCompany.Server
        DbName = objAddOn.objCompany.CompanyDB
        UID = objAddOn.objCompany.DbUserName


        oLogonInfo.ConnectionInfo.ServerName = SerNam
        oLogonInfo.ConnectionInfo.DatabaseName = DbName
        oLogonInfo.ConnectionInfo.UserID = UID
        oLogonInfo.ConnectionInfo.Password = SAPwd '"AIS@1234"
        ' oLogonInfo.ConnectionInfo.Password = "Pass@123"

        For intCounter = 0 To cryRpt.Database.Tables.Count - 1
            cryRpt.Database.Tables(intCounter).ApplyLogOnInfo(oLogonInfo)
        Next

        cryRpt.SetParameterValue("FrmDate@", FDate)
        cryRpt.SetParameterValue("ToDate@", TDate)
        RptFrm.CRViewer.ReportSource = Nothing
        RptFrm.CRViewer.ReportSource = cryRpt
        ' RptFrm.CRViewer.Refresh()

        System.Windows.Forms.Application.Run(RptFrm)

        GC.Collect()

    End Sub
#End Region

    <STAThread()> _
    Shared Sub Main(ByVal args As String())

    End Sub


#Region " Print the report "
    Sub showprintreport()
        'Dim pDoc As New System.Drawing.Printing.PrintDocument()
        'Dim PrintLayout As New CrystalDecisions.Shared.PrintLayoutSettings()
        'Dim printerSettings As New System.Drawing.Printing.PrinterSettings()
        'printerSettings.PrinterName = cboCurrentPrinters.SelectedItem.ToString()
        '' don't use this, use the new button
        ''PrintLayout.Scaling = PrintLayoutSettings.PrintScaling.DoNotScale;
        'Dim pSettings As New System.Drawing.Printing.PageSettings(printerSettings)
        ''rpt.PrintOptions.DissociatePageSizeAndPrinterPaperSize = false;
        'rpt.PrintOptions.PrinterDuplex = PrinterDuplex.Simplex
        'Dim pageSettings As New System.Drawing.Printing.PageSettings(printerSettings)
        'If pDoc.DefaultPageSettings.PaperSize.Height > pDoc.DefaultPageSettings.PaperSize.Width Then
        '    rpt.PrintOptions.DissociatePageSizeAndPrinterPaperSize = True
        '    rptClientDoc.PrintOutputController.ModifyPaperOrientation(CrPaperOrientationEnum.crPaperOrientationPortrait)
        'Else
        '    rpt.PrintOptions.DissociatePageSizeAndPrinterPaperSize = True
        '    rptClientDoc.PrintOutputController.ModifyPaperOrientation(CrPaperOrientationEnum.crPaperOrientationLandscape)
        'End If
        'rpt.PrintToPrinter(printerSettings, pSettings, False, PrintLayout)
        'MessageBox.Show(rpt.PrintOptions.PrinterName.ToString())

    End Sub

#End Region


#Region "Valiodation Event For From Date and To Date"
    Public Function ValidateEvent(ByVal FormUID As String) As String
        Try
            If (oForm.Items.Item("4").Specific.value = "" Or oForm.Items.Item("6").Specific.value = "") Then
                objAddOn.objApplication.SetStatusBarMessage("From Date and ToDate is Mandatory", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Validate Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
        Return True
    End Function
#End Region


End Class
