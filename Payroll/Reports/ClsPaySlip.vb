Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data
Imports System.Net
Imports System.Net.Mail

Public Class ClsPaySlip
    Public Const formtype = "PaySlip"
    Public objForm As SAPbouiCOM.Form
    Public objComboMonth, objComboYear As SAPbouiCOM.ComboBox
    Dim objCFLEvent As SAPbouiCOM.ChooseFromListEvent
    Dim objDataTable As SAPbouiCOM.DataTable
    Dim AttachFile, EmpName, EmpID, MonthName, SQL, oPath As String
    Dim oRS As SAPbobsCOM.Recordset

    Public Sub LoadScreen()
        objForm = objAddOn.objUIXml.LoadScreenXML("PaySlip.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, formtype)
        objComboMonth = objForm.Items.Item("10").Specific
        objComboYear = objForm.Items.Item("4").Specific
        Combo_Month()
        Combo_Year()
        SAPassWord()
        PayslipPath()
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "9" Then
                            If Validation() = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If
                End Select
            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                        If pVal.ItemUID = "6" Then
                            EmpidCFL(pVal)
                        ElseIf pVal.ItemUID = "8" Then
                            EmpidCFL1(pVal)
                        End If
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        If pVal.ItemUID = "9" Then
                            Dim ORecMail As SAPbobsCOM.Recordset
                            ORecMail = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            Dim IntYear, IntMonth As Integer
                            Dim FromEmpID As String
                            Dim ToEmpID, EmailId As String
                            IntMonth = objComboMonth.Selected.Description
                            MonthName = objComboMonth.Selected.Value
                            IntYear = objComboYear.Selected.Value
                            FromEmpID = objForm.Items.Item("6").Specific.value
                            ToEmpID = objForm.Items.Item("8").Specific.value
                            Dim StrSql As String
                            '  Dim oProgressBar As SAPbouiCOM.ProgressBar
                            'StrSql = "Select * from OHEM where EmpId >=" & FromEmpID & " and EmpId <=" & ToEmpID & " and Active ='Y'"
                            StrSql = "select empID ,firstName ,email from [@AIS_OSPS] left join OHEM on [@AIS_OSPS].U_empid =empID " & _
                                " where U_month =" & IntMonth & " and U_year ='" & IntYear & "' and U_heads ='Pay' and " & _
                                " U_amount ='Y' and empID between '" & FromEmpID & "' and '" & ToEmpID & "'"

                            ORecMail.DoQuery(StrSql)
                            Try
                                '     oProgressBar = objAddOn.objApplication.StatusBar.CreateProgressBar("Please Wait While Payslip Generating......", ORecMail.RecordCount, False)
                                If ORecMail.RecordCount > 0 Then
                                    ' objAddOn.objApplication.StatusBar.SetText("Please Wait While Payslip Generating....", SAPbouiCOM.BoMessageTime.bmt_Long, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                                    ' If ORecMail.EoF = False Then
                                    'While ORecMail.EoF = False

                                    For i As Integer = 0 To ORecMail.RecordCount - 1
                                        '    oProgressBar.Value = i + 1
                                        EmailId = ORecMail.Fields.Item("email").Value
                                        EmpName = ORecMail.Fields.Item("firstName").Value
                                        EmpID = ORecMail.Fields.Item("empID").Value
                                        objAddOn.objApplication.SetStatusBarMessage("Start Emp ID " & EmpID, SAPbouiCOM.BoMessageTime.bmt_Short, False)

                                        If Generate_Report() = True Then
                                            ' Send_Mail_toEmployee(AttachFile, EmailId)
                                        End If
                                        objAddOn.objApplication.SetStatusBarMessage("End Emp ID " & EmpID, SAPbouiCOM.BoMessageTime.bmt_Short, False)

                                        ORecMail.MoveNext()
                                        '  objAddOn.objApplication.SetStatusBarMessage("Move to Next ", SAPbouiCOM.BoMessageTime.bmt_Short, False)

                                        'End While
                                    Next
                                    '  oProgressBar.Stop()
                                    'End If
                                    objAddOn.objApplication.SetStatusBarMessage("You can Find the Payslip for the Month of " & MonthName & " - " & IntYear & " in the Following Path " & oPath & "", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                Else
                                    objAddOn.objApplication.SetStatusBarMessage("Please check Salary Processing " & MonthName & " - " & IntYear & "", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                                End If
                            Catch ex As Exception
                                '  objAddOn.objApplication.MessageBox(ex.Message)
                                objAddOn.objApplication.SetStatusBarMessage("Pay Slip:" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                            Finally
                                '  oProgressBar.Stop()
                            End Try
                           
                        End If
                End Select
            End If
        Catch ex As Exception
            '   objAddOn.objApplication.MessageBox(ex.Message)
            objAddOn.objApplication.SetStatusBarMessage("PaySlip :" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub

#Region "Generate PDF Payslip"
    Public Function Generate_Report() As Boolean
        Try

            Dim IntYear, IntMonth As Integer
            Dim FromEmpID As String
            Dim ToEmpID As String
            IntMonth = objComboMonth.Selected.Description
            IntYear = objComboYear.Selected.Value
            FromEmpID = objForm.Items.Item("6").Specific.value
            ToEmpID = objForm.Items.Item("8").Specific.value

            Dim sender As System.Object
            Dim e As System.EventArgs
            sender = ""
            e = Nothing
            Dim RptFrm As FrmRpt
            RptFrm = New FrmRpt
            RptFrm.Refresh()
            Dim cryRpt As New ReportDocument
            Dim ERRPT As New ReportDocument
            Dim objConInfo As New CrystalDecisions.Shared.ConnectionInfo
            Dim oLogonInfo As New CrystalDecisions.Shared.TableLogOnInfo
            'Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
            Dim intCounter As Integer
            'Dim Formula As String

            cryRpt.Load(System.Windows.Forms.Application.StartupPath & "\Reports\PaySlip2.rpt")


            Dim crParameterValues As New ParameterValues
            Dim crParameterDiscreteValue As New ParameterDiscreteValue

            'Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            'Dim crParameterFieldDefinition As ParameterFieldDefinition
            'Dim crParameterValues As New ParameterValues
            'Dim crParameterDiscreteValue As New ParameterDiscreteValue

            'crParameterDiscreteValue.Value = Convert.ToInt32(EmpID)
            'crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields
            'crParameterFieldDefinition = crParameterFieldDefinitions.Item("EmpId@")
            ''crParameterValues.Clear()
            'crParameterValues = crParameterFieldDefinition.CurrentValues
            'crParameterValues.Add(crParameterDiscreteValue)
            'crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)


            'crParameterDiscreteValue.Value = Convert.ToInt32(IntMonth)
            'crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields
            'crParameterFieldDefinition = crParameterFieldDefinitions.Item("Month@")
            'crParameterValues = crParameterFieldDefinition.CurrentValues
            'crParameterValues.Add(crParameterDiscreteValue)
            'crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            'crParameterDiscreteValue.Value = Convert.ToInt32(IntYear)
            'crParameterFieldDefinitions = cryRpt.DataDefinition.ParameterFields
            'crParameterFieldDefinition = crParameterFieldDefinitions.Item("Year@")
            'crParameterValues = crParameterFieldDefinition.CurrentValues
            'crParameterValues.Add(crParameterDiscreteValue)
            'crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            Dim SerNam, DbName, UID As String
            SerNam = objAddOn.objCompany.Server
            DbName = objAddOn.objCompany.CompanyDB
            UID = objAddOn.objCompany.DbUserName

            oLogonInfo.ConnectionInfo.ServerName = SerNam
            oLogonInfo.ConnectionInfo.DatabaseName = DbName
            oLogonInfo.ConnectionInfo.UserID = UID
            oLogonInfo.ConnectionInfo.Password = SAPwd '"AIS@1234"
            'oLogonInfo.ConnectionInfo.Password = "Pass@123"


            For intCounter = 0 To cryRpt.Database.Tables.Count - 1
                cryRpt.Database.Tables(intCounter).ApplyLogOnInfo(oLogonInfo)
            Next


            cryRpt.SetParameterValue("Month@", IntMonth)
            cryRpt.SetParameterValue("Year@", IntYear)
            cryRpt.SetParameterValue("EmpID@", EmpID)


            Dim CrExportOptions As ExportOptions
            Dim CrDiskFileDestinationOptions As New DiskFileDestinationOptions()
            Dim CrExcelFormat As New ExcelFormatOptions
            Dim CrFormatTypeOptions As New PdfRtfWordFormatOptions
            Dim CrExcelTypeOptions As New ExcelFormatOptions

            oRS.DoQuery("select  Name from [@AIS_SAPWD] where Code='Path'")
            oPath = oRS.Fields.Item("Name").Value & "\"


            AttachFile = oPath & "" & EmpID & "_" & EmpName & "_" & MonthName & "_" & IntYear & " .pdf"
            CrDiskFileDestinationOptions.DiskFileName = AttachFile
            CrExportOptions = cryRpt.ExportOptions
            With CrExportOptions
                .ExportDestinationType = ExportDestinationType.DiskFile
                .ExportFormatType = ExportFormatType.PortableDocFormat
                .DestinationOptions = CrDiskFileDestinationOptions
                .FormatOptions = CrFormatTypeOptions
            End With
            cryRpt.Export()
            objAddOn.objApplication.SetStatusBarMessage("PaySlip Generation Successfulluy:  " & EmpID, SAPbouiCOM.BoMessageTime.bmt_Short, False)

            Return True
       
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("PaySlip Generation Failed for Emp id:  " & EmpID & ";" & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
            Return False
        End Try
    End Function

#End Region

#Region " To Get Path from Administration"
    Sub PayslipPath()
        oRS.DoQuery("select oadp.AttachPath [Path]from OADP")
        If oRS.Fields.Item("Path").Value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Enter the Path in General Setting", SAPbouiCOM.BoMessageTime.bmt_Short, False)
        Else
            oPath = oRS.Fields.Item("Path").Value
        End If
    End Sub
#End Region


#Region "Month"
    Public Sub Combo_Month()
        Try
            If objComboMonth.ValidValues.Count > 0 Then
                Dim J As Integer
                For J = objComboMonth.ValidValues.Count - 1 To 0 Step -1
                    objComboMonth.ValidValues.Remove(J, SAPbouiCOM.BoSearchKey.psk_Index)
                Next
            End If
            objComboMonth.ValidValues.Add("January", "01")
            objComboMonth.ValidValues.Add("February", "02")
            objComboMonth.ValidValues.Add("March", "03")
            objComboMonth.ValidValues.Add("April", "04")
            objComboMonth.ValidValues.Add("May", "05")
            objComboMonth.ValidValues.Add("June", "06")
            objComboMonth.ValidValues.Add("July", "07")
            objComboMonth.ValidValues.Add("August", "08")
            objComboMonth.ValidValues.Add("September", "09")
            objComboMonth.ValidValues.Add("October", "10")
            objComboMonth.ValidValues.Add("November", "11")
            objComboMonth.ValidValues.Add("December", "12")
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub

#End Region

#Region "Year"
    Public Sub Combo_Year()
        Try
            If objComboYear.ValidValues.Count > 0 Then
                Dim J As Integer
                For J = objComboYear.ValidValues.Count - 1 To 0 Step -1
                    objComboYear.ValidValues.Remove(J, SAPbouiCOM.BoSearchKey.psk_Index)
                Next
            End If
            objComboYear.ValidValues.Add("2013", "2013")
            objComboYear.ValidValues.Add("2014", "2014")
            objComboYear.ValidValues.Add("2015", "2015")
            objComboYear.ValidValues.Add("2016", "2016")
            objComboYear.ValidValues.Add("2017", "2017")
            objComboYear.ValidValues.Add("2018", "2018")
            objComboYear.ValidValues.Add("2019", "2019")
            objComboYear.ValidValues.Add("2020", "2020")
            objComboYear.ValidValues.Add("2021", "2021")
            objComboYear.ValidValues.Add("2022", "2022")
        Catch ex As Exception
            objAddOn.objApplication.MessageBox(ex.ToString)
        End Try
    End Sub
#End Region

#Region "EmpID_CFL"
    Public Sub EmpidCFL(ByRef pval As SAPbouiCOM.ItemEvent)
        objCFLEvent = pval
        objDataTable = objCFLEvent.SelectedObjects
        If objDataTable Is Nothing Then
        Else
            Try
                objForm.Items.Item("6").Specific.value = objDataTable.GetValue("empID", 0)
            Catch ex As Exception

            End Try
        End If
    End Sub
#End Region

#Region "EmpID_CFL"
    Public Sub EmpidCFL1(ByRef pval As SAPbouiCOM.ItemEvent)
        objCFLEvent = pval
        objDataTable = objCFLEvent.SelectedObjects
        If objDataTable Is Nothing Then
        Else
            Try
                objForm.Items.Item("8").Specific.value = objDataTable.GetValue("empID", 0)
            Catch ex As Exception

            End Try
        End If
    End Sub
#End Region

#Region "Validation"
    Public Function Validation() As Boolean
        If objComboMonth.Selected Is Nothing Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select the Month", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objComboYear.Selected Is Nothing Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select the Year", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("6").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select - From Employee", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf objForm.Items.Item("8").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select - To Employee", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function
#End Region

#Region "Send Mail"
    Private Sub Send_Mail_toEmployee(ByVal Path As String, ByVal toid As String)
        Try
            Dim strsql, SSERVER, Port As String
            Dim From_Mail_Password As String = ""
            Dim From_Mail_Id As String = ""
            Dim SmtpMail As New MailMessage()
            Dim dtheader As DataTable
            Dim ORec As SAPbobsCOM.Recordset
            ORec = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            strsql = "select * from [@RMP_SRV]"
            ORec.DoQuery(strsql)
            dtheader = New DataTable
            '  Dim I As Integer
            If ORec.RecordCount > 0 Then
                While ORec.EoF = False
                    'MsgBox(Mid(dtheader.Rows(i).Item("Code").ToString(), 1, 2))
                    If ORec.Fields.Item("Code").Value = "SSERVER" Then
                        SSERVER = ORec.Fields.Item("U_email").Value

                    ElseIf ORec.Fields.Item("Code").Value = "FROM" Then
                        From_Mail_Id = ORec.Fields.Item("U_email").Value
                        SmtpMail.From = New MailAddress(From_Mail_Id)

                    ElseIf ORec.Fields.Item("Code").Value = "PORT" Then
                        Port = ORec.Fields.Item("U_email").Value

                    ElseIf Mid(ORec.Fields.Item("Code").Value, 1, 1) = "C" Then
                        SmtpMail.CC.Add(ORec.Fields.Item("U_email").Value)

                    ElseIf Mid(ORec.Fields.Item("Code").Value, 1, 1) = "B" Then
                        SmtpMail.Bcc.Add(ORec.Fields.Item("U_email").Value)

                        'ElseIf ORec.Fields.Item("Code").Value = "TO" Then
                        '    SmtpMail.[To].Add(ORec.Fields.Item("U_email").Value)

                        'ElseIf ORec.Fields.Item("Code").Value = "PWD" Then
                        '    From_Mail_Password = ORec.Fields.Item("U_email").Value

                    End If
                    ORec.MoveNext()
                End While
            End If

            Dim attachment As System.Net.Mail.Attachment
            attachment = New System.Net.Mail.Attachment(AttachFile)
            SmtpMail.Attachments.Add(attachment)

            ' Dim oAttch As Mail.Attachment = New Mail.Attachment(AttachFile)
            If toid <> "" Then
                SmtpMail.From = New MailAddress(From_Mail_Id)
                SmtpMail.[To].Add(toid)
                '  SmtpMail.Attachments.Add(oAttch)
                SmtpMail.Subject = "Payslip_" & objComboMonth.Selected.Value & "_" & objComboYear.Selected.Value

                'Dim sb As New System.Text.StringBuilder
                'sb.AppendLine("PFA for Payslip for the Month of " & objComboMonth.Selected.Value & " - " & objComboYear.Selected.Value)
                'sb.AppendLine(vbCrLf)
                'sb.AppendLine("")
                'sb.AppendLine("")
                'sb.AppendLine("Regards,")
                'sb.AppendLine(Environment.NewLine)
                'sb.AppendLine("HR - iValue")

                Dim myMessage As String = "PFA for Payslip for the Month of " & objComboMonth.Selected.Value & " - " & objComboYear.Selected.Value + Environment.NewLine + Environment.NewLine + Environment.NewLine
                myMessage = myMessage + "Regards," + Environment.NewLine
                myMessage = myMessage + "HR - iValue."

                '  SmtpMail.Body = "PFA for Payslip for the Month of " & objComboMonth.Selected.Value & " - " & objComboYear.Selected.Value + vbCrLf + Environment.NewLine + vbCrLf + Environment.NewLine + vbCrLf + Environment.NewLine + "Regard's," + vbCrLf + Environment.NewLine() + "HR - iValue"
                SmtpMail.Body = myMessage
                '  SmtpMail.IsBodyHtml = True

                'From_Mail_Id = "hr@ivalue.co.in"
                'From_Mail_Password = "AIS@1234"
                Dim oSmtp As New SmtpClient(SSERVER, Port)
                oSmtp.EnableSsl = True
                oSmtp.DeliveryMethod = SmtpDeliveryMethod.Network
                '  oSmtp.Timeout = 690000
                oSmtp.Credentials = New System.Net.NetworkCredential(From_Mail_Id, "AIS@1234")
                ' oSmtp.EnableSsl = True

                Try
                    objAddOn.objApplication.SetStatusBarMessage("Please Wait.... Sending Mail for " & EmpName & "", SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    oSmtp.Send(SmtpMail)
                    objAddOn.objApplication.SetStatusBarMessage("Mail has sent to  " & EmpName & "", SAPbouiCOM.BoMessageTime.bmt_Medium, False)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Else
                objAddOn.objApplication.SetStatusBarMessage("Please Enter E-Mail Address for " & EmpName & " in Employee Master Data", SAPbouiCOM.BoMessageTime.bmt_Short, False)
            End If


        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Taking SA Password from Table"
    Public Sub SAPassWord()
        Try

            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            SQL = "select * from [@AIS_SAPWD] where Code ='" & objAddOn.objCompany.DbUserName & "'"
            oRS.DoQuery(SQL)
            If oRS.RecordCount > 0 Then
                SAPwd = oRS.Fields.Item("Name").Value
                If SAPwd = "" Then
                    objAddOn.objApplication.MessageBox("Please Update the SQL Password in SQL Credential Table")
                End If
            Else

                objAddOn.objApplication.MessageBox("Please Update the SQL Password in SQL Credential Table")

            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("SA Passwod Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)

        End Try
    End Sub
#End Region

End Class
