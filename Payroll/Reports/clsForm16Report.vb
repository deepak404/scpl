﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Sql
Public Class clsForm16Report
    Dim oForm As SAPbouiCOM.Form
    Public Const FormType = "MNU_FRM16RPT"
    Dim oRS As SAPbobsCOM.Recordset
    Dim oComboYearFrom, oComboYearTo As SAPbouiCOM.ComboBox
    Dim EID, ToYear As Integer
    Dim FrmYear As String

    Public Sub LoadScreen()
        oForm = objAddOn.objUIXml.LoadScreenXML("Form16Report.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        oComboYearFrom = oForm.Items.Item("4").Specific
        ' oComboYearTo = oForm.Items.Item("6").Specific
        LoadYear()
        objAddOn.objPaySlip.SAPassWord()
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        If pVal.BeforeAction = True Then
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        If pVal.ItemUID = "1" Then
                            If Validate(FormUID) = False Then
                                BubbleEvent = False
                                Exit Sub
                            End If
                        End If
                        
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("Before Action Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try
            End Select

        Else
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    Try
                        If pVal.ItemUID = "8" Or pVal.ItemUID = "10" Then
                            ChooseItem(FormUID, pVal)
                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("After Action CFL Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try

                Case SAPbouiCOM.BoEventTypes.et_CLICK
                    Try
                        If pVal.ItemUID = "1" Then
                            FrmYear = oComboYearFrom.Selected.Value
                            ' ToYear = oComboYearTo.Selected.Value
                            oRS.DoQuery("select empID from OHEM where Active='Y' and empID>=" & oForm.Items.Item("8").Specific.string & " and empID<=" & oForm.Items.Item("10").Specific.string & "")
                            For IntI As Integer = 1 To oRS.RecordCount
                                EID = oRS.Fields.Item("empID").Value
                                GenerateReport(FormUID, FrmYear, EID)
                                oRS.MoveNext()
                            Next
                        End If
                    Catch ex As Exception
                        objAddOn.objApplication.SetStatusBarMessage("After Action Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
                    End Try
            End Select
        End If
    End Sub

#Region "Generate Report"
    Public Sub GenerateReport(ByVal FormUID As String, ByVal FinYear As String, ByVal EID As Integer)
        Dim sender As System.Object
        Dim e As System.EventArgs
        sender = ""
        e = Nothing
        Dim RptFrm As FrmRpt
        RptFrm = New FrmRpt
        RptFrm.Refresh()
        Dim cryRpt As New ReportDocument

        Dim objConInfo As New CrystalDecisions.Shared.ConnectionInfo
        Dim oLogonInfo As New CrystalDecisions.Shared.TableLogOnInfo
        'Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
        Dim intCounter As Integer

        cryRpt.Load(System.Windows.Forms.Application.StartupPath & "\Reports\Form16.rpt")

        Dim crParameterValues As New ParameterValues
        Dim crParameterDiscreteValue As New ParameterDiscreteValue


        Dim SerNam, DbName, UID As String
        SerNam = objAddOn.objCompany.Server
        DbName = objAddOn.objCompany.CompanyDB
        UID = objAddOn.objCompany.DbUserName


        oLogonInfo.ConnectionInfo.ServerName = SerNam
        oLogonInfo.ConnectionInfo.DatabaseName = DbName
        oLogonInfo.ConnectionInfo.UserID = UID
        oLogonInfo.ConnectionInfo.Password = SAPwd '"AIS@1234"
        ' oLogonInfo.ConnectionInfo.Password = "Pass@123"

        For intCounter = 0 To cryRpt.Database.Tables.Count - 1
            cryRpt.Database.Tables(intCounter).ApplyLogOnInfo(oLogonInfo)
        Next

        cryRpt.SetParameterValue("EmpID@", EID)
        cryRpt.SetParameterValue("FYear@", FinYear)
        ' cryRpt.SetParameterValue("EYear@", Toyr)
        cryRpt.SetParameterValue("SubEmpID@", EID)

        RptFrm.CRViewer.ReportSource = Nothing
        RptFrm.CRViewer.ReportSource = cryRpt
        RptFrm.CRViewer.Refresh()
        System.Windows.Forms.Application.Run(RptFrm)
    End Sub
#End Region


#Region "Load the Year In Combo Box"
    Sub LoadYear()
        Try
            oRS = objAddOn.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRS.DoQuery("select distinct category from ofpr order by category")
            If oRS.RecordCount > 0 Then
                For Int As Integer = 0 To oRS.RecordCount
                    oComboYearFrom.ValidValues.Add(oRS.Fields.Item("category").Value, oRS.Fields.Item("category").Value)
                    'oComboYearTo.ValidValues.Add(oRS.Fields.Item("Code").Value, oRS.Fields.Item("Name").Value)
                    oRS.MoveNext()
                Next
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Load Year Function Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, False)
        End Try
    End Sub
#End Region

#Region "Choose From List"
    Public Sub ChooseItem(ByVal FormUID As String, ByVal pval As SAPbouiCOM.ItemEvent)
        Dim objcfl As SAPbouiCOM.ChooseFromListEvent
        Dim objdt As SAPbouiCOM.DataTable
        objcfl = pval
        objdt = objcfl.SelectedObjects
        If objdt Is Nothing Then
        Else
            Try
                Select Case pval.ItemUID
                    Case "8"
                        '  oForm.Items.Item("21").Specific.string = objdt.GetValue("firstName", 0) + " " + objdt.GetValue("lastName", 0)
                        oForm.Items.Item("8").Specific.string = objdt.GetValue("empID", 0)
                    Case "10"
                        '  oForm.Items.Item("22").Specific.string = objdt.GetValue("firstName", 0) + " " + objdt.GetValue("lastName", 0)
                        oForm.Items.Item("10").Specific.string = objdt.GetValue("empID", 0)
                End Select
            Catch ex As Exception

            End Try
        End If
    End Sub
#End Region

#Region "Validate Event"

    Public Function Validate(ByVal FormUID As String) As Boolean
        oForm = objAddOn.objApplication.Forms.Item(FormUID)
        If oForm.Items.Item("4").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select Perid From", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
            ''ElseIf oForm.Items.Item("6").Specific.value = "" Then
            ''    objAddOn.objApplication.SetStatusBarMessage("Please Select Perid To", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            ''    Return False
        ElseIf oForm.Items.Item("8").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select From Employee ID", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        ElseIf oForm.Items.Item("10").Specific.value = "" Then
            objAddOn.objApplication.SetStatusBarMessage("Please Select To Employee ID", SAPbouiCOM.BoMessageTime.bmt_Short, True)
            Return False
        End If
        Return True
    End Function

#End Region

End Class
