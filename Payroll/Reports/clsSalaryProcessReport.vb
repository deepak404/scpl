﻿Public Class clsSalaryProcessReport
    Public Const FormType = "AIS_SALPRT"
    Dim objForm As SAPbouiCOM.Form
    Dim objGrid As SAPbouiCOM.Grid
    'Dim objDT As SAPbouiCOM.DataTable
    Dim strSQL As String
    Dim oGridColumn As SAPbouiCOM.EditTextColumn
    Public Sub LoadScreen(ByVal Month As String, ByVal Year As Integer)
        Try
            objForm = objAddOn.objUIXml.LoadScreenXML("SalaryProcessReport.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
            objForm.Freeze(True)
            objForm.State = SAPbouiCOM.BoFormStateEnum.fs_Maximized
            objForm = objAddOn.objApplication.Forms.ActiveForm()
            objGrid = objForm.Items.Item("3").Specific
            objGrid.DataTable = objForm.DataSources.DataTables.Add("3")
            strSQL = "select DISTINCT T1.empID [Emp ID],T1.firstname +' ' +T1.lastName [Employee Name],T1.WorkCity [Branch],T1.bankAcount [Account Number]" & _
                           ",round(T0.U_basic,0,0)[Basic],round(T0.U_hra,0,0) [HRA],round(T0.U_con,0,0)[Conveyance]" & _
                           ",round(T0.U_medall ,0,0)[Medical],round((T0.U_eduall ),0,0)[Education],round(T0.U_tele ,0,0)[Telephone],round((T0.U_treim ),0,0)[Reimbursement]" & _
                            ",round(T0.U_tarrer ,0,0)[Arrear],round(T0.U_totot ,0,0)[OT],round(T0.U_oadd ,0,0)[Other Addition],round((T0.U_bonus ),0,0)[Bonus]" & _
                            ",round(T0.U_paidhol ,0,0)[Earned PH],round((T0.U_tloan ),0,0) [Loan],round(T0.U_bonpble,0,0)[Bonus Payable] ,round((T0.U_pf),0,0)[PF]" & _
                            ",round(T0.U_esi,0,0)[ESI],round((T0.U_pt ),0,0)[PT],round((T0.U_it),0,0)[IT],round(T0.U_canded ,0,0)[Canteen],round(T0.U_dpaidhol ,0,0)[Deducted PH]" & _
                            ",round(T0.U_dedot ,0,0)[Deducted OT],round(U_adv,0,0) [Salary Advance],round((T0.U_lwf ),0,0)[LWF],round(T0.U_tds ,0,0)[TDS],round(T0.U_oded ,0,0)[Other Deductions],round(U_near,0,0) [Earnings]" & _
                            ",round(U_nded,0,0) [Deduction],round(U_npay,0,0) [Net pay] from [@AIS_ALPSL] T0 left outer join OHEM T1 on T0.U_empid =T1.empID " & _
                            "where T0.U_month='" & Month & "' and T0.U_year='" & Year & "'"
            objGrid.DataTable.ExecuteQuery(strSQL)
            For i As Integer = 0 To objGrid.Columns.Count - 1
                'objGrid.Columns.Item("RowsHeader").value = i + 1
                oGridColumn = objGrid.Columns.Item(i)
                oGridColumn.Editable = False
            Next
            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objAddOn.objApplication.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
            objForm.Freeze(False)

        End Try

    End Sub
End Class
