﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Sql
Public Class clsDailyAttendRpt
    Public Const FormType = "MNU_DARPT"
    Dim oForm As SAPbouiCOM.Form
    Dim FrmDate, ToDate As Date
    Public Sub LoadScreen()
        oForm = objAddOn.objUIXml.LoadScreenXML("DailyAttenRpt.xml", Mukesh.SBOLib.UIXML.enuResourceType.Embeded, FormType)
        oForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
        objAddOn.objPaySlip.SAPassWord()
    End Sub

    Public Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVal.BeforeAction = True Then
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        Try
                            If pVal.ItemUID = "1" Then
                                If ValidateEvent(FormUID) = False Then
                                    BubbleEvent = False
                                    Exit Sub
                                End If
                            End If
                           
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("Before Action - Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try
                End Select

            Else
                Select Case pVal.EventType
                    Case SAPbouiCOM.BoEventTypes.et_CLICK
                        Try
                            If pVal.ItemUID = "1" Then
                                FrmDate = oForm.Items.Item("4").Specific.string
                                ToDate = oForm.Items.Item("6").Specific.string
                                GenerateReport(FormUID, FrmDate, ToDate)
                            End If
                        Catch ex As Exception
                            objAddOn.objApplication.SetStatusBarMessage("After Action - Click Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
                        End Try
                End Select
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Item Event Failure", SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
    End Sub

#Region "To Generate Report"
    Public Sub GenerateReport(ByVal FormUID As String, ByVal FDate As String, ByVal TDate As String)
        Dim sender As System.Object
        Dim e As System.EventArgs
        sender = ""
        e = Nothing
        Dim RptFrm As FrmRpt
        RptFrm = New FrmRpt
        RptFrm.Refresh()
        Dim cryRpt As New ReportDocument

        Dim objConInfo As New CrystalDecisions.Shared.ConnectionInfo
        Dim oLogonInfo As New CrystalDecisions.Shared.TableLogOnInfo
        'Dim ConInfo As New CrystalDecisions.Shared.TableLogOnInfo
        Dim intCounter As Integer

        cryRpt.Load(System.Windows.Forms.Application.StartupPath & "\Reports\DailyAttendance1.rpt")

        Dim crParameterValues As New ParameterValues
        Dim crParameterDiscreteValue As New ParameterDiscreteValue


        Dim SerNam, DbName, UID As String
        SerNam = objAddOn.objCompany.Server
        DbName = objAddOn.objCompany.CompanyDB
        UID = objAddOn.objCompany.DbUserName

        oLogonInfo.ConnectionInfo.ServerName = SerNam
        oLogonInfo.ConnectionInfo.DatabaseName = DbName
        oLogonInfo.ConnectionInfo.UserID = UID
        oLogonInfo.ConnectionInfo.Password = SAPwd '"AIS@1234"
        ' oLogonInfo.ConnectionInfo.Password = "Pass@123"

        For intCounter = 0 To cryRpt.Database.Tables.Count - 1
            cryRpt.Database.Tables(intCounter).ApplyLogOnInfo(oLogonInfo)
        Next

        cryRpt.SetParameterValue("FrmDate@", FDate)
        cryRpt.SetParameterValue("ToDate@", TDate)
        RptFrm.CRViewer.ReportSource = Nothing
        RptFrm.CRViewer.ReportSource = cryRpt
        RptFrm.CRViewer.Refresh()
        System.Windows.Forms.Application.Run(RptFrm)
     
    End Sub
#End Region

#Region "Valiodation Event For From Date and To Date"
    Public Function ValidateEvent(ByVal FormUID As String) As String
        Try
            If (oForm.Items.Item("4").Specific.value = "" Or oForm.Items.Item("6").Specific.value = "") Then
                objAddOn.objApplication.SetStatusBarMessage("From Date and ToDate is Mandatory", SAPbouiCOM.BoMessageTime.bmt_Short, True)
                Return False
            End If
        Catch ex As Exception
            objAddOn.objApplication.SetStatusBarMessage("Validate Event Failure - " & ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, True)
        End Try
        Return True
    End Function
#End Region

End Class
